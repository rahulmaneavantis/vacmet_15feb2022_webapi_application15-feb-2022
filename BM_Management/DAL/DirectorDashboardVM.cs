﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Managment.DAL
{
    public class DirectorDashboardVM
    {
       
        public class DirectorDashboardCount
        {
            public int? EntityCount { get; set; }
            public int? DirectorCount { get; set; }
            public int? MeetingsCount { get; set; }
            public int? AgendaCount { get; set; }
            public int? DraftMinutesCount { get; set; }
        }

        public class DirectorCompliancesVM
        {
            public string Email { get; set; }
            public int EntityId { get; set; }
            public string CompanyName { get; set; }
            public long? DirectorId { get; set; }
            public string DirectorName { get; set; }
            public long ScheduleOnID { get; set; }
            public int RoleID { get; set; }
            public string MappingType { get; set; }
            public long MeetingID { get; set; }
            public string MeetingTitle { get; set; }
            public string ShortForm { get; set; }
            public string ShortDescription { get; set; }
            public string Description { get; set; }
            public string RequiredForms { get; set; }
            public int? ScheduleOnInt { get; set; }
            public DateTime? ScheduleOn { get; set; }
            public string ScheduleOnTime { get; set; }
            public string ComplianceStatus { get; set; }
            public string Frequency { get; set; }
            public string ForMonth { get; set; }
            public long? ForPeriod { get; set; }
            public string Risk { get; set; }
            public string FY_Year { get; set; }
            public DashboradCount DirDasbordcount { get; set; }
        }
        public class DashboradCount
        {
            
            public int ClosedTimely { get; set; }
            public int ClosedDelayed { get; set; }
            public int Overdue { get; set; }
 
        }

        public class EntityWiseStatusVM
        {
            public int EntityId { get; set; }
            public string CompanyName { get; set; }

            public int ClosedTimely { get; set; }
            public int ClosedDelayed { get; set; }
            public int Overdue { get; set; }

            // public List<DashboradCount> lstStatus { get; set; }
        }

        }
}