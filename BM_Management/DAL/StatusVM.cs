﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class StatusVM
    {
        public int ID { get; set; }

        public string Status { get; set; }
    }

    public class RiskVM
    {
        public int ID { get; set; }

        public string RiskType { get; set; }
    }
}