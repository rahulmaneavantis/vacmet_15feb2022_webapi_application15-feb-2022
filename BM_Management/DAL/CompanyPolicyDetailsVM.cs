﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
   
    public class CompanyPolicydetailsVM
    {
        public long DocumentId { get; set; }
        //public long EntiDescriptionty_Id { get; set; }
        public string AlteredDoc { get; set; }
        public DateTime? BMDate_LastRenew { get; set; }
        public DateTime? GMDate_UpcomingRenew { get; set; }
        public string License_RegNumber { get; set; }
        public long? FileId { get; set; }
        public string EndTimeAGM { get; set; }
        public string StartTimeAGM { get; set; }
        public string FileName { get; set; }
        public long? FYID { get; set; }
        public string FYText { get; set; }
        public string TypeDesc { get; set; }
        public String DocType { get; set; }
        public string Description { get; set; }


    }
}