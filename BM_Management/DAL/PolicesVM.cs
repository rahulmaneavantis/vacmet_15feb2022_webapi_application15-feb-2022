﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
  
        public partial class PolicesVM
        {
            public long Id { get; set; }

            public string PolicesName { get; set; }

            public string FilePath { get; set; }

            public long? EntityId { get; set; }

        }

    }
