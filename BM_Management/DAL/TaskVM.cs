﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Managment.DAL
{
    public class TaskVM
    {
        public class TaskDetails
        {
            public string Email { get; set; }
            public int UserId { get; set; }
            public int? Createby { get; set; }
            public long Id { get; set; }
            public string TaskTitle { get; set; }
            public string status { get; set; }
            public string DueDate { get; set; }
            public string TaskTypes { get; set; }
            public string RoleName { get; set; }
            public string Taskcreatedby { get; set; }
            public string TaskAssignTo { get; set; }
            public int PageID { get; set; }
            public long TaskId { get; set; }
            public string Description { get; set; }
            public int? EntityId { get; set; }
            public string AssignOnDate { get; set; }
            public long? MeetingId { get; set; }
            public long? AgendaId { get; set; }

            public long? TaskType { get; set; }
            public int Role { get; set; }
            public string Remark { get; set; }
            public string TaskTypeNameforMeeting { get; set; }
            public string TaskTypeNameforAgenda { get; set; }
            public string MeetingName { get; set; }
            public string MeetingType { get; set; }
            public string MeetingPurpose { get; set; }
            public DateTime? MettingDate { get; set; }

            public string AgendaName { get; set; }
            public string TaskTypeName { get; set; }
        }

        public class TaskTypeVM
        {
            public long TaskId { get; set; }
            public string TaskTypeName { get; set; }

            public string TaskTypeNameforMeeting { get; set; }
            public string TaskTypeNameforAgenda { get; set; }
            public string MeetingName { get; set; }
            public string MeetingType { get; set; }
            public string MeetingPurpose { get; set; }
            public DateTime? MettingDate { get; set; }

            public string AgendaName { get; set; }
            public string ComplienceName { get; set; }
        }

        public class TaskFile
        {
            public long Id { get; set; }
            public long? TaskId { get; set; }
            public long? FileId { get; set; }
            public string FileName { get; set; }
            public string UploadedBy { get; set; }
            public DateTime UploadedOn { get; set; }
            public string Remark { get; set; }
        }
        public class TaskRemarks
        {
            public long Id { get; set; }
            public long? TaskId { get; set; }
            public string Role { get; set; }
            public string User { get; set; }

            public DateTime UploadedOn { get; set; }
            public string Remark { get; set; }
        }
        public class TaskMeeting
        {
            public long MeetingId { get; set; }
            public string MeetingTitle { get; set; }
        }

        public class TaskPerandRevData
        {
            public TaskDetailsVM Details { get; set; }

            public MeetingDetailsVM TaskMeetingVM { get; set; }

            public AgendaDetailsVM AgendaVM { get; set; }

            
        }

        public class TaskDetailsVM
        {
            public string TaskTitle { get; set; }
            public string TaskDescription { get; set; }
            public DateTime? DueDate { get; set; }
        }

        public class MeetingDetailsVM
        {
            public string MeetingType { get; set; }
            public DateTime? MeetingDate { get; set; }
            public string MeetingDetails { get; set; }
        }

        public class AgendaDetailsVM
        {
            public string Agenda { get; set; }
        }

        public class TaskUpdateDetailsVM
        {
            public string status { get; set; }

            public StatusRemarkDetailsVM StatusRemarkVM { get; set; }
        }

        public class StatusRemarkDetailsVM
        {
            public long FileId { get; set; }
           
            public string Uploadedby { get; set; }

            public DateTime? Uploadedon { get; set; }

            public string FileName { get; set; }
        }

        public class StatusLogVM
        {
            public string Name { get; set; }
            public string Role { get; set; }
            public string status { get; set; }
            public DateTime? statusDate { get; set; }
        }
       

        public class AddTaskDetails_VM
        {
            public string EmailId { get; set; } 
            public int TaskTypeId { get; set; }
            public long Id { get; set; }
            public int EntityId { get; set; }
            public int MeetingId { get; set; }
            public int AgendaId { get; set; }
            public string TaskTitle { get; set; }
            public string TaskDescription { get; set; }
            public int UserId { get; set; }
            public DateTime DueDate { get; set; }
            public string TaskLevel { get; set; }
            HttpPostedFileBase[] files { get; set; }
        }

        public class UpdateTaskResponse
        {
            public long TaskId { get; set; }
            public string EmailId { get; set; }
            public string Status { get; set; }
            public string Remark { get; set; }
            public HttpRequest files { get; set; }
        }
    }
}