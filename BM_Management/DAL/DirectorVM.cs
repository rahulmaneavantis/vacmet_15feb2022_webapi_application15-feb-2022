﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class DirectorVM
    {
        public int Entity_Id { get; set; }
        public long Director_ID { get; set; }
        public long? UserID { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DIN { get; set; }
        public string Designation { get; set; }
        public int? DesignationId { get; set; }
        public int? PositionId { get; set; }
        public string ImagePath { get; set; }
        public DateTime? DSC_ExpiryDate { get; set; }
        public int itemlist { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public string interestType { get; set; }
        public string FullName { get; set; }
        public DateTime? DateofAppointment { get; set; }
        public string DesignationName { get; set; }

    }

    public class DetailsOfInterestVM
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        public int SrNo { get; set; }
        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
        public string EntityType { get; set; }
        public string NatureofIntrest { get; set; }
        public string NameOfOther { get; set; }
        public string CIN { get; set; }
        public string PAN { get; set; }
        public int TypeOfEntity { get; set; }
        public int NatureOfInterest { get; set; }
        public int IfDirector { get; set; }
        public int IfInterestThroughRelatives { get; set; }
        public string IfDirectorName { get; set; }
        public string IfInterestThroughRelativesName { get; set; }
        public decimal? PercentagesOfShareHolding { get; set; }
        public DateTime Arosed { get; set; }
        public DateTime? DateofAppointment { get; set; }
        public DateTime? BoardofResolution { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool? IsResigned { get; set; }
        public List<RelativesVM> Relatives { get; set; }
        public string DirectorDesignationName { get; set; }
    }

    public class RelativesVM
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        public string Name { get; set; }
        public bool Minor_or_Adult { get; set; }
        public bool MaritalStatus { get; set; }
    }

    public class DirectorDetailsVM
    {

        public long ID { get; set; }
        public long DirectorId { get; set; }
        public string DIN { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }


        public string MobileNo { get; set; }

        public bool ResidentInIndia { get; set; }

        public int? Nationality { get; set; }

        public int Occupation { get; set; }

        public int AreaOfOccupation { get; set; }

        public string OtherOccupation { get; set; }

        public int EducationalQualification { get; set; }

        public string OtherQualification { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public int? Gender { get; set; }

        public string PAN { get; set; }

        public string Adhaar { get; set; }

        public string PassportNo { get; set; }

        public string EmailId { get; set; }

        public string EmailId_Official { get; set; }

        public Nullable<DateTime> DESC_ExpiryDate { get; set; }
        public string FSalutations { get; set; }

        public string FatherFirstName { get; set; }

        public string FatherMiddleName { get; set; }

        public string FatherLastName { get; set; }



        public string Photo_Doc_Name { get; set; }
        public string PAN_Doc_Name { get; set; }
        public string Aadhaar_Doc_Name { get; set; }
        public string Passport_Doc_Name { get; set; }
        public HttpPostedFileBase Photo_Doc { get; set; }
        public HttpPostedFileBase PAN_Doc { get; set; }
        public HttpPostedFileBase Aadhaar_Doc { get; set; }
        public HttpPostedFileBase Passport_Doc { get; set; }
        public int UserID { get; set; }
        public string Approve_disapprove { get; set; }
        public int? NumberofTypeChange { get; set; }
        public bool IsApproved { get; set; }
        public bool IsDisapprove { get; set; }
        public string status { get; set; }
        public bool Ispending { get; set; }

        public DateTime? CreateonDate { get; set; }
        public string ChangedBy { get; set; }
        public long MappingID { get; set; }

        public string Designation { get; set; }
        public DateTime DateofAppointment { get; set; }

        public string ProfileDetails { get; set; }
    }
}