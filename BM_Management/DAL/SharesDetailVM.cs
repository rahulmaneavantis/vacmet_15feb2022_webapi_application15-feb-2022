﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class SharesDetailVM
    {
        public long capitalId { get; set; }
        public decimal AuthorizedCapital { get; set; }
        public string AuthorizedCapitall { get; set; }
        public decimal PaidupCapital { get; set; }
        public string PaidupCapitall { get; set; }
        public decimal EquietyCapital { get; set; }
        public string EquietyCapitall { get; set; }
        public decimal PrefrenceCapital { get; set; }
        public string PrefrenceCapitall { get; set; }
        public decimal DebentureCapital { get; set; }
        public decimal Othersecurities { get; set; }
        public decimal EquietyNominalVal { get; set; }
        public decimal PrefrenceNominalVal { get; set; }
        public decimal? DebencuteNominalVal { get; set; }
        public string Debenture { get; set; }

    }
}