﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public partial class MyComplianceListVM
    {
        public long ComplianceID { get; set; }
        public int EntityId { get; set; }
        public string CompanyName { get; set; }
        public long? DirectorId { get; set; }
        public string DirectorName { get; set; }
        public long ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }
        public long? MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string ShortForm { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public string StatusName { get; set; }
        public string RequiredForms { get; set; }
        public string Frequency { get; set; }
        public string Period { get; set; }
        public List<lstMeeting> FirstMeeting { get; set; }
    }
    public class lstMeeting
    {
        public int EntityId { get; set; }
    }

    public class F_YearVM
    {
        public long? FY { get; set; }
        public string FYText { get; set; }
    }

    public class MeetingVM
    {
        public long? MeetingId { get; set; }
        public string MeetingTitle { get; set; }
    }

    public class ActVM
    {
        public long? ActId { get; set; }
        public string ActName { get; set; }
    }

    public class RoleVM
    {
        public long? Id { get; set; }
        public string RoleName { get; set; }
    }

    public partial class MyCompliance_Director_ResultVM
    {
        public int EntityId { get; set; }
        public string CompanyName { get; set; }
        public long? DirectorId { get; set; }
        public string DirectorName { get; set; }
        public long ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string ShortForm { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string RequiredForms { get; set; }
        public int? ScheduleOnInt { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public string ComplianceStatus { get; set; }
        public string Frequency { get; set; }
        public string ForMonth { get; set; }
        public long? ForPeriod { get; set; }
        public string Risk { get; set; }
    }
}