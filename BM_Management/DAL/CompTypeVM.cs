﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class CompTypeVM
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}