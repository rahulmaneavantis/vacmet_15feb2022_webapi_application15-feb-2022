﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AppWebApplication.BM_Management.DAL
{
    public class CommitteeVM
    {
        

       
    }
    public class CommitteeListVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description_ { get; set; }
        public List<CommitteeDetailList> CommiteeDetailsVM { get; set; }
    }
    public class CommitteeDetailList
    {
        public long Id { get; set; }
        public int Entity_Id { get; set; }
        public string DIN { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string EntityName { get; set; }
        public int Committee_Id { get; set; }
        public string CommitteeName { get; set; }
        public string NameOfOtherCommittee { get; set; }
        public long Director_Id { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
        public int Designation_Id { get; set; }
        public string Designation_Name { get; set; }
        public int Committee_Type { get; set; }
        public string Committee_Type_Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    // for Committee Details 10Jun2021
    public class CommitteeMasterVM
    {
        public long Id { get; set; }
       
        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
      
        public int Committee_Id { get; set; }
        public string CommitteeName { get; set; }
        public string NameOfOtherCommittee { get; set; }

        
        public long Director_Id { get; set; }
        
        public int? UserId { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
        public int Designation_Id { get; set; }
        public int DirectorDesignation_Id { get; set; }
        public string Designation_Name { get; set; }
        public int Committee_Type { get; set; }
        public string Committee_Type_Name { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? AppointmentFrom { get; set; }
        public DateTime? AppointmentTo { get; set; }
        public string ParticipantType { get; set; }
        public string ParticipantDesignation { get; set; }
        public string DIN_PAN { get; set; }
        public int DirectorshipIdForQuorum { get; set; }
        public string Directorship { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public bool IsNew { get; set; }
        public bool? IsStandard { get; set; }

        public DateTime? CessionDate { get; set; }

        
    }

    //For Committee Matrix 11Jun2021
    public class CommitteeMatrixVM
    {
        public long Id { get; set; }
        public int CommitteeId { get; set; }
        public string CommitteeType { get; set; }
        public bool? IsStandard { get; set; }
        public string CommitteeName { get; set; }
        public int? DirectorId { get; set; }
        public int? UserId { get; set; }
        public string ParticipantType { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public int? Gender { get; set; }
        public string DIN_PAN { get; set; }
        public int? DesignationId { get; set; }
        public string Designation { get; set; }
        public int? DirectorShipIdForQuorum { get; set; }
        public string DirectorShip { get; set; }
        public string ParticipantDesignation { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public System.DateTime? DateOfAppointment { get; set; }
        public System.DateTime? DateOfCessation { get; set; }
        public string PhotoDoc { get; set; }
    }

}