﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class DocumentVM
    {
      
            public string FileName { get; set; }
            public byte[] FileContent { get; set; }
            public bool IsFileGenerated { get; set; }
       
    }
}