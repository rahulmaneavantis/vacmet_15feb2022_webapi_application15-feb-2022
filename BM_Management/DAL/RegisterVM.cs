﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class RegisterVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
           }
    public class DirectorListVM
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
    public class EntityRegisterVM
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string Address { get; set; }
    }
    public class DirectorsRegisterVM
    {
        public long DirectorId { get; set; }
        public string DIN { get; set; }
        public string FullName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string PresentAddress { get; set; }
        public string ParmanentAddress { get; set; }
        public string Nationalality { get; set; }
        public string Occupation { get; set; }
        public DateTime? DateofBoardResolution { get; set; }
        public DateTime? DateofAppointmentandReappointment { get; set; }
        public string DateofCessation { get; set; }
        public string ICSNo { get; set; }
        public string PAN { get; set; }
        public string EmailId { get; set; }

    }
    public class DirectorSecurityVM
    {
        public string CIN { get; set; }
        public string CompanyName { get; set; }
        public long? NoofSecurities { get; set; }
        public string DescriptionofSecurity { get; set; }
        public long? NominalvalueofSecurities { get; set; }
        public string DateofAccusation { get; set; }
        public decimal? PricePaidforaccusation { get; set; }
        public decimal? otherPricePaidforaccusation { get; set; }
        public string DateofDesposal { get; set; }
        public decimal? Pricerecivefordisposal { get; set; }
        public decimal? OtherPriceforDisposal { get; set; }

        public long? CumaltiveBalance { get; set; }
        public string ModeofAccusation { get; set; }
        public string ModeOfHolding { get; set; }
        public string SecurityHasbeenpledge { get; set; }
    }

    public class ReturnFileVM
    {
        public string FileName { get; set; }
    }

    public class VMShareholding
    {
        public int? designationId { get; set; }
        public long DirectorId { get; set; }
        public int Count { get; set; }
        public long Id { get; set; }
        public string Follio_No { get; set; }
        public string DPIN_PAN { get; set; }
        public string DPIN { get; set; }
        public string PAN { get; set; }
        public decimal? Tot_SharesHeld { get; set; }
        public decimal? Contribution_Obligation { get; set; }
        public decimal? Contribution_Received { get; set; }
        public decimal perofTotShareHolding { get; set; }
        public string Member_Name { get; set; }
        public string EmailId { get; set; }
        public string EntityName { get; set; }
        public decimal paidupCapital { get; set; }
        public ShareHoldings Shareholding { get; set; }
        //public List<ShareHoldingDetails> lstShareHoldingDetails { get; set; }
        //public Response Message { get; set; }

        public decimal? equity_PaidUp { get; set; }
        public decimal? Prifrencepaidup { get; set; }
        public decimal? perofHolding { get; set; }

        public long PartnerId { get; set; }
        public long partnerShersID { get; set; }
        public int EntityID { get; set; }
        public string NatureofIntrest { get; set; }
        public string Designation { get; set; }
        public int? BCID { get; set; }
        public int? BodyCorpAsPartnerId { get; set; }
    }

    public class ShareHoldings
    {
        public long Id { get; set; }
        public int Entity_Id { get; set; }
        public decimal TotalPrefrenceCapital { get; set; }
        public int? Customer_Id { get; set; }
      
        public string Follio_No { get; set; }
       
        public string Shares_Class { get; set; }
       
        public int Type { get; set; }

        public long NominaVal_per_Shares { get; set; }
      
        public long Tot_SharesHeld { get; set; }
        //[Required(ErrorMessage = "Please Enter Name of the member")]
        public string Member_Name { get; set; }
        public string JointHolder_Name { get; set; }
       
        public string bodycorporate_Address { get; set; }
        public string CIN { get; set; }
        //[Required(ErrorMessage = "Please Enter Email Address")]
       
        public string Email_Id { get; set; }
        public string Father_Mother_Spouse_Name { get; set; }

        public string Marital_Status { get; set; }

        public string Occupation { get; set; }
        
        public string PAN { get; set; }
        //[Required(ErrorMessage = "Please Select Nationality.")]
        public int Nationality { get; set; }
        //DataFormatString = @"{0:d}", 
        public string Guardian_Name { get; set; }
       
        public DateTime? DOB_minor { get; set; }
      
        public DateTime? becomingmember_Date { get; set; }
        
        public DateTime? declaration_Date_under89 { get; set; }
        public string Name_and_Address_ben { get; set; }
        
        public DateTime? Nomination_Date_Receipt { get; set; }
        public string NomineeName_Address { get; set; }
        public long? shares_abeyance { get; set; }
        public string lienonshares { get; set; }

        
        public DateTime? Cessation_Membership_Date { get; set; }
        public string Sendingnotices_Instruction { get; set; }
        public string PowerofAttorny { get; set; }
        public bool IsMemberMinnor { get; set; }
      
     
        public bool ISPramoter { get; set; }
        //public long? DirectorId { get; set; }
       
    }

    public class ChargeHeaderVM 
    {
        public long ChargeHeaderId { get; set; }
        public string ChargeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime? SatisficationDate { get; set; }
        public string ChargeHolder { get; set; }
        public string ChargeAmount { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
    }

    public class MemberRegister
    {
        public int MemberId { get; set; }
        public string EntityName { get; set; }
        public string EntityAddress { get; set; }
        public string FolioNumber { get; set; }
        public string ClassofShares { get; set; }
        public decimal Nominalvaluepershares { get; set; }
        public decimal? TotalSharesHeld { get; set; }
        public string MemberName { get; set; }
        public string JointHolderName { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string CIN { get; set; }
        public string UIN { get; set; }
        public string Father_Mother_SpouseName { get; set; }
        public string Status { get; set; }
        public string PAN { get; set; }
        public string GuardianName { get; set; }
        public string DateofBirth { get; set; }
        public string DateofBecomingMember { get; set; }
        public string Dateunder89 { get; set; }
        public string BenificialNameandAddress { get; set; }
        public string DateofNomination { get; set; }
        public string NameandAddressofNomminee { get; set; }
        public long? sharesinAbayes { get; set; }
        public string DateofCessation { get; set; }

        public MemberSecurities MemberSecuritiesDetails { get; set; }
    }

    public class MemberSecurities
    {
        public string AllotmentNo { get; set; }
        public string DateofAllotment { get; set; }
        public int? NumberofShares { get; set; }
        public string DistinctiveNoofShares_From { get; set; }
        public string DistinctiveNoofShares_To { get; set; }
        public string TransferorFolioNo { get; set; }
        public string NameOfTransferror { get; set; }
        public string DateofIssues { get; set; }
        public string CertificateNumber { get; set; }
        public string LockinPeriod { get; set; }
        public decimal? AmountPayable { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? AmountDue { get; set; }
        public string DescofConsidration { get; set; }
        public string DateofTransfer { get; set; }
        public int? NoofSharesTransfered { get; set; }

        public string DistinctiveNo_Form { get; set; }
        public string DistinctiveNo_To { get; set; }
        public string TransfreeFolio { get; set; }
        public string NameofTransfaree { get; set; }
        public int? BalanceofSharesHeld { get; set; }
        public string Remark { get; set; }
        public string Authentication { get; set; }
    }

    public class ChargeRegister
    {

        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
        public int Charge_Id { get; set; }
        public string ChargeIdMannual { get; set; }
        public int ChargeTypeId { get; set; }
        public DateTime Charge_CreationDate { get; set; }
        public string ChargeType { get; set; }
        public string Charge_TypeDesc { get; set; }
        public string ChargeAmount { get; set; }
        
        public DateTime CreationDate { get; set; }
       
        public DateTime? RegistrationChargeCreateDate { get; set; }
       
        public DateTime? ModificationDate { get; set; }
       
        public DateTime? RegistrationModificationDate { get; set; }

        
        public DateTime? SatisfactionDate { get; set; }
        public string InFavour { get; set; }
        public int? CreatedBy { get; set; }
        public string ShortDescPropertyCharged { get; set; }
        public string Namesaddresseschargeholder { get; set; }
        public string TermsconditionsOfcharge { get; set; }
        public string Descinstrument { get; set; }
        public DateTime? RegistrationChargeModificationDate { get; set; }
        public string Descinstrumentchargemodify { get; set; }
        public string Particularsmodification { get; set; }
        
        public DateTime? RegistrationsatisfactionDate { get; set; }
        
        public DateTime? FactsDelaycondonationDate { get; set; }
        public string Reasonsdelayfiling { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool Status { get; set; }
        public bool IsActive { get; set; }

    }

}