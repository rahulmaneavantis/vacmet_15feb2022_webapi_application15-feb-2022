﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class EntityVM
    {
        public int EntityId { get; set; }
        public string Entity { get; set; }
    }

    public class EntityDetailVM
    {
        public int Id { get; set; }
        public int Entity_Type { get; set; }
        public string RocCode { get; set; }
        public string EntityName { get; set; }
        public string Type { get; set; }
        public string LLPIN { get; set; }
        public string LLPI_CIN { get; set; }
        public string RegistrationNO { get; set; }
        public string CIN { get; set; }
        public string PAN { get; set; }
        public bool Islisted { get; set; }
        public bool? isDemate { get; set; }
        public bool? IsPartialorComplited { get; set; }
        public int? CustomerId { get; set; }
        public string CompanyCategory { get; set; }
        public string CompanysubCategory { get; set; }
        public string classofCompany { get; set; }
        public string RegisterAddress { get; set; }
        public string emailId { get; set; }
        public DateTime dateofIncorporaion { get; set; }
     
    }

    public class UserVM
    {
        public long UserId { get; set; }
        public string userName { get; set; }
    }
    public class TaskMeetingVM
    {
        public long MeetingId { get; set; }
        public string Meeting { get; set; }
    }
    public class TaskAgendagVM
    {
        public long AgendaId { get; set; }
        public string Agenda { get; set; }
    }
    public class TaskTypeDetailsVM
    {
        public long Id { get; set; }
        public string Type { get; set; }
    }
}