﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class FY_VM
    {
        public string FY { get; set; }
        public string FYName { get; set; }
    }
}