﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Management.DAL
{
    public class CompanyGroupVM
    {
   
            public long SubcompanyId { get; set; }
            public long EntityID { get; set; }
            public int CustomerId { get; set; }
            public string SubCompanyName { get; set; }
            public string ParentCIN { get; set; }
            public string CIN { get; set; }
            public int subcompanyTypeID { get; set; }
            public string subcompanyTypeName { get; set; }
            public decimal? PersentageofShareholding { get; set; }
         
    }
}