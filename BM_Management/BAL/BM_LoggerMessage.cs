﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.BM_Managment.BAL
{
    public class BM_LoggerMessage
    {
        public class LoggerMessage_AVASEC
        {
            public static void InsertPathlog(Exception ex, string ClassName, string FunctionName)
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        BM_APILogMessage obj = new BM_APILogMessage();
                        obj.LogLevel = 0;
                        obj.ClassName = ClassName;
                        obj.FunctionName = FunctionName;
                        if (ex != null)
                        {
                            obj.Message = ex.Message + "----\r\n" + ex.InnerException;
                            obj.StackTrace = ex.StackTrace;
                        }
                        else
                        {
                            obj.Message = ClassName;
                            obj.StackTrace = ClassName;
                        }
                        obj.CreatedOn = DateTime.Now;

                        entities.BM_APILogMessage.Add(obj);
                        entities.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public static void InsertPathlog(string ClassName, string FunctionName)
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        BM_APILogMessage obj = new BM_APILogMessage();
                        obj.LogLevel = 0;
                        obj.ClassName = ClassName;
                        obj.FunctionName = FunctionName;

                        obj.Message = ClassName;
                        obj.StackTrace = ClassName;

                        obj.CreatedOn = DateTime.Now;

                        entities.BM_APILogMessage.Add(obj);
                        entities.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }
}