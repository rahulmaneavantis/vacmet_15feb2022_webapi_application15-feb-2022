﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.TaskVM;
using AppWebApplication.BM_Managment.DAL;
using AppWebApplication.BM_Management.DAL;

namespace AppWebApplication.BM_Managment.BAL
{
    public class TaskManagment
    {
        public static List<TaskDetails> GetTaskDetails(int UserId)
        {
            List<TaskDetails> _objtask = new List<TaskDetails>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getTaskDetails = (from row in entities.BM_SP_Task(UserId,true,false) orderby row.ID descending select row).ToList();
                    _objtask = (from a in getTaskDetails
                                orderby a.ID descending
                                where a.RoleName.Trim() == "Performer"
                                select new TaskDetails
                                {
                                    UserId = (int)a.UserID,
                                    Createby = a.Createdby,
                                    Id = a.ID,
                                    TaskTitle = a.TaskTitle,
                                    status = a.Status,
                                    DueDate = a.DueDate.ToString("dd/MMM/yyyy"),
                                    TaskTypes = a.TaskType,
                                    RoleName = a.RoleName,
                                    Taskcreatedby = (from u in entities.Users where u.ID == a.Createdby select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                    TaskAssignTo = (from u in entities.Users where u.ID == a.UserID select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                }).ToList();

                    if (_objtask.Count > 0)
                    {
                        _objtask = (from g in _objtask
                                    group g by new
                                    {
                                        g.TaskId,
                                        g.UserId,
                                        g.Createby,
                                        g.Id,
                                        g.TaskTitle,
                                        g.Description,
                                        g.EntityId,
                                        g.DueDate,
                                        g.AssignOnDate,
                                        g.status,
                                        g.RoleName,
                                        g.TaskTypes,
                                        g.TaskAssignTo,
                                        g.Taskcreatedby,

                                    } into GCS
                                    select new TaskDetails()
                                    {
                                        Id = GCS.Key.Id,
                                        UserId = GCS.Key.UserId,
                                        Createby = GCS.Key.Createby,
                                        TaskTitle = GCS.Key.TaskTitle,
                                        Description = GCS.Key.Description,
                                        EntityId = GCS.Key.EntityId,
                                        DueDate = GCS.Key.DueDate,
                                        AssignOnDate = GCS.Key.AssignOnDate,
                                        status = GCS.Key.status,
                                        RoleName = GCS.Key.RoleName,
                                        TaskTypes = GCS.Key.TaskTypes,
                                        TaskAssignTo = GCS.Key.TaskAssignTo,
                                        Taskcreatedby = GCS.Key.Taskcreatedby,

                                    }).ToList();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objtask;
        }

        public static List<TaskPerandRevData> EditTaskDetails(int UserId, int CustomerId, int taskId)
        {
            List<TaskPerandRevData> _objedittask = new List<TaskPerandRevData>();
           
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objdata = (from row in entities.BM_Task
                                    where row.ID == taskId
                                    select row).FirstOrDefault();
                    if (_objdata != null)
                    {
                        var _objMeeting = (dynamic)null;
                        var objAjenda= (dynamic)null;

                    
                        
                         if (_objdata.MeetingId > 0 && _objdata.TaskType == 1)
                        {
                         
                            _objMeeting = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == _objdata.MeetingId select new { x, y }).FirstOrDefault();
                       
                        }
                        else if (_objdata.TaskType == 2 && _objdata.MeetingId > 0 && _objdata.AgendaId > 0)
                        {
                            _objMeeting = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == _objdata.MeetingId select new { x, y }).FirstOrDefault();

                            objAjenda = (from x in entities.BM_MeetingAgendaMapping where x.MeetingAgendaMappingID == _objdata.AgendaId && x.MeetingID == _objdata.MeetingId select x).FirstOrDefault();
                            if (objAjenda != null)
                            {
                                if (objAjenda.AgendaItemTextNew != null && objAjenda.AgendaItemTextNew != "")
                                {
                                    objAjenda = objAjenda.AgendaItemTextNew;
                                }
                                else
                                {
                                    objAjenda = objAjenda.AgendaItemText;
                                }
                            }
                        }
                         else if(_objdata.TaskType == 4)
                        {
                            _objedittask.Add(new TaskPerandRevData { Details = new TaskDetailsVM { TaskTitle = _objdata.TaskTitle, TaskDescription = _objdata.TaskDesc, DueDate = _objdata.DueDate } });
                        }
                        if (_objedittask.Count == 0)
                        {
                            _objedittask.Add(new TaskPerandRevData
                            {
                                Details = new TaskDetailsVM { TaskTitle = _objdata.TaskTitle, TaskDescription = _objdata.TaskDesc, DueDate = _objdata.DueDate },

                                TaskMeetingVM = new MeetingDetailsVM { MeetingType = _objMeeting.y.MeetingTypeName, MeetingDetails = _objMeeting.x.MeetingTitle, MeetingDate = _objMeeting.x.MeetingDate },

                                AgendaVM = new AgendaDetailsVM { Agenda = objAjenda }
                            }
                            );
                        }
                       
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objedittask;
        }

        public static List<StatusRemarkDetailsVM> GetTaskUpdatedDetails(int taskId)
        {
            List<StatusRemarkDetailsVM> _objdetails = new List<StatusRemarkDetailsVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objdetails = (from row in entities.BM_FileData
                                   where row.TaskId == taskId where row.IsDeleted==false
                                   select new StatusRemarkDetailsVM
                                   {
                                       FileId=row.Id,
                                       Uploadedby = (from x in entities.Users where x.ID == row.UploadedBy select x.FirstName + " " + x.LastName).FirstOrDefault(),
                                       FileName=row.FileName,
                                       Uploadedon=row.UploadedOn

                                   }).ToList();
                }


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objdetails;
        }

        public static List<StatusLogVM> GetTaskStatuslogDetails(int taskId)
        {
            List<StatusLogVM> _objlogstatus = new List<StatusLogVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objlogstatus = (from row in entities.BM_Task_SP_Status(taskId)
                                 select new StatusLogVM

                                 {
                                     Name = row.UserName,
                                     Role = row.RoleName,
                                     status = row.status,
                                     statusDate = row.CreatedOn
                                 }
                                 ).ToList();
            }
            return _objlogstatus;
        }

        public static List<TaskMeetingVM> GetTaskMeetingDetails(int userId, int CostomerId, int entityId)
        {
            List<TaskMeetingVM> _objGetMeetingList = new List<TaskMeetingVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objGetMeetingList = (from row in entities.BM_Meetings
                                      where row.EntityId == entityId
                                      select new TaskMeetingVM
                                      {
                                          MeetingId = row.MeetingID,
                                          Meeting = row.MeetingTitle
                                      }
                                          ).ToList();

            }
            return _objGetMeetingList;
        }

        public static List<UserVM> GetTaskUserDetails(int UserId, int CustomerId)
        {
            List<UserVM> getUser = new List<UserVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    getUser = (from row in entities.Users
                               join rows in entities.Roles
                              on row.SecretarialRoleID equals rows.ID
                               where
                                row.IsActive == true && row.IsDeleted == false
                                 && row.CustomerID == CustomerId
                                 && row.ID != UserId
                               select new UserVM
                               {
                                   UserId = row.ID,
                                   userName = row.FirstName + " " + row.LastName,
                               }).OrderBy(row => row.userName).ToList();

                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return getUser;
        }

        public static List<EntityVM> GetTaskEntityDetails(int userId, int CustomerId)
        {
            List<EntityVM> _onjTasklist = new List<EntityVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _onjTasklist = (from row in entities.BM_SP_EntityListforBOD(userId, CustomerId)
                                    select new EntityVM
                                    {
                                        Entity = row.CompanyName,
                                        EntityId = row.ID
                                    }).ToList();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _onjTasklist;
        }

        public static List<TaskAgendagVM> GetTaskAgendaDetails(int v1, int v2, int entityId, int meetingId)
        {
            List<TaskAgendagVM> _objGetMeetingList = new List<TaskAgendagVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objGetMeetingList = (from row in entities.BM_MeetingAgendaMapping
                                      where row.IsDeleted == false && row.MeetingID == meetingId
                                      select new TaskAgendagVM

                                      {
                                          AgendaId = row.MeetingAgendaMappingID,
                                          Agenda = row.AgendaItemText,
                                      }

                                          ).ToList();

                return _objGetMeetingList;
            }
        }

        public static List<TaskTypeDetailsVM> GetTaskTypeDetails(int v1, int v2)
        {
            List<TaskTypeDetailsVM> _objType = new List<TaskTypeDetailsVM>();
            try
            {
                _objType = new List<TaskTypeDetailsVM>
                {
                    new TaskTypeDetailsVM {Id=-1,Type="Select" },
                    new TaskTypeDetailsVM {Id=1,Type="Meeting" },
                    new TaskTypeDetailsVM {Id=2,Type="Agenda"},
                    new TaskTypeDetailsVM {Id=3,Type="Compliance" },
                     new TaskTypeDetailsVM {Id=4,Type="other" }
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objType;
        }
    }
}