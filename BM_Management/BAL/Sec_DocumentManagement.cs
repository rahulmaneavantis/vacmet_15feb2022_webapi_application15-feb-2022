﻿using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf.Parsing;

using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using AppWebApplication.BM_Managment.DAL;
using static AppWebApplication.BM_Management.DAL.AnnotationVM;

namespace AppWebApplication.BM_Management.BAL
{
    public class Sec_DocumentManagement
    {
        public static string fetchpath(int agendaMappingId, int UserId)
        {
            string CompDocReviewPath = string.Empty;
            try
            {

                List<BM_FileData> ComplianceFileData = new List<BM_FileData>();


                ComplianceFileData = GetFileData(Convert.ToInt32(agendaMappingId)).ToList();


                using (ZipFile ComplianceZip = new ZipFile())
                {


                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = UserId + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return CompDocReviewPath;
        }

        public static string fetchpathAgendaDoc(int meetingId, int userId, int CustomerId)
        {
            string file = "";
            try
            {
                var result = new DocumentVM();

                string agendaReviewFileName = null;


                if (meetingId > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        agendaReviewFileName = (from row in entities.BM_Meetings
                                                where row.MeetingID == meetingId && row.GenerateAgendaDocument == false
                                                select row.AgendaFilePath).FirstOrDefault();
                    }
                }

                if (!string.IsNullOrEmpty(agendaReviewFileName))
                {

                    string inputfilepath = (agendaReviewFileName).ToString();
                    inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                    inputfilepath = inputfilepath.Replace(@"/", @"\");

                    string ApplicationPathSource = string.Empty;

                    ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();
                    LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), ApplicationPathSource);

                    string filepathserver = ApplicationPathSource + inputfilepath;

                    string filePath = Path.Combine(filepathserver);
                    LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), filePath);
                    if (File.Exists(filePath))
                    {

                        using (ZipFile ComplianceZip = new ZipFile())
                        {

                            string destinationPath = string.Empty;

                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), destinationPath);

                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);

                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userId)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = userId + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);

                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                            bw.Close();


                            file = finalfilepathforuploadfile;

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return file;
        }

        public static string fetchpathAgendapdftronDoc_old(int meetingId, int UserId, int CustomerId, string role, long meetingParticipantId)
        {
            string file = "";
            try
            {
                var result = new AgendaMinutesCommentsVM();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_MeetingAgendaMinutesComments
                              where row.MeetingId == meetingId && row.DocType == "A" &&
                              row.IsActive == true
                              select new AgendaMinutesCommentsVM
                              {
                                  AgendaMinutesCommentsId = row.Id,
                                  MeetingId = row.MeetingId,
                                  DocType = row.DocType,
                                  FilePath = row.FilePath,
                                  CommentData = row.CommentData,
                                  MeetingParticipantId = meetingParticipantId
                              }).FirstOrDefault();

                    if (result == null)
                    {
                        result = new AgendaMinutesCommentsVM()
                        {
                            MeetingId = meetingId,
                            DocType = "A",
                            CommentData = "",
                            MeetingParticipantId = meetingParticipantId,
                        };
                        var fileDetails = (from row in entities.BM_Meetings
                                           where row.MeetingID == meetingId && row.IsDeleted == false
                                           select new
                                           {
                                               row.AgendaFilePath,
                                               row.EnType
                                           }).FirstOrDefault();
                        if (fileDetails != null)
                        {
                            result.FilePath = fileDetails.AgendaFilePath;

                            if (!string.IsNullOrEmpty(result.FilePath))
                            {
                                var _obj = new BM_MeetingAgendaMinutesComments()
                                {
                                    MeetingId = meetingId,
                                    DocType = "A",
                                    FilePath = result.FilePath,
                                    EnType = fileDetails.EnType,
                                    CommentData = "",
                                    IsActive = true,
                                    CreatedBy = UserId,
                                    CreatedOn = DateTime.Now
                                };
                                entities.BM_MeetingAgendaMinutesComments.Add(_obj);
                                entities.SaveChanges();

                                result.AgendaMinutesCommentsId = _obj.Id;
                            }
                        }
                    }

                    if (result != null)
                    {
                        if (role == "HDCS" || role == "CS")
                        {
                            result.IsCS = true;
                        }
                        LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), result.FilePath);
                        if (!string.IsNullOrEmpty(result.FilePath))
                        {
                            
                            string inputfilepath = (result.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();
                            LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), ApplicationPathSource);

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver);
                            if (File.Exists(filePath))
                            {

                                using (ZipFile ComplianceZip = new ZipFile())
                                {

                                    string destinationPath = string.Empty;

                                    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                                    LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), destinationPath);

                                    string Folder = "~/TempFiles";
                                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + FileData;

                                    string extension = System.IO.Path.GetExtension(result.FilePath);

                                    string DateFolderforconvert = (DateFolder).ToString();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                    string finalDateFolder = destinationPath + DateFolderforconvert;

                                    Directory.CreateDirectory(finalDateFolder);

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = UserId + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    //for server change
                                    string filenameConvertforfilepath = FileName;
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                    string destinationpathplaceoutput = destinationPath + @"\";

                                    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);

                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                                    bw.Close();


                                    file = finalfilepathforuploadfile;
                                }
                            }
                            else
                            {
                                file = "File not found";
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return file;
        }

        //API for Draft minutes And Agenda Dcoument View -- 10 aug 2021
        public static string fetchpathAgendapdftronDoc(int meetingId, int UserId, int CustomerId, string role, long meetingParticipantId, string docType, long? draftCirculationID, bool viewOnly)
        {
            string file = "";
            try
            {
                var result = new AgendaMinutesCommentsVM();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_MeetingAgendaMinutesComments
                              where row.MeetingId == meetingId && row.DocType == docType &&
                              row.IsActive == true
                              select new AgendaMinutesCommentsVM
                              {
                                  AgendaMinutesCommentsId = row.Id,
                                  MeetingId = row.MeetingId,
                                  DocType = row.DocType,
                                  FilePath = row.FilePath,
                                  CommentData = row.CommentData,
                                  MeetingParticipantId = meetingParticipantId
                              }).FirstOrDefault();

                    if (result == null)
                    {
                        result = new AgendaMinutesCommentsVM()
                        {
                            MeetingId = meetingId,
                            DocType = docType,
                            CommentData = "",
                            MeetingParticipantId = meetingParticipantId,
                        };
                        var fileDetails = (from row in entities.BM_Meetings
                                           where row.MeetingID == meetingId && row.IsDeleted == false
                                           select new
                                           {
                                               row.AgendaFilePath,
                                               row.EnType,
                                               row.DraftMinutesFilePath
                                              
                                           }).FirstOrDefault();
                        if (fileDetails != null)
                        {

                            if (docType == "A")
                            {
                                result.FilePath = fileDetails.AgendaFilePath;
                            }
                            else
                            {
                                result.FilePath = fileDetails.DraftMinutesFilePath;
                            }

                            if (!string.IsNullOrEmpty(result.FilePath))
                            {
                                var _obj = new BM_MeetingAgendaMinutesComments()
                                {
                                    MeetingId = meetingId,
                                    DocType = docType,
                                    FilePath = result.FilePath,
                                    EnType = fileDetails.EnType,
                                    CommentData = "",
                                    IsActive = true,
                                    CreatedBy = UserId,
                                    CreatedOn = DateTime.Now
                                };
                                entities.BM_MeetingAgendaMinutesComments.Add(_obj);
                                entities.SaveChanges();

                                result.AgendaMinutesCommentsId = _obj.Id;
                            }
                        }
                    }

                    if (result != null)
                    {
                        if (role == "HDCS" || role == "CS")
                        {
                            result.IsCS = true;
                        }
                        else
                        {
                            #region Added on 28 July 2021
                            if (draftCirculationID > 0)
                            {
                                try
                                {
                                    var minutesDetails = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                                          where row.ID == draftCirculationID && row.IsDeleted == false && row.ReadOn == null
                                                          select row).FirstOrDefault();
                                    if (minutesDetails != null)
                                    {
                                        minutesDetails.ReadOn = DateTime.Now;
                                        minutesDetails.UpdatedBy = UserId;
                                        minutesDetails.UpdatedOn = DateTime.Now;
                                        entities.SaveChanges();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }
                            #endregion
                        }

                        LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), result.FilePath);
                        if (!string.IsNullOrEmpty(result.FilePath))
                        {

                            string inputfilepath = (result.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();
                            LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), ApplicationPathSource);

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver);
                            if (File.Exists(filePath))
                            {

                                using (ZipFile ComplianceZip = new ZipFile())
                                {

                                    string destinationPath = string.Empty;

                                    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                                    LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), destinationPath);

                                    string Folder = "~/TempFiles";
                                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + FileData;

                                    string extension = System.IO.Path.GetExtension(result.FilePath);

                                    string DateFolderforconvert = (DateFolder).ToString();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                    string finalDateFolder = destinationPath + DateFolderforconvert;

                                    Directory.CreateDirectory(finalDateFolder);

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = UserId + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    //for server change
                                    string filenameConvertforfilepath = FileName;
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                    string destinationpathplaceoutput = destinationPath + @"\";

                                    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);

                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                                    bw.Close();


                                    file = finalfilepathforuploadfile;
                                }
                            }
                            else
                            {
                                file = "File not found";
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return file;
        }

        public static DocumentSettingVM GetDocumentSetting(int customerId, string documentType)
        {
            var result = new DocumentSettingVM();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result.PageSetting = (from row in entities.BM_SP_GetDocumentPageSetting(customerId, documentType)
                                          select new PageSettingVM
                                          {
                                              PageSize = row.PageSize,
                                              MarginTop = (float)row.MarginTop,
                                              MarginLeft = (float)row.MarginLeft,
                                              MarginRight = (float)row.MarginRight,
                                              MarginBottom = (float)row.MarginBottom,
                                          }).FirstOrDefault();

                    result.Styles = (from row in entities.BM_SP_GetDocumentFormatSetting(customerId, documentType)
                                     select new StyleVM
                                     {
                                         StyleName = row.StyleName,
                                         FontName = row.FontName,
                                         FontSize = row.FontSize,
                                         FontColor = row.FontColor,
                                         IsBold = (bool)row.IsBold,
                                         IsItalic = (bool)row.IsItalic,
                                         IsUnderline = (bool)row.IsUnderline
                                     }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        private static List<BM_FileData> GetFileData(int AgendaMappingId)
        {
            List<BM_FileData> _objFiledata = new List<BM_FileData>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objFiledata = (from x in entities.BM_FileData where x.AgendaMappingId == AgendaMappingId && x.IsDeleted == false select x).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objFiledata;
        }

        public static string fetchComppath(int scheduleOnID, int complianceInstanceID, int userID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;
            string version = "1.0";

            // List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
            List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

            ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(scheduleOnID)).ToList();
            ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == false).ToList();
            if (ComplianceDocument.Count < 0)
            {
                ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == true).ToList();
            }
            var documentVersionData = ComplianceDocument.GroupBy(entry => new { entry.Version, entry.FileName }).Select(entry => entry.FirstOrDefault()).ToList();
            //string version = documentVersionData.v
            //if (version.Equals("1.0"))
            //{
            //    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
            //    if (ComplianceFileData.Count <= 0)
            //    {
            //       ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
            //    }
            //}
            //else
            //{
            //    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
            //}

            //ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(scheduleOnID)).ToList();

            using (ZipFile ComplianceZip = new ZipFile())
            {

                var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(scheduleOnID));
                if (compliancetype == 1)
                {
                    var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(scheduleOnID));
                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                }
                else
                {
                    var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(scheduleOnID));
                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                }



                if (ComplianceDocument.Count > 0)
                {
                    int i = 0;
                    foreach (var file in ComplianceDocument)
                    {
                        string inputfilepath = (file.FilePath).ToString();
                        inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                        inputfilepath = inputfilepath.Replace(@"/", @"\");

                        string ApplicationPathSource = string.Empty;

                        ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                        string filepathserver = ApplicationPathSource + inputfilepath;

                        string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                        filePath = filePath.Replace(@"\\", @"\");

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string destinationPath = string.Empty;

                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);

                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = userID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            i++;

                            CompDocReviewPath = finalfilepathforuploadfile;
                        }
                    }
                }
            }
            return CompDocReviewPath;
        }

        public static string fetchpolicespath(int policesId, int UserId)
        {
            string CompDocReviewPath = string.Empty;
            try
            {

                List<BM_FileData> ComplianceFileData = new List<BM_FileData>();


                ComplianceFileData = GetplicesFileData(Convert.ToInt32(policesId)).ToList();


                using (ZipFile ComplianceZip = new ZipFile())
                {


                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = UserId + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return CompDocReviewPath;
        }

        private static List<BM_FileData> GetplicesFileData(int policesId)
        {
            List<BM_FileData> _objFiledata = new List<BM_FileData>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objFiledata = (from x in entities.BM_FileData where x.ReportPolicesId == policesId && x.IsDeleted == false select x).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objFiledata;
        }
        private static List<BM_FileData> GetTaskFileData(long fileId)
        {
            List<BM_FileData> _objFiledata = new List<BM_FileData>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objFiledata = (from x in entities.BM_FileData where x.Id == fileId && x.IsDeleted == false select x).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objFiledata;
        }
        public static string fetchTaskpath(int fileId, int UserId)
        {
            string CompDocReviewPath = string.Empty;
            try
            {

                List<BM_FileData> TaskFileData = new List<BM_FileData>();

                TaskFileData = GetTaskFileData(Convert.ToInt32(fileId)).ToList();


                using (ZipFile ComplianceZip = new ZipFile())
                {


                    if (TaskFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in TaskFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = UserId + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return CompDocReviewPath;
        }

        public static List<FileDataVM> fetchpathForMinutespreview(int meetingId, int UserID)
        {

            string _strMinutesPreview = string.Empty;
            List<FileDataVM> _objfile = new List<FileDataVM>();

            try
            {
               
                string eventName = "FinalizedMinutes";
                var scheduleOnID = GetScheduleOnIDForMeetingEvent(meetingId, eventName);
                _objfile = BindFileDataList(scheduleOnID);

                using (ZipFile ComplianceZip = new ZipFile())
                {


                    if (_objfile.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in _objfile)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss")) + i;

                                string User = UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                _strMinutesPreview = finalfilepathforuploadfile;
                                file.FilePath = finalfilepathforuploadfile;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objfile;
        }

        private static List<FileDataVM> BindFileDataList(long scheduleOnID)
        {
            var result = FileDataListByScheduleOnID(scheduleOnID);
            return result;
        }

        public static List<FileDataVM> FileDataListByScheduleOnID(long schdeuleOnId)
        {

            var result = new List<FileDataVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from mapping in entities.FileDataMappings
                              join file in entities.FileDatas on mapping.FileID equals file.ID
                              where mapping.ScheduledOnID == schdeuleOnId && file.IsDeleted == false
                              select new FileDataVM
                              {
                                  FileID = file.ID,
                                  FileName = file.Name,
                                  Version = file.Version,
                                  FilePath = file.FilePath,
                                  FileKey = file.FileKey
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        private static long GetScheduleOnIDForMeetingEvent(int meetingId, string autoCompleteOn)
        {
            long result = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from scheduleOn in entities.BM_ComplianceScheduleOnNew
                              join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                              where scheduleOn.MeetingID == meetingId && scheduleOn.MappingType == "M"
                                      && mapping.AutoCompleteOn == autoCompleteOn
                                      && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                              select scheduleOn.ScheduleOnID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public static PreviewAgendaVM Preview(long MeetingId, int CustomerId, bool GenerateMinutes, long? taskAssignmentId)
        {
            var result = new PreviewAgendaVM();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                result = (from row in entities.BM_Meetings
                          join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                          join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                          join fy in entities.BM_YearMaster on row.FY equals fy.FYID
                          from city in entities.Cities.Where(k => k.ID == entity.Regi_CityId).Select(k => k.Name).DefaultIfEmpty()
                          from state in entities.States.Where(k => k.ID == entity.Regi_StateId).Select(k => k.Name).DefaultIfEmpty()
                          where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                          select new PreviewAgendaVM
                          {
                              MeetingID = row.MeetingID,
                              IsVirtual = row.IsVirtualMeeting,
                              IsEVoting = row.IsEVoting,
                              GenerateMinutes = GenerateMinutes,
                              MeetingSrNo = row.MeetingSrNo,
                              MeetingCircular = row.MeetingCircular,
                              MeetingTypeId = row.MeetingTypeId,
                              MeetingTypeName = meeting.MeetingTypeName,
                              Quarter_ = row.Quarter_,
                              MeetingTitle = row.MeetingTitle,
                              NoticeDate = row.SendDateNotice,
                              CircularDate = row.CircularDate,
                              MeetingDate = row.MeetingDate,
                              MeetingTime = row.MeetingTime,
                              MeetingAddressType = row.MeetingVenueType,
                              MeetingVenue = row.MeetingVenue,

                              MeetingStartDate = row.StartMeetingDate,
                              MeetingStartTime = row.StartTime,
                              MeetingEndDate = row.EndMeetingDate,
                              MeetingEndTime = row.EndMeetingTime,

                              GM_Notes = row.Notes,
                              TotalMemberPresentInAGM = row.TotalMemberPresentInAGM,
                              TotalProxyPresentInAGM = row.TotalProxyPresentInAGM,

                              IsAdjourned = row.IsAdjourned,

                              Entityt_Id = row.EntityId,
                              EntityCIN_LLPIN = entity.CIN_LLPIN,
                              EntityName = entity.CompanyName,
                              EntityAddressLine1 = entity.Regi_Address_Line1,
                              EntityAddressLine2 = entity.Regi_Address_Line2,
                              EntityCity = city == null ? "" : city,
                              EntityState = state == null ? "" : state,
                              FYText = fy.FYText
                          }).FirstOrDefault();

                if (result != null)
                {
                    result.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, null, GenerateMinutes, taskAssignmentId)
                                             select new MeetingAgendaMappingVM
                                             {
                                                 SrNo = (int)TemplateList.SrNo,
                                                 MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                 Meeting_Id = TemplateList.MeetingID,
                                                 AgendaID = TemplateList.BM_AgendaMasterId,
                                                 PartId = TemplateList.PartID,
                                                 AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                 AgendaItemText = TemplateList.AgendaItemText,

                                                 AgendaFormat = TemplateList.AgendaFormat,
                                                 ExplanatoryStatement = TemplateList.ExplanatoryStatement,
                                                 IsOrdinaryBusiness = TemplateList.IsOrdinaryBusiness,
                                                 IsSpecialResolution = TemplateList.IsSpecialResolution
                                             }).ToList();

                    if (GenerateMinutes)
                    {
                        var minutesDetails = (from row in entities.BM_MeetingMinutesDetails
                                              where row.MeetingId == MeetingId && row.IsDeleted == false
                                              select new
                                              {
                                                  row.TotalMemberPresentInAGM,
                                                  row.TotalProxyPresentInAGM,
                                                  row.TotalMemberInAGM,

                                                  //row.TotalValidProxy,
                                                  //row.TotalMemberProxy,
                                                  //row.TotalShareHeldByProxy,
                                                  //row.FaceValueOfEnquityShare,
                                                  //row.PercentageOfIssuedCapital
                                              }).FirstOrDefault();

                        if (minutesDetails != null)
                        {
                            result.TotalMemberPresentInAGM = minutesDetails.TotalMemberPresentInAGM;
                            result.TotalProxyPresentInAGM = minutesDetails.TotalProxyPresentInAGM;
                            result.TotalMemberInAGM = minutesDetails.TotalMemberInAGM;

                            //result.TotalValidProxy = minutesDetails.TotalValidProxy;
                            //result.TotalMemberProxy = minutesDetails.TotalMemberProxy;
                            //result.TotalShareHeldByProxy = minutesDetails.TotalShareHeldByProxy;
                            //result.FaceValueOfEnquityShare = minutesDetails.FaceValueOfEnquityShare;
                            //result.PercentageOfIssuedCapital = minutesDetails.PercentageOfIssuedCapital;
                        }

                        if (result.MeetingTypeId == 11)
                        {
                            var hasConsolidatedFinancial = (from row in entities.BM_Meetings
                                                            join mapping in entities.BM_MeetingAgendaMapping on row.MeetingID equals mapping.MeetingID
                                                            join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                                            where row.MeetingID == MeetingId && mapping.IsDeleted == false && mapping.IsActive == true &&
                                                            agenda.DefaultAgendaFor == "CF"
                                                            select row.MeetingID).Any();
                            result.HasConsolidatedFinancial = hasConsolidatedFinancial;
                        }
                    }
                    else if (result.MeetingTypeId == 11 || result.MeetingTypeId == 12)
                    {
                        result.MeetingSigningAuthorityForAGM = (from row in entities.BM_SP_MeetingSigningAuthorityForAGM(result.MeetingID, result.Entityt_Id)
                                                                select new MeetingSigningAuthorityForAGM_VM
                                                                {
                                                                    AuthorityName = row.AuthorityName,
                                                                    Designation = row.Designation,
                                                                    DIN_PAN = row.DIN_PAN,
                                                                    MembershipNo = row.MembershipNo,
                                                                    IsCS = row.IsCS,
                                                                    IsChairman = row.IsChairman,
                                                                    IsAuthorisedSignotory = row.IsAuthorisedSignotory
                                                                }).FirstOrDefault();
                    }
                }
            }
            return result;
        }


        private static string GetSuffix(int day)
        {
            string suffix = "th";

            if (day < 11 || day > 20)
            {
                day = day % 10;
                switch (day)
                {
                    case 1:
                        suffix = "st";
                        break;
                    case 2:
                        suffix = "nd";
                        break;
                    case 3:
                        suffix = "rd";
                        break;
                }
            }

            return suffix;
        }

        public static List<AgendaDocumentVM> GetAgendaDocument(long agendaMappingID, int customerId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getAgendaDocument = (from row in entities.BM_FileData
                                             where row.AgendaMappingId == agendaMappingID
                                             && row.IsDeleted == false
                                             orderby row.FileSrNo
                                             select new AgendaDocumentVM
                                             {
                                                 ID = row.Id,
                                                 FileName = row.FileName
                                             }
                                   ).ToList();
                    return getAgendaDocument;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetSecretarialFile(long fileID, int userId, int customerId)
        {

            var actualFileName = "";
            var obj = new FileDataVM();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var result = (from row in entities.BM_FileData
                                  where row.Id == fileID
                                  select new FileDataVM
                                  {
                                      FileName = row.FileName,
                                      FileKey = row.FileKey,
                                      FilePath = row.FilePath,
                                      Version = row.Version
                                  }).FirstOrDefault();

                    if (result != null)
                    {
                        //string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                        //string fileExtension = Path.GetExtension(result.FileName);
                        //string fileName = result.FileKey + fileExtension;

                        //string filePath = Path.Combine(path, fileName);

                        //for to get the path from web config 17Jun2021 by Nafees
                        string inputfilepath = (result.FilePath).ToString();
                        inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                        inputfilepath = inputfilepath.Replace(@"/", @"\");

                        string ApplicationPathSource = string.Empty;

                        ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                        string filepathserver = ApplicationPathSource + inputfilepath;

                        string filePath = Path.Combine(filepathserver, result.FileKey + Path.GetExtension(result.FileName));

                        filePath = filePath.Replace(@"\\", @"\");
                        
                        //


                        if (File.Exists(filePath))
                        {
                            // added by Nafees
                            string destinationPath = string.Empty;
                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), destinationPath);

                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(result.FilePath);
                            string DateFolderforconvert = (DateFolder).ToString();

                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userId)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = userId + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath + Path.GetExtension(result.FileName);
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);

                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                            bw.Close();

                            actualFileName = finalfilepathforuploadfile;

                            // end of change//

                            //string Folder = "~/TempFiles";
                            //string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            //string DateFolder = Folder + "/" + File;
                            //string extension = System.IO.Path.GetExtension(filePath);
                            //Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                            //if (!Directory.Exists(DateFolder))
                            //{
                            //    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                            //}

                            //string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            //string User = userId + "" + customerId + "" + FileDate;
                            //string FileName = DateFolder + "/" + User + "" + extension;
                            //FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            //BinaryWriter bw = new BinaryWriter(fs);

                            //bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //bw.Close();

                            //actualFileName = FileName.Substring(2, FileName.Length - 2);
                        }
                        else
                        {
                            actualFileName = "File not found";
                        }
                    }
                    else
                    {
                        actualFileName = "File not found";
                    }
                }
                catch (Exception ex)
                {

                    LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }

            return actualFileName;
        }

        private static Syncfusion.DocIO.DLS.Entity IncreaseSpacingsInTOC(TableOfContent toc, float beforeSpacing, float afterSpacing)
        {
            int tocIndex = toc.OwnerParagraph.Items.IndexOf(toc);
            //TOC may contains nested fields and each fields has its owner field end mark 
            //so to indentify the TOC Field end mark (WFieldMark instance) used the stack.
            Stack<Syncfusion.DocIO.DLS.Entity> fieldStack = new Stack<Syncfusion.DocIO.DLS.Entity>();
            fieldStack.Push(toc);

            //Finds whether TOC end item is exist in same paragraph.
            for (int i = tocIndex + 1; i < toc.OwnerParagraph.Items.Count; i++)
            {
                Syncfusion.DocIO.DLS.Entity item = toc.OwnerParagraph.Items[i];

                if (item is WField)
                    fieldStack.Push(item);
                else if (item is WFieldMark && (item as WFieldMark).Type == FieldMarkType.FieldEnd)
                {
                    if (fieldStack.Count == 1)
                    {
                        fieldStack.Clear();
                        return item;
                    }
                    else
                        fieldStack.Pop();
                }

            }

            return FindLastItemInTextBody(toc, fieldStack, beforeSpacing, afterSpacing);
        }
        private static Syncfusion.DocIO.DLS.Entity FindLastItemInTextBody(TableOfContent toc, Stack<Syncfusion.DocIO.DLS.Entity> fieldStack, float beforeSpacing, float afterSpacing)
        {
            WTextBody tBody = toc.OwnerParagraph.OwnerTextBody;

            for (int i = tBody.ChildEntities.IndexOf(toc.OwnerParagraph) + 1; i < tBody.ChildEntities.Count; i++)
            {
                WParagraph paragraph = tBody.ChildEntities[i] as WParagraph;

                //Set after spacings for paragraphs inside TOC
                paragraph.ParagraphFormat.AfterSpacing = afterSpacing;
                //Set before spacings for paragraphs inside TOC
                paragraph.ParagraphFormat.BeforeSpacing = beforeSpacing;

                foreach (Syncfusion.DocIO.DLS.Entity item in paragraph.Items)
                {
                    if (item is WField)
                        fieldStack.Push(item);
                    else if (item is WFieldMark && (item as WFieldMark).Type == FieldMarkType.FieldEnd)
                    {
                        if (fieldStack.Count == 1)
                        {
                            fieldStack.Clear();
                            return item;
                        }
                        else
                            fieldStack.Pop();
                    }
                }
            }
            return null;
        }

        //Get List of minutes of Concluded Meetings 24Jun 2021
        public static List<FileDataVM> MinutesofConcludedmeeting(int meetingId, int UserID)
        {

            List<FileDataVM> _objfile = new List<FileDataVM>(); 
            try
            {
                
                string eventName = "FinalizedMinutes";
                var scheduleOnID = GetScheduleOnIDForMeetingEvent(meetingId, eventName);
                _objfile = BindFileDataList(scheduleOnID);

                
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objfile;
        }
        //

        public static bool IsDraftMinutesApproved(long draftCirculationID)
        {
            var result = false;
            try
            {
                //result = (from row in entities.BM_MeetingMinutesDetailsTransaction
                //          where row.ID == draftCirculationID && row.ApprovedOn != null
                //          select row).Any();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var details = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                   join participant in entities.BM_MeetingParticipant on row.MeetingParticipantId equals participant.MeetingParticipantId
                                   where row.ID == draftCirculationID && row.IsDeleted == false && participant.IsDeleted == false
                                   select new
                                   {
                                       row.ApprovedOn,
                                       participant.ParticipantType
                                   }).FirstOrDefault();
                    if (details != null)
                    {
                        if (details.ApprovedOn == null && (details.ParticipantType == "D" || details.ParticipantType =="M"))
                        {
                            result = false;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = true;
                    }

                }
                    
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        // aded for preview of minutes 18 aug 2021
        public static string previewMinutesfetchpath(int MeetingId, int UserId)
        {
            string CompDocReviewPath = string.Empty;
            try
            {

                List<FileDataVM> ComplianceFileData = new List<FileDataVM>();


                ComplianceFileData = GetFileDataPreviewMinutes(Convert.ToInt32(MeetingId)).ToList();


                using (ZipFile ComplianceZip = new ZipFile())
                {


                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(UserId)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = UserId + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return CompDocReviewPath;
        }
        //

        // preview minute docs
        private static List<FileDataVM> GetFileDataPreviewMinutes(int MeetingId)
        {
            List<FileDataVM> _objFiledata = new List<FileDataVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    // _objFiledata = (from x in entities.BM_FileData where x.AgendaMappingId == AgendaMappingId && x.IsDeleted == false select x).ToList();
                   

                    var scheduleOnID = (from scheduleOn in entities.BM_ComplianceScheduleOnNew
                                        join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                                        where scheduleOn.MeetingID == MeetingId && scheduleOn.MappingType == "M"
                                                && mapping.AutoCompleteOn == "FinalizedMinutes"
                                                && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                                        select scheduleOn.ScheduleOnID).FirstOrDefault();

                    _objFiledata = (from mapping in entities.FileDataMappings
                                    join file in entities.FileDatas on mapping.FileID equals file.ID
                                    where mapping.ScheduledOnID == scheduleOnID && file.IsDeleted == false
                                    select new FileDataVM {
                                        FileID = file.ID,
                                        FileName = file.Name,
                                        Version = file.Version
                                    }).ToList();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objFiledata;
        }
        //
    }
}