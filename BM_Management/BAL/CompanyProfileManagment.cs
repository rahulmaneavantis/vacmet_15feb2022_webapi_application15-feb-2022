﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.BM_Management.DAL;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using System.Reflection;
using AppWebApplication.Data;
using System.Globalization;
using System.Configuration;
using System.IO;

namespace AppWebApplication.BM_Management.BAL
{
    public class CompanyProfileManagment
    {
        public static List<EntityDetailVM> GetEntityDetails(int? customerID, int UserId, int entityID)
        {
            List<EntityDetailVM> _objentityDetails = new List<EntityDetailVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime date;
                    _objentityDetails = (from row in entities.BM_EntityMaster
                                         join rows in entities.BM_EntityType
                                         on row.Entity_Type equals (rows.Id)
                                         where row.Is_Deleted == false && row.Customer_Id == customerID && row.Id == entityID
                                         orderby row.CompanyName ascending
                                         select new EntityDetailVM
                                         {
                                             Id = row.Id,
                                             EntityName = row.CompanyName,
                                             Type = rows.EntityName,
                                             LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                             RegistrationNO = row.Registration_No,
                                             CIN = row.CIN_LLPIN,
                                             Entity_Type = row.Entity_Type,
                                             dateofIncorporaion = row.IncorporationDate,
                                             PAN = row.PAN,
                                             Islisted = row.IS_Listed,
                                             RocCode = (from r in entities.BM_ROC_code where r.Id == row.ROC_Code select r.Name).FirstOrDefault(),
                                             CompanyCategory = (from c in entities.BM_CompanyCategory where c.Id == row.CompanyCategory_Id select c.Category).FirstOrDefault(),
                                             CompanysubCategory = (from cs in entities.BM_CompanySubCategory where cs.Id == row.CompanySubCategory select cs.SubCategoryName).FirstOrDefault(),
                                             classofCompany = rows.EntityName,
                                             RegisterAddress = row.Regi_Address_Line1 + " " + row.Regi_Address_Line2,
                                             emailId = row.Email_Id
                                         }).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentityDetails;
        }

        public static List<DirectorVM> GetDirectorsList(int? customerID, int userID, int entityID, int v2)
        {
            List<DirectorVM> _objDirectorList = new List<DirectorVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //_objDirectorList = (from director in entities.BM_DirectorMaster
                    //                    join row in entities.BM_Directors_DetailsOfInterest on director.Id equals row.Director_Id
                    //                    join nature in entities.BM_Directors_NatureOfInterest
                    //                    on row.NatureOfInterest equals nature.Id
                    //                    from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
                    //                    where row.EntityId == entityID && director.Customer_Id == customerID
                    //                    && row.IsActive == true && row.IsDeleted == false

                    //                    //&& (nature.Id == 2 || nature.Id == 9 || nature.Id == 10)
                    //                    && (nature.IsDirector == true || nature.IsMNGT == true)
                    //                    && director.Is_Deleted == false && row.IsDeleted == false
                    //                    select new DirectorVM
                    //                    {

                    //                        Entity_Id = entityID,
                    //                        Director_ID = row.Director_Id,
                    //                        UserID = director.UserID,
                    //                        DIN = director.DIN,
                    //                        Salutation = director.Salutation,
                    //                        FirstName = director.FirstName,
                    //                        LastName = director.LastName,
                    //                        FullName = director.FirstName + "  " + director.MiddleName + "  " + director.LastName,
                    //                        itemlist = 0,
                    //                        Designation = ifDir.Name,
                    //                        DesignationId = ifDir.Id,
                    //                        PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
                    //                        ImagePath = director.Photo_Doc,
                    //                        EmailId = director.EmailId_Official,
                    //                        PhoneNumber = director.MobileNo,
                    //                        DSC_ExpiryDate = (DateTime)director.DSC_ExpiryDate,
                    //                        //DateofAppointment = row.DateOfAppointment,
                    //                    }).Distinct().ToList();

                    _objDirectorList = (from director in entities.BM_SP_GetManagementEntityWise(entityID, customerID)
                                        orderby director.FullName
                                        select new DirectorVM
                                        {
                                            interestType = director.InterestType,
                                            Entity_Id = entityID,
                                            Director_ID = director.Director_Id,
                                            UserID = director.UserID,
                                            DIN = director.DIN,
                                            Salutation = director.Salutation,
                                            FirstName = director.FirstName,
                                            LastName = director.LastName,
                                            FullName = director.FullName,
                                            itemlist = 0,
                                            Designation = director.Designation,
                                            DesignationId = director.DesignationId,
                                            //PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
                                            ImagePath = director.ImagePath,
                                            DSC_ExpiryDate = director.DSC_ExpiryDate,
                                            DateofAppointment = director.DateOfAppointment,
                                            DesignationName = director.DesignationName
                                        }).ToList();

                    foreach (var item in _objDirectorList)
                    {

                        if (item.UserID == userID)
                        {

                            item.itemlist = 1;
                            item.FirstName = item.FirstName + "" + "(me)";
                        }
                        else
                        {
                            item.itemlist = 0;

                        }

                        // FOR TO GET THE Image Path of Director - 21 oct 2021

                        var FilePath = string.IsNullOrEmpty(item.ImagePath) ? "" : ("/Areas/BM_Management/Documents/Director/" + item.Director_ID + "/" + item.ImagePath);

                        if (!string.IsNullOrEmpty(FilePath))
                        {

                            string inputfilepath = (FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();
                            LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), ApplicationPathSource);

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver);
                            if (File.Exists(filePath))
                            {

                                    string destinationPath = string.Empty;

                                    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                                    LoggerMessage_AVASEC.InsertPathlog(MethodBase.GetCurrentMethod().DeclaringType.ToString(), destinationPath);

                                    string Folder = "~/TempFiles";
                                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + FileData;

                                    string extension = System.IO.Path.GetExtension(FilePath);

                                    string DateFolderforconvert = (DateFolder).ToString();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                    string finalDateFolder = destinationPath + DateFolderforconvert;

                                    Directory.CreateDirectory(finalDateFolder);

                                    string custID = Convert.ToString(customerID??0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = userID + "" + custID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    //for server change
                                    string filenameConvertforfilepath = FileName;
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                    string destinationpathplaceoutput = destinationPath + @"\";

                                    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);

                                using (FileStream fs1 = File.OpenRead(filePath))
                                {
                                    int length = (int)fs1.Length;
                                    byte[] buffer;

                                    using (BinaryReader br = new BinaryReader(fs1))
                                    {
                                        buffer = br.ReadBytes(length);
                                    }
                                    bw.Write(buffer);
                                }                               

                                    bw.Close();


                                   var file = finalfilepathforuploadfile;

                                if (!string.IsNullOrEmpty(file))
                                {

                                    string pathforandriodoutput = file;
                                    pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                    pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                    string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                    finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                    item.ImagePath = finaloutputforandriodpath;
                                }
                                else
                                {
                                    item.ImagePath = null;
                                }

                            }
                            else
                            {
                                item.ImagePath = null;
                            }
                        }


                        //end 

                    }


                    _objDirectorList = _objDirectorList.OrderByDescending(s => s.itemlist).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorList;
        }

        public static List<DirectorDetailsVM> GetDirectorProfile(int? customerID, int userId)
        {
            List<DirectorDetailsVM> _objDirectorDetails = new List<DirectorDetailsVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDirectorDetails = (from dir in entities.BM_SP_DirectorProfileDetails(userId)

                                           select new DirectorDetailsVM
                                           {
                                               #region Assign Values
                                               ID = dir.Id,
                                               DirectorId = dir.Id,
                                               Salutation = dir.Salutation,

                                               DIN = dir.DIN,
                                               FirstName = dir.FirstName,
                                               MiddleName = dir.MiddleName,
                                               LastName = dir.LastName,
                                               FullName = dir.Salutation + " " + dir.FirstName + " " + (dir.MiddleName == "" ? "" : dir.MiddleName) + " " + (dir.LastName == null ? "" : dir.LastName),
                                               DateOfBirth = dir.DOB,
                                               MobileNo = dir.MobileNo,
                                               EmailId = dir.EmailId_Personal,
                                               EmailId_Official = dir.EmailId_Official,
                                               Photo_Doc_Name = dir.Photo_Doc,

                                               #endregion
                                           }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorDetails;
        }


        //For Director Info 14 Jun 2021
        public static List<DirectorDetailsVM> GetDirectorInfo(int? customerID,int directorId)
        {
            List<DirectorDetailsVM> _objDirectorDetails = new List<DirectorDetailsVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDirectorDetails = (from dir in entities.BM_SP_DirectorInfo(customerID,directorId)

                                           select new DirectorDetailsVM
                                           {
                                               #region Assign Values
                                               ID = dir.Id,
                                               DirectorId = dir.Id,
                                               Salutation = dir.Salutation,

                                               DIN = dir.DIN,
                                               FirstName = dir.FirstName,
                                               MiddleName = dir.MiddleName,
                                               LastName = dir.LastName,
                                               FullName = dir.Salutation + " " + dir.FirstName + " " + (dir.MiddleName == "" ? "" : dir.MiddleName) + " " + (dir.LastName == null ? "" : dir.LastName),
                                               DateOfBirth = dir.DOB,
                                               MobileNo = dir.MobileNo,
                                               EmailId = dir.EmailId_Personal,
                                               EmailId_Official = dir.EmailId_Official,
                                               Photo_Doc_Name = dir.Photo_Doc,
                                               ProfileDetails=dir.ProfileDetails,

                                               #endregion
                                           }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorDetails;
        }
        //End 

        public static List<DirectorDetailsVM> GetCompanyDirectorDetails(int? customerID, int DirectorId)
        {
            List<DirectorDetailsVM> _objDirectorDetail = new List<DirectorDetailsVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDirectorDetail = (from row in entities.BM_DirectorMaster

                                          where row.Id == DirectorId && row.Customer_Id == customerID && row.Is_Deleted == false

                                          select new DirectorDetailsVM
                                          {
                                              #region Assign Values
                                              ID = row.Id,
                                              Salutation = row.Salutation,
                                              DIN = row.DIN,
                                              FirstName = row.FirstName,
                                              MiddleName = row.MiddleName,
                                              LastName = row.LastName,
                                              DateOfBirth = row.DOB,
                                              Gender = row.Gender,
                                              MobileNo = row.MobileNo,
                                              EmailId_Official = row.EmailId_Official,
                                              Photo_Doc_Name = row.Photo_Doc,
                                              PAN_Doc_Name = row.PAN_Doc,
                                              #endregion
                                          }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorDetail;
        }



        public static List<DetailsOfInterestVM> GetDetailsofIntrest(int? customerID, int DirectorId)
        {
            List<DetailsOfInterestVM> _objDetailsofIntrest = new List<DetailsOfInterestVM>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //_objDetailsofIntrest = (from row in entities.BM_Directors_DetailsOfInterest
                    //                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                    //                        join rows in entities.BM_EntityType on entity.Entity_Type equals rows.Id
                    //                        //join rows in entities.BM_EntityType on row.Entity_Type equals rows.Id
                    //                        join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                    //                        //from entity in entities.BM_EntityMaster.Where(k => k.Id == row.EntityId).DefaultIfEmpty()
                    //                        from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
                    //                        from ifRelative in entities.BM_Directors_Relatives.Where(k => k.Id == row.IfInterestThroughRelatives && k.Director_Id == row.Director_Id && k.IsActive == true && k.IsDeleted == false).DefaultIfEmpty()
                    //                        from designation in entities.BM_DesignationMaster.Where(k => k.ID == row.DirectorDesignationId).DefaultIfEmpty()
                    //                        where row.Director_Id == DirectorId && row.IsDeleted == false
                    //                        //Commented on 25 Sep 2020 due to display resigned director till take note on agenda in board.
                    //                        //&& row.IsActive == true
                    //                        orderby row.NatureOfInterest == 7 ? 1 : 0, row.EntityId == -1 ? row.NameOfOther : entity.CompanyName
                    //                        select new DetailsOfInterestVM
                    //                        {
                    //                            Director_ID = row.Director_Id,
                    //                            Id = row.Id,
                    //                            EntityName = row.EntityId == -1 ? row.NameOfOther : entity.CompanyName,
                    //                            EntityType = rows.EntityName,
                    //                            CIN = row.EntityId == -1 ? row.CIN : entity.CIN_LLPIN,
                    //                            PAN = row.EntityId == -1 ? row.PAN : entity.PAN,
                    //                            NatureofIntrest = nature.Name,

                    //                            Entity_Id = row.EntityId,
                    //                            TypeOfEntity = entity.Entity_Type,
                    //                            NatureOfInterest = row.NatureOfInterest,
                    //                            IfDirector = row.IfDirector,
                    //                            IfInterestThroughRelatives = row.IfInterestThroughRelatives,
                    //                            PercentagesOfShareHolding = row.PercentagesOfShareHolding,
                    //                            Arosed = row.ArosedOrChangedDate,
                    //                            BoardofResolution = row.DateOfResolution,
                    //                            DateofAppointment = row.DateOfAppointment,
                    //                            IfDirectorName = ifDir.Name,
                    //                            IfInterestThroughRelativesName = ifRelative.Name

                    //                        }).ToList();

                     _objDetailsofIntrest = (from row in entities.BM_Directors_DetailsOfInterest
                                  join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                  join rows in entities.BM_EntityType on entity.Entity_Type equals rows.Id
                                  //join rows in entities.BM_EntityType on row.Entity_Type equals rows.Id
                                  join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                                  //from entity in entities.BM_EntityMaster.Where(k => k.Id == row.EntityId).DefaultIfEmpty()
                                  from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
                                  from ifRelative in entities.BM_Directors_Relatives.Where(k => k.Id == row.IfInterestThroughRelatives && k.Director_Id == row.Director_Id && k.IsActive == true && k.IsDeleted == false).DefaultIfEmpty()
                                  from designation in entities.BM_DesignationMaster.Where(k => k.ID == row.DirectorDesignationId).DefaultIfEmpty()
                                  where row.Director_Id == DirectorId && row.IsDeleted == false
                                  //Commented on 25 Sep 2020 due to display resigned director till take note on agenda in board.
                                  //&& row.IsActive == true
                                  orderby row.NatureOfInterest == 7 ? 1 : 0, row.EntityId == -1 ? row.NameOfOther : entity.CompanyName
                                             select new DetailsOfInterestVM
                                             {
                                                 Director_ID = row.Director_Id,
                                                 Id = row.Id,
                                                 EntityName = row.EntityId == -1 ? row.NameOfOther : entity.CompanyName,
                                                 EntityType = rows.EntityName,
                                                 CIN = row.EntityId == -1 ? row.CIN : entity.CIN_LLPIN,
                                                 PAN = row.EntityId == -1 ? row.PAN : entity.PAN,
                                                 NatureofIntrest = nature.Name,

                                                 Entity_Id = row.EntityId,
                                                 TypeOfEntity = entity.Entity_Type,
                                                 NatureOfInterest = row.NatureOfInterest,
                                                 IfDirector = row.IfDirector,
                                                 IfInterestThroughRelatives = row.IfInterestThroughRelatives,
                                                 PercentagesOfShareHolding = row.PercentagesOfShareHolding,
                                                 Arosed = row.ArosedOrChangedDate,
                                                 BoardofResolution = row.DateOfResolution,
                                                 DateofAppointment = row.DateOfAppointment,
                                                 IfDirectorName = ifDir.Name,
                                                 IfInterestThroughRelativesName = ifRelative.Name,
                                                 DirectorDesignationName = designation.DesignationName
                                             }).ToList();

                    foreach (var item in _objDetailsofIntrest)
                    {
                        var data = (from row in entities.BM_Directors_Relatives
                                    join details in entities.BM_Directors_DetailsOfInterest_RelativeMapping on row.Id equals details.RelativeId
                                    where details.DetailsOfInterestID == item.Id && details.IsDeleted == false
                                    select new RelativesVM
                                    {
                                        Id = row.Id,
                                        Director_ID = row.Director_Id,
                                        Name = row.Name,
                                        Minor_or_Adult = row.IsMinor,
                                        MaritalStatus = row.IsMarried,
                                    }).ToList();
                        item.Relatives = data;
                        var relativenames = "";
                        foreach (var s in data)
                        {
                            relativenames += s.Name + ",";
                        }
                        item.IfInterestThroughRelativesName = relativenames.TrimEnd(',');
                    }
                }

                //}
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objDetailsofIntrest;
        }



        public static List<CommitteeListVM> GetCommiteeList(int? customerID, int userId, int entityID)
        {
            List<CommitteeListVM> objcommiteelist = new List<CommitteeListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {


                    objcommiteelist = (from row in entities.BM_Directors_DetailsOfCommiteePosition
                                       join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                                       join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                                       where row_e.Customer_Id == customerID
                                       && row.EntityId == entityID
                                       && row_e.Is_Deleted == false
                                       && row.IsDeleted == false
                                       orderby committee.Name
                                       select new CommitteeListVM
                                       {
                                           Id = committee.Id,
                                           Name = committee.Name,
                                           Description_ = committee.Description_
                                       }).Distinct().ToList();


                    if (objcommiteelist.Count > 0)
                    {
                        foreach (var item in objcommiteelist)
                        {
                            var getCommiteeList = GetCommitteeDetailsList(customerID, userId, entityID, item.Id);
                            if (getCommiteeList.Count > 0)
                            {
                                item.CommiteeDetailsVM = getCommiteeList;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objcommiteelist;
        }

        public static List<CommitteeDetailList> GetCommitteeDetailsList(int? customerID, int userId, int entityID, int CommitteeId)
        {
            List<CommitteeDetailList> _objcommiteelist = new List<CommitteeDetailList>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _objcommiteelist = (from row in entities.BM_SP_GetCommitteeOrCompositionEntitywise(entityID, customerID, CommitteeId)
                                        select new CommitteeDetailList
                                        {
                                            Id = row.Id,
                                            Entity_Id = row.EntityId,
                                            EntityName = row.CompanyName,
                                            Director_Id = row.Director_Id,
                                            DirectorName = row.FullName,
                                            //DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + "  " + director.FirstName + " " + (string.IsNullOrEmpty(director.MiddleName) ? "" : director.MiddleName) + " " + director.LastName,
                                            ImagePath = row.Photo_Doc,
                                            Designation_Id = row.Designation_Id,
                                            Committee_Id = row.Committee_Id,
                                            CommitteeName = row.Name,
                                            Committee_Type = row.Committee_Type,
                                            Committee_Type_Name = row.Name,
                                            FirstName = row.FirstName,
                                            LastName = row.LastName,
                                            DIN = row.DIN,
                                            EmailId = row.EmailId_Official,
                                            PhoneNumber = row.MobileNo,
                                        }).Distinct().ToList();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objcommiteelist;
        }

        public static List<PolicesVM> GetCompanyPolices(int? customerID, int userID, int entityID)
        {

            List<PolicesVM> _objpoloces = new List<PolicesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objpoloces = (from row in entities.BM_ReportPolices
                                   where row.EntityId == entityID && row.CustomerId == customerID && row.IsActive == true
                                   select new PolicesVM
                                   {
                                       Id = row.Id,
                                       PolicesName = row.PolicesName,
                                       EntityId = row.EntityId
                                   }
                                   ).ToList();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objpoloces;
        }

        //Added By NK 
        public static List<CompanyPolicydetailsVM> GetCompanyPolicesDetails(int? customerID, int userID, int entityID)
        {

            List<CompanyPolicydetailsVM> _objpoloces = new List<CompanyPolicydetailsVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objpoloces = (from row in entities.BM_SP_GetEntityDocuments(customerID, entityID)
                                    select new CompanyPolicydetailsVM
                                   {
                                       DocumentId = row.DocumentID,
                                       AlteredDoc = row.AlteredDoc,
                                       BMDate_LastRenew = row.BMDate_LastRenew,
                                       GMDate_UpcomingRenew = row.GMDate_UpcomingRenew,
                                       License_RegNumber = row.License_RegNumber,
                                       FileId = row.Id,
                                       EndTimeAGM = row.EndTimeAGM,
                                       StartTimeAGM = row.StartTimeAGM,
                                       FileName = row.FileName,
                                       FYID = row.FYID,
                                       FYText = row.FYText,
                                       TypeDesc = row.EntitydocType,
                                       DocType = row.DocType,
                                       Description = row.Description,
                                   }
                                   ).ToList();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objpoloces;
        }
        //End of change

        public static List<CompanyGroupVM> GetCompanyGropDetails(int? customerID, int v, int entityID)
        {
            List<CompanyGroupVM> _objcompanyGroupDetails = new List<CompanyGroupVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objcompanyGroupDetails = (from row in entities.BM_SubEntityMapping
                                               where row.EntityId == entityID && row.CustomerId == customerID
                                                && row.isActive == true
                                               select new CompanyGroupVM
                                               {
                                                   EntityID = row.EntityId,
                                                   SubcompanyId = row.Id,
                                                   subcompanyTypeID = row.CompanysubType,
                                                   SubCompanyName = row.NameofCompany,
                                                   PersentageofShareholding = row.PerofShareHeld,
                                                   subcompanyTypeName = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                                   CIN = row.CIN,
                                                   ParentCIN = (from e in entities.BM_EntityMaster where e.Id == entityID select e.CIN_LLPIN).FirstOrDefault(),
                                                   CustomerId = (int)customerID,
                                               }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objcompanyGroupDetails;
        }

        public static List<EntityVM> GetEntityDetails(int userId, int CustomerId)
        {
            List<EntityVM> objentity = new List<EntityVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objentity = (from row in entities.BM_SP_EntityListforBOD(userId, CustomerId)
                                 orderby row.CompanyName
                                 select new EntityVM
                                 {
                                     EntityId = row.ID,
                                     Entity = row.CompanyName,

                                 }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objentity;
        }

        public static List<SharesDetailVM> GetCompanysharesDetails(int? customerID, int v, int entityID)
        {
            List<SharesDetailVM> _objsharesDetails = new List<SharesDetailVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _objsharesDetails = (from row in entities.BM_CapitalMaster

                                         where row.Entity_Id == entityID
                                         select new SharesDetailVM
                                         {
                                             AuthorizedCapital = row.AuthorizedCapital,
                                             capitalId = row.Id,
                                             PaidupCapital = row.Pri_TotamtNoPaidupCapital + row.TotamtNoPaidupCapital,
                                             EquietyCapital = row.TotamtNoPaidupCapital,
                                             PrefrenceCapital = row.Pri_TotamtNoPaidupCapital,
                                             Othersecurities = row.Unclassified_AuhorisedCapital,
                                             EquietyNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "E" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
                                             PrefrenceNominalVal = (from x in entities.BM_Share where x.CapitalMasterId == row.Id && x.Sharetype == "P" select x.Nominalper_AuhorisedCapital).FirstOrDefault(),
                                         }).ToList();

                    if (_objsharesDetails.Count > 0)
                    {
                        foreach (var item in _objsharesDetails)
                        {
                            item.DebentureCapital = (from y in entities.BM_Debentures where y.CapitalId == item.capitalId select y.Total).FirstOrDefault();
                            item.DebencuteNominalVal = (from y in entities.BM_Debentures where y.CapitalId == item.capitalId select y.Nom_FullyConDebperUnit).FirstOrDefault();
                            item.AuthorizedCapitall = item.AuthorizedCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                            item.PaidupCapitall = item.PaidupCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                            item.EquietyCapitall = item.EquietyCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                            item.PrefrenceCapitall = item.PrefrenceCapital.ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                            if (item.DebentureCapital > 0)
                            {
                                item.Debenture = Convert.ToDecimal(item.DebentureCapital).ToString("0,0", CultureInfo.CreateSpecificCulture("hi-IN"));
                            }
                            else
                            {
                                item.Debenture = "";
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objsharesDetails;

        }

        public static List<MeetingVM> GetMeetingFilters(int UserId, int CustomerId)
        {
            List<MeetingVM> _objMeeting = new List<MeetingVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objMeeting = (from x in entities.BM_CommitteeComp
                                       //join y in entities.BM_Meetings
                                       //on x.Id equals y.MeetingTypeId
                                       // where 
                                       // x.Customer_Id == CustomerId && 
                                       //y.Customer_Id == CustomerId
                                   select new MeetingVM
                                   {
                                       MeetingId = x.Id,
                                       MeetingTitle = x.MeetingTypeName
                                   }
                                 ).ToList();
                    if (_objMeeting.Count > 0)
                    {
                        _objMeeting = _objMeeting.GroupBy(s => new { s.MeetingId, s.MeetingTitle })
                                .Select(g => new MeetingVM
                                {
                                    MeetingId = g.Key.MeetingId,
                                    MeetingTitle = g.Key.MeetingTitle,

                                }).ToList();
                    }
                    if (_objMeeting.Count > 0)
                    {
                        _objMeeting.Insert(0, new MeetingVM { MeetingId = -1, MeetingTitle = "All" });
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objMeeting;
        }

        //Added By NK 10Jun2021
        public static List<CommitteeMasterVM> GetCommitteeMasterdtls(int? customerID, int userID, int directorId)
        {

            List<CommitteeMasterVM> _objCommitteeMaster = new List<CommitteeMasterVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objCommitteeMaster = (from director in entities.BM_DirectorMaster
                                           join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                                           join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                                           join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                                           join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                                           join designation in entities.BM_DirectorPosition on row.Designation_Id equals designation.Id
                                           join committeeType in entities.BM_Directors_Designation on row.Committee_Type equals committeeType.Id
                                           where director.Customer_Id == customerID
                                           && director.Id == directorId
                                           && director.Is_Deleted == false
                                           && details.IsActive == true
                                           && row.IsDeleted == false
                                           orderby row.Designation_Id
                                           select new CommitteeMasterVM
                                           {
                                               Id = row.Id,
                                               Entity_Id = row.EntityId,
                                               EntityName = row_e.CompanyName.ToUpper(),
                                               Director_Id = row.Director_Id,
                                               DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) + (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName),
                                               Designation_Name = designation.Name,
                                               PositionId = row.Designation_Id,
                                               Committee_Id = row.Committee_Id,
                                               CommitteeName = committee.Name,
                                               Committee_Type = row.Committee_Type,
                                               Committee_Type_Name = committeeType.Name,
                                               AppointmentFrom = row.AppointmentFrom,
                                               AppointmentTo = row.AppointmentTo,
                                               CessionDate = director.cessation_Date
                                           }
                                           ).Distinct().ToList();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objCommitteeMaster;
        }
        //End of change

        //Added By NK 11June2021 Committee matrix
        public static List<CommitteeMatrixVM> GetCommitteeMatrix(int? customerID, int userID, int entityID)
        {

            List<CommitteeMatrixVM> _objCommitteeMatrix = new List<CommitteeMatrixVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objCommitteeMatrix = (from row in entities.BM_SP_GetCommitteeMatrix(entityID)
                                   select new CommitteeMatrixVM
                                   {
                                       Id = row.Id,
                                       CommitteeId = row.CommitteeId,
                                       CommitteeName = row.CommitteeName,
                                       CommitteeType = row.CommitteeType,
                                       DirectorId = row.DirectorId,
                                       UserId = row.UserId,
                                       ParticipantType = row.ParticipantType,
                                       FirstName = row.FirstName,
                                       FullName = row.FullName,
                                       Gender = row.Gender,
                                       DIN_PAN = row.DIN_PAN,
                                       DesignationId = row.DesignationId,
                                       Designation = row.Designation,
                                       DirectorShipIdForQuorum = row.DirectorShipIdForQuorum,
                                       DirectorShip = row.DirectorShip,
                                       ParticipantDesignation = row.ParticipantDesignation,
                                       PositionId = row.PositionId,
                                       Position = row.Position,
                                       DateOfAppointment = row.DateOfAppointment
                                   }
                                   ).ToList();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objCommitteeMatrix;
        }
        //End of change


        public static List<CommitteeListVM> GetCommiteeListNew(int? customerID, int userId, int entityID)
        {
            List<CommitteeListVM> objcommiteelist = new List<CommitteeListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {


                    objcommiteelist = (from row in entities.BM_Directors_DetailsOfCommiteePosition
                                       join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                                       join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                                       where row_e.Customer_Id == customerID
                                       && row.EntityId == entityID
                                       && row_e.Is_Deleted == false
                                       && row.IsDeleted == false
                                       orderby committee.Name
                                       select new CommitteeListVM
                                       {
                                           Id = committee.Id,
                                           Name = committee.Name,
                                           Description_ = committee.Description_
                                       }).Distinct().ToList();


                    if (objcommiteelist.Count > 0)
                    {
                        foreach (var item in objcommiteelist)
                        {
                            var getCommiteeList = GetCommitteeDetailsListNew(customerID, userId, entityID, item.Id);
                            if (getCommiteeList.Count > 0)
                            {
                                item.CommiteeDetailsVM = getCommiteeList;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objcommiteelist;
        }

        public static List<CommitteeDetailList> GetCommitteeDetailsListNew(int? customerID, int userId, int entityID, int CommitteeId)
        {
            List<CommitteeDetailList> _objcommiteelist = new List<CommitteeDetailList>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _objcommiteelist = (from row in entities.BM_SP_GetCommitteeOrCompositionById(entityID, CommitteeId,false)
                                        select new CommitteeDetailList
                                        {
                                            Id = row.Id,
                                            Entity_Id = entityID,
                                          
                                            Director_Id = (long)row.DirectorId,
                                            DirectorName = row.FullName,
                                            //DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + "  " + director.FirstName + " " + (string.IsNullOrEmpty(director.MiddleName) ? "" : director.MiddleName) + " " + director.LastName,
                                            ImagePath = row.PhotoDoc,
                                            //Designation_Id = row.DesignationId,
                                            Committee_Id = row.CommitteeId,
                                            CommitteeName = row.CommitteeName,
                                          
                            
                                            FirstName = row.FirstName,
                             
                                            DIN = row.DIN_PAN,
                                           
                                          
                                        }).Distinct().ToList();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objcommiteelist;
        }

    }
}
