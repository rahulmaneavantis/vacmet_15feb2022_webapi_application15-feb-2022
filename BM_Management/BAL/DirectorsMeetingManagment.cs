﻿using AppWebApplication.BM_Managment.DAL;
using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.DirectorsMeetingVM;

namespace AppWebApplication.BM_Managment.BAL
{
    public class DirectorsMeetingManagment
    {
        public static List<MeetingListVM> getUpcomingMeeting(int UserId, int CustomerId, string Role)
        {
            VideoMeetingVM _objVmeeting = new VideoMeetingVM();
            var result = new List<MeetingListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                    result = (from row in entities.BM_SP_MyMeetingsUpcoming(CustomerId, UserId, Role)
                              where row.IsVirtualMeeting == false
                              select new MeetingListVM
                              {
                                  EntityId = row.EntityId,
                                  EntityName = row.EntityName,

                                  MeetingID = row.MeetingId,
                                  MeetingTypeName = row.MeetingTypeName,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTitle = row.MeetingTitle,
                                  IsShorter = row.IsShorter,

                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  MeetingstartDate = row.MeetingStartDate,
                                  CanStartMeeting=row.CanStart,
                                  IsCompleted=row.IsCompleted,

                                  Day = row.Day_,
                                  DayName = row.DayName_,
                                  Month = row.Month_,
                                  YearName = row.Year_,
                                  MeetingTypeId =0,
                                  participantId = (long)row.ParticipantId,
                                  RSPV = row.RSPV,
                                  ReasonforRSPV = row.ReasonforRSPV,
                                  joinurl=row.Join_url,
                                  IsVideoMeeting=row.IsVideoMeeting,
                                  password=row.VPassword,
                                  passcode="",
                                  VMeetingID="",
                                  providerName=""
                              }).ToList();
                    if(result.Count>0)
                    {
                        foreach(var item in result)
                        {
                            if(item.IsVideoMeeting)
                            {
                                item.providerName = (from x in entities.BM_CustomerConfiguration where x.CustomerID == CustomerId select x.ProviderName).FirstOrDefault();
                                item.VMeetingID = (from x in entities.BM_VideoMeeting where x.MeetingID == item.MeetingID select x.V_MeetingId).FirstOrDefault();
                            }
                            item.MeetingTypeId = (from x in entities.BM_Meetings where x.MeetingID == item.MeetingID select x.MeetingTypeId).FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}