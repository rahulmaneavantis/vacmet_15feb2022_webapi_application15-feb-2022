﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static AppWebApplication.BM_Managment.DAL.DirectorDashboardVM;
using AppWebApplication.BM_Managment.DAL;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using System.Reflection;
using AppWebApplication.BM_Management.DAL;

namespace AppWebApplication.BM_Managment.BAL
{
    public class DirectorDashboardManagment
    {
        public static List<DirectorCompliancesVM> GetDirectorComplianceDetails(int UserId, int CustomerId, int roleId, string FY_Year, int EntityID)
        {
            List<DirectorCompliancesVM> _objDirectorComp = new List<DirectorCompliancesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    _objDirectorComp = (from row in entities.BM_SP_MyCompliance_Director(UserId, CustomerId, 1, roleId, 2, FY_Year)
                                        select new DirectorCompliancesVM
                                        {
                                            EntityId = row.EntityId,
                                            CompanyName = row.CompanyName,
                                            DirectorId = row.DirectorId,
                                            DirectorName = row.DirectorName,
                                            ScheduleOnID = row.ScheduleOnID,

                                            //RoleID = row.RoleID,
                                            MappingType = row.MappingType,

                                            MeetingID = row.MeetingID,
                                            MeetingTitle = row.MeetingTitle,

                                            ShortForm = row.ShortForm,
                                            ShortDescription = row.ShortDescription,
                                            Description = row.Description,
                                            RequiredForms = row.RequiredForms,

                                            ScheduleOn = row.ScheduleOn,
                                            ScheduleOnInt = row.ScheduleOnInt,
                                            ScheduleOnTime = row.ScheduleOnTime,
                                            ComplianceStatus = row.ComplianceStatus,

                                            Frequency = row.Frequency,
                                            ForMonth = row.ForMonth,
                                            ForPeriod = row.ForPeriod,
                                            Risk = row.Risk

                                        }).ToList();
                

                    if (_objDirectorComp.Count > 0)
                    {

                        if (EntityID > 0)
                        {
                            _objDirectorComp = _objDirectorComp.Where(x => x.EntityId == EntityID).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorComp;
        }
        public static List<DirectorDashboardCount> GetDashboardCountForDirector(int? customerID, int UserID)
        {
            List<DirectorDashboardCount> count = new List<DirectorDashboardCount>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    count = (from row in entities.BM_SP_GetDashboardCountForDirector(customerID, UserID)
                             select new DirectorDashboardCount
                             {
                                 EntityCount = row.Entities,
                                 DirectorCount = row.Directors,
                                 MeetingsCount = row.Meetings,
                                 AgendaCount = row.Agenda,
                                 DraftMinutesCount = row.Drafts
                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return count;
        }

        public static List<EntityVM> GetEntityList(int userId, int customerId)
        {
            List<EntityVM> _objentity = new List<EntityVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objentity = (from row in entities.BM_SP_EntityListforBOD(userId, customerId)
                                  orderby row.CompanyName
                                  select new EntityVM
                                  {
                                      EntityId = row.ID,
                                      Entity = row.CompanyName,
                                  }).ToList();
                    _objentity.Insert(0, new EntityVM { EntityId = -1, Entity = "All" });

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objentity;
        }

        public static List<FY_VM> GetFY()
        {
            List<FY_VM> _obj = new List<FY_VM>();
            try
            {
                _obj = new List<FY_VM>
                {
                    new FY_VM {FY="CYTD",FYName="Current Financial YTD" },
                    new FY_VM {FY="CPYTD",FYName="Current + Previous Financial YTD" },
                    new FY_VM {FY="All",FYName="ALL" }
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _obj;
        }

        public static List<DirectorCompliancesVM> GetDirComplianceDetails(int UserId, int CustomerId, int roleId, string FY_Year)
        {
            List<DirectorCompliancesVM> _objDirectorComp = new List<DirectorCompliancesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    _objDirectorComp = (from row in entities.BM_SP_MyCompliance_Director(UserId, CustomerId, 1, roleId, 2, FY_Year)
                                        select new DirectorCompliancesVM
                                        {
                                            EntityId = row.EntityId,
                                            CompanyName = row.CompanyName,
                                            DirectorId = row.DirectorId,
                                            DirectorName = row.DirectorName,
                                            ScheduleOnID = row.ScheduleOnID,

                                            //RoleID = row.RoleID,
                                            MappingType = row.MappingType,

                                            MeetingID = row.MeetingID,
                                            MeetingTitle = row.MeetingTitle,

                                            ShortForm = row.ShortForm,
                                            ShortDescription = row.ShortDescription,
                                            Description = row.Description,
                                            RequiredForms = row.RequiredForms,

                                            ScheduleOn = row.ScheduleOn,
                                            ScheduleOnInt = row.ScheduleOnInt,
                                            ScheduleOnTime = row.ScheduleOnTime,
                                            ComplianceStatus = row.ComplianceStatus,

                                            Frequency = row.Frequency,
                                            ForMonth = row.ForMonth,
                                            ForPeriod = row.ForPeriod,
                                            Risk = row.Risk

                                        }).ToList();


                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorComp;
        }
    }
}