﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.BM_Managment.DAL;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using System.Reflection;
using AppWebApplication.Data;
using AppWebApplication.BM_Management.DAL;

namespace AppWebApplication.BM_Management.BAL
{
    public class MyMeetingManagment
    {
        public static List<SeekAviabilityVM> GetSeekAvailability(int? customerID, int UserId)
        {
            List<SeekAviabilityVM> _objaviability = new List<SeekAviabilityVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var date = DateTime.Now.Date;
                    _objaviability = (from row in entities.BM_Meetings
                                      join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                      join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                      join participants in entities.BM_MeetingParticipant on row.MeetingID equals participants.Meeting_ID

                                      where participants.UserId == UserId && row.IsSeekAvailabilitySent == true && row.AvailabilityDueDate >= date && row.IsDeleted == false && participants.IsDeleted == false
                                      select new SeekAviabilityVM
                                      {
                                          MeetingID = row.MeetingID,
                                          MeetingSrNo = row.MeetingSrNo,
                                          MeetingTypeId = row.MeetingTypeId,
                                          MeetingTypeName = meeting.MeetingTypeName,
                                          Quarter_ = row.Quarter_,
                                          MeetingTitle = row.MeetingTitle,
                                          MeetingDate = row.MeetingDate,
                                          MeetingTime = row.MeetingTime,
                                          MeetingVenue = row.MeetingVenue,
                                          IsSeekAvailability = row.IsSeekAvailability,
                                          SeekAvailability_DueDate = row.AvailabilityDueDate,
                                          Availability_ShortDesc = row.Availability_ShortDesc,
                                          ParticipantId = participants.MeetingParticipantId,
                                          Entityt_Id = row.EntityId,
                                          EntityName = entity.CompanyName,
                                          MeetingAviability = (from ava in entities.BM_MeetingAvailability where ava.MeetingID == row.MeetingID select new Aviability { SeekAviabiltyDate = ava.AvailabilityDate, Isavialale = false, seekavaTime = ava.AvailabilityTime + "" + " - " + "" + ava.Availability_ToTime + "   " + "IST", MeetingId = ava.MeetingID, meetingavaiId = ava.MeetingAvailabilityId, ParticipantId = participants.MeetingParticipantId }).ToList()

                                      }).ToList();
                    if (_objaviability != null)
                    {
                        foreach (var item in _objaviability)
                        {
                            if (item.MeetingAviability != null)
                            {
                                foreach (var items in item.MeetingAviability)
                                {
                                    items.ResponseAviabilityId = (from y in entities.BM_MeetingAvailabilityResponse where y.Meeting_ID == items.MeetingId && y.MeetingParticipantID == items.ParticipantId && y.MeetingAvailabilityID == items.meetingavaiId select y.AvailabilityResponseID).FirstOrDefault();
                                    items.Responses = (from x in entities.BM_MeetingAvailabilityResponse where x.Meeting_ID == items.MeetingId && x.MeetingParticipantID == items.ParticipantId && x.MeetingAvailabilityID == items.meetingavaiId select x.Response).FirstOrDefault();
                                    items.seekavadate = items.SeekAviabiltyDate.ToString("dddd, MMM  dd");
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaviability;
        }

        public static List<CircularResolutionVM> GetCircularResolution(int? customerID, int UserID)
        {
            List<CircularResolutionVM> _objCircularResolution = new List<CircularResolutionVM>();
            try
            {

                _objCircularResolution = GetCircularForParticipant(UserID);

                if (_objCircularResolution != null)
                {


                    _objCircularResolution = (from r in _objCircularResolution
                                              where
                                              //r.Stage.ToLower() == "upcoming"
                                              r.MeetingDate != null
                                              && r.MeetingDate >= DateTime.Now.Date
                                              orderby r.MeetingDate
                                              select new CircularResolutionVM
                                              {
                                                  MeetingID = r.MeetingID,
                                                  MeetingTypeName = r.MeetingTypeName,
                                                  MeetingSrNo = r.MeetingSrNo,
                                                  EntityName = r.EntityName,
                                                  IsShorter = r.IsShorter,
                                                  MeetingDate = r.MeetingDate,
                                                  Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                                  DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("ddd") : "",
                                                  Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                                  YearName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("yyyy") : "",
                                                  MeetingTime = r.MeetingTime,
                                                  //MeetingVenue = r.MeetingVenue,
                                                  MeetingTypeId = r.MeetingTypeId,
                                                  FY = r.FY_CY,
                                                  srno = r.MeetingSrNo,
                                                  Stage = r.Stage,
                                                  EntityId = r.Entityt_Id,
                                                  participantId = r.ParticipantId,

                                              }).ToList();
                    if (_objCircularResolution.Count > 0)
                    {
                        foreach (var item in _objCircularResolution)
                        {
                            if (item.srno == 1)
                            {
                                item.sirialnumber = 1 + "st";
                            }
                            else if (item.srno == 2)
                            {
                                item.sirialnumber = 2 + "nd";
                            }
                            else if (item.srno == 3)
                            {
                                item.sirialnumber = 2 + "rd";
                            }
                            else if (item.srno > 3)
                            {
                                item.sirialnumber = item.srno + "th";
                            }
                        }
                    }



                }


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objCircularResolution;
        }

        public static List<ResponsesVM> addUpdateAviabilityResponses(Aviability item, int UserId)
        {
            List<ResponsesVM> _objResponse = new List<ResponsesVM>();
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var check = (from row in entities.BM_MeetingAvailabilityResponse where row.Meeting_ID == item.MeetingId && row.MeetingAvailabilityID == item.meetingavaiId && row.MeetingParticipantID == item.ParticipantId select row).FirstOrDefault();
                    if (check != null)
                    {
                        if (item.AviabilityResponses.ToUpper() == "TRUE")
                        {
                            check.Response = true;

                        }
                        else
                        {
                            check.Response = false;
                        }
                        check.UpdatedOn = DateTime.Now;
                        check.UpdatedBy = UserId;
                        entities.SaveChanges();
                        _objResponse.Add(new ResponsesVM { Message = "Responses saved successfully" });

                    }
                    else
                    {
                        BM_MeetingAvailabilityResponse objaviability = new BM_MeetingAvailabilityResponse();
                        objaviability.Meeting_ID = item.MeetingId;
                        objaviability.MeetingAvailabilityID = item.meetingavaiId;
                        objaviability.MeetingParticipantID = item.ParticipantId;
                        if (item.AviabilityResponses.ToLower() == "true")
                        {
                            objaviability.Response = true;
                        }
                        else
                        {
                            objaviability.Response = false;
                        }
                        objaviability.CreatedOn = DateTime.Now;
                        objaviability.CreatedBy = UserId;
                        entities.BM_MeetingAvailabilityResponse.Add(objaviability);
                        entities.SaveChanges();
                        _objResponse.Add(new ResponsesVM { Message = "Responses saved successfully" });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objResponse.Add(new ResponsesVM { Message = "Server error occured" });
            }
            return _objResponse;
        }

        public static List<CircularResolutionVM> GetPastCircularResolution(int? customerID, int UserId)
        {
            List<CircularResolutionVM> _objCircularResolution = new List<CircularResolutionVM>();
            try
            {

                IEnumerable<CircularResolutionVM> meetings = null;


                meetings = GetPastCircularForParticipant(UserId);


                if (meetings != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        _objCircularResolution = (from r in meetings
                                                      //where r.Stage.ToLower() == "past"
                                                      //orderby r.MeetingDate
                                                  orderby r.EndMeetingDate descending
                                                  select new CircularResolutionVM
                                                  {
                                                      MeetingID = r.MeetingID,
                                                      MeetingTypeName = r.MeetingTypeName,
                                                      MeetingSrNo = r.MeetingSrNo,
                                                      EntityName = r.EntityName,
                                                      IsShorter = r.IsShorter,
                                                      MeetingDate = r.MeetingDate,

                                                      Day = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.Day.ToString() : "",
                                                      DayName = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("ddd") : "",
                                                      Month = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("MMM") : "",
                                                      YearName = r.EndMeetingDate.HasValue ? r.EndMeetingDate.Value.ToString("yyyy") : "",

                                                      //Day = r.MeetingDate.HasValue ? r.MeetingDate.Value.Day.ToString() : "",
                                                      //DayName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("ddd") : "",
                                                      //Month = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("MMM") : "",
                                                      //YearName = r.MeetingDate.HasValue ? r.MeetingDate.Value.ToString("yyyy") : "",
                                                      MeetingTime = r.MeetingTime,
                                                      MeetingTitle = r.MeetingTitle,
                                                      //MeetingVenue = r.MeetingVenue,
                                                      MeetingTypeId = r.MeetingTypeId,
                                                      FY = r.FY_CY,
                                                      Stage = r.Stage,
                                                      EntityId = r.Entityt_Id,
                                                      participantId = r.ParticipantId,
                                                      MappingID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == r.MeetingID && a.MeetingParticipantID == r.ParticipantId select a.MeetingAgendaMappingId).FirstOrDefault(),
                                                      //AgendaID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == r.MeetingID && a.MeetingParticipantID == r.ParticipantId select a.AgendaId).FirstOrDefault(),
                                                      //RSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP).FirstOrDefault(),
                                                      //ReasonforRSPV = (from att in entities.BM_MeetingAttendance where att.MeetingId == r.MeetingID && att.MeetingParticipantId == r.ParticipantId select att.RSVP_Reason).FirstOrDefault(),
                                                  }).ToList();
                        if (_objCircularResolution.Count > 0)
                        {
                            foreach (var item in _objCircularResolution)
                            {
                                if (item.MeetingSrNo == 1)
                                {
                                    item.sirialnumber = 1 + "st";
                                }
                                else if (item.MeetingSrNo == 2)
                                {
                                    item.sirialnumber = 2 + "nd";
                                }
                                else if (item.MeetingSrNo == 3)
                                {
                                    item.sirialnumber = 2 + "rd";
                                }
                                else if (item.MeetingSrNo > 3)
                                {
                                    item.sirialnumber = item.MeetingSrNo + "th";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objCircularResolution;
        }

        private static IEnumerable<CircularResolutionVM> GetPastCircularForParticipant(int userId)
        {
            List<CircularResolutionVM> _objpastcircular = new List<CircularResolutionVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var today = DateTime.Today;
                    _objpastcircular = (from row in entities.BM_Meetings
                                        join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                                        join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                        join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                        where //row.Customer_Id == CustomerId &&
                                        row.IsDeleted == false
                                        && row.IsNoticeSent == true
                                        && row.IsCompleted == true
                                        && participant.UserId == userId
                                        && participant.IsDeleted == false
                                        && participant.IsInvited == null
                                        && row.MeetingCircular == "C"
                                        orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                                        select new CircularResolutionVM
                                        {
                                            MeetingID = row.MeetingID,
                                            Type = row.MeetingCircular.Trim(),
                                            MeetingSrNo = row.MeetingSrNo,
                                            MeetingTypeId = row.MeetingTypeId,
                                            MeetingTypeName = meeting.MeetingTypeName,
                                            Quarter_ = row.Quarter_,
                                            MeetingTitle = row.MeetingTitle,
                                            MeetingDate = row.MeetingDate,
                                            EndMeetingDate = row.EndMeetingDate,
                                            MeetingTime = row.MeetingTime,
                                            MeetingVenue = row.MeetingVenue,
                                            IsSeekAvailability = row.IsSeekAvailability,
                                            Availability_ShortDesc = row.Availability_ShortDesc,
                                            TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                            Entityt_Id = row.EntityId,
                                            EntityName = entity.CompanyName,
                                            IsShorter = row.IsShorter,
                                            ParticipantId = participant.MeetingParticipantId,
                                            MappingID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == row.MeetingID && a.MeetingParticipantID == participant.MeetingParticipantId select a.MeetingAgendaMappingId).FirstOrDefault(),
                                            FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                            Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past",
                                            //Stage = row.CircularDate == null ? "" : row.CircularDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past",

                                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objpastcircular;
        }

        public static List<MeetingAgendaSummaryVM> GetCircularAgenda(int? customerID, int userID, long meetingId)
        {
            List<MeetingAgendaSummaryVM> _objCircularAgenda = new List<MeetingAgendaSummaryVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long participantID = 0;
                    long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == userID select dir.Id).FirstOrDefault();
                    if (directorID > 0)
                    {
                        participantID = (from par in entities.BM_MeetingParticipant where par.Director_Id == directorID && par.Meeting_ID == meetingId select par.MeetingParticipantId).FirstOrDefault();
                    }

                    _objCircularAgenda = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, 0)
                                          select new MeetingAgendaSummaryVM
                                          {

                                              AgendaId = row.BM_AgendaMasterId,
                                              ParticipantId = participantID,
                                              AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                              MeetingId = row.MeetingID,
                                              AgendaFormate = row.AgendaItemText,
                                              MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                              AgendaResponseResult = row.ResultRemark,
                                              MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                              Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                              Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                              hasDocuments = (from x in entities.BM_FileData where x.AgendaMappingId == row.MeetingAgendaMappingID && x.IsDeleted == false select x).Any(),
                                          }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objCircularAgenda;
        }

        public static List<ResponsesVM> addUpdateAgendaResponses(MeetingAgendaSummaryVM vottings, int UserId)
        {
            List<ResponsesVM> _objResponses = new List<ResponsesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkvotingexist = (from row in entities.BM_Meetings_Agenda_Responses where row.Meeting_ID == vottings.MeetingId && row.MeetingParticipantID == vottings.ParticipantId && row.MeetingAgendaMappingId == vottings.MeetingAgendaMappingId select row).FirstOrDefault();
                    if (checkvotingexist != null)
                    {
                        checkvotingexist.Meeting_ID = vottings.MeetingId;
                        checkvotingexist.MeetingParticipantID = vottings.ParticipantId;
                        checkvotingexist.AgendaId = (long)vottings.AgendaId;
                        checkvotingexist.Response = vottings.AgendaResponse;
                        checkvotingexist.MeetingAgendaMappingId = (long)vottings.MeetingAgendaMappingId;
                        checkvotingexist.CircularResponseTime = DateTime.Now.TimeOfDay.ToString();
                        checkvotingexist.IsDeleted = false;
                        checkvotingexist.CreatedBy = UserId;
                        checkvotingexist.CreatedOn = DateTime.Now;
                        entities.SaveChanges();
                        _objResponses.Add(new ResponsesVM { Message = "Responses saved successfully" });
                    }
                    else
                    {
                        BM_Meetings_Agenda_Responses _objvotting = new BM_Meetings_Agenda_Responses();
                        _objvotting.Meeting_ID = vottings.MeetingId;
                        _objvotting.MeetingParticipantID = vottings.ParticipantId;
                        _objvotting.AgendaId = (long)vottings.AgendaId;
                        _objvotting.Response = vottings.AgendaResponse;
                        _objvotting.MeetingAgendaMappingId = (long)vottings.MeetingAgendaMappingId;
                        _objvotting.CircularResponseTime = DateTime.Now.TimeOfDay.ToString();
                        _objvotting.IsDeleted = false;
                        _objvotting.CreatedBy = UserId;
                        _objvotting.CreatedOn = DateTime.Now;
                        entities.BM_Meetings_Agenda_Responses.Add(_objvotting);
                        entities.SaveChanges();
                        _objResponses.Add(new ResponsesVM { Message = "Responses saved successfully" });
                    }
                }
                GetAllAgendaDetails(vottings.MeetingId);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objResponses.Add(new ResponsesVM { Message = "Server error occured" });
            }
            return _objResponses;
        }

        private static void GetAllAgendaDetails(long meetingId)
        {
            try
            {
                BM_Meetings_Agenda_Responses _objcircular = new BM_Meetings_Agenda_Responses();
                CircularAgendaData obj = new CircularAgendaData();

                obj.CircularAgendaResultVM = new CircularAgendaResult();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getdata = (from rows in entities.BM_MeetingAgendaMapping

                                   where rows.MeetingID == meetingId
                                   select new CircularAgendaData
                                   {
                                       MeetingAgendaMappingId = rows.MeetingAgendaMappingID,
                                       AgendaName = rows.AgendaItemText,
                                       AgendaId = rows.AgendaID,
                                       MeetingId = meetingId
                                   }).ToList();
                    if (getdata.Count > 0)
                    {
                        foreach (var item in getdata)
                        {
                            var getCirculardata = (from row in entities.BM_Meetings_Agenda_Responses
                                                   join MP in entities.BM_MeetingParticipant on row.MeetingParticipantID equals MP.MeetingParticipantId
                                                   where row.MeetingAgendaMappingId == item.MeetingAgendaMappingId && row.Meeting_ID == item.MeetingId && row.AgendaId == item.AgendaId
                                                   select row).FirstOrDefault();
                            var counttotalresponses = (from per in entities.BM_Meetings_Agenda_Responses where per.Meeting_ID == meetingId && per.AgendaId == item.AgendaId && per.MeetingAgendaMappingId == item.MeetingAgendaMappingId && per.MeetingParticipantID == getCirculardata.MeetingParticipantID select per).Count();
                            item.CircularAgendaResultVM = new CircularAgendaResult();
                            var getresultlist = (from row in entities.BM_SP_CircularAgendaCountResponse(item.MeetingId, item.AgendaId, item.MeetingAgendaMappingId) select row).FirstOrDefault();
                            if (counttotalresponses == getresultlist.Participant)
                            {

                                if (getresultlist != null)
                                {
                                    int totalApproved = (int)Math.Round((double)(100 * getresultlist.Approve) / getresultlist.Participant);
                                    int totaldisaproved = (int)Math.Round((double)(100 * getresultlist.Disapprove) / getresultlist.Participant);
                                    int totalatmeet = (int)Math.Round((double)(100 * getresultlist.Atmeet) / getresultlist.Participant);
                                    int totalAbstain = (int)Math.Round((double)(100 * getresultlist.Abstain) / getresultlist.Participant);
                                    if (totalApproved > 50)
                                    {
                                        item.CircularAgendaResultVM.Result = "A";
                                        item.CircularAgendaResultVM.Remark = "Passed";
                                    }
                                    if (totalApproved == 100)
                                    {
                                        item.CircularAgendaResultVM.Result = "A";
                                        item.CircularAgendaResultVM.Remark = "Unanimously";
                                    }
                                    else if (totalAbstain > 50)
                                    {
                                        item.CircularAgendaResultVM.Result = "D";
                                        item.CircularAgendaResultVM.Remark = "Not Passed";
                                    }
                                    else if (totaldisaproved > 50)
                                    {
                                        item.CircularAgendaResultVM.Result = "D";
                                        item.CircularAgendaResultVM.Remark = "Not Passed";
                                    }

                                    else if (totalatmeet > 50)
                                    {
                                        item.CircularAgendaResultVM.Result = "Atmeet";
                                        item.CircularAgendaResultVM.Remark = "Discuss in meeting";
                                    }
                                    else if (totalApproved == 0 && totaldisaproved == 0 && totalatmeet == 0 || totalAbstain == 0 && totaldisaproved == 0 && totalatmeet == 0)
                                    {
                                        if (getresultlist.CircularDuedate <= DateTime.Now)
                                        {
                                            item.CircularAgendaResultVM.Result = "D";
                                            item.CircularAgendaResultVM.Remark = "Not Passed";
                                        }
                                        else
                                        {
                                            item.CircularAgendaResultVM.Result = "No Responses";
                                        }
                                    }
                                    else if ((totalApproved == 50 && totaldisaproved == 50) || (totalatmeet == 50 && totalApproved == 50))
                                    {
                                        item.CircularAgendaResultVM.Result = "D";
                                        item.CircularAgendaResultVM.Remark = "Not Passed";
                                    }
                                    if (item.CircularAgendaResultVM.Result != null)
                                    {
                                        var checkResultforVoting = (from mappin in entities.BM_MeetingAgendaMapping
                                                                    where mappin.AgendaID == item.AgendaId
                    && mappin.MeetingID == item.MeetingId && mappin.MeetingAgendaMappingID == item.MeetingAgendaMappingId
                                                                    select mappin).FirstOrDefault();
                                        if (checkResultforVoting != null)
                                        {
                                            checkResultforVoting.Result = item.CircularAgendaResultVM.Result;
                                            checkResultforVoting.ResultRemark = item.CircularAgendaResultVM.Remark;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static List<DraftMeetingListVM> GetConcludeMeetings(int? customerID, int userId)
        {
            var result = new List<DraftMeetingListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_SP_MyMeetingsConcluded(customerID, userId)
                              where row.MeetingCircular == "M"
                              orderby row.MeetingID descending
                              select new DraftMeetingListVM
                              {
                                  MeetingID = row.MeetingID,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeName = row.MeetingTypeName,
                                  MeetingTypeId = row.MeetingTypeId,

                                  Quarter_ = row.Quarter_,
                                  //MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  Day = row.Day_,
                                  DayName = row.Year_,
                                  Month = row.Month_,

                                  MeetingTime = row.MeetingTime,
                                  MeetingVenue = row.MeetingVenue,
                                  EntityName = row.CompanyName,
                                  EntityId = row.EntityId,

                                  participantId = row.MeetingParticipantId,
                                  DraftCirculationID = (long)row.DraftCirculationID,
                                  MinutesDetailsID = (long)row.MinutesDetailsID,
                                  DateOfDraftCirculation = row.DateOfDraftCirculation,
                                  DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                                  IsCirculate = row.IsCirculate,
                                  IsFinalized = row.IsFinalized,

                                  IsDelivered = row.IsDelivered,
                                  IsRead = row.IsRead,
                                  IsApproved = row.IsApproved,
                                  FY = row.FYText,
                                  Attendance = row.Attendance,
                                  Present = row.Present,
                                  FYID = row.FYID,
                                  MappingID = (from a in entities.BM_Meetings_Agenda_Responses where a.Meeting_ID == row.MeetingID && a.MeetingParticipantID == row.MeetingParticipantId select a.MeetingAgendaMappingId).FirstOrDefault(),
                                  StartMeetingTime = row.StartTime,
                                  EndMeetingTime = row.EndMeetingTime,
                                  HistoricalID = row.HistoricalID,
                              }).ToList();
                    if (result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            string Meetingtype = item.MeetingTypeName;
                            string quarter = item.Quarter_ != null ? item.Quarter_ : "";
                            string srno = item.MeetingSrNo != null ? "[" + item.MeetingSrNo + "]" : "[#]";
                            string FY = item.FY;
                            item.MeetingTitle = "Meeting -" + Meetingtype + " " + quarter + " " + srno + " " + FY;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public static List<DraftMeetingListVM> GetMyDraftMinutes(int? customerID, int userID)
        {
            List<DraftMeetingListVM> _objDraftMeeting = new List<DraftMeetingListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDraftMeeting = (from row in entities.BM_SP_MyMeetingsDraftMinutes(customerID, userID)
                                        select new DraftMeetingListVM
                                        {
                                            MeetingID = row.MeetingID,
                                            MeetingSrNo = row.MeetingSrNo,
                                            MeetingTypeName = row.MeetingTypeName,
                                            MeetingTypeId = row.MeetingTypeId,

                                            Quarter_ = row.Quarter_,
                                            MeetingTitle = row.MeetingTitle,
                                            MeetingDate = row.MeetingDate,
                                            Day = row.Day_,
                                            DayName = row.Year_,
                                            Month = row.Month_,

                                            MeetingTime = row.MeetingTime,
                                            MeetingVenue = row.MeetingVenue,
                                            EntityName = row.CompanyName,
                                            EntityId = row.EntityId,

                                            participantId = row.MeetingParticipantId,
                                            DraftCirculationID = row.DraftCirculationID,
                                            MinutesDetailsID = row.MinutesDetailsID,
                                            DateOfDraftCirculation = row.DateOfDraftCirculation,
                                            DueDateOfDraftCirculation = row.DueDateOfDraftCirculation,

                                            IsCirculate = row.IsCirculate,
                                            IsFinalized = row.IsFinalized,

                                            IsDelivered = row.IsDelivered,
                                            IsRead = row.IsRead,
                                            IsApproved = row.IsApproved,
                                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDraftMeeting;
        }

        public static List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingId, int? customerID, int UserId)
        {
            List<MeetingAttendance_VM> _objMeetingAttendence = new List<MeetingAttendance_VM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _objMeetingAttendence = (from row in entities.BM_SP_GetParticipantAttendance(meetingId)
                                             select new MeetingAttendance_VM
                                             {
                                                 ParticipantId = row.MeetingParticipantId,
                                                 MeetingParticipant_Name = row.Participant_Name,
                                                 MeetingId = row.Meeting_ID,
                                                 Director_Id = row.Director_Id,
                                                 ImagePath = row.Photo_Doc,
                                                 Designation = row.Designation,
                                                 Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                                 Remark = row.Reason,
                                                 Position = row.Position,
                                                 RSVP = row.RSVP,
                                                 RSVP_Reason = row.RSVP_Reason
                                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objMeetingAttendence;
        }

        public static MeetingMinutesDetailsVM GetMinutesDetails(long meetingId)
        {
            MeetingMinutesDetailsVM obj = new MeetingMinutesDetailsVM();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    obj = (from row in entities.BM_MeetingMinutesDetails
                           where row.MeetingId == meetingId
                           select new MeetingMinutesDetailsVM
                           {
                               MeetingMinutesDetailsId = row.Id,
                               MOM_MeetingId = row.MeetingId,
                               Place = row.Place,
                               DateOfEntering = row.DateOfEntering,
                               DateOfSigning = row.SigningOfEntering,
                               IsCirculate = row.IsCirculate,
                               IsFinalized = row.IsFinalized,
                               ConfirmMeetingId = row.ConfirmMeetingId
                           }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return obj;
        }

        public static List<ResponsesVM> saveDraftMinutesComments(int? customerID, int UserId, MeetingMinutesCommentVM _objMinutesComment)
        {
            List<ResponsesVM> _objResponses = new List<ResponsesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                where row.ID == _objMinutesComment.DraftCirculationID
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.DraftComments = _objMinutesComment.CommentData;
                        _obj.UpdatedBy = UserId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        _objResponses.Add(new ResponsesVM { Message = "Comment saved successfully" });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objResponses.Add(new ResponsesVM { Message = "Server error occurred" });
            }
            return _objResponses;
        }

        public static List<MeetingMinutesCommentVM> getDraftMinutesComments(int? customerID, int v, long draftCirculationID)
        {
            List<MeetingMinutesCommentVM> _objdraftcomments = new List<MeetingMinutesCommentVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objdraftcomments = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                         where row.ID == draftCirculationID
                                         select new MeetingMinutesCommentVM
                                         {
                                             CommentData = row.DraftComments,
                                         }
                                         ).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objdraftcomments;
        }

        public static bool IsDraftMinutesApproved(long draftCirculationID)
        {
            var result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_MeetingMinutesDetailsTransaction
                              where row.ID == draftCirculationID && row.ApprovedOn != null
                              select row).Any();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public static List<PreviewAgendaVM> GetMyDraftMinutesDocuments(int? customerID, int UserID, long draftCirculationID, long meetingId)
        {
            List<PreviewAgendaVM> _objdraftDoc = new List<PreviewAgendaVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var minutesDetails = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                          where row.ID == draftCirculationID && row.IsDeleted == false && row.ReadOn == null
                                          select row).FirstOrDefault();
                    if (minutesDetails != null)
                    {
                        minutesDetails.ReadOn = DateTime.Now;
                        minutesDetails.UpdatedBy = UserID;
                        minutesDetails.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    }
                    _objdraftDoc = (from row in entities.BM_Meetings
                                    join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                    join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                    where row.MeetingID == meetingId && row.Customer_Id == customerID && row.IsDeleted == false
                                    select new PreviewAgendaVM
                                    {
                                        MeetingID = row.MeetingID,
                                        IsVirtual = row.IsVirtualMeeting,
                                        GenerateMinutes = true,
                                        MeetingSrNo = row.MeetingSrNo,
                                        MeetingTypeId = row.MeetingTypeId,
                                        MeetingTypeName = meeting.MeetingTypeName,
                                        Quarter_ = row.Quarter_,
                                        MeetingTitle = row.MeetingTitle,
                                        MeetingDate = row.MeetingDate,
                                        MeetingTime = row.MeetingTime,
                                        MeetingAddressType = row.MeetingVenueType,
                                        MeetingVenue = row.MeetingVenue,

                                        MeetingStartDate = row.StartMeetingDate,
                                        MeetingStartTime = row.StartTime,
                                        MeetingEndDate = row.EndMeetingDate,
                                        MeetingEndTime = row.EndMeetingTime,

                                        IsAdjourned = row.IsAdjourned,

                                        Entityt_Id = row.EntityId,
                                        EntityCIN_LLPIN = entity.CIN_LLPIN,
                                        EntityName = entity.CompanyName,
                                        EntityAddressLine1 = entity.Regi_Address_Line1,
                                        EntityAddressLine2 = entity.Regi_Address_Line2
                                    }).ToList();

                    if (_objdraftDoc.Count > 0)
                    {
                        foreach (var item in _objdraftDoc)
                        {
                            item.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, true, 0)
                                                   select new MeetingAgendaMappingVM
                                                   {
                                                       SrNo = (int)TemplateList.SrNo,
                                                       MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                       Meeting_Id = TemplateList.MeetingID,
                                                       AgendaID = TemplateList.BM_AgendaMasterId,
                                                       PartId = TemplateList.PartID,
                                                       AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                       AgendaItemText = TemplateList.AgendaItemText,

                                                       AgendaFormat = TemplateList.AgendaFormat
                                                   }).ToList();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objdraftDoc;

        }

        public static List<ResponsesVM> addUpdateVootingNotingAgendaResponses(MeetingAgendaSummaryVM items, long UserID)
        {
            int UserId = Convert.ToInt32(UserID);
            List<ResponsesVM> _objresponse = new List<ResponsesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkIsvotingNotingExist = (from row in entities.BM_Meetings_Agenda_Responses
                                                    where row.Meeting_ID == items.MeetingId

    && row.MeetingParticipantID == items.ParticipantId
    && row.AgendaId == items.AgendaId
    && row.MeetingAgendaMappingId == items.MeetingAgendaMappingId
                                                    select row).FirstOrDefault();
                    if (checkIsvotingNotingExist != null)
                    {
                        checkIsvotingNotingExist.Response = items.AgendaResponse;
                        checkIsvotingNotingExist.IsDeleted = false;
                        checkIsvotingNotingExist.UpdatedOn = DateTime.Now;
                        checkIsvotingNotingExist.UpdatedBy = UserId;
                        entities.SaveChanges();
                        _objresponse.Add(new ResponsesVM { Message = "Responses saved successfully" });
                    }
                    else
                    {
                        BM_Meetings_Agenda_Responses objvotingnoting = new BM_Meetings_Agenda_Responses();
                        objvotingnoting.MeetingParticipantID = items.ParticipantId;
                        objvotingnoting.MeetingAgendaMappingId = (long)items.MeetingAgendaMappingId;
                        objvotingnoting.AgendaId = (long)items.AgendaId;
                        objvotingnoting.Meeting_ID = items.MeetingId;
                        objvotingnoting.Response = items.AgendaResponse;
                        objvotingnoting.IsDeleted = false;
                        objvotingnoting.CreatedOn = DateTime.Now;
                        objvotingnoting.CreatedBy = UserId;
                        entities.BM_Meetings_Agenda_Responses.Add(objvotingnoting);
                        entities.SaveChanges();
                        _objresponse.Add(new ResponsesVM { Message = "Responses saved successfully" });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objresponse.Add(new ResponsesVM { Message = "Server error occured" });
            }
            return _objresponse;
        }

        public static List<MeetingAgendaSummaryVM> GetMeetingwiseAgendaList(int? customerID, int UserId, int meetingId)
        {
            List<MeetingAgendaSummaryVM> _objAgendaList = new List<MeetingAgendaSummaryVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (UserId > 0 && meetingId > 0)
                    {
                        long participantID = 0;
                        long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == UserId && dir.IsActive == true select dir.Id).FirstOrDefault();
                        if (directorID > 0)
                        {
                            participantID = (from par in entities.BM_MeetingParticipant where par.Director_Id == directorID && par.Meeting_ID == meetingId && par.IsDeleted == false select par.MeetingParticipantId).FirstOrDefault();
                        }

                        _objAgendaList = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, 0)
                                          select new MeetingAgendaSummaryVM
                                          {

                                              AgendaId = row.BM_AgendaMasterId,
                                              ParticipantId = participantID,
                                              AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                              MeetingId = row.MeetingID,
                                              AgendaFormate = row.AgendaItemText,
                                              MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                              AgendaResponseResult = row.ResultRemark,
                                              MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                              Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                              Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                          }).ToList();
                        if (_objAgendaList.Count > 0)
                        {
                            foreach (var item in _objAgendaList)
                            {
                                if (item.AgendaResponse == "A")
                                {
                                    item.AgendaResponse = "Approved";
                                }
                                else if (item.AgendaResponse == "D")
                                {
                                    item.AgendaResponse = "Disapproved";
                                }
                                else if (item.AgendaResponse == "Atmeet")
                                {
                                    item.AgendaResponse = "At Meeting";
                                }
                                else if (item.AgendaResponse == "AB")
                                {
                                    item.AgendaResponse = "Abstain";
                                }
                                else if (item.AgendaResponse == "N")
                                {
                                    item.AgendaResponse = "Noted";
                                }
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objAgendaList;
        }

        public static List<AgendaDocumentVM> GetMeetingwiseAgendaDocument(int? customerID, int userID, int meetingAgendaMapping)
        {
            List<AgendaDocumentVM> _objDocument = new List<AgendaDocumentVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDocument = (from row in entities.BM_FileData
                                    where row.AgendaMappingId == meetingAgendaMapping
                                         && row.IsDeleted == false
                                    select new AgendaDocumentVM
                                    {
                                        ID = row.Id,
                                        FileName = row.FileName

                                    }
                                      ).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objDocument;
        }

        public static List<ResponsesVM> AddUpdateRSVP(int? customerID, int UserId, long meetingId, long participantId, string reasonforRSPV, string RSPV)
        {
            List<ResponsesVM> _objresponse = new List<ResponsesVM>();
            try
            {
                if (RSPV != null && RSPV != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var IsExsist = (from row in entities.BM_MeetingAttendance where row.MeetingId == meetingId && row.MeetingParticipantId == participantId && row.IsActive == true select row).FirstOrDefault();
                        if (IsExsist != null)
                        {
                            IsExsist.RSVP = RSPV;
                            IsExsist.RSVP_Reason = reasonforRSPV;
                            IsExsist.Updatedby = UserId;
                            IsExsist.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                            _objresponse.Add(new ResponsesVM { Message = "Responses updated successfully" });
                        }
                        else
                        {
                            BM_MeetingAttendance objmeetingattendence = new BM_MeetingAttendance();
                            objmeetingattendence.MeetingId = meetingId;
                            objmeetingattendence.MeetingParticipantId = participantId;
                            objmeetingattendence.RSVP = RSPV;
                            objmeetingattendence.CreatedOn = DateTime.Now;
                            objmeetingattendence.Createdby = UserId;
                            objmeetingattendence.Attendance = "P";
                            objmeetingattendence.RSVP_Reason = reasonforRSPV;
                            objmeetingattendence.IsActive = true;
                            objmeetingattendence.Createdby = UserId;
                            objmeetingattendence.CreatedOn = DateTime.Now;
                            entities.BM_MeetingAttendance.Add(objmeetingattendence);
                            entities.SaveChanges();
                            _objresponse.Add(new ResponsesVM { Message = "Responses saved successfully" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objresponse.Add(new ResponsesVM { Message = "Server error occurred" });
            }
            return _objresponse;
        }

        public static List<ConcludedMeetingAgendaItemVM> GetConcludeMeetingAgenda(int? customerID, int UserId)
        {
            List<ConcludedMeetingAgendaItemVM> _objconcludeagendaList = new List<ConcludedMeetingAgendaItemVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objconcludeagendaList = (from row in entities.BM_SP_MeetingConcludeAgendaList(customerID, UserId)
                                              select new ConcludedMeetingAgendaItemVM
                                              {

                                                  EntityId = row.EntityId,
                                                  CompanyName = row.CompanyName,
                                                  MeetingTypeId_ = row.MeetingTypeId,
                                                  MeetingTypeName = row.MeetingTypeName,
                                                  FY = row.FY,
                                                  FYText = row.FYText,
                                                  Meeting_Id = row.MeetingID,
                                                  MeetingID = row.MeetingID,
                                                  MeetingTitle = row.Meeting,
                                                  MeetingDate = row.MeetingDate,
                                                  MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                                                  AgendaID = row.AgendaId,
                                                  AgendaItemText = row.AgendaItem,
                                                  ResultRemark = row.ResultRemark,
                                                  HasCompliance = row.HasCompliance,
                                                  HasInfo = row.HasInfo,
                                                  HistoricalID = row.HistoricalID
                                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objconcludeagendaList;
        }

        public static List<VideoMeetingVM> GetJoinVideoConfrencingDetails(int? customerID, int userID, int meetingId)
        {
            List<VideoMeetingVM> _objVmeeting = new List<VideoMeetingVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objVmeeting = (from x in entities.BM_Meetings
                                    join y in entities.BM_VideoMeeting on x.MeetingID equals y.MeetingID into vm
                                    from z in vm.DefaultIfEmpty()
                                    where x.MeetingID == meetingId
                                    && x.IsDeleted == false
                                    select new VideoMeetingVM
                                    {
                                        MeetingID = x.MeetingID,
                                        IsVideoMeeting = x.IsVideoMeeting,
                                        Join_Url = z.Join_Url,
                                        V_MeetingId = z.V_MeetingId

                                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objVmeeting;
        }

        public static List<ResponsesVM> Markattendence(long meetingId, int? customerID, int UserId, long ParticipantID)
        {
            List<ResponsesVM> _objresponses = new List<ResponsesVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var checkAttendence = (from row in entities.BM_MeetingAttendance where row.MeetingId == meetingId && row.MeetingParticipantId == ParticipantID select row).FirstOrDefault();
                    if (checkAttendence != null)
                    {
                        checkAttendence.Attendance = "P";
                        checkAttendence.UpdatedOn = DateTime.Now;
                        checkAttendence.Updatedby = UserId;
                        entities.SaveChanges();
                        _objresponses.Add(new ResponsesVM { Message = "Attendance mark successfully" });
                    }
                    else
                    {
                        BM_MeetingAttendance _objattendence = new BM_MeetingAttendance();
                        _objattendence.MeetingId = meetingId;
                        _objattendence.MeetingParticipantId = ParticipantID;
                        _objattendence.Attendance = "P";
                        _objattendence.CreatedOn = DateTime.Now;
                        _objattendence.Createdby = UserId;
                        entities.BM_MeetingAttendance.Add(_objattendence);
                        entities.SaveChanges();
                        _objresponses.Add(new ResponsesVM { Message = "Attendance mark successfully" });

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objresponses.Add(new ResponsesVM { Message = "Server error Occurred" });
            }
            return _objresponses;
        }

        public static List<MeetingAgendaSummaryVM> GetMeetingDetails(long meetingId, int? customerID, int userId)
        {
            List<MeetingAgendaSummaryVM> _objMeetingAgenda = new List<MeetingAgendaSummaryVM>();
            try
            {
                long participantID = 0;
                VideoMeetingVM _objVmeeting = new VideoMeetingVM();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == userId where dir.Customer_Id == customerID select dir.Id).FirstOrDefault();
                    if (directorID > 0)
                    {
                        participantID = (from par in entities.BM_MeetingParticipant where par.Director_Id == directorID && par.Meeting_ID == meetingId where par.IsDeleted == false select par.MeetingParticipantId).FirstOrDefault();

                    }

                    _objMeetingAgenda = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, 0)
                                         select new MeetingAgendaSummaryVM
                                         {

                                             AgendaId = row.BM_AgendaMasterId,
                                             ParticipantId = participantID,
                                             AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                             MeetingId = row.MeetingID,
                                             AgendaFormate = row.AgendaItemHeading,
                                             MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                             AgendaResponseResult = row.ResultRemark,
                                             MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                             Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                             Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                             // For if Agenda has Document then use this flag 26 Jul 2021
                                             hasDocuments = (from x in entities.BM_FileData where x.AgendaMappingId == row.MeetingAgendaMappingID && x.IsDeleted == false select x).Any(),


                                         }).ToList();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objMeetingAgenda;
        }

        private static List<CircularResolutionVM> GetCircularForParticipant(int userID)
        {
            List<CircularResolutionVM> _objCircular = new List<CircularResolutionVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var today = DateTime.Today;
                    //_objCircular = (from row in entities.BM_Meetings
                    //                join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                    //                join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                    //                join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id

                    //                where //row.Customer_Id == CustomerId && 
                    //                row.IsDeleted == false && row.IsNoticeSent == true &&
                    //                participant.UserId == userID 
                    //&& participant.IsDeleted == false && participant.IsInvited == null && row.MeetingCircular == "C"
                    //                orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                    _objCircular = (from row in entities.BM_Meetings
                                    join participant in entities.BM_MeetingParticipant on row.MeetingID equals participant.Meeting_ID
                                    join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                    join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                    where //row.Customer_Id == CustomerId &&
                                    row.IsDeleted == false
                                    && row.IsNoticeSent == true
                                    && participant.UserId == userID
                                    && participant.IsDeleted == false
                                    && participant.IsInvited == null
                                    && row.MeetingCircular == "C"
                                    && row.IsVirtualMeeting == false
                                    && row.IsCompleted == false
                                    orderby row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn descending
                                    select new CircularResolutionVM
                                    {
                                        MeetingID = row.MeetingID,
                                        Type = row.MeetingCircular.Trim(),
                                        MeetingSrNo = row.MeetingSrNo,
                                        MeetingTypeId = row.MeetingTypeId,
                                        MeetingTypeName = meeting.Name,
                                        Quarter_ = row.Quarter_,
                                        MeetingTitle = row.MeetingTitle,
                                        MeetingDate = row.MeetingDate,
                                        MeetingTime = row.MeetingTime,
                                        MeetingVenue = row.MeetingVenue,
                                        IsSeekAvailability = row.IsSeekAvailability,
                                        Availability_ShortDesc = row.Availability_ShortDesc,
                                        TypeName = row.MeetingCircular == "C" ? "Circular" : "Meeting",
                                        Entityt_Id = row.EntityId,
                                        EntityName = entity.CompanyName,
                                        IsShorter = row.IsShorter,
                                        ParticipantId = participant.MeetingParticipantId,
                                        FY_CY = (from x in entities.BM_YearMaster where x.FYID == row.FY select x.FYText).FirstOrDefault(),
                                        Stage = row.MeetingDate == null ? "" : row.MeetingDate == today ? "Todays" : row.MeetingDate > today ? "Upcoming" : "Past"
                                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objCircular;

        }


        public static List<PreviewAgendaVM> PreviewAgenda(long MeetingId, int? CustomerId, bool GenerateMinutes)
        {
            List<PreviewAgendaVM> _objagendaPre = new List<PreviewAgendaVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objagendaPre = (from row in entities.BM_Meetings
                                     join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                     join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                     where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                                     select new PreviewAgendaVM
                                     {
                                         MeetingID = row.MeetingID,
                                         IsVirtual = row.IsVirtualMeeting,
                                         GenerateMinutes = GenerateMinutes,
                                         MeetingSrNo = row.MeetingSrNo,
                                         MeetingTypeId = row.MeetingTypeId,
                                         MeetingTypeName = meeting.MeetingTypeName,
                                         Quarter_ = row.Quarter_,
                                         MeetingTitle = row.MeetingTitle,
                                         MeetingDate = row.MeetingDate,
                                         MeetingTime = row.MeetingTime,
                                         MeetingAddressType = row.MeetingVenueType,
                                         MeetingVenue = row.MeetingVenue,

                                         MeetingStartDate = row.StartMeetingDate,
                                         MeetingStartTime = row.StartTime,
                                         MeetingEndDate = row.EndMeetingDate,
                                         MeetingEndTime = row.EndMeetingTime,

                                         IsAdjourned = row.IsAdjourned,

                                         Entityt_Id = row.EntityId,
                                         EntityCIN_LLPIN = entity.CIN_LLPIN,
                                         EntityName = entity.CompanyName,
                                         EntityAddressLine1 = entity.Regi_Address_Line1,
                                         EntityAddressLine2 = entity.Regi_Address_Line2
                                     }).ToList();

                    if (_objagendaPre.Count > 0)
                    {
                        foreach (var items in _objagendaPre)
                        {
                            items.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, null, GenerateMinutes, 0)
                                                    select new MeetingAgendaMappingVM
                                                    {
                                                        SrNo = (int)TemplateList.SrNo,
                                                        MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                        Meeting_Id = TemplateList.MeetingID,
                                                        AgendaID = TemplateList.BM_AgendaMasterId,
                                                        PartId = TemplateList.PartID,
                                                        AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                        AgendaItemText = TemplateList.AgendaItemText,

                                                        AgendaFormat = TemplateList.AgendaFormat
                                                    }).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objagendaPre;
        }


        // For AgendawithVotingNoting
        public static List<MeetingAgendaSummary> GetAgendaMeetingwise(long meetingId, int userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    long participantID = 0;
                    long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == userID select dir.Id).FirstOrDefault();

                    if (directorID > 0)
                    {
                        participantID = (from par in entities.BM_MeetingParticipant
                                         where par.Director_Id == directorID
                                         && par.Meeting_ID == meetingId
                                         && par.IsDeleted == false
                                         select par.MeetingParticipantId).FirstOrDefault();
                    }

                    var getMeetingwiseAgenda = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, null)
                                                select new MeetingAgendaSummary
                                                {
                                                    AgendaId = row.BM_AgendaMasterId,
                                                    ParticipantId = participantID,
                                                    AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                                    MeetingId = row.MeetingID,
                                                    AgendaFormate = row.AgendaItemText,
                                                    //AgendaFormate = row.AgendaItemHeading,
                                                    MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                                    AgendaResponseResult = row.ResultRemark,
                                                    MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                                    Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                                    Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                                    hasDocuments = (from x in entities.BM_FileData where x.AgendaMappingId == row.MeetingAgendaMappingID && x.IsDeleted == false select x).Any(),
                                                }).ToList();

                    return getMeetingwiseAgenda;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        //END

        // For AgendawithVotingNotingDirector 23 Jul 2021
        public static List<MeetingAgendaSummary> GetAgendaMeetingwiseDirector(long meetingId, int userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    long participantID = 0;
                    long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == userID select dir.Id).FirstOrDefault();

                    if (directorID > 0)
                    {
                        participantID = (from par in entities.BM_MeetingParticipant
                                         where par.Director_Id == directorID
                                         && par.Meeting_ID == meetingId
                                         && par.IsDeleted == false
                                         select par.MeetingParticipantId).FirstOrDefault();
                    }

                    var getMeetingwiseAgenda = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, null)
                                                select new MeetingAgendaSummary
                                                {
                                                    AgendaId = row.BM_AgendaMasterId,
                                                    ParticipantId = participantID,
                                                    AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                                    MeetingId = row.MeetingID,
                                                    AgendaFormate = row.AgendaItemText,
                                                    //AgendaFormate = row.AgendaItemHeading,
                                                    MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                                    AgendaResponseResult = row.ResultRemark,
                                                    MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                                    Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                                    Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                                    hasDocuments = (from x in entities.BM_FileData where x.AgendaMappingId == row.MeetingAgendaMappingID && x.IsDeleted == false select x).Any(),
                                                }).ToList();

                    return getMeetingwiseAgenda;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        //END

        // For Agenda Response Chart Data 23 Jul 2021 
        public static List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long MeetingId, long AgendaId, long MappingId, int UserId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var lstResponse = GetCircularAgendaResponse(MeetingId, AgendaId, MappingId);

                    var getMeetingwiseAgenda = (from row in entities.BM_SP_MeetingAgendaResponseChartData(MeetingId, AgendaId, MappingId)
                                                select new AvailabilityResponseChartData_ResultVM
                                                {
                                                    Category = row.Category,
                                                    MappingId = MappingId,
                                                    Response_Count = row.Response_Count,
                                                    Color = row.Color,
                                                    MeetingId = MeetingId,
                                                    lstParticiapants = lstResponse.Where(k => k.Category == row.Category).ToList()
                                                }).ToList();

                    return getMeetingwiseAgenda;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
        private static List<AvailabilityResponseByAvailabilityId_ResultVM> GetCircularAgendaResponse(long meetingId, long agendaId, long MappingId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = (from row in entities.BM_SP_MeetingAgendaResponsesCircular(meetingId, agendaId, MappingId)
                              select new AvailabilityResponseByAvailabilityId_ResultVM
                              {
                                  ParticipantName = row.ParticipantName,
                                  Email = row.EmailId_Official,
                                  Responses = row.Response,
                                  Category = row.Category
                              }).ToList();
                return result;
            }

        }
        //End 

        // For To get the Interested Party 
        public static List<MeetingAgendaInterestedPartyVM> GetAll(long meetingAgendaMappingID, int UserId)
        {

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var result = entities.BM_MeetingAgendaInterestedParty.Where(template => template.MeetingAgendaMappingID == meetingAgendaMappingID && template.IsDeleted == false).Select(template => new MeetingAgendaInterestedPartyVM
                    {
                        InterestedPartyID = template.InterestedPartyID,
                        MeetingAgendaMappingID = template.MeetingAgendaMappingID,
                        Director_Id = template.Director_Id,

                        Category = new DirectorMasterListVM()
                        {
                            ID = template.Director_Id,
                            Name = entities.BM_DirectorMaster.Where(k => k.Id == template.Director_Id && k.Is_Deleted == false).Select(k => k.FirstName + " " + k.LastName).FirstOrDefault()
                        }
                    }).ToList();
                    return result;

                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
            //

        }


        // API for Historical File 11 nov 2021
        public static List<AgendaDocumentVM> GetHistoricalMeetingDocument(int? customerID, int userID, long historicalID,string doctype)
        {
            List<AgendaDocumentVM> _objDocument = new List<AgendaDocumentVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDocument = (from row in entities.BM_HistoricalData
                                    join file in entities.BM_FileData on row.ID equals (int)file.HistoricalDtataID
                                    where row.ID == historicalID && file.DocType == doctype && file.IsDeleted == false
                                    select new AgendaDocumentVM
                                    {
                                        ID = file.Id,
                                        FileName = file.FileName
                                    }
                                    ).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objDocument;
        }
        //End 

        // API for Agenda List for Historical meeting
        public static List<MeetingAgendaSummaryVM> GetAgendaItemForConcludedMeeting(long HistoricalID)
        {
            //var result = new List<MeetingAgendaSummary>();

            List<MeetingAgendaSummaryVM> result = new List<MeetingAgendaSummaryVM>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_HistoricalDataAgenda
                              where row.HistoricalId == HistoricalID && row.IsDeleted == false
                              select new MeetingAgendaSummaryVM
                              {
                                  HMappingId = row.Id,
                                  HId = row.HistoricalId,
                                  SrNo = row.SrNo,
                                  AgendaFormate = row.AgendaItemText,
                                  Result = row.Result,
                                  ParticipantId = 0,
                                  MeetingId = 0,
                                  MeetingAgendaMappingId = 0,
                                  t = row.SrNo == null ? (int)row.Id : row.SrNo
                              }).OrderBy(k => k.t).ToList();
                }

                   
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        // 


    }
}