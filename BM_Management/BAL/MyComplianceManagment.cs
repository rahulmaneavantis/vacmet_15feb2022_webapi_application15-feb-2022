﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using System.Reflection;

namespace AppWebApplication.BM_Management.BAL
{
    public class MyComplianceManagment
    {
        public static List<MyComplianceListVM> GetDirectorCompliances(int? customerID, int userId, int roleId, int statusId)
        {
            List<MyComplianceListVM> _objComplianceList = new List<MyComplianceListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objComplianceList = (from row in entities.BM_SP_DirectorMyCompliance(userId,customerID,null)

                                          select new MyComplianceListVM
                                          {
                                              MappingType = row.MappingType,
                                              EntityId = row.EntityId,
                                              CompanyName = row.CompanyName,
                                              DirectorId = row.DirectorId,
                                              DirectorName = row.DirectorName,
                                              MeetingID = row.MeetingID,
                                              MeetingTitle = row.MeetingTitle,
                                              //ComplianceID=row.
                                              ShortForm = row.ShortForm,
                                              ShortDescription = row.ShortDescription,
                                              Description = row.Description,
                                              RoleID = row.RoleID,
                                              ScheduleOnID = row.ScheduleOnID,
                                              ScheduleOn = row.ScheduleOn,
                                              ScheduleOnTime = row.ScheduleOnTime,
                                              StatusName = row.ComplianceStatus,
                                              RequiredForms = row.RequiredForms,
                                              Frequency = row.Frequency,
                                              Period = row.ForMonth,
                                          }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objComplianceList;
        }

        public static List<BM_SP_DirectorComplianceStatusReport_Result> GetDirectorComplianceStatusreport(int? customerID, int userId, int statusId, int roleID)
        {
            List<BM_SP_DirectorComplianceStatusReport_Result> _objcomplianceStatusReport = new List<BM_SP_DirectorComplianceStatusReport_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objcomplianceStatusReport = entities.BM_SP_DirectorComplianceStatusReport(userId, customerID, statusId, 2).ToList();
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objcomplianceStatusReport;
        }

        public static List<CompTypeVM> GetComplianceType(string flag)
        {
            List<CompTypeVM> _objType = new List<CompTypeVM>();
            try
            {
                if (flag == "OC")
                {
                    _objType = new List<CompTypeVM>
                {
                    new CompTypeVM {Id=-1,Type="Type-All" },
                    new CompTypeVM {Id=1,Type="Meeting" },
                    new CompTypeVM {Id=2,Type="Statutory"},
                    new CompTypeVM {Id=3,Type="Self" }
                };
                }
                else if(flag == "WC")
                {
                    _objType = new List<CompTypeVM>
                {
                    new CompTypeVM {Id=-1,Type="Type-All" },
                    new CompTypeVM {Id=1,Type="Meeting" },
                    new CompTypeVM {Id=2,Type="Statutory"},
                    new CompTypeVM {Id=3,Type="Director" }
                };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objType;
        }

        public static List<StatusVM> GetComplianceStatus()
        {
            List<StatusVM> _objstatus = new List<StatusVM>();
            try
            {
                _objstatus = new List<StatusVM>
                {
                    new StatusVM {ID=-1,Status="Select ALL" },
                    new StatusVM {ID=1,Status="Upcoming" },
                    new StatusVM {ID=2,Status="Overdue"},
                    new StatusVM {ID=3,Status="Pending for Review" },
                    new StatusVM {ID=4,Status="Closed-Timely"},
                    new StatusVM {ID=5,Status="Closed-Delayed" }
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objstatus;
        }

        public static List<BM_SP_DirectorAttendanceReport_Result> GetDirectorAttendenceReport(int? customerID, int userId)
        {
            List<BM_SP_DirectorAttendanceReport_Result> objattendencereport = new List<BM_SP_DirectorAttendanceReport_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objattendencereport = entities.BM_SP_DirectorAttendanceReport(userId, customerID).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objattendencereport;
        }

        public static List<BM_SP_DirectorComplianceDocuments_Result> GetComplianceDocument(int? customerID, int UserID, int RoleID, int statusId)
        {
            List<BM_SP_DirectorComplianceDocuments_Result> _objComplianceDoc = new List<BM_SP_DirectorComplianceDocuments_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objComplianceDoc = (from x in entities.BM_SP_DirectorComplianceDocuments(UserID, customerID, statusId, RoleID, 2) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objComplianceDoc;
        }

        public static List<F_YearVM> GetFYear(int UserId, int customerID, int IsforHhistorical)
        {
            List<F_YearVM> _objFyear = new List<F_YearVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //_objFyear = (from x in entities.BM_SP_DirectorComplianceStatusReport(UserId, customerID, statusId, 2)
                //             select new F_YearVM

                //             {
                //                 FY = x.FY,
                //                 FYText = x.FYText
                //             }).ToList();
                //if (_objFyear.Count > 0)
                //{
                //    _objFyear = _objFyear.GroupBy(s => new { s.FY, s.FYText })
                //              .Select(g => new F_YearVM
                //              {
                //                  FY = g.Key.FY,
                //                  FYText = g.Key.FYText,

                //              }).ToList();
                //}
                _objFyear = (from x in entities.BM_SP_GetFYListForDirector(customerID, UserId, IsforHhistorical)
                             select new F_YearVM

                             {
                                 FY = x.Id,
                                 FYText = x.FYText
                             }).ToList();
               
                if (_objFyear.Count > 0)
                {
                    _objFyear.Insert(0, new F_YearVM { FY = -1, FYText = "All" });
                }
            }
            return _objFyear;
        }

        public static List<MeetingVM> GetMeeting(int UserId, int customerID, int statusId)
        {
            List<MeetingVM> _objMeeting = new List<MeetingVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objMeeting = (from x in entities.BM_SP_DirectorComplianceStatusReport(UserId, customerID, statusId, 2)
                               select new MeetingVM

                               {
                                   MeetingId = x.MeetingID,
                                   MeetingTitle = x.MeetingTitle
                               }).ToList();
                if (_objMeeting.Count > 0)
                {
                    _objMeeting = _objMeeting.GroupBy(s => new { s.MeetingId, s.MeetingTitle })
                              .Select(g => new MeetingVM
                              {
                                  MeetingId = g.Key.MeetingId,
                                  MeetingTitle = g.Key.MeetingTitle,

                              }).ToList();
                }
                if (_objMeeting.Count > 0)
                {
                    _objMeeting.Insert(0, new MeetingVM { MeetingId = -1, MeetingTitle = "All" });
                }
            }
            return _objMeeting;
        }

        public static List<F_YearVM> GetFYearForDocument(int UserId, int customerID, int statusId, int RoleID)
        {
            List<F_YearVM> _objFyear = new List<F_YearVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objFyear = (from x in entities.BM_SP_DirectorComplianceDocuments(UserId, customerID, statusId, RoleID, 2)
                             select new F_YearVM

                             {
                                 FY = x.FY,
                                 FYText = x.FYText
                             }).ToList();
                if (_objFyear.Count > 0)
                {
                    _objFyear = _objFyear.GroupBy(s => new { s.FY, s.FYText })
                              .Select(g => new F_YearVM
                              {
                                  FY = g.Key.FY,
                                  FYText = g.Key.FYText,

                              }).ToList();
                }
                if (_objFyear.Count > 0)
                {
                    _objFyear.Insert(0, new F_YearVM { FY = -1, FYText = "All" });
                }
            }
            return _objFyear;
        }

        public static List<MeetingVM> GetMeetingForDocument(int UserId, int customerID, int statusId, int RoleID)
        {
            List<MeetingVM> _objMeeting = new List<MeetingVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objMeeting = (from x in entities.BM_SP_DirectorComplianceDocuments(UserId, customerID, statusId, RoleID, 2)
                               select new MeetingVM

                               {
                                   MeetingId = x.MeetingID,
                                   MeetingTitle = x.MeetingTitle
                               }).ToList();
                if (_objMeeting.Count > 0)
                {
                    _objMeeting = _objMeeting.GroupBy(s => new { s.MeetingId, s.MeetingTitle })
                              .Select(g => new MeetingVM
                              {
                                  MeetingId = g.Key.MeetingId,
                                  MeetingTitle = g.Key.MeetingTitle,

                              }).ToList();
                }
                if (_objMeeting.Count > 0)
                {
                    _objMeeting.Insert(0, new MeetingVM { MeetingId = -1, MeetingTitle = "All" });
                }
            }
            return _objMeeting;
        }

        public static List<ActVM> GetAct(int UserId, int CustomerId, int StatusId)
        {
            List<ActVM> _objAct = new List<ActVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objAct = (from x in entities.BM_SP_DirectorComplianceStatusReport(UserId, CustomerId, StatusId, 2)
                           select new ActVM

                           {
                               ActId = x.ActID,
                               ActName = x.ActName
                           }).ToList();
                if (_objAct.Count > 0)
                {
                    _objAct = _objAct.GroupBy(s => new { s.ActId, s.ActName })
                            .Select(g => new ActVM
                            {
                                ActId = g.Key.ActId,
                                ActName = g.Key.ActName,

                            }).ToList();
                }
                if (_objAct.Count > 0)
                {
                    _objAct.Insert(0, new ActVM { ActId = -1, ActName = "All" });
                }

            }
            return _objAct;
        }


        public static List<MyCompliance_Director_ResultVM> MyCompliances_Director(int customerId, int userId, int roleId, string FY_Year)
        {
            List<MyCompliance_Director_ResultVM> _objDirectorsComp = new List<MyCompliance_Director_ResultVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    _objDirectorsComp=(from row in entities.BM_SP_MyCompliance_Director(userId, customerId, 1, roleId, 2, FY_Year)
                            select new MyCompliance_Director_ResultVM
                            {
                                EntityId = row.EntityId,
                                CompanyName = row.CompanyName,
                                DirectorId = row.DirectorId,
                                DirectorName = row.DirectorName,
                                ScheduleOnID = row.ScheduleOnID,

                                //RoleID = row.RoleID,
                                MappingType = row.MappingType,

                                MeetingID = row.MeetingID,
                                MeetingTitle = row.MeetingTitle,

                                ShortForm = row.ShortForm,
                                ShortDescription = row.ShortDescription,
                                Description = row.Description,
                                RequiredForms = row.RequiredForms,

                                ScheduleOn = row.ScheduleOn,
                                ScheduleOnInt = row.ScheduleOnInt,
                                ScheduleOnTime = row.ScheduleOnTime,
                                ComplianceStatus = row.ComplianceStatus,

                                Frequency = row.Frequency,
                                ForMonth = row.ForMonth,
                                ForPeriod = row.ForPeriod,
                                Risk = row.Risk

                            }).ToList();
                    if(_objDirectorsComp.Count>0)
                    {
                        _objDirectorsComp = _objDirectorsComp.Where(x => x.EntityId > 0).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
            return _objDirectorsComp;
        }

        public static List<RiskVM> Risk()
        {
            List<RiskVM> _objRisk = new List<RiskVM>();
            try
            {
                _objRisk = new List<RiskVM>
                {
                    new RiskVM {ID=-1,RiskType="ALL" },
                    new RiskVM {ID=1,RiskType="High" },
                    new RiskVM {ID=2,RiskType="Medium"},
                    new RiskVM {ID=3,RiskType="Low" },
                 
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objRisk;
        }

        public static List<RoleVM> GetRole()
        {
            List<RoleVM> _objRisk = new List<RoleVM>();
            try
            {
                _objRisk = new List<RoleVM>
                {
                    new RoleVM {Id=-1,RoleName="ALL" },
                    new RoleVM {Id=3,RoleName="Performer" },
                    new RoleVM {Id=4,RoleName="Reviewer"},
                 
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objRisk;
        }
    }
}