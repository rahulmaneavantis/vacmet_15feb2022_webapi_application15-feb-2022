﻿using AppWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace AppWebApplication.BM_Management.BAL
{
    public class UserManagemet
    {
        public static string key = "avantis";
        public static string getDecryptedText(string text, string type)
        {
            string DecryptedString = string.Empty;
            text = text.Replace(" ", "+");
            var encryptedBytes = Convert.FromBase64String(text);
            if (type.Equals("Android"))
            {
                try
                {
                    DecryptedString = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
            }
            else if (type.Equals("IOS"))
            {
                try
                {
                    DecryptedString = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
            }

            //try
            //{
            //    DecryptedString = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
            //}
            //catch { }

            return DecryptedString;
        }
    }
}