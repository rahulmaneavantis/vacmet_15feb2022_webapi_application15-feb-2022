﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;

namespace AppWebApplication.BM_Management.BAL
{
    public class AuthorizationManagment
    {
        public static List<AuthorizationVM> GetPageAuthorizationDetails(int? customerID, int UserId, string Role)
        {
            List<AuthorizationVM> _pageAutenUserList = new List<AuthorizationVM>();
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    _pageAutenUserList = (from row in entities.BM_SP_GetUserWisePageAuthorization(UserId, 0, 0, 0)
                                          where row.Role == Role
                                          select new AuthorizationVM

                                          {
                                              UserId = UserId,
                                              parentId = row.ParentId,
                                              Id = row.ID,
                                              PageName = row.Name,
                                              Editview = row.CanEdit,
                                              DeleteView = row.CanDelete,
                                              Addview = row.CanAdd,
                                              Pageview = (bool)row.CanView,
                                          }).ToList();

                
                }
            }
            catch (Exception ex)
            {

            }

            return _pageAutenUserList;
        }
             
            
    }
}