﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.BM_Management.DAL;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using System.Reflection;
using AppWebApplication.Data;

namespace AppWebApplication.BM_Management.BAL
{
    public class RegisterManagment
    {
        public static List<RegisterVM> GetRegisters(int? customerID, int UserId, int entityID)
        {
            List<RegisterVM> _objRegister = new List<RegisterVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int EntityType = (from row in entities.BM_EntityMaster where row.Id == entityID select row.Entity_Type).FirstOrDefault();
                    if (EntityType > 0)
                    {
                        _objRegister = (from RTM in entities.BM_RegisterTypeMapping
                                        join R in entities.BM_Register on RTM.RegisterId equals R.Id
                                        where RTM.EntityTypeId == EntityType && R.isActive == true && RTM.IsActive==true
                                        select new RegisterVM
                                        {

                                            Id = R.Id,
                                            Name = R.Name
                                        }).ToList();                       


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objRegister;
        }

        public static List<DirectorListVM> GetDirectorList(int? customerID, int UserId, int entityID)
        {
            List<DirectorListVM> _objDirectorList = new List<DirectorListVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objDirectorList = (from director in entities.BM_DirectorMaster
                                        join DOI in entities.BM_Directors_DetailsOfInterest on director.Id equals DOI.Director_Id
                                        join NOI in entities.BM_Directors_NatureOfInterest on DOI.NatureOfInterest equals NOI.Id
                                        //join designation in entities.BM_Directors_Designation on row.IfDirector equals designation.Id
                                        where DOI.EntityId == entityID
                                        && director.Customer_Id == customerID
                                        && NOI.IsDirector == true
                                           //&& (row.NatureOfInterest == 2 || row.NatureOfInterest == 9 || row.NatureOfInterest == 10)
                                           && director.Is_Deleted == false
                                           && DOI.IsDeleted == false
                                           && DOI.IsActive == true
                                        select new DirectorListVM
                                        {
                                            ID = DOI.Director_Id,
                                            Name = (director.Salutation == null ? "" : director.Salutation + " ") + director.FirstName + (director.LastName == null ? "" : " " + director.LastName)
                                        }).ToList();
                    if (_objDirectorList.Count > 0)
                    {
                        _objDirectorList.Insert(0, new DirectorListVM { ID = 0, Name = "All" });
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objDirectorList;
        }


        public static List<DirectorListVM> GetManagementEntityWise(int customerID, int entityID, bool showAllOption)
        {
            var result = new List<DirectorListVM>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                result = (from director in entities.BM_SP_GetManagementEntityWise(entityID, customerID)
                          orderby director.FullName
                          select new DirectorListVM
                          {
                              //ID = director.Director_Id,
                              ID = director.DetailsOfInterestID,
                              Name = director.FullName + " - [" + director.DesignationName + "]"
                          }).ToList();

                if (showAllOption)
                    result.Insert(0, new DirectorListVM { ID = 0, Name = "All" });
            }

            return result;
        }

        public static EntityRegisterVM getEntityDetails(int entityID)
        {
            EntityRegisterVM _objEntity = new EntityRegisterVM();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _objEntity = (from row in entities.BM_EntityMaster
                                  where row.Id == entityID 
                                  && row.Is_Deleted==false
                                  select new EntityRegisterVM
                                  {
                                      EntityId = row.Id,
                                      EntityName = row.CompanyName,
                                      Address = row.Regi_Address_Line1 + " " + row.Regi_Address_Line2

                                  }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objEntity;
        }

        public static List<DirectorsRegisterVM> getDirectorDetails(int entityID, int directorId, int customerId)
        {
            List<DirectorsRegisterVM> obj = new List<DirectorsRegisterVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    obj = (from row in entities.BM_SP_DirectorDetails(entityID, directorId, customerId)
                                                         select new DirectorsRegisterVM
                                                         {
                                                             DirectorId = row.DirectorId,
                                                             DIN = row.DIN,
                                                             FullName = row.DirectorFullName,
                                                             FatherName = row.FatherName,
                                                             MotherName = row.Mother,
                                                             SpouseName = (from r in entities.BM_Directors_Relatives where r.Director_Id == row.DirectorId && r.Relation == "Spouse" select r.Name).FirstOrDefault(),
                                                             DateofBirth = row.DOB,
                                                             PresentAddress = row.PresentAddress,
                                                             ParmanentAddress = row.PermanentAddress,
                                                             Nationalality = "Indian",
                                                             Occupation = row.Occupation,
                                                             DateofCessation = row.cessiondateandReasons,
                                                             PAN = row.PAN,
                                                             DateofBoardResolution = row.DateOfResolution,
                                                             DateofAppointmentandReappointment = row.DateOfAppointment,
                                                         }).ToList();
                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public static List<DirectorSecurityVM> getDirectorSecurityDetails(int directorId, int customerId, int entityID)
        {
            List<DirectorSecurityVM> objsecurityDetals = new List<DirectorSecurityVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objsecurityDetals = (from row in entities.BM_SP_DirectorSecuritiesDetails(directorId, customerId, entityID)
                                                 select new DirectorSecurityVM
                                                 {
                                                     CIN = row.CIN,
                                                     CompanyName = row.CompanyName,
                                                     NoofSecurities = row.securities_No,
                                                     DescriptionofSecurity = row.Dis_of_securities,
                                                     NominalvalueofSecurities = row.Nominam_valSecurities,
                                                     DateofAccusation = row.acquisition_Date != null ? Convert.ToDateTime(row.acquisition_Date).ToString("dd/MM/yyyy") : "",
                                                     PricePaidforaccusation = row.acquisition_pricePaid,
                                                     otherPricePaidforaccusation = row.Other_ConsiPaid_acquisition,
                                                     DateofDesposal = row.Date_disposal != null ? Convert.ToDateTime(row.Date_disposal).ToString("dd/MM/yyyy") : "",
                                                     Pricerecivefordisposal = row.Price_Received_disposal,
                                                     OtherPriceforDisposal = row.otherconsideration_disposal_RescPrice,
                                                     CumaltiveBalance = row.Cbal_No_securities_afterTransaction,
                                                     ModeofAccusation = row.Mode_of_acquisitionSecurities,
                                                     ModeOfHolding = row.DematerializedorPhysical,
                                                     SecurityHasbeenpledge = row.Securities_encumbrance
                                                 }).ToList();


                    objsecurityDetals.ForEach(u => { (from y in entities.BM_SubEntityMapping where y.CIN == u.CIN select y).ToList(); });

                    if (objsecurityDetals.Count > 0)
                    {
                        objsecurityDetals.ForEach(u => { (from y in entities.BM_EntityMaster where y.CIN_LLPIN == u.CIN select y).ToList(); });

                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objsecurityDetals;
        }

        // for Drop down of Member List 
        public static List<VMShareholding> GetShareholdingMemberForRegister(int entityID,int? CustomerId)
        {
            List<VMShareholding> _objRegister = new List<VMShareholding>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getShareholdingData = (from row in entities.BM_SP_ShareHolderDetails(entityID, CustomerId)
                                               select new VMShareholding
                                               {
                                                   Id = row.Id,
                                                   Member_Name = row.Member_Name,
                                               }).ToList();

                    getShareholdingData.Insert(0, new VMShareholding { Id = 0, Member_Name = "All" });
                    return getShareholdingData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objRegister;
        }
        //

        // for Drop down of Charge List 
        public static List<ChargeHeaderVM> GetAllChargeIndexDropdown(int entityID)
        {
            List<ChargeHeaderVM> _objRegister = new List<ChargeHeaderVM>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getChargeData = (from row in entities.BM_ChargeHeader
                                               where row.EntityId == entityID && row.IsActive == true && row.IsDeleted == false
                                               select new ChargeHeaderVM
                                               {
                                                   ChargeHeaderId = row.Id,
                                                   ChargeId = row.ChargeID,
                                                   CreationDate = row.CreationDate,
                                                   ModificationDate = row.ModificationDate,
                                                   SatisficationDate = row.SatisficationDate,
                                                   ChargeAmount = row.Charge_Amount,
                                                   ChargeHolder = row.Names_addresses_chargeholder,
                                                   StatusId = row.StatusId,
                                               }).ToList();

                    getChargeData.Insert(0, new ChargeHeaderVM { ChargeHeaderId = 0, ChargeId = "All" });
                    return getChargeData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objRegister;
        }
        //

        // API for Member Register View 5 Aug 2021
        public static List<MemberRegister> GetMemberofRegisterDetails(long entityID, int memberId, int customerID)
        {
           
            List<MemberRegister> obj = new List<MemberRegister>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    obj = (from row in entities.BM_SP_RegisterofMember(memberId, entityID, customerID)
                           select new MemberRegister
                           {
                               MemberId = (int)row.Id,
                               EntityName = row.CompanyName,
                               EntityAddress = row.CompanyAddress,
                               FolioNumber = row.Follio_No,
                               ClassofShares = row.ClassofShares,
                               Nominalvaluepershares = row.NominaVal_per_Shares,
                               TotalSharesHeld = row.Tot_SharesHeld,
                               MemberName = row.MemberName,
                               JointHolderName = row.JointHolder_Name,
                               Address = row.bodycorporate_Address,
                               EmailId = row.Email_Id,
                               CIN = row.CIN,
                               UIN = "",
                               Father_Mother_SpouseName = row.Father_Mother_Spouse_Name,
                               Status = row.Status,
                               PAN = row.PAN != null ? row.PAN.ToUpper() : "",
                               GuardianName = row.Guardian_Name,
                               DateofBirth = row.DOB_minor != null ? Convert.ToDateTime(row.DOB_minor).ToString("dd-MM-yyyy") : "",
                               DateofBecomingMember = row.becomingmember_Date != null ? Convert.ToDateTime(row.becomingmember_Date).ToString("dd-MM-yyyy") : "",
                               Dateunder89 = row.declaration_Date_under89 != null ? Convert.ToDateTime(row.declaration_Date_under89).ToString("dd-MM-yyyy") : "",
                               BenificialNameandAddress = row.Name_and_Address_ben,
                               DateofNomination = row.Nomination_Date_Receipt != null ? Convert.ToDateTime(row.Nomination_Date_Receipt).ToString("dd-MM-yyyy") : "",
                               NameandAddressofNomminee = row.NomineeName_Address,
                               sharesinAbayes = row.shares_abeyance,
                               DateofCessation = row.Cessation_Membership_Date != null ? Convert.ToDateTime(row.Cessation_Membership_Date).ToString("dd-MM-yyyy") : "",

                           }).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public static List<MemberSecurities> GetMemberSecurities(long entityID, int memberId, int customerID)
        {
            List<MemberSecurities> obj = new List<MemberSecurities>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    obj = (from row in entities.BM_SP_MemberSecurityDetails(entityID, memberId, customerID)
                           select new MemberSecurities
                           {
                               AllotmentNo = row.AllotmentNo,
                               DateofAllotment = row.allotmentDate != null ? Convert.ToDateTime(row.allotmentDate).ToString("dd-MM-yyyy") : "",
                               NumberofShares = row.NumberOfShares,
                               DistinctiveNoofShares_From = row.DistinctiveFrom,
                               DistinctiveNoofShares_To = row.DistinctiveTo,
                               TransferorFolioNo = row.FolioOfTransferor,
                               NameOfTransferror = row.NameOfTransferor,
                               DateofIssues = row.DateOfIssued != null ? Convert.ToDateTime(row.DateOfIssued).ToString("dd-MM-yyyy") : "",
                               CertificateNumber = row.CertificateNo,
                               LockinPeriod = row.LockInPeriod,
                               AmountPayable = row.PayableAmount,
                               AmountPaid = row.PaidOrToBePaidAmount,
                               AmountDue = row.DueAmount,
                               DescofConsidration = row.Thereof,
                               DateofTransfer = row.DateOfTransfer != null ? Convert.ToDateTime(row.DateOfTransfer).ToString("dd-MM-yyyy") : "",
                               NoofSharesTransfered = row.NoOfShareTransfered,
                               DistinctiveNo_Form = row.DistinctiveFrom1,
                               DistinctiveNo_To = row.DistinctiveTo1,
                               NameofTransfaree = row.NameOfTransferee,
                               BalanceofSharesHeld = row.BalanceShare,
                               Remark = row.Remarks,
                               Authentication = row.Authentication_
                           }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        //

        //API for Charge Register View 5 Aug 2021
        public static List<ChargeRegister> GetChargeRegisterDetails(int ChargeHeaderId, int entityID, int customerID)
        {

            List<ChargeRegister> obj = new List<ChargeRegister>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    obj = (from row in entities.BM_SP_RegisterofCharge( ChargeHeaderId, entityID, customerID)
                           select new ChargeRegister
                           {
                               Charge_Id = row.Charge_Id,
                               Entity_Id = row.Entity_Id,
                               ChargeTypeId = row.Charge_TypeId,
                               ChargeIdMannual = row.ChargeIdMannual,
                               Charge_TypeDesc = row.Charge_TypeDesc,
                               ChargeAmount = row.Charge_Amount,
                               CreationDate = row.Charge_CreationDate,
                               RegistrationChargeCreateDate = row.Registration_ChargeCreateDate,
                               ModificationDate = row.Charge_Modification_Date,
                               RegistrationModificationDate = row.Registration_ChargeModificationDate,
                               SatisfactionDate = row.Satisfaction_Date,
                               InFavour = row.In_Favour,

                               ShortDescPropertyCharged = row.ShortDesc_PropertyCharged,
                               Namesaddresseschargeholder = row.Names_addresses_chargeholder,
                               TermsconditionsOfcharge = row.Terms_conditionsOfcharge,
                               Descinstrument = row.Desc_instrument,

                               Descinstrumentchargemodify = row.Desc_instrument_chargemodify,
                               Particularsmodification = row.Particulars_modification,
                               RegistrationsatisfactionDate = row.Registration_satisfactionDate,
                               FactsDelaycondonationDate = row.Facts_Delaycondonation_Date,
                               Reasonsdelayfiling = row.Reasons_delay_filing,
                               Status = row.Status
                           }).ToList();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        //

    }
}