﻿//using AppWebApplication.Data;
//using AppWebApplication.Models;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Reflection;
//using System.Web.Http;
//using static AppWebApplication.BM_Management.DAL.AnnotationVM;
//using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;

//namespace AppWebApplication.BM_Management.Controller
//{
//    public class AnnotationController : ApiController
//    {

//        public HttpResponseMessage Get()
//        {
           
//            string agendaMinutesCommentsId = GetId(this.Request);
//            if (!string.IsNullOrEmpty(agendaMinutesCommentsId))
//            {
//                long id = 0;
//                long.TryParse(agendaMinutesCommentsId, out id);
//                if (id > 0)
//                {
//                    string xfdfData = GetAnnote(id);
//                    return new HttpResponseMessage() { Content = new StringContent(xfdfData) };
//                }
//                else
//                {
//                    return new HttpResponseMessage(HttpStatusCode.NoContent);
//                }

//            }
//            else
//            {
//                return new HttpResponseMessage(HttpStatusCode.NoContent);
//            }
//        }

//        // Handle POST request sent to '/api/annotation'
//        // POST api/<controller>
//        //public async System.Threading.Tasks.Task Post()
//        //{
//        //    string filePath = GetFilePath(this.Request);
//        //    string xfdfData = await Request.Content.ReadAsStringAsync();
//        //    File.WriteAllText(filePath, xfdfData);
//        //}
//        public async System.Threading.Tasks.Task Post()
//        {
//            long meetingId = 0;
//            long meetingParticipantId = 0;
//            var docType = "";
//            var pairs = this.Request.GetQueryNameValuePairs();
//            foreach (KeyValuePair<string, string> kvp in pairs)
//            {
//                switch (kvp.Key)
//                {
//                    case "MeetingId":
//                        long.TryParse(kvp.Value, out meetingId);
//                        break;
//                    case "DocType":
//                        docType = kvp.Value;
//                        break;
//                    case "MeetingParticipantId":
//                        long.TryParse(kvp.Value, out meetingParticipantId);
//                        break;
//                    default:
//                        break;
//                }
//            }

//            var model = new AgendaMinutesCommentsVM()
//            {
//                MeetingId = meetingId,
//                MeetingParticipantId = meetingParticipantId,
//                DocType = docType
//            };

//            string xfdfData = await Request.Content.ReadAsStringAsync();
//            model.CommentData = xfdfData;

//            if (model.MeetingId > 0)
//            {
//              //  int userId = System.Convert.ToInt32(Common.AuthenticationHelper.UserID);
//              //  SaveAnnotation(model, userId);
//            }
//        }

//        private string GetFilePath(HttpRequestMessage request)
//        {
//            string id = GetId(request);
//            string fileName = (id != null) ? id : "default";
//            return System.Web.HttpContext.Current.Server.MapPath(string.Format("~\\{0}.xfdf", fileName));
//        }

//        private string GetId(HttpRequestMessage request)
//        {
//            var pairs = Request.GetQueryNameValuePairs();
//            string agendaMinutesCommentsId = null;
//            foreach (KeyValuePair<string, string> kvp in pairs)
//            {
//                if (kvp.Key == "id")
//                {
//                    agendaMinutesCommentsId = kvp.Value;
//                }
//            }
//            return agendaMinutesCommentsId;
//        }

//        public AgendaMinutesCommentsVM GetAnnotation(long meetingId, long meetingParticipantId, string docType, int userId, string role, int customerId)
//        {
//            var result = new AgendaMinutesCommentsVM();
//            try
//            {
//                using (ComplianceDBEntities entities = new ComplianceDBEntities())
//                {
//                    result = (from row in entities.BM_MeetingAgendaMinutesComments
//                              where row.MeetingId == meetingId && row.DocType == docType &&
//                              row.IsActive == true
//                              select new AgendaMinutesCommentsVM
//                              {
//                                  AgendaMinutesCommentsId = row.Id,
//                                  MeetingId = row.MeetingId,
//                                  DocType = row.DocType,
//                                  FilePath = row.FilePath,
//                                  CommentData = row.CommentData,
//                                  MeetingParticipantId = meetingParticipantId
//                              }).FirstOrDefault();

//                    if (result == null)
//                    {
//                        result = new AgendaMinutesCommentsVM()
//                        {
//                            MeetingId = meetingId,
//                            DocType = docType,
//                            CommentData = "",
//                            MeetingParticipantId = meetingParticipantId,
//                        };
//                        var fileDetails = (from row in entities.BM_Meetings
//                                           where row.MeetingID == meetingId && row.IsDeleted == false
//                                           select new
//                                           {
//                                               row.AgendaFilePath,
//                                               row.EnType
//                                           }).FirstOrDefault();
//                        if (fileDetails != null)
//                        {
//                            result.FilePath = fileDetails.AgendaFilePath;

//                            if (!string.IsNullOrEmpty(result.FilePath))
//                            {
//                                var _obj = new BM_MeetingAgendaMinutesComments()
//                                {
//                                    MeetingId = meetingId,
//                                    DocType = docType,
//                                    FilePath = result.FilePath,
//                                    EnType = fileDetails.EnType,
//                                    CommentData = "",
//                                    IsActive = true,
//                                    CreatedBy = userId,
//                                    CreatedOn = DateTime.Now
//                                };
//                                entities.BM_MeetingAgendaMinutesComments.Add(_obj);
//                                entities.SaveChanges();

//                                result.AgendaMinutesCommentsId = _obj.Id;
//                            }
//                        }
//                    }

//                    if (result != null)
//                    {
//                        if (role == "HDCS" || role == "CS")
//                        {
//                            result.IsCS = true;
//                        }
//                        if (!string.IsNullOrEmpty(result.FilePath))
//                        {
//                            var fileName = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
//                            if (System.IO.File.Exists(fileName))
//                            {
//                                #region Generate temp File
//                                string DateFolder = "~/TempFiles" + "/" + Convert.ToString(DateTime.Now.ToString("ddMMyy"));
//                                if (!System.IO.Directory.Exists(DateFolder))
//                                {
//                                    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
//                                }
//                                string User = userId + "" + customerId + "" + Convert.ToString(DateTime.Now.ToString("HHmmss"));
//                                string FileName = System.Web.Hosting.HostingEnvironment.MapPath(DateFolder + "/" + User + Path.GetExtension(fileName));

//                                FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite);
//                                BinaryWriter bw = new BinaryWriter(fs);
//                                if (true)
//                                {
//                                    //bw.Write(CryptographyHandler.AESDecrypt(objIFileData_Service.ReadDocFiles(fileName)));
//                                }
//                                bw.Close();

//                                result.DocURL = DateFolder + "/" + User + Path.GetExtension(fileName);
//                                #endregion

                              
//                            }
//                            else
//                            {
                              
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
//            }
//            return result;
//        }

//        public Message SaveAnnotation(AgendaMinutesCommentsVM obj, int userId)
//        {
//            var result = new Message();
//            try
//            {
//                using (ComplianceDBEntities entities = new ComplianceDBEntities())
//                {
//                    var _obj = (from row in entities.BM_MeetingAgendaMinutesComments
//                                where row.MeetingId == obj.MeetingId && row.DocType == obj.DocType &&
//                                row.IsActive == true
//                                select row).FirstOrDefault();
//                    if (_obj != null)
//                    {
//                        _obj.CommentData = obj.CommentData;
//                        _obj.UpdatedBy = userId;
//                        _obj.UpdatedOn = DateTime.Now;
//                        entities.SaveChanges();
//                    }
//                    else
//                    {
//                        _obj = new BM_MeetingAgendaMinutesComments();
//                        _obj.MeetingId = obj.MeetingId;
//                        _obj.DocType = obj.DocType;
//                        _obj.FilePath = obj.FilePath;
//                        _obj.CommentData = obj.CommentData;
//                        _obj.IsActive = true;
//                        _obj.CreatedBy = userId;
//                        _obj.CreatedOn = DateTime.Now;
//                        entities.BM_MeetingAgendaMinutesComments.Add(_obj);
//                        entities.SaveChanges();
//                    }

//                    #region Transaction
//                    if (_obj != null)
//                    {
//                        if (_obj.Id > 0)
//                        {
//                            var _objTransaction = new BM_MeetingAgendaMinutesCommentsTransaction();
//                            _objTransaction.AgendaMinutesCommentsId = _obj.Id;
//                            _objTransaction.MeetingParticipantId = obj.MeetingParticipantId;
//                            _objTransaction.CommentData = obj.CommentData;
//                            _objTransaction.CreatedBy = userId;
//                            _objTransaction.CreatedOn = DateTime.Now;
//                            entities.BM_MeetingAgendaMinutesCommentsTransaction.Add(_objTransaction);
//                            entities.SaveChanges();
//                        }
//                    }
//                    #endregion

//                    result.Success = true;
//                    result.Message = SecretarialConst.Messages.saveSuccess;
//                }
//            }
//            catch (Exception ex)
//            {
//                result.Error = true;
//                result.Message = SecretarialConst.Messages.serverError;
//                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
//            }
//            return result;
//        }

//        public string GetAnnote(long id)
//        {
//            var result = string.Empty;
//            try
//            {
//                using (ComplianceDBEntities entities = new ComplianceDBEntities())
//                {
//                    result = (from row in entities.BM_MeetingAgendaMinutesComments
//                              where row.Id == id && row.IsActive == true
//                              select row.CommentData).FirstOrDefault();
//                }
//            }
//            catch (Exception ex)
//            {
//                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
//            }
//            return result;
//        }
//    }
//}
