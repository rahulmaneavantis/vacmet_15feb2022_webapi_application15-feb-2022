﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/Filter")]
    public class FiltersController : ApiController
    {
     
        [Authorize]
        [Route("DirectorList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorListVM> _objDirectorList = new List<DirectorListVM>();
                int EntityID = 0;
               EntityID = _objdata.EntityId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objDirectorList = RegisterManagment.GetManagementEntityWise(Convert.ToInt32(user.CustomerID),EntityID,true);
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("ComplianceType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceType([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<CompTypeVM> _objType = new List<CompTypeVM>();
                int RoleId = 0;
                int statusId = -1;
                RoleId = _objdata.RoleID;
                if (_objdata.statusId > 0)
                    statusId = _objdata.statusId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objType = MyComplianceManagment.GetComplianceType(_objdata.Flag);

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objType).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("ComplianceStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Status([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<StatusVM> _objStatus = new List<StatusVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objStatus = MyComplianceManagment.GetComplianceStatus();

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objStatus).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Risk")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Risk([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<RiskVM> _objrisk = new List<RiskVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objrisk = MyComplianceManagment.Risk();

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objrisk).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("F_Year")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage F_Year([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<F_YearVM> _objFyear = new List<F_YearVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objFyear = MyComplianceManagment.GetFYear(Convert.ToInt32(user.ID),Convert.ToInt32(user.CustomerID),_objdata.IsforHhistorical);

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objFyear).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("Meeting")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Meeting([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingVM> _objMeeting = new List<MeetingVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objMeeting = MyComplianceManagment.GetMeeting(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), -1);

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objMeeting).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("F_YearForComplianceDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage F_YearForComplianceDocument([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<F_YearVM> _objFyear = new List<F_YearVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objFyear = MyComplianceManagment.GetFYearForDocument(Convert.ToInt32(user.CustomerID), Convert.ToInt32(user.ID),-1,4);

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objFyear).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("MeetingForComplianceDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MeetingForComplianceDocuments([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingVM> _objMeeting = new List<MeetingVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objMeeting = MyComplianceManagment.GetMeetingForDocument(Convert.ToInt32(user.CustomerID), Convert.ToInt32(user.ID), -1, 4);

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objMeeting).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Act")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Act([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<ActVM> _objAct = new List<ActVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = MyComplianceManagment.GetAct(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), -1);
                           
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Role")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Role([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<RoleVM> _objAct = new List<RoleVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = MyComplianceManagment.GetRole();
                            

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Entity")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Entity([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<EntityVM> _objAct = new List<EntityVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = CompanyProfileManagment.GetEntityDetails(Convert.ToInt32(user.ID),Convert.ToInt32(user.CustomerID));


                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("MeetingType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MeetingType([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingVM> _objMeetingType = new List<MeetingVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objMeetingType = CompanyProfileManagment.GetMeetingFilters(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objMeetingType).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

       
    }
}
