﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/MyCompliances")]
    public class MyCompliancesController : ApiController
    {



        [Authorize]
        [Route("DirectorCompliances")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorCompliances([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<MyComplianceListVM> _objDirectorsCompliance = new List<MyComplianceListVM>();



                int StatusId = 0;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        //RoleId = _objdata.RoleID;
                        StatusId = _objdata.statusId;
                        if (user != null)
                        {
                            _objDirectorsCompliance = MyComplianceManagment.GetDirectorCompliances(user.CustomerID, Convert.ToInt32(user.ID), 4, StatusId);
                            if (_objDirectorsCompliance.Count > 0)
                            {
                                if (_objdata.RoleID > 0)
                                {
                                    _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.RoleID == _objdata.RoleID).ToList();

                                }
                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                        }

                                        // _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                       // _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId)); ---commented

                                    }
                                }
                                if (_objdata.MeetingId != null)
                                {
                                    if (_objdata.MeetingId.Count > 0)
                                    {
                                        //_objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.MeetingID == _objdata.MeetingId).ToList();
                                        _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.MeetingId.Contains((long)emp.MeetingID));

                                    }
                                }

                                if (_objdata.statusId > 0)
                                {
                                    if (_objdata.statusId == 1)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.StatusName.ToLower() == "upcoming").ToList();
                                    }
                                    if (_objdata.statusId == 2)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.StatusName.ToLower() == "overdue").ToList();
                                    }
                                    else if (_objdata.statusId == 3)//pending for review
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.StatusName.ToLower() == "pending for review").ToList();
                                    }
                                    else if (_objdata.statusId == 4)//closed-timely
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.StatusName.ToLower() == "closed-timely").ToList();
                                    }
                                    else if (_objdata.statusId == 5)//closed-delayed
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.StatusName.ToLower() == "closed-delayed").ToList();
                                    }

                                }
                            }

                            if (_objDirectorsCompliance.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objDirectorsCompliance.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objDirectorsCompliance = _objDirectorsCompliance.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorsCompliance).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("DirectorCompliancePerformance")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorCompliancePerformance([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<MyCompliance_Director_ResultVM> _objDirectorsCompliance = new List<MyCompliance_Director_ResultVM>();

                int StatusId = 0;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        //RoleId = _objdata.RoleID;
                        StatusId = _objdata.statusId;
                        if (user != null)
                        {
                            _objDirectorsCompliance = MyComplianceManagment.MyCompliances_Director(Convert.ToInt32(user.CustomerID), Convert.ToInt32(user.ID), 3, _objdata.FY);
                            if (_objDirectorsCompliance.Count > 0)
                            {
                                if (_objdata.EntityId > 0)
                                {
                                    _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.EntityId.Equals(_objdata.EntityId)).ToList();
                                }
                                if (_objdata.statusId > 0)
                                {
                                    if (_objdata.statusId == 2)//Overdue
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.ComplianceStatus.ToLower() == "overdue").ToList();
                                    }
                                    else if (_objdata.statusId == 4)//close-timelt
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.ComplianceStatus.ToLower() == "closed-timely").ToList();
                                    }
                                    else if (_objdata.statusId == 5)//close-delayed
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.ComplianceStatus.ToLower() == "closed-delayed").ToList();
                                    }
                                    else if (_objdata.statusId == 1)//close-delayed
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.ComplianceStatus.ToLower() == "closed-delayed").ToList();
                                    }
                                }
                                if (_objdata.EntityId > 0)
                                {
                                    _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                }
                                if (_objdata.Risk > 0)
                                {
                                    if (_objdata.Risk == 1)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.Risk.ToLower() == "high").ToList();
                                    }
                                    else if (_objdata.Risk == 2)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.Risk.ToLower() == "medium").ToList();
                                    }
                                    else if (_objdata.Risk == 3)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.Risk.ToLower() == "low").ToList();
                                    }
                                }
                            }
                            if (_objDirectorsCompliance.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objDirectorsCompliance.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objDirectorsCompliance = _objDirectorsCompliance.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorsCompliance).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("ComplianceDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceDocument([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<BM_SP_DirectorComplianceDocuments_Result> _objDirectorsCompliance = new List<BM_SP_DirectorComplianceDocuments_Result>();


                int StatusId = -1;
                //_objdata.Entity = new List<int>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (_objdata.statusId > 0)
                            StatusId = _objdata.statusId;

                        if (user != null)
                        {
                            _objDirectorsCompliance = MyComplianceManagment.GetComplianceDocument(user.CustomerID, Convert.ToInt32(user.ID), 4, StatusId);
                            if (_objDirectorsCompliance.Count > 0)
                            {
                                if (_objdata.ComplianceType > 0)
                                {
                                    if (_objdata.ComplianceType == 1)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => emp.MappingType == "A" || emp.MappingType == "M");
                                        
                                    }
                                    else if (_objdata.ComplianceType == 2)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => emp.MappingType == "S");
                                       
                                    }
                                    else if (_objdata.ComplianceType == 3)
                                    {
                                        _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => emp.MappingType == "D");

                                    }
                                }

                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => listentities.Contains(emp.EntityId));
                                        }
                                        //_objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                        //_objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                    }
                                }

                                if (_objdata.MeetingId != null)
                                {
                                    if (_objdata.MeetingId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.MeetingId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => listentities.Contains(emp.MeetingID));
                                        }
                                        //_objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.MeetingID == _objdata.MeetingId).ToList();
                                       // _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.MeetingId.Contains((long)emp.MeetingID));
                                    }
                                }
                                if (_objdata.Fyear != null)
                                {
                                    if (_objdata.Fyear.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.Fyear where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => listentities.Contains(emp.FY));
                                        }
                                        // _objDirectorsCompliance = _objDirectorsCompliance.Where(x => x.FY == _objdata.Fyear).ToList();
                                        //_objDirectorsCompliance = _objDirectorsCompliance.FindAll(emp => _objdata.Fyear.Contains((long)emp.FY));

                                    }
                                }

                            }
                                if (_objDirectorsCompliance.Count > 0)
                                {

                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = _objDirectorsCompliance.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (_objdata.PageId - 1);
                                    var canPage = skip < total;
                                    _objDirectorsCompliance = _objDirectorsCompliance.Skip(skip).Take(pageSize).ToList();

                                }
                            }
                            else
                            {
                                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                                errordetail.Add(new ErrorDetails { ErrorMessage = "Email not found", ErrorDetailMessage = "" });
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                                };
                            }
                        }
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(_objDirectorsCompliance).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("ComplianceStatusReport")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceStatusReport([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<BM_SP_DirectorComplianceStatusReport_Result> _objComplianceStatusReport = new List<BM_SP_DirectorComplianceStatusReport_Result>();


                int StatusId = -1;
                //_objdata.Entity = new List<int>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (_objdata.statusId > 0)
                            StatusId = _objdata.statusId;

                        if (user != null)
                        {
                            _objComplianceStatusReport = MyComplianceManagment.GetDirectorComplianceStatusreport(user.CustomerID, Convert.ToInt32(user.ID), 2, StatusId);
                            if (_objComplianceStatusReport.Count > 0)
                            {
                                string Type = string.Empty;

                                if (_objdata.ComplianceType > 0)
                                {

                                    //if (_objdata.ComplianceType == 2 || _objdata.ComplianceType == 3)
                                    //{
                                    //    _objComplianceStatusReport = new List<BM_SP_DirectorComplianceStatusReport_Result>();
                                    //}

                                    if (_objdata.ComplianceType == 1)
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => emp.MappingType == "A" || emp.MappingType == "M");

                                    }
                                    else if (_objdata.ComplianceType == 2)
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => emp.MappingType == "S");

                                    }
                                    else if (_objdata.ComplianceType == 3)
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => emp.MappingType == "D");

                                    }


                                }
                                if (_objdata.statusId > 0)
                                {
                                    if (_objdata.statusId == 1)//upcoming
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ComplianceStatus.ToLower() == "upcoming").ToList();
                                    }
                                    else if (_objdata.statusId == 2)//overdue
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ComplianceStatus.ToLower() == "overdue").ToList();
                                    }
                                    else if (_objdata.statusId == 3)//pending for review
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ComplianceStatus.ToLower() == "pending for review").ToList();
                                    }
                                    else if (_objdata.statusId == 4)//closed-timely
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ComplianceStatus.ToLower() == "closed-timely").ToList();
                                    }
                                    else if (_objdata.statusId == 5)//closed-delayed
                                    {
                                        _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ComplianceStatus.ToLower() == "closed-delayed").ToList();
                                    }

                                }
                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {

                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => listentities.Contains(emp.EntityId));
                                        }
                                        //_objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                        //_objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                    }

                                }
                                if (_objdata.ActId != null)
                                {
                                    if (_objdata.ActId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.ActId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => listentities.Contains(emp.ActID));
                                        }
                                        //_objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.ActID == _objdata.ActId).ToList();
                                        //_objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => _objdata.ActId.Contains(emp.ActID));
                                    }
                                }

                                if (_objdata.MeetingId != null)
                                {
                                    if (_objdata.MeetingId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.MeetingId where row > 0 select row).ToList();
                                        if (listentities.Count > 0)
                                        {
                                            _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => listentities.Contains(emp.MeetingID));
                                        }
                                        //_objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => _objdata.MeetingId.Contains((long)emp.MeetingID));
                                    }
                                }
                                if (_objdata.Fyear != null)
                                {
                                    if (_objdata.Fyear.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.Fyear where row > 0 select row).ToList();
                                        if (listentities.Count > 0)
                                        {
                                            _objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => listentities.Contains(emp.FY));
                                        }
                                        // _objComplianceStatusReport = _objComplianceStatusReport.Where(x => x.FY == _objdata.Fyear).ToList();
                                        //_objComplianceStatusReport = _objComplianceStatusReport.FindAll(emp => _objdata.Fyear.Contains((long)emp.FY));
                                    }
                                }

                            }
                            if (_objComplianceStatusReport.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objComplianceStatusReport.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                    _objComplianceStatusReport = _objComplianceStatusReport.Skip(skip).Take(pageSize).ToList();

                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objComplianceStatusReport).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("AttendenceReport")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AttendenceReport([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<BM_SP_DirectorAttendanceReport_Result> _objDirectorsAttendenceReport = new List<BM_SP_DirectorAttendanceReport_Result>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();


                        if (user != null)
                        {
                            _objDirectorsAttendenceReport = MyComplianceManagment.GetDirectorAttendenceReport(user.CustomerID, Convert.ToInt32(user.ID));
                            if (_objDirectorsAttendenceReport.Count > 0)
                            {
                                if (_objdata.Fyear != null)
                                {
                                    if (_objdata.Fyear.Count > 0)
                                    {

                                        // _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.Where(x => x.FY == _objdata.Fyear).ToList();
                                       // _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.FindAll(emp => _objdata.Fyear.Contains(emp.FY));

                                        var listentities = (from row in _objdata.Fyear where row > 0 select row).ToList();
                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.FindAll(emp => _objdata.Fyear.Contains(emp.FY));
                                        }


                                    }
                                }

                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {

                                        // _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                        //_objDirectorsAttendenceReport = _objDirectorsAttendenceReport.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));

                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                        }

                                    }
                                }
                            }
                            if (_objDirectorsAttendenceReport.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objDirectorsAttendenceReport.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objDirectorsAttendenceReport = _objDirectorsAttendenceReport.Skip(skip).Take(pageSize).ToList();
                            }

                        }


                        
                    }
                }
                else
                {
                    List<ErrorDetails> errordetail = new List<ErrorDetails>();
                    errordetail.Add(new ErrorDetails { ErrorMessage = "Email not found", ErrorDetailMessage = "" });
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                    };
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorsAttendenceReport).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("ComplianceStatusReport_ExportDouments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceStatusReport_ExportDouments([FromBody] UserDataVM _objdata)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string ReturnPath = "";
            List<ReturnFileVM> _objRegisterList = new List<ReturnFileVM>();

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<BM_SP_DirectorComplianceStatusReport_Result> _objComplianceStatusReport = new List<BM_SP_DirectorComplianceStatusReport_Result>();

                int StatusId = -1;
                //_objdata.Entity = new List<int>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (_objdata.statusId > 0)
                            StatusId = _objdata.statusId;

                        if (user != null)
                        {
                            _objComplianceStatusReport = MyComplianceManagment.GetDirectorComplianceStatusreport(user.CustomerID, Convert.ToInt32(user.ID), 2, StatusId);
                        }
                        if (_objComplianceStatusReport.Count > 0)
                        {
                            string ReportName = string.Empty;
                            ReportName = "Compliance_Status_Report";

                            string ReportNameAdd = ReportName;
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);

                                DataView view1 = new DataView();
                                DataTable table1 = new DataTable();
                                int P = 0;
                                table1.Columns.Add("SrNo", typeof(string));
                                table1.Columns.Add("CompanyName", typeof(string));
                                table1.Columns.Add("FYText", typeof(string));
                                table1.Columns.Add("MeetingTitle", typeof(string));
                                table1.Columns.Add("AgendaItemTextNew", typeof(string));
                                table1.Columns.Add("ActName", typeof(string));
                                table1.Columns.Add("ShortDescription", typeof(string));

                                table1.Columns.Add("ScheduleOn", typeof(string));
                                table1.Columns.Add("ComplianceStatus", typeof(string));
                                foreach (var item in _objComplianceStatusReport)
                                {
                                    P++;
                                    table1.Rows.Add(P, item.CompanyName, item.FYText, item.MeetingTitle, item.AgendaItemTextNew, item.ActName, item.ShortDescription, item.ScheduleOn, item.ComplianceStatus);
                                }
                                view1 = new System.Data.DataView(table1);
                                //ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "Section", "ShortDescription", "DepartmentName", "Frequency", "User");
                                DataTable ExcelData = null;

                                ExcelData = view1.ToTable("Selected", false, "SrNo", "CompanyName", "FYText", "MeetingTitle", "AgendaItemTextNew", "ActName", "ShortDescription", "ScheduleOn", "ComplianceStatus");
                                exWorkSheet1.Row(6).Height = 50.25;

                                exWorkSheet1.Cells["A1"].Value = "SrNo";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A1"].Style.WrapText = true;
                                exWorkSheet1.Cells["A1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells["B1"].Value = "Company Name";
                                exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["B1"].Style.WrapText = true;
                                exWorkSheet1.Cells["B1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(2).Width = 50;

                                exWorkSheet1.Cells["C1"].Value = "Financial Year";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["C1"].Style.WrapText = true;
                                exWorkSheet1.Cells["C1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(3).Width = 50;

                                exWorkSheet1.Cells["D1"].Value = "Meeting";
                                exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D1"].Style.WrapText = true;
                                exWorkSheet1.Cells["D1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(4).Width = 60;

                                exWorkSheet1.Cells["E1"].Value = "Agenda";
                                exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["E1"].Style.WrapText = true;
                                exWorkSheet1.Cells["E1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(5).Width = 80;

                                exWorkSheet1.Cells["F1"].Value = "ActName";
                                exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["F1"].Style.WrapText = true;
                                exWorkSheet1.Cells["F1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(6).Width = 80;

                                exWorkSheet1.Cells["G1"].Value = "Short Description";
                                exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["G1"].Style.WrapText = true;
                                exWorkSheet1.Cells["G1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(7).Width = 70;

                                exWorkSheet1.Cells["H1"].Value = "Due Date";
                                exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["H1"].Style.WrapText = true;
                                exWorkSheet1.Cells["H1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["I1"].Value = "Status";
                                exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["I1"].Style.WrapText = true;
                                exWorkSheet1.Cells["I1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["A2"].LoadFromDataTable(ExcelData, false);

                                using (ExcelRange col = exWorkSheet1.Cells[2, 1, 1 + ExcelData.Rows.Count, 9])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                //Byte[] fileBytes = exportPackge.GetAsByteArray();
                                //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");


                                string reportName = string.Empty;
                                string destinationPath = string.Empty;

                                reportName = ReportNameAdd + "_" + user.ID.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();

                                string Folder = "~/TempFiles/Secretarial";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(reportName);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                //Directory.CreateDirectory(finalDateFolder);
                                if (!Directory.Exists(finalDateFolder))
                                {
                                    Directory.CreateDirectory(finalDateFolder);
                                }

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = Convert.ToString(user.ID) + "" + Convert.ToString(user.CustomerID) + "" + FileDate;

                                string FileName = DateFolder + "/" + reportName;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream objFileStrm = System.IO.File.Create(finalfilepathforuploadfile);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(finalfilepathforuploadfile, exportPackge.GetAsByteArray());
                                string pathforandriodoutput = finalfilepathforuploadfile;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;
                                _objRegisterList.Add(new ReturnFileVM { FileName = finaloutputforandriodpath });
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(JArray.FromObject(_objRegisterList).ToString(), Encoding.UTF8, "application/json")
                                };
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }

                    }
                }
                ReturnPath = "File not Found";
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = "", ErrorDetailMessage = ReturnPath });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        [Authorize]
        [Route("AttendenceReport_ExportDouments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AttendenceReport_ExportDouments([FromBody] UserDataVM _objdata)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string ReturnPath = "";
            List<ReturnFileVM> _objRegisterList = new List<ReturnFileVM>();
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<BM_SP_DirectorAttendanceReport_Result> _objDirectorsAttendenceReport = new List<BM_SP_DirectorAttendanceReport_Result>();

                int StatusId = -1;
                //_objdata.Entity = new List<int>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (_objdata.statusId > 0)
                            StatusId = _objdata.statusId;

                        if (user != null)
                        {
                            _objDirectorsAttendenceReport = MyComplianceManagment.GetDirectorAttendenceReport(user.CustomerID, Convert.ToInt32(user.ID));
                        }
                        if (_objDirectorsAttendenceReport.Count > 0)
                        {
                            string ReportName = string.Empty;
                            ReportName = "Attendence_Report";

                            string ReportNameAdd = ReportName;
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);

                                DataView view1 = new DataView();
                                DataTable table1 = new DataTable();
                                int P = 0;
                                table1.Columns.Add("SrNo", typeof(string));
                                table1.Columns.Add("CompanyName", typeof(string));
                                table1.Columns.Add("FYText", typeof(string));
                                table1.Columns.Add("MeetingTitle", typeof(string));
                                table1.Columns.Add("MeetingDate", typeof(string));
                                table1.Columns.Add("StartTime", typeof(string));
                                table1.Columns.Add("EndMeetingTime", typeof(string));
                                table1.Columns.Add("Present", typeof(string));

                                foreach (var item in _objDirectorsAttendenceReport)
                                {
                                    P++;
                                    table1.Rows.Add(P, item.CompanyName, item.FYText, item.MeetingTitle, item.MeetingDate, item.StartTime, item.EndMeetingTime, item.Present);
                                }
                                view1 = new System.Data.DataView(table1);
                                //ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "Section", "ShortDescription", "DepartmentName", "Frequency", "User");
                                DataTable ExcelData = null;

                                ExcelData = view1.ToTable("Selected", false, "SrNo", "CompanyName", "FYText", "MeetingTitle", "MeetingDate", "StartTime", "EndMeetingTime", "Present");
                                exWorkSheet1.Row(6).Height = 50.25;

                                exWorkSheet1.Cells["A1"].Value = "SrNo";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A1"].Style.WrapText = true;
                                exWorkSheet1.Cells["A1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells["B1"].Value = "Company Name";
                                exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["B1"].Style.WrapText = true;
                                exWorkSheet1.Column(2).Width = 50;
                                exWorkSheet1.Cells["B1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["C1"].Value = "Financial Year";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["C1"].Style.WrapText = true;
                                exWorkSheet1.Cells["C1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(3).Width = 50;

                                exWorkSheet1.Cells["D1"].Value = "Meeting";
                                exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D1"].Style.WrapText = true;
                                exWorkSheet1.Cells["D1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Column(4).Width = 50;

                                exWorkSheet1.Cells["E1"].Value = "Meeting Date";
                                exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["E1"].Style.WrapText = true;
                                exWorkSheet1.Cells["E1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["F1"].Value = "Start Time";
                                exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["F1"].Style.WrapText = true;
                                exWorkSheet1.Cells["F1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["G1"].Value = "End Time";
                                exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["G1"].Style.WrapText = true;
                                exWorkSheet1.Cells["G1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["H1"].Value = "Present";
                                exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["H1"].Style.WrapText = true;
                                exWorkSheet1.Cells["H1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;



                                exWorkSheet1.Cells["A2"].LoadFromDataTable(ExcelData, false);

                                using (ExcelRange col = exWorkSheet1.Cells[2, 1, 1 + ExcelData.Rows.Count, 8])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                string reportName = string.Empty;
                                string destinationPath = string.Empty;

                                reportName = ReportNameAdd + "_" + user.ID.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();

                                string Folder = "~/TempFiles/Secretarial";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(reportName);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                //Directory.CreateDirectory(finalDateFolder);
                                if (!Directory.Exists(finalDateFolder))
                                {
                                    Directory.CreateDirectory(finalDateFolder);
                                }

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = Convert.ToString(user.ID) + "" + Convert.ToString(user.CustomerID) + "" + FileDate;

                                string FileName = DateFolder + "/" + reportName;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream objFileStrm = System.IO.File.Create(finalfilepathforuploadfile);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(finalfilepathforuploadfile, exportPackge.GetAsByteArray());
                                string pathforandriodoutput = finalfilepathforuploadfile;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;
                                _objRegisterList.Add(new ReturnFileVM { FileName = finaloutputforandriodpath });
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(JArray.FromObject(_objRegisterList).ToString(), Encoding.UTF8, "application/json")
                                };
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }

                    }
                }
                ReturnPath = "File not Found";
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = "", ErrorDetailMessage = ReturnPath });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        [Authorize]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [Route("GetComplianceFile")]
        public HttpResponseMessage GetComplianceFile(string userpath)
        {

            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            var dataBytes = File.ReadAllBytes(p_strPath);
            var dataStream = new MemoryStream(dataBytes);

            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(dataStream);
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = userpath;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            return httpResponseMessage;
        }


        [Authorize]
        [Route("ComplianceStatusReportDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceStatusReportDocument([FromBody] UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchComppath(_objdata.ScheduleOnID, _objdata.ComplianceInstanceID, Convert.ToInt32(user.ID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}

