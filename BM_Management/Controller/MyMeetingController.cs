﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.BM_Managment.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.DirectorsMeetingVM;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/MyMeeting")]
    public class MyMeetingController : ApiController
    {

        [Authorize]
        [Route("SeekAvailability")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SeekAvailability([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<SeekAviabilityVM> _objSeekaviability = new List<SeekAviabilityVM>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objSeekaviability = MyMeetingManagment.GetSeekAvailability(user.CustomerID, Convert.ToInt32(user.ID));
                            if (_objSeekaviability.Count > 0)
                            {
                                if (_objdata.EntityId > 0)
                                {
                                    _objSeekaviability = _objSeekaviability.Where(x => x.Entityt_Id == _objdata.EntityId).ToList();
                                }

                            }
                            if (_objdata.MeetingId > 0)
                            {

                                _objSeekaviability = _objSeekaviability.Where(x => x.MeetingTypeId == _objdata.MeetingId).ToList();

                            }
                        }

                        if (_objSeekaviability.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objSeekaviability.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objSeekaviability = _objSeekaviability.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }

                else
                {
                    List<ErrorDetails> errordetail = new List<ErrorDetails>();
                    errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                    };
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objSeekaviability).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("SeekAviabilityResponses")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SeekAviabilityResponses([FromBody] List<Aviability> _objAvibility)
        {
            List<ResponsesVM> _objCircularResponses = new List<ResponsesVM>();
            try
            {
                if (_objAvibility.Count > 0)
                {
                    foreach (var item in _objAvibility)
                    {
                        var CallerAgent = HttpContext.Current.Request.UserAgent;
                        string EmailId = UserManagemet.getDecryptedText(item.EmailId.ToString(), CallerAgent);



                        if (!string.IsNullOrEmpty(EmailId))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                User user = (from row in entities.Users
                                             where (row.Email == EmailId)
                                               && row.IsActive == true
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();

                                if (user != null)
                                {
                                    _objCircularResponses = MyMeetingManagment.addUpdateAviabilityResponses(item, Convert.ToInt32(user.ID));
                                }

                            }
                        }

                    }
                }
                else
                {
                    List<ErrorDetails> errordetail = new List<ErrorDetails>();
                    errordetail.Add(new ErrorDetails { ErrorMessage = "No data found", ErrorDetailMessage = "" });
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                    };
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularResponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("CircularResolution")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CircularResolution([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<CircularResolutionVM> _objCircularResolution = new List<CircularResolutionVM>();



                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCircularResolution = MyMeetingManagment.GetCircularResolution(user.CustomerID, Convert.ToInt32(user.ID));
                        if (_objCircularResolution.Count > 0)
                        {
                            if (_objdata.EntityId != null)
                            {
                                if (_objdata.EntityId.Count > 0)
                                {
                                    var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();
                                    if (listentities.Count > 0)
                                    {
                                        _objCircularResolution = _objCircularResolution.FindAll(emp => listentities.Contains(emp.EntityId));
                                    }
                                    
                                }
                            }

                            if (_objdata.MeetingId != null)
                            {
                                if (_objdata.MeetingId.Count > 0)
                                {
                                    var listmeetings = (from row in _objdata.MeetingId where row > 0 select row).ToList();
                                    if (listmeetings.Count > 0)
                                    {
                                        _objCircularResolution = _objCircularResolution.FindAll(emp => listmeetings.Contains(emp.MeetingTypeId));
                                    }                                
                                }
                            }

                            // Filters From Date and to date added 29 Jun 2021
                            if (!string.IsNullOrEmpty(_objdata.FromDate))
                            {
                                var FDate = DateTime.ParseExact(_objdata.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                _objCircularResolution = _objCircularResolution.Where(x => x.MeetingDate >= FDate).ToList();
                            }
                            if (!string.IsNullOrEmpty(_objdata.ToDate))
                            {
                                var TDate = DateTime.ParseExact(_objdata.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                _objCircularResolution = _objCircularResolution.Where(x => x.MeetingDate <= TDate).ToList();
                            }
                            //
                        }

                        if (_objCircularResolution.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCircularResolution.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCircularResolution = _objCircularResolution.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularResolution).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("CircularResolutionAgenda")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CircularResolutionAgenda([FromBody] UserDataVM _objdata)
        {
            long MeetingId = 0;
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingAgendaSummaryVM> _objCircularMeetingAgenda = new List<MeetingAgendaSummaryVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        MeetingId = _objdata.MeetingId;
                        _objCircularMeetingAgenda = MyMeetingManagment.GetCircularAgenda(user.CustomerID, Convert.ToInt32(user.ID), MeetingId);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularMeetingAgenda).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("PastCircularResolution")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PastCircularResolution([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<CircularResolutionVM> _objCircularResolution = new List<CircularResolutionVM>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCircularResolution = MyMeetingManagment.GetPastCircularResolution(user.CustomerID, Convert.ToInt32(user.ID));
                        if (_objCircularResolution.Count > 0)
                        {
                            if (_objdata.EntityId != null)
                            {
                                if (_objdata.EntityId.Count > 0)
                                {
                                    var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                    if (listentities.Count > 0)
                                    {
                                        _objCircularResolution = _objCircularResolution.FindAll(emp => listentities.Contains(emp.EntityId));
                                    } 

                                }
                            }

                            if (_objdata.MeetingId != null)
                            {
                                if (_objdata.MeetingId.Count > 0)
                                {
                                    var listmeetings = (from row in _objdata.MeetingId where row > 0 select row).ToList();
                                    if (listmeetings.Count > 0)
                                    {
                                        _objCircularResolution = _objCircularResolution.FindAll(emp => listmeetings.Contains(emp.MeetingID));
                                    }
                                  
                                }
                            }
                            if (!string.IsNullOrEmpty(_objdata.FromDate))
                            {
                                var FDate = DateTime.ParseExact(_objdata.FromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                _objCircularResolution = _objCircularResolution.Where(x => x.MeetingDate >= FDate).ToList();
                            }
                            if (!string.IsNullOrEmpty(_objdata.ToDate))
                            {
                                var TDate = DateTime.ParseExact(_objdata.ToDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                _objCircularResolution = _objCircularResolution.Where(x => x.MeetingDate <= TDate).ToList();
                            }
                        }

                        if (_objCircularResolution.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCircularResolution.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCircularResolution = _objCircularResolution.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularResolution).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("CircularResolutionResponses")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CircularResolutionResponses([FromBody] List<MeetingAgendaSummaryVM> _objagendaresponse)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = string.Empty;
                List<ResponsesVM> _objCircularResponses = new List<ResponsesVM>();
                if (_objagendaresponse != null)
                {
                    foreach (var item in _objagendaresponse)
                    {
                        EmailId = UserManagemet.getDecryptedText(item.EmailId, CallerAgent);
                        if (!string.IsNullOrEmpty(EmailId))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                User user = (from row in entities.Users
                                             where (row.Email == EmailId)
                                               && row.IsActive == true
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();

                                if (user != null)
                                {
                                    int userId = Convert.ToInt32(user.ID);
                                    _objCircularResponses = MyMeetingManagment.addUpdateAgendaResponses(item, userId);
                                }
                            }
                        }
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularResponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("ConcludeMeetings")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ConcludeMeetings([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<DraftMeetingListVM> _objConcludeMeetings = new List<DraftMeetingListVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objConcludeMeetings = MyMeetingManagment.GetConcludeMeetings(user.CustomerID, Convert.ToInt32(user.ID));
                            if (_objConcludeMeetings.Count > 0)
                            {
                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetings = _objConcludeMeetings.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));                                           
                                        }
                                        

                                    }
                                }
                                if (_objdata.MeetingId != null)
                                {
                                    if (_objdata.MeetingId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.MeetingId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetings = _objConcludeMeetings.FindAll(emp => _objdata.MeetingId.Contains(emp.MeetingID));
                                        }
                                        // _objConcludeMeetings = _objConcludeMeetings.FindAll(emp => _objdata.MeetingId.Contains(emp.MeetingID));
                                    }
                                }

                                // added for FY filter 24Jun2021
                                if (_objdata.Fyear != null)
                                {
                                    if (_objdata.Fyear.Count > 0)                                    
                                    {
                                        var listentities = (from row in _objdata.Fyear where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetings = _objConcludeMeetings.FindAll(emp => _objdata.Fyear.Contains(emp.FYID));
                                        }
                                       
                                    }
                                }
                                //

                                if (_objConcludeMeetings.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = _objConcludeMeetings.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (_objdata.PageId - 1);
                                    var canPage = skip < total;
                                    _objConcludeMeetings = _objConcludeMeetings.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "EmailId not Found", ErrorDetailMessage = "" });
                        }
                    }


                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objConcludeMeetings).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("ConcludeMeetingAgenda")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ConcludeMeetingAgenda([FromBody] userfilter _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<ConcludedMeetingAgendaItemVM> _objConcludeMeetingAgenda = new List<ConcludedMeetingAgendaItemVM>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objConcludeMeetingAgenda = MyMeetingManagment.GetConcludeMeetingAgenda(user.CustomerID, Convert.ToInt32(user.ID));
                            if (_objConcludeMeetingAgenda.Count > 0)
                            {
                                if (_objdata.EntityId != null)
                                {
                                    if (_objdata.EntityId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.EntityId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => listentities.Contains(emp.EntityId));
                                        }

                                        // _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                        //_objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                    }
                                }

                                if (_objdata.MeetingId != null)
                                {
                                    if (_objdata.MeetingId.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.MeetingId where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => listentities.Contains(emp.MeetingID));
                                        }

                                        // _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.Where(x => x.MeetingTypeId_ == _objdata.MeetingId).ToList();
                                        // _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => _objdata.MeetingId.Contains((long)emp.MeetingTypeId_));
                                    }
                                }

                                if (_objdata.Fyear != null)
                                {
                                    if (_objdata.Fyear.Count > 0)
                                    {
                                        var listentities = (from row in _objdata.Fyear where row > 0 select row).ToList();

                                        if (listentities.Count > 0)
                                        {
                                            _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => listentities.Contains(emp.FY));
                                        }
                                        // _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.Where(x => x.FY == _objdata.Fyear).ToList();
                                        //_objConcludeMeetingAgenda = _objConcludeMeetingAgenda.FindAll(emp => _objdata.Fyear.Contains(emp.FY));
                                    }
                                }
                                if (!string.IsNullOrEmpty(_objdata.agendaText))
                                {
                                    _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.Where(x => x.AgendaItemText.ToLower().Contains(_objdata.agendaText.ToLower())).ToList();

                                }
                            }
                            if (_objConcludeMeetingAgenda.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objConcludeMeetingAgenda.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objConcludeMeetingAgenda = _objConcludeMeetingAgenda.Skip(skip).Take(pageSize).ToList();

                            }
                        }
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objConcludeMeetingAgenda).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("MyDraftMinutes")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDraftMinutes([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<DraftMeetingListVM> _objDraftminutes = new List<DraftMeetingListVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objDraftminutes = MyMeetingManagment.GetMyDraftMinutes(user.CustomerID, Convert.ToInt32(user.ID));
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDraftminutes).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("MyDraftMinutesDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDraftMinutesDocuments([FromBody] UserDataVM _objdata)
        {
            long DraftCirculationID = 0;
            long MeetingId = 0;

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<PreviewAgendaVM> _objDraftminutesdocs = new List<PreviewAgendaVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        DraftCirculationID = _objdata.DraftCirculationId;
                        MeetingId = _objdata.MeetingId;
                        _objDraftminutesdocs = MyMeetingManagment.GetMyDraftMinutesDocuments(user.CustomerID, Convert.ToInt32(user.ID), DraftCirculationID, MeetingId);
                        if (_objDraftminutesdocs.Count > 0)
                        {

                            foreach (var item in _objDraftminutesdocs)
                            {
                                item.DraftCirculationID_ = DraftCirculationID;
                                item.IsDraftMinutesApproved = MyMeetingManagment.IsDraftMinutesApproved(DraftCirculationID);
                                item.lstMeetingAttendance = MyMeetingManagment.GetPerticipenforAttendenceCS(MeetingId, user.CustomerID, Convert.ToInt32(user.ID));
                                item.meetingMinutesDetails = MyMeetingManagment.GetMinutesDetails(MeetingId);
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDraftminutesdocs).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("MyDraftMinutesComments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDraftMinutesComments([FromBody] MeetingMinutesCommentVM _objMinutesComment)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objMinutesComment.EmailId.ToString(), CallerAgent);
                List<ResponsesVM> _objDraftMinutesCommentResponses = new List<ResponsesVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();


                        _objDraftMinutesCommentResponses = MyMeetingManagment.saveDraftMinutesComments(user.CustomerID, Convert.ToInt32(user.ID), _objMinutesComment);

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDraftMinutesCommentResponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("MyDraftMinutesShowComments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDraftMinutesShowComments([FromBody] UserDataVM _objdata)
        {
            try
            {
                long draftCirculationID = 0;
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingMinutesCommentVM> _objDraftMinutesComments = new List<MeetingMinutesCommentVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        draftCirculationID = _objdata.DraftCirculationId;

                        _objDraftMinutesComments = MyMeetingManagment.getDraftMinutesComments(user.CustomerID, Convert.ToInt32(user.ID), draftCirculationID);

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDraftMinutesComments).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("MyAgendaList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyAgendaList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<MeetingAgendaSummaryVM> _objAgenda = new List<MeetingAgendaSummaryVM>();
                int MeetingId = 0;
                MeetingId = _objdata.MeetingId;
                long HistoricalID = _objdata.HistoricalID;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        //--- commented for Historical meeting
                        //  _objAgenda = MyMeetingManagment.GetMeetingwiseAgendaList(user.CustomerID, Convert.ToInt32(user.ID), MeetingId); 

                        if (MeetingId > 0)
                        {
                            _objAgenda = MyMeetingManagment.GetMeetingwiseAgendaList(user.CustomerID, Convert.ToInt32(user.ID), MeetingId);
                        }
                        else
                        {
                            _objAgenda = MyMeetingManagment.GetAgendaItemForConcludedMeeting(HistoricalID);
                        }
                        // end of change for historical meeting
                        if (_objAgenda.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objAgenda.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objAgenda = _objAgenda.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAgenda).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // API For To read the Agenda Document 
        [Authorize]
        [Route("MyAgendaDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyAgendaDocuments([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<AgendaDocumentVM> _objAgendaDocument = new List<AgendaDocumentVM>();
                int MeetingAgendaMapping = 0;


                MeetingAgendaMapping = _objdata.MeetingAgendaMappingId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objAgendaDocument = MyMeetingManagment.GetMeetingwiseAgendaDocument(user.CustomerID, Convert.ToInt32(user.ID), MeetingAgendaMapping);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAgendaDocument).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("AddUpdateRSPVResponses")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddUpdateRSPVResponses([FromBody] RSPVResponseVM _objrspvresponse)
        {
            try
            {
                long MeetingId = Convert.ToInt32(_objrspvresponse.MeetingID);
                long participantId = Convert.ToInt32(_objrspvresponse.participantID);
                string ReasonforRSPV = _objrspvresponse.ReasonforRSPV;
                string RSPV = _objrspvresponse.RSPV;
                if (RSPV != "" && RSPV != null)
                {
                    if (RSPV.ToLower() == "yes")
                    {
                        RSPV = "Yes";
                    }
                }

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objrspvresponse.Email.ToString(), CallerAgent);


                List<ResponsesVM> _objresponses = new List<ResponsesVM>();

                if (!string.IsNullOrEmpty(_objrspvresponse.Email))
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objresponses = MyMeetingManagment.AddUpdateRSVP(user.CustomerID, Convert.ToInt32(user.ID), MeetingId, participantId, ReasonforRSPV, RSPV);
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objresponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("AgendaPreview")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AgendaPreview([FromBody] UserDataVM _objdata)
        {
            try
            {
                long MeetingId = 0;
                MeetingId = _objdata.MeetingId;


                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<PreviewAgendaVM> _objAgendaPriview = new List<PreviewAgendaVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objAgendaPriview = MyMeetingManagment.PreviewAgenda(MeetingId, user.CustomerID, false);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAgendaPriview).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("MarkAttendence")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MarkAttendence([FromBody] UserDataVM _objdata)
        {
            try
            {
                long MeetingId = 0;
                long ParticipantID = 0;
                MeetingId = _objdata.MeetingId;
                ParticipantID = _objdata.ParticipantId;


                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<ResponsesVM> _objResponses = new List<ResponsesVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        int UserId = Convert.ToInt32(user.ID);
                        _objResponses = MyMeetingManagment.Markattendence(MeetingId, user.CustomerID, UserId, ParticipantID);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objResponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("JoinMeeting")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage JoinMeeting([FromBody] UserDataVM _objdata)
        {
            try
            {
                long MeetingId = 0;

                MeetingId = _objdata.MeetingId;



                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<MeetingAgendaSummaryVM> _objMeetingAgenda = new List<MeetingAgendaSummaryVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        int UserId = Convert.ToInt32(user.ID);
                        _objMeetingAgenda = MyMeetingManagment.GetMeetingDetails(MeetingId, user.CustomerID, UserId);
                        if (_objMeetingAgenda.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objMeetingAgenda.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objMeetingAgenda = _objMeetingAgenda.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objMeetingAgenda).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("MeetingVootingNotingResponses")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MeetingVootingNotingResponses([FromBody] List<MeetingAgendaSummaryVM> _objagendaresponse)
        {
            List<ResponsesVM> _objCircularResponses = new List<ResponsesVM>();
            try
            {
                if (_objagendaresponse != null)
                {
                    foreach (var item in _objagendaresponse)
                    {
                        var CallerAgent = HttpContext.Current.Request.UserAgent;
                        string EmailId = UserManagemet.getDecryptedText(item.EmailId.ToString(), CallerAgent);


                        if (!string.IsNullOrEmpty(EmailId))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                User user = (from row in entities.Users
                                             where (row.Email == EmailId)
                                               && row.IsActive == true
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();

                                if (user != null)
                                {
                                    _objCircularResponses = MyMeetingManagment.addUpdateVootingNotingAgendaResponses(item, user.ID);
                                }
                                else
                                {
                                    //LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    List<ErrorDetails> errordetail = new List<ErrorDetails>();
                                    errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not Found", ErrorDetailMessage = "" });
                                    return new HttpResponseMessage()
                                    {
                                        Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                                    };
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCircularResponses).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("AgendaDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AgendaDocuments([FromBody]UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchpath(_objdata.MeetingAgendaMappingId, Convert.ToInt32(user.ID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }






        [Authorize]
        [Route("MinutesPreviewDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MinutesPreviewDocuments([FromBody]UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            var lstFiles = Sec_DocumentManagement.fetchpathForMinutespreview(_objdata.MeetingId, Convert.ToInt32(user.ID));
                            if (lstFiles != null) 
                            {
                                foreach (var file in lstFiles) 
                                {
                                    if (!string.IsNullOrEmpty(file.FilePath))
                                    {
                                        string pathforandriodoutput = file.FilePath;
                                        pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                        pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                        string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                        finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;
                                        file.FilePath = finaloutputforandriodpath;

                                        
                                    }
                                }
                                //UpdateNotificationObj.Add(new UpdateNotification { Message =  lstFiles });
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(JArray.FromObject(lstFiles).ToString(), Encoding.UTF8, "application/json")
                                };
                            }
                           
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // for minutes of Concluded meeting 24 Jun 2021
        [Authorize]
        [Route("ConcludedMeetingMinutes")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ConcludedMeetingMinutes([FromBody] UserDataVM _objdata)
        {   

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<FileDataVM> _objAgendaDocument = new List<FileDataVM>();
                int MeetingAgendaMapping = 0;


                MeetingAgendaMapping = _objdata.MeetingAgendaMappingId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objAgendaDocument = Sec_DocumentManagement.MinutesofConcludedmeeting(_objdata.MeetingId, Convert.ToInt32(user.ID));
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAgendaDocument).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        //

        [Authorize]
        [Route("JoinVideoConfrencingbyDirector")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage JoinVideoConfrencingbyDirector([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<VideoMeetingVM> _objVMeetings = new List<VideoMeetingVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            if (_objdata.MeetingId > 0)
                            {
                                _objVMeetings = MyMeetingManagment.GetJoinVideoConfrencingDetails(user.CustomerID, Convert.ToInt32(user.ID), _objdata.MeetingId);
                            }
                            else
                            {
                                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                                errordetail.Add(new ErrorDetails { ErrorMessage = "MeetingId not found", ErrorDetailMessage = "" });
                            }

                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "EmailId not Found", ErrorDetailMessage = "" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objVMeetings).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // API for Agenda View for PastCircular
        [Authorize]
        [Route("AgendaDetailswithvotingnoting")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AgendaDetailswithvotingnoting([FromBody] UserDataVM _objdata)
        {

            try
            {
                long MeetingId = 0;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
           
                List<MeetingAgendaSummary> _objagendadeals = new List<MeetingAgendaSummary>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                                     MeetingId = _objdata.MeetingId;
                        _objagendadeals = MyMeetingManagment.GetAgendaMeetingwise(MeetingId, Convert.ToInt32(user.ID)); 

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objagendadeals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // End 

        #region Agenda Document for Dorector with noting voting 23 Jul 2021
        [Authorize]
        [Route("AgendaDetailswithvotingnotingDirector")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AgendaDetailswithvotingnotingDirector([FromBody] UserDataVM _objdata)
        {

            try
            {
                long MeetingId = 0;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<MeetingAgendaSummary> _objagendadeals = new List<MeetingAgendaSummary>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        MeetingId = _objdata.MeetingId;
                        _objagendadeals = MyMeetingManagment.GetAgendaMeetingwiseDirector(MeetingId, Convert.ToInt32(user.ID));

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objagendadeals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion  Agenda Document for Director with noting voting 


        #region Voting Sumary with noting voting 23 Jul 2021
        [Authorize]
        [Route("GetAgendaResponseChartData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAgendaResponseChartData([FromBody] UserDataVM _objdata)
        {

            try
            {
                long MeetingId = 0;
                long AgendaId = 0;
                long MappingId = 0;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<AvailabilityResponseChartData_ResultVM> _objagendadeals = new List<AvailabilityResponseChartData_ResultVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        MeetingId = _objdata.MeetingId;
                        AgendaId = _objdata.AgendaId;
                        MappingId = _objdata.MappingId;
                        //_objagendadeals = MyMeetingManagment.GetAgendaMeetingwiseDirector(MeetingId, Convert.ToInt32(user.ID));
                        _objagendadeals = MyMeetingManagment.GetAgendaResponseChartData(MeetingId, AgendaId, MappingId, Convert.ToInt32(user.ID));

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objagendadeals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion  Agenda Document for Director with noting voting 


        #region Interested Party  26 Jul 2021
        [Authorize]
        [Route("GetInterestedParty")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetInterestedParty([FromBody] UserDataVM _objdata)
        {

            try
            {
               
                long meetingAgendaMappingID = 0;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<MeetingAgendaInterestedPartyVM> _objagendadeals = new List<MeetingAgendaInterestedPartyVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                       
                        meetingAgendaMappingID = _objdata.MeetingAgendaMappingId;
                        _objagendadeals = MyMeetingManagment.GetAll(meetingAgendaMappingID, Convert.ToInt32(user.ID));
                        

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objagendadeals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion  Interested party 

        // added for Concluded meeting preview 18 Aug 2021
        [Authorize]
        [Route("ConcludedMeetingMinutesPreviewDocs")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ConcludedMeetingMinutesPreviewDocs([FromBody] UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.previewMinutesfetchpath(_objdata.MeetingId, Convert.ToInt32(user.ID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        //

        //Added for to Get Historical file data list by ID - 11Nov2021
        [Authorize]
        [Route("GetHistoricalMeetingDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetHistoricalMeetingDocuments([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<AgendaDocumentVM> _objAgendaDocument = new List<AgendaDocumentVM>();
                long historicalID = 0;


                historicalID = _objdata.HistoricalID;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objAgendaDocument = MyMeetingManagment.GetHistoricalMeetingDocument(user.CustomerID, Convert.ToInt32(user.ID), historicalID, _objdata.docType);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAgendaDocument).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //End
    }
}
