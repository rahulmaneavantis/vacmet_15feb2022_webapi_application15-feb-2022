﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.BM_Managment.BAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.DirectorDashboardVM;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/DirectorDashboard")]
    public class DirectorDashboardController : ApiController
    {
      

        [Authorize]
        [Route("SecretarialDirectorDashboradCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SecretarialDirectorDashboradCount([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorDashboardCount> count = new List<DirectorDashboardCount>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        count = DirectorDashboardManagment.GetDashboardCountForDirector(user.CustomerID, Convert.ToInt32(user.ID));
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(count).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("SecretarialDirectorDashboradCompliances")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SecretarialDirectorDashboradCompliances([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorCompliancesVM> objCompliances = new List<DirectorCompliancesVM>();
                List<DashboradCount> Compliancecount = new List<DashboradCount>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        
                        int UserId = Convert.ToInt32(user.ID);
                        int CustomerId = Convert.ToInt32(user.CustomerID);
                        int roleId = 3;
                        string FY_Year = _objdata.FY;
                        int EntityId = _objdata.EntityId;
                        int Overdue = 0;
                        int ClosedDelayed = 0;
                        int ClosedTimely = 0;
                        if(string.IsNullOrEmpty(FY_Year))
                        {
                            FY_Year = "CYTD";
                        }
                        objCompliances = DirectorDashboardManagment.GetDirectorComplianceDetails(UserId, CustomerId, roleId, FY_Year, EntityId);
                        if (objCompliances.Count > 0)
                        {
                            objCompliances = objCompliances.Where(row => row.EntityId > 0).ToList();
                        }
                        if(objCompliances.Count>0)
                        {
                            Overdue = objCompliances.Where(x => x.ComplianceStatus == "Overdue").Count();
                            ClosedDelayed = objCompliances.Where(x => x.ComplianceStatus == "Closed-Delayed").Count();
                            ClosedTimely= objCompliances.Where(x => x.ComplianceStatus == "Closed-Timely").Count();
                        }
                        else
                        {
                            Overdue = 0;
                            ClosedDelayed = 0;
                            ClosedTimely = 0;
                        }
                        Compliancecount.Add(new DashboradCount { Overdue = Overdue, ClosedDelayed = ClosedDelayed, ClosedTimely = ClosedTimely });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Compliancecount).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]//filter
        [Route("EntityList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DashboradEntityList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<EntityVM> objEntity = new List<EntityVM>();
             
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        int UserId = Convert.ToInt32(user.ID);
                        int CustomerId = Convert.ToInt32(user.CustomerID);


                        objEntity = DirectorDashboardManagment.GetEntityList(UserId, CustomerId);

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objEntity).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]//filter
        [Route("DashboradFY")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DashboradFY([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<FY_VM> objFY = new List<FY_VM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        int UserId = Convert.ToInt32(user.ID);
                        int CustomerId = Convert.ToInt32(user.CustomerID);


                        objFY = DirectorDashboardManagment.GetFY();

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objFY).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("DirDashboradCompliancesPerformance")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirDashboradCompliancesPerformance([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorCompliancesVM> objCompliances = new List<DirectorCompliancesVM>();
                List<DashboradCount> Compliancecount = new List<DashboradCount>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();


                        int UserId = Convert.ToInt32(user.ID);
                        int CustomerId = Convert.ToInt32(user.CustomerID);
                        int roleId = 3;
                        string FY_Year = _objdata.FY;
                        string ComplianceStatus = _objdata.ComplianceStatus;
                        int EntityId = _objdata.EntityId;                       
                        int Overdue = 0;
                        int ClosedDelayed = 0;
                        int ClosedTimely = 0;
                        if (string.IsNullOrEmpty(FY_Year))
                        {
                            FY_Year = "CYTD";
                        }
                        objCompliances = DirectorDashboardManagment.GetDirComplianceDetails(UserId, CustomerId, roleId, FY_Year);

                        if (_objdata.LstEntitiId != null)
                        {
                            if (_objdata.LstEntitiId.Count > 0)
                            {
                                var listentities = (from row in _objdata.LstEntitiId where row > 0 select row).ToList();

                                if (listentities.Count > 0)
                                {
                                    objCompliances = objCompliances.FindAll(emp => _objdata.LstEntitiId.Contains(emp.EntityId));                                  

                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(_objdata.ComplianceStatus))
                        {
                            objCompliances = objCompliances.FindAll(emp => _objdata.ComplianceStatus.Contains(emp.ComplianceStatus));
                        }

                        //if (objCompliances.Count > 0)
                        //{
                        //    Overdue = objCompliances.Where(x => x.ComplianceStatus == "Overdue").Count();
                        //    ClosedDelayed = objCompliances.Where(x => x.ComplianceStatus == "Closed-Delayed").Count();
                        //    ClosedTimely = objCompliances.Where(x => x.ComplianceStatus == "Closed-Timely").Count();
                        //}
                        //else
                        //{
                        //    Overdue = 0;
                        //    ClosedDelayed = 0;
                        //    ClosedTimely = 0;
                        //}
                        //Compliancecount.Add(new DashboradCount { Overdue = Overdue, ClosedDelayed = ClosedDelayed, ClosedTimely = ClosedTimely });


                    }


                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objCompliances).ToString(), Encoding.UTF8, "application/json")
                    //Content = new StringContent(JArray.FromObject(Compliancecount).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("DirDashboradCompliancesPerformanceCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirDashboradCompliancesPerformanceCount([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorCompliancesVM> objCompliances = new List<DirectorCompliancesVM>();

                List<EntityWiseStatusVM> result = new List<EntityWiseStatusVM>();
                List<DashboradCount> Compliancecount = new List<DashboradCount>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();


                        int UserId = Convert.ToInt32(user.ID);
                        int CustomerId = Convert.ToInt32(user.CustomerID);
                        int roleId = 3;
                        string FY_Year = _objdata.FY;
                        string ComplianceStatus = _objdata.ComplianceStatus;
                        int EntityId = _objdata.EntityId;
                        int Overdue = 0;
                        int ClosedDelayed = 0;
                        int ClosedTimely = 0;
                        if (string.IsNullOrEmpty(FY_Year))
                        {
                            FY_Year = "CYTD";
                        }
                        objCompliances = DirectorDashboardManagment.GetDirComplianceDetails(UserId, CustomerId, roleId, FY_Year);

                        if (_objdata.LstEntitiId != null)
                        {
                            if (_objdata.LstEntitiId.Count > 0)
                            {
                                var listentities = (from row in _objdata.LstEntitiId where row > 0 select row).ToList();

                                if (listentities.Count > 0)
                                {
                                    objCompliances = objCompliances.FindAll(emp => _objdata.LstEntitiId.Contains(emp.EntityId));

                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(_objdata.ComplianceStatus))
                        {
                            objCompliances = objCompliances.FindAll(emp => _objdata.ComplianceStatus.Contains(emp.ComplianceStatus));
                        }

                        var lstEntitiesLookUp = objCompliances.Select(r => new { r.EntityId, r.CompanyName}).Distinct().ToList();
                        if(lstEntitiesLookUp != null)
                        {
                            
                            
                            foreach (var item in lstEntitiesLookUp)
                            {
                                if(item.EntityId > 0)
                                {
                                    var entityItem = new EntityWiseStatusVM();
                                    entityItem.EntityId = item.EntityId;
                                    entityItem.CompanyName = item.CompanyName;

                                    Overdue = objCompliances.Where(x => x.EntityId == item.EntityId && x.ComplianceStatus == "Overdue").Count();
                                    ClosedDelayed = objCompliances.Where(x => x.EntityId == item.EntityId && x.ComplianceStatus == "Closed-Delayed").Count();
                                    ClosedTimely = objCompliances.Where(x => x.EntityId == item.EntityId && x.ComplianceStatus == "Closed-Timely").Count();

                                    //entityItem.lstStatus = new List<DashboradCount>()
                                    entityItem.Overdue = Overdue;
                                    entityItem.ClosedDelayed = ClosedDelayed;
                                    entityItem.ClosedDelayed = ClosedTimely;

                                    result.Add(entityItem);
                                }
                                
                            }
                        }
                        //if (objCompliances.Count > 0)
                        //{
                        //    Overdue = objCompliances.Where(x => x.ComplianceStatus == "Overdue").Count();
                        //    ClosedDelayed = objCompliances.Where(x => x.ComplianceStatus == "Closed-Delayed").Count();
                        //    ClosedTimely = objCompliances.Where(x => x.ComplianceStatus == "Closed-Timely").Count();
                        //}
                        //else
                        //{
                        //    Overdue = 0;
                        //    ClosedDelayed = 0;
                        //    ClosedTimely = 0;
                        //}
                        //Compliancecount.Add(new DashboradCount { Overdue = Overdue, ClosedDelayed = ClosedDelayed, ClosedTimely = ClosedTimely });


                    }


                }
                return new HttpResponseMessage()
                {
                    //Content = new StringContent(JArray.FromObject(objCompliances).ToString(), Encoding.UTF8, "application/json")
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}
