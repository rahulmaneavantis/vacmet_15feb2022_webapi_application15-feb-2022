﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/Authorization")]
    public class AuthorizationController : ApiController
    {
        [Authorize]
        [Route("PageDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                string RoleCode = string.Empty;
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<AuthorizationVM> _objEntityDetals = new List<AuthorizationVM>();
               
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if(user!=null)
                        {
                          RoleCode = (from x in entities.Roles
                                               where x.IsForSecretarial == true
                                               && x.ID == user.SecretarialRoleID
                                               select x.Code).FirstOrDefault();
                        }
                        if (user != null && (!(string.IsNullOrEmpty(RoleCode))))
                        {
                            _objEntityDetals = AuthorizationManagment.GetPageAuthorizationDetails(user.CustomerID, Convert.ToInt32(user.ID), RoleCode);
                        }

                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objEntityDetals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
