﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;

namespace AppWebApplication.BM_Management.Controller
{
    [RoutePrefix("api/CompanyProfile")]
    public class CompanyProfileController : ApiController
    {

        [Authorize]
        [Route("CompanyDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<EntityDetailVM> _objEntityDetals = new List<EntityDetailVM>();
                int EntityID = 0;
                EntityID = _objdata.EntityId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objEntityDetals = CompanyProfileManagment.GetEntityDetails(user.CustomerID, Convert.ToInt32(user.ID), EntityID);
                        }

                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objEntityDetals).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("DirectorProfile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorProfile([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<DirectorDetailsVM> _objDirectorsProfile = new List<DirectorDetailsVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objDirectorsProfile = CompanyProfileManagment.GetDirectorProfile(user.CustomerID, Convert.ToInt32(user.ID));
                            if (_objDirectorsProfile.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objDirectorsProfile.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objDirectorsProfile = _objDirectorsProfile.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorsProfile).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanyDirectorList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyDirectorList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorVM> _objDirectorList = new List<DirectorVM>();
                int EntityID = 0;
                 EntityID = _objdata.EntityId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objDirectorList = CompanyProfileManagment.GetDirectorsList(user.CustomerID, Convert.ToInt32(user.ID), EntityID,-1);

                        if (_objDirectorList.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objDirectorList.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objDirectorList = _objDirectorList.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanyDirectorDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyDirectorDetails([FromBody] UserDataVM _objdata)
        {
            int DirectorID = 0;
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<DirectorDetailsVM> _objDirectorList = new List<DirectorDetailsVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        DirectorID = _objdata.DirectorId;
                        _objDirectorList = CompanyProfileManagment.GetCompanyDirectorDetails(user.CustomerID, DirectorID);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("DirectorDetailsofIntrestList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorDetailsofIntrestList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<DetailsOfInterestVM> _objDirectorDetailsofIntrest = new List<DetailsOfInterestVM>();
                int DirectorId = 0;
                DirectorId = _objdata.DirectorId;
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objDirectorDetailsofIntrest = CompanyProfileManagment.GetDetailsofIntrest(user.CustomerID, DirectorId);

                        if (_objDirectorDetailsofIntrest.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objDirectorDetailsofIntrest.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objDirectorDetailsofIntrest = _objDirectorDetailsofIntrest.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorDetailsofIntrest).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanyCommitteeListOld")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyCommitteeListOld([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<CommitteeListVM> _objCommiteelist = new List<CommitteeListVM>();
                int EntityID = 0;

               EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCommiteelist = CompanyProfileManagment.GetCommiteeList(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCommiteelist.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCommiteelist.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCommiteelist = _objCommiteelist.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCommiteelist).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        #region Commented by Ruchi
        //[Authorize]
        //[Route("CommitteeDetailsList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage CommitteeDetailsList([FromBody] UserDataVM _objdata)
        //{
        //    try
        //    {
        //        var CallerAgent = HttpContext.Current.Request.UserAgent;
        //        string EmailId = getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
        //        //string EmailId = _objdata.EmailId;
        //        List<CommitteeVM> _objCompanyCommitteeDetailsList = new List<CommitteeVM>();
        //        int EntityID = 0;
        //        int CommitteeId = 0;
        //        EntityID = _objdata.EntityId;
        //        CommitteeId = _objdata.CommitteeID;
        //        if (!string.IsNullOrEmpty(EmailId))
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                User user = (from row in entities.Users
        //                             where (row.Email == EmailId)
        //                               && row.IsActive == true
        //                               && row.IsDeleted == false
        //                             select row).FirstOrDefault();
        //                _objCompanyCommitteeDetailsList = CompanyProfileManagment.GetCommitteeDetailsList(user.CustomerID, Convert.ToInt32(user.ID), EntityID, CommitteeId);

        //                if (_objCompanyCommitteeDetailsList.Count > 0)
        //                {
        //                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

        //                    var total = _objCompanyCommitteeDetailsList.Count;
        //                    var pageSize = PageSizeDyanamic;
        //                    var skip = pageSize * (_objdata.PageId - 1);
        //                    var canPage = skip < total;
        //                    _objCompanyCommitteeDetailsList = _objCompanyCommitteeDetailsList.Skip(skip).Take(pageSize).ToList();
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(_objCompanyCommitteeDetailsList).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}
        #endregion

        [Authorize]
        [Route("CompanyPolicy")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyPolicy([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<PolicesVM> _objCompanyPolices = new List<PolicesVM>();
                int EntityID = 0;
                EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCompanyPolices = CompanyProfileManagment.GetCompanyPolices(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCompanyPolices.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCompanyPolices.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCompanyPolices = _objCompanyPolices.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCompanyPolices).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // Added By NK for CompanyPolicyDetails
        [Authorize]
        [Route("CompanyPolicyDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyPolicyDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<CompanyPolicydetailsVM> _objCompanyPolices = new List<CompanyPolicydetailsVM>();
                int EntityID = 0;
                EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCompanyPolices = CompanyProfileManagment.GetCompanyPolicesDetails(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCompanyPolices.Count > 0)
                        {
                            //int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCompanyPolices.Count;
                            //var pageSize = PageSizeDyanamic;
                            //var skip = pageSize * (_objdata.PageId - 1);
                            //var canPage = skip < total;
                            //_objCompanyPolices = _objCompanyPolices.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCompanyPolices).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanyPolicyDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyPolicyDocument([FromBody]UserDataVM _objdata)
        {
        
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchpolicespath(_objdata.PolicesId, Convert.ToInt32(user.ID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("CompanyGroupDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyGroupDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<CompanyGroupVM> _objCompanyGropDetails = new List<CompanyGroupVM>();
                int EntityID = 0;
                 EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCompanyGropDetails = CompanyProfileManagment.GetCompanyGropDetails(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCompanyGropDetails.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCompanyGropDetails.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCompanyGropDetails = _objCompanyGropDetails.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCompanyGropDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanySharesDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanySharesDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<SharesDetailVM> _objCompanysharesDetails = new List<SharesDetailVM>();
                int EntityID = 0;
                EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCompanysharesDetails = CompanyProfileManagment.GetCompanysharesDetails(user.CustomerID, Convert.ToInt32(user.ID), EntityID);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCompanysharesDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //Added for Committee details 10Jun2021
        [Authorize]
        [Route("GetCommitteeDtls")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCommitteeDtls([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
               
                List<CommitteeMasterVM> _objCommitteeMaster = new List<CommitteeMasterVM>();
                int DirectorId = 0;
                DirectorId = _objdata.DirectorId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCommitteeMaster = CompanyProfileManagment.GetCommitteeMasterdtls(user.CustomerID, Convert.ToInt32(user.ID), DirectorId);

                        if (_objCommitteeMaster.Count > 0)
                        {
                            //int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCommitteeMaster.Count;
                            //var pageSize = PageSizeDyanamic;
                            //var skip = pageSize * (_objdata.PageId - 1);
                            //var canPage = skip < total;
                            //_objCompanyPolices = _objCompanyPolices.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCommitteeMaster).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //Added fro Committee Matrix 11June2021
        #region Committee Matrix
        [Authorize]
        [Route("GetCommitteeMatrix")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCommitteeMatrix([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                
                List<CommitteeMatrixVM> _objCommitteeMatrix = new List<CommitteeMatrixVM>();
                int EntityID = 0;
                EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCommitteeMatrix = CompanyProfileManagment.GetCommitteeMatrix(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCommitteeMatrix.Count > 0)
                        {
                            //int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCommitteeMatrix.Count;
                            //var pageSize = PageSizeDyanamic;
                            //var skip = pageSize * (_objdata.PageId - 1);
                            //var canPage = skip < total;
                            //_objCompanyPolices = _objCompanyPolices.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCommitteeMatrix).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        // added for Director Info 14 jun 2021
        [Authorize]
        [Route("DirectorInfo")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DirectorInfo([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);

                List<DirectorDetailsVM> _objDirectorsProfile = new List<DirectorDetailsVM>();

                int DirectorID = 0;
                DirectorID = _objdata.DirectorId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objDirectorsProfile = CompanyProfileManagment.GetDirectorInfo(user.CustomerID, DirectorID);
                            if (_objDirectorsProfile.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = _objDirectorsProfile.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (_objdata.PageId - 1);
                                var canPage = skip < total;
                                _objDirectorsProfile = _objDirectorsProfile.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objDirectorsProfile).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CompanyCommitteeList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompanyCommitteeList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                List<CommitteeListVM> _objCommiteelist = new List<CommitteeListVM>();
                int EntityID = 0;

                EntityID = _objdata.EntityId;

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        _objCommiteelist = CompanyProfileManagment.GetCommiteeListNew(user.CustomerID, Convert.ToInt32(user.ID), EntityID);

                        if (_objCommiteelist.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = _objCommiteelist.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            _objCommiteelist = _objCommiteelist.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objCommiteelist).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
