﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.BM_Managment.BAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.TaskVM;

namespace AppWebApplication.BM_Managment.Controller
{
   [RoutePrefix("api/Task")]
    public class TaskController : ApiController
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();


        [Authorize]
        [Route("MyTaskList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
               
                List<TaskDetails> objResult = new List<TaskDetails>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    User user = (from row in entities.Users
                                 where (row.Email == EmailId)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {

                        objResult = BAL.TaskManagment.GetTaskDetails(Convert.ToInt32(user.ID));

                        if(objResult.Count>0)
                        {
                            if (_objdata.Tasksearch != null && _objdata.Tasksearch != "")
                            {
                                objResult = objResult.Where(x => x.TaskTitle.ToLower().Contains(_objdata.Tasksearch.ToLower()) || x.status.ToLower().Contains(_objdata.Tasksearch.ToLower()) || x.TaskTypes.ToLower().Contains(_objdata.Tasksearch.ToLower()))
                                    //.Where(y=> _objdata.Tasksearch.ToLower().Contains(y.status.ToLower()))
                                    .ToList();
                            }
                        }
                        if (objResult.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = objResult.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            objResult = objResult.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("TaskEditDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskEditDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<TaskPerandRevData> objResult = new List<TaskPerandRevData>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    User user = (from row in entities.Users
                                 where (row.Email == EmailId)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        objResult = BAL.TaskManagment.EditTaskDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), _objdata.TaskId);

                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("TaskUpdatedDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskUpdatedDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<StatusRemarkDetailsVM> objResult = new List<StatusRemarkDetailsVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    User user = (from row in entities.Users
                                 where (row.Email == EmailId)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        objResult = BAL.TaskManagment.GetTaskUpdatedDetails(_objdata.TaskId);

                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("TaskStatusLog")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskStatusLog([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<StatusLogVM> objResult = new List<StatusLogVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {

                    User user = (from row in entities.Users
                                 where (row.Email == EmailId)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        objResult = BAL.TaskManagment.GetTaskStatuslogDetails(_objdata.TaskId);

                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("TaskEntityList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskEntityList([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<EntityVM> _objAct = new List<EntityVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = BAL.TaskManagment.GetTaskEntityDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));


                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occurred" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("TaskUsersDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskEntityUsers([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UserVM> _objAct = new List<UserVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = BAL.TaskManagment.GetTaskUserDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("TaskMeetingDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskMeetingDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<TaskMeetingVM> _objAct = new List<TaskMeetingVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = BAL.TaskManagment.GetTaskMeetingDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), 1);
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("TaskAgendaDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskAgendaDetails([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<TaskAgendagVM> _objAct = new List<TaskAgendagVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = BAL.TaskManagment.GetTaskAgendaDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID),1, _objdata.MeetingId);
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("TaskType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskType([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<TaskTypeDetailsVM> _objAct = new List<TaskTypeDetailsVM>();
                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            _objAct = BAL.TaskManagment.GetTaskTypeDetails(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));
                        }
                        else
                        {
                            List<ErrorDetails> errordetail = new List<ErrorDetails>();
                            errordetail.Add(new ErrorDetails { ErrorMessage = "Email Id not found", ErrorDetailMessage = "" });
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                            };
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objAct).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("CreateTaskDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CreateTaskDetail(string EmailID,int EntityId,string TaskTitle,DateTime DueDate,int TaskTypeId,string TaskDescription,int MeetingId,int AgendaId,string TaskLevel,int UserID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            var httpRequest = HttpContext.Current.Request;
            try
            {
               string key = "avantis";
      
                string Email = EmailID.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                long TaskId = 0;
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        User user = (from row in entities.Users
                                     where (row.Email == Email)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int UserId = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);
                            int RoleId = Convert.ToInt32(user.SecretarialRoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId && row.IsForSecretarial == true
                                                select row.Code).FirstOrDefault();

                            var CheckExist = (from row in entities.BM_Task
                                              where row.EntityId == EntityId && row.TaskTitle == TaskTitle
                                              select row).FirstOrDefault();
                            #region Add Task Details
                            if (CheckExist == null)
                            {
                                if (DueDate != null)
                                {
                                    #region Task Add
                                    BM_Task bm_task = new BM_Task();
                                    if (EntityId > 0)
                                    {
                                        bm_task.EntityId = (long)EntityId;
                                    }
                                    bm_task.CustomerId = customerId;
                                    bm_task.TaskType = TaskTypeId;
                                    bm_task.TaskTitle = TaskTitle;
                                    if (TaskDescription == null)
                                    {
                                        bm_task.TaskDesc = "";
                                    }
                                    else
                                    {
                                        bm_task.TaskDesc = TaskDescription;
                                    }
                                    bm_task.AssignOn = DateTime.Now;
                                    bm_task.DueDate = DueDate;
                                    if (TaskTypeId == 1)
                                    {
                                        bm_task.MeetingId = MeetingId;
                                    }
                                    else if (TaskTypeId == 2)
                                    {
                                        bm_task.MeetingId = MeetingId;
                                        bm_task.AgendaId = AgendaId;
                                    }
                                    if (TaskLevel == "M" && RoleCheck.ToUpper() == "DRCTR")
                                    {
                                        bm_task.IsATR = true;
                                    }
                                    else
                                    {
                                        bm_task.IsATR = false;
                                    }

                                    bm_task.IsActive = true;
                                    bm_task.CreatedOn = DateTime.Now;
                                    bm_task.Createdby = Convert.ToInt32(user.ID);
                                    bm_task.status = "Pending";
                                    entities.BM_Task.Add(bm_task);
                                    entities.SaveChanges();
                                    #endregion

                                    TaskId = bm_task.ID;
                                    #region Add File 
                                   
                                    if (httpRequest.Files.Count>0 && TaskId > 0)
                                    {
                                        string fileName = string.Empty;
                                        string directoryPath = string.Empty;

                                        for (int i = 0; i < httpRequest.Files.Count; i++)
                                        {
                                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();


                                            HttpPostedFile uploadedFile = httpRequest.Files[i];
                                            fileName = uploadedFile.FileName;
                                            string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                            string FileLocationPath = "/Areas/Document/" + customerId + "/" +"/Task/";

                                            string FileLocationPathforconvert = (FileLocationPath).ToString();
                                            FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                            FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                            directoryPath = destinationPath + FileLocationPathforconvert;
                                            Directory.CreateDirectory(directoryPath);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                           
                                            SaveDocFiles(Filelist1);

                                            BM_FileData _objTaskDocument = new BM_FileData();
                                            _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            _objTaskDocument.FileKey = fileKey1.ToString();
                                            _objTaskDocument.FileName = fileName;
                                            _objTaskDocument.TaskId = TaskId;
                                            _objTaskDocument.UploadedBy = Convert.ToInt32(user.ID);
                                            _objTaskDocument.UploadedOn = DateTime.Now;
                                            if (uploadedFile.ContentLength > 0)
                                                _objTaskDocument.FileSize = (uploadedFile.ContentLength).ToString();
                                            entities.BM_FileData.Add(_objTaskDocument);
                                            entities.SaveChanges();
                                        }
                                    }
                                   
                                    #endregion

                                    #region Task Assignment
                                    if (TaskId > 0)
                                    {
                                        BM_TaskAssignment objtaskAssignment = new BM_TaskAssignment();
                                        objtaskAssignment.UserID = user.ID;
                                        objtaskAssignment.RoleId = 4;
                                        objtaskAssignment.TaskInstanceID = TaskId;
                                        objtaskAssignment.CreatedOn = DateTime.Now;
                                        objtaskAssignment.Createdby = Convert.ToInt32(user.ID);
                                        objtaskAssignment.IsActive = true;
                                        entities.BM_TaskAssignment.Add(objtaskAssignment);
                                        entities.SaveChanges();
                                        if (objtaskAssignment.ID > 0)
                                        {
                                            BM_TaskAssignment objtaskAssignmentper = new BM_TaskAssignment();
                                            objtaskAssignmentper.UserID = UserID;
                                            objtaskAssignmentper.RoleId = 3;
                                            objtaskAssignmentper.TaskInstanceID = TaskId;
                                            objtaskAssignmentper.CreatedOn = DateTime.Now;
                                            objtaskAssignmentper.Createdby = Convert.ToInt32(user.ID);
                                            objtaskAssignmentper.IsActive = true;
                                            entities.BM_TaskAssignment.Add(objtaskAssignmentper);
                                            entities.SaveChanges();
                                        }

                                    }
                                    #endregion
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Task added successfully" });
                                }
                                #endregion
                                else
                                {
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Please select task due date" });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Task already exist" });
                            }
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }

                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                UpdateNotificationObj.Clear();
                UpdateNotificationObj.Add(new UpdateNotification { Message = "Server error occured" });
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        public  void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }


        //[Authorize]
        //[Route("CreateTaskDetail")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage CreateTaskDetail([FromBody] AddTaskDetails_VM _objtask)
        //{
        //    List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

        //    try
        //    {
        //        string Email = _objtask.EmailId.Replace(" ", "+");
        //        var CallerAgent = HttpContext.Current.Request.UserAgent;
        //        string EmailId = UserManagemet.getDecryptedText(Email.ToString(), CallerAgent);
        //        long TaskId = 0;
        //        if (!string.IsNullOrEmpty(EmailId))
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {

        //                User user = (from row in entities.Users
        //                             where (row.Email == EmailId)
        //                               && row.IsActive == true
        //                               && row.IsDeleted == false
        //                             select row).FirstOrDefault();
        //                if (user != null)
        //                {
        //                    int UserId = Convert.ToInt32(user.ID);
        //                    int customerId = Convert.ToInt32(user.CustomerID);
        //                    int RoleId = Convert.ToInt32(user.SecretarialRoleID);
        //                    string RoleCheck = (from row in entities.Roles
        //                                        where row.ID == RoleId && row.IsForSecretarial == true
        //                                        select row.Code).FirstOrDefault();

        //                    var CheckExist = (from row in entities.BM_Task
        //                                      where row.EntityId == _objtask.EntityId && row.TaskTitle == _objtask.TaskTitle
        //                                      select row).FirstOrDefault();
        //                    #region Add Task Details
        //                    if (CheckExist == null)
        //                    {
        //                        if (_objtask.DueDate != null)
        //                        {
        //                            #region Task Add
        //                            BM_Task bm_task = new BM_Task();
        //                            if (_objtask.EntityId > 0)
        //                            {
        //                                bm_task.EntityId = (long)_objtask.EntityId;
        //                            }
        //                            bm_task.CustomerId = customerId;
        //                            bm_task.TaskType = _objtask.TaskTypeId;
        //                            bm_task.TaskTitle = _objtask.TaskTitle;
        //                            if (_objtask.TaskDescription == null)
        //                            {
        //                                bm_task.TaskDesc = "";
        //                            }
        //                            else
        //                            {
        //                                bm_task.TaskDesc = _objtask.TaskDescription;
        //                            }
        //                            bm_task.AssignOn = DateTime.Now;
        //                            bm_task.DueDate = _objtask.DueDate;
        //                            if (_objtask.TaskTypeId == 1)
        //                            {
        //                                bm_task.MeetingId = _objtask.MeetingId;
        //                            }
        //                            else if (_objtask.TaskTypeId == 2)
        //                            {
        //                                bm_task.MeetingId = _objtask.MeetingId;
        //                                bm_task.AgendaId = _objtask.AgendaId;
        //                            }
        //                            if (_objtask.TaskLevel == "M" && RoleCheck.ToUpper() == "DRCTR")
        //                            {
        //                                bm_task.IsATR = true;
        //                            }
        //                            else
        //                            {
        //                                bm_task.IsATR = false;
        //                            }

        //                            bm_task.IsActive = true;
        //                            bm_task.CreatedOn = DateTime.Now;
        //                            bm_task.Createdby = Convert.ToInt32(user.ID);
        //                            bm_task.status = "Pending";
        //                            entities.BM_Task.Add(bm_task);
        //                            entities.SaveChanges();
        //                            #endregion

        //                            TaskId = bm_task.ID;
        //                            #region Add File 
        //                            var httpRequest = HttpContext.Current.Request;
        //                            if (httpRequest.Files != null && TaskId > 0)
        //                            {
        //                                string fileName = string.Empty;
        //                                string directoryPath = string.Empty;

        //                                for (int i = 0; i < httpRequest.Files.Count; i++)
        //                                {
        //                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();


        //                                    HttpPostedFile uploadedFile = httpRequest.Files[i];
        //                                    fileName = uploadedFile.FileName;
        //                                    string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
        //                                    string FileLocationPath = "/SecterialDocuments/" + customerId + "/" + Convert.ToInt32(_objtask.Id) + "/Task/";

        //                                    string FileLocationPathforconvert = (FileLocationPath).ToString();
        //                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
        //                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
        //                                    directoryPath = destinationPath + FileLocationPathforconvert;
        //                                    Directory.CreateDirectory(directoryPath);

        //                                    if (!Directory.Exists(directoryPath))
        //                                        Directory.CreateDirectory(directoryPath);

        //                                    Guid fileKey1 = Guid.NewGuid();
        //                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
        //                                    Stream fs = uploadedFile.InputStream;
        //                                    BinaryReader br = new BinaryReader(fs);
        //                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
        //                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

        //                                    BM_FileData _objTaskDocument = new BM_FileData();
        //                                    _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
        //                                    _objTaskDocument.FileKey = fileKey1.ToString();
        //                                    _objTaskDocument.FileName = fileName;
        //                                    _objTaskDocument.TaskId = TaskId;
        //                                    _objTaskDocument.UploadedBy = Convert.ToInt32(user.ID);
        //                                    _objTaskDocument.UploadedOn = DateTime.Now;
        //                                    if (uploadedFile.ContentLength > 0)
        //                                        _objTaskDocument.FileSize = (uploadedFile.ContentLength).ToString();
        //                                    entities.BM_FileData.Add(_objTaskDocument);
        //                                    entities.SaveChanges();

        //                                }
        //                            }
        //                            #endregion


        //                            #region Task Assignment
        //                            if (TaskId > 0)
        //                            {
        //                                BM_TaskAssignment objtaskAssignment = new BM_TaskAssignment();
        //                                objtaskAssignment.UserID = user.ID;
        //                                objtaskAssignment.RoleId = 4;
        //                                objtaskAssignment.TaskInstanceID = TaskId;
        //                                objtaskAssignment.CreatedOn = DateTime.Now;
        //                                objtaskAssignment.Createdby = Convert.ToInt32(user.ID);
        //                                objtaskAssignment.IsActive = true;
        //                                entities.BM_TaskAssignment.Add(objtaskAssignment);
        //                                entities.SaveChanges();
        //                                if (objtaskAssignment.ID > 0)
        //                                {
        //                                    BM_TaskAssignment objtaskAssignmentper = new BM_TaskAssignment();
        //                                    objtaskAssignmentper.UserID = _objtask.UserId;
        //                                    objtaskAssignmentper.RoleId = 3;
        //                                    objtaskAssignmentper.TaskInstanceID = TaskId;
        //                                    objtaskAssignmentper.CreatedOn = DateTime.Now;
        //                                    objtaskAssignmentper.Createdby = Convert.ToInt32(user.ID);
        //                                    objtaskAssignmentper.IsActive = true;
        //                                    entities.BM_TaskAssignment.Add(objtaskAssignmentper);
        //                                    entities.SaveChanges();
        //                                }

        //                            }
        //                            #endregion
        //                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Task added successfully" });
        //                        }
        //                        #endregion
        //                        else
        //                        {
        //                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Please select task due date" });
        //                        }
        //                    }
        //                    else
        //                    {
        //                        UpdateNotificationObj.Add(new UpdateNotification { Message = "Task already exist" });
        //                    }
        //                }
        //                else
        //                {
        //                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
        //                }
        //            }

        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        UpdateNotificationObj.Clear();
        //        UpdateNotificationObj.Add(new UpdateNotification { Message = "Server error occured" });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}



        [Authorize]
        [Route("TaskDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TaskDocuments([FromBody]UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchTaskpath(_objdata.FileId, Convert.ToInt32(user.ID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "file not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "file not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("UpdateTaskStatusDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateTaskStatusDetail(string EmailID,string Status,long TaskId,string Remark)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            var httpRequest = HttpContext.Current.Request;
            try
            {
                string key = "avantis";

                string Email = EmailID.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
               
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        User user = (from row in entities.Users
                                     where (row.Email == Email)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int UserId = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);
                            int RoleId = Convert.ToInt32(user.SecretarialRoleID);


                            var checkRole = (from row in entities.BM_TaskAssignment
                                             where row.TaskInstanceID == TaskId && row.UserID == UserId
                                             select row).FirstOrDefault();
                            if (checkRole != null)
                                #region Add Preformer
                                if (checkRole.RoleId == 3)
                                {
                                    Status = "Submitted";

                                    if (checkRole.ID > 0)
                                    {
                                        var getTasassignkData = (from row in entities.BM_TaskAssignment where row.TaskInstanceID == TaskId && row.UserID == UserId select row).FirstOrDefault();
                                        var getTaskData = (from row in entities.BM_Task where row.ID == TaskId && row.IsActive == true select row).FirstOrDefault();
                                        //   var getTaskremark = (from x in entities.BM_TaskAssignment where x.TaskId == getTaskData.ID  select x).FirstOrDefault();
                                        if (getTaskData != null)
                                        {
                                            getTasassignkData.Remark = Remark;
                                            entities.SaveChanges();

                                            if (getTaskData != null)
                                            {
                                                getTaskData.status = Status;
                                                entities.SaveChanges();
                                            }
                                            if (Status.ToLower() == "approved")
                                            {
                                                getTaskData.Isclose = true;
                                                entities.SaveChanges();
                                            }
                                            BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                                            bm_taskTransaction.TaskID = TaskId;
                                            bm_taskTransaction.IsActive = true;
                                            bm_taskTransaction.Status = Remark;
                                            bm_taskTransaction.StatusChangeOn = DateTime.Now;
                                            bm_taskTransaction.UserID = UserId;
                                            bm_taskTransaction.RoleID = checkRole.RoleId;
                                            bm_taskTransaction.CreatedOn = DateTime.Now;
                                            bm_taskTransaction.CreatedBy = UserId;
                                            entities.BM_TaskTransaction.Add(bm_taskTransaction);

                                            entities.SaveChanges();

                                            #region Add File 

                                            if (httpRequest.Files.Count > 0 && TaskId > 0)
                                            {
                                                string fileName = string.Empty;
                                                string directoryPath = string.Empty;

                                                for (int i = 0; i < httpRequest.Files.Count; i++)
                                                {
                                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();


                                                    HttpPostedFile uploadedFile = httpRequest.Files[i];
                                                    fileName = uploadedFile.FileName;
                                                    string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                                    string FileLocationPath = "/Areas/Document/" + customerId + "/" +"Task/";

                                                    string FileLocationPathforconvert = (FileLocationPath).ToString();
                                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                                    directoryPath = destinationPath + FileLocationPathforconvert;
                                                    Directory.CreateDirectory(directoryPath);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                                    

                                                    SaveDocFiles(Filelist1);
                                                    BM_FileData _objTaskDocument = new BM_FileData();
                                                    _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    _objTaskDocument.FileKey = fileKey1.ToString();
                                                    _objTaskDocument.FileName = fileName;
                                                    _objTaskDocument.TaskId = TaskId;
                                                    _objTaskDocument.UploadedBy = Convert.ToInt32(user.ID);
                                                    _objTaskDocument.UploadedOn = DateTime.Now;
                                                    if (uploadedFile.ContentLength > 0)
                                                        _objTaskDocument.FileSize = (uploadedFile.ContentLength).ToString();
                                                    entities.BM_FileData.Add(_objTaskDocument);
                                                    entities.SaveChanges();

                                                }
                                            }
                                            else
                                            {
                                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File attachment not found" });
                                            }
                                            #endregion

                                        }
                                    }
                                }
                                #endregion
                                #region Check Reviewer
                                else if (checkRole.RoleId == 4)
                                {
                                    // _objtask.Status = "Submitted";

                                    if (checkRole.ID > 0)
                                    {
                                        var getTasassignkData = (from row in entities.BM_TaskAssignment where row.TaskInstanceID == TaskId && row.UserID == UserId select row).FirstOrDefault();
                                        var getTaskData = (from row in entities.BM_Task where row.ID == TaskId && row.IsActive == true select row).FirstOrDefault();
                                        //   var getTaskremark = (from x in entities.BM_TaskAssignment where x.TaskId == getTaskData.ID  select x).FirstOrDefault();
                                        if (getTaskData != null)
                                        {
                                            getTasassignkData.Remark = Remark;
                                            entities.SaveChanges();

                                            if (getTaskData != null)
                                            {
                                                getTaskData.status = Status;
                                                entities.SaveChanges();
                                            }
                                            if (Status.ToLower() == "approved")
                                            {
                                                getTaskData.Isclose = true;
                                                entities.SaveChanges();
                                            }
                                            BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                                            bm_taskTransaction.TaskID = TaskId;
                                            bm_taskTransaction.IsActive = true;
                                            bm_taskTransaction.Status = Remark;
                                            bm_taskTransaction.StatusChangeOn = DateTime.Now;
                                            bm_taskTransaction.UserID = UserId;
                                            bm_taskTransaction.RoleID = checkRole.RoleId;
                                            bm_taskTransaction.CreatedOn = DateTime.Now;
                                            bm_taskTransaction.CreatedBy = UserId;
                                            entities.BM_TaskTransaction.Add(bm_taskTransaction);

                                            entities.SaveChanges();
                                            #region Add File 

                                            if (httpRequest.Files.Count > 0 && TaskId > 0)
                                            {
                                                string fileName = string.Empty;
                                                string directoryPath = string.Empty;

                                                for (int i = 0; i < httpRequest.Files.Count; i++)
                                                {
                                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();


                                                    HttpPostedFile uploadedFile = httpRequest.Files[i];
                                                    fileName = uploadedFile.FileName;
                                                    string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                                    string FileLocationPath = "/Areas/Document/"+ customerId+"/"+"Task/";

                                                    string FileLocationPathforconvert = (FileLocationPath).ToString();
                                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                                    directoryPath = destinationPath + FileLocationPathforconvert;
                                                    Directory.CreateDirectory(directoryPath);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                                  
                                                    SaveDocFiles(Filelist1);
                                                    BM_FileData _objTaskDocument = new BM_FileData();
                                                    _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    _objTaskDocument.FileKey = fileKey1.ToString();
                                                    _objTaskDocument.FileName = fileName;
                                                    _objTaskDocument.TaskId = TaskId;
                                                    _objTaskDocument.UploadedBy = Convert.ToInt32(user.ID);
                                                    _objTaskDocument.UploadedOn = DateTime.Now;
                                                    if (uploadedFile.ContentLength > 0)
                                                        _objTaskDocument.FileSize = (uploadedFile.ContentLength).ToString();
                                                    entities.BM_FileData.Add(_objTaskDocument);
                                                    entities.SaveChanges();

                                                }
                                            }
                                            else
                                            {
                                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File attachment not found" });
                                            }
                                            #endregion

                                        }
                                    }
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Saved successfully" });
                                }
                            #endregion
                           

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "EmailId not found" });

                        }


                    }

                }
                return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                    };
               
            }
            catch (Exception ex)
            {
                UpdateNotificationObj.Clear();
                UpdateNotificationObj.Add(new UpdateNotification { Message = "server error occured" });
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
