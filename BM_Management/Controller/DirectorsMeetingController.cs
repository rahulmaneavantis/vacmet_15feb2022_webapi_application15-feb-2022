﻿using AppWebApplication.BM_Management.BAL;
using AppWebApplication.BM_Management.DAL;
using AppWebApplication.BM_Managment.BAL;
using AppWebApplication.Data;
using AppWebApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using static AppWebApplication.BM_Management.DAL.AnnotationVM;
using static AppWebApplication.BM_Managment.BAL.BM_LoggerMessage;
using static AppWebApplication.BM_Managment.DAL.DirectorsMeetingVM;

namespace AppWebApplication.BM_Managment.Controller
{
    [RoutePrefix("api/DirectorsMeeting")]
    public class DirectorsMeetingController : ApiController
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();

        [Authorize]
        [Route("MyUpcomingMeeting")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyUpcomingMeeting([FromBody]userfilter _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                //string EmailId = _objdata.EmailId;
                var objResult = new List<MeetingListVM>();


                if (!string.IsNullOrEmpty(EmailId))
                {

                    User user = (from row in entities.Users

                                 where (row.Email == EmailId)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {


                        objResult = DirectorsMeetingManagment.getUpcomingMeeting(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), "DRCTR");

                        if (objResult.Count > 0)
                        {
                            if (_objdata.EntityId != null)
                            {
                                if (_objdata.EntityId.Count > 0)
                                {

                                    //objResult = objResult.Where(x => x.EntityId == _objdata.EntityId).ToList();
                                    // objResult.Where(x => _objdata.EntityId.Contains(x.EntityId));
                                    objResult= objResult.FindAll(emp => _objdata.EntityId.Contains(emp.EntityId));
                                }
                            }

                            if (_objdata.MeetingId != null)
                            {
                                if (_objdata.MeetingId.Count > 0)
                                {

                                    objResult = objResult.FindAll(emp => _objdata.MeetingId.Contains(emp.MeetingID));

                                }
                            }
                        }

                        if (objResult.Count > 0)
                        {
                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            var total = objResult.Count;
                            var pageSize = PageSizeDyanamic;
                            var skip = pageSize * (_objdata.PageId - 1);
                            var canPage = skip < total;
                            objResult = objResult.Skip(skip).Take(pageSize).ToList();
                        }
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("GetAgendaDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAgendaDocument([FromBody]UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchpathAgendaDoc(_objdata.MeetingId, Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("GetAgendapdftronDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAgendapdftronDocument([FromBody]UserDataVM _objdata)
        {

            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                List<AgendaMinutesCommentsVM> _objagendaminutes = new List<AgendaMinutesCommentsVM>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            string pathforandriod = Sec_DocumentManagement.fetchpathAgendapdftronDoc(_objdata.MeetingId, Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), "DRCTR", _objdata.ParticipantId, _objdata.docType, _objdata.draftCirculationID, _objdata.viewOnly);
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                var result = (from row in entities.BM_MeetingAgendaMinutesComments
                                              where row.MeetingId == _objdata.MeetingId && row.DocType == _objdata.docType &&
                                              row.IsActive == true
                                              select new AgendaMinutesCommentsVM
                                              {
                                                  AgendaMinutesCommentsId = row.Id,
                                                  MeetingId = row.MeetingId,
                                                  DocType = row.DocType,
                                                  FilePath = row.FilePath,
                                                  CommentData = row.CommentData,
                                                  MeetingParticipantId = _objdata.ParticipantId
                                              }).FirstOrDefault();
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                var agendaminutes = new AgendaMinutesCommentsVM { FilePath = result.DocURL, MeetingId = _objdata.MeetingId, MeetingParticipantId = _objdata.ParticipantId, IsCS = false, UserName = user.FirstName + " " + user.LastName, DocType = _objdata.docType, AgendaMinutesCommentsId = result.AgendaMinutesCommentsId, DocURL = finaloutputforandriodpath, UserID = user.ID };
                                
                                agendaminutes.ViewOnly = _objdata.viewOnly;

                                if (_objdata.draftCirculationID > 0)
                                {
                                    agendaminutes.DraftCirculationID_ = _objdata.draftCirculationID;
                                    agendaminutes.IsDraftMinutesApproved = Sec_DocumentManagement.IsDraftMinutesApproved(Convert.ToInt64(_objdata.draftCirculationID));
                                }

                                _objagendaminutes.Add(agendaminutes);
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(_objagendaminutes).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("GetAgendaDocumentComments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAgendaDocumentComments([FromBody]UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            if (_objdata.AgendaComentId > 0)
                            {

                                if (_objdata.AgendaComentId > 0)
                                {
                                    string xfdfData = GetAnnote(_objdata.AgendaComentId);
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = xfdfData });
                                }
                                else
                                {
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "" });
                                }

                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };

            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("SaveAgendaDocumentComments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SaveAgendaDocumentComments([FromBody]UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();


                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            if (!(string.IsNullOrEmpty(_objdata.AgendaComments)) && _objdata.MeetingId > 0 && _objdata.ParticipantId > 0)
                            {


                                var _obj = (from row in entities.BM_MeetingAgendaMinutesComments
                                            where row.MeetingId == _objdata.MeetingId && row.DocType == _objdata.docType &&
                                            row.IsActive == true
                                            select row).FirstOrDefault();
                                if (_obj != null)
                                {
                                    _obj.CommentData = _objdata.AgendaComments;
                                    _obj.UpdatedBy = (int)user.ID;
                                    _obj.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    _obj = new BM_MeetingAgendaMinutesComments();
                                    _obj.MeetingId = _objdata.MeetingId;
                                    _obj.DocType = _objdata.docType;
                                    _obj.FilePath = _objdata.DocPath;
                                    _obj.CommentData = _objdata.AgendaComments;
                                    _obj.IsActive = true;
                                    _obj.CreatedBy = (int)user.ID;
                                    _obj.CreatedOn = DateTime.Now;
                                    entities.BM_MeetingAgendaMinutesComments.Add(_obj);
                                    entities.SaveChanges();
                                }

                                #region Transaction
                                if (_obj != null)
                                {
                                    if (_obj.Id > 0)
                                    {
                                        var _objTransaction = new BM_MeetingAgendaMinutesCommentsTransaction();
                                        _objTransaction.AgendaMinutesCommentsId = _obj.Id;
                                        _objTransaction.MeetingParticipantId = _objdata.ParticipantId;
                                        _objTransaction.CommentData = _objdata.AgendaComments;
                                        _objTransaction.CreatedBy = (int)user.ID;
                                        _objTransaction.CreatedOn = DateTime.Now;
                                        entities.BM_MeetingAgendaMinutesCommentsTransaction.Add(_objTransaction);
                                        entities.SaveChanges();
                                    }
                                }
                                #endregion


                                #region Save indivisual Director comments in old table
                                if (_objdata.draftCirculationID > 0)
                                {
                                    try
                                    {
                                        var minutesDetails = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                                              where row.ID == _objdata.draftCirculationID && row.IsDeleted == false
                                                              select row).FirstOrDefault();
                                        if (minutesDetails != null)
                                        {
                                            minutesDetails.DraftComments = _objdata.AgendaComments;
                                            minutesDetails.UpdatedBy = (int)user.ID;
                                            minutesDetails.UpdatedOn = DateTime.Now;
                                            entities.SaveChanges();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                #endregion

                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Comment saved successfully" });

                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Comment is not found" });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };

            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        public string GetAnnote(long id)
        {
            var result = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.BM_MeetingAgendaMinutesComments
                              where row.Id == id && row.IsActive == true
                              select row.CommentData).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        //For TO get the path of Agenda document for downlaod 16Jun2021
        [Authorize]
        [Route("GetAgendaDocumentDownload")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAgendaDocumentDownload([FromBody] UserDataVM _objdata)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string EmailId = UserManagemet.getDecryptedText(_objdata.EmailId.ToString(), CallerAgent);
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(EmailId))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == EmailId)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {


                            // string pathforandriod = Sec_DocumentManagement.fetchpathAgendaDoc(_objdata.MeetingId, Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));
                            string pathforandriod = Sec_DocumentManagement.GetSecretarialFile(_objdata.FileId, Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "File not found" });
                            }

                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Email Id not found" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertPathlog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "Server error occured" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
