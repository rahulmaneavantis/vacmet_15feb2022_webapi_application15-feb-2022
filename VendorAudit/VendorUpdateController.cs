﻿using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using AppWebApplication.Models;
using AppWebApplication.VendorAudit.Model;
using AppWebApplication.VendorAuditModel;
using Ionic.Zip;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VendorAuditData;

namespace AppWebApplication.VendorAudit
{
    public class VendorUpdateController : Controller
    {
        string filepath = ConfigurationManager.AppSettings["ComplianceDocument"];
        string message = "";
        HttpResponseMessage ResponseMessage = null;
        DataSet dsexcelRecords = new DataSet();
        HttpPostedFileBase Inputfile = null;
        Stream FileStream = null;

        public class ContractorProjectPercentage
        {
            public int ProjectID { get; set; }
            public long ContractProjMappingID { get; set; }
            public string ProjectName { get; set; }
            public int ContractorID { get; set; }
            public string ContractorName { get; set; }
            public string ForMonth { get; set; }
            public string Apr { get; set; }
            public string May { get; set; }
            public string Jun { get; set; }
            public string Jul { get; set; }
            public string Aug { get; set; }
            public string Sep { get; set; }
            public string Oct { get; set; }
            public string Nov { get; set; }
            public string Dec { get; set; }
            public string Jan { get; set; }
            public string Feb { get; set; }
            public string Mar { get; set; }
        }

        public ActionResult GetAllContractorProject(int UID, int CID, string role, string Period)
        {
            List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            List<ContractorProjectPercentage> finalOutPut = new List<ContractorProjectPercentage>();
            ContractorProjectPercentage singleFinalOutPut = null;
            string[] splitMonth = Period.Split(new char[0]);
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            string ComplianceScoreInPercent = "0";
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CID, role, false);


                    outputSchedules = outputSchedules.Where(x => x.Status == "Closed").OrderBy(m => m.ProjectName).ToList();

                    foreach (var item in outputSchedules)
                    {
                        singleFinalOutPut = new ContractorProjectPercentage();

                        singleFinalOutPut.ContractorName = item.ContractorName;
                        singleFinalOutPut.ContractProjMappingID = item.ContractorProjectMappingID;
                        singleFinalOutPut.ProjectID = item.ProjectID;
                        singleFinalOutPut.ForMonth = item.ForMonth;
                        singleFinalOutPut.ProjectName = item.ProjectName;
                        singleFinalOutPut.ContractorID = item.ContractorID;
                        finalOutPut.Add(singleFinalOutPut);
                    }

                    finalOutPut = finalOutPut
                                  .GroupBy(p => new { p.ContractProjMappingID, p.ContractorName, p.ForMonth })
                                  .Select(g => g.First())
                                  .ToList();

                    finalOutPut = finalOutPut.DistinctBy(x => x.ContractorID).ToList();

                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();
                  
                    foreach (var item in finalOutPut)
                    {
                        foreach (var items in Months)
                        {   
                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractProjMappingID && x.ProjectID == item.ProjectID).ToList();
                            int percentage = 0;
                            if (ComplianceScore.Count > 0)
                            {
                                var compliedList = (from row in output
                                                    select row).Where(x => x.ComplianceStatusID == 1 && x.AuditCheckListStatusID == 4).ToList();

                                var noncompliedList = (from row in output
                                                       select row).Where(x => x.ComplianceStatusID == 2 && x.AuditCheckListStatusID == 4).ToList();


                                int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                                percentage = (compliedList.Count * 100) / totalCompliednoncomplied;

                                ComplianceScoreInPercent = percentage + "%";
                            }
                            else
                                ComplianceScoreInPercent = ComplianceScoreInPercent + "%";

                            Period = Period.Replace("-", "");

                            if (Period.Contains(item.ForMonth.Substring(4, 2)) && Period.Contains(DateTime.Now.Year.ToString()))
                            {
                                if (items == "Jan" && item.ForMonth.Contains("Jan"))
                                    item.Jan = ComplianceScoreInPercent;
                                else
                                    item.Jan = "0%";
                                if (items == "Feb" && item.ForMonth.Contains("Feb"))
                                    item.Feb = ComplianceScoreInPercent;
                                else
                                    item.Feb = "0%";
                                if (items == "Mar" && item.ForMonth.Contains("Mar"))
                                    item.Mar = ComplianceScoreInPercent;
                                else
                                    item.Mar = "0%";
                                if (items == "Apr" && item.ForMonth.Contains("Apr"))
                                    item.Apr = ComplianceScoreInPercent;
                                else
                                    item.Apr = "0%";
                                if (items == "May" && item.ForMonth.Contains("May"))
                                    item.May = ComplianceScoreInPercent;
                                else
                                    item.May = "0%";
                                if (items == "Jun" && item.ForMonth.Contains("Jun"))
                                    item.Jun = ComplianceScoreInPercent;
                                else
                                    item.Jun = "0%";
                                if (items == "Jul" && item.ForMonth.Contains("Jul"))
                                    item.Jul = ComplianceScoreInPercent;
                                else
                                    item.Jul = "0%";
                                if (items == "Aug" && item.ForMonth.Contains("Aug"))
                                    item.Aug = ComplianceScoreInPercent;
                                else
                                    item.Aug = "0%";
                                if (items == "Sep" && item.ForMonth.Contains("Sep"))
                                    item.Sep = ComplianceScoreInPercent;
                                else
                                    item.Sep = "0%";
                                if (items == "Oct" && item.ForMonth.Contains("Oct"))
                                    item.Oct = ComplianceScoreInPercent;
                                else
                                    item.Oct = "0%";
                                if (items == "Nov" && item.ForMonth.Contains("Nov"))
                                    item.Nov = ComplianceScoreInPercent;
                                else
                                    item.Nov = "0%";
                                if (items == "Dec" && item.ForMonth.Contains("Dec"))
                                    item.Dec = ComplianceScoreInPercent;
                                else
                                    item.Dec = "0%";
                            }
                            else
                            {
                                item.Jan = ComplianceScoreInPercent;
                                item.Feb = ComplianceScoreInPercent;
                                item.Mar = ComplianceScoreInPercent;
                                item.Apr = ComplianceScoreInPercent;
                                item.May = ComplianceScoreInPercent;
                                item.Jun = ComplianceScoreInPercent;
                                item.Jul = ComplianceScoreInPercent;
                                item.Aug = ComplianceScoreInPercent;

                                item.Sep = ComplianceScoreInPercent;
                                item.Oct = ComplianceScoreInPercent;
                                item.Nov = ComplianceScoreInPercent;
                                item.Dec = ComplianceScoreInPercent;
                            }
                        }
                    }


                    return Json(finalOutPut, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public FileResult DownloadSampleFileLicenseReport(string SampleFileName, string FilePath)
        {
            try
            {
                FilePath = FilePath.Replace(@"~", "");
                FilePath = FilePath.Substring(1);
                FilePath = FilePath.Replace(@"/", "\\");
                var path = ConfigurationManager.AppSettings["VendorFile"] + "\\" + FilePath;//+ "\\" + SampleFileName;
                string extension = Path.GetExtension(SampleFileName);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return File(stream, System.Net.Mime.MediaTypeNames.Application.Octet, SampleFileName);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        public ActionResult ClosedAuditReportInfraWiseFull(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            string[] splitMonth = Period.Split(new char[0]);

            try
            {

                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<Vendor_ProjectElement> ProjectElementObj = new List<Vendor_ProjectElement>();
                List<Vendor_ProjectCategorization> CategoryElementObj = new List<Vendor_ProjectCategorization>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                List<Vendor_LicenseType> objLicenseType = new List<Vendor_LicenseType>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();


                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();

                        ProjectElementObj = (from row in entities.Vendor_ProjectElement
                                             where row.IsDeleted == false
                                             && row.CustID == CustomerID
                                             select row).OrderBy(x => x.Element).ToList();

                        CategoryElementObj = (from row in entities.Vendor_ProjectCategorization
                                              where row.IsDeleted == false
                                              && row.CustID == CustomerID
                                              select row).OrderBy(x => x.Category).ToList();

                        objLicenseType = VendorAuditMaster.GetLicenseMaster(CustomerID);

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {
                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int InfraLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["InfraLocationID"]);

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, InfraLocationID).Select(x => x.ID).ToList();

                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();
                            }

                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("Element", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Category", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("DirectorName", typeof(string));
                        table.Columns.Add("ProjectHeadName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        string LDetails = "";
                        string LDetailsTableColumns = "";
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";

                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            foreach (var LD in objLicenseType)
                            {
                                LDetailsTableColumns = LDetailsTableColumns + LD.Name + " " + "License NO," + LD.Name + " " + "Validity," + LD.Name + " " + "Head Count,";
                                table.Columns.Add(LD.Name + " " + "License NO", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Validity", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Head Count", typeof(string));
                            }

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }

                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }

                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            foreach (var DP in ObjDataPoints)
                            {
                                table.Columns.Add(DP.Description, typeof(string));
                                DataPoints = DataPoints + DP.Description + ",";
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                LDetails = "";
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ElementName = ProjectElementObj.Where(x => x.Id == ProjectDetails.ElementId).Select(x => x.Element).FirstOrDefault();

                                var CategoryName = CategoryElementObj.Where(x => x.Id == ProjectDetails.CatID).Select(x => x.Category).FirstOrDefault();

                                if (string.IsNullOrEmpty(ElementName))
                                {
                                    ElementName = "Element Not Defined";
                                }

                                if (string.IsNullOrEmpty(CategoryName))
                                {
                                    CategoryName = "Category Not Defined";
                                }

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }

                                var LicenseDetailed = VendorAuditMaster.GetProjectFiles(CustomerID, Convert.ToInt32(item.ContractorProjectMappingID), 1);

                                foreach (var obs in objLicenseType)
                                {
                                    var result = LicenseDetailed.Where(x => x.LicTypeID == obs.ID).FirstOrDefault();

                                    if (result != null)
                                    {
                                        LDetails = LDetails + result.RegNo + '$' + result.Validity.Value.ToString("dd-MMM-yyyy") + '$' + result.HeadCount + '$';
                                    }
                                    else
                                    {
                                        LDetails = LDetails + "Empty" + '$' + "Empty" + '$' + "Empty" + '$';
                                    }
                                }

                                if (LDetails != "")
                                {
                                    LDetails = LDetails.TrimEnd('$');
                                    LDetailsTableColumns = LDetailsTableColumns.TrimEnd(',');
                                }
                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach (var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue == "Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }

                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "Empty";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {

                                                y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {
                                            var ReviewerRemarks = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                                 && x.ProjectID == item.ProjectID && x.ContractProjMappingScheduleOnID==item.ScheduleOnID && x.ComplianceID==y.ComplianceId).Select(x => x.ReviewerRemark).FirstOrDefault();

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "= Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "= Not Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "= Not Applicable" + ReviewerRemarks;
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied"+ ReviewerRemarks;
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }
                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }

                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }

                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }

                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }

                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (LDetails != "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                           ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                           item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + LDetails + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                    else
                                    {
                                        if (ScheduleDataPoint == "")
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                       ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                       item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                        }
                                        else
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                 ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                 item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                        }
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,Element,State,Location,Category,ProjectStatus,DirectorName,ProjectHeadName,ContractorType,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,ContarctorName,NatureOfWork,EndDateContractor,";
                            string[] LDetailsss;
                            if (LDetailsTableColumns != "")
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }
                            else
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }

                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Business";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "Project/Assest Management";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Project";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "Element";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["E6:E" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            exWorkSheet.Cells["F5"].Value = "State";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F6:F" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Location/City";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Categorization";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Project     Active/Inactive";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Name of the Project Director";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "Name of the Project Head";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Type of Contractor";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["O5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["O5"].AutoFitColumns(20);
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["O5"].Style.WrapText = true;
                            exWorkSheet.Cells["O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["P5"].Value = "Name of the Contractor";
                            exWorkSheet.Cells["P5"].AutoFitColumns(20);
                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["P5"].Style.WrapText = true;
                            exWorkSheet.Cells["P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["Q5"].Value = "Nature of Work";
                            exWorkSheet.Cells["Q5"].AutoFitColumns(20);
                            exWorkSheet.Cells["Q5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Q5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["Q5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["Q5"].Style.WrapText = true;
                            exWorkSheet.Cells["Q5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["Q5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["R5"].Value = "End Date of Contract with Contractor";
                            exWorkSheet.Cells["R5"].AutoFitColumns(20);
                            exWorkSheet.Cells["R5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["R5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["R5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["R5"].Style.WrapText = true;
                            exWorkSheet.Cells["R5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["R5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                            #endregion Baic

                            char c1 = 'S';
                            string PrefixA = "";
                            int Count = ExcelData.Rows.Count + 5;
                            List<string> ComplianceStatusColumn = new List<string>();

                            if (objLicenseType.Count != 0)
                            {
                                foreach (var items in objLicenseType)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "License No.";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Validity";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Head Count";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            if (outputCompliances.Count != 0)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    int ComplianceHeadersID = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeadersID != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeadersID = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }
                                else
                                {
                                    int ComplianceHeadersID = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeadersID != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeadersID = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }

                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion


                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 30;
                            exWorkSheet.Column(3).Width = 20;
                            exWorkSheet.Column(4).Width = 22;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 16;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 16;
                            exWorkSheet.Column(12).Width = 20;
                            exWorkSheet.Column(13).Width = 24;
                            exWorkSheet.Column(14).Width = 17;
                            exWorkSheet.Column(15).Width = 17;
                            exWorkSheet.Column(16).Width = 22;
                            exWorkSheet.Column(17).Width = 22;
                            exWorkSheet.Column(18).Width = 19;

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count + 5; i++)
                                {
                                    exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Applicable"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Applicable", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }
                            for (var i = 19; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, Convert.ToInt32(ExcelData.Columns.Count)])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Infra" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }


        public ActionResult ClosedAuditReportInfraWise(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID,string Period, int UID, string role)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            string[] splitMonth = Period.Split(new char[0]);
       
            try
            {

                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<Vendor_ProjectElement> ProjectElementObj = new List<Vendor_ProjectElement>();
                List<Vendor_ProjectCategorization> CategoryElementObj = new List<Vendor_ProjectCategorization>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                List<Vendor_LicenseType> objLicenseType = new List<Vendor_LicenseType>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();


                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();

                        ProjectElementObj = (from row in entities.Vendor_ProjectElement
                                             where row.IsDeleted == false
                                             && row.CustID == CustomerID
                                             select row).OrderBy(x => x.Element).ToList();

                        CategoryElementObj = (from row in entities.Vendor_ProjectCategorization
                                              where row.IsDeleted == false
                                              && row.CustID == CustomerID
                                              select row).OrderBy(x => x.Category).ToList();

                        objLicenseType = VendorAuditMaster.GetLicenseMaster(CustomerID);

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {
                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int InfraLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["InfraLocationID"]);

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, InfraLocationID).Select(x => x.ID).ToList();

                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();
                            }

                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("Element", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Category", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("DirectorName", typeof(string));
                        table.Columns.Add("ProjectHeadName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        string LDetails = "";
                        string LDetailsTableColumns = "";
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";

                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            foreach (var LD in objLicenseType)
                            {
                                LDetailsTableColumns = LDetailsTableColumns + LD.Name + " " + "License NO," + LD.Name + " " + "Validity," + LD.Name + " " + "Head Count,";
                                table.Columns.Add(LD.Name + " " + "License NO", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Validity", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Head Count", typeof(string));
                            }

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }

                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }
                            
                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            foreach (var DP in ObjDataPoints)
                            {
                                table.Columns.Add(DP.Description, typeof(string));
                                DataPoints = DataPoints + DP.Description + ",";
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                LDetails = "";
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ElementName = ProjectElementObj.Where(x => x.Id == ProjectDetails.ElementId).Select(x => x.Element).FirstOrDefault();

                                var CategoryName= CategoryElementObj.Where(x => x.Id == ProjectDetails.CatID).Select(x => x.Category).FirstOrDefault(); 

                                if (string.IsNullOrEmpty(ElementName))
                                {
                                    ElementName = "Element Not Defined";
                                }

                                if (string.IsNullOrEmpty(CategoryName))
                                {
                                    CategoryName = "Category Not Defined";
                                }

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }

                                var LicenseDetailed = VendorAuditMaster.GetProjectFiles(CustomerID, Convert.ToInt32(item.ContractorProjectMappingID), 1);

                                foreach (var obs in objLicenseType)
                                {
                                    var result = LicenseDetailed.Where(x => x.LicTypeID == obs.ID).FirstOrDefault();

                                    if (result!=null)
                                    {
                                        LDetails = LDetails + result.RegNo + '$' + result.Validity.Value.ToString("dd-MMM-yyyy") + '$' + result.HeadCount + '$';
                                    }
                                    else
                                    {
                                        LDetails = LDetails + "Empty" + '$' + "Empty" + '$' + "Empty" + '$';
                                    }
                                }

                                if (LDetails != "")
                                {
                                    LDetails = LDetails.TrimEnd('$');
                                    LDetailsTableColumns = LDetailsTableColumns.TrimEnd(',');
                                }
                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach(var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue=="Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }

                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "Empty";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {

                                                y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "Not Complied";
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "Not Applicable";
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }
                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }

                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }

                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }
                             
                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }

                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (LDetails != "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                           ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                           item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + LDetails + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                    else
                                    {
                                        if (ScheduleDataPoint == "")
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                       ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                       item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                        }
                                        else
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + ElementName + "$" + item.StateName + "$" + item.Location + "$" + CategoryName + "$" + ProjectDetails.DisableCheck + "$" +
                                                 ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                 item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                        }
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,Element,State,Location,Category,ProjectStatus,DirectorName,ProjectHeadName,ContractorType,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,ContarctorName,NatureOfWork,EndDateContractor,";
                            string [] LDetailsss;
                            if (LDetailsTableColumns != "")
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }
                            else
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }
                           
                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Business";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "Project/Assest Management";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Project";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "Element";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["E6:E" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            exWorkSheet.Cells["F5"].Value = "State";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F6:F" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Location/City";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Categorization";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Project     Active/Inactive";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Name of the Project Director";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "Name of the Project Head";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Type of Contractor";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["O5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["O5"].AutoFitColumns(20);
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["O5"].Style.WrapText = true;
                            exWorkSheet.Cells["O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["P5"].Value = "Name of the Contractor";
                            exWorkSheet.Cells["P5"].AutoFitColumns(20);
                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["P5"].Style.WrapText = true;
                            exWorkSheet.Cells["P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["Q5"].Value = "Nature of Work";
                            exWorkSheet.Cells["Q5"].AutoFitColumns(20);
                            exWorkSheet.Cells["Q5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Q5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["Q5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["Q5"].Style.WrapText = true;
                            exWorkSheet.Cells["Q5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["Q5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["R5"].Value = "End Date of Contract with Contractor";
                            exWorkSheet.Cells["R5"].AutoFitColumns(20);
                            exWorkSheet.Cells["R5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["R5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["R5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["R5"].Style.WrapText = true;
                            exWorkSheet.Cells["R5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["R5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                            #endregion Baic

                            char c1 = 'S';
                            string PrefixA = "";
                            int Count = ExcelData.Rows.Count + 5;
                            List<string> ComplianceStatusColumn = new List<string>();

                            if (objLicenseType.Count != 0)
                            {  
                                foreach (var items in objLicenseType)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "License No.";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Validity";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Head Count";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            if (outputCompliances.Count != 0)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    int ComplianceHeadersID = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeadersID != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeadersID = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }
                                else
                                {
                                    int ComplianceHeadersID = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeadersID != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeadersID = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }
                        
                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion


                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 30;
                            exWorkSheet.Column(3).Width = 20;
                            exWorkSheet.Column(4).Width = 22;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 16;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 16;
                            exWorkSheet.Column(12).Width = 20;
                            exWorkSheet.Column(13).Width = 24;
                            exWorkSheet.Column(14).Width = 17;
                            exWorkSheet.Column(15).Width = 17;
                            exWorkSheet.Column(16).Width = 22;
                            exWorkSheet.Column(17).Width = 22;
                            exWorkSheet.Column(18).Width = 19;

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count+5; i++)
                                {
                                    exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    if (exWorkSheet.Cells[items + i].Text == "Complied")
                                    {  
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Complied")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Applicable")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }
                            for (var i = 19; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, Convert.ToInt32(ExcelData.Columns.Count)])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Infra" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }


        public ActionResult ClosedAuditReportFactoryWiseFull(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role, string BusinessName)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            Period = Period.Trim();
            string[] splitMonth = Period.Split(new char[0]);

            try
            {
                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                List<Vendor_LicenseType> objLicenseType = new List<Vendor_LicenseType>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();

                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();
                        objLicenseType = VendorAuditMaster.GetLicenseMaster(CustomerID);

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {
                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int FactoryLocationID = 0;

                                if (BusinessName == "Corporate")
                                {
                                    FactoryLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["CorporateLocationID"]);
                                }
                                else
                                {
                                    FactoryLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["FactoryLocationID"]);
                                }

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, FactoryLocationID).Select(x => x.ID).ToList();


                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();
                            }

                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));

                        string LDetails = "";
                        string LDetailsTableColumns = "";
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";


                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            foreach (var LD in objLicenseType)
                            {
                                LDetailsTableColumns = LDetailsTableColumns + LD.Name + " " + "License NO," + LD.Name + " " + "Validity," + LD.Name + " " + "Head Count,";
                                table.Columns.Add(LD.Name + " " + "License NO", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Validity", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Head Count", typeof(string));
                            }

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }


                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }
                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            if (ObjDataPoints.Count > 0)
                            {
                                foreach (var DP in ObjDataPoints)
                                {
                                    table.Columns.Add(DP.Description, typeof(string));
                                    DataPoints = DataPoints + DP.Description + ",";
                                }
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                LDetails = "";
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }
                                var LicenseDetailed = VendorAuditMaster.GetProjectFiles(CustomerID, Convert.ToInt32(item.ContractorProjectMappingID), 1);

                                foreach (var obs in objLicenseType)
                                {
                                    var result = LicenseDetailed.Where(x => x.LicTypeID == obs.ID).FirstOrDefault();

                                    if (result != null)
                                    {
                                        LDetails = LDetails + result.RegNo + '$' + result.Validity.Value.ToString("dd-MMM-yyyy") + '$' + result.HeadCount + '$';
                                    }
                                    else
                                    {
                                        LDetails = LDetails + "Empty" + '$' + "Empty" + '$' + "Empty" + '$';
                                    }
                                }

                                if (LDetails != "")
                                {
                                    LDetails = LDetails.TrimEnd('$');
                                    LDetailsTableColumns = LDetailsTableColumns.TrimEnd(',');
                                }
                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach (var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue == "Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }
                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {
                                                y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {

                                            var ReviewerRemarks = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                                 && x.ProjectID == item.ProjectID && x.ContractProjMappingScheduleOnID == item.ScheduleOnID && x.ComplianceID == y.ComplianceId).Select(x => x.ReviewerRemark).FirstOrDefault();

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "= Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "= Not Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "= Not Applicable" + ReviewerRemarks;
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied" + ReviewerRemarks;
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }

                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }
                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }
                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }
                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }

                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (LDetails != "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                            item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                            ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                            ContractMappingDetails[0].SpocEmail + "$" + LDetails + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                    else
                                    {
                                        if (ScheduleDataPoint == "")
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                                item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                                ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                                ContractMappingDetails[0].SpocEmail + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                        }
                                        else
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                           item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                           ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                           ContractMappingDetails[0].SpocEmail + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                        }
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,State,Location,ContarctorName,ContractorType,NatureOfWork,ProjectStatus,EndDateContractor,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,";
                            string[] LDetailsss;
                            if (LDetailsTableColumns != "")
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }
                            else
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }

                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Business";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "BU";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Factory Unit";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "State";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["F5"].Value = "Location/City";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Name of the Contarctor";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Man Power Contarct/Service Contract";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Nature of Work";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Status -    Active/Inactive";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "End date of Contract with Contractor";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            #endregion Baic

                            char c1 = 'O';
                            string PrefixA = "";
                            List<string> ComplianceStatusColumn = new List<string>();
                            if (objLicenseType.Count != 0)
                            {
                                foreach (var items in objLicenseType)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "License No.";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;

                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Validity";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;

                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Head Count";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;


                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            if (outputCompliances.Count != 0)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    int ComplianceHeaderIDs = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeaderIDs != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            ComplianceHeaderIDs = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }
                                else
                                {
                                    int ComplianceHeaderIDs = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeaderIDs != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeaderIDs = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }

                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in SchedulePeriods)
                                    {
                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 18;
                            exWorkSheet.Column(3).Width = 18;
                            exWorkSheet.Column(4).Width = 24;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 25;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 22;
                            exWorkSheet.Column(12).Width = 22;
                            exWorkSheet.Column(13).Width = 19;
                            exWorkSheet.Column(14).Width = 17;

                            for (var i = 15; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count + 5; i++)
                                {
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Applicable"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Applicable", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, ExcelData.Columns.Count])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Factory" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }

        public ActionResult ClosedAuditReportFactoryWise(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role, string BusinessName)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            Period = Period.Trim();
            string[] splitMonth = Period.Split(new char[0]);

            try
            {
                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                List<Vendor_LicenseType> objLicenseType = new List<Vendor_LicenseType>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();

                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();
                        objLicenseType = VendorAuditMaster.GetLicenseMaster(CustomerID);

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {
                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int FactoryLocationID = 0;

                                if (BusinessName == "Corporate")
                                {
                                    FactoryLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["CorporateLocationID"]);
                                }
                                else
                                {
                                    FactoryLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["FactoryLocationID"]);
                                }

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, FactoryLocationID).Select(x => x.ID).ToList();


                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();
                            }

                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));

                        string LDetails = "";
                        string LDetailsTableColumns = "";
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";


                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            foreach (var LD in objLicenseType)
                            {
                                LDetailsTableColumns = LDetailsTableColumns + LD.Name + " " + "License NO," + LD.Name + " " + "Validity," + LD.Name + " " + "Head Count,";
                                table.Columns.Add(LD.Name + " " + "License NO", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Validity", typeof(string));
                                table.Columns.Add(LD.Name + " " + "Head Count", typeof(string));
                            }

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }


                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }
                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            if (ObjDataPoints.Count > 0)
                            {
                                foreach (var DP in ObjDataPoints)
                                {
                                    table.Columns.Add(DP.Description, typeof(string));
                                    DataPoints = DataPoints + DP.Description + ",";
                                }
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                LDetails = "";
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }
                                var LicenseDetailed = VendorAuditMaster.GetProjectFiles(CustomerID, Convert.ToInt32(item.ContractorProjectMappingID), 1);

                                foreach (var obs in objLicenseType)
                                {
                                    var result = LicenseDetailed.Where(x => x.LicTypeID == obs.ID).FirstOrDefault();

                                    if (result != null)
                                    {
                                        LDetails = LDetails + result.RegNo + '$' + result.Validity.Value.ToString("dd-MMM-yyyy") + '$' + result.HeadCount + '$';
                                    }
                                    else
                                    {
                                        LDetails = LDetails + "Empty" + '$' + "Empty" + '$' + "Empty" + '$';
                                    }
                                }

                                if (LDetails != "")
                                {
                                    LDetails = LDetails.TrimEnd('$');
                                    LDetailsTableColumns = LDetailsTableColumns.TrimEnd(',');
                                }
                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach (var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue == "Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }
                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {
                                                y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "Not Complied";
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "Not Applicable";
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }

                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }
                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }
                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }
                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }

                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (LDetails != "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                            item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                            ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                            ContractMappingDetails[0].SpocEmail + "$" + LDetails + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                    else
                                    {
                                        if (ScheduleDataPoint == "")
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                                item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                                ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                                ContractMappingDetails[0].SpocEmail + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                        }
                                        else
                                        {
                                            tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" +
                                           item.ContractorName + "$" + item.ContractorType + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ProjectDetails.DisableCheck + "$" +
                                           ContractEndDate + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" +
                                           ContractMappingDetails[0].SpocEmail + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                        }
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,State,Location,ContarctorName,ContractorType,NatureOfWork,ProjectStatus,EndDateContractor,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,";
                            string[] LDetailsss;
                            if (LDetailsTableColumns != "")
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + LDetailsTableColumns + "," + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }
                            else
                            {
                                if (DataPoints != "" && DataPoints != null)
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                                }
                                else
                                {
                                    LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                                }
                            }

                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Business";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "BU";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Factory Unit";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "State";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["F5"].Value = "Location/City";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Name of the Contarctor";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Man Power Contarct/Service Contract";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Nature of Work";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Status -    Active/Inactive";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "End date of Contract with Contractor";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            #endregion Baic

                            char c1 = 'O';
                            string PrefixA = "";
                            List<string> ComplianceStatusColumn = new List<string>();
                            if (objLicenseType.Count != 0)
                            {
                                foreach (var items in objLicenseType)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "License No.";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;

                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Validity";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;

                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.Name + " " + "Head Count";
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;


                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            if (outputCompliances.Count != 0)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    int ComplianceHeaderIDs = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeaderIDs != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            ComplianceHeaderIDs = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }

                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }
                                else
                                {
                                    int ComplianceHeaderIDs = 0;
                                    foreach (var items in outputCompliances)
                                    {
                                        if (ComplianceHeaderIDs != items.ComplianceId)
                                        {
                                            ComplianceStatusColumn.Add(PrefixA + c1);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            ComplianceHeaderIDs = items.ComplianceId;
                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                        if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            if (c1 == 'Z')
                                            {
                                                if (PrefixA + c1 == "CZ")
                                                {
                                                    PrefixA = "D";
                                                }
                                                if (PrefixA + c1 == "BZ")
                                                {
                                                    PrefixA = "C";
                                                }
                                                if (PrefixA + c1 == "AZ")
                                                {
                                                    PrefixA = "B";
                                                }
                                                if (PrefixA + c1 == "Z")
                                                {
                                                    PrefixA = "A";
                                                }

                                                c1 = 'A';
                                                c1--;
                                            }
                                            c1++;
                                        }
                                    }
                                }

                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in SchedulePeriods)
                                    {
                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in SchedulePeriods)
                                    {

                                        if (items == Period)
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }
                                        else
                                        {
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        }

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                if (objLicenseType.Count != 0)
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }
                                else
                                {
                                    foreach (var items in DataPointss)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 18;
                            exWorkSheet.Column(3).Width = 18;
                            exWorkSheet.Column(4).Width = 24;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 25;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 22;
                            exWorkSheet.Column(12).Width = 22;
                            exWorkSheet.Column(13).Width = 19;
                            exWorkSheet.Column(14).Width = 17;

                            for (var i = 15; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count + 5; i++)
                                {
                                    exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    if (exWorkSheet.Cells[items + i].Text == "Complied")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Complied")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Applicable")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, ExcelData.Columns.Count])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Factory" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }

      
        public ActionResult ClosedAuditReportMSIWise(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role, string BusinessName)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            string[] splitMonth = Period.Split(new char[0]);

            try
            {

                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();

                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {

                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int MSILocationID = 0;

                                if (BusinessName == "PCBMSI")
                                {
                                    MSILocationID = Convert.ToInt32(ConfigurationManager.AppSettings["PCBMSILocationID"]);
                                }
                                else
                                {
                                    MSILocationID = Convert.ToInt32(ConfigurationManager.AppSettings["MSILocationID"]);
                                }

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, MSILocationID).Select(x => x.ID).ToList();

                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();

                            }
                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("DirectorName", typeof(string));
                        table.Columns.Add("ProjectHeadName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";

                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }

                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }

                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            foreach (var DP in ObjDataPoints)
                            {
                                table.Columns.Add(DP.Description, typeof(string));
                                DataPoints = DataPoints + DP.Description + ",";
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }

                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach (var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue == "Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }

                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "Empty";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {
                                                if (y.ParameterValue != "Nil" && y.ParameterValue != "" && y.ParameterValue != "NIL" && y.ParameterValue != "Empty")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "Not Complied";
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "Not Applicable";
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied";
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    if (y.ParameterValue != "Nil" && y.ParameterValue != "" && y.ParameterValue != "NIL" && y.ParameterValue != "Empty")
                                                    {
                                                        y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                    }
                                                }
                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }

                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }

                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }

                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }
                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (ScheduleDataPoint == "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" + ProjectDetails.DisableCheck + "$" +
                                                   ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                   item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                    }
                                    else
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" + ProjectDetails.DisableCheck + "$" +
                                             ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                             item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,State,Location,ProjectStatus,DirectorName,ProjectHeadName,ContractorType,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,ContarctorName,NatureOfWork,EndDateContractor,";
                            string[] LDetailsss;
                            if (DataPoints != "" && DataPoints != null)
                            {
                                LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                            }
                            else
                            {
                                LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                            }

                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Sr. No.";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "Project/Assest Management";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Project";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "State";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["F5"].Value = "Location/City";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Project     Active/Inactive";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Name of the Project Director";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Name of the Project Head";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Type of Contractor";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Name of the Contractor";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["O5"].Value = "Nature of Work";
                            exWorkSheet.Cells["O5"].AutoFitColumns(20);
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["O5"].Style.WrapText = true;
                            exWorkSheet.Cells["O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["P5"].Value = "End Date of Contract with Contractor";
                            exWorkSheet.Cells["P5"].AutoFitColumns(20);
                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["P5"].Style.WrapText = true;
                            exWorkSheet.Cells["P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                            #endregion Baic

                            char c1 = 'Q';
                            string PrefixA = "";
                            int Count = ExcelData.Rows.Count + 5;
                            List<string> ComplianceStatusColumn = new List<string>();

                            if (outputCompliances.Count != 0)
                            {
                                int ComplianceHeadersID = 0;
                                foreach (var items in outputCompliances)
                                {
                                    if (ComplianceHeadersID != items.ComplianceId)
                                    {
                                        ComplianceStatusColumn.Add(PrefixA + c1);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        ComplianceHeadersID = items.ComplianceId;
                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }

                                    if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                foreach (var items in SchedulePeriods)
                                {

                                    if (items == Period)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                foreach (var items in DataPointss)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion


                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 30;
                            exWorkSheet.Column(3).Width = 20;
                            exWorkSheet.Column(4).Width = 22;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 16;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 16;
                            exWorkSheet.Column(12).Width = 20;
                            exWorkSheet.Column(13).Width = 24;
                            exWorkSheet.Column(14).Width = 17;
                            exWorkSheet.Column(15).Width = 17;
                            exWorkSheet.Column(16).Width = 22;
                            exWorkSheet.Column(17).Width = 22;
                            exWorkSheet.Column(18).Width = 19;

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count + 5; i++)
                                {
                                    exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    if (exWorkSheet.Cells[items + i].Text == "Complied")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Complied")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text == "Not Applicable")
                                    {
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }
                            for (var i = 19; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, Convert.ToInt32(ExcelData.Columns.Count)])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Infra" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }


        public ActionResult ClosedAuditReportMSIWiseFull(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role,string BusinessName)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            string[] Months = { "Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };
            string[] splitMonth = Period.Split(new char[0]);

            try
            {

                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
                List<Vendor_SP_GetProjectMaster_Result> ProjectObj = new List<Vendor_SP_GetProjectMaster_Result>();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> outputSchedules = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                List<Vendor_CompMasterValueDetail> ObjComplianceWithParamenter = new List<Vendor_CompMasterValueDetail>();
                List<Vendor_DataPoints> ObjDataPoints = new List<Vendor_DataPoints>();
                List<Vendor_AuditDataPointParameter> ObjDataPointValues = new List<Vendor_AuditDataPointParameter>();
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                #region Sheet 1   

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                  select row).ToList();

                        ObjComplianceWithParamenter = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == CustomerID
                                                       && row.IsDeleted == false
                                                       select row).OrderBy(x => x.ComplianceId).ToList();

                        ProjectObj = (from row in entities.Vendor_SP_GetProjectMaster(CustomerID)
                                      select row).ToList();

                        outputSchedules = VendorAuditMaster.GetDashboradCount(UID, CustomerID, role, false);

                        outputSchedules = outputSchedules.Where(x => x.ForMonth == Period && x.Frequency == "M").OrderBy(m => m.ProjectName).ToList();

                        ObjDataPoints = (from row in entities.Vendor_DataPoints
                                         where row.CustID == CustomerID
                                         && row.IsDeleted == false
                                         select row).ToList();

                        ObjDataPointValues = (from row in entities.Vendor_AuditDataPointParameter
                                              where row.CustomerID == CustomerID
                                              && row.IsDeleted == false
                                              select row).ToList();

                        #region Filter

                        if (outputSchedules.Count > 0)
                        {

                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }

                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = outputSchedules.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";

                                int MSILocationID = 0;

                                if (BusinessName == "PCBMSI")
                                {
                                    MSILocationID = Convert.ToInt32(ConfigurationManager.AppSettings["PCBMSILocationID"]);
                                }
                                else
                                {
                                    MSILocationID = Convert.ToInt32(ConfigurationManager.AppSettings["MSILocationID"]);
                                }

                                var Branches = VendorAuditMaster.GetEntitiesBranches(CustomerID, MSILocationID).Select(x => x.ID).ToList();

                                outputSchedules = outputSchedules.Where(x => Branches.Contains(x.LocationID)).ToList();

                            }
                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    outputSchedules = outputSchedules.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                outputSchedules = outputSchedules.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                        }

                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectType", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("State", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("ProjectStatus", typeof(string));
                        table.Columns.Add("DirectorName", typeof(string));
                        table.Columns.Add("ProjectHeadName", typeof(string));
                        table.Columns.Add("ContractorType", typeof(string));
                        table.Columns.Add("ContractorSPOCName", typeof(string));
                        table.Columns.Add("ContractorSPOCContact", typeof(string));
                        table.Columns.Add("ContractorSPOCEmail", typeof(string));
                        table.Columns.Add("ContarctorName", typeof(string));
                        table.Columns.Add("NatureOfWork", typeof(string));
                        table.Columns.Add("EndDateContractor", typeof(string));
                        string ComplianceHeaderTableColumns = "";
                        string SchedulePeriod = "";
                        string DataPoints = "";

                        #region
                        {
                            outputSchedules = outputSchedules.OrderBy(entry => entry.ProjectID).ToList();
                            int P = 0;

                            var outputCompliances = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == outputSchedules[0].ContractorProjectMappingID
                            && x.ScheduleOnId == outputSchedules[0].ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                            int ComplianceHeaderID = 0;

                            foreach (var OT in outputCompliances)
                            {
                                if (ComplianceHeaderID != OT.ComplianceId)
                                {
                                    table.Columns.Add(Convert.ToString(OT.ComplianceId + "Separabled" + OT.ID), typeof(string));
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + "," + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                    else
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ComplianceId + "Separabled" + OT.ID + ",";
                                    }
                                    ComplianceHeaderID = OT.ComplianceId;
                                }
                                else
                                {
                                    if (OT.ParameterName != "" && OT.ParameterName != "null" && OT.ParameterName != null)
                                    {
                                        ComplianceHeaderTableColumns = ComplianceHeaderTableColumns + OT.ParameterName + "Separabledd" + OT.ID + ",";
                                        table.Columns.Add(Convert.ToString(OT.ParameterName + "Separabledd" + OT.ID), typeof(string));
                                    }
                                }
                            }

                            foreach (var items in Months)
                            {
                                if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                {
                                    table.Columns.Add(Convert.ToString(items + " " + splitMonth[1]), typeof(string));
                                    SchedulePeriod = SchedulePeriod + items + " " + splitMonth[1] + ",";
                                }
                            }

                            if (SchedulePeriod != "")
                            {
                                SchedulePeriod = SchedulePeriod.TrimEnd(',');
                            }

                            foreach (var DP in ObjDataPoints)
                            {
                                table.Columns.Add(DP.Description, typeof(string));
                                DataPoints = DataPoints + DP.Description + ",";
                            }

                            if (DataPoints != "")
                            {
                                DataPoints = DataPoints.TrimEnd(',');
                            }

                            foreach (var item in outputSchedules)
                            {
                                var ProjectDetails = ProjectObj.Where(x => x.ID == item.ProjectID).FirstOrDefault();

                                var ContractMappingDetails = VendorAuditMaster.GetContractMappingDetail(CustomerID, item.ProjectID, item.ContractorID);

                                var ComplianceDetailsAsPerSchedule = ObjComplianceWithParamenter.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID && x.ScheduleOnId == item.ScheduleOnID).OrderBy(x => x.ComplianceId).ToList();

                                var ObjDP = ObjDataPointValues.Where(row => row.ContractorProjectMappingID == item.ContractorProjectMappingID
                                             && row.ScheduleOnID == item.ScheduleOnID
                                             && row.ProjectID == item.ProjectID
                                             && row.ForMonth == Period).ToList();

                                foreach (var items in ObjDataPoints)
                                {
                                    ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                                    var result = ObjDP.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x => x.DescriptionValue).FirstOrDefault();

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        result = "Empty";
                                    }

                                    obj.Description = items.Description;
                                    obj.DescType = items.DescType;
                                    obj.DescriptionValue = result;
                                    finalResult.Add(obj);
                                }

                                if (ComplianceHeaderTableColumns != "")
                                {
                                    ComplianceHeaderTableColumns = ComplianceHeaderTableColumns.TrimEnd(',');
                                }
                                ++P;
                                var tableRowsDynamic = "";

                                string ScheduleDataPoint = "";

                                if (finalResult.Count > 0)
                                {
                                    foreach (var z in finalResult)
                                    {
                                        if (z.DescriptionValue == "Nil" || z.DescriptionValue == "" || z.DescriptionValue == "NIL" || z.DescriptionValue == null || z.DescriptionValue == "Empty")
                                        {
                                            z.DescriptionValue = "Empty";
                                        }
                                        else if (z.DescType == "Date")
                                        {
                                            z.DescriptionValue = Convert.ToDateTime(z.DescriptionValue).ToString("dd-MMM-yyyy");
                                        }

                                        ScheduleDataPoint = ScheduleDataPoint + z.DescriptionValue + "$";
                                    }
                                    ScheduleDataPoint = ScheduleDataPoint.TrimEnd('$');
                                }

                                if (ComplianceDetailsAsPerSchedule.Count > 0)
                                {
                                    string ParameterValues = "";
                                    string ComplianceStatus = "";
                                    int ComplianceIDCompare = 0;
                                    string ComplianceScoreInPercent = "";
                                    string ContractEndDate = "Empty";

                                    foreach (var y in ComplianceDetailsAsPerSchedule)
                                    {
                                        if (y.ParameterName != "" && y.ParameterName != "null")
                                        {
                                            if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                            {
                                                y.ParameterValue = "Empty";
                                            }

                                            else if (y.ParameterType == "Date")
                                            {
                                                if (y.ParameterValue != "Nil" && y.ParameterValue != "" && y.ParameterValue != "NIL" && y.ParameterValue != "Empty")
                                                {
                                                    y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                }
                                            }
                                        }

                                        if (ComplianceIDCompare != y.ComplianceId)
                                        {

                                            var ReviewerRemarks = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                                                                          && x.ProjectID == item.ProjectID && x.ContractProjMappingScheduleOnID == item.ScheduleOnID && x.ComplianceID == y.ComplianceId).Select(x => x.ReviewerRemark).FirstOrDefault();

                                            if (y.ComplianceStatusID == 1)
                                            {
                                                ComplianceStatus = "= Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 2)
                                            {
                                                ComplianceStatus = "= Not Complied" + ReviewerRemarks;
                                            }
                                            else if (y.ComplianceStatusID == 3)
                                            {
                                                ComplianceStatus = "= Not Applicable" + ReviewerRemarks;
                                            }
                                            else
                                            {
                                                ComplianceStatus = "Complied" + ReviewerRemarks;
                                            }
                                            ComplianceIDCompare = y.ComplianceId;
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$" + y.ParameterValue + "$";
                                            }
                                            else
                                            {
                                                ParameterValues = ParameterValues + ComplianceStatus + "$";
                                            }
                                        }
                                        else
                                        {
                                            if (y.ParameterName != "" && y.ParameterName != "null")
                                            {
                                                if (y.ParameterValue == "Nil" || y.ParameterValue == "" || y.ParameterValue == "NIL" || y.ParameterValue == "Empty")
                                                {
                                                    y.ParameterValue = "Empty";
                                                }
                                                else if (y.ParameterType == "Date")
                                                {
                                                    if (y.ParameterValue != "Nil" && y.ParameterValue != "" && y.ParameterValue != "NIL" && y.ParameterValue != "Empty")
                                                    {
                                                        y.ParameterValue = Convert.ToDateTime(y.ParameterValue).ToString("dd-MMM-yyyy");
                                                    }
                                                }
                                                ParameterValues = ParameterValues + y.ParameterValue + "$";
                                            }
                                        }

                                    }

                                    if (ParameterValues != "")
                                    {
                                        ParameterValues = ParameterValues.TrimEnd('$');
                                    }
                                    else
                                    {
                                        ParameterValues = "Empty";
                                    }

                                    foreach (var items in Months)
                                    {
                                        if (DateTime.ParseExact(items, "MMM", CultureInfo.CurrentCulture).Month <= DateTime.ParseExact(splitMonth[0], "MMM", CultureInfo.CurrentCulture).Month)
                                        {

                                            var ComplianceScore = output.Where(x => x.ContractProjMappingID == item.ContractorProjectMappingID
                                              && x.ProjectID == item.ProjectID
                                              && x.Period == items + " " + splitMonth[1]).ToList();

                                            int percentage = 0;
                                            if (ComplianceScore.Count > 0)
                                            {
                                                var CompliedAndNotComplied = ComplianceScore.Where(x => x.CompliancechecklistStatus == "Complied" || x.CompliancechecklistStatus == "Not Complied").Count();

                                                if (CompliedAndNotComplied != 0)
                                                {
                                                    percentage = (CompliedAndNotComplied * 100) / ComplianceScore.Count();
                                                }
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + percentage + " % " + "$";
                                            }
                                            else
                                                ComplianceScoreInPercent = ComplianceScoreInPercent + "0%$";
                                        }
                                    }

                                    if (ComplianceScoreInPercent != "")
                                    {
                                        ComplianceScoreInPercent = ComplianceScoreInPercent.TrimEnd('$');
                                    }

                                    if (ContractMappingDetails[0].ContractEndDate != null)
                                    {
                                        ContractEndDate = Convert.ToDateTime(ContractMappingDetails[0].ContractEndDate).ToString("dd-MMM-yyyy");
                                    }

                                    if (string.IsNullOrEmpty(ContractMappingDetails[0].NatureOfWork))
                                    {
                                        ContractMappingDetails[0].NatureOfWork = "Empty";
                                    }
                                    var MainLocationName = "";
                                    using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                    {

                                        var MainLocationID = (from row in entitiess.CustomerBranches
                                                              where row.ID == item.LocationID
                                                              select row.ParentID).FirstOrDefault();
                                        if (MainLocationID != null)
                                        {
                                            MainLocationName = (from row in entitiess.CustomerBranches
                                                                where row.ID == MainLocationID
                                                                select row.Name).FirstOrDefault();
                                        }
                                    }

                                    if (ScheduleDataPoint == "")
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" + ProjectDetails.DisableCheck + "$" +
                                                   ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                                   item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent;
                                    }
                                    else
                                    {
                                        tableRowsDynamic = P + "$" + MainLocationName + "$" + ProjectDetails.Projectoffice + "$" + ProjectDetails.Name + "$" + item.StateName + "$" + item.Location + "$" + ProjectDetails.DisableCheck + "$" +
                                             ProjectDetails.directorname + "$" + ProjectDetails.headname + "$" + item.ContractorType + "$" + ContractMappingDetails[0].SpocName + "$" + ContractMappingDetails[0].SpocContactNo + "$" + ContractMappingDetails[0].SpocEmail + "$" +
                                             item.ContractorName + "$" + ContractMappingDetails[0].NatureOfWork + "$" + ContractEndDate + "$" + ParameterValues + "$" + ComplianceScoreInPercent + "$" + ScheduleDataPoint;
                                    }
                                }

                                var LDetailss = tableRowsDynamic.Split('$');

                                table.Rows.Add(LDetailss);
                                finalResult.Clear();
                            }


                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Detailed Closed Audit Report");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            var Values = "SrNo,MainLocation,ProjectType,ProjectName,State,Location,ProjectStatus,DirectorName,ProjectHeadName,ContractorType,ContractorSPOCName,ContractorSPOCContact,ContractorSPOCEmail,ContarctorName,NatureOfWork,EndDateContractor,";
                            string[] LDetailsss;
                            if (DataPoints != "" && DataPoints != null)
                            {
                                LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod + "," + DataPoints).Split(',');
                            }
                            else
                            {
                                LDetailsss = (Values + ComplianceHeaderTableColumns + "," + SchedulePeriod).Split(',');
                            }

                            ExcelData = view.ToTable("Selected", false, LDetailsss);

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Sr. No.";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "Project/Assest Management";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Name of Project";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "State";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["F5"].Value = "Location/City";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Project     Active/Inactive";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Name of the Project Director";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Name of the Project Head";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Type of Contractor";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "Contractor SPOC Name";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Contractor SPOC Contact Details";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["M5"].Value = "Contractor SPOC Email ID";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["M5"].Style.WrapText = true;
                            exWorkSheet.Cells["M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["N5"].Value = "Name of the Contractor";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["N5"].Style.WrapText = true;
                            exWorkSheet.Cells["N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["O5"].Value = "Nature of Work";
                            exWorkSheet.Cells["O5"].AutoFitColumns(20);
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["O5"].Style.WrapText = true;
                            exWorkSheet.Cells["O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["P5"].Value = "End Date of Contract with Contractor";
                            exWorkSheet.Cells["P5"].AutoFitColumns(20);
                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["P5"].Style.WrapText = true;
                            exWorkSheet.Cells["P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                            #endregion Baic

                            char c1 = 'Q';
                            string PrefixA = "";
                            int Count = ExcelData.Rows.Count + 5;
                            List<string> ComplianceStatusColumn = new List<string>();

                            if (outputCompliances.Count != 0)
                            {
                                int ComplianceHeadersID = 0;
                                foreach (var items in outputCompliances)
                                {
                                    if (ComplianceHeadersID != items.ComplianceId)
                                    {
                                        ComplianceStatusColumn.Add(PrefixA + c1);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = GetCompliancebyID(items.ComplianceId);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        ComplianceHeadersID = items.ComplianceId;
                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }

                                    if (items.ParameterName != "" && items.ParameterName != null && items.ParameterName != "null")
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items.ParameterName;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        if (c1 == 'Z')
                                        {
                                            if (PrefixA + c1 == "CZ")
                                            {
                                                PrefixA = "D";
                                            }
                                            if (PrefixA + c1 == "BZ")
                                            {
                                                PrefixA = "C";
                                            }
                                            if (PrefixA + c1 == "AZ")
                                            {
                                                PrefixA = "B";
                                            }
                                            if (PrefixA + c1 == "Z")
                                            {
                                                PrefixA = "A";
                                            }

                                            c1 = 'A';
                                            c1--;
                                        }
                                        c1++;
                                    }
                                }

                            }

                            var SchedulePeriods = SchedulePeriod.Split(',');
                            if (SchedulePeriods != null)
                            {
                                foreach (var items in SchedulePeriods)
                                {

                                    if (items == Period)
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Audited Month - " + items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    else
                                    {
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Value = "Compliance Score for  Previous Month - " + items;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            var DataPointss = DataPoints.Split(',');
                            if (DataPoints != null && DataPoints != "")
                            {
                                foreach (var items in DataPointss)
                                {
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Value = items;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].AutoFitColumns(50);
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Font.Bold = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.WrapText = true;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet.Cells[PrefixA + c1 + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    if (c1 == 'Z')
                                    {
                                        if (PrefixA + c1 == "CZ")
                                        {
                                            PrefixA = "D";
                                        }
                                        if (PrefixA + c1 == "BZ")
                                        {
                                            PrefixA = "C";
                                        }
                                        if (PrefixA + c1 == "AZ")
                                        {
                                            PrefixA = "B";
                                        }
                                        if (PrefixA + c1 == "Z")
                                        {
                                            PrefixA = "A";
                                        }

                                        c1 = 'A';
                                        c1--;
                                    }
                                    c1++;
                                }
                            }

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion


                            #region Column Width
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 30;
                            exWorkSheet.Column(3).Width = 20;
                            exWorkSheet.Column(4).Width = 22;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 23;
                            exWorkSheet.Column(8).Width = 16;
                            exWorkSheet.Column(9).Width = 17;
                            exWorkSheet.Column(10).Width = 16;
                            exWorkSheet.Column(11).Width = 16;
                            exWorkSheet.Column(12).Width = 20;
                            exWorkSheet.Column(13).Width = 24;
                            exWorkSheet.Column(14).Width = 17;
                            exWorkSheet.Column(15).Width = 17;
                            exWorkSheet.Column(16).Width = 22;
                            exWorkSheet.Column(17).Width = 22;
                            exWorkSheet.Column(18).Width = 19;

                            foreach (var items in ComplianceStatusColumn)
                            {
                                for (var i = 6; i <= ExcelData.Rows.Count + 5; i++)
                                {
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(104, 238, 50));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Complied"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Complied", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                                    }
                                    if (exWorkSheet.Cells[items + i].Text.Contains("= Not Applicable"))
                                    {
                                        exWorkSheet.Cells[items + i].Value = exWorkSheet.Cells[items + i].Text.Replace("= Not Applicable", "");
                                        exWorkSheet.Cells[items + i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet.Cells[items + i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                                    }
                                }
                            }
                            for (var i = 19; i <= ExcelData.Columns.Count; i++)
                            {
                                exWorkSheet.Column(i).Width = 30;
                            }

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, Convert.ToInt32(ExcelData.Columns.Count)])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Audit_Detailed_Report_Infra" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }

        public ActionResult GetAllComplaincesActNew(int CID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var complainceList = (from row in entities.Vendor_ComplainceMaster
                                          where row.IsDeleted == false
                                          && row.CustomerId == CID
                                          && row.ActID != null && row.ActID != 0
                                          && row.IsDisabled == false
                                          select row).GroupBy(x => x.ActID).Select(x => x.FirstOrDefault());

                    List<ComplainceDetails> complainceFinalList = new List<ComplainceDetails>();

                    foreach (var item in complainceList)
                    {
                        ComplainceDetails finalData = new ComplainceDetails();
                        finalData.ActID = item.ActID;
                        if (item.ActID != null)
                            finalData.ActName = GetActbyID(item.ActID);
                        complainceFinalList.Add(finalData);
                    }
                    return Json(complainceFinalList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplaincesAct(int CID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var complainceList = (from row in entities.Vendor_ComplainceMaster
                                          where row.IsDeleted == false && row.CustomerId == CID
                                          && row.ActID != null && row.ActID != 0
                                          select row).GroupBy(x => x.ActID).Select(x => x.FirstOrDefault());

                    List<ComplainceDetails> complainceFinalList = new List<ComplainceDetails>();

                    foreach (var item in complainceList)
                    {
                        ComplainceDetails finalData = new ComplainceDetails();
                        finalData.ActID = item.ActID;
                        if (item.ActID != null)
                            finalData.ActName = GetActbyID(item.ActID);
                        complainceFinalList.Add(finalData);
                    }
                    return Json(complainceFinalList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplainceMappedDataForComplianceReport(int CustomerID, string ProjectID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Vendor_SP_ConplianceMapping_Result> objResult = new List<Vendor_SP_ConplianceMapping_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                                 select row).OrderByDescending(x => x.ProjectID).ToList();

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<int?> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            objResult = objResult.Where(x => ProjectIDs.Contains(x.ProjectID)).ToList();
                        }
                    }

                    return Json(objResult, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplainceMappedDataForCompliance(int CustomerID, int ContractorID, int ProjectID, int ContractorTypeID, string TemplateType)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Vendor_SP_ConplianceMapping_Result> objResult = new List<Vendor_SP_ConplianceMapping_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    List<int> ComplianceList = (from v in entities.Vendor_ComplainceMaster
                                                where v.IsDeleted == false
                                                && (v.IsDisabled == false || v.IsDisabled == null)
                                                select v.ID).ToList();

                    objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                                 where row.ContractorID == ContractorID
                                 && row.ContractorTypeID == ContractorTypeID
                                 && row.ProjectID == ProjectID
                                 select row).ToList();

                    objResult = objResult.Where(x => ComplianceList.Contains(Convert.ToInt32(x.ComplainceID))).ToList();

                    if (TemplateType == "Others")
                    {
                        objResult = objResult.Where(x => x.TemplateID == -1).ToList();
                    }
                    else if (TemplateType == "-1")
                    {
                        objResult = objResult.Where(x => x.TemplateID != -1).ToList();
                    }
                    else if (TemplateType == "All")
                    {
                        objResult = objResult.ToList();
                    }
                    else
                    {
                        int TT = Convert.ToInt32(TemplateType);
                        objResult = objResult.Where(x => x.TemplateID == TT).ToList();
                    }

                    return Json(objResult, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteProjectMGMTMapped(int UserId, int CustomerID, int ID)
        {
            try
            {
                string ReturnPath = "false";
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    Vendor_ProjectMGMTMapping lst = (from row in entities.Vendor_ProjectMGMTMapping
                                                     where row.ID == ID
                                                     && row.CustID == CustomerID
                                                     select row).FirstOrDefault();

                    if (lst != null)
                    {
                        lst.UpdatedBy = UserId;
                        lst.UpdatedOn = DateTime.Now;
                        lst.IsDeleted = true;
                        entities.SaveChanges();
                    }
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }
        
        public ActionResult GetContractCommentedFile(string userpath)
        {
            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["DownloadVendorCommentFile"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename);
            Response.BinaryWrite(DocumentManagement.ReadDocFiles(p_strPath)); // create the file
            Response.Flush(); // send it to the client to download
            HttpContext.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            return View();
        }

        public ActionResult SaveComplianceRevData(List<CompliancePerformerRevieweData> things)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        if (things != null)
                        {
                            int CustomerID = -1;
                            int ContractProjectMappingId = -1;
                            int ScheduleOnId = -1;
                            string UserName = string.Empty;
                            int CreatedBy = -1;
                            foreach (var DetailsObj in things)
                            {
                                if (DetailsObj.ParameterWithvalues != null)
                                {
                                    var objResult1 = (from row in entities.Vendor_CompMasterValueDetail
                                                      where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                      && row.CustomerID == DetailsObj.CustomerID
                                                      && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                      && row.ComplianceId == DetailsObj.ComplianceId
                                                      select row.ID).ToList();

                                    if (objResult1.Count > 0)
                                    {
                                        var objResult11 = (from row in entities.Vendor_CompMasterValueDetail
                                                           where row.CustomerID == DetailsObj.CustomerID
                                                           && objResult1.Contains(row.ID)
                                                           select row).ToList();

                                        objResult11.ForEach(entry =>
                                        {
                                            if (objResult1.Contains(entry.ID))
                                            {
                                                entities.Vendor_CompMasterValueDetail.Remove(entry);
                                            }
                                        });
                                        entities.SaveChanges();
                                        //var objResult11 = (from row in entities.Vendor_CompMasterValueDetail
                                        //                   select row).ToList();

                                        //objResult11.RemoveAll(x => objResult1.Contains(x.ID));
                                    }

                                    foreach (var items in DetailsObj.ParameterWithvalues)
                                    {
                                        if (items.ParameterValue == null)
                                        {
                                            items.ParameterValue = "";
                                        }

                                        Vendor_CompMasterValueDetail ComplianceMapped = new Vendor_CompMasterValueDetail()
                                        {
                                            CustomerID = DetailsObj.CustomerID,
                                            ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                            ScheduleOnId = DetailsObj.ScheduleOnId,
                                            ComplianceId = DetailsObj.ComplianceId,
                                            ParameterType = items.ParameterType,
                                            ParameterName = items.ParameterName,
                                            ParameterValue = items.ParameterValue,
                                            CreatedBy = DetailsObj.CreatedBy,
                                            CreatedOn = DateTime.Today,
                                            UpdatedBy = DetailsObj.CreatedBy,
                                            UpdatedOn = DateTime.Today,
                                            IsDeleted = false,
                                            ComplianceHeader = DetailsObj.ComplianceHeader
                                        };
                                        if (DetailsObj.ComplianceStatusID != null)
                                        {
                                            ComplianceMapped.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                        }
                                        if (DetailsObj.StatusID != null)
                                        {
                                            ComplianceMapped.ReviewerStatusID = DetailsObj.StatusID;
                                        }
                                        entities.Vendor_CompMasterValueDetail.Add(ComplianceMapped);
                                        entities.SaveChanges();

                                    }
                                }
                                else
                                {
                                    Vendor_CompMasterValueDetail objResult1 = (from row in entities.Vendor_CompMasterValueDetail
                                                                               where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                                               && row.CustomerID == DetailsObj.CustomerID
                                                                               && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                                               && row.ComplianceId == DetailsObj.ComplianceId
                                                                               select row).FirstOrDefault();
                                    if (objResult1 == null)
                                    {
                                        Vendor_CompMasterValueDetail ComplianceMapped = new Vendor_CompMasterValueDetail()
                                        {
                                            CustomerID = DetailsObj.CustomerID,
                                            ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                            ScheduleOnId = DetailsObj.ScheduleOnId,
                                            ComplianceId = DetailsObj.ComplianceId,
                                            ParameterType = "",
                                            ParameterName = "",
                                            ParameterValue = "",
                                            CreatedBy = DetailsObj.CreatedBy,
                                            CreatedOn = DateTime.Today,
                                            UpdatedBy = DetailsObj.CreatedBy,
                                            UpdatedOn = DateTime.Today,
                                            IsDeleted = false,
                                            ComplianceHeader = DetailsObj.ComplianceHeader
                                        };
                                        if (DetailsObj.ComplianceStatusID != null)
                                        {
                                            ComplianceMapped.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                        }
                                        if (DetailsObj.StatusID != null)
                                        {
                                            ComplianceMapped.ReviewerStatusID = DetailsObj.StatusID;
                                        }
                                        entities.Vendor_CompMasterValueDetail.Add(ComplianceMapped);
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        var updateParameterDetails = (from row in entities.Vendor_CompMasterValueDetail
                                                                      where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                                      && row.CustomerID == DetailsObj.CustomerID
                                                                      && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                                      && row.ComplianceId == DetailsObj.ComplianceId
                                                                      select row).ToList();

                                        if (updateParameterDetails != null)
                                        {
                                            foreach (var item in updateParameterDetails)
                                            {

                                                if (DetailsObj.ComplianceStatusID != null)
                                                {
                                                    item.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                                }
                                                if (DetailsObj.StatusID != null)
                                                {
                                                    item.ReviewerStatusID = DetailsObj.StatusID;
                                                }
                                                entities.SaveChanges();
                                            }
                                        }
                                    }
                                }

                                if (DetailsObj.StatusID != 0)
                                {
                                    string rem = "";
                                    if (DetailsObj.Remark != null)
                                        rem = DetailsObj.Remark;

                                    CustomerID = DetailsObj.CustomerID;
                                    ContractProjectMappingId = DetailsObj.ContractProjectMappingId;
                                    ScheduleOnId = DetailsObj.ScheduleOnId;
                                    CreatedBy = DetailsObj.CreatedBy;
                                    UserName = VendorAuditMaster.GetUserNameDetails(DetailsObj.CreatedBy, DetailsObj.CustomerID);
                                    Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                    {
                                        ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                        StatusId = DetailsObj.StatusID,
                                        Remarks = rem,
                                        CreatedBy = DetailsObj.CreatedBy,
                                        CreatedByText = UserName,
                                        StatusChangedOn = DateTime.Now,
                                        ComplianceScheduleOnID = DetailsObj.ScheduleOnId,
                                        ComplianceID = DetailsObj.ComplianceId,
                                        ContractProjMappingStepMappingID = DetailsObj.ContractProjMappingStepMappingID,
                                        ComplianceStatusID = DetailsObj.ComplianceStatusID
                                    };
                                    VendorAuditMaster.CreateTransactioncompliance(transaction1);
                                }
                            }
                            if (ContractProjectMappingId > 0 && ScheduleOnId > 0 && CreatedBy > 0 && !string.IsNullOrEmpty(UserName))
                            {

                                var Result = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, ContractProjectMappingId, ScheduleOnId)
                                              select row).ToList();
                                int cnt = Result.Where(x => x.AuditCheckListStatusID == 2).ToList().Count();
                                if (cnt == 0)
                                {
                                    int closedResult = (from row in Result
                                                        where row.AuditCheckListStatusID == 4//check statusid is closed or not
                                                        select row).ToList().Count;

                                    int rejectResult = (from row in Result
                                                        where row.AuditCheckListStatusID == 5//check statusid is rejected
                                                        select row).ToList().Count;

                                    if (closedResult == Result.Count)
                                    {
                                        Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                                        {
                                            ContractProjectMappingId = ContractProjectMappingId,
                                            StatusId = 4,
                                            Remarks = "",
                                            CreatedBy = CreatedBy,
                                            CreatedByText = UserName,
                                            StatusChangedOn = DateTime.Now,
                                            ComplianceScheduleOnID = ScheduleOnId,
                                        };
                                        VendorAuditMaster.CreateTransaction(transaction);
                                    }
                                    if (rejectResult > 0)
                                    {
                                        Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                                        {
                                            ContractProjectMappingId = ContractProjectMappingId,
                                            StatusId = 1,
                                            Remarks = "",
                                            CreatedBy = CreatedBy,
                                            CreatedByText = UserName,
                                            StatusChangedOn = DateTime.Now,
                                            ComplianceScheduleOnID = ScheduleOnId,
                                        };
                                        VendorAuditMaster.CreateTransaction(transaction);
                                    }
                                }
                            }
                        }
                        else
                        {
                            return Json("Error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult SaveCompliancePerformerRevData(List<CompliancePerformerRevieweData> things)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                bool result = false;
                List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    things = things.Where(x => x.StatusID != 4).ToList();
                    if (things.Count > 0)
                    {
                        string UserName = string.Empty;
                        foreach (var DetailsObj in things)
                        {
                            if (DetailsObj.Remark == null)
                            {
                                DetailsObj.Remark = "";
                            }
                            if (DetailsObj.ParameterWithvalues != null)
                            {

                                var objResult1 = (from row in entities.Vendor_CompMasterValueDetail
                                                  where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                  && row.CustomerID == DetailsObj.CustomerID
                                                  && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                  && row.ComplianceId == DetailsObj.ComplianceId
                                                  select row.ID).ToList();

                                if (objResult1.Count > 0)
                                {
                                    var objResult11 = (from row in entities.Vendor_CompMasterValueDetail
                                                       where row.CustomerID == DetailsObj.CustomerID
                                                       && objResult1.Contains(row.ID)
                                                       select row).ToList();

                                    objResult11.ForEach(entry =>
                                    {
                                        if (objResult1.Contains(entry.ID))
                                        {
                                            entities.Vendor_CompMasterValueDetail.Remove(entry);
                                        }
                                    });
                                    entities.SaveChanges();

                                    //var objResult11 = (from row in entities.Vendor_CompMasterValueDetail
                                    //                   select row).ToList();

                                    //objResult11.RemoveAll(x => objResult1.Contains(x.ID));
                                }

                                foreach (var items in DetailsObj.ParameterWithvalues)
                                {
                                    if (items.ParameterValue == null) { items.ParameterValue = ""; }

                                    Vendor_CompMasterValueDetail ComplianceMapped = new Vendor_CompMasterValueDetail()
                                    {
                                        CustomerID = DetailsObj.CustomerID,
                                        ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                        ScheduleOnId = DetailsObj.ScheduleOnId,
                                        ComplianceId = DetailsObj.ComplianceId,
                                        ParameterType = items.ParameterType,
                                        ParameterName = items.ParameterName,
                                        ParameterValue = items.ParameterValue,
                                        CreatedBy = DetailsObj.CreatedBy,
                                        CreatedOn = DateTime.Today,
                                        UpdatedBy = DetailsObj.CreatedBy,
                                        UpdatedOn = DateTime.Today,
                                        IsDeleted = false,
                                        ComplianceHeader = DetailsObj.ComplianceHeader
                                    };
                                    if (DetailsObj.ComplianceStatusID != null)
                                    {
                                        ComplianceMapped.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                    }
                                    if (DetailsObj.StatusID != 0)
                                    {
                                        ComplianceMapped.ReviewerStatusID = DetailsObj.StatusID;
                                    }
                                    entities.Vendor_CompMasterValueDetail.Add(ComplianceMapped);
                                    entities.SaveChanges();

                                }
                            }
                            else
                            {
                                Vendor_CompMasterValueDetail objResult1 = (from row in entities.Vendor_CompMasterValueDetail
                                                                           where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                                           && row.CustomerID == DetailsObj.CustomerID
                                                                           && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                                           && row.ComplianceId == DetailsObj.ComplianceId
                                                                           select row).FirstOrDefault();

                                if (objResult1 == null)
                                {
                                    var ObjCM = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(DetailsObj.CustomerID, DetailsObj.ContractProjectMappingId, DetailsObj.ScheduleOnId)
                                                 where row.ComplianceID == DetailsObj.ComplianceId
                                                 select row).ToList();
                                    foreach (var item1 in ObjCM)
                                    {
                                        var ObjPM = (from row in entities.Vendor_CompMasterParameter
                                                     where row.ComplianceId == item1.ComplianceID
                                                     select row).ToList();
                                        foreach (var item in ObjPM)
                                        {
                                            Vendor_CompMasterValueDetail ComplianceMapped = new Vendor_CompMasterValueDetail()
                                            {
                                                CustomerID = DetailsObj.CustomerID,
                                                ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                                ScheduleOnId = DetailsObj.ScheduleOnId,
                                                ComplianceId = DetailsObj.ComplianceId,
                                                ParameterType = item.ParameterType,
                                                ParameterName = item.ParameterName,
                                                ParameterValue = "",
                                                CreatedBy = DetailsObj.CreatedBy,
                                                CreatedOn = DateTime.Today,
                                                UpdatedBy = DetailsObj.CreatedBy,
                                                UpdatedOn = DateTime.Today,
                                                IsDeleted = false,
                                                ComplianceHeader = DetailsObj.ComplianceHeader
                                            };
                                            if (DetailsObj.ComplianceStatusID != null)
                                            {
                                                ComplianceMapped.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                            }
                                            if (DetailsObj.StatusID != 0)
                                            {
                                                ComplianceMapped.ReviewerStatusID = DetailsObj.StatusID;
                                            }
                                            entities.Vendor_CompMasterValueDetail.Add(ComplianceMapped);
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    var updateParameterDetails = (from row in entities.Vendor_CompMasterValueDetail
                                                                  where row.ContractProjectMappingId == DetailsObj.ContractProjectMappingId
                                                                  && row.CustomerID == DetailsObj.CustomerID
                                                                  && row.ScheduleOnId == DetailsObj.ScheduleOnId
                                                                  && row.ComplianceId == DetailsObj.ComplianceId
                                                                  select row).ToList();

                                    if (updateParameterDetails != null)
                                    {
                                        foreach (var item in updateParameterDetails)
                                        {

                                            if (DetailsObj.ComplianceStatusID != null)
                                            {
                                                item.ComplianceStatusID = DetailsObj.ComplianceStatusID;
                                            }
                                            if (DetailsObj.StatusID != null)
                                            {
                                                item.ReviewerStatusID = DetailsObj.StatusID;
                                            }
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                            if (DetailsObj.StatusID != 0)
                            {
                                int tempStatusID = DetailsObj.StatusID;
                                if (DetailsObj.StatusID == 5)
                                {
                                    tempStatusID = 1;
                                }
                                UserName = VendorAuditMaster.GetUserNameDetails(DetailsObj.CreatedBy, DetailsObj.CustomerID);
                                Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                {
                                    ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                    StatusId = tempStatusID,//Convert.ToInt32(DetailsObj.StatusID),
                                    Remarks = DetailsObj.Remark,
                                    CreatedBy = DetailsObj.CreatedBy,
                                    CreatedByText = UserName,
                                    StatusChangedOn = DateTime.Now,
                                    ComplianceScheduleOnID = DetailsObj.ScheduleOnId,
                                    ComplianceID = DetailsObj.ComplianceId,
                                    ContractProjMappingStepMappingID = DetailsObj.ContractProjMappingStepMappingID,
                                    ComplianceStatusID = DetailsObj.ComplianceStatusID
                                };
                                if (VendorAuditMaster.CreateTransactioncompliance(transaction1))
                                {
                                    Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                                    {
                                        ContractProjectMappingId = DetailsObj.ContractProjectMappingId,
                                        StatusId = tempStatusID,//DetailsObj.StatusID,
                                        Remarks = DetailsObj.Remark,
                                        CreatedBy = DetailsObj.CreatedBy,
                                        CreatedByText = UserName,
                                        StatusChangedOn = DateTime.Now,
                                        ComplianceScheduleOnID = DetailsObj.ScheduleOnId,
                                    };
                                    if (VendorAuditMaster.CreateTransaction(transaction))
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = false;
                                }
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplainceScheduledData(int UID, int CustomerID, string Role)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustomerID)
                                    select row).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || x.ProjectSubContSpocID == UID).ToList();
                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustomerID)
                                     select row).Where(x => x.AuditorID == UID).ToList();
                    }
                    return Json(objResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult EnableUser(int UserId, int CustomerID, int UID)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.EnableUser(UserId, CustomerID, UID);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UnlockUser(int UserId, int CustomerID, int UID)
        {
            try
            {
                string ReturnPath = "false";
                UserManagement.WrongAttemptCountUpdate(UID);
                UserManagementRisk.WrongAttemptCountUpdate(UID);

                ReturnPath = "Success";
             
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }
        
        public ActionResult DeleteUser(int UserId, int CustomerID, int UID)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.DeleteUser(UserId, CustomerID, UID);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult DeleteContractor(int UserId, int CustomerID, int UID, int ContractorID)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.checkExistforContractorMaster(ContractorID, CustomerID);
                if (!output)
                {
                    bool output1 = VendorAuditMaster.DeleteContractorUser(UserId, CustomerID, ContractorID);
                    bool output2 = VendorAuditMaster.DeleteUser(UserId, CustomerID, UID);
                    if (output1 && output2)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Used";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult GetAssignedData(int UserID, int CustID, int assignmentUserId, int MappingID)
        {
            try
            {
                string ReturnPath = "false";

                VendorAuditMaster.UpdateAssignedAuditor(UserID, CustID, assignmentUserId, MappingID);
                
                ReturnPath = "Success";

                return Json(ReturnPath, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
        
        public ActionResult GetUnScheduleData(string Freq,int ContID,int PID,int MappingId, int UID, int CustomerID,int Period,int Year,string activationdate)
        {
            try
            {
                string ReturnPath = "false";

                int freqint = 0;
                byte frqqq = 0;
                if (Freq == "M")
                {
                    freqint = 0;
                    frqqq = 0;
                }
                if (Freq == "Q")
                {
                    freqint = 1;
                    frqqq = 1;
                }
                if (Freq == "H")
                {
                    freqint = 2;
                    frqqq = 2;
                }
                if (Freq == "Y")
                {
                    freqint = 3;
                    frqqq = 3;
                }
                //if (Freq == "Y")
                //{
                //    freqint = 3;
                //    frqqq = 0;
                //}

                //if (Freq == "O")
                //{
                //    VendorAuditMaster.CreateOneTimeScheduleOn(scheduledOn, MappingId, Freq, UID, UserName, ContID, PID);
                //}
                if (Freq != "O")
                {
                    var complianceSchedule = VendorAuditMaster.GetScheduleByComplianceID(MappingId);
                    complianceSchedule = complianceSchedule.Where(c => c.ForMonth == Period).ToList();
                    int activationDate = Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2));
                    int activationMonth = Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2));

                    string UserName = VendorAuditMaster.GetUserNameDetails(UID, CustomerID);
                    DateTime scheduledOn = new DateTime(Year, activationMonth, activationDate);
                    string creatdByName = UserName;
                    int createdByID = UID;
                    string forMonth = VendorAuditMaster.GetForMonth(scheduledOn, Convert.ToInt32(complianceSchedule[0].ForMonth), frqqq);

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        #region new code

                        Vendor_ContractorProjectMapping data = (from row in entities.Vendor_ContractorProjectMapping
                                                                where row.ID == MappingId
                                                                select row).FirstOrDefault();
                        if (data != null)
                        {
                            data.Activationdate = scheduledOn;
                            entities.SaveChanges();
                        }

                        Vendor_ScheduleOn complianceScheduleon = new Vendor_ScheduleOn();
                        complianceScheduleon.ContractProjectMappingID = MappingId;
                        complianceScheduleon.ScheduleOn = scheduledOn;
                        complianceScheduleon.ForMonth = forMonth;
                        complianceScheduleon.ForPeriod = complianceSchedule[0].ForMonth;
                        complianceScheduleon.IsActive = true;
                        complianceScheduleon.ProjectID = PID;
                        complianceScheduleon.ContractorID = ContID;

                        entities.Vendor_ScheduleOn.Add(complianceScheduleon);
                        entities.SaveChanges();

                        DateTime nextDate = scheduledOn;

                        Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                        {
                            ContractProjectMappingId = MappingId,
                            StatusId = 1,
                            Remarks = "New compliance assigned.",
                            CreatedBy = UID,
                            CreatedByText = UserName,
                            StatusChangedOn = DateTime.Now,
                            ComplianceScheduleOnID = complianceScheduleon.ID
                        };
                        VendorAuditMaster.CreateTransaction(transaction);

                        List<int> templatedetails = (from row in entities.Vendor_ComplianceMapping
                                                     where row.ContractorProjectMappingID == MappingId
                                                     && row.TemplateID != null && row.TemplateID != -1
                                                     && row.CustID == CustomerID
                                                        && row.IsDeleted == false
                                                     select (int)row.TemplateID).ToList();

                        List<int> Compliancedetails1 = (from row in entities.Vendor_AssignedTemplate
                                                        where templatedetails.Contains(row.TemplateID)
                                                        && row.IsDeleted == false
                                                        select (int)row.ComplianceID).ToList();

                        List<int> Compliancedetails2 = (from row in entities.Vendor_ComplianceMapping
                                                        where row.ContractorProjectMappingID == MappingId
                                                        && row.ComplainceID != null
                                                        && row.CustID == CustomerID
                                                           && row.IsDeleted == false
                                                        select (int)row.ComplainceID).ToList();

                        List<int> Compliancedetails = Compliancedetails1.Union(Compliancedetails2).ToList();
                        Compliancedetails = Compliancedetails.Distinct().ToList();
                        foreach (var item in Compliancedetails)
                        {
                            Vendor_ContractProjMappingStepMapping obb = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                         where row.ContractProjMappingID == MappingId
                                                                         && row.ContractProjMappingScheduleOnID == complianceScheduleon.ID
                                                                         && row.ComplianceID == item
                                                                         select row).FirstOrDefault();
                            if (obb != null)
                            {
                                obb.IsActive = true;
                            }
                            else
                            {
                                Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                {
                                    ContractProjMappingID = MappingId,
                                    ContractProjMappingScheduleOnID = complianceScheduleon.ID,
                                    ComplianceID = item,
                                    IsActive = true
                                };
                                entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                entities.SaveChanges();
                                Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                {
                                    ContractProjectMappingId = MappingId,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned.",
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusChangedOn = DateTime.Now,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    ComplianceID = item,
                                    ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                };
                                VendorAuditMaster.CreateTransactioncompliance(transaction1);
                            }
                        }
                        #endregion
                    }
                    if (scheduledOn != null)
                    {
                        if (Freq == "O")
                        {
                            VendorAuditMaster.CreateOneTimeScheduleOn(scheduledOn, MappingId, Freq, UID, UserName, ContID, PID);
                        }
                        else
                        {
                            VendorAuditMaster.CreateScheduleOn(scheduledOn, MappingId, Freq, UID, UserName, ContID, PID, freqint, frqqq);
                        }
                    }
                }
                else
                {
                    string UserName = VendorAuditMaster.GetUserNameDetails(UID, CustomerID);
                    DateTime scheduledOn = DateTime.ParseExact(activationdate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    
                    VendorAuditMaster.CreateOneTimeScheduleOn(scheduledOn, MappingId, Freq, UID, UserName, ContID, PID);
                }
                ReturnPath = "Success";
                
               return Json(ReturnPath, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllTemplateData(int CustomerID, string TemplateName, string GroupBy)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Vendor_SP_AssingTemplateMapping_Result> objResult = new List<Vendor_SP_AssingTemplateMapping_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    objResult = (from row in entities.Vendor_SP_AssingTemplateMapping(CustomerID)
                                 select row).ToList();

                    if (TemplateName != "-1")
                    {
                        var split = TemplateName.Split(',');

                        objResult = objResult.Where(x => split.Contains(x.TemplateName)).ToList();
                    }
                    if (!string.IsNullOrEmpty(GroupBy))
                    {
                        if (GroupBy == "0")
                        {
                            objResult = objResult.GroupBy(x => x.TemplateName).Select(x => x.FirstOrDefault()).ToList();
                        }
                    }

                    return Json(objResult, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
           
        public ActionResult GetAllCategories()
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var categoryList = (from row in entities.Vendor_ProjectCategorization
                                        where row.IsDeleted == false
                                        select row);
                    List<CategoryDTO> categoryFinalList = new List<CategoryDTO>();

                    foreach (var item in categoryList)
                    {
                        CategoryDTO finalData = new CategoryDTO();
                        finalData.Id = item.Id;
                        finalData.Category = item.Category;
                        categoryFinalList.Add(finalData);
                    }

                    return Json(categoryFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
               
        public ActionResult GetAllStates()
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var allStateList = ActManagement.GetAllState();
                    allStateList = allStateList.OrderBy(x => x.Name).ToList();
                    return Json(allStateList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplainceMappedData(int CustomerID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Vendor_SP_ConplianceMapping_Result> objResult = new List<Vendor_SP_ConplianceMapping_Result>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                                 select row).OrderByDescending(x => x.TemplateName).GroupBy(x => new { x.ProjectID, x.ContractorID }).Select(x => x.FirstOrDefault()).ToList();

                    return Json(objResult, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
        public ActionResult DeleteComplianceMapping(DeleteComplianceMappingClass DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    List<Vendor_ComplianceMapping> objResultMapping = new List<Vendor_ComplianceMapping>();

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        objResultMapping = (from row in entities.Vendor_ComplianceMapping
                                            where row.IsDeleted == false
                                            select row).ToList();

                        if (objResultMapping.Count > 0)
                        {
                            if (DetailsObj.assignedTemplateDTO != null)
                            {
                                foreach (var items in DetailsObj.assignedTemplateDTO)
                                {
                                    if (items.ComplainceID == null || items.ComplainceID == 0)
                                    {
                                        var objResult = objResultMapping.Where(x => x.TemplateID == items.TemplateID && x.ContractorTypeID == items.ContractorTypeID
                                        && x.ContractorID == items.ContractorID && x.ProjectID == items.ProjectID).FirstOrDefault();
                                        if (objResult != null)
                                        {

                                            var TemplateResult = (from row in entities.Vendor_SP_ConplianceMapping(items.CustID)
                                                                  where row.ContractorID == items.ContractorID
                                                                  && row.ContractorTypeID == items.ContractorTypeID
                                                                  && row.ProjectID == items.ProjectID
                                                                  && row.TemplateID == items.TemplateID
                                                                  select row).ToList();

                                            foreach (var temp in TemplateResult)
                                            {

                                                var objMapping = (from row in entities.Vendor_ContractorProjectMapping
                                                                  where row.ContractorID == items.ContractorID
                                                                  && row.ProjectID == items.ProjectID
                                                                  && row.CustID == items.CustID
                                                                  select row).FirstOrDefault();

                                                if (objMapping != null)
                                                {
                                                    var SchListDetails = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(items.CustID)
                                                                          where row.ContractorID == items.ContractorID
                                                                          && row.ProjectID == items.ProjectID
                                                                          select row).ToList();

                                                    SchListDetails = SchListDetails.Where(x => x.AuditCheckListStatusID == 1).ToList();

                                                    List<long> SchList = SchListDetails.Select(x => x.ScheduleOnID).ToList();

                                                    var complianceScheduleon = (from row in entities.Vendor_ScheduleOn
                                                                                where row.ContractorID == items.ContractorID && row.ProjectID == items.ProjectID
                                                                                && row.ContractProjectMappingID == objMapping.ID
                                                                                && SchList.Contains(row.ID)
                                                                                select row).ToList();

                                                    foreach (var sched in complianceScheduleon)
                                                    {
                                                        var objmappcompliance = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                 where row.ContractProjMappingID == objMapping.ID
                                                                                 && row.ContractProjMappingScheduleOnID == sched.ID
                                                                                 && row.ComplianceID == temp.ComplainceID
                                                                                 select row).ToList();


                                                        if (objmappcompliance.Count > 0)
                                                        {
                                                            foreach (var b in objmappcompliance)
                                                            {
                                                                b.IsActive = false;
                                                                entities.SaveChanges();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            objResult.IsDeleted = true;
                                            entities.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var objResult = objResultMapping.Where(x => x.TemplateID == -1 && x.ContractorTypeID == items.ContractorTypeID
                                        && x.ContractorID == items.ContractorID && x.ProjectID == items.ProjectID && x.ComplainceID == items.ComplainceID).FirstOrDefault();
                                        if (objResult != null)
                                        {
                                            objResult.IsDeleted = true;
                                            entities.SaveChanges();

                                            var objMapping = (from row in entities.Vendor_ContractorProjectMapping
                                                              where row.ContractorID == items.ContractorID
                                                              && row.ProjectID == items.ProjectID
                                                              && row.CustID == items.CustID
                                                              select row).FirstOrDefault();

                                            if (objMapping != null)
                                            {
                                                var SchListDetails = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(items.CustID)
                                                                      where row.ContractorID == items.ContractorID
                                                                      && row.ProjectID == items.ProjectID
                                                                      select row).ToList();

                                                SchListDetails = SchListDetails.Where(x => x.AuditCheckListStatusID == 1).ToList();

                                                List<long> SchList = SchListDetails.Select(x => x.ScheduleOnID).ToList();

                                                var complianceScheduleon = (from row in entities.Vendor_ScheduleOn
                                                                            where row.ContractorID == items.ContractorID && row.ProjectID == items.ProjectID
                                                                            && row.ContractProjectMappingID == objMapping.ID
                                                                            && SchList.Contains(row.ID)
                                                                            select row).ToList();

                                                foreach (var sched in complianceScheduleon)
                                                {
                                                    var objmappcompliance = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                             where row.ContractProjMappingID == objMapping.ID
                                                                             && row.ContractProjMappingScheduleOnID == sched.ID
                                                                             && row.ComplianceID == objResult.ComplainceID
                                                                             select row).ToList();
                                                    if (objmappcompliance.Count > 0)
                                                    {
                                                        foreach (var b in objmappcompliance)
                                                        {
                                                            b.IsActive = false;
                                                            entities.SaveChanges();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        return Json("Success", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplainceNotMappedData(int CustomerID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> objContractMapping = new List<ContractDetails>();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    
                    var objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                                     select row).GroupBy(x => new { x.ProjectID, x.ContractorID }).Select(x => x.FirstOrDefault()).ToList();

                    List<int> MapppingIds = objResult.Select(x => x.ContractorProjectMappingID).Distinct().ToList();

                    objContractMapping = VendorAuditMaster.GetContractMasterformapping(0, CustomerID);
                    
                    objContractMapping = objContractMapping.Where(x => !MapppingIds.Contains(x.ProjectMappingID)).ToList();
                }

                //old
                //using (VendorAuditEntities entities = new VendorAuditEntities())
                //{
                //    entities.Configuration.LazyLoadingEnabled = false;

                //    //List<int> objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                //    //                 select row.ID).ToList();

                //    //objContractMapping = VendorAuditMaster.GetContractMaster(0, CustomerID);

                //    //objContractMapping = objContractMapping.Where(x => !objResult.Contains(x.ProjectMappingID)).ToList();

                //    var objResult = (from row in entities.Vendor_SP_ConplianceMapping(CustomerID)
                //                     select row).GroupBy(x => new { x.ProjectID, x.ContractorID }).Select(x => x.FirstOrDefault()).ToList();

                //    objContractMapping = VendorAuditMaster.GetContractMasterformapping(0, CustomerID);

                //    List<int> MapppingIds = new List<int>();

                //    foreach (var item in objResult)
                //    {
                //        MapppingIds.Add(objContractMapping.Where(y => y.ContractorID == item.ContractorID && y.PID == item.ProjectID).Select(x => x.ContractorID).FirstOrDefault());
                //    }

                //    objContractMapping = objContractMapping.Where(x => !MapppingIds.Contains(x.ContractorID)).ToList();
                //}
                return Json(objContractMapping, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllContractorType(int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<VC_ContractTypeDTO> contractorType = new List<VC_ContractTypeDTO>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var contractorTypeList = (from row in entities.Vendor_ContractType
                                              where row.IsDeleted == false && row.CustomerID == CID
                                              select row);

                    foreach (var item in contractorTypeList)
                    {
                        VC_ContractTypeDTO vcContractor = new VC_ContractTypeDTO();
                        {
                            vcContractor.ID = item.ID;
                            vcContractor.Name = item.Name;
                            contractorType.Add(vcContractor);
                        }
                    }
                    contractorType = contractorType.OrderBy(x => x.Name).ToList();
                    return Json(contractorType, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
        
        public ActionResult GetAllTemplate(string Type, int CID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var templateList = (from row in entities.Vendor_TemplateMaster
                                        where row.IsDeleted == false
                                        && row.CustomerID == CID
                                        select row);
                    if (!string.IsNullOrEmpty(Type))
                    {
                        templateList = templateList.Where(x => x.TemplateTye == Type);
                    }
                    List<TemplateMasterDTO> templateFinalList = new List<TemplateMasterDTO>();

                    foreach (var item in templateList)
                    {
                        TemplateMasterDTO finalData = new TemplateMasterDTO();
                        finalData.ID = item.ID;
                        finalData.TemplateName = item.TemplateName;
                        finalData.TemplateTye = item.TemplateTye;

                        if (item.IsDisabled == true)
                        {
                            finalData.IsDisabled = true;
                        }
                        else
                        {
                            finalData.IsDisabled = false;
                        }
                        if (item.IsDisabled == true)//&& item.DisabledDate < DateTime.Today && item.DisabledDate != null
                        {
                            finalData.Status = "Inactive";
                        }
                        else
                        {
                            finalData.Status = "Active";
                        }

                        templateFinalList.Add(finalData);
                    }

                    return Json(templateFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllTemplateNew(string Type, int CID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var templateList = (from row in entities.Vendor_TemplateMaster
                                        where row.IsDeleted == false
                                        && row.CustomerID == CID
                                        && row.IsDisabled != true
                                        select row);
                    if (!string.IsNullOrEmpty(Type))
                    {
                        templateList = templateList.Where(x => x.TemplateTye == Type);
                    }
                    List<TemplateMasterDTO> templateFinalList = new List<TemplateMasterDTO>();

                    foreach (var item in templateList)
                    {
                        TemplateMasterDTO finalData = new TemplateMasterDTO();
                        finalData.ID = item.ID;
                        finalData.TemplateName = item.TemplateName;
                        finalData.TemplateTye = item.TemplateTye;

                        if (item.IsDisabled == true)
                        {
                            finalData.IsDisabled = true;
                        }
                        else
                        {
                            finalData.IsDisabled = false;
                        }
                        if (item.IsDisabled == true)//&& item.DisabledDate < DateTime.Today && item.DisabledDate != null
                        {
                            finalData.Status = "Inactive";
                        }
                        else
                        {
                            finalData.Status = "Active";
                        }

                        templateFinalList.Add(finalData);
                    }

                    return Json(templateFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        internal string ProcessComplianceMappingFileData(ExcelPackage package, string fileName, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["Compliance Mapping"];
            List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    objResult = (from row in entities.Vendor_SP_ContractorProjectMapping(CustID)
                                 select row).ToList();

                    var objResultNew = (from row in entities.Vendor_SP_ContractorProjectMapping(CustID)
                                        select row).ToList();

                    if (xlWorksheet != null)
                    {
                        var noOfCol = xlWorksheet.Dimension.End.Column;
                        var noOfRow = GetLastUsedRow(xlWorksheet);
                        List<String> LogErrors = new List<string>();
                        int projectID = 0;
                        Nullable<int> ContractorTypeID = 0;
                        Nullable<int> ContractorID = 0;
                        int ComlianceID = 0;
                        Nullable<int> ActID = 0;
                        int TemplateID = -1;
                        ComplainceDetails finalData = new ComplainceDetails();
                        if (noOfRow > 1)
                        {
                            for (int i = 2; i <= noOfRow; i++)
                            {
                                count = i;

                                //Project Name
                                string ProjectName = xlWorksheet.Cells[i, 1].Text.Trim();
                                if (string.IsNullOrEmpty(ProjectName))
                                {
                                    LogErrors.Add("Required Name of Project at Row-" + count);
                                }
                                else
                                {
                                    objResult = objResult.Where(x => x.ProjectName.Trim().ToLower() == ProjectName.Trim().ToLower()).ToList();
                                    if (objResult.Count == 0)
                                    {
                                        LogErrors.Add("Project is not mapped to Contractor at Row-" + count);
                                    }
                                }

                                //Contractor Type
                                string ContractorType = xlWorksheet.Cells[i, 2].Text.Trim();
                                if (string.IsNullOrEmpty(ContractorType))
                                {
                                    LogErrors.Add("Required Type of Contractor at Row-" + count);
                                }
                                else
                                {
                                    objResult = objResult.Where(x => x.ContractorType.Trim().ToLower() == ContractorType.Trim().ToLower()).ToList();
                                    if (objResult.Count == 0)
                                    {
                                        LogErrors.Add("Type of Contractor is not mapped or not exist at Row-" + count);
                                    }
                                }

                                //Contractor Name
                                string ContractorName = xlWorksheet.Cells[i, 3].Text.Trim();
                                if (string.IsNullOrEmpty(ContractorName))
                                {
                                    LogErrors.Add("Required Name of Contractor at Row-" + count);
                                }
                                else
                                {
                                    objResult = objResult.Where(x => x.ContractorName.Trim().ToLower() == ContractorName.Trim().ToLower()).ToList();
                                    if (objResult.Count == 0)
                                    {
                                        LogErrors.Add("Name of Contractor is not mapped or not exist at Row-" + count);
                                    }
                                }

                                string TemplateType = xlWorksheet.Cells[i, 4].Text.Trim();
                                if (string.IsNullOrEmpty(TemplateType))
                                {
                                    LogErrors.Add("Required Type of Template at Row-" + count);
                                }
                                else
                                {
                                    if (TemplateType.Trim().ToLower() != "compliance" && TemplateType.Trim().ToLower() != "template")
                                    {
                                        LogErrors.Add("Type of Compliance mapping not exist at Row-" + count);
                                    }
                                    else
                                    {
                                        if (TemplateType.Trim().ToLower() == "compliance")
                                        {
                                            string ComplainceHeaderName = xlWorksheet.Cells[i, 6].Text.Trim();
                                            if (string.IsNullOrEmpty(ComplainceHeaderName))
                                            {
                                                LogErrors.Add("Required Compliance Header at Row-" + count);
                                            }
                                            else
                                            {
                                                if (CheckComplianceNameExist(ComplainceHeaderName, CustID) == true)
                                                {
                                                    LogErrors.Add("Compliance Header not exist at Row-" + count);
                                                }
                                            }
                                        }
                                        else if (TemplateType.Trim().ToLower() == "template")
                                        {
                                            string TemplateName = xlWorksheet.Cells[i, 5].Text.Trim();
                                            if (string.IsNullOrEmpty(TemplateName))
                                            {
                                                LogErrors.Add("Required Name of Template at Row-" + count);
                                            }
                                            else
                                            {
                                                if (CheckTemplateNameExist(TemplateName, CustID) == true)
                                                {
                                                    LogErrors.Add("Name of Template not exist at Row-" + count);
                                                }
                                            }
                                        }
                                    }
                                }

                                objResult.Clear();
                                objResult = objResultNew;
                            }

                            if (LogErrors.Count <= 0)
                            {
                                projectID = 0;
                                ContractorTypeID = 0;
                                ContractorID = 0;
                                ComlianceID = 0;
                                ActID = 0;
                                TemplateID = -1;

                                for (int i = 2; i <= noOfRow; i++)
                                {
                                    count = i;

                                    string ProjectName = xlWorksheet.Cells[i, 1].Text.Trim();
                                    if (!string.IsNullOrEmpty(ProjectName))
                                    {
                                        projectID = objResult.Where(x => x.ProjectName.Trim().ToLower() == ProjectName.Trim().ToLower()).Select(x => x.ProjectID).FirstOrDefault();
                                    }

                                    string ContractorType = xlWorksheet.Cells[i, 2].Text.Trim();
                                    if (!string.IsNullOrEmpty(ContractorType))
                                    {
                                        ContractorTypeID = objResult.Where(x => x.ContractorType.Trim().ToLower() == ContractorType.Trim().ToLower()).Select(x => x.ContractorTypeID).FirstOrDefault();
                                    }

                                    string ContractorName = xlWorksheet.Cells[i, 3].Text.Trim();
                                    if (!string.IsNullOrEmpty(ContractorName))
                                    {
                                        ContractorID = objResult.Where(x => x.ContractorName.Trim().ToLower() == ContractorName.Trim().ToLower()).Select(x => x.ContractorID).FirstOrDefault();
                                    }

                                    string TemplateType = xlWorksheet.Cells[i, 4].Text.Trim();
                                    if (!string.IsNullOrEmpty(TemplateType))
                                    {
                                        if (TemplateType.Trim().ToLower() == "compliance")
                                        {
                                            string ComplainceHeaderName = xlWorksheet.Cells[i, 6].Text.Trim();
                                            if (!string.IsNullOrEmpty(ComplainceHeaderName))
                                            {
                                                finalData = GetComplianceDetailsByName(ComplainceHeaderName);
                                                ComlianceID = finalData.ID;
                                                ActID = finalData.ActID;
                                            }
                                        }
                                        else if (TemplateType.Trim().ToLower() == "template")
                                        {
                                            string TemplateName = xlWorksheet.Cells[i, 5].Text.Trim();
                                            if (!string.IsNullOrEmpty(TemplateName))
                                            {
                                                TemplateID = GetTemplateIDbyName(TemplateName, CustID);
                                            }
                                        }
                                    }
                                    List<Vendor_SP_ConplianceMapping_Result> objResultMapping = new List<Vendor_SP_ConplianceMapping_Result>();


                                    objResultMapping = (from row in entities.Vendor_SP_ConplianceMapping(CustID)
                                                        select row).ToList();

                                    if (projectID != 0)
                                    {
                                        objResult = objResult.Where(x => x.ProjectID == projectID).ToList();
                                    }
                                    if (ContractorTypeID != 0)
                                    {
                                        objResult = objResult.Where(x => x.ContractorTypeID == ContractorTypeID).ToList();
                                    }
                                    if (ContractorID != 0)
                                    {
                                        objResult = objResult.Where(x => x.ContractorID == ContractorID).ToList();
                                    }

                                    if (objResult.Count > 0)
                                    {
                                        message = "success";
                                        foreach (var item in objResult)
                                        {
                                            long contractorprojectmappingid = VendorAuditMaster.GetMappingContractProjectID(item.ContractorID, item.ProjectID);
                                            Vendor_ComplianceMapping ComplianceMapped = new Vendor_ComplianceMapping()
                                            {
                                                CustID = CustID,
                                                ContractorID = Convert.ToInt32(item.ContractorID),
                                                ProjectID = Convert.ToInt32(item.ProjectID),
                                                ContractorTypeID = Convert.ToInt32(item.ContractorTypeID),
                                                CreatedBy = UserID,
                                                CreatedOn = DateTime.Today,
                                                UpdatedBy = UserID,
                                                UpdatedOn = DateTime.Today,
                                                IsDeleted = false,
                                                ContractorProjectMappingID = Convert.ToInt32(contractorprojectmappingid)
                                            };
                                            int result = 0;
                                            if (TemplateType.Trim().ToLower() == "template")
                                            {
                                                result = objResultMapping.Where(x => x.ContractorID == item.ContractorID && x.ProjectID == item.ProjectID && x.ContractorTypeID == item.ContractorTypeID
                                                 && x.TemplateID == TemplateID).Select(x => x.ProjectID).FirstOrDefault();
                                            }
                                            else
                                            {
                                                result = objResultMapping.Where(x => x.ContractorID == item.ContractorID && x.ProjectID == item.ProjectID && x.ContractorTypeID == item.ContractorTypeID
                                                && x.ComplainceID == ComlianceID).Select(x => x.ProjectID).FirstOrDefault();
                                            }

                                            if (result == 0)
                                            {
                                                ComplianceMapped.TemplateID = TemplateID;
                                                ComplianceMapped.ActID = ActID;
                                                ComplianceMapped.ComplainceID = ComlianceID;
                                                entities.Vendor_ComplianceMapping.Add(ComplianceMapped);
                                                entities.SaveChanges();

                                                List<Vendor_SP_ConplianceMapping_Result> templateCompliances = new List<Vendor_SP_ConplianceMapping_Result>();
                                                var objMapping = (from row in entities.Vendor_ContractorProjectMapping
                                                                  where row.ContractorID == item.ContractorID && row.ProjectID == item.ProjectID
                                                                  && row.CustID == CustID
                                                                  select row).FirstOrDefault();

                                                if (TemplateType.Trim().ToLower() == "template")
                                                {

                                                    List<int> ComplianceList = (from v in entities.Vendor_ComplainceMaster
                                                                                where v.IsDeleted == false
                                                                                && (v.IsDisabled == false || v.IsDisabled == null)
                                                                                select v.ID).ToList();

                                                    templateCompliances = (from row in entities.Vendor_SP_ConplianceMapping(CustID)
                                                                           where row.ContractorID == ContractorID
                                                                           && row.ContractorTypeID == ContractorTypeID
                                                                           && row.ProjectID == item.ProjectID
                                                                           && row.TemplateID == TemplateID
                                                                           select row).ToList();

                                                    templateCompliances = templateCompliances.Where(x => ComplianceList.Contains(Convert.ToInt32(x.ComplainceID))).ToList();
                                                }

                                                if (objMapping != null)
                                                {
                                                    var SchListDetails = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustID)
                                                                          where row.ContractorID == item.ContractorID
                                                                          && row.ProjectID == item.ProjectID
                                                                          select row).ToList();

                                                    SchListDetails = SchListDetails.Where(x => x.AuditCheckListStatusID == 1).ToList();

                                                    List<long> SchList = SchListDetails.Select(x => x.ScheduleOnID).ToList();

                                                    var complianceScheduleon = (from row in entities.Vendor_ScheduleOn
                                                                                where row.ContractorID == item.ContractorID && row.ProjectID == item.ProjectID
                                                                                && row.ContractProjectMappingID == objMapping.ID
                                                                                && SchList.Contains(row.ID)
                                                                                select row).ToList();

                                                    if (templateCompliances.Count > 0)
                                                    {
                                                        foreach (var temp in templateCompliances)
                                                        {
                                                            foreach (var sched in complianceScheduleon)
                                                            {
                                                                Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                                                {
                                                                    ContractProjMappingID = objMapping.ID,
                                                                    ContractProjMappingScheduleOnID = sched.ID,
                                                                    ComplianceID = Convert.ToInt64(temp.ComplainceID),
                                                                    IsActive = true
                                                                };
                                                                entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                                                entities.SaveChanges();


                                                                string UserName = VendorAuditMaster.GetUserNameDetails(UserID, CustID);

                                                                Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                                                {
                                                                    ContractProjectMappingId = item.ProjectID,
                                                                    StatusId = 1,
                                                                    Remarks = "New compliance assigned.",
                                                                    CreatedBy = UserID,
                                                                    CreatedByText = UserName,
                                                                    StatusChangedOn = DateTime.Now,
                                                                    ComplianceScheduleOnID = sched.ID,
                                                                    ComplianceID = Convert.ToInt64(temp.ComplainceID),
                                                                    ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                                                };
                                                                VendorAuditMaster.CreateTransactioncompliance(transaction1);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (var sched in complianceScheduleon)
                                                        {
                                                            Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                                            {
                                                                ContractProjMappingID = objMapping.ID,
                                                                ContractProjMappingScheduleOnID = sched.ID,
                                                                ComplianceID = Convert.ToInt64(ComlianceID),
                                                                IsActive = true
                                                            };
                                                            entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                                            entities.SaveChanges();


                                                            string UserName = VendorAuditMaster.GetUserNameDetails(UserID, CustID);

                                                            Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                                            {
                                                                ContractProjectMappingId = item.ProjectID,
                                                                StatusId = 1,
                                                                Remarks = "New compliance assigned.",
                                                                CreatedBy = UserID,
                                                                CreatedByText = UserName,
                                                                StatusChangedOn = DateTime.Now,
                                                                ComplianceScheduleOnID = sched.ID,
                                                                ComplianceID = ComlianceID,
                                                                ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                                            };
                                                            VendorAuditMaster.CreateTransactioncompliance(transaction1);
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        message = "success";
                                    }
                                }
                            }
                            else
                            {
                                string path = string.Empty;
                                string name = WriteLog(LogErrors, Inputfile.FileName, out path);
                                message = name;
                            }
                        }
                        else
                        {
                            message = "norecord";
                        }
                    }
                    else
                    {
                        message = "nofile";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }

        public ComplainceDetails GetComplianceDetailsByName(string CompName)
        {
            ComplainceDetails finalData = new ComplainceDetails();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var compIDList = (from row in entities.Vendor_ComplainceMaster
                                      where row.IsDeleted == false && row.ComplainceHeader.ToLower().Trim() == CompName.ToLower().Trim()
                                      select row).FirstOrDefault();

                    if (compIDList != null)
                    {
                        finalData.ID = compIDList.ID;
                        finalData.ActID = compIDList.ActID;
                    }
                    else
                        return finalData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return finalData;
        }

        public int GetTemplateIDbyName(string templateName, int CustID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_TemplateMaster
                                   where row.TemplateName == templateName && row.IsDeleted == false && row.CustomerID == CustID
                                   select row.ID).FirstOrDefault();
                    return actList;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        #region Complaince and Template Mapping

        [System.Web.Http.HttpPost]
        public ActionResult ComplianceMappingUploadFile(HttpRequestMessage request, int CustID, int UserID)
        {
            string message = string.Empty;
            HttpFileCollectionBase httpRequest = Request.Files;
            HttpPostedFile file = null;

            try
            {
                if (httpRequest.Count > 0)
                {
                    Inputfile = httpRequest[0];
                    if (Inputfile.FileName.EndsWith(".xls") || Inputfile.FileName.EndsWith(".xlsx"))
                    {
                        if (Inputfile != null && Inputfile.ContentLength > 0)
                        {
                            string fileName = Inputfile.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = Inputfile.ContentType;
                            byte[] fileBytes = new byte[Inputfile.ContentLength];

                            var data = Inputfile.InputStream.Read(fileBytes, 0, Convert.ToInt32(Inputfile.ContentLength));

                            using (var package = new ExcelPackage(Inputfile.InputStream))
                            {
                                message = ProcessComplianceMappingFileData(package, fileWithoutExtn, CustID, UserID);
                            }
                        }
                    }
                    else
                    {
                        message = "nofile";
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return Json(message);
        }

        #endregion
        public ActionResult SaveComplianceMapping(ComplianceMapping DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();
                    List<Vendor_SP_ConplianceMapping_Result> objResultMapping = new List<Vendor_SP_ConplianceMapping_Result>();

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        objResult = (from row in entities.Vendor_SP_ContractorProjectMapping(DetailsObj.CustID)
                                     select row).ToList();

                        objResultMapping = (from row in entities.Vendor_SP_ConplianceMapping(DetailsObj.CustID)
                                            select row).ToList();

                        if (DetailsObj.ContractorID != "[]" && !string.IsNullOrEmpty(DetailsObj.ContractorID))
                        {
                            System.Collections.Generic.IList<int> ContractorIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(DetailsObj.ContractorID);
                            if (ContractorIDs.Count != 0)
                            {
                                objResult = objResult.Where(x => ContractorIDs.Contains(x.ContractorID)).ToList();
                            }
                        }
                        if (DetailsObj.ProjectID != "[]" && !string.IsNullOrEmpty(DetailsObj.ProjectID))
                        {
                            System.Collections.Generic.IList<int> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(DetailsObj.ProjectID);
                            if (ProjectIDs.Count != 0)
                            {
                                objResult = objResult.Where(x => ProjectIDs.Contains(x.ProjectID)).ToList();
                            }
                        }
                        if (DetailsObj.ContractorTypeID != "[]" && !string.IsNullOrEmpty(DetailsObj.ContractorTypeID))
                        {
                            System.Collections.Generic.IList<int> ContractorTypeIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(DetailsObj.ContractorTypeID);
                            if (ContractorTypeIDs.Count != 0)
                            {
                                objResult = objResult.Where(x => ContractorTypeIDs.Contains(Convert.ToInt32(x.ContractorTypeID))).ToList();
                            }
                        }
                        if (objResult.Count > 0)
                        {
                            foreach (var item in objResult)
                            {
                                long contractorprojectmappingid = VendorAuditMaster.GetMappingContractProjectID(item.ContractorID, item.ProjectID);
                                Vendor_ComplianceMapping ComplianceMapped = new Vendor_ComplianceMapping()
                                {
                                    CustID = DetailsObj.CustID,
                                    ContractorID = Convert.ToInt32(item.ContractorID),
                                    ProjectID = Convert.ToInt32(item.ProjectID),
                                    ContractorTypeID = Convert.ToInt32(item.ContractorTypeID),
                                    CreatedBy = DetailsObj.CreatedBy,
                                    CreatedOn = DateTime.Today,
                                    UpdatedBy = DetailsObj.CreatedBy,
                                    UpdatedOn = DateTime.Today,
                                    IsDeleted = false,
                                    ContractorProjectMappingID = Convert.ToInt32(contractorprojectmappingid)
                                };
                                if (DetailsObj.assignedTemplateDTO != null)
                                {
                                    foreach (var items in DetailsObj.assignedTemplateDTO)
                                    {
                                        int result = 0;
                                        if (items.ComplianceID != null && items.ComplianceID != 0)
                                        {
                                            result = objResultMapping.Where(x => x.ContractorID == item.ContractorID && x.ProjectID == item.ProjectID && x.ContractorTypeID == item.ContractorTypeID
                                            && x.ComplainceID == items.ComplianceID).Select(x => x.ProjectID).FirstOrDefault();
                                        }
                                        else
                                        {
                                            result = objResultMapping.Where(x => x.ContractorID == item.ContractorID && x.ProjectID == item.ProjectID && x.ContractorTypeID == item.ContractorTypeID
                                            && x.TemplateID == items.TemplateID).Select(x => x.ProjectID).FirstOrDefault();
                                        }

                                        if (result == 0)
                                        {
                                            ComplianceMapped.TemplateID = items.TemplateID;
                                            ComplianceMapped.ActID = items.ActID;
                                            ComplianceMapped.ComplainceID = items.ComplianceID;
                                            entities.Vendor_ComplianceMapping.Add(ComplianceMapped);
                                            entities.SaveChanges();

                                            var objMapping = (from row in entities.Vendor_ContractorProjectMapping
                                                              where row.ContractorID == item.ContractorID && row.ProjectID == item.ProjectID
                                                              && row.CustID == DetailsObj.CustID
                                                              select row).FirstOrDefault();

                                            if (objMapping != null)
                                            {
                                                var SchListDetails = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(DetailsObj.CustID)
                                                                      where row.ContractorID == item.ContractorID
                                                                      && row.ProjectID == item.ProjectID
                                                                      select row).ToList();

                                                SchListDetails = SchListDetails.Where(x => x.AuditCheckListStatusID == 1).ToList();

                                                List<long> SchList = SchListDetails.Select(x => x.ScheduleOnID).ToList();

                                                var complianceScheduleon = (from row in entities.Vendor_ScheduleOn
                                                                            where row.ContractorID == item.ContractorID && row.ProjectID == item.ProjectID
                                                                            && row.ContractProjectMappingID == objMapping.ID
                                                                            && SchList.Contains(row.ID)
                                                                            select row).ToList();

                                                //var complianceScheduleon = (from row in entities.Vendor_ScheduleOn
                                                //                            where row.ContractorID == item.ContractorID && row.ProjectID == item.ProjectID
                                                //                            // && row.ScheduleOn == objMapping.Activationdate
                                                //                            && row.ContractProjectMappingID == objMapping.ID
                                                //                            select row).ToList();

                                                //complianceScheduleon = complianceScheduleon.Where(x => SchList.Contains((int)x.ID)).ToList();

                                                foreach (var sched in complianceScheduleon)
                                                {

                                                    Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                                    {
                                                        ContractProjMappingID = objMapping.ID,
                                                        ContractProjMappingScheduleOnID = sched.ID,
                                                        ComplianceID = Convert.ToInt64(items.ComplianceID),
                                                        IsActive = true
                                                    };
                                                    entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                                    entities.SaveChanges();


                                                    string UserName = VendorAuditMaster.GetUserNameDetails(DetailsObj.CreatedBy, DetailsObj.CustID);

                                                    Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                                    {
                                                        ContractProjectMappingId = item.ProjectID,
                                                        StatusId = 1,
                                                        Remarks = "New compliance assigned.",
                                                        CreatedBy = DetailsObj.CreatedBy,
                                                        CreatedByText = UserName,
                                                        StatusChangedOn = DateTime.Now,
                                                        ComplianceScheduleOnID = sched.ID,
                                                        ComplianceID = items.ComplianceID,
                                                        ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                                    };
                                                    VendorAuditMaster.CreateTransactioncompliance(transaction1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return Json("Success", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult UpdateStatusContractMaster(int UserId, int CustomerID, int CId,int status)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.updatestatusContractorMaster(UserId, CustomerID, CId, status);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult DeleteContractProjectMappingMaster(int UserId, int CustomerID, int PCId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.DeleteContractProjectMappingMaster(UserId, CustomerID, PCId);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }
        
        public ActionResult DeleteContractMaster(int UserId, int CustomerID, int CId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.DeleteContractorMaster(UserId, CustomerID, CId);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }
        
        public ActionResult DeleteProjectMaster(int UserId, int CustomerID, int PId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.DeleteProjectMaster(UserId, CustomerID, PId);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateProjectOfficeMaster(int UserId, int CustomerID, int POId, string Name)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistProjectOfficeMaster(UserId, CustomerID, POId, Name);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateProjectofficeMaster(UserId, CustomerID, POId, Name);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateComplianceProjectMapping(int UID, int CID, int ProjMappingID, int ContractorID, string ContractorName, int ProjectID, string SpoceName, string SpocEmailID
       , string SpoceContNo, string NatureofWork, int IsSubContractor, int SubContractorID, string subcontname, string subcontEmail
       , string subcontcontno, string freq, int duedays, string StartDate, string enddate, bool IsShow, string ContractStartDate, string ContractEnddate)
        {
            string ReturnPath = "false";
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    long SpocContUserID = VendorAuditMaster.GetUserIdByEmail(SpocEmailID);
                    long SpocSubContUserID = VendorAuditMaster.GetUserIdByEmail(subcontEmail);

                    DateTime duedatefnal = DateTime.Now;
                    int RoleID = UserManagement.GetRoleIdByCode("VCCON");

                    if (SpocContUserID == 0)
                    {
                        SpocContUserID = UserManagement.UpdateUserDetail(SpocEmailID, SpoceName, SpoceContNo, CID, RoleID, UID,true);
                    }
                    if (SpocSubContUserID == 0)
                    {
                        SpocSubContUserID = UserManagement.UpdateUserDetail(subcontEmail, subcontname, subcontcontno, CID, RoleID, UID,true);
                    }
                   
                    if (ProjMappingID == 0)
                    {
                        Vendor_ContractorProjectMapping obj = (from row in entities.Vendor_ContractorProjectMapping
                                                               where row.ContractorID == ContractorID
                                                               && row.CustID == CID
                                                               && row.ProjectID == ProjectID
                                                               select row).FirstOrDefault();

                        if (obj == null)
                        {
                            Vendor_ContractorProjectMapping obj1 = new Vendor_ContractorProjectMapping();
                            obj1.CustID = CID;
                            obj1.ContractorID = ContractorID;
                            obj1.ProjectID = ProjectID;
                            obj1.IsSubcontract = IsSubContractor;

                            if (IsSubContractor == 1)
                            {
                                obj1.UnderContractorID = SubContractorID;
                                if (SpocSubContUserID != 0)
                                {
                                    obj1.ProjectSubContSpocID = SpocSubContUserID;
                                }
                            }
                            if (SpocContUserID != 0)
                            {
                                obj1.ProjectContSpocID = SpocContUserID;
                            }

                            obj1.Frequency = freq;
                            if (!string.IsNullOrEmpty(ContractStartDate) && ContractStartDate != "day-month-year")
                            {
                                obj1.ContractStartDate = DateTime.ParseExact(ContractStartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj1.ContractStartDate = null;
                            }
                            if (!string.IsNullOrEmpty(ContractEnddate) && ContractEnddate != "day-month-year")
                            {
                                obj1.ContractEndDate = DateTime.ParseExact(ContractEnddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj1.ContractEndDate = null;
                            }
                            obj1.IsDelete = false;
                            obj1.CreatedBy = UID;
                            obj1.CreatedOn = DateTime.Now;
                            obj1.UpdatedBy = UID;
                            obj1.UpdatedOn = DateTime.Now;
                            obj1.DueDays = duedays;

                            if (freq == "O")
                            {
                                obj1.endDate = DateTime.ParseExact(enddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                obj1.DueDate = DateTime.ParseExact(StartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj1.DueDate = Convert.ToDateTime(duedatefnal);
                            }

                            if (IsShow && IsSubContractor == 1)
                                obj1.IsShowContractor = 1;
                            else
                                obj1.IsShowContractor = 0;

                            entities.Vendor_ContractorProjectMapping.Add(obj1);

                            entities.SaveChanges();

                            int mappingId = Convert.ToInt32(obj1.ID);
                            //int custbranchid = 1;
                            VendorAuditMaster.updateVendor_Schedule(mappingId, false, freq, Convert.ToDateTime(duedatefnal), duedays);
                            //VendorAuditMaster.Vendor_ScheduleOn(UID, CID, mappingId, ProjectID, ContractorID, custbranchid);
                            ReturnPath = "Success";
                        }
                        else
                        {
                            obj.IsSubcontract = IsSubContractor;

                            if (IsSubContractor == 1)
                            {
                                obj.UnderContractorID = SubContractorID;
                                if (SpocSubContUserID != 0)
                                {
                                    obj.ProjectSubContSpocID = SpocSubContUserID;
                                }
                            }
                            else
                                obj.UnderContractorID = 0;


                            if (SpocContUserID != 0)
                            {
                                obj.ProjectContSpocID = SpocContUserID;
                            }

                            obj.Frequency = freq;
                            if (freq == "O")
                            {
                                obj.endDate = DateTime.ParseExact(enddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                obj.DueDate = DateTime.ParseExact(StartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.DueDate = Convert.ToDateTime(duedatefnal);
                            }
                            if (!string.IsNullOrEmpty(ContractStartDate) && ContractStartDate != "day-month-year")
                            {
                                obj.ContractStartDate = DateTime.ParseExact(ContractStartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.ContractStartDate = null;
                            }
                            if (!string.IsNullOrEmpty(ContractEnddate) && ContractEnddate != "day-month-year")
                            {
                                obj.ContractEndDate = DateTime.ParseExact(ContractEnddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.ContractEndDate = null;
                            }
                            if (IsShow && IsSubContractor == 1)
                                obj.IsShowContractor = 1;
                            else
                                obj.IsShowContractor = 0;

                            obj.IsDelete = false;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();

                            ReturnPath = "Success";
                        }
                    }
                    else
                    {
                        Vendor_ContractorProjectMapping obj = (from row in entities.Vendor_ContractorProjectMapping
                                                               where row.ID == ProjMappingID
                                                               select row).FirstOrDefault();

                        if (obj != null)
                        {
                            obj.CustID = CID;
                            obj.ContractorID = ContractorID;
                            obj.ProjectID = ProjectID;
                            obj.IsSubcontract = IsSubContractor;

                            if (IsSubContractor == 1)
                            {
                                obj.UnderContractorID = SubContractorID;
                                if (SpocSubContUserID != 0)
                                {
                                    obj.ProjectSubContSpocID = SpocSubContUserID;
                                }
                            }
                            else
                                obj.UnderContractorID = 0;

                            obj.Frequency = freq;
                            //obj.DueDate = Convert.ToDateTime(duedatefnal);
                            if (freq == "O")
                            {
                                obj.endDate = DateTime.ParseExact(enddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                obj.DueDate = DateTime.ParseExact(StartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.DueDate = Convert.ToDateTime(duedatefnal);
                            }
                            if (!string.IsNullOrEmpty(ContractStartDate) && ContractStartDate != "day-month-year")
                            {
                                obj.ContractStartDate = DateTime.ParseExact(ContractStartDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.ContractStartDate = null;
                            }
                            if (!string.IsNullOrEmpty(ContractEnddate) && ContractEnddate != "day-month-year")
                            {
                                obj.ContractEndDate = DateTime.ParseExact(ContractEnddate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                obj.ContractEndDate = null;
                            }
                            if (IsShow && IsSubContractor == 1)
                                obj.IsShowContractor = 1;
                            else
                                obj.IsShowContractor = 0;

                            obj.IsDelete = false;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;

                            if (SpocContUserID != 0)
                            {
                                obj.ProjectContSpocID = SpocContUserID;
                            }
                            entities.SaveChanges();

                            ReturnPath = "Success";
                        }
                    }
                    return Content(ReturnPath);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath);
            }
        }

        public ActionResult GetFile(string userpath, string ExportType)
        {
            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename);
            Response.BinaryWrite(VendorAuditMaster.ReadDocFiles(p_strPath)); // create the file
            Response.Flush(); // send it to the client to download
            HttpContext.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            return View();
        }

        public ActionResult ComplianceRemarksReport(int CustomerID, string location, string StartDate, string EndDate, string StatusID, string ProjectID, string Period, int UID, string role)
        {
            string ReturnPath = string.Empty;
            string reportName = string.Empty;
            string dtfrom = string.Empty;
            string dtto = string.Empty;
            string LocationNames = string.Empty;
            int OutputCount = 0;
            try
            {
                List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();

                #region Sheet 1   
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {

                        if (role.Equals("VCPD"))
                        {
                            output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                      select row).ToList();

                            output = output.Where(x => x.ProjectDirector == UID).ToList();

                        }
                        else if (role.Equals("VCPH"))
                        {
                            output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                      select row).ToList();

                            output = output.Where(x => x.ProjectHead == UID).ToList();

                        }
                        else if (role.Equals("VCMNG"))
                        {

                            List<int> ProjectIds = (from row in entities.Vendor_ProjectMGMTMapping
                                                    where row.UserID == UID && row.CustID == CustomerID
                                                    && row.IsDeleted == false
                                                    select row.ProjectId).ToList();

                            if (ProjectIds.Count > 0)
                            {
                                output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                          select row).ToList();

                                output = output.Where(x => ProjectIds.Contains(x.ProjectID)).ToList();
                            }
                        }
                        else if (role.Equals("VCCON"))
                        {
                            output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                      where row.CompliancechecklistStatus != null
                                      select row).ToList();

                            output = output.Where(x => x.ProjectContSpocID == UID || x.ProjectSubContSpocID == UID).ToList();

                        }
                        else if (role.Equals("VCCAD"))
                        {
                            output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                          //where row.CompliancechecklistStatus != null
                                      select row).ToList();
                        }
                        else
                        {
                            output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                      where row.CompliancechecklistStatus != null
                                      && row.AuditorID == UID
                                      select row).ToList();
                        }


                        OutputCount = output.Count;

                        #region Filter

                        if (output.Count > 0)
                        {
                            if (StatusID != "[]" && !string.IsNullOrEmpty(StatusID))
                            {
                                System.Collections.Generic.IList<int> StatusIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(StatusID);
                                if (StatusIDs.Count != 0)
                                {
                                    output = output.Where(x => StatusIDs.Contains(x.AuditCheckListStatusID)).ToList();
                                }
                            }
                            if (location != "[]" && location != null)
                            {
                                System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (locationIds.Count != 0)
                                {
                                    output = output.Where(x => locationIds.Contains(x.LocationID)).ToList();
                                }

                                var Branches = output.Select(x => x.Location).Distinct().ToList();
                                foreach (var item in Branches)
                                {
                                    LocationNames = LocationNames + item + ',';
                                }
                                if (!string.IsNullOrEmpty(LocationNames))
                                {
                                    LocationNames = LocationNames.TrimEnd(',');
                                }
                            }
                            else
                            {
                                LocationNames = "All Location";
                            }
                            if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                            {
                                System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                                if (ProjectIDs.Count != 0)
                                {
                                    output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(StartDate))
                            {
                                DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                output = output.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(EndDate))
                            {
                                DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                output = output.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                            }
                            if (!string.IsNullOrEmpty(Period))
                            {
                                output = output.Where(entry => (entry.Period == Period)).ToList();
                            }
                        }
                        #endregion
                    }
                    #region All Forum
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("MainLocation", typeof(string));
                        table.Columns.Add("ProjectName", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Period", typeof(string));
                        table.Columns.Add("ContractorName", typeof(string));
                        table.Columns.Add("Act", typeof(string));
                        table.Columns.Add("Compliance", typeof(string));
                        table.Columns.Add("ComplianceStatus", typeof(string));
                        table.Columns.Add("RevComplianceStatus", typeof(string));
                        table.Columns.Add("AuditorRemark", typeof(string));
                        table.Columns.Add("AuditeeRemark", typeof(string));

                        #region
                        {
                            output = output.OrderBy(entry => entry.ProjectContSpocID).ThenBy(x => x.ProjectID).ThenBy(x => x.ScheduleOn).ToList();
                            int P = 0;
                            string ProjectName = "";
                            string Periods = "";
                            foreach (var item in output)
                            {
                                var AuditCheckListStatusID = "";
                                if (item.AuditCheckListStatusID == 4)
                                {
                                    AuditCheckListStatusID = "Closed";
                                }
                                else if (item.AuditCheckListStatusID == 5)
                                {
                                    AuditCheckListStatusID = "Rejected";
                                }
                                else
                                {
                                    AuditCheckListStatusID = "";
                                }

                                var MainLocationName = "";
                                using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                                {

                                    var MainLocationID = (from row in entitiess.CustomerBranches
                                                          where row.ID == item.LocationID
                                                          select row.ParentID).FirstOrDefault();
                                    if (MainLocationID != null)
                                    {
                                        MainLocationName = (from row in entitiess.CustomerBranches
                                                            where row.ID == MainLocationID
                                                            select row.Name).FirstOrDefault();
                                    }
                                }

                                if (OutputCount == output.Count)
                                {
                                    if ((ProjectName != item.ProjectName && Periods != item.Period) || (ProjectName == item.ProjectName && Periods != item.Period))
                                    {
                                        ++P;
                                        table.Rows.Add(P, MainLocationName, item.ProjectName,item.Location, item.Period, item.ContractorName, item.ActName, item.ComplainceHeader, item.CompliancechecklistStatus, AuditCheckListStatusID, item.ComplianceChecklistReamrk, item.ReviewerRemark);
                                        ProjectName = item.ProjectName; Periods = item.Period;
                                    }
                                    else
                                    {
                                        table.Rows.Add("", "", "", "", "","", item.ActName, item.ComplainceHeader, item.CompliancechecklistStatus, AuditCheckListStatusID, item.ComplianceChecklistReamrk, item.ReviewerRemark);
                                    }
                                }
                                else
                                {
                                    ++P;
                                    table.Rows.Add(P, MainLocationName, item.ProjectName, item.Location, item.Period, item.ContractorName, item.ActName, item.ComplainceHeader, item.CompliancechecklistStatus, AuditCheckListStatusID, item.ComplianceChecklistReamrk, item.ReviewerRemark);
                                }
                            }

                            OfficeOpenXml.ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Compliances with Remarks");
                            //exWorkSheet.View.FreezePanes(1, 3);
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;

                            ExcelData = view.ToTable("Selected", false, "SrNo", "MainLocation", "ProjectName", "Location", "Period", "ContractorName", "Act", "Compliance", "ComplianceStatus", "RevComplianceStatus", "AuditorRemark", "AuditeeRemark");

                            #region Basic
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 6;

                            exWorkSheet.Cells["A2"].Value = "Created On";
                            exWorkSheet.Cells["A2"].AutoFitColumns(20);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.WrapText = true;

                            exWorkSheet.Cells["B2"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            exWorkSheet.Cells["B2"].AutoFitColumns(20);
                            exWorkSheet.Cells["B2"].Style.WrapText = true;

                            exWorkSheet.Cells["A3"].Value = "Location";
                            exWorkSheet.Cells["A3"].AutoFitColumns(20);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.WrapText = true;

                            exWorkSheet.Cells["B3"].Value = LocationNames;
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);
                            exWorkSheet.Cells["B3"].Style.WrapText = true;

                            exWorkSheet.Cells["A5"].Value = "Sr. No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["A5"].Style.WrapText = true;
                            exWorkSheet.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["B5"].Value = "Business";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["B5"].Style.WrapText = true;
                            exWorkSheet.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C5"].Value = "Project Name";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["C5"].Style.WrapText = true;
                            exWorkSheet.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["D5"].Value = "Location";
                            exWorkSheet.Cells["D5"].AutoFitColumns(20);
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["D5"].Style.WrapText = true;
                            exWorkSheet.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["E5"].Value = "Period";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["E5"].Style.WrapText = true;
                            exWorkSheet.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["E6:E" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            exWorkSheet.Cells["F5"].Value = "Contractor Name";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["F5"].Style.WrapText = true;
                            exWorkSheet.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F6:F" + count1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            exWorkSheet.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["G5"].Value = "Act Name";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["G5"].Style.WrapText = true;
                            exWorkSheet.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["H5"].Value = "Compliance";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["H5"].Style.WrapText = true;
                            exWorkSheet.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["I5"].Value = "Performer Status";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["I5"].Style.WrapText = true;
                            exWorkSheet.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["J5"].Value = "Reveiwer Status";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["J5"].Style.WrapText = true;
                            exWorkSheet.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["K5"].Value = "Auditee Remark";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["K5"].Style.WrapText = true;
                            exWorkSheet.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["L5"].Value = "Auditor Remark";
                            exWorkSheet.Cells["L5"].AutoFitColumns(20);
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(231, 230, 230));
                            exWorkSheet.Cells["L5"].Style.WrapText = true;
                            exWorkSheet.Cells["L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            #endregion Baic

                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            #region Merging cells

                            int j = 6;
                            int k = 6; ;
                            for (int i = 6; i <= 6 + ExcelData.Rows.Count; i++)
                            {
                                string chke = "A" + i;
                                string Checkcell = "";
                                if (exWorkSheet.Cells[chke].Value != null)
                                {
                                    Checkcell = exWorkSheet.Cells[chke].Value.ToString();
                                }
                                if (i > 6)
                                {
                                    if (string.IsNullOrEmpty(Checkcell))
                                    {
                                        j++;
                                    }
                                    else
                                    {
                                        string checknow1 = "A" + k + ":A" + j;
                                        string chekcnow2 = "B" + k + ":B" + j;
                                        string chekcnow3 = "C" + k + ":C" + j;
                                        string chekcnow4 = "D" + k + ":D" + j;
                                        string chekcnow5 = "E" + k + ":E" + j;
                                        string chekcnow6 = "E" + k + ":E" + j;


                                        exWorkSheet.Cells[checknow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        exWorkSheet.Cells[chekcnow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow5].Merge = true;
                                        exWorkSheet.Cells[chekcnow6].Merge = true;

                                        k = i;
                                        j = i;
                                    }
                                    if (j == (5 + ExcelData.Rows.Count))
                                    {
                                        string checknow1 = "A" + k + ":A" + j;
                                        string chekcnow2 = "B" + k + ":B" + j;
                                        string chekcnow3 = "C" + k + ":C" + j;
                                        string chekcnow4 = "D" + k + ":D" + j;
                                        string chekcnow5 = "E" + k + ":E" + j;


                                        exWorkSheet.Cells[checknow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        exWorkSheet.Cells[chekcnow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow5].Merge = true;
                                    }
                                }
                            }

                            #endregion

                            #region Column Width
                            exWorkSheet.Row(5).Height = 15;
                            exWorkSheet.Column(1).Width = 12;
                            exWorkSheet.Column(2).Width = 30;
                            exWorkSheet.Column(3).Width = 20;
                            exWorkSheet.Column(4).Width = 22;
                            exWorkSheet.Column(5).Width = 18;
                            exWorkSheet.Column(6).Width = 24;
                            exWorkSheet.Column(7).Width = 50;
                            exWorkSheet.Column(8).Width = 50;
                            exWorkSheet.Column(9).Width = 32;
                            exWorkSheet.Column(10).Width = 32;
                            exWorkSheet.Column(11).Width = 35;
                            exWorkSheet.Column(12).Width = 35;

                            #endregion

                            using (ExcelRange col = exWorkSheet.Cells[2, 1, 3, 2])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 12])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                    reportName = "Compliances_Remark_Report_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                    string p_strPath = string.Empty;
                    string dirpath = string.Empty;
                    p_strPath = @"" + storagedrive + "/" + reportName;
                    dirpath = @"" + storagedrive;
                    if (!Directory.Exists(dirpath))
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    FileStream objFileStrm = System.IO.File.Create(p_strPath);
                    objFileStrm.Close();
                    System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                    ReturnPath = reportName;
                }
                #endregion
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }

        public ActionResult SaveDataPointValues(List<DataPointDataWithValues> things)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                bool result = false;

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (things.Count > 0)
                    {
                        string UserName = string.Empty;
                        foreach (var DetailsObj in things)
                        {
                            if (DetailsObj.ParameterWithvalueForDataPoints != null)
                            {

                                var objResult1 = (from row in entities.Vendor_AuditDataPointParameter
                                                  where row.ContractorProjectMappingID == DetailsObj.ProjectContractMappingID
                                                  && row.ScheduleOnID == DetailsObj.scheduleonID
                                                  && row.CustomerID == DetailsObj.CustID
                                                  && row.ProjectID == DetailsObj.ProjectID
                                                  && row.ForMonth == DetailsObj.Period
                                                  && row.IsDeleted == false
                                                  select row).ToList();


                                if (objResult1.Count > 0)
                                {
                                    objResult1.ForEach(entry =>
                                    {
                                        entry.IsDeleted = true;
                                        entities.SaveChanges();
                                    });
                                }

                                foreach (var items in DetailsObj.ParameterWithvalueForDataPoints)
                                {
                                    if (items.ParameterValue == null) { items.ParameterValue = ""; }

                                    Vendor_AuditDataPointParameter objMapped = new Vendor_AuditDataPointParameter()
                                    {
                                        CustomerID = DetailsObj.CustID,
                                        ProjectID = DetailsObj.ProjectID,
                                        ContractorProjectMappingID = DetailsObj.ProjectContractMappingID,
                                        ScheduleOnID = DetailsObj.scheduleonID,
                                        ForMonth = DetailsObj.Period,
                                        DescType = items.ParameterType,
                                        Description = items.ParameterName,
                                        DescriptionValue = items.ParameterValue,
                                        CreatedBy = DetailsObj.CreatedBy,
                                        CreatedOn = DateTime.Today,
                                        UpdatedOn = DateTime.Today,
                                        IsDeleted = false,
                                    };
                                    entities.Vendor_AuditDataPointParameter.Add(objMapped);
                                    entities.SaveChanges();
                                    result = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult UpdateContractTypeMaster(int UserId, int CustomerID, int CTId, string CTName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistContractTypeMaster(UserId, CustomerID, CTId, CTName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateContTypeMaster(UserId, CustomerID, CTId, CTName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult DeleteContractTypeMaster(int UserId, int CustomerID, int CTId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.checkExistforContractTypeMaster(UserId, CustomerID, CTId);
                if (!output)
                {
                    bool output1 = VendorAuditMaster.DeleteContTypeMaster(UserId, CustomerID, CTId);
                    if (output1)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Used";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdatemappingClouserDate(int UserId, int CustomerID, int ProjectId,int ContractId,int mappingId, string ClouserDate, int Type)
        {
            try
            {
                string ReturnPath = "false";
                DateTime x = DateTime.ParseExact(ClouserDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                bool output = VendorAuditMaster.UpdateMappingClouserDate(UserId, CustomerID, ProjectId, ContractId, mappingId, x, Type);
                if (output)
                {
                    ReturnPath = "Success";
                }

                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }      
        
        public ActionResult UpdateClouserDate(int UserId, int CustomerID, int ProjectId, string ClouserDate,int Type)
        {
            try
            {
                string ReturnPath = "false";
                DateTime x = DateTime.ParseExact(ClouserDate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                bool output = VendorAuditMaster.UpdateClouserDate(UserId, CustomerID, ProjectId, x, Type);
                if (output)
                {
                    ReturnPath = "Success";
                }

                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateUserMaster(int UID, int CID, int Id, string Name, string LastName, int RoleID, string Email, string ContNo)
        {
            try
            {
                string ReturnPath = "false";

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

                User user = new User()
                {
                    FirstName = Name.ToString(),
                    LastName = LastName.ToString(),
                    Designation = "",
                    Email = Email.ToString(),
                    ContactNumber = ContNo,
                    Address = "",
                    //RoleID = -1,
                    IsHead = false,
                    CustomerID = CID,
                    PAN = "",
                    VendorAuditRoleID = RoleID,
                    UserName = Name.ToString(),
                    EnType = "M"
                };

                mst_User mstUser = new mst_User()
                {
                    FirstName = Name.ToString(),
                    LastName = LastName.ToString(),
                    Designation = "",
                    Email = Email.ToString(),
                    ContactNumber = ContNo,
                    Address = "",
                    //RoleID = RoleID,
                    IsExternal = false,
                    IsHead = false,
                    CustomerID = CID,
                    PAN = "",
                    VendorAuditRoleID = RoleID,
                    UserName = Name.ToString(),
                    EnType = "M"
                };
                int resultValue = 0;
                bool result = false;
                bool flagEmailExist = true;
                bool flagContactExist = true;
                bool emailExists;
                bool ContactExist;
                string UserName = UserManagement.GetByUsername(UID);
                
                if (Id > 0)
                {
                    user.ID = Id;
                    mstUser.ID = Id;
                    UserManagement.ContactExists(user, out ContactExist);
                    if (ContactExist)
                    {
                        ReturnPath = "Contact No Exist.";
                    }
                    else
                    {
                        UserManagementRisk.ContactExists(mstUser, out ContactExist);
                        if (ContactExist)
                        {
                            ReturnPath = "Contact No Exist.";
                        }
                        else
                        {
                            UserManagement.Exists(user, out emailExists);
                            if (emailExists)
                            {
                                ReturnPath = "User with Same Email Already Exists.";
                            }
                            else
                            {
                                UserManagementRisk.Exists(mstUser, out emailExists);
                                if (emailExists)
                                {
                                    ReturnPath = "User with Same Email Already Exists.";
                                }
                                else
                                {
                                    user.ID = Convert.ToInt32(Id);
                                    mstUser.ID = Convert.ToInt32(Id);

                                    result = UserManagement.Update(user, parameters);
                                    result = UserManagementRisk.Update(mstUser, parametersRisk);
                                    if (result)
                                    {
                                        ReturnPath = "Success";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {                  
                    user.RoleID = RoleID;
                    mstUser.RoleID = RoleID;
                    UserManagement.ContactExists(user, out ContactExist);
                    if (ContactExist)
                    {
                        ReturnPath = "Contact No Exist.";
                        flagContactExist = false;
                    }
                    UserManagementRisk.ContactExists(mstUser, out ContactExist);
                    if (ContactExist)
                    {
                        ReturnPath = "Contact No Exist.";
                        flagContactExist = false;
                    }
                    if (flagContactExist)
                    {
                        UserManagement.Exists(user, out emailExists);
                        if (emailExists)
                        {
                            ReturnPath = "Email ID Exist.";
                            flagEmailExist = false;
                        }
                        UserManagementRisk.Exists(mstUser, out emailExists);
                        if (emailExists)
                        {
                            ReturnPath = "Email ID Exist.";
                            flagEmailExist = false;
                        }
                        if (flagEmailExist)
                        {
                            user.CreatedBy = UID;
                            user.CreatedByText = UserName;

                            string passwordText = Util.CreateRandomPassword(10);
                            user.Password = Util.CalculateMD5Hash(passwordText);
                            string message = "true";
                            mstUser.CreatedBy = UID;
                            mstUser.CreatedByText = UserName;
                            
                            mstUser.Password = Util.CalculateMD5Hash(passwordText);

                            resultValue = UserManagement.CreateNewVendor(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message, passwordText);
                            if (resultValue > 0)
                            {
                                result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message,true);
                                if (result == false)
                                {
                                    UserManagement.deleteUser(resultValue);
                                }
                                if (result)
                                {
                                    ReturnPath = "Success";
                                }
                            }
                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult DeleteProjectDocument(int UserId, int CustomerID, int PId,int FID)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.DeleteProjectDocument(UserId, CustomerID, PId, FID);
                if (output)
                {
                    ReturnPath = "Success";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }
        
        public ActionResult UpdateProjectMaster(int ProjectID,int UserId, int CustomerID, string Projectoffice, int status,
            string name,string Jurisdiction,int catid,int elementid,int stateid,int cityid,string location,string locadd
            ,int sitestatus,int director,int head,string authname,string authmail,string authcontact,int ProjectoffID,
            int locationID)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistProjectMaster(ProjectID, CustomerID, name);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateProjectMaster(ProjectID, UserId, CustomerID, Projectoffice, status,
                     name, Jurisdiction, catid, elementid, stateid, cityid, location, locadd
                    , sitestatus, director, head, authname, authmail, authcontact, ProjectoffID,
            locationID);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateSiteStatusMaster(int UserId, int CustomerID, int SSId, string SSName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistSiteStatusMaster(UserId, CustomerID, SSId, SSName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateSiteStatusMaster(UserId, CustomerID, SSId, SSName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateElementMaster(int UserId, int CustomerID, int EId, string EName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistElementMaster(UserId, CustomerID, EId, EName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateElementMaster(UserId, CustomerID, EId, EName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateCategoryMaster(int UserId, int CustomerID, int catId, string catName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistCategoryMaster(UserId, CustomerID, catId, catName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateCategoryMaster(UserId, CustomerID, catId, catName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateStatusMaster(int UserId, int CustomerID, int StatusId, string StatusName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistStatusMaster(UserId, CustomerID, StatusId, StatusName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdatestatusMaster(UserId, CustomerID, StatusId, StatusName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult BindProject()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    var ProjectList = (from row in entities.Vendor_ContractorMaster
                                       where row.PID != 0
                                       select new ProjectDTO
                                       {
                                           PID = row.PID,
                                       }).Distinct().ToList();

                    return Json(ProjectList, JsonRequestBehavior.AllowGet);
                    //return JsonConvert.SerializeObject(ProjectList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult SaveContractDetails(int UserId, int CustomerID, int PID, int ContractorID, string ContractorName, string SPOCName, string SPOCEmailID, string SPOCMobileNo, string NatureOfWork,int ContType)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = Convert.ToInt32(UserId);
                    int CustID = Convert.ToInt32(CustomerID);
                    int id = Convert.ToInt32(ContractorID);
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        bool exist = VendorAuditMaster.CheckExistContractorMaster(UserId, CustomerID, SPOCEmailID, ContractorID);
                        if (exist)
                        {
                            Vendor_ContractorMaster updateDetails = (from row in entities.Vendor_ContractorMaster
                                                                     where row.ID == id
                                                                     && row.CustID == CustID
                                                                     select row).FirstOrDefault();

                            if (updateDetails != null)
                            {
                                bool output1 = UserManagement.checkContactExists(SPOCMobileNo, updateDetails.SpocEmail);
                                bool output = UserManagement.checkContactAduitExists(SPOCMobileNo, updateDetails.SpocEmail);
                                if (output == true || output1 == true)
                                {
                                    return Json("Contact No Exist.", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);                                
                                }
                                else
                                {
                                    bool outputEmail1 = UserManagement.checkEmailExists(updateDetails.SpocEmail, (int)updateDetails.AvacomUserID);
                                    bool outputEmail = UserManagement.checkauditEmailExists(updateDetails.SpocEmail, (int)updateDetails.AvacomUserID);
                                    if (outputEmail1 == true || outputEmail == true)
                                    {
                                        return Json("Email Exist.", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);                                
                                    }
                                    else
                                    {
                                        updateDetails.ContractorType = ContType;
                                        updateDetails.ContractorName = ContractorName;
                                        updateDetails.SpocName = SPOCName;
                                        updateDetails.SpocEmail = SPOCEmailID;
                                        updateDetails.SpocContactNo = SPOCMobileNo;
                                        updateDetails.NatureOfWork = NatureOfWork;
                                        updateDetails.UpdatedBy = UserID;
                                        updateDetails.UpdatedOn = DateTime.Now;
                                        updateDetails.IsDelete = false;
                                        entities.SaveChanges();

                                        UserManagement.UpdateUserDetail((int)updateDetails.AvacomUserID, SPOCName, CustID, SPOCEmailID, SPOCMobileNo);

                                        return Json("Success", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                                    }
                                }
                            }
                            else
                            {
                                #region add new detail

                                bool output1 = UserManagement.checkContactExists1(SPOCMobileNo);
                                bool output = UserManagement.checkContactAduitExists1(SPOCMobileNo);
                                if (output == true || output1 == true)
                                {
                                    return Json("Contact No Exist.", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);                                
                                }
                                else
                                {
                                    bool outputEmail1 = UserManagement.checkEmailExists1(SPOCEmailID);
                                    bool outputEmail = UserManagement.checkauditEmailExists1(SPOCEmailID);
                                    if (outputEmail1 == true || outputEmail == true)
                                    {
                                        return Json("Email Exist.", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);                                
                                    }
                                    else
                                    {
                                        int RoleID = UserManagement.GetRoleIdByCode("VCCON");

                                        int avacomuserid = UserManagement.UpdateUserDetail(SPOCEmailID, SPOCName, SPOCMobileNo, CustID, RoleID, UserID,true);
                                        if (avacomuserid > 0)
                                        {
                                            Vendor_ContractorMaster ContractorMaster = new Vendor_ContractorMaster()
                                            {
                                                ID = Convert.ToInt32(ContractorID),
                                                PID = PID,
                                                CustID = CustID,
                                                ContractorName = ContractorName,
                                                SpocName = SPOCName,
                                                SpocContactNo = SPOCMobileNo,
                                                SpocEmail = SPOCEmailID,
                                                NatureOfWork = NatureOfWork,
                                                CreatedBy = UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = UserID,
                                                UpdatedOn = DateTime.Now,
                                                ContractorType = ContType,
                                                IsDelete = false,
                                                AvacomUserID = avacomuserid
                                            };
                                            entities.Vendor_ContractorMaster.Add(ContractorMaster);
                                            entities.SaveChanges();

                                            return Json("Success", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                                        }
                                        else
                                        {
                                            return Json("Error", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                                        }
                                    }
                                }
                                #endregion
                            }                           
                        }
                        else
                        {
                            return Json("Exist", JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                        }                                                  
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult UpdateCityMaster(int UserId, int CustomerID, int CityId, int StateID, string CityName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistCityMaster(UserId, CustomerID, CityId, StateID, CityName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateCityMaster(UserId, CustomerID, CityId, StateID, CityName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult DeleteCityMaster(int UserId, int CustomerID, int CityId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.checkExistforCityMaster(UserId, CustomerID, CityId);
                if (!output)
                {
                    bool output3 = VendorAuditMaster.checkExistforCityMasterinbranch(UserId, CustomerID, CityId);
                    if (!output3)
                    {
                        bool output1 = VendorAuditMaster.DeleteCityMaster(UserId, CustomerID, CityId);
                        if (output1)
                        {
                            ReturnPath = "Success";
                        }
                    }
                    else
                    {
                        ReturnPath = "Used";
                    }
                }
                else
                {
                    ReturnPath = "Used";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult UpdateLicenseMaster(int UserId, int CustomerID, int LicId, string LicName)
        {
            try
            {
                string ReturnPath = "false";
                bool exist = VendorAuditMaster.CheckExistLicMaster(UserId, CustomerID, LicId, LicName);
                if (exist)
                {
                    bool output = VendorAuditMaster.UpdateLicMaster(UserId, CustomerID, LicId, LicName);
                    if (output)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Exist";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        public ActionResult SaveEntityDetails(CustomerBranch DetailsObj)
        {
            string result = string.Empty;
            try
            {
                result = VendorAuditMaster.SaveEntityDetails(DetailsObj);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
   
        public ActionResult DeleteEntityDetails(CustomerBranch DetailsObj)
        {

            try
            {
                VendorAuditMaster.DeleteEntityDetails(DetailsObj);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteLicenseMaster(int UserId, int CustomerID, int LICId)
        {
            try
            {
                string ReturnPath = "false";
                bool output = VendorAuditMaster.checkExistforLicMaster(UserId, CustomerID, LICId);
                if (!output)
                {
                    bool output1 = VendorAuditMaster.DeleteLicMaster(UserId, CustomerID, LICId);
                    if (output1)
                    {
                        ReturnPath = "Success";
                    }
                }
                else
                {
                    ReturnPath = "Used";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string ReturnPath = "Error";
                return Content(ReturnPath);
            }
        }

        #region Vishal

        public ActionResult GetAllActsInTemplateDropdown(int CID, string category, string CentralState, int StateID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_ActMaster
                                   where row.IsDeleted == false
                                   && row.CustomerID == CID
                                   select row);

                    actList = actList.Where(x => x.IsDisabled == false || x.IsDisabled == null || x.IsDisabled == true && x.DisabledDate >= DateTime.Today);

                    if (!string.IsNullOrEmpty(category))
                        actList = actList.Where(x => x.Category == category);

                    if (!string.IsNullOrEmpty(CentralState))
                        actList = actList.Where(x => x.CentralORState == CentralState);

                    if (StateID != 0)
                        actList = actList.Where(x => x.StateID == StateID);
                    List<ActMasterDTO> actFinalList = new List<ActMasterDTO>();

                    foreach (var item in actList)
                    {
                        ActMasterDTO finalData = new ActMasterDTO();
                        finalData.ID = item.ID;
                        finalData.ActName = item.ActName;
                        finalData.CentralORState = item.CentralORState;
                        if (item.CentralORState == "State")
                            finalData.StateID = item.StateID;
                        finalData.IsDeleted = item.IsDeleted;
                        finalData.Category = item.Category;
                        actFinalList.Add(finalData);
                    }

                    actFinalList = actFinalList.OrderBy(x => x.ActName).ToList();
                    return Json(actFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public FileResult DownloadSampleFile(string SampleFileName, string FilePath)
        {
            try
            {
                FilePath = FilePath.Replace(@"~", "");
                FilePath = FilePath.Substring(1);
                FilePath = FilePath.Replace(@"/", "\\");
                var path = ConfigurationManager.AppSettings["ComplianceDocument"] + "\\" + FilePath;//+ "\\" + SampleFileName;
                string extension = Path.GetExtension(SampleFileName);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return File(stream, System.Net.Mime.MediaTypeNames.Application.Octet, SampleFileName);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region Act Bulk Upload

        [System.Web.Http.HttpPost]
        public ActionResult SendUploadFile(HttpRequestMessage request, int CustID, int UserID, string formName)
        {
            string message = string.Empty;
            //var httpRequest = HttpContext.Current.Request;
            HttpFileCollectionBase httpRequest = Request.Files;
            HttpPostedFile file = null;

            try
            {
                if (httpRequest.Count > 0)
                {
                    Inputfile = httpRequest[0];
                    if (Inputfile.FileName.EndsWith(".xls") || Inputfile.FileName.EndsWith(".xlsx"))
                    {
                        if (Inputfile != null && Inputfile.ContentLength > 0)
                        {
                            string fileName = Inputfile.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = Inputfile.ContentType;
                            byte[] fileBytes = new byte[Inputfile.ContentLength];

                            var data = Inputfile.InputStream.Read(fileBytes, 0, Convert.ToInt32(Inputfile.ContentLength));

                            using (var package = new ExcelPackage(Inputfile.InputStream))
                            {
                                if (formName.Equals("Act"))
                                    message = ProcessActFileData(package, fileWithoutExtn, CustID, UserID);
                                else if (formName.Equals("Compliance"))
                                    message = ProcessComplianceFileData(package, fileWithoutExtn, CustID, UserID);
                                else
                                    message = ProcessTemplateFileData(package, fileWithoutExtn, CustID, UserID);

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return Json(message);
        }

        public Vendor_ComplainceMaster GetCompliancebyName(string ComplianceName)
        {
            Vendor_ComplainceMaster result = new Vendor_ComplainceMaster();
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    Vendor_ComplainceMaster compIDList = (from row in entities.Vendor_ComplainceMaster
                                                          where row.IsDeleted == false && row.ComplainceHeader == ComplianceName && row.IsDisabled != true
                                                          select row).FirstOrDefault();

                    //result.Add(compIDList.ID);
                    //if (compIDList.ActID > 0)
                    //{
                    //    result.Add(compIDList.ActID);
                    //}
                    //result.Add(compIDList.Statutory);
                    result = compIDList;

                }
            }
            catch (Exception ex)
            {
                result = null;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public Vendor_ComplainceMaster GetCompliancebyName(string ComplianceName, int CustID)
        {
            Vendor_ComplainceMaster result = new Vendor_ComplainceMaster();
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    Vendor_ComplainceMaster compIDList = (from row in entities.Vendor_ComplainceMaster
                                                          where row.IsDeleted == false && row.ComplainceHeader == ComplianceName && row.IsDisabled != true && row.CustomerId == CustID
                                                          select row).FirstOrDefault();

                    //result.Add(compIDList.ID);
                    //if (compIDList.ActID > 0)
                    //{
                    //    result.Add(compIDList.ActID);
                    //}
                    //result.Add(compIDList.Statutory);
                    result = compIDList;

                }
            }
            catch (Exception ex)
            {
                result = null;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        internal string ProcessTemplateFileData(ExcelPackage package, string fileName, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["TemplateBulkUpload"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);
                    List<String> LogErrors = new List<string>();
                    List<Vendor_TemplateMaster> LstObj = new List<Vendor_TemplateMaster>();
                    List<Vendor_AssignedTemplate> assignedCompliance = new List<Vendor_AssignedTemplate>();
                    List<List<Vendor_AssignedTemplate>> LstObjCompliance = new List<List<Vendor_AssignedTemplate>>();
                    string tempplateType = "";
                    string compareTemplateType = "";
                    string finaltempplateType = "";


                    if (noOfRow > 1)
                    {
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            Vendor_TemplateMaster vendorTemplate = new Vendor_TemplateMaster();
                            Vendor_AssignedTemplate vendorCompliance = new Vendor_AssignedTemplate();

                            string previousTemplateName = xlWorksheet.Cells[i - 1, 1].Text.Trim();
                            string previousComplianceHeade = xlWorksheet.Cells[i - 1, 2].Text.Trim();



                            //Template Name
                            string templateName = xlWorksheet.Cells[i, 1].Text.Trim();

                            Vendor_ComplainceMaster newList = new Vendor_ComplainceMaster();
                            string assignedComp = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (!string.IsNullOrEmpty(assignedComp))
                            {
                                newList = GetCompliancebyName(assignedComp, CustID);

                                if (newList != null)
                                {
                                    tempplateType = newList.Statutory;

                                    if (!string.Equals(tempplateType, compareTemplateType) && string.Equals(previousTemplateName, templateName))
                                    {
                                        finaltempplateType = "All";
                                    }
                                    else
                                    {
                                        finaltempplateType = tempplateType;
                                    }
                                }
                            }

                            if (newList != null)
                            {

                                if (!string.Equals(previousTemplateName, templateName))
                                {

                                    if (string.IsNullOrEmpty(templateName))
                                    {
                                        LogErrors.Add("Required Template Name at Row - " + count);
                                    }
                                    else
                                    {
                                        if (CheckTemplateNameExist(templateName, CustID) == false)
                                        {
                                            LogErrors.Add("Template Name : " + templateName + " already exist at Row-" + count);

                                        }
                                        else
                                        {
                                            vendorTemplate.TemplateName = templateName;
                                            vendorTemplate.TemplateTye = finaltempplateType;
                                            if (assignedCompliance.Count > 0)
                                                LstObjCompliance.Add(assignedCompliance);
                                            assignedCompliance = new List<Vendor_AssignedTemplate>();
                                        }
                                    }
                                }
                                else
                                {
                                    vendorTemplate.TemplateTye = finaltempplateType;
                                }


                                if (string.IsNullOrEmpty(assignedComp))
                                    LogErrors.Add("Required Compliance Header at Row - " + count);
                                else
                                {
                                    if (string.Equals(previousTemplateName, templateName))
                                    {
                                        if (string.Equals(assignedComp, previousComplianceHeade))
                                        {
                                            LogErrors.Add(assignedComp + ": Compliance Header already exist for " + templateName + " at Row - " + count);
                                        }
                                        else
                                        {
                                            vendorCompliance.ComplianceID = newList.ID;
                                            vendorCompliance.ActID = newList.ActID;
                                        }
                                    }
                                    else
                                    {
                                        vendorCompliance.ComplianceID = newList.ID;
                                        vendorCompliance.ActID = newList.ActID;
                                    }
                                }

                            }
                            else
                            {
                                LogErrors.Add("Compliance Header is not Found or It may be In-Active. Please check at Row - " + count);
                            }

                            assignedCompliance.Add(vendorCompliance);

                            LstObj.Add(vendorTemplate);

                            if (string.Equals(previousTemplateName, templateName))
                            {
                                if (LstObj.Where(x => x.TemplateTye == "All").Count() > 0)
                                {
                                    //foreach (var item in LstObj)
                                    //{
                                    //    item.TemplateTye = "All";
                                    //}
                                    Vendor_TemplateMaster newlist = LstObj.Find(x => x.TemplateName == templateName);
                                    if (newlist != null)
                                    {
                                        newlist.TemplateTye = "All";
                                        newlist.TemplateName = templateName;
                                        LstObj = LstObj.Where(x => x.TemplateName != templateName).ToList();
                                        LstObj.Add(newlist);
                                    }
                                }
                            }

                            LstObj = LstObj.Where(s => !string.IsNullOrWhiteSpace(s.TemplateName)).Distinct().ToList();


                            compareTemplateType = tempplateType;

                        }

                        if (assignedCompliance.Count > 0)
                            LstObjCompliance.Add(assignedCompliance);

                        if (LogErrors.Count <= 0)
                        {
                            int compCount = 0;
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {

                                foreach (var addTemplate in LstObj)
                                {
                                    Vendor_TemplateMaster objTemplate = new Vendor_TemplateMaster();
                                    objTemplate.ID = 0;
                                    objTemplate.TemplateName = addTemplate.TemplateName;
                                    objTemplate.CustomerID = CustID;
                                    objTemplate.CreatedBy = UserID;
                                    objTemplate.CreatedOn = DateTime.Today;
                                    objTemplate.IsDeleted = false;
                                    objTemplate.TemplateTye = addTemplate.TemplateTye;
                                    entities.Vendor_TemplateMaster.Add(objTemplate);
                                    entities.SaveChanges();
                                    message = "success";
                                    int templateID = objTemplate.ID;

                                    if (templateID > 0)
                                    {
                                        foreach (var item in LstObjCompliance[compCount])
                                        {

                                            Vendor_AssignedTemplate objnewAssignedTemplate = new Vendor_AssignedTemplate();
                                            objnewAssignedTemplate.ID = 0;
                                            objnewAssignedTemplate.TemplateID = templateID;
                                            objnewAssignedTemplate.CustomerID = CustID;
                                            objnewAssignedTemplate.IsDeleted = false;
                                            objnewAssignedTemplate.CreatedBy = UserID;
                                            objnewAssignedTemplate.CreatedOn = DateTime.Today;
                                            objnewAssignedTemplate.TemplateTye = item.TemplateTye;
                                            objnewAssignedTemplate.ComplianceID = item.ComplianceID;
                                            objnewAssignedTemplate.ActID = item.ActID;
                                            entities.Vendor_AssignedTemplate.Add(objnewAssignedTemplate);
                                            entities.SaveChanges();
                                            message = "success";

                                        }

                                    }

                                    compCount = compCount + 1;
                                }

                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, Inputfile.FileName, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        message = "norecord";
                    }
                }
                else
                {
                    message = "nofile";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }


        internal string ProcessComplianceFileData(ExcelPackage package, string fileName, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["ComplianceBulkUpload"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);
                    List<String> LogErrors = new List<string>();
                    List<Vendor_ComplainceMaster> LstObj = new List<Vendor_ComplainceMaster>();
                    List<List<Vendor_CompMasterParameter>> LstObjParameter = new List<List<Vendor_CompMasterParameter>>();
                    int? stateID = 0;

                    if (noOfRow > 1)
                    {
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            Vendor_ComplainceMaster vendorCompliance = new Vendor_ComplainceMaster();
                            Vendor_CompMasterParameter vendorParameter = null;
                            List<Vendor_CompMasterParameter> parameters = new List<Vendor_CompMasterParameter>();

                            //Statutory Or Internal
                            string statutoryOrInternal = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(statutoryOrInternal))
                            {
                                LogErrors.Add("Required Central Or State at Row-" + count);
                            }
                            else if (!(statutoryOrInternal.Equals("Statutory") || statutoryOrInternal.Equals("Internal")))
                            {
                                LogErrors.Add("Please enter Central or State." + statutoryOrInternal + " is not valid at Row- " + count);
                            }
                            else
                                vendorCompliance.Statutory = statutoryOrInternal;

                            if (statutoryOrInternal.Equals("Statutory"))
                            {
                                //centralOrState
                                string centralorState = xlWorksheet.Cells[i, 2].Text.Trim();
                                if (string.IsNullOrEmpty(centralorState))
                                {
                                    LogErrors.Add("Required Central Or State at Row- " + count);
                                }
                                else if (!(centralorState.Equals("Central") || centralorState.Equals("State")))
                                {
                                    LogErrors.Add("Please enter Central or State." + centralorState + " is not valid at Row- " + count);
                                }
                                else
                                    vendorCompliance.CentralOrState = centralorState;

                                //state

                                if (centralorState == "State")
                                {
                                    string state = xlWorksheet.Cells[i, 3].Text.Trim();
                                    if (string.IsNullOrEmpty(state)) LogErrors.Add("Required State at Row - " + count);
                                    else
                                    {
                                        stateID = GetStateByName(state.Trim());
                                        if (stateID != 0)
                                        {
                                            vendorCompliance.StateID = Convert.ToInt32(stateID);
                                        }
                                        else
                                        {
                                            LogErrors.Add("State : " + state + " is not found at Row - " + count);
                                        }
                                    }
                                }
                                if (centralorState == "Central" && !string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.Trim()))
                                {
                                    LogErrors.Add("Central Act should not contain State at Row - " + count);
                                }

                                //ActName
                                int actID = 0;
                                string actName = xlWorksheet.Cells[i, 4].Text.Trim();
                                if (string.IsNullOrEmpty(actName)) LogErrors.Add("Required Act Name at Row - " + count);
                                else
                                {
                                    if (stateID == 0)
                                        actID = GetActIdByName(actName, CustID, xlWorksheet.Cells[i, 2].Text.Trim());
                                    else
                                        actID = GetActIdByName(actName, CustID, xlWorksheet.Cells[i, 2].Text.Trim(), stateID);
                                    if (actID != 0)
                                    {
                                        vendorCompliance.ActID = Convert.ToInt32(actID);
                                    }
                                    else
                                    {
                                        LogErrors.Add("Act : " + actName + " is not found or It may be In-Active. Please check at Row - " + count);
                                    }
                                }
                            }

                            if (statutoryOrInternal == "Internal" && (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.Trim()) || !string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.Trim()) || !string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.Trim())))
                            {
                                LogErrors.Add("Internal Compliance should not contain Central/State, State and Act Name at Row-" + count);
                            }

                            //contract type
                            string complianceHeader = xlWorksheet.Cells[i, 5].Text.Trim();
                            if (string.IsNullOrEmpty(complianceHeader)) LogErrors.Add("Required Compliance Header at Row-" + count);
                            else
                            {

                                //if (LstObj.Count > 0)
                                //{
                                //    foreach (var item in LstObj)
                                //    {
                                //        if (item.ComplainceHeader != null)
                                //        {
                                //            if (complianceHeader == item.ComplainceHeader.Trim())
                                //            {
                                //                if (stateID == item.StateID)
                                //                {

                                //                     LogErrors.Add("Compliance Header : " + complianceHeader + " already exist for "+ xlWorksheet.Cells[i, 3].Text.Trim() + " at Row-" + count);

                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                                if (CheckComplianceNameExist(complianceHeader, CustID, stateID) == false)
                                {
                                    LogErrors.Add("Compliance Header : " + complianceHeader + " already exist at Row-" + count);
                                    vendorCompliance.ComplainceHeader = complianceHeader;
                                }
                                else
                                {
                                    vendorCompliance.ComplainceHeader = complianceHeader;
                                }

                            }

                            //ComplianceDescription
                            vendorCompliance.ComplianceDescription = xlWorksheet.Cells[i, 6].Text.Trim();

                            vendorCompliance.DetailedDesc = xlWorksheet.Cells[i, 7].Text.Trim();

                            vendorCompliance.Section = xlWorksheet.Cells[i, 8].Text.Trim();

                            //25Aug
                            //Consequences
                            vendorCompliance.Consequences = xlWorksheet.Cells[i, 9].Text.Trim();

                            vendorCompliance.Mitigation = xlWorksheet.Cells[i, 10].Text.Trim();

                            //Document
                            string documentUpload = xlWorksheet.Cells[i, 11].Text.Trim();
                            //if (string.IsNullOrEmpty(category)) LogErrors.Add("Required Category at Row-" + count);
                            if (!string.IsNullOrEmpty(documentUpload))
                            {
                                if (documentUpload == "Yes" || documentUpload == "No")
                                {
                                    if (documentUpload.Equals("Yes"))
                                        vendorCompliance.IsDocument = true;
                                    else
                                        vendorCompliance.IsDocument = false;
                                }
                                else
                                {
                                    LogErrors.Add("Document (Yes/No) : " + documentUpload + " is not found at Row-" + count);
                                }
                            }

                            //Risk Category
                            string riskCategory = xlWorksheet.Cells[i, 12].Text.Trim();
                            if (!string.IsNullOrEmpty(riskCategory))
                            {
                                if (riskCategory == "High" || riskCategory == "Medium" || riskCategory == "Low")
                                {
                                    vendorCompliance.RiskCategory = riskCategory;
                                }
                                else
                                {
                                    LogErrors.Add("Risk Category : " + riskCategory + " is not found at Row-" + count);
                                }
                            }

                            for (int noofParameters = 13; noofParameters <= noOfCol; noofParameters++)
                            {
                                vendorParameter = new Vendor_CompMasterParameter();
                                string parameterName1 = xlWorksheet.Cells[i, noofParameters].Text.Trim();
                                noofParameters = noofParameters + 1;
                                if (!string.IsNullOrEmpty(parameterName1))
                                {
                                    bool parameterNameExist = parameters.Any(s => s.ParameterName.Contains(parameterName1));
                                    if (parameterNameExist)
                                    {
                                        LogErrors.Add(parameterName1 + " - Parameter Name already Exist for same Compliance at Row-" + count);
                                    }
                                    else
                                    {
                                        vendorParameter.ParameterName = parameterName1;
                                        string parameterType1 = xlWorksheet.Cells[i, noofParameters].Text.Trim();
                                        //noofParameters = noofParameters + 1;
                                        if (string.IsNullOrEmpty(parameterType1))
                                            LogErrors.Add("Required Parameter Type for " + parameterName1 + " at Row-" + count);
                                        else
                                        {
                                            if (parameterType1 == "Text" || parameterType1 == "Number" || parameterType1 == "Date")
                                            {
                                                vendorParameter.ParameterType = parameterType1;
                                            }
                                            else
                                            {
                                                LogErrors.Add("Required Parameter Type should be Text, Number or Date" + " at Row-" + count);
                                            }

                                        }
                                        parameters.Add(vendorParameter);
                                    }
                                }
                                else if ((!string.IsNullOrEmpty(xlWorksheet.Cells[i, noofParameters].Text.Trim())) && string.IsNullOrEmpty(parameterName1))
                                {
                                    LogErrors.Add("Required Parameter Name for " + xlWorksheet.Cells[i, noofParameters].Text.Trim() + " at Row-" + count);
                                }
                            }


                            LstObj.Add(vendorCompliance);
                            LstObjParameter.Add(parameters);

                        }


                        if (LogErrors.Count <= 0)
                        {
                            int compCount = 0;
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                foreach (var addact in LstObj)
                                {
                                    Vendor_ComplainceMaster objAct = new Vendor_ComplainceMaster();
                                    objAct.ID = 0;
                                    objAct.Statutory = addact.Statutory;
                                    objAct.CentralOrState = addact.CentralOrState;
                                    objAct.StateID = addact.StateID;
                                    objAct.ActID = addact.ActID;
                                    objAct.ComplainceHeader = addact.ComplainceHeader;
                                    objAct.IsDocument = addact.IsDocument;
                                    objAct.RiskCategory = addact.RiskCategory;
                                    objAct.CreatedOn = DateTime.Today;
                                    objAct.CreatedBy = UserID;
                                    objAct.CustomerId = CustID;
                                    objAct.IsDeleted = false;
                                    //25Aug
                                    objAct.Consequences = addact.Consequences;
                                    objAct.Mitigation = addact.Mitigation;
                                    objAct.DetailedDesc = addact.DetailedDesc;
                                    objAct.Section = addact.Section;
                                    objAct.ComplianceDescription = addact.ComplianceDescription;
                                    entities.Vendor_ComplainceMaster.Add(objAct);
                                    entities.SaveChanges();
                                    message = "success";
                                    int complainceID = objAct.ID;

                                    if (complainceID > 0)
                                    {
                                        foreach (var item in LstObjParameter[compCount])
                                        {
                                            Vendor_CompMasterParameter objNewPara = new Vendor_CompMasterParameter();
                                            objNewPara.ID = 0;
                                            objNewPara.ComplianceId = complainceID;
                                            objNewPara.ParameterName = item.ParameterName;
                                            objNewPara.ParameterType = item.ParameterType;
                                            objNewPara.CustomerID = CustID;
                                            objNewPara.IsDeleted = false;
                                            objNewPara.CreatedBy = UserID;
                                            objNewPara.CreatedOn = DateTime.Today;
                                            entities.Vendor_CompMasterParameter.Add(objNewPara);
                                            entities.SaveChanges();
                                            message = "success";
                                        }

                                    }

                                    compCount = compCount + 1;
                                }

                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, Inputfile.FileName, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        message = "norecord";
                    }
                }
                else
                {
                    message = "nofile";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }

        internal string ProcessActFileData(ExcelPackage package, string fileName, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["ActBulkUpload"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);
                    List<String> LogErrors = new List<string>();
                    //List<Vendor_ActMaster> LstObj = new List<Vendor_ActMaster>();
                    List<int> contractID = new List<int>();
                    List<ActMasterDTO> LstObj = new List<ActMasterDTO>();
                    if (noOfRow > 1)
                    {
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            //Vendor_ActMaster vendorAct = new Vendor_ActMaster();
                            ActMasterDTO vendorAct = new ActMasterDTO();
                            //centralorState
                            string centralorState = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(centralorState))
                            {
                                LogErrors.Add("Required Central Or State at Row-" + count);
                            }
                            else if (!(centralorState.Equals("Central") || centralorState.Equals("State")))
                            {
                                LogErrors.Add("Please enter Central or State." + centralorState + " is not valid." + count);
                            }
                            //else if (!centralorState.Equals("State"))
                            //{
                            //    LogErrors.Add("Please enter Central or State." + centralorState + " is not valid." + count);
                            //}
                            else
                                vendorAct.CentralORState = centralorState;

                            //state

                            int? stateID = null;
                            if (centralorState == "State")
                            {
                                string state = xlWorksheet.Cells[i, 2].Text.Trim();
                                if (string.IsNullOrEmpty(state)) LogErrors.Add("Required State at Row-" + count);
                                else
                                {
                                    stateID = GetStateByName(state.Trim());
                                    if (stateID != 0)
                                    {
                                        vendorAct.StateID = Convert.ToInt32(stateID);
                                    }
                                    else
                                    {
                                        LogErrors.Add("State : " + state + " is not found at Row-" + count);
                                    }
                                }
                            }
                            if (centralorState == "Central" && !string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.Trim()))
                            {
                                LogErrors.Add("Central Act should not contain State at Row-" + count);
                            }

                            //ActName

                            string actName = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(actName)) LogErrors.Add("Required Act Name at Row-" + count);
                            else
                            {
                                if (CheckActNameExist(actName, CustID, stateID) == false)
                                {
                                    LogErrors.Add("Act : " + actName + " already exist at Row-" + count);
                                }
                                else
                                {
                                    vendorAct.ActName = actName;
                                }

                            }

                            //contract type
                            string contractType = xlWorksheet.Cells[i, 4].Text.Trim();

                            if (string.IsNullOrEmpty(contractType)) LogErrors.Add("Required Contract Type at Row-" + count);
                            else
                            {
                                List<string> cType = contractType.Split(',').ToList<string>();

                                foreach (var item in cType)
                                {
                                    contractID.Add(GetContractByName(item, CustID));
                                }

                                //contractID = GetContractByName(contractType.Trim(), CustID);
                                List<int> finalContractTypeID = new List<int>();
                                foreach (int item in contractID)
                                {
                                    if (item == 0)
                                        LogErrors.Add("Contract Type from following : " + contractType + " is not found at Row-" + count);
                                    else
                                        finalContractTypeID.Add(item);
                                }
                                vendorAct.ContractType = finalContractTypeID;
                                contractID = new List<int>();
                                //if (contractID.Count() > 0)
                                //{
                                //    vendorAct.ContractType = contractID;
                                //}
                                //else
                                //{
                                //    LogErrors.Add("Contract Type : " + contractType + " is not found at Row-" + count);
                                //}

                            }

                            //Category
                            string category = xlWorksheet.Cells[i, 5].Text.Trim();
                            if (string.IsNullOrEmpty(category)) LogErrors.Add("Required Category at Row-" + count);
                            else
                            {
                                if (category == "Labour" || category == "Finance & Taxation" || category == "Secretarial" || category == "EHS" || category == "General" || category == "Commercial" || category == "Industry Specific")
                                    vendorAct.Category = category;
                                else
                                {
                                    LogErrors.Add("Category : " + category + " is not found at Row-" + count);
                                }
                            }

                            LstObj.Add(vendorAct);
                        }


                        if (LogErrors.Count <= 0)
                        {
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                foreach (var addact in LstObj)
                                {
                                    Vendor_ActMaster objAct = new Vendor_ActMaster();
                                    objAct.ID = 0;
                                    objAct.CentralORState = addact.CentralORState;
                                    objAct.StateID = addact.StateID;
                                    objAct.ActName = addact.ActName;
                                    //objAct.ContractType = addact.ContractType;
                                    objAct.Category = addact.Category;
                                    objAct.CreatedBy = UserID;
                                    objAct.CustomerID = CustID;
                                    objAct.CreatedOn = DateTime.Today;
                                    objAct.IsDeleted = false;
                                    entities.Vendor_ActMaster.Add(objAct);
                                    entities.SaveChanges();
                                    message = "success";
                                    int ActID = objAct.ID;
                                    //List<int> ContractTypeIDs = new List<int>();

                                    //GetContractByName(string contractName, int CustID)

                                    if (ActID > 0 && addact.ContractType.Count > 0)//contractID.Count
                                    {
                                        foreach (var item in addact.ContractType)
                                        {
                                            Vendor_ActContractType chk = (from row in entities.Vendor_ActContractType
                                                                          where row.ActID == ActID
                                                                          && row.ContractType == item
                                                                          && row.CustID == CustID
                                                                          select row).FirstOrDefault();

                                            if (chk == null)
                                            {
                                                Vendor_ActContractType ActContractType = new Vendor_ActContractType()
                                                {
                                                    ActID = ActID,
                                                    ContractType = Convert.ToInt32(item),
                                                    CustID = CustID,
                                                    CreatedBy = UserID,
                                                    CreatedOn = DateTime.Today,
                                                    UpdatedOn = DateTime.Today,
                                                    UpdatedBy = UserID,
                                                    IsChecked = true
                                                };
                                                entities.Vendor_ActContractType.Add(ActContractType);
                                                entities.SaveChanges();
                                                message = "success";
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, Inputfile.FileName, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        message = "norecord";
                    }
                }
                else
                {
                    message = "nofile";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }

        static int GetLastUsedRow(ExcelWorksheet sheet)
        {
            if (sheet.Dimension == null) { return 0; } // In case of a blank sheet
            var row = sheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }

        public string WriteLog(List<string> Errors, string ExcelName, out string file)
        {
            try
            {
                //errorLogFilename = "";

                // errorLogFilename = @"\RLCS_ErrorLog_"+ ExcelName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";               

                string logDirectoryPath = ConfigurationManager.AppSettings["LogFileVishal"];

                string dateFolder = Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");

                string directoryPath = logDirectoryPath + "\\" + dateFolder;
                string errorLogFilename = @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                // errorLogFilename = dateFolder + @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                file = dateFolder + errorLogFilename;
                string filePath = directoryPath + errorLogFilename;

                if (Errors != null && Errors.Count > 0)
                {
                    using (StreamWriter stwriter = new StreamWriter(filePath, true))
                    {
                        stwriter.WriteLine("-------------------Error Log Start-----------as on " + DateTime.Now.ToString("hh:mm tt"));
                        stwriter.WriteLine("Excel Name :" + ExcelName);
                        for (int i = 0; i < Errors.Count; i++)
                        {
                            stwriter.WriteLine(Errors[i].ToString());
                        }
                        Errors = new List<string>();
                        stwriter.WriteLine("-------------------End----------------------------");
                    }
                }
                return file;

            }
            catch (Exception ex)
            {
                file = "";
                return file;
            }
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public FileResult DownloadErrorFile(string Name)
        {
            try
            {
                var path = ConfigurationManager.AppSettings["LogFileVishal"] + "\\" + Name;
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return File(stream, "application/text", "Error_" + Name);


            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public int GetActIdByName(string actName, int CustID, string CentralOrState)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int actId = (from row in entities.Vendor_ActMaster
                                 where row.ActName == actName && row.IsDeleted == false && row.IsDisabled != true && row.CustomerID == CustID && row.CentralORState == CentralOrState
                                 select row.ID).First();

                    return actId;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetActIdByName(string actName, int CustID, string CentralOrState, int? StateID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int actId = (from row in entities.Vendor_ActMaster
                                 where row.ActName == actName && row.IsDeleted == false && row.CustomerID == CustID && row.CentralORState == CentralOrState && row.StateID == StateID && row.IsDisabled != true
                                 select row.ID).First();

                    return actId;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        #endregion

        public FileResult DownloadSampleFilevishal(int ID)
        {
            // Find user by passed id
            //var student = GetCompliancebyID(ID);

            //var file = student.EmailAttachmentReceived.FirstOrDefault(x => x.LisaId == studentId);

            //byte[] fileBytes = System.IO.File.ReadAllBytes(file.Filepath);

            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Filename);
            byte[] fileBytes = null;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var filePathbyCompID = (from row in entities.Vendor_ComplainceMaster
                                            where row.IsDeleted == false && row.ID == ID
                                            select row).FirstOrDefault();

                    ComplainceDetails finalData = new ComplainceDetails();
                    if (filePathbyCompID != null)
                    {
                        ZipFile ComplianceZip = new ZipFile();
                        string filepathbyid = filePathbyCompID.FilePath;
                        filepathbyid = filepathbyid.Replace("~", "");
                        filepathbyid = filepathbyid.Replace("/", "\\");
                        string filePathnew = filepath + "\\" + filepathbyid;// + Path.GetExtension(filePathbyCompID.FileName));
                        if (filePathbyCompID.FilePath != null)
                        {
                            var stream = new FileStream(filePathnew, FileMode.Open, FileAccess.Read);
                            return File(stream, System.Net.Mime.MediaTypeNames.Application.Octet, filePathbyCompID.FileName);
                            //return File(stream, "application/text", filePathbyCompID.FileName);

                            //ComplianceZip.AddEntry(filePathbyCompID.FileName, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));

                            //var zipMs = new MemoryStream();
                            //ComplianceZip.Save(zipMs);
                            //zipMs.Position = 0;
                            //byte[] data = zipMs.ToArray();
                            //Response.Buffer = true;
                            //Response.ClearContent();
                            //Response.ClearHeaders();
                            //Response.Clear();
                            //Response.ContentType = "application/zip";
                            //Response.AddHeader("content-disposition", "attachment; filename=sampleFile.zip");
                            //Response.BinaryWrite(data);
                            //Response.Flush();
                            //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public ActionResult GetAllAssingTemplate(int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var assignTemplateList = (from row in entities.Vendor_AssignedTemplate
                                              where row.IsDeleted == false
                                              && row.CustomerID == CID
                                              select row).Distinct();
                    List<AssignedTemplateDTO> assignTemplateFinalList = new List<AssignedTemplateDTO>();

                    foreach (var item in assignTemplateList)
                    {
                        AssignedTemplateDTO finalData = new AssignedTemplateDTO();
                        finalData.ID = item.ID;
                        finalData.TemplateID = item.TemplateID;
                        finalData.TemplateName = GetTemplatebyID(item.TemplateID, CID);
                        finalData.ActID = item.ActID;
                        if (item.ActID != null)
                            finalData.ActName = GetActbyID(item.ActID);
                        finalData.ComplianceID = item.ComplianceID;
                        if (item.ComplianceID != null)
                            finalData.ComplianceName = GetCompliancebyID(item.ComplianceID);

                        finalData.IsDeleted = item.IsDeleted;
                        finalData.IsDisabled = item.IsDisabled;
                        finalData.DisabledDate = item.DisabledDate;
                        finalData.TemplateTye = item.TemplateTye;
                        assignTemplateFinalList.Add(finalData);
                    }

                    return Json(assignTemplateFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteAssignedTemplate(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_AssignedTemplate updateDetails = (from row in entities.Vendor_AssignedTemplate
                                                             where row.ID == id
                                                             && row.CustomerID == CustomerID
                                                             select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDeleted = true;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.CustomerID = CustomerID;
                        updateDetails.CreatedOn = DateTime.Today;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetUnAssignedCompliance(int CID, int TemplateID, string StatuOrIntenal)
        {
            try
            {
                List<Vendor_ComplainceMaster> complainceList = null;


                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    if (!string.IsNullOrEmpty(StatuOrIntenal))
                    {
                        complainceList = (from row in entities.Vendor_ComplainceMaster
                                          where row.IsDeleted == false && row.CustomerId == CID && row.Statutory == StatuOrIntenal
                                          select row).ToList();
                    }
                    else
                    {
                        complainceList = (from row in entities.Vendor_ComplainceMaster
                                          where row.IsDeleted == false && row.CustomerId == CID
                                          select row).ToList();


                    }

                    var assingCompliance = (from row in entities.Vendor_AssignedTemplate
                                            where row.CustomerID == CID && row.TemplateID == TemplateID
                                            select row).Distinct().ToList();



                    complainceList = complainceList.Where(x => x.IsDisabled == false || x.IsDisabled == null || x.IsDisabled == true && x.DisabledDate >= DateTime.Today).ToList();



                    List<ComplainceDetails> complainceFinalList = new List<ComplainceDetails>();

                    foreach (var item in complainceList)
                    {
                        ComplainceDetails finalData = new ComplainceDetails();
                        finalData.ID = item.ID;
                        finalData.Statutory = item.Statutory;
                        finalData.CentralOrState = item.CentralOrState;
                        finalData.StateID = item.StateID;

                        finalData.ActID = item.ActID;
                        if (item.ActID != null)
                            finalData.ActName = GetActbyID(item.ActID);
                        finalData.ComplainceHeader = item.ComplainceHeader;
                        finalData.IsDocument = item.IsDocument;
                        finalData.RiskCategory = item.RiskCategory;
                        finalData.IsDeleted = item.IsDeleted;
                        finalData.Category = item.Category;
                        finalData.DisabledDate = item.DisabledDate;
                        finalData.IsDisabled = item.IsDisabled;
                        finalData.FileName = item.FileName;
                        complainceFinalList.Add(finalData);
                    }

                    foreach (var item in complainceFinalList.ToList())
                    {
                        foreach (var item1 in assingCompliance)
                        {
                            if (item.ID == item1.ComplianceID && item1.IsDeleted == false)
                            {

                                complainceFinalList.Remove(item);
                            }
                        }
                    }

                    return Json(complainceFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
     
        public ActionResult DisableActnCompliance(int? UserId, int? CustomerID, int id, string disableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_AssignedTemplate updateDetails = (from row in entities.Vendor_AssignedTemplate
                                                             where row.ID == id
                                                             && row.CustomerID == CustomerID
                                                             select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDisabled = true;
                        updateDetails.DisabledDate = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.CustomerID = CustomerID;
                        updateDetails.CreatedOn = DateTime.Today;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteActnCompliance(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_AssignedTemplate updateDetails = (from row in entities.Vendor_AssignedTemplate
                                                             where row.ID == id
                                                             && row.CustomerID == CustomerID
                                                             select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDeleted = true;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.CustomerID = CustomerID;
                        updateDetails.CreatedOn = DateTime.Today;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }


        public ActionResult EnableActnCompliance(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_AssignedTemplate updateDetails = (from row in entities.Vendor_AssignedTemplate
                                                             where row.ID == id
                                                             && row.CustomerID == CustomerID
                                                             select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDisabled = false;
                        updateDetails.DisabledDate = null;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.CustomerID = CustomerID;
                        updateDetails.CreatedOn = DateTime.Today;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }


        public List<int> GetAllContractType(int ActID)
        {
            List<int> result = new List<int>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var contractTypeList = (from row in entities.Vendor_ActContractType
                                            where row.ActID == ActID
                                            && row.IsChecked == true
                                            select row.ContractType);

                    result = contractTypeList.ToList();
                }

            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public ActionResult GetAllActs(int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<string> contractorType = new List<string>();


                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_ActMaster
                                   where row.IsDeleted == false && row.CustomerID == CID
                                   select row);
                    List<ActMasterDTO> actFinalList = new List<ActMasterDTO>();

                    foreach (var item in actList)
                    {
                        ActMasterDTO finalData = new ActMasterDTO();
                        finalData.ID = item.ID;
                        finalData.ActName = item.ActName;
                        finalData.CentralORState = item.CentralORState;
                        finalData.ContractType = GetAllContractType(item.ID);
                        finalData.StateID = item.StateID;
                        if (item.StateID != null)
                            finalData.StateName = GetStateById(item.StateID);
                        
                        if (finalData.ContractType.Count > 0)
                        {
                            foreach (int newitem in finalData.ContractType)
                            {
                                contractorType.Add(GetContractById(newitem, CID));

                            }
                            finalData.ContractName = contractorType.ToList();
                        }
                        else
                        {
                            finalData.ContractName = null;
                        }
                        finalData.CustomerID = item.CustomerID;
                        finalData.Category = item.Category;
                        finalData.IsDeleted = item.IsDeleted;
                        finalData.IsDisabled = item.IsDisabled;
                        finalData.DisabledDate = item.DisabledDate;

                        if (item.IsDisabled == true)//&& item.DisabledDate < DateTime.Today && item.DisabledDate != null
                        {
                            finalData.Status = "Inactive";
                        }
                        else
                        {
                            finalData.Status = "Active";
                        }

                        actFinalList.Add(finalData);
                    }


                    return Json(actFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
      
        public ActionResult GetAllActsInDropdown(int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_ActMaster
                                   where row.IsDeleted == false
                                   && row.CustomerID == CID
                                   select row);

                    actList = actList.Where(x => x.IsDisabled == false || x.IsDisabled == null || x.IsDisabled == true && x.DisabledDate >= DateTime.Today);
                    List<ActMasterDTO> actFinalList = new List<ActMasterDTO>();

                    foreach (var item in actList)
                    {
                        ActMasterDTO finalData = new ActMasterDTO();
                        finalData.ID = item.ID;
                        finalData.ActName = item.ActName;
                        finalData.CentralORState = item.CentralORState;
                        if (item.CentralORState == "State")
                            finalData.StateID = item.StateID;
                        finalData.IsDeleted = item.IsDeleted;
                        actFinalList.Add(finalData);
                    }
                    actFinalList = actFinalList.OrderBy(x => x.ActName).ToList();
                    return Json(actFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }


        public string GetStateById(int? stateID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    string stateName = (from row in entities.States
                                        where row.ID == stateID && row.IsDeleted == false
                                        select row.Name).SingleOrDefault();

                    return stateName;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        public int GetStateByName(string State)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int stateID = (from row in entities.States
                                   where row.Name == State && row.IsDeleted == false
                                   select row.ID).Single();

                    return stateID;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetContractByName(string contractName, int CustID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int contractId = (from row in entities.Vendor_ContractType
                                      where row.Name == contractName && row.IsDeleted == false && row.CustomerID == CustID
                                      select row.ID).FirstOrDefault();

                    return contractId;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public string GetContractById(int? contractID, int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    string contractName = (from row in entities.Vendor_ContractType
                                           where row.ID == contractID && row.IsDeleted == false && row.CustomerID == CID
                                           select row.Name).FirstOrDefault();

                    return contractName;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        public string GetActbyID(int? actID)
        {
            string result = "";
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_ActMaster
                                   where row.ID == actID && row.IsDeleted == false
                                   select row);
                    List<ActMasterDTO> actFinalList = new List<ActMasterDTO>();

                    if (actList.Count() > 0)
                    {
                        foreach (var item in actList)
                        {
                            ActMasterDTO finalData = new ActMasterDTO();
                            finalData.ID = item.ID;
                            finalData.ActName = item.ActName;
                            actFinalList.Add(finalData);
                        }

                        result = actFinalList[0].ActName;
                    }
                    else
                    {
                        result = string.Empty;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public string GetTemplatebyID(int? templateID, int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var actList = (from row in entities.Vendor_TemplateMaster
                                   where row.ID == templateID && row.IsDeleted == false && row.CustomerID == CID
                                   select row);
                    List<TemplateMasterDTO> templateFinalList = new List<TemplateMasterDTO>();

                    foreach (var item in actList)
                    {
                        TemplateMasterDTO finalData = new TemplateMasterDTO();
                        finalData.ID = item.ID;
                        finalData.TemplateName = item.TemplateName;
                        templateFinalList.Add(finalData);
                    }

                    return templateFinalList[0].TemplateName;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public bool GetComplianceISdisablebyID(int? CompID)
        {
            bool result = false;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var compIDList = (from row in entities.Vendor_ComplainceMaster
                                      where row.IsDeleted == false && row.ID == CompID  //&& row.CustomerId == CID
                                      select row).FirstOrDefault();
                    if (compIDList != null && compIDList.IsDisabled == true)
                    {
                        result = true;
                    }
                    else
                        result = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
            return result;
        }
        public string GetCompliancebyID(int? CompID)
        {
            var result = "";
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var compIDList = (from row in entities.Vendor_ComplainceMaster
                                      where row.IsDeleted == false && row.ID == CompID  //&& row.CustomerId == CID
                                      select row);
                    List<ComplainceDetails> compFinalList = new List<ComplainceDetails>();

                    if (compIDList.Count() > 0)
                    {
                        foreach (var item in compIDList)
                        {
                            ComplainceDetails finalData = new ComplainceDetails();
                            finalData.ID = item.ID;
                            finalData.ComplainceHeader = item.ComplainceHeader;
                            compFinalList.Add(finalData);
                        }

                        result = compFinalList[0].ComplainceHeader;
                    }
                    else
                        result = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public ActionResult GetAllComplainces(int CID)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var complainceList = (from row in entities.Vendor_SP_ComplainceMaster(CID)
                                          select row).ToList();
                    return Json(complainceList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllComplaincesNew(int CID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var complainceList = (from row in entities.Vendor_ComplainceMaster
                                          where row.IsDeleted == false
                                          && row.CustomerId == CID
                                          && (row.IsDisabled == null || row.IsDisabled == false)
                                          select row);

                    List<ComplainceDetails> complainceFinalList = new List<ComplainceDetails>();

                    //complainceList = complainceList.Where(x => x.IsDisabled == false || x.IsDisabled == null || x.IsDisabled == true && x.DisabledDate <= DateTime.Today);

                    foreach (var item in complainceList)
                    {
                        ComplainceDetails finalData = new ComplainceDetails();
                        finalData.ID = item.ID;
                        finalData.Statutory = item.Statutory;
                        finalData.CentralOrState = item.CentralOrState;
                        finalData.StateID = item.StateID;

                        finalData.ActID = item.ActID;
                        if (item.ActID != null)
                            finalData.ActName = GetActbyID(item.ActID);
                        finalData.ComplainceHeader = item.ComplainceHeader;
                        finalData.IsDocument = item.IsDocument;
                        finalData.RiskCategory = item.RiskCategory;
                        finalData.IsDeleted = item.IsDeleted;
                        finalData.Category = item.Category;
                        finalData.DisabledDate = item.DisabledDate;
                        finalData.IsDisabled = item.IsDisabled;
                        finalData.FileName = item.FileName;
                        if (item.IsDisabled == true)//&& item.DisabledDate < DateTime.Today && item.DisabledDate != null
                        {
                            finalData.Status = "Inactive";
                        }
                        else
                        {
                            finalData.Status = "Active";
                        }
                        complainceFinalList.Add(finalData);
                    }

                    return Json(complainceFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
      
        public ActionResult GetParameterByCompliance(int ComplianceId, int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ParamterDTO> parameterFinalList = new List<ParamterDTO>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    entities.Configuration.LazyLoadingEnabled = false;
                    var complainceParameterList = (from row in entities.Vendor_CompMasterParameter
                                                   where row.ComplianceId == ComplianceId
                                                   && row.CustomerID == CID && row.IsDeleted == false
                                                   select row);

                    foreach (var itemParameter in complainceParameterList)
                    {
                        ParamterDTO finalParaData = new ParamterDTO();
                        finalParaData.ID = itemParameter.ID;
                        finalParaData.ComplianceId = itemParameter.ComplianceId;
                        finalParaData.ParameterName = itemParameter.ParameterName;
                        finalParaData.ParameterType = itemParameter.ParameterType;
                        parameterFinalList.Add(finalParaData);
                    }

                    //var finallist = JsonConvert.SerializeObject(userList);
                    return Json(parameterFinalList, JsonRequestBehavior.AllowGet);
                    //return JsonConvert.SerializeObject(ProjectList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
                
        public ActionResult SaveParamters(ParamterDTO[] paramterDTO)
        {
            var result = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int complainceID = 0;
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {

                        if (paramterDTO != null)
                        {
                            foreach (var item in paramterDTO)
                            {


                                Vendor_CompMasterParameter updateParameterDetails = (from row in entities.Vendor_CompMasterParameter
                                                                                     where row.ID == item.ID
                                                                                     select row).FirstOrDefault();
                                if (updateParameterDetails != null)
                                {
                                    //if (updateParameterDetails.ParameterName != item.ParameterName)
                                    //{
                                    //    if (CheckParameterNameExist(item.ParameterName, item.ComplianceId) == false)
                                    //        result = "Exist";
                                    //}
                                    //if (result != "Exist")
                                    //{
                                    updateParameterDetails.ID = item.ID;
                                    updateParameterDetails.ComplianceId = item.ComplianceId;
                                    updateParameterDetails.ParameterName = item.ParameterName;
                                    updateParameterDetails.ParameterType = item.ParameterType;

                                    entities.SaveChanges();
                                    result = "Success";
                                    //}
                                }
                                else
                                {
                                    //if (CheckParameterNameExist(item.ParameterName,item.ComplianceId))
                                    //{
                                    Vendor_CompMasterParameter CompMasterParameter = new Vendor_CompMasterParameter()
                                    {
                                        ID = 0,
                                        ComplianceId = item.ComplianceId,
                                        ParameterName = item.ParameterName,
                                        ParameterType = item.ParameterType,
                                        CustomerID = item.CustomerID,
                                        CreatedBy = item.UserID,
                                        IsDeleted = false,
                                        CreatedOn = DateTime.Today
                                    };
                                    entities.Vendor_CompMasterParameter.Add(CompMasterParameter);
                                    entities.SaveChanges();
                                    result = "Success";
                                    //}
                                    //else
                                    //{
                                    //    result = "Exist";
                                    //}
                                }

                            }
                        }

                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public bool CheckParameterNameExist(string ParameterName, long CompId)
        {
            bool result = true;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    var checkParameterName = (from row in entities.Vendor_CompMasterParameter
                                              where row.ComplianceId == CompId
                                              select row).ToList();
                    bool containsItem = checkParameterName.Any(item => item.ParameterName == ParameterName);
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        [System.Web.Http.HttpPost]
        public ActionResult SaveComplianceDetails()
        {
            var finalResult = "";

            string FilePath = string.Empty;
            string dbFilePath = string.Empty;
            string FileLocationPath = string.Empty;

            string ComplainceHeader = Request.Form["ComplainceHeader"];
            string ComplianceDescription = Request.Form["ComplianceDescription"];
            string DetailedDesc = Request.Form["DetailedDesc"];
            string Section = Request.Form["Section"];
            int id = Convert.ToInt32(Request.Form["ID"]);
            int UserID = Convert.ToInt32(Request.Form["uid"]);
            int CustID = Convert.ToInt32(Request.Form["CId"]);
            string Statutory = Request.Form["Statutory"];
            string CentralOrState = Request.Form["CentralOrState"];
            int? StateID = Convert.ToInt32(Request.Form["StateID"]);
            int? ActID = Convert.ToInt32(Request.Form["ActID"]);
            string RiskCategory = Request.Form["RiskCategory"];
            bool? IsDocument = Convert.ToBoolean(Request.Form["IsDocument"]);

            string Consequences = Request.Form["Consequences"];
            string Mitigation = Request.Form["Mitigation"];

            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase SampleFile = null;
            if (files.Count > 0)
            {
                SampleFile = files[0];


                if (SampleFile.ContentLength > 0)
                {
                    string filepath = ConfigurationManager.AppSettings["ComplianceDocument"];

                    string storefolder = "ComplianceDocument";
                    string path = @"" + filepath + "\\" + storefolder + "\\" + UserID + "\\" + CustID + "\\";

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    string fileName = Path.GetFileName(SampleFile.FileName);
                    SampleFile.SaveAs(path + fileName);
                    FilePath = storefolder + "/" + UserID + "/" + CustID + "/" + fileName;

                    dbFilePath = "~/" + FilePath;
                }
            }
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                if (true)
                {

                    //int id = Convert.ToInt32(ID);
                    int complainceID = 0;
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {

                        Vendor_ComplainceMaster updateDetails = (from row in entities.Vendor_ComplainceMaster
                                                                 where row.ID == id
                                                                 select row).FirstOrDefault();
                        if (updateDetails != null)
                        {
                            if (updateDetails.ComplainceHeader != ComplainceHeader)
                            {
                                if (CheckComplianceNameExist(ComplainceHeader, CustID, StateID) == false)
                                    finalResult = "Exist";

                            }
                            if (finalResult != "Exist")
                            {
                                updateDetails.ID = id;
                                updateDetails.Statutory = Statutory;
                                updateDetails.CentralOrState = CentralOrState;
                                updateDetails.StateID = StateID;
                                updateDetails.ActID = ActID;
                                if (Statutory == "Statutory")
                                {
                                    updateDetails.ActID = ActID;
                                    updateDetails.Category = GetCategoryByActId(ActID).ToString();
                                }
                                else
                                {
                                    updateDetails.ActID = null;
                                }
                                updateDetails.Consequences = Consequences;
                                updateDetails.Mitigation = Mitigation;
                                updateDetails.ComplainceHeader = ComplainceHeader;
                                updateDetails.ComplianceDescription = ComplianceDescription;
                                updateDetails.DetailedDesc = DetailedDesc;
                                updateDetails.Section = Section;
                                updateDetails.IsDocument = IsDocument;
                                updateDetails.RiskCategory = RiskCategory;
                                updateDetails.CustomerId = CustID;
                                updateDetails.CreatedBy = UserID;
                                updateDetails.IsDeleted = false;
                                updateDetails.CreatedOn = DateTime.Today;
                                updateDetails.UpdatedBy = UserID;
                                updateDetails.UpdatedOn = DateTime.Today;
                                updateDetails.FileName = SampleFile == null ? null : SampleFile.FileName;
                                updateDetails.FileSize = SampleFile == null ? 0 : SampleFile.ContentLength;
                                updateDetails.Version = "2.0";
                                updateDetails.FilePath = dbFilePath;
                                entities.SaveChanges();
                                finalResult = "Success";
                                complainceID = updateDetails.ID;
                            }
                        }
                        else
                        {
                            #region add new detail
                            if (CheckComplianceNameExist(ComplainceHeader, CustID, StateID))
                            {
                                int ACTIDD = -1;
                                string sampleCategory = string.Empty;
                                if (ActID != null)
                                {
                                    ACTIDD = (int)ActID;
                                    if (ACTIDD == 0 || ACTIDD == null)
                                    {
                                        sampleCategory = "";
                                    }
                                    else
                                        sampleCategory = GetCategoryByActId(ACTIDD).ToString();
                                }

                                Vendor_ComplainceMaster ComplianceMaster = new Vendor_ComplainceMaster()
                                {
                                    ID = 0,
                                    Statutory = Statutory,
                                    CentralOrState = CentralOrState,
                                    StateID = StateID,
                                    ActID = ACTIDD,
                                    Category = sampleCategory,
                                    ComplainceHeader = ComplainceHeader,
                                    IsDocument = IsDocument,
                                    RiskCategory = RiskCategory,
                                    CustomerId = CustID,
                                    CreatedBy = UserID,
                                    IsDeleted = false,
                                    CreatedOn = DateTime.Today,
                                    UpdatedBy = UserID,
                                    UpdatedOn = DateTime.Today,
                                    FileName = SampleFile == null ? null : SampleFile.FileName,
                                    FileSize = SampleFile == null ? 0 : SampleFile.ContentLength,
                                    Version = "1.0",
                                    FilePath = dbFilePath,
                                    IsDisabled = false,
                                    Consequences = Consequences,
                                    Mitigation = Mitigation,
                                    ComplianceDescription = ComplianceDescription,
                                    DetailedDesc = DetailedDesc,
                                    Section = Section

                                };
                                entities.Vendor_ComplainceMaster.Add(ComplianceMaster);
                                entities.SaveChanges();
                                finalResult = "Success";
                                complainceID = ComplianceMaster.ID;
                            }
                            else
                            {
                                finalResult = "Exist";
                            }
                            #endregion
                        }
                        var result = new { Result = finalResult, complainceID };

                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public bool CheckComplianceNameExist(string ComplianceName, int CustID, int? StateID)
        {
            bool result = true;

            var checkComplianceName = new List<Vendor_ComplainceMaster>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    if (StateID != null && StateID != 0)
                    {

                        checkComplianceName = (from row in entities.Vendor_ComplainceMaster
                                               where row.CustomerId == CustID && row.StateID == StateID
                                               select row).ToList();
                    }
                    else
                    {

                        checkComplianceName = (from row in entities.Vendor_ComplainceMaster
                                               where row.CustomerId == CustID
                                               select row).ToList();
                    }
                    bool containsItem = checkComplianceName.Any(item => item.ComplainceHeader.Trim().ToLower() == ComplianceName.Trim().ToLower());
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public bool CheckComplianceNameExist(string ComplianceName, int CustID)
        {
            bool result = true;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    var checkComplianceName = (from row in entities.Vendor_ComplainceMaster
                                               where row.CustomerId == CustID
                                               select row).ToList();
                    bool containsItem = checkComplianceName.Any(item => item.ComplainceHeader.Trim().ToLower() == ComplianceName.Trim().ToLower());
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public string GetCategoryByActId(int? ActID)
        {
            string result = string.Empty;
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    entities.Configuration.LazyLoadingEnabled = false;
                    string ActCategory = (from row in entities.Vendor_ActMaster
                                          where row.ID == ActID
                                          select row.Category).FirstOrDefault();
                    result = ActCategory;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public ActionResult SaveTemplateDetails(TemplateMasterDTO DetailsObj)
        {
            //List<AssignedTemplateDTO> assignedTemplateDTO = new List<AssignedTemplateDTO>();

            //assignedTemplateDTO = DetailsObj.assignedTemplateDTO;

            DetailsObj.assignedTemplateDTO = DetailsObj.assignedTemplateDTO.GroupBy(y => y.ComplianceID).Select(g => g.First()).ToList();

            var result = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = Convert.ToInt32(DetailsObj.UserId);
                    int CustID = Convert.ToInt32(DetailsObj.CID);
                    int id = Convert.ToInt32(DetailsObj.ID);
                    int templateID = 0;
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        Vendor_TemplateMaster updateDetails = (from row in entities.Vendor_TemplateMaster
                                                               where row.ID == id
                                                               select row).FirstOrDefault();

                        if (updateDetails != null)
                        {
                            if (updateDetails.TemplateName != DetailsObj.TemplateName)
                            {
                                if (CheckTemplateNameExist(DetailsObj.TemplateName, CustID) == false)
                                    result = "Exist";
                            }
                            if (result != "Exist")
                            {
                                updateDetails.TemplateName = DetailsObj.TemplateName;
                                updateDetails.TemplateTye = DetailsObj.TemplateTye;
                                updateDetails.CustomerID = CustID;
                                updateDetails.CentralOrState = DetailsObj.CentralOrState;
                                updateDetails.CategoryID = DetailsObj.CategoryID;
                                updateDetails.ActID = DetailsObj.ActID;
                                updateDetails.CreatedBy = UserID;
                                updateDetails.CreatedOn = DateTime.Today;
                                updateDetails.UpdatedBy = UserID;
                                updateDetails.UpdatedOn = DateTime.Today;
                                updateDetails.IsDeleted = false;
                                entities.SaveChanges();
                                templateID = updateDetails.ID;
                                result = "Success";
                            }
                        }
                        else
                        {
                            #region add new detail
                            if (CheckTemplateNameExist(DetailsObj.TemplateName, CustID))
                            {
                                Vendor_TemplateMaster TemplateMaster = new Vendor_TemplateMaster()
                                {
                                    ID = id,
                                    TemplateName = DetailsObj.TemplateName,
                                    TemplateTye = DetailsObj.TemplateTye,
                                    CustomerID = CustID,
                                    CentralOrState = DetailsObj.CentralOrState,
                                    CategoryID = DetailsObj.CategoryID,
                                    ActID = DetailsObj.ActID,
                                    CreatedBy = UserID,
                                    CreatedOn = DateTime.Today,
                                    UpdatedBy = UserID,
                                    UpdatedOn = DateTime.Today,
                                    IsDeleted = false

                                };
                                entities.Vendor_TemplateMaster.Add(TemplateMaster);
                                entities.SaveChanges();
                                templateID = TemplateMaster.ID;
                                result = "Success" + templateID;
                            }
                            else
                            {
                                result = "Exist";
                            }

                            #endregion
                        }
                        if (DetailsObj.assignedTemplateDTO != null && templateID > 0)
                        {
                            foreach (var item in DetailsObj.assignedTemplateDTO)
                            {

                                Vendor_AssignedTemplate updateAssignDetails = (from row in entities.Vendor_AssignedTemplate
                                                                               where row.ID == item.ID
                                                                               select row).FirstOrDefault();
                                if (updateAssignDetails != null)
                                {
                                    updateAssignDetails.ID = item.ID;
                                    updateAssignDetails.TemplateID = templateID;
                                    updateAssignDetails.ActID = DetailsObj.TemplateTye == "Internal" ? null : item.ActID;
                                    updateAssignDetails.ComplianceID = item.ComplianceID;
                                    updateAssignDetails.CustomerID = CustID;
                                    updateAssignDetails.CreatedBy = UserID;
                                    updateAssignDetails.CreatedOn = DateTime.Today;
                                    updateAssignDetails.UpdatedBy = UserID;
                                    updateAssignDetails.UpdatedOn = DateTime.Today;
                                    updateAssignDetails.IsDeleted = false;
                                    updateAssignDetails.TemplateTye = DetailsObj.TemplateTye;
                                    entities.SaveChanges();

                                }
                                else
                                {
                                    Vendor_AssignedTemplate AssignedTemplateMaster = new Vendor_AssignedTemplate()
                                    {
                                        ID = 0,
                                        TemplateID = templateID,
                                        ActID = DetailsObj.TemplateTye == "Internal" ? null : item.ActID,
                                        ComplianceID = item.ComplianceID,
                                        CustomerID = CustID,
                                        CreatedBy = UserID,
                                        CreatedOn = DateTime.Today,
                                        UpdatedBy = UserID,
                                        UpdatedOn = DateTime.Today,
                                        IsDeleted = false,
                                        TemplateTye = DetailsObj.TemplateTye

                                    };
                                    entities.Vendor_AssignedTemplate.Add(AssignedTemplateMaster);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteComplianceParam(int ID, int CompID, int UserID, int CustID)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_CompMasterParameter updateDetails = (from row in entities.Vendor_CompMasterParameter
                                                                where row.ID == ID
                                                                && row.CustomerID == CustID && row.ComplianceId == CompID
                                                                select row).FirstOrDefault();

                    if (updateDetails != null)
                    {

                        updateDetails.IsDeleted = true;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
      
        public bool CheckTemplateNameExist(string TemplateName, int CustID)
        {
            bool result = true;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    var checkTemplateName = (from row in entities.Vendor_TemplateMaster
                                             where row.CustomerID == CustID
                                             select row).ToList();
                    bool containsItem = checkTemplateName.Any(item => item.TemplateName.Trim().ToLower() == TemplateName.Trim().ToLower());
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public ActionResult SaveActDetails(int ID, int UID, int CustomerID, string ContractType, int? StateID, string ActName, string CentralORState, string Category)//(ActMasterDTO DetailsObj)
        {

            var result = "";
            System.Collections.Generic.IList<int> ContractTypeIDs = null;

            if (ContractType != null)
            {
                ContractTypeIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(ContractType);
            }


            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = Convert.ToInt32(UID);//AuthenticationHelper.UserID
                    int CustID = Convert.ToInt32(CustomerID);//AuthenticationHelper.CustomerID
                    int id = Convert.ToInt32(ID);

                    List<Vendor_ActContractType> ActContractTypeList = new List<Vendor_ActContractType>();
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        Vendor_ActMaster updateDetails = (from row in entities.Vendor_ActMaster
                                                          where row.ID == id
                                                          select row).FirstOrDefault();




                        if (updateDetails != null)
                        {

                            var updateDetailsContractType = (from row in entities.Vendor_ActContractType
                                                             where row.ActID == updateDetails.ID
                                                             select row);



                            var oldContractType = new List<int>();

                            foreach (var item in updateDetailsContractType)
                            {
                                oldContractType.Add(item.ContractType);
                            }

                            var NewContractTypeIDs = ContractTypeIDs.Except(oldContractType);
                            var UpdateContractTypeIDs = ContractTypeIDs.Except(oldContractType);

                            if (NewContractTypeIDs.Count() > 0)
                            {
                                foreach (var itemID in NewContractTypeIDs)
                                {
                                    Vendor_ActContractType ActContractType = new Vendor_ActContractType()
                                    {
                                        ActID = updateDetails.ID,
                                        ContractType = Convert.ToInt32(itemID),
                                        CustID = CustomerID,
                                        CreatedBy = UID,
                                        CreatedOn = DateTime.Today,
                                        UpdatedOn = DateTime.Today,
                                        UpdatedBy = UID,
                                        IsChecked = true,
                                    };
                                    entities.Vendor_ActContractType.Add(ActContractType);
                                    entities.SaveChanges();
                                }
                            }

                            foreach (var itemnew in updateDetailsContractType)
                            {
                                if (ContractTypeIDs.Contains(itemnew.ContractType))
                                {
                                    foreach (var item in ContractTypeIDs)
                                    {
                                        if (item == itemnew.ContractType && itemnew.ID != 0)
                                        {
                                            itemnew.IsChecked = true;
                                            itemnew.UpdatedOn = DateTime.Today;
                                            itemnew.UpdatedBy = UID;
                                        }
                                    }
                                }
                                else
                                {
                                    itemnew.IsChecked = false;
                                    itemnew.UpdatedOn = DateTime.Today;
                                    itemnew.UpdatedBy = UID;
                                }
                            }
                            entities.SaveChanges();


                            //updateDetailsContractType = updateDetailsContractType.Where(x=>x.ID>0 && x.ContractType == )
                            //foreach (var item in updateDetailsContractType)
                            //{

                            //}


                            //if (UpdateContractTypeIDs.Count() > 0)
                            //{
                            //    foreach (var itemID in updateDetailsContractType)
                            //    {
                            //        if (UpdateContractTypeIDs == itemID.ContractType)
                            //        {
                            //            itemID.IsChecked = false;
                            //            itemID.UpdatedOn = DateTime.Today;
                            //            itemID.UpdatedBy = UID;
                            //            entities.SaveChanges();
                            //        }
                            //    }
                            //}

                            //foreach (var item in ContractTypeIDs)
                            //{
                            //    updateDetailsContractType = updateDetailsContractType.Where(x => x.ContractType != item);

                            //    if (updateDetailsContractType.Count() > 0)
                            //    {
                            //        foreach (var itemContractID in updateDetailsContractType)
                            //        {

                            //            itemContractID.IsChecked = false;
                            //            itemContractID.UpdatedOn = DateTime.Today;
                            //            itemContractID.UpdatedBy = UID;
                            //            entities.SaveChanges();
                            //        }
                            //    }
                            //}


                            //foreach (var itemnew in updateDetailsContractType)
                            //{
                            //    if (item != itemnew.ContractType && itemnew.ID == 0)
                            //    {
                            //        Vendor_ActContractType ActContractType = new Vendor_ActContractType()
                            //        {
                            //            ActID = itemnew.ActID,
                            //            ContractType = Convert.ToInt32(item),
                            //            CustID = CustomerID,
                            //            CreatedBy = UID,
                            //            CreatedOn = DateTime.Today,
                            //            UpdatedOn = DateTime.Today,
                            //            UpdatedBy = UID,
                            //            IsChecked = true,
                            //        };
                            //        entities.Vendor_ActContractType.Add(ActContractType);
                            //        entities.SaveChanges();
                            //    }

                            //    else if (item != itemnew.ContractType && itemnew.ID != 0)
                            //    {
                            //        itemnew.IsChecked = false;
                            //        itemnew.UpdatedOn = DateTime.Today;
                            //        itemnew.UpdatedBy = UID;
                            //    }
                            //    else
                            //    {

                            //    }
                            //}
                            //}

                            if (updateDetails.ActName != ActName)
                            {
                                if (CheckActNameExist(ActName, CustID, StateID) == false)
                                    result = "Exist";
                            }
                            if (result != "Exist")
                            {
                                updateDetails.ActName = ActName;
                                updateDetails.CustomerID = CustID;
                                updateDetails.CentralORState = CentralORState;
                                updateDetails.StateID = StateID;
                                updateDetails.Category = Category;
                                //updateDetails.ContractType = ContractType;
                                updateDetails.CreatedBy = UserID;
                                updateDetails.CreatedOn = DateTime.Today;
                                updateDetails.UpdatedBy = UserID;
                                updateDetails.UpdatedOn = DateTime.Today;
                                entities.SaveChanges();
                                result = "Success";
                            }

                        }
                        else
                        {
                            #region add new detail
                            if (CheckActNameExist(ActName, CustID, StateID))
                            {

                                Vendor_ActMaster ActMaster = new Vendor_ActMaster()
                                {
                                    ID = id,
                                    ActName = ActName,
                                    CustomerID = CustID,
                                    CentralORState = CentralORState,
                                    StateID = StateID,
                                    //ContractType = ContractType,
                                    Category = Category,
                                    CreatedBy = UserID,
                                    CreatedOn = DateTime.Today,
                                    UpdatedBy = UserID,
                                    UpdatedOn = DateTime.Today,

                                    IsDeleted = false

                                };
                                entities.Vendor_ActMaster.Add(ActMaster);
                                entities.SaveChanges();
                                int ActID = ActMaster.ID;

                                if (ActID > 0 && ContractTypeIDs.Count > 0)
                                {
                                    foreach (var item in ContractTypeIDs)
                                    {
                                        Vendor_ActContractType ActContractType = new Vendor_ActContractType()
                                        {
                                            ActID = ActID,
                                            ContractType = Convert.ToInt32(item),
                                            CustID = CustomerID,
                                            CreatedBy = UID,
                                            CreatedOn = DateTime.Today,
                                            UpdatedOn = DateTime.Today,
                                            UpdatedBy = UID,
                                            IsChecked = true

                                        };
                                        entities.Vendor_ActContractType.Add(ActContractType);
                                        entities.SaveChanges();
                                    }

                                }
                                result = "Success";
                            }
                            else
                            {
                                result = "Exist";
                            }
                            #endregion
                        }
                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public bool CheckActNameExist(string ActName, int CustID, int? stateID)
        {
            bool result = true;
            var checkActName = new List<Vendor_ActMaster>();


            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    if (stateID != null)
                    {
                        checkActName = (from row in entities.Vendor_ActMaster
                                        where row.CustomerID == CustID && row.StateID == stateID
                                        select row).ToList();
                    }
                    else
                    {
                        checkActName = (from row in entities.Vendor_ActMaster
                                        where row.CustomerID == CustID
                                        select row).ToList();
                    }

                    bool containsItem = checkActName.Any(item => item.ActName.Trim().ToLower() == ActName.Trim().ToLower());
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public ActionResult DisableAct(int? UserId, int? CustomerID, int id, string disableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_ActMaster updateDetails = (from row in entities.Vendor_ActMaster
                                                      where row.ID == id
                                                      && row.CustomerID == CustomerID
                                                      select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        if (DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                        {
                            updateDetails.IsDisabled = true;
                        }
                        else
                        {
                            updateDetails.IsDisabled = false;
                        }
                        //updateDetails.IsDisabled = true;
                        updateDetails.DisabledDate = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.startdate = null;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;
                        
                        var getallList = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                    select row).ToList();

                        var getListcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                              select row).ToList();

                        DateTime resultdt = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime D1 = resultdt.AddMonths(1);
                        DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                        var updatecompDetails = (from row in entities.Vendor_ComplainceMaster
                                                 where row.ActID == id
                                          && row.CustomerId == CustomerID
                                                 select row).ToList();

                        foreach (var item in updatecompDetails)
                        {
                            Vendor_ComplainceMaster updatecDetails = (from row in entities.Vendor_ComplainceMaster
                                                                     where row.ID == item.ID
                                                                     && row.CustomerId == CustomerID
                                                                     select row).FirstOrDefault();


                            if (updatecDetails != null)
                            {
                                if (DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                                {
                                    updatecDetails.IsDisabled = true;
                                }
                                else
                                {
                                    updatecDetails.IsDisabled = false;
                                }                                
                                updatecDetails.DisabledDate = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                updatecDetails.UpdatedBy = UserId;
                                updatecDetails.startdate = null;
                                updatecDetails.UpdatedOn = DateTime.Today;
                                entities.SaveChanges();
                                result = true;
                                
                                #region for disable
                                
                                var List = (from row in getallList
                                            select row).ToList();

                                var Listcompliance = (from row in getListcompliance
                                                      where row.ComplianceID == item.ID
                                                      select row).ToList();
                                
                                List = List.Where(x => x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                                foreach (var item1 in List)
                                {
                                    int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item1.ScheduleOnID
                                         && x.ContractProjMappingID == item1.ContractorProjectMappingID
                                         && x.ComplianceStatusID != null
                                         && x.ComplianceID == item.ID).ToList().Count;

                                    if (cnt > 1)
                                    {

                                    }
                                    else
                                    {
                                        Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                         where row.ContractProjMappingID == item1.ContractorProjectMappingID
                                                                                        && row.ContractProjMappingScheduleOnID == item1.ScheduleOnID
                                                                                        && row.ComplianceID == item.ID
                                                                                         select row).FirstOrDefault();
                                        if (scheobj != null)
                                        {
                                            scheobj.IsActive = false;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteAct(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_ActMaster updateDetails = (from row in entities.Vendor_ActMaster
                                                      where row.ID == id
                                                      && row.CustomerID == CustomerID
                                                      select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDeleted = true;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
        
        public ActionResult EnableAct(int? UserId, int? CustomerID, int id,string enableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_ActMaster updateDetails = (from row in entities.Vendor_ActMaster
                                                      where row.ID == id
                                                      && row.CustomerID == CustomerID
                                                      select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;

                        DateTime resultdt1 = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        if (Convert.ToDateTime(resultdt1) < DateTime.Now)
                        {
                            updateDetails.IsDisabled = false;
                        }
                        else
                        {
                            updateDetails.IsDisabled = true;
                        }

                        updateDetails.startdate = resultdt1;
                        //updateDetails.IsDisabled = false;
                        updateDetails.DisabledDate = null;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;


                        var getallList = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                          select row).ToList();

                        var getListcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                                 select row).ToList();

                        DateTime resultdt = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime D1 = resultdt;
                        //DateTime D1 = resultdt.AddMonths(1);
                        DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);


                        var updatecompDetails = (from row in entities.Vendor_ComplainceMaster
                                                                 where row.ActID == id
                                                                 && row.CustomerId == CustomerID
                                                                 select row).ToList();
                        foreach (var item in updatecompDetails)
                        {
                            Vendor_ComplainceMaster updateDetails1 = (from row in entities.Vendor_ComplainceMaster
                                                                     where row.ID == item.ID
                                                                     && row.CustomerId == CustomerID
                                                                     select row).FirstOrDefault();

                            if (updateDetails1 != null)
                            {
                                updateDetails1.ID = item.ID;
                                updateDetails1.IsDisabled = false;
                                updateDetails1.DisabledDate = null;
                                updateDetails1.startdate = resultdt1;
                                updateDetails1.UpdatedBy = UserId;
                                updateDetails1.UpdatedOn = DateTime.Today;
                                entities.SaveChanges();
                                result = true;
                                
                                #region for Enable

                                var List = (from row in getallList
                                            select row).ToList();

                                var Listcompliance = (from row in getListcompliance
                                                      where row.ComplianceID == item.ID
                                                      select row).ToList();

                                List = List.Where(x => x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                                foreach (var item1 in List)
                                {
                                    int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item1.ScheduleOnID
                                         && x.ContractProjMappingID == item1.ContractorProjectMappingID
                                         && x.ComplianceStatusID != null
                                         && x.ComplianceID == item.ID).ToList().Count;

                                    if (cnt > 1)
                                    {

                                    }
                                    else
                                    {
                                        Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                         where row.ContractProjMappingID == item1.ContractorProjectMappingID
                                                                                        && row.ContractProjMappingScheduleOnID == item1.ScheduleOnID
                                                                                        && row.ComplianceID == item.ID
                                                                                         select row).FirstOrDefault();
                                        if (scheobj != null)
                                        {
                                            scheobj.IsActive = true;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                            }
                        }                        
                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DisableCompliance(int? UserId, int? CustomerID, int id, string disableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_ComplainceMaster updateDetails = (from row in entities.Vendor_ComplainceMaster
                                                             where row.ID == id
                                                             && row.CustomerId == CustomerID
                                                             select row).FirstOrDefault();


                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        if (DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                        {
                            updateDetails.IsDisabled = true;
                        }
                        else
                        {
                            updateDetails.IsDisabled = false;
                        }

                        //obj.ClosureDate = Convert.ToDateTime(ClouserDate);
                        //updateDetails.IsDisabled = true;
                        updateDetails.startdate = null;
                        updateDetails.DisabledDate = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();

                        #region for disable

                        DateTime resultdt = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                    select row).ToList();

                        var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                              where row.ComplianceID == id
                                              select row).ToList();

                        DateTime D1 = resultdt.AddMonths(1);                     
                        DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                        List = List.Where(x => x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                        foreach (var item in List)
                        {
                            int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                 && x.ContractProjMappingID == item.ContractorProjectMappingID
                                 && x.ComplianceStatusID != null
                                 && x.ComplianceID == id).ToList().Count;

                            if (cnt > 1)
                            {

                            }
                            else
                            {
                                Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                 where row.ContractProjMappingID == item.ContractorProjectMappingID
                                                                                && row.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                                                                && row.ComplianceID == id
                                                                                 select row).FirstOrDefault();
                                if (scheobj != null)
                                {
                                    scheobj.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion

                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteCompliance(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_ComplainceMaster updateDetails = (from row in entities.Vendor_ComplainceMaster
                                                             where row.ID == id
                                                             && row.CustomerId == CustomerID
                                                             select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDeleted = true;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult EnableCompliance(int? UserId, int? CustomerID, int id, string enableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    
                    Vendor_ComplainceMaster updateDetails1 = (from row in entities.Vendor_ComplainceMaster
                                                             join row1 in entities.Vendor_ActMaster
                                                             on row.ActID equals row1.ID
                                                             where row.ID == id
                                                             && row.CustomerId == CustomerID
                                                             && row1.IsDisabled == false || row1.IsDisabled == null
                                                             select row).FirstOrDefault();

                    if (updateDetails1 != null)
                    {
                        Vendor_ComplainceMaster updateDetails = (from row in entities.Vendor_ComplainceMaster                                                                
                                                                 where row.ID == id
                                                                 && row.CustomerId == CustomerID
                                                                 select row).FirstOrDefault();

                        if (updateDetails != null)
                        {
                            updateDetails.ID = id;
                            if (DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                            {
                                updateDetails.IsDisabled = false;
                            }
                            else
                            {
                                updateDetails.IsDisabled = true;
                            }
                            updateDetails.startdate = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            //updateDetails.IsDisabled = false;
                            updateDetails.DisabledDate = null;
                            updateDetails.UpdatedBy = UserId;
                            updateDetails.UpdatedOn = DateTime.Today;
                            entities.SaveChanges();

                            #region for enable

                            DateTime resultdt = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                        select row).ToList();

                            var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                                  where row.ComplianceID == id
                                                  select row).ToList();

                            DateTime D1 = resultdt;
                            //DateTime D1 = resultdt.AddMonths(1);
                            DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                            List = List.Where(x => x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                            foreach (var item in List)
                            {
                                int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                     && x.ContractProjMappingID == item.ContractorProjectMappingID
                                     && x.ComplianceStatusID != null
                                     && x.ComplianceID == id).ToList().Count;

                                if (cnt > 1)
                                {

                                }
                                else
                                {
                                    Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                     where row.ContractProjMappingID == item.ContractorProjectMappingID
                                                                                    && row.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                                                                    && row.ComplianceID == id
                                                                                     select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DisableTemplate(int? UserId, int? CustomerID, int id, string disableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_TemplateMaster updateDetails = (from row in entities.Vendor_TemplateMaster
                                                           where row.ID == id
                                                           && row.CustomerID == CustomerID
                                                           select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        if (DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                        {
                            updateDetails.IsDisabled = true;
                        }
                        else
                        {
                            updateDetails.IsDisabled = false;
                        }
                        //updateDetails.IsDisabled = true;
                        updateDetails.DisabledDate = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();

                        #region disable

                        DateTime resultdt = DateTime.ParseExact(disableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        DateTime D1 = resultdt.AddMonths(1);
                        DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                        List<int> finalcomplianceDetails = (from row in entities.Vendor_AssignedTemplate
                                                            where row.TemplateID == id
                                                               && row.CustomerID == CustomerID
                                                            select (int)row.ComplianceID).ToList();


                        var getallList = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                          select row).ToList();

                        var MappingDetails = (from row in entities.Vendor_ComplianceMapping
                                              where row.TemplateID == id
                                                               && row.CustID == CustomerID
                                              select row).ToList();

                        var ContractorProjectMappingIDs = MappingDetails.Select(x => x.ContractorProjectMappingID).ToList();

                        if (ContractorProjectMappingIDs.Count > 0 && finalcomplianceDetails.Count > 0)
                        {
                            getallList = getallList.Where(x => x.AuditCheckListStatusID == 1
                                            && x.ScheduleOn >= Datevalue
                                        && ContractorProjectMappingIDs.Contains((int)x.ContractorProjectMappingID)).ToList();

                            var getListcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustomerID, -1, -1)
                                                     select row).ToList();

                            foreach (var item in getallList)
                            {
                                var Listcompliance = (from row in getListcompliance
                                                      where row.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                                      && row.ContractProjMappingID == item.ContractorProjectMappingID
                                                      && finalcomplianceDetails.Contains((int)row.ComplianceID)
                                                       && row.ComplianceStatusID == null
                                                      select row).ToList();
                                foreach (var item1 in Listcompliance)
                                {

                                    Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                     where row.ContractProjMappingID == item1.ContractProjMappingID
                                                                                    && row.ContractProjMappingScheduleOnID == item1.ContractProjMappingScheduleOnID
                                                                                    && row.ComplianceID == item1.ID
                                                                                     select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = false;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }                        
                        
                        #endregion
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult DeleteTemplate(int? UserId, int? CustomerID, int id)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_TemplateMaster updateDetails = (from row in entities.Vendor_TemplateMaster
                                                           where row.ID == id
                                                           && row.CustomerID == CustomerID
                                                           select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        updateDetails.IsDeleted = true;
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult EnableTemplate(int? UserId, int? CustomerID, int id, string enableDate)
        {
            try
            {
                bool result = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;


                    Vendor_TemplateMaster updateDetails = (from row in entities.Vendor_TemplateMaster
                                                           where row.ID == id
                                                           && row.CustomerID == CustomerID
                                                           select row).FirstOrDefault();

                    if (updateDetails != null)
                    {
                        updateDetails.ID = id;
                        if (DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) < DateTime.Now)
                        {
                            updateDetails.IsDisabled = false;
                        }
                        else
                        {
                            updateDetails.IsDisabled = true;
                        }
                        updateDetails.startdate = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        //updateDetails.IsDisabled = false;
                        updateDetails.DisabledDate = null;
                        
                        updateDetails.UpdatedBy = UserId;
                        updateDetails.UpdatedOn = DateTime.Today;
                        entities.SaveChanges();

                        #region Enable

                        DateTime resultdt = DateTime.ParseExact(enableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        
                        DateTime D1 = resultdt;                        
                        DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                        List<int> finalcomplianceDetails = (from row in entities.Vendor_AssignedTemplate
                                                            where row.TemplateID == id
                                                               && row.CustomerID == CustomerID
                                                            select (int)row.ComplianceID).ToList();


                        var getallList = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CustomerID)
                                          select row).ToList();

                        var MappingDetails = (from row in entities.Vendor_ComplianceMapping
                                              where row.TemplateID == id
                                                               && row.CustID == CustomerID
                                              select row).ToList();

                        var ContractorProjectMappingIDs = MappingDetails.Select(x => x.ContractorProjectMappingID).ToList();

                        if (ContractorProjectMappingIDs.Count > 0 && finalcomplianceDetails.Count > 0)
                        {
                            getallList = getallList.Where(x => x.AuditCheckListStatusID == 1
                                            && x.ScheduleOn >= Datevalue
                                        && ContractorProjectMappingIDs.Contains((int)x.ContractorProjectMappingID)).ToList();

                            var getListcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListStatus(CustomerID, -1, -1)
                                                     select row).ToList();

                            foreach (var item in getallList)
                            {
                                var Listcompliance = (from row in getListcompliance
                                                      where row.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                                      && row.ContractProjMappingID == item.ContractorProjectMappingID
                                                      && finalcomplianceDetails.Contains((int)row.ComplianceID)
                                                       && row.ComplianceStatusID == null
                                                      select row).ToList();
                                foreach (var item1 in Listcompliance)
                                {

                                    Vendor_ContractProjMappingStepMapping scheobj = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                     where row.ContractProjMappingID == item1.ContractProjMappingID
                                                                                    && row.ContractProjMappingScheduleOnID == item1.ContractProjMappingScheduleOnID
                                                                                    && row.ComplianceID == item1.ID
                                                                                     select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }

                        #endregion
                        result = true;

                    }
                    else
                    {
                        result = false;
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetTemplateByTemplateID(int TemplateId, int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<AssignedTemplateDTO> templateFinalList = new List<AssignedTemplateDTO>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;

                    var ComplianceList = (from row in entities.Vendor_ComplainceMaster
                                        where row.CustomerId == CID
                                         && row.IsDeleted == false
                                        select row).ToList();

                    var templateList = (from row in entities.Vendor_AssignedTemplate
                                        where row.TemplateID == TemplateId
                                        && row.CustomerID == CID
                                         && row.IsDeleted == false
                                        select row);

                    foreach (var itemTemplate in templateList)
                    {
                        string compName = ComplianceList.Where(y => y.ID == itemTemplate.ComplianceID).Select(x => x.ComplainceHeader).FirstOrDefault();

                        bool output = false;
                        var compIDList = (from row in ComplianceList
                                          where row.IsDeleted == false && row.ID == itemTemplate.ComplianceID 
                                          select row).FirstOrDefault();

                        if (compIDList != null && compIDList.IsDisabled == true)
                        {
                            output = true;
                        }

                        AssignedTemplateDTO finalTemplateData = new AssignedTemplateDTO();
                        finalTemplateData.ID = itemTemplate.ID;
                        finalTemplateData.TemplateTye = itemTemplate.TemplateTye;
                        finalTemplateData.TemplateID = itemTemplate.TemplateID;
                        finalTemplateData.ActID = itemTemplate.ActID;
                        if (itemTemplate.ActID != null)
                            finalTemplateData.ActName = GetActbyID(itemTemplate.ActID);
                        finalTemplateData.ComplianceID = itemTemplate.ComplianceID;
                        finalTemplateData.ComplianceName = compName;
                        finalTemplateData.ComplianceIsDisabled = output;

                        //finalTemplateData.ComplianceName = GetCompliancebyID(itemTemplate.ComplianceID);
                        //finalTemplateData.ComplianceIsDisabled = GetComplianceISdisablebyID(itemTemplate.ComplianceID);                        
                        templateFinalList.Add(finalTemplateData);
                    }
                    if (templateFinalList.Count > 0)
                    {
                        templateFinalList = templateFinalList.Where(x => x.ComplianceIsDisabled == false).ToList();
                    }
                    //var finallist = JsonConvert.SerializeObject(userList);
                    return Json(templateFinalList, JsonRequestBehavior.AllowGet);
                    //return JsonConvert.SerializeObject(ProjectList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        #endregion

        #region Data Points
        public ActionResult SaveDataPoints(int ID, int UserId, int CustomerID, string Type, string Desc)
        {
            var result = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = UserId;
                    int CustID = CustomerID;
                    int id = ID;
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        Vendor_DataPoints updateDetails = (from row in entities.Vendor_DataPoints
                                                           where row.ID == id
                                                           select row).FirstOrDefault();

                        if (updateDetails != null)
                        {
                            if (updateDetails.Description != Desc)
                            {
                                if (CheckDescriptionExist(Desc, CustID) == false)
                                    result = "Exist";
                            }
                            if (result != "Exist")
                            {
                                updateDetails.Description = Desc;
                                updateDetails.DescType = Type;
                                updateDetails.CustID = CustID;
                                updateDetails.CreatedBy = UserID;
                                updateDetails.CreatedOn = DateTime.Today;
                                updateDetails.UpdatedBy = UserID;
                                updateDetails.UpdatedOn = DateTime.Today;
                                updateDetails.IsDeleted = false;
                                entities.SaveChanges();
                                result = "Success";
                            }
                        }
                        else
                        {
                            #region add new detail
                            if (CheckDescriptionExist(Desc, CustID))
                            {
                                Vendor_DataPoints DatePointMaster = new Vendor_DataPoints()
                                {
                                    ID = id,
                                    Description = Desc,
                                    DescType = Type,
                                    CustID = CustID,
                                    CreatedBy = UserID,
                                    CreatedOn = DateTime.Today,
                                    UpdatedBy = UserID,
                                    UpdatedOn = DateTime.Today,
                                    IsDeleted = false

                                };
                                entities.Vendor_DataPoints.Add(DatePointMaster);
                                entities.SaveChanges();
                                result = "Success";
                            }
                            else
                            {
                                result = "Exist";
                            }

                            #endregion
                        }

                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public ActionResult GetAllDataPoints(int CID)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    var complainceList = (from row in entities.Vendor_DataPoints
                                          where row.IsDeleted == false && row.CustID == CID
                                          select row);
                    List<Vendor_DataPoints> complainceFinalList = new List<Vendor_DataPoints>();

                    //complainceList = complainceList.Where(x => x.IsDisabled == false || x.IsDisabled == null || x.IsDisabled == true && x.DisabledDate <= DateTime.Today);

                    foreach (var item in complainceList)
                    {
                        Vendor_DataPoints finalData = new Vendor_DataPoints();
                        finalData.ID = item.ID;
                        finalData.Description = item.Description;
                        finalData.DescType = item.DescType;
                        finalData.IsDeleted = item.IsDeleted;
                        complainceFinalList.Add(finalData);
                    }

                    complainceFinalList = complainceFinalList.OrderByDescending(q => q.ID).ToList();

                    return Json(complainceFinalList, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }

        public bool CheckDescriptionExist(string Desc, int CustID)
        {
            bool result = true;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {

                    var checkTemplateName = (from row in entities.Vendor_DataPoints
                                             where row.CustID == CustID && row.IsDeleted == false
                                             select row).ToList();
                    bool containsItem = checkTemplateName.Any(item => item.Description.Trim().ToLower() == Desc.Trim().ToLower());
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public ActionResult DeleteDataPointMaster(int ID, int CustomerID, int UserId)
        {
            var result = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = UserId;
                    int CustID = CustomerID;
                    int id = ID;
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        Vendor_DataPoints updateDetails = (from row in entities.Vendor_DataPoints
                                                           where row.ID == id
                                                           select row).FirstOrDefault();
                        updateDetails.CustID = CustID;
                        updateDetails.CreatedBy = UserID;
                        updateDetails.CreatedOn = DateTime.Today;
                        updateDetails.UpdatedBy = UserID;
                        updateDetails.UpdatedOn = DateTime.Today;
                        updateDetails.IsDeleted = true;
                        entities.SaveChanges();
                        result = "Success";


                        return Json(result, JsonRequestBehavior.AllowGet); //serializer.Serialize(RtrnMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json(ex.Message.ToString());
            }
        }
        #endregion

    }
}