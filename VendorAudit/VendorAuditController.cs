﻿using AppWebApplication.Data;
using AppWebApplication.Models;
using AppWebApplication.VendorAudit.Model;
using AppWebApplication.VendorAuditModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using VendorAuditData;

namespace AppWebApplication.VendorAudit
{
    public class VendorAuditController : ApiController
    {
        [Route("VendorAudit/PostExcelFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UploadContractorMasterFile(HttpRequestMessage request, int CustID, int UserID, string fileMasterName)
        {
            string message = string.Empty;
            var httpRequest = HttpContext.Current.Request;
            HttpPostedFile file = null;

            try
            {
                if (httpRequest.Files.Count > 0)
                {
                    file = httpRequest.Files[0];
                    if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string fileName = file.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];

                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                if (fileMasterName == "ContractorMaster")
                                {
                                    message = VendorAuditMaster.ProcessContractorFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "UserMaster")
                                {
                                    message = VendorAuditMaster.ProcessUserFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "ProjectMaster")
                                {
                                    message = VendorAuditMaster.ProcessProjectFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "Contractor-ProjectMapping")
                                {
                                    message = VendorAuditMaster.ProcessProjectMappingFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else
                                {
                                    return request.CreateResponse(HttpStatusCode.HttpVersionNotSupported, "error");
                                }
                            }
                        }
                    }
                }
                return request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new HttpResponseMessage()
                {
                    Content = new StringContent("error", Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ErrorFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ErrorFile(string Name)
        {
            try
            {
                var path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["LogFileVaibhav"]) + Name;
                var test = Path.ChangeExtension(path, ".txt");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "ErrorFile.txt"
                };
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new HttpResponseMessage()
                {
                    Content = new StringContent("File Not Found", Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphCountForSubContractor")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphCountForSubContractor(int UID, int CID, string Role, bool SubContractor, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<DashboradCount> objResult = new List<DashboradCount>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;
            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, SubContractor);


                if (!string.IsNullOrEmpty(StartDateDetail))
                {
                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                }
                if (!string.IsNullOrEmpty(EndDateDetail))
                {
                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                }
                if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                {
                    System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                    if (ProjectIDs.Count != 0)
                    {
                        output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                    }
                }
                if (Location != "[]" && !string.IsNullOrEmpty(Location))
                {
                    System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                    if (Locations.Count != 0)
                    {
                        output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(Period))
                {
                    int period = Convert.ToInt32(Period);

                    if (period == 0) //Current YTD
                    {
                        if (DateTime.Today.Month > 3)
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-1);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }
                    else if (period == 1) //Current + Previous YTD
                    {
                        if (DateTime.Today.Month > 3)
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-1);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-2);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }
                    else if (period == 2) //All
                    {
                        string dtfrom = Convert.ToString("01-01-1900");
                        string dtto = Convert.ToString("01-01-1900");
                        FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else if (period == 3)// Last Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 4)//Last 3 Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 5)//Last 6 Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 6)//This Month
                    {
                        DateTime now = DateTime.Now;
                        FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                        ToFinancialYearSummary = DateTime.Now;
                    }

                    if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                    {
                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                        }
                    }

                    if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                    {
                        output = (from row in output
                                  where row.ScheduleOn >= FromFinancialYearSummary
                                  && row.ScheduleOn <= ToFinancialYearSummary
                                  select row).ToList();
                    }
                }

                int Open = output.Where(x => x.Status == "Upcoming").ToList().Count;
                int Overdue = output.Where(x => x.Status == "Overdue").ToList().Count;
                int Underreview = output.Where(x => x.Status == "Pending Review").ToList().Count;//"Underreview"
                int Closed = output.Where(x => x.Status == "Closed").ToList().Count;
                DateTime PeriodStartDate = FromFinancialYearSummary;
                DateTime PeriodEndDate = ToFinancialYearSummary;

                objResult.Clear();
                objResult.Add(new DashboradCount { Open = Open, Overdue = Overdue, Underreview = Underreview, Closed = Closed, PeriodStartDate = PeriodStartDate, PeriodEndDate = PeriodEndDate });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new DashboradCount { Open = 0, Overdue = 0, Underreview = 0, Closed = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetCustomerName")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCustomerName(int custID)
        {
            string CustomerName = string.Empty;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CustomerName = (from row in entities.Customers
                                where row.ID == custID
                                select row.Name).SingleOrDefault();
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent(CustomerName, Encoding.UTF8, "application/json")
            };
        }

        [Route("VendorAudit/GetAuditscheduledcheckListNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAuditscheduledcheckListNew(int CustID, int UID, string Role)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result> objResult = new List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCPD"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CustID, -1, -1)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    else if (Role.Equals("VCPH"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CustID, -1, -1)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectHead == UID).ToList();

                    }
                    else if (Role.Equals("VCMNG"))
                    {

                        List<int> ProjectIds = (from row in entities.Vendor_ProjectMGMTMapping
                                                where row.UserID == UID && row.CustID == CustID
                                                && row.IsDeleted == false
                                                select row.ProjectId).ToList();

                        if (ProjectIds.Count > 0)
                        {
                            objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CustID, -1, -1)
                                         select row).ToList();

                            objResult = objResult.Where(x => ProjectIds.Contains(x.ProjectID)).ToList();
                        }
                    }
                    else if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CustID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || x.ProjectSubContSpocID == UID).ToList();

                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CustID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     && row.AuditorID == UID
                                     select row).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetAuditscheduledcheckList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAuditscheduledcheckList(int CustID, int UID, string Role)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCPD"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID,-1,-1)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    else if (Role.Equals("VCPH"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectHead == UID).ToList();

                    }
                    else if (Role.Equals("VCMNG"))
                    {

                        List<int> ProjectIds = (from row in entities.Vendor_ProjectMGMTMapping
                                                where row.UserID == UID && row.CustID == CustID
                                                && row.IsDeleted == false
                                                select row.ProjectId).ToList();

                        if (ProjectIds.Count > 0)
                        {
                            objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1)
                                         select row).ToList();

                            objResult = objResult.Where(x => ProjectIds.Contains(x.ProjectID)).ToList();
                        }
                    }
                    else if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || x.ProjectSubContSpocID == UID).ToList();

                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     && row.AuditorID == UID
                                     select row).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetAuditscheduledList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAuditscheduledList(int CustID, int UID, string Role)
        {
            List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCPD"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    else if (Role.Equals("VCPH"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectHead == UID).ToList();

                    }
                    else if (Role.Equals("VCMNG"))
                    {                        

                        List<int> ProjectIds = (from row in entities.Vendor_ProjectMGMTMapping
                                                where row.UserID == UID && row.CustID == CustID
                                                && row.IsDeleted == false
                                                select row.ProjectId).ToList();

                        if (ProjectIds.Count > 0)
                        {
                            objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                         select row).ToList();

                            objResult = objResult.Where(x => ProjectIds.Contains(x.ProjectID)).ToList();
                        }
                    }
                    else if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }
                    else if (Role.Equals("VCCAD"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     select row).OrderBy(x=>x.ProjectID).ToList();
                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     select row).Where(x => x.AuditorID == UID).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetProjectMGMTMappedUSerList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetProjectMGMTMappedUSerList(int CustID)
        {
            List<UserLIST> UserDetailsList = new List<UserLIST>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    UserDetailsList = (from row in entities.Vendor_SP_GetUserMGMTMapped(CustID)
                                                 select new UserLIST
                                                 {
                                                     UID = row.ID,
                                                     UName = row.UserName
                                                 }).ToList();
                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UserDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UserDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetProjectMGMTMappedList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetProjectMGMTMappedList(int CustID)
        {
            List<Vendor_SP_GetProjectMGMTMapped_Result> ContractorDetailsList = new List<Vendor_SP_GetProjectMGMTMapped_Result>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    ContractorDetailsList = (from row in entities.Vendor_SP_GetProjectMGMTMapped(CustID)                                            
                                             select row).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/SaveMGMTAssignemtDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SaveMGMTAssignemtDetails(int UID, int CID, string ProjectID, string MGMTID)
        {
            List<UpdateNotification> objResult = new List<UpdateNotification>();
            try
            {

                List<int> MGMTIDs = new List<int>();
                List<int> ProjectIDs = new List<int>();


                ProjectIDs.Clear();
                if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                {
                    System.Collections.Generic.IList<int> ProjectIDdd = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(ProjectID);
                    if (ProjectIDdd.Count != 0)
                    {
                        ProjectIDs.Clear();
                        foreach (var item in ProjectIDdd)
                        {
                            ProjectIDs.Add(item);
                        }
                    }
                }

                MGMTIDs.Clear();
                if (MGMTID != "[]" && !string.IsNullOrEmpty(MGMTID))
                {
                    System.Collections.Generic.IList<int> MGMTIDddd = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(MGMTID);
                    if (MGMTIDddd.Count != 0)
                    {
                        MGMTIDs.Clear();
                        foreach (var item in MGMTIDddd)
                        {
                            MGMTIDs.Add(item);
                        }
                    }
                }
                if (ProjectIDs.Count > 0 && MGMTIDs.Count > 0)
                {
                    var output = VendorAuditMaster.GetProjectMaster(CID);
                    output = output.Where(x => ProjectIDs.Contains(x.ID)).ToList();

                    foreach (var item1 in output)
                    {
                        foreach (var item in MGMTIDs)
                        {
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                Vendor_ProjectMGMTMapping lst = (from row in entities.Vendor_ProjectMGMTMapping
                                                                 where row.UserID == item
                                                                 && row.ProjectId == item1.ID
                                                                 && row.CustID == CID
                                                                    && row.IsDeleted == false
                                                                 select row).FirstOrDefault();

                                if (lst != null)
                                {
                                    lst.UpdatedBy = UID;
                                    lst.UpdatedOn = DateTime.Now;
                                    lst.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    Vendor_ProjectMGMTMapping Mapped = new Vendor_ProjectMGMTMapping()
                                    {
                                        ProjectId = item1.ID,
                                        UserID = item,
                                        CustID = CID,
                                        LocationId = item1.LocationID,
                                        CreatedBy = UID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = UID,
                                        UpdatedOn = DateTime.Now,
                                        IsDeleted = false,
                                    };
                                    entities.Vendor_ProjectMGMTMapping.Add(Mapped);
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                }

                objResult.Add(new UpdateNotification { Message = "Success" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetScheduleStatusCont")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetScheduleStatusCont(int CustID, int UID, string ForMonth, string ProjectName, string Role, long ContractorProjectMappingID, long ScheduleOnID, int SubContractorID)
        {
            List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     where row.ProjectName == ProjectName
                                     && row.ForMonth == ForMonth
                                     && row.ContractorID == SubContractorID
                                     select row).ToList();
                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     where row.ProjectName == ProjectName
                                     && row.ForMonth == ForMonth
                                     && row.AuditorID == UID
                                     && row.IsSubcontract == 1
                                     select row).Where(x => x.AuditorID == UID).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetSubmisionSummaryList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSubmisionSummaryList(int UID,int CustID,int SID,int ContractProjMappingID, string ForMonth,string Role)
        {
            List<SubmissionSummary> DetailsList = new List<SubmissionSummary>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    DetailsList.Clear();
                    if (Role.Equals("VCCON"))
                    {
                        List<Vendor_ContractorMaster> getContractorList = (from row in entities.Vendor_ContractorMaster
                                                                           where row.CustID == CustID
                                                                           && row.IsDelete == false
                                                                           && row.Isdisable == 0
                                                                           select row).ToList();

                        var getContractor = (from row in getContractorList
                                             where row.AvacomUserID == UID
                                             && row.CustID == CustID
                                             && row.IsDelete == false
                                             && row.Isdisable == 0
                                             select row).FirstOrDefault();

                        var objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)                                         
                                         select row).ToList();
                        
                        if (objResult.Count > 0)
                        {
                            var objResult1 = (from row in objResult
                                              where row.ScheduleOnID == SID && row.ContractorProjectMappingID == ContractProjMappingID
                                             select row).ToList();

                            if (objResult1.Count > 0)
                            {
                                int getcount = objResult1.Where(x => x.AuditCheckListStatusID != 1).ToList().Count;

                                if (getcount > 0)
                                {
                                    DetailsList.Add(new SubmissionSummary { ContractorName = getContractor.ContractorName, complition = "100" });
                                }
                                else
                                {
                                    DetailsList.Add(new SubmissionSummary { ContractorName = getContractor.ContractorName, complition = "0" });
                                }
                            }
                            #region sub contractor list

                            List<long> undercontractorlist = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                                              where row.ForMonth == ForMonth
                                                              && row.UnderContractorID == getContractor.ID
                                                              && row.IsShowContractor == 1
                                                              select (long)row.ContractorID).ToList();
                            foreach (var item in undercontractorlist)
                            {
                                var objResult12 = (from row in entities.Vendor_ScheduleOn
                                                   where row.ForMonth == ForMonth
                                                   && row.ContractorID == item
                                                   && row.IsActive == true
                                                   select row).FirstOrDefault();

                                if (objResult12 != null)
                                {
                                    var outputx = (from row in objResult
                                                   where row.ScheduleOnID == objResult12.ID
                                                   && row.ContractorProjectMappingID == objResult12.ContractProjectMappingID
                                                   select row).ToList();

                                    var getContractorname = (from row in getContractorList
                                                             where row.ID == item
                                                             && row.CustID == CustID
                                                             && row.IsDelete == false
                                                             && row.Isdisable == 0
                                                             select row.ContractorName).FirstOrDefault();
                                    if (outputx.Count > 0)
                                    {
                                        int getcount = outputx.Where(x => x.AuditCheckListStatusID != 1).ToList().Count;
                                        if (getcount > 0)
                                        {
                                            DetailsList.Add(new SubmissionSummary { ContractorName = getContractorname, complition = "100" });
                                        }
                                        else
                                        {
                                            DetailsList.Add(new SubmissionSummary { ContractorName = getContractorname, complition = "0" });
                                        }                                       
                                    }                                      
                                }
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        var objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                         where row.ScheduleOnID == SID && row.ContractorProjectMappingID == ContractProjMappingID
                                         select row).ToList();

                        long contractProductmappingID = (from row in objResult                                                         
                                                         select row.ContractorProjectMappingID).FirstOrDefault();

                        var getContractorName = (from row in entities.Vendor_ContractorMaster
                                                 join row1 in entities.Vendor_ContractorProjectMapping
                                                 on row.ID equals row1.ContractorID
                                                 where row.CustID == CustID
                                                 && row.IsDelete == false
                                                 && row.Isdisable == 0
                                                 && row1.ID == contractProductmappingID
                                                 select row.ContractorName).FirstOrDefault();

                        int ClosedCount1 = objResult.Where(x => x.AuditCheckListStatusID != 1).ToList().Count;

                        if (ClosedCount1 > 0)
                        {
                            DetailsList.Add(new SubmissionSummary { ContractorName = getContractorName, complition = "100" });
                        }
                        else
                        {
                            DetailsList.Add(new SubmissionSummary { ContractorName = getContractorName, complition = "0" });
                        }
                    }

                    #region old
                    //if (Role.Equals("VCCON"))
                    //{
                    //    List<Vendor_ContractorMaster> getContractorList = (from row in entities.Vendor_ContractorMaster
                    //                                                       where row.CustID == CustID
                    //                                                       && row.IsDelete == false
                    //                                                       && row.Isdisable == 0
                    //                                                       select row).ToList();

                    //    var getContractor = (from row in getContractorList
                    //                         where row.AvacomUserID == UID
                    //                         && row.CustID == CustID
                    //                         && row.IsDelete == false
                    //                         && row.Isdisable == 0
                    //                         select row).FirstOrDefault();

                    //    var objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1)
                    //                     select row).ToList();

                    //    if (objResult.Count > 0)
                    //    {
                    //        var output = (from row in objResult
                    //                      where row.ContractProjMappingScheduleOnID == SID
                    //                      && row.ContractProjMappingID == ContractProjMappingID
                    //                      select row).ToList();

                    //        int ClosedCount = output.Where(x => x.AuditCheckListStatusID == 4).ToList().Count;

                    //        int percentComplete = (int)Math.Round((double)(100 * ClosedCount) / (output.Count));

                    //        DetailsList.Add(new SubmissionSummary { ContractorName = getContractor.ContractorName, complition = percentComplete.ToString() });

                    //        #region sub contractor list

                    //        List<long> undercontractorlist = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                    //                                          where row.ForMonth == ForMonth
                    //                                          && row.UnderContractorID == getContractor.ID
                    //                                          && row.IsShowContractor == 1
                    //                                          select (long)row.ContractorID).ToList();
                    //        foreach (var item in undercontractorlist)
                    //        {
                    //            var objResult12 = (from row in entities.Vendor_ScheduleOn
                    //                               where row.ForMonth == ForMonth
                    //                               && row.ContractorID == item
                    //                               && row.IsActive == true
                    //                               select row).FirstOrDefault();

                    //            if (objResult12 != null)
                    //            {
                    //                var outputx = (from row in objResult
                    //                               where row.ContractProjMappingScheduleOnID == objResult12.ID
                    //                               && row.ContractProjMappingID == objResult12.ContractProjectMappingID
                    //                               select row).ToList();

                    //                var getContractorname = (from row in getContractorList
                    //                                         where row.ID == item
                    //                                         && row.CustID == CustID
                    //                                         && row.IsDelete == false
                    //                                         && row.Isdisable == 0
                    //                                         select row.ContractorName).FirstOrDefault();

                    //                int ClosedCount1 = output.Where(x => x.AuditCheckListStatusID == 4).ToList().Count;

                    //                int percentComplete1 = (int)Math.Round((double)(100 * ClosedCount1) / (outputx.Count));

                    //                DetailsList.Add(new SubmissionSummary { ContractorName = getContractorname, complition = percentComplete1.ToString() });

                    //            }
                    //        }
                    //        #endregion
                    //    }
                    //}
                    //else
                    //{
                    //    var objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, ContractProjMappingID, SID)                                         
                    //                     select row).ToList();

                    //    long contractProductmappingID = (from row in objResult
                    //                                    select row.ContractProjMappingID).FirstOrDefault();

                    //    var getContractorName = (from row in entities.Vendor_ContractorMaster
                    //                             join row1 in entities.Vendor_ContractorProjectMapping
                    //                             on row.ID equals row1.ContractorID
                    //                            where row.CustID == CustID
                    //                            && row.IsDelete == false
                    //                            && row.Isdisable == 0
                    //                             select row.ContractorName).FirstOrDefault();

                    //    int ClosedCount1 = objResult.Where(x => x.AuditCheckListStatusID == 4).ToList().Count;

                    //    int percentComplete1 = (int)Math.Round((double)(100 * ClosedCount1) / (objResult.Count));

                    //    DetailsList.Add(new SubmissionSummary { ContractorName = getContractorName, complition = percentComplete1.ToString() });

                    //}

                    #endregion
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(DetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(DetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetContractorListrolewise")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetContractorListrolewise(int UID,int CustID,string Role)
        {
            List<Vendor_ContractorMaster> ContractorDetailsList = new List<Vendor_ContractorMaster>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var output = VendorAuditMaster.GetDashboradCount(UID, CustID, Role, false);

                    List<int> Contractors = output.Select(x => x.ContractorID).Distinct().ToList();

                    ContractorDetailsList = (from row in entities.Vendor_ContractorMaster
                                             where row.CustID == CustID
                                             && row.IsDelete == false
                                             && row.Isdisable == 0
                                             && Contractors.Contains(row.ID)
                                             select row).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetContractorList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetContractorList(int CustID)
        {
            List<Vendor_ContractorMaster> ContractorDetailsList = new List<Vendor_ContractorMaster>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    ContractorDetailsList = (from row in entities.Vendor_ContractorMaster
                                             where row.CustID == CustID
                                             && row.IsDelete == false
                                             && row.Isdisable == 0
                                             select row).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetdocumentChecklistList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetdocumentChecklistList(int CustID, int UID, string Role)
        {
            List<Vendor_Sp_GetContractProjectchecklistDocumentList_Result> objResult = new List<Vendor_Sp_GetContractProjectchecklistDocumentList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCCON"))// for Contractor
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                     select row).GroupBy(x => new { x.ScheduleOnID, x.ContractorProjectMappingID, x.ForMonth }).Select(x => x.FirstOrDefault()).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();

                    }
                    else if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CustID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        objResult = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                     select row).Where(x => ProjectList.Contains(x.ProjectID)).GroupBy(x => new { x.ScheduleOnID, x.ContractorProjectMappingID, x.ForMonth }).Select(x => x.FirstOrDefault()).ToList();
                    }
                    else if (Role.Equals("VCPD"))//for Director
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                     select row).Where(x => x.ProjectDirector == UID).GroupBy(x => new { x.ScheduleOnID, x.ContractorProjectMappingID, x.ForMonth }).Select(x => x.FirstOrDefault()).ToList();
                    }
                    else if (Role.Equals("VCPH"))//for Head
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                     select row).Where(x => x.ProjectHead == UID).GroupBy(x => new { x.ScheduleOnID, x.ContractorProjectMappingID, x.ForMonth }).Select(x => x.FirstOrDefault()).ToList();
                    }
                    else if (Role.Equals("VCAUD"))//for Auditor
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                     select row).Where(x => x.AuditorID == UID).GroupBy(x => new { x.ScheduleOnID, x.ContractorProjectMappingID, x.ForMonth }).Select(x => x.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        objResult.Clear();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetSubcontractorPeriodchecklistList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSubcontractorPeriodchecklistList(int contId,int CustID, int UID, string ForMonth,int ContractProjectMappingID)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> ContractorDetailsList = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var objResult = (from row in entities.Vendor_ScheduleOn
                                     where row.ForMonth == ForMonth
                                     && row.ContractorID == contId
                                     && row.IsActive == true
                                      && row.ContractProjectMappingID == ContractProjectMappingID
                                     select row).FirstOrDefault();

                    if (objResult != null)
                    {
                        ContractorDetailsList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, objResult.ContractProjectMappingID, Convert.ToInt32(objResult.ID))
                                                 select row).ToList();
                        //ContractorDetailsList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, objResult.ContractProjectMappingID, Convert.ToInt32(objResult.ID))
                        //                 select row).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetSubcontractorList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSubcontractorList(int CustID, int UID, string ForMonth,string role,int projectmappingID)
        {
            List<Vendor_ContractorMaster> ContractorDetailsList = new List<Vendor_ContractorMaster>();

            try
            {                
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (role.Equals("VCCON"))
                    {
                        List<int> objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                               where row.ForMonth == ForMonth
                                               && row.ProjectSubContSpocID == UID
                                               && row.IsShowContractor == 1
                                               && row.ContractorProjectMappingID == projectmappingID
                                               select row.ContractorID).ToList();

                        if (objResult.Count > 0)
                        {
                            ContractorDetailsList = (from row in entities.Vendor_ContractorMaster
                                                     where objResult.Contains(row.ID)
                                                     && row.IsDelete == false
                                                     && row.Isdisable == 0
                                                     select row).ToList();
                        }
                    }
                    else if (role.Equals("VCAUD"))
                    {
                        List<int> objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                               where row.ForMonth == ForMonth
                                               && row.AuditorID == UID
                                               && row.IsShowContractor == 1
                                               && row.ContractorProjectMappingID == projectmappingID
                                               select row.ContractorID).ToList();

                        if (objResult.Count > 0)
                        {
                            ContractorDetailsList = (from row in entities.Vendor_ContractorMaster
                                                     where objResult.Contains(row.ID)
                                                     && row.IsDelete == false
                                                     && row.Isdisable == 0
                                                     select row).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetScheduleStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetScheduleStatus(int CustID, int UID, string ForMonth, string ProjectName, string Role,int ContractorProjectMappingID,int ScheduleOnID)
        {
            List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (string.IsNullOrEmpty(ForMonth))
                        ForMonth = "";


                    if (Role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     where row.ProjectName == ProjectName
                                     && row.ForMonth == ForMonth
                                     && row.ProjectContSpocID == UID
                                     && row.ContractorProjectMappingID == ContractorProjectMappingID
                                     && row.ScheduleOnID == ScheduleOnID
                                     select row).ToList();
                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CustID)
                                     where row.ProjectName == ProjectName
                                     && row.ForMonth == ForMonth
                                     && row.AuditorID == UID
                                     && row.IsShowContractor == 0//by amol
                                     && row.ContractorProjectMappingID == ContractorProjectMappingID
                                     && row.ScheduleOnID == ScheduleOnID
                                     select row).Where(x => x.AuditorID == UID).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetVendorCommentDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetVendorCommentDetail(string MsgID, string message_id)
        {
            try
            {

                List<UpdateNotification> output = new List<UpdateNotification>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    int CommntID = Convert.ToInt32(MsgID);
                    if (message_id == "0")
                    {
                        Vendor_tbl_PostComment Uobj1 = (from row in entities.Vendor_tbl_PostComment
                                                      where row.ID == CommntID
                                                      && row.IsActive == true
                                                      select row).FirstOrDefault();
                        if (Uobj1 != null)
                        {
                            output.Clear();
                            if (!string.IsNullOrEmpty(Uobj1.FilePath))
                            {
                                output.Add(new UpdateNotification { Message = Uobj1.FilePath.Trim() });
                            }
                        }
                    }
                    else
                    {
                        Vendor_tbl_PostSubComment Uobj1 = (from row in entities.Vendor_tbl_PostSubComment
                                                           where row.Id == CommntID
                                                         && row.IsActive == true
                                                         select row).FirstOrDefault();
                        if (Uobj1 != null)
                        {
                            output.Clear();
                            if (!string.IsNullOrEmpty(Uobj1.FilePath))
                            {
                                output.Add(new UpdateNotification { Message = Uobj1.FilePath.Trim() });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/PostSubVendorCommentdata")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PostSubVendorCommentdata()//string MsgID, string Comments, int UserId, int CustId, string ContractID
        {
            try
            {
                string MsgID = HttpContext.Current.Request.Form["MsgID"];
                string Comments = HttpContext.Current.Request.Form["Comments"];

                var UId = HttpContext.Current.Request.Form["UserId"];
                int UserId = Convert.ToInt32(UId);

                var CId = HttpContext.Current.Request.Form["CustId"];
                int CustId = Convert.ToInt32(CId);


                string CompID = HttpContext.Current.Request.Form["ComplianceID"];
                int ComplianceID = Convert.ToInt32(CompID);

                string SID = HttpContext.Current.Request.Form["ScheduleonID"];
                int ScheduleonID = Convert.ToInt32(SID);
                
                string role = HttpContext.Current.Request.Form["Loginrole"];

                List<Check> ComplianceTypeobj = new List<Check>();

                if (UserId != null && UserId > 0 && !string.IsNullOrEmpty(role))
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        User user = UserManagement.GetByID(UserId);
                        if (user != null && user.IsDeleted == false && user.IsActive == true)                        
                        {
                            int IsFile = 0;
                            string FilePath = string.Empty;

                            HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                            if (postedFile1 != null)
                            {
                                #region old
                                string filepath = ConfigurationManager.AppSettings["VendorCommentFile"];

                                string storefolder = "VendorSubCommentedFile";
                                string path = @"" + filepath + "\\" + storefolder + "\\" + user.ID + "\\";

                                if (!Directory.Exists(path))
                                    Directory.CreateDirectory(path);

                                string fileName = Path.GetFileName(postedFile1.FileName);
                                postedFile1.SaveAs(path + fileName);
                                IsFile = 1;
                                FilePath = storefolder + "/" + user.ID + "/" + fileName;

                                string dbFilePath = "~/" + FilePath;

                                Stream fs = postedFile1.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                VendorDocument tempContractDocument = null;

                                tempContractDocument = new VendorDocument()
                                {
                                    ComplianceID = ComplianceID,
                                    ScheduleOnID= ScheduleonID,
                                    UserID = UserId,
                                    CustomerID = CustId,
                                    DocPath = dbFilePath,
                                    DocData = bytes,
                                    DocName = fileName,
                                    FileSize = postedFile1.ContentLength,
                                    IsfromComment = true
                                };

                                long _objTempDocumentID = VendorAuditMaster.CreateVendorDocument(tempContractDocument);
                                #endregion
                            }

                            Vendor_tbl_PostSubComment addsubcomment = new Vendor_tbl_PostSubComment();
                            addsubcomment.ComplianceID = Convert.ToInt64(ComplianceID);
                            addsubcomment.ScheduleOnID = Convert.ToInt64(ScheduleonID);
                            addsubcomment.CustID = CustId;
                            addsubcomment.PostCommentID = Convert.ToInt64(MsgID);
                            addsubcomment.PostSubComment = Comments;
                            addsubcomment.IsFile = IsFile;
                            addsubcomment.FilePath = FilePath;
                            addsubcomment.FromUserId = UserId;
                            addsubcomment.FromUserName = user.FirstName + " " + user.LastName;
                            addsubcomment.IsActive = true;
                            addsubcomment.CreatedBy = UserId;
                            addsubcomment.CreatedOn = DateTime.Now;
                            entities.Vendor_tbl_PostSubComment.Add(addsubcomment);
                            entities.SaveChanges();

                            long PostCommentID = Convert.ToInt64(MsgID);
                            List<long> UserId1s = (from row in entities.Vendor_tbl_UserMappingComment
                                                   where row.ComplianceID == ComplianceID
                                                   && row.ScheduleOnID == ScheduleonID
                                                  && row.PostCommentID == PostCommentID
                                                  && row.CustID == CustId
                                                   select row.ToUserId).ToList();

                            List<long> UserId2s = (from row in entities.Vendor_tbl_PostComment
                                                   where row.ComplianceID == ComplianceID
                                                   && row.ScheduleOnID == ScheduleonID
                                                   && row.ID == PostCommentID
                                                    && row.CustID == CustId
                                                   select row.PostUserID).ToList();

                            List<long> UserIds = UserId1s.Union(UserId2s).Distinct().ToList();
                            UserIds = UserIds.Distinct().ToList();
                            List<long> lstUserIdEmails = new List<long>();
                            lstUserIdEmails.Clear();
                            foreach (var item in UserIds)
                            {
                                if (item == UserId)
                                {
                                    Vendor_tbl_commentDetail tocntobj = (from row in entities.Vendor_tbl_commentDetail
                                                                         where row.ComplianceID == ComplianceID
                                                   && row.ScheduleOnID == ScheduleonID
                                                                       && row.msgid == MsgID
                                                                       && row.UserID == item
                                                                       select row).FirstOrDefault();
                                    if (tocntobj != null)
                                    {
                                        tocntobj.Flag = true;
                                        tocntobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        Vendor_tbl_commentDetail addobj = new Vendor_tbl_commentDetail();                                        
                                        addobj.ComplianceID = ComplianceID;
                                        addobj.ScheduleOnID = ScheduleonID;
                                        addobj.CustId = CustId;
                                        addobj.msgid = MsgID;
                                        addobj.UserID = item;
                                        addobj.IsActive = true;
                                        addobj.Flag = true;
                                        entities.Vendor_tbl_commentDetail.Add(addobj);
                                        entities.SaveChanges();
                                    }
                                }
                                else
                                {
                                    lstUserIdEmails.Add(item);
                                    Vendor_tbl_commentDetail tocntobj = (from row in entities.Vendor_tbl_commentDetail
                                                                         where row.ComplianceID == ComplianceID
                                                   && row.ScheduleOnID == ScheduleonID
                                                                       && row.msgid == MsgID
                                                                       && row.UserID == item
                                                                       select row).FirstOrDefault();
                                    if (tocntobj != null)
                                    {
                                        tocntobj.Flag = false;
                                        tocntobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        Vendor_tbl_commentDetail addobj = new Vendor_tbl_commentDetail();
                                        addobj.ComplianceID = ComplianceID;
                                        addobj.ScheduleOnID = ScheduleonID;
                                        addobj.CustId = CustId;
                                        addobj.msgid = MsgID;
                                        addobj.UserID = item;
                                        addobj.IsActive = true;
                                        addobj.Flag = false;
                                        entities.Vendor_tbl_commentDetail.Add(addobj);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            if (lstUserIdEmails.Count > 0)
                            {
                                //Getalluserlistbyids
                                List<User> userlst = UserManagement.Getalluserlistbyids(lstUserIdEmails);
                                foreach (var item1 in userlst)
                                {
                                    #region email after comment
                                    //Cont_tbl_ContractInstance contobj = (from row in entities.Cont_tbl_ContractInstance
                                    //                                     where row.IsDeleted == false
                                    //                                     && row.ID == ContractID
                                    //                                     select row).FirstOrDefault();
                                    //if (contobj != null)
                                    //{
                                    //    string username = string.Format("{0} {1}", item1.FirstName, item1.LastName);

                                    //    string contractnumber = contobj.ContractNo;
                                    //    string toEmailid = item1.Email;
                                    //    string url = string.Empty;
                                    //    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(item1.CustomerID));
                                    //    if (Urloutput != null)
                                    //    {
                                    //        url = Urloutput.URL;
                                    //    }
                                    //    else
                                    //    {
                                    //        url = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    //    }
                                    //    string message = Properties.Settings.Default.EmailCommentTemplate
                                    //    .Replace("@User", username)
                                    //    .Replace("@ContNum", contractnumber)
                                    //    .Replace("@From", Convert.ToString(url));

                                    //    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    //    try
                                    //    {
                                    //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { toEmailid }).ToList(), null, null, "Email reminder for Comment on Contract", message);
                                    //    }
                                    //    catch { }
                                    //}
                                    #endregion
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ComplianceTypeobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetVendorCommentWithUploadFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetVendorCommentWithUploadFile(int SID,int ComplianceID, int CustomerID, int userID)
        {
            try
            {
                List<CommentsVendor> Commentsans = new List<CommentsVendor>();
                List<long> obj = new List<long>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    List<long> Uobj1 = (from row in entities.Vendor_tbl_PostComment
                                        where row.PostUserID == userID
                                        && row.CustID == CustomerID
                                        && row.ComplianceID == ComplianceID
                                        && row.ScheduleOnID == SID
                                        && row.IsActive == true
                                        select row.ID).ToList();

                    List<long> Uobj2 = (from row in entities.Vendor_tbl_UserMappingComment
                                        where row.ToUserId == userID
                                           && row.CustID == CustomerID
                                        && row.ComplianceID == ComplianceID
                                        && row.ScheduleOnID == SID
                                        && row.IsActive == true
                                        select row.PostCommentID).ToList();

                    var CommentidList = Uobj1.Union(Uobj2).Distinct().ToList();

                    CommentidList = CommentidList.Distinct().ToList();

                    if (CommentidList.Count > 0)
                    {
                        foreach (var item in CommentidList)
                        {
                            var postcomment = (from row in entities.Vendor_tbl_PostComment
                                               where row.ID == item
                                                 && row.CustID == CustomerID
                                                && row.ComplianceID == ComplianceID
                                                && row.ScheduleOnID == SID
                                               && row.IsActive == true
                                               select row).FirstOrDefault();
                            if (postcomment != null)
                            {
                                Commentsans.Add(new CommentsVendor
                                {
                                    _id = Convert.ToString(postcomment.ID),
                                    complianceID = Convert.ToInt32(postcomment.ComplianceID),
                                    sodID = Convert.ToInt32(postcomment.ScheduleOnID),
                                    user_id = Convert.ToInt32(postcomment.PostUserID),
                                    customer_id = Convert.ToInt32(postcomment.CustID),
                                    comment = postcomment.Comment,
                                    created_at = postcomment.CreatedOn,
                                    updated_at = postcomment.CreatedOn,
                                    message_id = "0",
                                    user_name = postcomment.FromUserName,
                                    isfile = Convert.ToInt32(postcomment.IsFile),
                                    filepath = postcomment.FilePath,
                                    touser_id = 0
                                });

                                List<Vendor_tbl_PostSubComment> postsubcommentlst = (from row in entities.Vendor_tbl_PostSubComment
                                                                                     where row.PostCommentID == postcomment.ID
                                                                                     && row.CustID == CustomerID && row.ComplianceID == ComplianceID
                                                                                    && row.ScheduleOnID == SID
                                                                                   && row.IsActive == true
                                                                                   select row).ToList();
                                foreach (var postsubcomment in postsubcommentlst)
                                {
                                    Commentsans.Add(new CommentsVendor
                                    {
                                        _id = Convert.ToString(postsubcomment.Id),
                                        complianceID = Convert.ToInt32(postsubcomment.ComplianceID),
                                        sodID = Convert.ToInt32(postsubcomment.ScheduleOnID),
                                        user_id = Convert.ToInt32(postsubcomment.FromUserId),
                                        customer_id = Convert.ToInt32(postsubcomment.CustID),
                                        comment = postsubcomment.PostSubComment,
                                        created_at = postsubcomment.CreatedOn,
                                        updated_at = postsubcomment.CreatedOn,
                                        message_id = Convert.ToString(postcomment.ID),
                                        user_name = postsubcomment.FromUserName,
                                        isfile = Convert.ToInt32(postsubcomment.IsFile),
                                        filepath = postsubcomment.FilePath,
                                        touser_id = 0
                                    });
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Commentsans).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<CommentsContract> Commentsans = new List<CommentsContract>();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Commentsans).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/VendorCommentWithUploadFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public void VendorCommentWithUploadFile()
        {
            if (true)
            {
                int IsFile = 0;
                string FilePath = string.Empty;
                string Uids = HttpContext.Current.Request.Form["Uids"];
                string Commentdetails = HttpContext.Current.Request.Form["Comments"];
                var UId = HttpContext.Current.Request.Form["UserId"];
                int UserId = Convert.ToInt32(UId);

                var CId = HttpContext.Current.Request.Form["CustId"];
                int CustId = Convert.ToInt32(CId);

                string CompID = HttpContext.Current.Request.Form["ComplianceID"];
                int ComplianceID = Convert.ToInt32(CompID);

                string SID = HttpContext.Current.Request.Form["ScheduleonID"];
                int ScheduleonID = Convert.ToInt32(SID);

                string role = HttpContext.Current.Request.Form["Loginrole"];

                string FileLocationPath = string.Empty;
                if (UserId != null && UserId > 0 && !string.IsNullOrEmpty(role))
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        User user = UserManagement.GetByID(UserId);
                        if (user != null && user.IsDeleted == false && user.IsActive == true)
                        {
                            HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                            if (postedFile1 != null)
                            {
                                #region DOC OLD
                                string filepath = ConfigurationManager.AppSettings["VendorCommentFile"];

                                string storefolder = "VendorCommentedFile";
                                string path = @"" + filepath + "\\" + storefolder + "\\" + user.ID + "\\";

                                if (!Directory.Exists(path))
                                    Directory.CreateDirectory(path);

                                string fileName = Path.GetFileName(postedFile1.FileName);
                                postedFile1.SaveAs(path + fileName);
                                FilePath = storefolder + "/" + user.ID + "/" + fileName;
                                IsFile = 1;

                                string dbFilePath = "~/" + FilePath;

                                Stream fs = postedFile1.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                VendorDocument tempContractDocument = null;

                                tempContractDocument = new VendorDocument()
                                {
                                    ComplianceID = ComplianceID,
                                    ScheduleOnID = ScheduleonID,
                                    UserID = UserId,
                                    CustomerID = CustId,
                                    DocPath = dbFilePath,
                                    DocData = bytes,
                                    DocName = fileName,
                                    FileSize = postedFile1.ContentLength,
                                    IsfromComment = true
                                };

                                long _objTempDocumentID = VendorAuditMaster.CreateVendorDocument(tempContractDocument);
                                #endregion
                            }

                            Vendor_tbl_PostComment addPostComment = new Vendor_tbl_PostComment();
                            addPostComment.ComplianceID = ComplianceID;
                            addPostComment.ScheduleOnID = ScheduleonID;
                            addPostComment.PostUserID = UserId;
                            addPostComment.CustID = CustId;
                            addPostComment.Comment = Commentdetails;
                            addPostComment.IsFile = IsFile;
                            addPostComment.FilePath = FilePath;
                            addPostComment.CreatedBy = UserId;
                            addPostComment.CreatedOn = DateTime.Now;
                            addPostComment.IsActive = true;
                            addPostComment.FromUserName = user.FirstName + "" + user.LastName;
                            entities.Vendor_tbl_PostComment.Add(addPostComment);
                            entities.SaveChanges();

                            long PostCommentId = addPostComment.ID;

                            if (PostCommentId > 0)
                            {
                                List<long> TagIds = new List<long>();
                                if (role == "VCCON")
                                {
                                    TagIds.Clear();
                                    long UserID = (from row in entities.Vendor_ScheduleOn
                                                   join row1 in entities.Vendor_ContractorProjectMappingAssignment
                                                   on row.ContractProjectMappingID equals row1.ContractorProjectMappingID
                                                   where row.ID == ScheduleonID && row.IsActive == true
                                                   select row1.UserID).FirstOrDefault();
                                    if (UserID > 0)
                                        TagIds.Add(UserID);
                                }
                                else if (role == "VCAUD")
                                {
                                    List<long> userIds = new List<long>();
                                    var details = (from row in entities.Vendor_ScheduleOn
                                                   join row1 in entities.Vendor_ContractorProjectMapping
                                                   on row.ContractProjectMappingID equals row1.ID
                                                   where row.ID == ScheduleonID && row.IsActive == true
                                                   select row1).FirstOrDefault();

                                    if (details.ContractorID != null)
                                        userIds.Add(details.ContractorID);

                                    if (details.UnderContractorID != null)
                                        userIds.Add((long)details.UnderContractorID);

                                    TagIds.Clear();
                                    TagIds = (from row in entities.Vendor_ContractorMaster
                                              where userIds.Contains(row.ID)
                                              select (long)row.AvacomUserID).ToList();
                                }
                                else
                                {
                                    TagIds.Clear();
                                }
                                
                                foreach (var item in TagIds)
                                {
                                    Vendor_tbl_UserMappingComment addusermap = new Vendor_tbl_UserMappingComment();
                                    addusermap.PostCommentID = PostCommentId;
                                    addusermap.CustID = CustId;
                                    addusermap.ComplianceID = ComplianceID;
                                    addusermap.ScheduleOnID = ScheduleonID;
                                    addusermap.ToUserId = item;
                                    addusermap.IsActive = true;
                                    entities.Vendor_tbl_UserMappingComment.Add(addusermap);
                                    entities.SaveChanges();

                                    Vendor_tbl_commentDetail addobj = new Vendor_tbl_commentDetail();
                                    addobj.ComplianceID = ComplianceID;
                                    addobj.ScheduleOnID = ScheduleonID;
                                    addobj.CustId = CustId;
                                    addobj.msgid = Convert.ToString(PostCommentId);
                                    addobj.UserID = item;
                                    addobj.IsActive = true;
                                    addobj.Flag = false;
                                    entities.Vendor_tbl_commentDetail.Add(addobj);
                                    entities.SaveChanges();

                                    #region Comment to mail
                                    User user1 = UserManagement.GetByID(Convert.ToInt32(item));

                                    if (user1 != null && user1.IsDeleted == false && user1.IsActive == true)
                                    {
                                        //Cont_tbl_ContractInstance contobj = (from row in entities.Cont_tbl_ContractInstance
                                        //                                     where row.IsDeleted == false
                                        //                                     && row.ID == ContractID
                                        //                                     select row).FirstOrDefault();
                                        //if (contobj != null)
                                        //{
                                        //    string username = string.Format("{0} {1}", user1.FirstName, user1.LastName);

                                        //    string contractnumber = contobj.ContractNo;
                                        //    string toEmailid = user1.Email;
                                        //    string url = string.Empty;
                                        //    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user1.CustomerID));
                                        //    if (Urloutput != null)
                                        //    {
                                        //        url = Urloutput.URL;
                                        //    }
                                        //    else
                                        //    {
                                        //        url = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                        //    }
                                        //    string message = Properties.Settings.Default.EmailCommentTemplate
                                        //    .Replace("@User", username)
                                        //    .Replace("@ContNum", contractnumber)
                                        //    .Replace("@From", Convert.ToString(url));

                                        //    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                        //    try
                                        //    {
                                        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { toEmailid }).ToList(), null, null, "Email reminder for Comment on Contract", message);
                                        //    }
                                        //    catch { }
                                        //}
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }
        
        [Route("VendorAudit/GetUSerDetailbySearch")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetUSerDetailbySearch(int customerID,int UID, int SID, int CompID, string search)
        {
            try
            {
                List<Vendor_SP_GetCommentUser_Result> UserDetailobj = new List<Vendor_SP_GetCommentUser_Result>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    UserDetailobj = entities.Vendor_SP_GetCommentUser(Convert.ToInt32(customerID), Convert.ToInt32(UID), Convert.ToInt32(SID), Convert.ToInt32(CompID), search).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UserDetailobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindPeriodWiseComplianceStatusNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindPeriodWiseComplianceStatusNew(int CID, int UID, string role, bool SubContractor, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusWithRisk> objResult = new List<ComplianceStatusWithRisk>();
            List<Vendor_Sp_GetContractDashboard_Result> output = new List<Vendor_Sp_GetContractDashboard_Result>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractDashboard(CID)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    if (role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();
                    }
                    else if (role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();
                    }
                    else if (role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    else
                    {
                        if (role.Equals("VCCON") && SubContractor)
                        {
                            output = output.Where(x => x.ProjectSubContSpocID == UID && x.IsShowContractor == 1).ToList();
                        }
                        else if (role.Equals("VCCON") && !SubContractor)
                        {
                            output = output.Where(x => (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1) || x.ProjectContSpocID == UID).ToList();
                        }
                        else if (role.Equals("VCAUD"))
                        {
                            output = output.Where(x => x.AuditorID == UID).ToList();
                        }
                        else
                        {
                            output.Clear();
                        }
                    }
                }

                int Complied = output.Where(x => x.ComplianceStatusID == 1).ToList().Count;
                int CompliedWithHighRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High").ToList().Count;
                int CompliedWithMediumRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium").ToList().Count;
                int CompliedWithLowRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low").ToList().Count;

                int NotComplied = output.Where(x => x.ComplianceStatusID == 2).ToList().Count;
                int NotCompliedWithHighRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High").ToList().Count;
                int NotCompliedWithMediumRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium").ToList().Count;
                int NotCompliedWithLowRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low").ToList().Count;


                int notApplicable = output.Where(x => x.ComplianceStatusID == 3).ToList().Count;
                int notApplicableWithHighRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High").ToList().Count;
                int notApplicableWithMediumRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium").ToList().Count;
                int notApplicableWithLowRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low").ToList().Count;

                objResult.Clear();
                objResult.Add(new ComplianceStatusWithRisk
                {
                    Complied = Complied,
                    CompliedHigh = CompliedWithHighRisk,
                    CompliedMedium = CompliedWithMediumRisk,
                    CompliedLow = CompliedWithLowRisk,
                    NotComplied = NotComplied,
                    NotCompliedHigh = NotCompliedWithHighRisk,
                    NotCompliedMedium = NotCompliedWithMediumRisk,
                    NotCompliedLow = NotCompliedWithLowRisk,
                    NotApplicable = notApplicable,
                    NotApplicableHigh = notApplicableWithHighRisk,
                    NotApplicableMedium = notApplicableWithMediumRisk,
                    NotApplicableLow = notApplicableWithLowRisk,
                    PeriodStartDate = FromFinancialYearSummary,
                    PeriodEndDate = ToFinancialYearSummary,
                });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusWithRisk
                {
                    Complied = 0,
                    CompliedHigh = 0,
                    CompliedMedium = 0,
                    CompliedLow = 0,
                    NotComplied = 0,
                    NotCompliedHigh = 0,
                    NotCompliedMedium = 0,
                    NotCompliedLow = 0,
                    NotApplicable = 0,
                    NotApplicableHigh = 0,
                    NotApplicableMedium = 0,
                    NotApplicableLow = 0
                });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/BindPeriodWiseComplianceStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindPeriodWiseComplianceStatus(int CID, int UID, string role, bool SubContractor, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusWithRisk> objResult = new List<ComplianceStatusWithRisk>();
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    if (role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();
                    }
                    else if (role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();
                    }
                    else if (role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    else
                    {
                        if (role.Equals("VCCON") && SubContractor)
                        {
                            output = output.Where(x => x.ProjectSubContSpocID == UID && x.IsShowContractor == 1).ToList();
                        }
                        else if (role.Equals("VCCON") && !SubContractor)
                        {
                            output = output.Where(x => (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1) || x.ProjectContSpocID == UID).ToList();
                        }
                        else if (role.Equals("VCAUD"))
                        {
                            output = output.Where(x => x.AuditorID == UID).ToList();
                        }
                        else
                        {
                            output.Clear();
                        }
                    }
                }

                int Complied = output.Where(x => x.ComplianceStatusID == 1).ToList().Count;
                int CompliedWithHighRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High").ToList().Count;
                int CompliedWithMediumRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium").ToList().Count;
                int CompliedWithLowRisk = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low").ToList().Count;

                int NotComplied = output.Where(x => x.ComplianceStatusID == 2).ToList().Count;
                int NotCompliedWithHighRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High").ToList().Count;
                int NotCompliedWithMediumRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium").ToList().Count;
                int NotCompliedWithLowRisk = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low").ToList().Count;


                int notApplicable = output.Where(x => x.ComplianceStatusID == 3).ToList().Count;
                int notApplicableWithHighRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High").ToList().Count;
                int notApplicableWithMediumRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium").ToList().Count;
                int notApplicableWithLowRisk = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low").ToList().Count;

                objResult.Clear();
                objResult.Add(new ComplianceStatusWithRisk
                {
                    Complied = Complied,
                    CompliedHigh = CompliedWithHighRisk,
                    CompliedMedium = CompliedWithMediumRisk,
                    CompliedLow = CompliedWithLowRisk,
                    NotComplied = NotComplied,
                    NotCompliedHigh = NotCompliedWithHighRisk,
                    NotCompliedMedium = NotCompliedWithMediumRisk,
                    NotCompliedLow = NotCompliedWithLowRisk,
                    NotApplicable = notApplicable,
                    NotApplicableHigh = notApplicableWithHighRisk,
                    NotApplicableMedium = notApplicableWithMediumRisk,
                    NotApplicableLow = notApplicableWithLowRisk,
                    PeriodStartDate = FromFinancialYearSummary,
                    PeriodEndDate = ToFinancialYearSummary,
                });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusWithRisk
                {
                    Complied = 0,
                    CompliedHigh = 0,
                    CompliedMedium = 0,
                    CompliedLow = 0,
                    NotComplied = 0,
                    NotCompliedHigh = 0,
                    NotCompliedMedium = 0,
                    NotCompliedLow = 0,
                    NotApplicable = 0,
                    NotApplicableHigh = 0,
                    NotApplicableMedium = 0,
                    NotApplicableLow = 0
                });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ComplianceDocumentforreview")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceDocumentforreview(int CustID, long ProjectContractMappingID, int scheduleonID, int ComplianceID)
        {
            List<Vendor_tbl_FileTransactionData> objResult = new List<Vendor_tbl_FileTransactionData>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    objResult = (from row in entities.Vendor_tbl_FileTransactionData
                                 where row.ContractProjectMappingId == ProjectContractMappingID
                                 && row.CustomerID == CustID
                                 && row.ScheduleOnId == scheduleonID
                                 && row.ComplianceId == ComplianceID
                                 && row.IsDeleted == false
                                 select row).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/DeletePerDoc")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DeletePerDoc(string FileID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(FileID))
                {
                    bool result = VendorAuditMaster.Delete_PerDoc(Convert.ToInt32(FileID));
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ComplianceAuditLog")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ComplianceAuditLog(int CustID, long ProjectContractMappingID, int scheduleonID, int ComplianceID,int ContractProjMappingStepMappingID)
        {
            List<Vendor_ContractMappingComplianceTransaction> objResult = new List<Vendor_ContractMappingComplianceTransaction>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    objResult = (from row in entities.Vendor_ContractMappingComplianceTransaction
                                 where row.ContractProjectMappingId == ProjectContractMappingID
                                 && row.ComplianceScheduleOnID == scheduleonID
                                 && row.ComplianceID == ComplianceID
                                 && row.ContractProjectMappingId == ProjectContractMappingID
                                 && row.ContractProjMappingStepMappingID == ContractProjMappingStepMappingID
                                 select row).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetCompliancesForVendorAsSchedule")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCompliancesForVendorAsSchedule(int CustID, long ProjectContractMappingID, int scheduleonID, int ComplianceID)
        {
            List<Vendor_CompMasterValueDetail> objResult = new List<Vendor_CompMasterValueDetail>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    objResult = (from row in entities.Vendor_CompMasterValueDetail
                                 where row.ContractProjectMappingId == ProjectContractMappingID
                                 && row.CustomerID == CustID
                                 && row.ScheduleOnId == scheduleonID
                                 && row.ComplianceId == ComplianceID
                                 select row).ToList();

                    if (objResult.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(objResult[0].ParameterName))
                        {

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/DocumentUploadCompliancePerformerRevData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DocumentUploadCompliancePerformerRevData(MultipartDataMediaFormatter.Infrastructure.FormData files, int UserId, int CustId, int ContractProjectMappingId, int ScheduleOnId, int ComplianceId)
        {
            string Result = "";
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath = string.Empty;

                    Vendor_tbl_FileTransactionData objIC = new Vendor_tbl_FileTransactionData()
                    {
                        ContractProjectMappingId = ContractProjectMappingId,
                        CustomerID = CustId,
                        ScheduleOnId = ScheduleOnId,
                        ComplianceId = ComplianceId,
                        Version = "1.0",
                        CreatedBy = UserId,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = UserId,
                        UpdatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    objIC.FileName = files.Files[0].Value.FileName;

                    var caseDocVersion = VendorAuditMaster.ExistsDocumentReturnVersion(objIC);

                    caseDocVersion++;
                    objIC.Version = caseDocVersion + ".0";
                    var url = ConfigurationManager.AppSettings["VendorDocument"];

                    directoryPath = url + "/Vendor/" + ContractProjectMappingId + "/" + ScheduleOnId + "/" + ComplianceId + "/" + objIC.Version;

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    Guid fileKey = Guid.NewGuid();
                    string finalPath = System.IO.Path.Combine(directoryPath, fileKey + System.IO.Path.GetExtension(files.Files[0].Value.FileName));

                    Byte[] bytes = files.Files[0].Value.Buffer;

                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    objIC.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                    objIC.CreatedOn = DateTime.Now;
                    objIC.FileSize = Convert.ToInt64(100);
                    objIC.FileKey = fileKey.ToString();
                    SaveDocFiles(fileList);
                    long FileID = VendorAuditMaster.CreateContractorDocument(objIC);
                    if (FileID != 0)
                    {
                        Result = "Successfully Added";
                    }
                    fileList.Clear();
                }

                string jsonstring = JsonConvert.SerializeObject(Result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
     
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(System.IO.File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }

        [Route("VendorAudit/GetLocationListrolewise")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLocationListrolewise(int UID,int customerId,string role)///, int userId, string Flag, string IsStatutoryInternal
        {
            try
            {
                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var output = VendorAuditMaster.GetDashboradCount(UID, customerId, role, false);
                    List<int> locations = output.Select(x => x.LocationID).Distinct().ToList();

                    var LocationList = CustomerBranchManagement.GetAssignedLocationListVendorAuditRahul(customerId);

                    LocationList = LocationList.Where(x => locations.Contains(x)).ToList();

                    var branches = BranchClass.GetAllHierarchyVendor(customerId, LocationList);
                    CFODashboardGraphObj.Add(new CFODashboardGraph
                    {
                        locationList = branches
                    });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GetLocationList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLocationList(int customerId)///, int userId, string Flag, string IsStatutoryInternal
        {
            try
            {
                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LocationList = CustomerBranchManagement.GetAssignedLocationListVendorAuditRahul(customerId);
                    var branches = BranchClass.GetAllHierarchyVendor(customerId, LocationList);
                    CFODashboardGraphObj.Add(new CFODashboardGraph
                    {
                        locationList = branches
                    });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ProjectMasterMGMT")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMasterMGMT(int CID, int UID,string role)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {                
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, role, false);

                List<int> Projectlst = output.Select(x => x.ProjectID).ToList();

                objResult = VendorAuditMaster.GetProjectMaster(CID);

                objResult = objResult.Where(x => Projectlst.Contains(x.ID)).ToList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindContractorVendorUserMGMT")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindContractorVendorUserMGMT(int CID, int UID, string role)
        {
            List<Vendor_SP_GetContractorUserMasterList_Result> objResult = new List<Vendor_SP_GetContractorUserMasterList_Result>();

            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, role, false);

                List<int> lst = output.Select(x => x.ContractorID).ToList();

                objResult = VendorAuditMaster.GetContractorUserList(CID);

                objResult = objResult.Where(x => lst.Contains(x.ID)).ToList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/BindLocationMGMT")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindLocationMGMT(int CID, int UID, string role)
        {
            List<Vendor_Sp_Mgmt_TopLocations_Result> objResult = new List<Vendor_Sp_Mgmt_TopLocations_Result>();

            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, role, false);

                List<int> locationlist = output.Select(x => x.LocationID).Distinct().ToList();

                List<int> finallocationIds = new List<int>();
               

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    finallocationIds.Clear();
                    foreach (var item in locationlist)
                    {
                        List<int> outputlist = (from row in entities.Vendor_Sp_GetParentBranchesFromChildNode(item, CID)
                                                select (int)row.ID).ToList();
                        foreach (var x in outputlist)
                        {
                            finallocationIds.Add(x);
                        }
                    }
                    finallocationIds = finallocationIds.Distinct().ToList();



                    entities.Configuration.LazyLoadingEnabled = false;

                    objResult = (from row in entities.Vendor_Sp_Mgmt_TopLocations(CID)
                                     select row).ToList();

                    objResult = objResult.Where(x => finallocationIds.Contains(x.ID)).ToList();
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindCountMGMTcomplincespercentage")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindCountMGMTcomplincespercentage(int UID, int CID, string Role, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<MGMTDashboradCount> objResult = new List<MGMTDashboradCount>();
            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, false);

                int Project = output.GroupBy(x => x.ProjectID).Distinct().ToList().Count;
                int Contractors = output.GroupBy(x => x.ContractorID).Distinct().ToList().Count;
                int Locations = output.GroupBy(x => x.LocationID).Distinct().ToList().Count;
                int Finalcompletion = 0;
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        List<int> projectids = output.Select(x => x.ProjectID).ToList();

                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListCount(CID, -1, -1)
                                              select row).Where(x => projectids.Contains(x.ProjectID)).ToList();

                        #region filter

                        DateTime FromFinancialYearSummary = DateTime.Now;
                        DateTime ToFinancialYearSummary = DateTime.Now;

                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                        }

                        if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                        {
                            System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                            if (ProjectIDs.Count != 0)
                            {
                                complianceList = complianceList.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                            }
                        }
                        if (Location != "[]" && !string.IsNullOrEmpty(Location))
                        {
                            System.Collections.Generic.IList<int?> Locations1 = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                            if (Locations1.Count != 0)
                            {
                                complianceList = complianceList.Where(x => Locations1.Contains(x.LocationID)).ToList();
                            }
                        }

                        if (!string.IsNullOrEmpty(Period))
                        {

                            int period = Convert.ToInt32(Period);

                            if (period == 0) //Current YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 1) //Current + Previous YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-2);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 2) //All
                            {
                                string dtfrom = Convert.ToString("01-01-1900");
                                string dtto = Convert.ToString("01-01-1900");
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else if (period == 3)// Last Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 4)//Last 3 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 5)//Last 6 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 6)//This Month
                            {
                                DateTime now = DateTime.Now;
                                FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                                ToFinancialYearSummary = DateTime.Now;
                            }

                            if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                            {
                                if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                                {
                                    complianceList = complianceList.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                                }
                            }

                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                complianceList = (from row in complianceList
                                                  where row.ScheduleOn >= FromFinancialYearSummary
                                          && row.ScheduleOn <= ToFinancialYearSummary
                                          select row).ToList();
                            }
                        }

                        #endregion

                        try {
                            var compliedList = (from row in complianceList
                                                select row).Where(x => x.ComplianceStatusID == 1 && x.AuditCheckListStatusID == 4).ToList();

                            var noncompliedList = (from row in complianceList
                                                   select row).Where(x => x.ComplianceStatusID == 2 && x.AuditCheckListStatusID == 4).ToList();


                            int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                            Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;
                        }//no need add overdue
                        catch (Exception e)
                        {
                        }
                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListCount(CID, -1, -1)
                                              select row).Where(x => x.ProjectDirector == UID).ToList();

                        #region filter

                        DateTime FromFinancialYearSummary = DateTime.Now;
                        DateTime ToFinancialYearSummary = DateTime.Now;

                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                        }

                        if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                        {
                            System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                            if (ProjectIDs.Count != 0)
                            {
                                complianceList = complianceList.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                            }
                        }
                        if (Location != "[]" && !string.IsNullOrEmpty(Location))
                        {
                            System.Collections.Generic.IList<int?> Locations1 = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                            if (Locations1.Count != 0)
                            {
                                complianceList = complianceList.Where(x => Locations1.Contains(x.LocationID)).ToList();
                            }
                        }

                        if (!string.IsNullOrEmpty(Period))
                        {

                            int period = Convert.ToInt32(Period);

                            if (period == 0) //Current YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 1) //Current + Previous YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-2);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 2) //All
                            {
                                string dtfrom = Convert.ToString("01-01-1900");
                                string dtto = Convert.ToString("01-01-1900");
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else if (period == 3)// Last Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 4)//Last 3 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 5)//Last 6 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 6)//This Month
                            {
                                DateTime now = DateTime.Now;
                                FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                                ToFinancialYearSummary = DateTime.Now;
                            }

                            if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                            {
                                if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                                {
                                    complianceList = complianceList.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                                }
                            }

                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                complianceList = (from row in complianceList
                                                  where row.ScheduleOn >= FromFinancialYearSummary
                                          && row.ScheduleOn <= ToFinancialYearSummary
                                                  select row).ToList();
                            }
                        }

                        #endregion

                        try
                        {
                            var compliedList = (from row in complianceList
                                                select row).Where(x => x.ComplianceStatusID == 1 && x.AuditCheckListStatusID == 4).ToList();

                            var noncompliedList = (from row in complianceList
                                                   select row).Where(x => x.ComplianceStatusID == 2 && x.AuditCheckListStatusID == 4).ToList();


                            int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                            Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListCount(CID, -1, -1)
                                              select row).Where(x => x.ProjectHead == UID).ToList();

                        #region filter

                        DateTime FromFinancialYearSummary = DateTime.Now;
                        DateTime ToFinancialYearSummary = DateTime.Now;

                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            complianceList = complianceList.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                        }

                        if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                        {
                            System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                            if (ProjectIDs.Count != 0)
                            {
                                complianceList = complianceList.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                            }
                        }
                        if (Location != "[]" && !string.IsNullOrEmpty(Location))
                        {
                            System.Collections.Generic.IList<int?> Locations1 = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                            if (Locations1.Count != 0)
                            {
                                complianceList = complianceList.Where(x => Locations1.Contains(x.LocationID)).ToList();
                            }
                        }

                        if (!string.IsNullOrEmpty(Period))
                        {

                            int period = Convert.ToInt32(Period);

                            if (period == 0) //Current YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 1) //Current + Previous YTD
                            {
                                if (DateTime.Today.Month > 3)
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-1);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    DateTime dtprev = DateTime.Now.AddYears(-2);
                                    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                    FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                }
                            }
                            else if (period == 2) //All
                            {
                                string dtfrom = Convert.ToString("01-01-1900");
                                string dtto = Convert.ToString("01-01-1900");
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else if (period == 3)// Last Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 4)//Last 3 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 5)//Last 6 Month
                            {
                                FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                                ToFinancialYearSummary = DateTime.Now;
                            }
                            else if (period == 6)//This Month
                            {
                                DateTime now = DateTime.Now;
                                FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                                ToFinancialYearSummary = DateTime.Now;
                            }

                            if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                            {
                                if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                                {
                                    complianceList = complianceList.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                                }
                            }

                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                complianceList = (from row in complianceList
                                                  where row.ScheduleOn >= FromFinancialYearSummary
                                          && row.ScheduleOn <= ToFinancialYearSummary
                                                  select row).ToList();
                            }
                        }

                        #endregion

                        try
                        {
                            var compliedList = (from row in complianceList
                                                select row).Where(x => x.ComplianceStatusID == 1 && x.AuditCheckListStatusID == 4).ToList();

                            var noncompliedList = (from row in complianceList
                                                   select row).Where(x => x.ComplianceStatusID == 2 && x.AuditCheckListStatusID == 4).ToList();

                            int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                            Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;

                        }
                        catch (Exception e)
                        {
                        }
                    }

                    objResult.Clear();
                    objResult.Add(new MGMTDashboradCount { Location = Locations, Project = Project, Contractors = Contractors, Completion = Finalcompletion });
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new MGMTDashboradCount { Location = 0, Project = 0, Contractors = 0, Completion = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/BindCountMGMT")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindCountMGMT(int UID, int CID, string Role)
        {
            List<MGMTDashboradCount> objResult = new List<MGMTDashboradCount>();
            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, false);
                
                int Project = output.GroupBy(x => x.ProjectID).Distinct().ToList().Count;
                int Contractors = output.GroupBy(x => x.ContractorID).Distinct().ToList().Count;
                int Locations = output.GroupBy(x => x.LocationID).Distinct().ToList().Count;
                int Finalcompletion = 0;
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        List<int> projectids = output.Select(x => x.ProjectID).ToList();

                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                              select row).Where(x => projectids.Contains(x.ProjectID)).ToList();
                        
                        var compliedList = (from row in complianceList
                                            select row).Where(x => x.ComplianceReviewerStatus == "Complied").ToList();

                        var noncompliedList = (from row in complianceList
                                            select row).Where(x => x.ComplianceReviewerStatus == "Not Complied").ToList();


                        int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;
                                              
                        Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;

                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                              select row).Where(x => x.ProjectDirector == UID).ToList();

                        var compliedList = (from row in complianceList
                                            select row).Where(x => x.ComplianceReviewerStatus == "Complied").ToList();

                        var noncompliedList = (from row in complianceList
                                               select row).Where(x => x.ComplianceReviewerStatus == "Not Complied").ToList();


                        int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                        Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;
                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        var complianceList = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                              select row).Where(x => x.ProjectHead == UID).ToList();

                        var compliedList = (from row in complianceList
                                            select row).Where(x => x.ComplianceReviewerStatus == "Complied").ToList();

                        var noncompliedList = (from row in complianceList
                                               select row).Where(x => x.ComplianceReviewerStatus == "Not Complied").ToList();


                        int totalCompliednoncomplied = compliedList.Count + noncompliedList.Count;

                        Finalcompletion = (compliedList.Count * 100) / totalCompliednoncomplied;// (compliedList.Count / complianceList.Count) * 100;
                    }

                    objResult.Clear();
                    objResult.Add(new MGMTDashboradCount { Location = Locations, Project = Project, Contractors = Contractors, Completion = Finalcompletion });
                }
               
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new MGMTDashboradCount { Location = 0, Project = 0, Contractors = 0, Completion = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindCount(int UID, int CID, string Role, bool SubContractor, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<DashboradCount> objResult = new List<DashboradCount>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;
            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, SubContractor);

                if (!string.IsNullOrEmpty(StartDateDetail))
                {
                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                }
                if (!string.IsNullOrEmpty(EndDateDetail))
                {
                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                }

                if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                {
                    System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                    if (ProjectIDs.Count != 0)
                    {
                        output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                    }
                }
                if (Location != "[]" && !string.IsNullOrEmpty(Location))
                {
                    System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                    if (Locations.Count != 0)
                    {
                        output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(Period))
                {

                    int period = Convert.ToInt32(Period);

                    if (period == 0) //Current YTD
                    {
                        if (DateTime.Today.Month > 3)
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-1);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }
                    else if (period == 1) //Current + Previous YTD
                    {
                        if (DateTime.Today.Month > 3)
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-1);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            DateTime dtprev = DateTime.Now.AddYears(-2);
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }
                    else if (period == 2) //All
                    {
                        string dtfrom = Convert.ToString("01-01-1900");
                        string dtto = Convert.ToString("01-01-1900");
                        FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else if (period == 3)// Last Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 4)//Last 3 Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 5)//Last 6 Month
                    {
                        FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                        ToFinancialYearSummary = DateTime.Now;
                    }
                    else if (period == 6)//This Month
                    {
                        DateTime now = DateTime.Now;
                        FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                        ToFinancialYearSummary = DateTime.Now;
                    }

                    if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                    {
                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                        }
                    }

                    if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                    {
                        output = (from row in output
                                  where row.ScheduleOn >= FromFinancialYearSummary
                                  && row.ScheduleOn <= ToFinancialYearSummary
                                  select row).ToList();
                    }
                }

                int Open = output.Where(x => x.Status == "Upcoming").ToList().Count;
                int Overdue = output.Where(x => x.Status == "Overdue").ToList().Count;
                int Underreview = output.Where(x => x.Status == "Pending Review").ToList().Count;//"Underreview"
                int Closed = output.Where(x => x.Status == "Closed").ToList().Count;
                DateTime PeriodStartDate = FromFinancialYearSummary;
                DateTime PeriodEndDate = ToFinancialYearSummary;


                objResult.Clear();
                objResult.Add(new DashboradCount { Open = Open, Overdue = Overdue, Underreview = Underreview, Closed = Closed, PeriodStartDate = PeriodStartDate, PeriodEndDate = PeriodEndDate });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new DashboradCount { Open = 0, Overdue = 0, Underreview = 0, Closed = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/MultipleUpdateSchedule")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MultipleUpdateSchedule(List<updateScheduleDetail> things)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                if (things.Count > 0)
                {
                    foreach (var item in things)
                    {
                        int freqint = 0;
                        byte frqqq = 0;
                        if (item.Frequency == "M")
                        {
                            freqint = 0;
                            frqqq = 0;
                        }
                        if (item.Frequency == "Q")
                        {
                            freqint = 1;
                            frqqq = 1;
                        }
                        if (item.Frequency == "H")
                        {
                            freqint = 2;
                            frqqq = 2;
                        }
                        if (item.Frequency == "Y")
                        {
                            freqint = 3;
                            frqqq = 0;
                        }

                        DateTime x = DateTime.ParseExact(item.activationdate, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        //DateTime x = Convert.ToDateTime(item.activationdate);

                        string UserName = VendorAuditMaster.GetUserNameDetails(item.CreatedBy, item.CustID);

                        if (item.Frequency == "O")
                        {
                            VendorAuditMaster.CreateOneTimeScheduleOn(x, item.ID, item.Frequency, item.CreatedBy, UserName, item.ContractorID, item.ProjectID);                            
                        }
                        else
                        {
                            VendorAuditMaster.CreateScheduleOn(x, item.ID, item.Frequency, item.CreatedBy, UserName, item.ContractorID, item.ProjectID, freqint, frqqq);
                        }
                    }
                }
                UpdateNotificationObj.Add(new UpdateNotification { Message = "Save" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/CompliancesForVendorAsSchedule")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CompliancesForVendorAsSchedule(int CustID, long ProjectContractMappingID, int scheduleonID)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, ProjectContractMappingID, scheduleonID)
                                 select row).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        [Route("VendorAudit/MultipleUpdateAssignment")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MultipleUpdateAssignment(List<updateAssignmentDetail> things)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                if (things.Count > 0)
                {
                    foreach (var item in things)
                    {
                        VendorAuditMaster.UpdateAssignedAuditor(item.CreatedBy, item.CustID, (int)item.assignmentID, item.ID);
                    }
                }
                UpdateNotificationObj.Add(new UpdateNotification { Message = "Save" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/BindFrequencyschedule")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindFrequencyschedule(string value)
        {
            List<Vendor_Frequencyschedule> objResult = new List<Vendor_Frequencyschedule>();

            try
            {
                objResult = VendorAuditMaster.GetFrequencyschedule(value);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindYear")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindYear()
        {
            List<Vendor_Year> objResult = new List<Vendor_Year>();

            try
            {
                objResult = VendorAuditMaster.GetYearList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindMGMTUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindMGMTUser(int CID)
        {
            List<UserDetails> objResult = new List<UserDetails>();

            try
            {
                objResult = VendorAuditMaster.GetMGMTUserList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/BindAuditorUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindAuditorUser(int CID)
        {
            List<UserDetails> objResult = new List<UserDetails>();

            try
            {
                objResult = VendorAuditMaster.GetAuditorUserList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/AssignmentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AssignmentList(int CID)
        {
            List<Vendor_Sp_GetAllAssignmentList_Result> objResult = new List<Vendor_Sp_GetAllAssignmentList_Result>();
            try
            {
                objResult = VendorAuditMaster.AllAssignmentList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/UnAssignmentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UnAssignmentList(int CID)
        {
            List<Vendor_Sp_GetAllUnAssignmentList_Result> objResult = new List<Vendor_Sp_GetAllUnAssignmentList_Result>();
            try
            {
                objResult = VendorAuditMaster.AllUnAssignmentList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ScheduledList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ScheduledList(int CID)
        {
            List<Vendor_Sp_GetAllScheduleList_Result> objResult = new List<Vendor_Sp_GetAllScheduleList_Result>();
            try
            {
                objResult = VendorAuditMaster.AllScheduleList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetComplianceDetailsWithParameter")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetComplianceDetailsWithParameter(int CustID, long ProjectContractMappingID, int scheduleonID, int ComplianceID)
        {
            try
            {
                List<CombinedResult> finalResult = new List<CombinedResult>();
                List<long> CMResult = new List<long>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var ObjCM = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, ProjectContractMappingID, scheduleonID)
                                 where row.ComplianceID == ComplianceID
                                 select row).ToList();

                    foreach (var x in ObjCM)
                    {
                        CMResult.Add(x.ComplianceID);
                    }

                    var ObjPM = (from row in entities.Vendor_CompMasterParameter
                                 where CMResult.Contains(row.ComplianceId)
                                 && row.IsDeleted == false
                                 select row).ToList();

                    if (ObjCM.Count > 0)
                    {
                        if (ObjPM.Count > 0)
                        {
                            foreach (var item in ObjCM)
                            {
                                foreach (var y in ObjPM)
                                {
                                    if (item.ComplianceID == y.ComplianceId)
                                    {
                                        finalResult.Add(new CombinedResult
                                        {
                                            ID = item.ID,
                                            ComplianceID = item.ComplianceID,
                                            ComplainceHeader = item.ComplainceHeader,
                                            Statutory = item.Statutory,
                                            RiskCategory = item.RiskCategory,
                                            ActName = item.ActName,
                                            IsDocument = item.IsDocument,
                                            ParameterName = y.ParameterName,
                                            ParameterType = y.ParameterType,
                                            ContractProjMappingID = item.ContractProjMappingID,
                                            ScheduleOnID = item.ContractProjMappingScheduleOnID
                                        });
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in ObjCM)
                            {
                                finalResult.Add(new CombinedResult
                                {
                                    ID = item.ID,
                                    ComplianceID = item.ComplianceID,
                                    ComplainceHeader = item.ComplainceHeader,
                                    Statutory = item.Statutory,
                                    RiskCategory = item.RiskCategory,
                                    ActName = item.ActName,
                                    IsDocument = item.IsDocument,
                                    ParameterName = "",
                                    ParameterType = "",
                                    ContractProjMappingID = item.ContractProjMappingID,
                                    ScheduleOnID = item.ContractProjMappingScheduleOnID,                                   
                                });

                            }
                        }
                    }


                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(finalResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/UnScheduledList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UnScheduledList(int CID)
        {
            List<Vendor_Sp_GetAllUnScheduleList_Result> objResult = new List<Vendor_Sp_GetAllUnScheduleList_Result>();
            try
            {
                objResult = VendorAuditMaster.AllUnScheduleList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/Delete_ComplianceMappingByID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_ComplianceMappingByID(int ID)
        {

            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (ID != 0)
                {
                    bool result = false;

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        var queryResult = (from row in entities.Vendor_ComplianceMapping
                                           where row.ID == ID
                                           select row).FirstOrDefault();
                        if (queryResult != null)
                        {
                            queryResult.IsDeleted = true;
                            entities.SaveChanges();
                            result = true;
                        }
                        else
                            result = false;
                    }

                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ProjectMasterFilterRecordRoleWise")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMasterFilterRecordRoleWise(int UID,int CID, string LocationIDs,string role)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {
                var output = VendorAuditMaster.GetDashboradCount(UID, CID, role, false);

                List<int> Projectlist = output.Select(x => x.ProjectID).Distinct().ToList();
                if (Projectlist.Count > 0)
                {
                    objResult = VendorAuditMaster.GetProjectMaster(CID);
                    objResult = objResult.Where(x => x.Isdisable == false || x.Isdisable == null).ToList();
                    objResult = objResult.Where(x => Projectlist.Contains(x.ID)).ToList();

                    if (LocationIDs != "[]" && !string.IsNullOrEmpty(LocationIDs))
                    {
                        System.Collections.Generic.IList<int?> LocationID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(LocationIDs);
                        if (LocationID.Count != 0)
                        {
                            objResult = objResult.Where(x => LocationID.Contains(x.LocationID)).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectMasterFilterRecord")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMasterFilterRecord(int CID, string LocationIDs)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectMaster(CID);
                objResult = objResult.Where(x => x.Isdisable == false || x.Isdisable == null).ToList();
                if (LocationIDs != "[]" && !string.IsNullOrEmpty(LocationIDs))
                {
                    System.Collections.Generic.IList<int?> LocationID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(LocationIDs);
                    if (LocationID.Count != 0)
                    {
                        objResult = objResult.Where(x => LocationID.Contains(x.LocationID)).ToList();
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/ContractTypeForFilterAssingmentNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTypeForFilterAssingmentNew(int CID, string ProjectIDs, string ContractorTypeIDs)
        {
            List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();
            try
            {
                objResult = VendorAuditMaster.GetContractTypeForFilterAssingment(CID);

                if (ProjectIDs != "[]" && !string.IsNullOrEmpty(ProjectIDs))
                {
                    System.Collections.Generic.IList<int?> ProjectID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(ProjectIDs);
                    if (ProjectID.Count != 0)
                    {
                        objResult = objResult.Where(x => ProjectID.Contains(x.ProjectID)).GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault()).ToList();
                    }
                }

                if (ContractorTypeIDs != "[]" && !string.IsNullOrEmpty(ContractorTypeIDs))
                {
                    System.Collections.Generic.IList<int?> ContractorTypeID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(ContractorTypeIDs);
                    if (ContractorTypeID.Count != 0)
                    {
                        objResult = objResult.Where(x => ContractorTypeID.Contains(x.ContractorTypeID)).GroupBy(x => x.ContractorTypeID).Select(x => x.FirstOrDefault()).ToList();
                    }
                }

                objResult = objResult.GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault()).ToList();
                
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/ContractTypeForFilterAssingment")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTypeForFilterAssingment(int CID, string ProjectIDs, string ContractorTypeIDs)
        {
            List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();
            //List<Vendor_SP_ContractorProjectMapping_Result> objResult = new List<Vendor_SP_ContractorProjectMapping_Result>();
            try
            {
                objResult = VendorAuditMaster.GetContractTypeForFilterAssingment(CID);

                if (ProjectIDs != "[]" && !string.IsNullOrEmpty(ProjectIDs))
                {
                    System.Collections.Generic.IList<int?> ProjectID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(ProjectIDs);
                    if (ProjectID.Count != 0)
                    {
                        objResult = objResult.Where(x => ProjectID.Contains(x.ProjectID)).GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault()).ToList();
                    }
                }

                if (ContractorTypeIDs != "[]" && !string.IsNullOrEmpty(ContractorTypeIDs))
                {
                    System.Collections.Generic.IList<int?> ContractorTypeID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(ContractorTypeIDs);
                    if (ContractorTypeID.Count != 0)
                    {
                        objResult = objResult.Where(x => ContractorTypeID.Contains(x.ContractorTypeID)).GroupBy(x => x.ContractorTypeID).Select(x => x.FirstOrDefault()).ToList();
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ProjectLocationMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectLocationMaster(int CID)
        {
            List<Vendor_SP_GetLocation_Result> objResult = new List<Vendor_SP_GetLocation_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectLocationMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/GetCustomerBranchDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCustomerBranchDetail(int CID, int ID)
        {
            List<CustomerLocationDetails> Namelist = new List<CustomerLocationDetails>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   var list = (from obj in entities.CustomerBranches
                                   where obj.ID == ID
                                   && obj.IsDeleted == false
                                   select obj).ToList();

                    foreach (var item in list)
                    {
                        CustomerLocationDetails obj = new CustomerLocationDetails();
                        obj.Address = item.AddressLine1;
                        obj.CityID = item.CityID;
                        obj.StateID = item.StateID;
                        Namelist.Add(obj);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Namelist).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Namelist).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectOfficeMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectOfficeMaster(int CID)
        {
            List<Vendor_ProjectOfficeName> objResult = new List<Vendor_ProjectOfficeName>();
            try
            {
                objResult = VendorAuditMaster.GetProjectOfficeMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetContractMappingDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetContractMappingDetail(int CID, int ID,int ContID)
        {
            List<Vendor_SP_GetProjectcontractorMapping_Result> ContractorDetailsList = new List<Vendor_SP_GetProjectcontractorMapping_Result>();

            try
            {
                ContractorDetailsList = VendorAuditMaster.GetContractMappingDetail(CID, ID, ContID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindContractorVendorUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindContractorVendorUser(int CID)
        {
            List<Vendor_SP_GetContractorUserMasterList_Result> objResult = new List<Vendor_SP_GetContractorUserMasterList_Result>();

            try
            {
                objResult = VendorAuditMaster.GetContractorUserList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/BindUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindUser(int CID)
        {
            List<UserDetails> objResult = new List<UserDetails>();
            
            try
            {               
                objResult = VendorAuditMaster.GetUserList(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/BindRole")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindRole()
        {
            List<Vendor_SP_GetRoleList_Result> objResult = new List<Vendor_SP_GetRoleList_Result>();
            try
            {
                objResult = VendorAuditMaster.GetRole();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectFileDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectFileDetail(int CID, int PID,int Type)
        {
            List<Vendor_SP_GetFileData_Result> objResult = new List<Vendor_SP_GetFileData_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectFiles(CID, PID, Type);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/DocumentUploadFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public void DocumentUploadFile()
        {
            if (true)//HttpContext.Current.Request.Files.AllKeys.Any()
            {
                string FilePath = string.Empty;

                var FIds = HttpContext.Current.Request.Form["FId"];
                int FId = Convert.ToInt32(FIds);

                var UId = HttpContext.Current.Request.Form["UserId"];
                int UserId = Convert.ToInt32(UId);

                var CId = HttpContext.Current.Request.Form["CustId"];
                int CustId = Convert.ToInt32(CId);

                var RegistrationNo = HttpContext.Current.Request.Form["RegistrationNo"];
                
                var LicenseType = HttpContext.Current.Request.Form["LicenseType"];
                int LicenseTypeId = Convert.ToInt32(LicenseType);

                var ProjectIDs = HttpContext.Current.Request.Form["ProjectID"];
                int ProjectID = Convert.ToInt32(ProjectIDs);

                var Validity = HttpContext.Current.Request.Form["Validity"];

                var Remark = HttpContext.Current.Request.Form["Remark"];

                string FileLocationPath = string.Empty;
                if (UserId != null && UserId > 0)
                {
                    if (FId > 0)
                    {
                        HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                        if (postedFile1 != null)
                        {
                            string filepath = ConfigurationManager.AppSettings["VendorFile"];

                            string storefolder = "VendorFile";
                            string path = @"" + filepath + "\\" + storefolder + "\\" + UserId + "\\" + ProjectID + "\\";

                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string fileName = Path.GetFileName(postedFile1.FileName);
                            postedFile1.SaveAs(path + fileName);
                            FilePath = storefolder + "/" + UserId + "/" + ProjectID + "/" + fileName;

                            string dbFilePath = "~/" + FilePath;

                            Stream fs = postedFile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Vendor_tbl_FileData tempDocument = null;

                            tempDocument = new Vendor_tbl_FileData()
                            {
                                ProjectID = ProjectID,
                                CustomerID = CustId,
                                LicTypeID = LicenseTypeId,
                                RegNo = RegistrationNo,
                                Remark = Remark,
                                //Validity = Convert.ToDateTime(Validity),
                                Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None),
                                FileName = fileName,
                                FilePath = dbFilePath,
                                Version = "1.0",
                                CreatedBy = UserId,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = UserId,
                                UpdatedOn = DateTime.Now,
                                FileSize = postedFile1.ContentLength,
                                IsDeleted = false
                            };
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                           where row.ID == FId
                                                         && row.CustomerID == CustId
                                                         && row.ProjectID == ProjectID
                                                           select row).FirstOrDefault();
                                if (obj != null)
                                {
                                    obj.LicTypeID = LicenseTypeId;
                                    obj.RegNo = RegistrationNo;
                                    obj.Remark = Remark;
                                    obj.Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    //obj.Validity = Convert.ToDateTime(Validity);
                                    obj.FileName = fileName;
                                    obj.FilePath = dbFilePath;
                                    obj.UpdatedBy = UserId;
                                    obj.UpdatedOn = DateTime.Now;
                                    obj.FileSize = postedFile1.ContentLength;
                                    obj.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        else
                        {

                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                           where row.ID == FId
                                                         && row.CustomerID == CustId
                                                         && row.ProjectID == ProjectID
                                                           select row).FirstOrDefault();
                                if (obj != null)
                                {
                                    obj.LicTypeID = LicenseTypeId;
                                    obj.RegNo = RegistrationNo;
                                    obj.Remark = Remark;
                                    obj.Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    //obj.Validity = Convert.ToDateTime(Validity);                                   
                                    obj.UpdatedBy = UserId;
                                    obj.UpdatedOn = DateTime.Now;
                                    obj.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                        if (postedFile1 != null)
                        {
                            string filepath = ConfigurationManager.AppSettings["VendorFile"];

                            string storefolder = "VendorFile";
                            string path = @"" + filepath + "\\" + storefolder + "\\" + UserId + "\\" + ProjectID + "\\";

                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string fileName = Path.GetFileName(postedFile1.FileName);
                            postedFile1.SaveAs(path + fileName);
                            FilePath = storefolder + "/" + UserId + "/" + ProjectID + "/" + fileName;

                            string dbFilePath = "~/" + FilePath;

                            Stream fs = postedFile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Vendor_tbl_FileData tempDocument = null;

                            tempDocument = new Vendor_tbl_FileData()
                            {
                                ProjectID = ProjectID,
                                CustomerID = CustId,
                                LicTypeID = LicenseTypeId,
                                RegNo = RegistrationNo,
                                Remark = Remark,
                                Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None),
                                //Validity = Convert.ToDateTime(Validity),
                                FileName = fileName,
                                FilePath = dbFilePath,
                                Version = "1.0",
                                CreatedBy = UserId,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = UserId,
                                UpdatedOn = DateTime.Now,
                                FileSize = postedFile1.ContentLength,
                                IsDeleted = false
                            };

                            long _objTempDocumentID = VendorAuditMaster.CreateVendorDocument(tempDocument);
                        }
                    }
                }
            }
        }


        [Route("VendorAudit/CheckExistlicnumber")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CheckExistlicnumber(int CustID,string regnumber,int IsProjectID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (IsProjectID == 0)//for contract
                    {
                        Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                   where row.CustomerID == CustID
                                                 && row.RegNo.Trim() == regnumber.Trim()
                                                 && row.ProjectID == IsProjectID
                                                   select row).FirstOrDefault();
                        if (obj != null)
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                    else//for Project
                    {
                        Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                   where row.CustomerID == CustID
                                                 && row.RegNo == regnumber.Trim()
                                                 && row.ProjectID != 0
                                                   select row).FirstOrDefault();
                        if (obj != null)
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/DocumentUploadFileMapping")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public void DocumentUploadFileMapping()
        {
            if (true)//HttpContext.Current.Request.Files.AllKeys.Any()
            {
                string FilePath = string.Empty;

                var FIds = HttpContext.Current.Request.Form["FId"];
                int FId = Convert.ToInt32(FIds);

                var UId = HttpContext.Current.Request.Form["UserId"];
                int UserId = Convert.ToInt32(UId);

                var CId = HttpContext.Current.Request.Form["CustId"];
                int CustId = Convert.ToInt32(CId);

                var RegistrationNo = HttpContext.Current.Request.Form["RegistrationNo"];
                                
                var LicenseType = HttpContext.Current.Request.Form["LicenseType"];
                int LicenseTypeId = Convert.ToInt32(LicenseType);

                var ProjectMappingIDs = HttpContext.Current.Request.Form["ProjectMappingID"];
                int ProjectMappingID = Convert.ToInt32(ProjectMappingIDs);

                var Validity = HttpContext.Current.Request.Form["Validity"];

                var Remark = HttpContext.Current.Request.Form["Remark"];

                var HeadCount = HttpContext.Current.Request.Form["HeadCount"];

                string FileLocationPath = string.Empty;
                if (UserId != null && UserId > 0)
                {
                    if (FId > 0)
                    {
                        HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                        if (postedFile1 != null)
                        {
                            string filepath = ConfigurationManager.AppSettings["VendorFile"];

                            string storefolder = "VendorMappingFile";
                            string path = @"" + filepath + "\\" + storefolder + "\\" + UserId + "\\" + ProjectMappingID + "\\";

                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string fileName = Path.GetFileName(postedFile1.FileName);
                            postedFile1.SaveAs(path + fileName);
                            FilePath = storefolder + "/" + UserId + "/" + ProjectMappingID + "/" + fileName;

                            string dbFilePath = "~/" + FilePath;

                            Stream fs = postedFile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Vendor_tbl_FileData tempDocument = null;

                            tempDocument = new Vendor_tbl_FileData()
                            {
                                ProjectID = 0,
                                CustomerID = CustId,
                                LicTypeID = LicenseTypeId,
                                RegNo = RegistrationNo,
                                Remark = Remark,
                                Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None),
                                //Validity = Convert.ToDateTime(Validity),
                                FileName = fileName,
                                FilePath = dbFilePath,
                                Version = "1.0",
                                CreatedBy = UserId,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = UserId,
                                UpdatedOn = DateTime.Now,
                                FileSize = postedFile1.ContentLength,
                                IsDeleted = false,
                                IsMapping = 1,
                                ProjectMappingID = ProjectMappingID,
                                HeadCount = Convert.ToInt32(HeadCount)
                            };
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                           where row.ID == FId
                                                         && row.CustomerID == CustId
                                                         && row.ProjectMappingID == ProjectMappingID
                                                           select row).FirstOrDefault();
                                if (obj != null)
                                {
                                    obj.LicTypeID = LicenseTypeId;
                                    obj.RegNo = RegistrationNo;
                                    obj.Remark = Remark;
                                    obj.Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    //obj.Validity = Convert.ToDateTime(Validity);
                                    obj.FileName = fileName;
                                    obj.FilePath = dbFilePath;
                                    obj.UpdatedBy = UserId;
                                    obj.UpdatedOn = DateTime.Now;
                                    obj.FileSize = postedFile1.ContentLength;
                                    obj.HeadCount = Convert.ToInt32(HeadCount);
                                    obj.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        else
                        {

                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                                           where row.ID == FId
                                                         && row.CustomerID == CustId
                                                         && row.ProjectMappingID == ProjectMappingID
                                                           select row).FirstOrDefault();
                                if (obj != null)
                                {
                                    obj.LicTypeID = LicenseTypeId;
                                    obj.RegNo = RegistrationNo;
                                    obj.Remark = Remark;
                                    obj.Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    //obj.Validity = Convert.ToDateTime(Validity);
                                    obj.UpdatedBy = UserId;
                                    obj.UpdatedOn = DateTime.Now;
                                    obj.HeadCount = Convert.ToInt32(HeadCount);
                                    obj.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        HttpPostedFile postedFile1 = HttpContext.Current.Request.Files["UploadedImage"];
                        if (postedFile1 != null)
                        {
                            string filepath = ConfigurationManager.AppSettings["VendorFile"];

                            string storefolder = "VendorMappingFile";
                            string path = @"" + filepath + "\\" + storefolder + "\\" + UserId + "\\" + ProjectMappingID + "\\";

                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string fileName = Path.GetFileName(postedFile1.FileName);
                            postedFile1.SaveAs(path + fileName);
                            FilePath = storefolder + "/" + UserId + "/" + ProjectMappingID + "/" + fileName;

                            string dbFilePath = "~/" + FilePath;

                            Stream fs = postedFile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Vendor_tbl_FileData tempDocument = null;

                            tempDocument = new Vendor_tbl_FileData()
                            {
                                ProjectID = 0,
                                CustomerID = CustId,
                                LicTypeID = LicenseTypeId,
                                RegNo = RegistrationNo,
                                Remark = Remark,
                                Validity = DateTime.ParseExact(Validity, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None),
                                //Validity = Convert.ToDateTime(Validity),
                                FileName = fileName,
                                FilePath = dbFilePath,
                                Version = "1.0",
                                CreatedBy = UserId,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = UserId,
                                UpdatedOn = DateTime.Now,
                                FileSize = postedFile1.ContentLength,
                                IsDeleted = false,
                                IsMapping = 1,
                                ProjectMappingID = ProjectMappingID,
                                HeadCount = Convert.ToInt32(HeadCount)
                            };

                            long _objTempDocumentID = VendorAuditMaster.CreateVendorDocument(tempDocument);
                        }
                    }
                }
            }
        }

        [Route("VendorAudit/ProjectMasterForMapping")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMasterForMapping(int CID)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectMaster(CID);
                objResult = objResult.Where(x => x.Isdisable == false).ToList();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ProjectMasterNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMasterNew(int CID)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectMaster(CID);
                objResult = objResult.Where(x => x.Isdisable == null || x.Isdisable == false).ToList();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/ProjectMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectMaster(int CID)
        {
            List<Vendor_SP_GetProjectMaster_Result> objResult = new List<Vendor_SP_GetProjectMaster_Result>();
            try
            {
                objResult = VendorAuditMaster.GetProjectMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        

        [Route("VendorAudit/ProjectUserRoleMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectUserRoleMaster(int CID,string code)
        {
            List<Vendor_SP_GetUserList_Result> userFinalList = new List<Vendor_SP_GetUserList_Result>();
            try
            {
                userFinalList = VendorAuditMaster.GetUserMaster(CID, code);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectRoleMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectRoleMaster(int CID)
        {
            List<RoleDTO> userFinalList = new List<RoleDTO>();
            try
            {
                userFinalList = UserManagement.GetRoleDetails(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectUserMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectUserMaster(int CID)
        {
            List<UserDetails> userFinalList = new List<UserDetails>();
            try
            {
                userFinalList = UserManagement.GetUserDetails(CID);
                
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(userFinalList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectSiteStatusMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectSiteStatusMaster(int CID)
        {
            List<Vendor_SiteStatus> objResult = new List<Vendor_SiteStatus>();
            try
            {
                objResult = VendorAuditMaster.GetProjectSitestatusMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectElementMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectElementMaster(int CID)
        {
            List<Vendor_ProjectElement> objResult = new List<Vendor_ProjectElement>();
            try
            {
                objResult = VendorAuditMaster.GetProjectElementMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Route("VendorAudit/ProjectCategoryMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectCategoryMaster(int CID)
        {
            List<Vendor_ProjectCategorization> objResult = new List<Vendor_ProjectCategorization>();
            try
            {
                objResult = VendorAuditMaster.GetProjectCategoryMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ProjectStatusMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ProjectStatusMaster(int CID)
        {
            List<Vendor_ProjectStatus> objResult = new List<Vendor_ProjectStatus>();
            try
            {
                objResult = VendorAuditMaster.GetProjectStatusMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/LicenseMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LicenseMaster(int CID)
        {
            List<Vendor_LicenseType> objResult = new List<Vendor_LicenseType>();
            try
            {
                objResult = VendorAuditMaster.GetLicenseMaster(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/ContractTypeMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTypeMaster(int CID)
        {
            List<Vendor_ContractType> objResult = new List<Vendor_ContractType>();
            try
            {
                objResult = VendorAuditMaster.GetContractTypeMaster(CID);

                objResult = objResult.GroupBy(x => x.Name).Select(x => x.FirstOrDefault()).ToList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ChecklistStatusMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ChecklistStatusMaster(string role,int uid)
        {
            List<Vendor_CheckListStatus> objResult = new List<Vendor_CheckListStatus>();
            try
            {
                objResult = VendorAuditMaster.GetchecklistStatusMaster();
                if (role == "VCCON")
                {
                    objResult = objResult.Where(x => x.Name == "Submitted").ToList();
                }
                else
                {
                    objResult = objResult.Where(x => x.Name == "Closed").ToList();
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/CityMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CityMaster(int CID,int StateID)
        {
            List<Vendor_SP_GetCityData_Result> objResult = new List<Vendor_SP_GetCityData_Result>();
            try
            {   
                objResult = VendorAuditMaster.GetCityMaster(CID, StateID);
                
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/StateMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage StateMaster()
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {
                objResult = VendorAuditMaster.GetStateMaster();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GetContractMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetContractMaster(int CID)
        {
            List<Vendor_ContractorMaster> ContractorDetailsList = new List<Vendor_ContractorMaster>();

            try
            {
                ContractorDetailsList = VendorAuditMaster.GetContractMasterList(CID);

                ContractorDetailsList = ContractorDetailsList.GroupBy(x => x.ContractorName).Select(x => x.FirstOrDefault()).ToList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
   
        [Route("VendorAudit/ContractMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractMaster(int PID,int CID)
        {
            List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
            
            try
            {
                ContractorDetailsList = VendorAuditMaster.GetContractMaster(PID, CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetContractDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetContractDetail(int CID, int ContID)
        {
            List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

            try
            {
                ContractorDetailsList = VendorAuditMaster.GetContractDetail(CID, ContID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/ContractdrpMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractdrpMaster(int CID,int ContID)
        {
            List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

            try
            {
                ContractorDetailsList = VendorAuditMaster.GetdrpContractMaster(CID, ContID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ContractorDetailsList).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GetVendorEntitiesBranches")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetVendorEntitiesBranches(int CustomerId, int ParentId)
        {
            List<Vendor_SP_GetEntitiesMasterBranchList_Result> objResult = new List<Vendor_SP_GetEntitiesMasterBranchList_Result>();

            try
            {
                objResult = VendorAuditMaster.GetEntitiesBranches(CustomerId, ParentId);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetEntityStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetEntityStatus()
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {
                objResult.Add(new RoleDTO { ID = 0, Name = "Inactive" });
                objResult.Add(new RoleDTO { ID = 1, Name = "Active" });
                objResult.Add(new RoleDTO { ID = 2, Name = "Suspended" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetSubEntityType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSubEntityType(string Flag)
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {
                var Result = VendorAuditMaster.GetSubEntityType();
                if (Result != null)
                {
                    foreach (var item in Result)
                        if (Flag == "E")
                        {
                            if (item.ID == 1)
                            {
                                objResult.Add(new RoleDTO { ID = item.ID, Name = item.Name });
                            }

                        }
                        else
                        {
                            //if(item.ID != 1)
                            //{
                            //    objResult.Add(new RoleDTO { ID = item.ID, Name = item.Name });
                            //}
                            objResult.Add(new RoleDTO { ID = item.ID, Name = item.Name });
                        }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetEntityType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetEntityType()
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {
                objResult.Add(new RoleDTO { ID = 0, Name = "Inactive" });
                objResult.Add(new RoleDTO { ID = 1, Name = "Active" });
                objResult.Add(new RoleDTO { ID = 2, Name = "Suspended" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetLegalEntityType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalEntityType()
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {

                var Result = VendorAuditMaster.GetLegalEntityType();
                if (Result != null)
                {
                    foreach (var item in Result)
                        objResult.Add(new RoleDTO { ID = item.ID, Name = item.EntityTypeName });
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetIndustry")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetIndustry()
        {
            List<DDLDTO> objResult = new List<DDLDTO>();
            try
            {
                var Result = VendorAuditMaster.GetEntityIndustry();
                if (Result != null)
                {
                    foreach (var item in Result)
                        objResult.Add(new DDLDTO { value = item.ID, text = item.Name });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetEntityCompanyType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetEntityCompanyType()
        {
            List<RoleDTO> objResult = new List<RoleDTO>();
            try
            {
                var Result = VendorAuditMaster.GetEntityCompanyType();
                if (Result != null)
                {
                    foreach (var item in Result)
                        objResult.Add(new RoleDTO { ID = item.ID, Name = item.Name });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/VendorCustomerBreadScrum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage VendorCustomerBreadScrum(int CustomerId, int ParentId)
        {
            List<SP_Audit_EntityBreadScrum_Result> objResult = new List<SP_Audit_EntityBreadScrum_Result>();
            try
            {
                objResult = VendorAuditMaster.GetVendorCustomerBreadScrum(CustomerId, ParentId);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetVendorEntities")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetVendorEntities(int CID)
        {
            List<Vendor_SP_GetEntitiesMasterList_Result> objResult = new List<Vendor_SP_GetEntitiesMasterList_Result>();

            try
            {
                objResult = VendorAuditMaster.GetVendorEntities(CID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphPopUpDataForCompliances")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphPopUpDataForCompliances(int CID,int UID,string role)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();

                    }
                    else
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     && row.AuditorID == UID
                                     select row).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GraphPopUpDataForCompliancesonlysubcontractor")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphPopUpDataForCompliancesonlysubcontractor(int CID, int UID, string role)
        {
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (role.Equals("VCCON"))
                    {
                        objResult = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                     where row.CompliancechecklistStatus != null
                                     select row).ToList();

                        objResult = objResult.Where(x => x.ProjectSubContSpocID == UID && x.IsShowContractor == 1).ToList();                        
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphDataForCompliancesProjectWiseNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataForCompliancesProjectWiseNew(int UID, string Role, int CID, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusForProject> objResult = new List<ComplianceStatusForProject>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                            && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();

                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    if (Role.Equals("VCAUD"))//for auditor
                    {
                        output = output.Where(x => x.AuditorID == UID).ToList();
                    }
                    if (Role.Equals("VCCON"))//for Vendor
                    {
                        output = output.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    var distinctProject = output.GroupBy(x => x.ProjectName).Distinct().Select(x => x.FirstOrDefault());

                    foreach (var item in distinctProject)
                    {
                        objResult.Add(new ComplianceStatusForProject
                        {
                            ProjectName = item.ProjectName,
                            ProjectID = item.ProjectID,
                            Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusForProject
                { ProjectName = "", Complied = 0, CompliedHigh = 0, CompliedMedium = 0, CompliedLow = 0, NotComplied = 0, NotCompliedHigh = 0, NotCompliedMedium = 0, NotCompliedLow = 0, NotApplicable = 0, NotApplicableHigh = 0, NotApplicableMedium = 0, NotApplicableLow = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GraphDataForCompliancesProjectWise")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataForCompliancesProjectWise(int UID, string Role, int CID, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusForProject> objResult = new List<ComplianceStatusForProject>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                            && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();

                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    if (Role.Equals("VCAUD"))//for auditor
                    {
                        output = output.Where(x => x.AuditorID == UID).ToList();
                    }
                    if (Role.Equals("VCCON"))//for Vendor
                    {
                        output = output.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    //var distinctProject = (from row in output
                    //                       select row.ProjectName).Distinct().ToList();

                    var distinctProject = output.GroupBy(x => x.ProjectName).Distinct().Select(x => x.FirstOrDefault());

                    foreach (var item in distinctProject)
                    {
                        objResult.Add(new ComplianceStatusForProject
                        {
                            ProjectName = item.ProjectName,
                            ProjectID = item.ProjectID,
                            Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }

                    //var distinctProject = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                    //                       where row.CompliancechecklistStatus != null
                    //                       select row.ProjectName).Distinct().ToList();

                    //foreach (var item in distinctProject)
                    //{
                    //    objResult.Add(new ComplianceStatusForProject
                    //    {
                    //        ProjectName = item,
                    //        Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ProjectName == item).ToList().Count,
                    //        CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ProjectName == item).ToList().Count,
                    //        CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ProjectName == item).ToList().Count,
                    //        CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ProjectName == item).ToList().Count,
                    //        NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ProjectName == item).ToList().Count,
                    //        NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ProjectName == item).ToList().Count,
                    //        NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ProjectName == item).ToList().Count,
                    //        NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ProjectName == item).ToList().Count,
                    //        NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ProjectName == item).ToList().Count,
                    //        NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ProjectName == item).ToList().Count,
                    //        NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ProjectName == item).ToList().Count,
                    //        NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ProjectName == item).ToList().Count,
                    //    });
                    //}
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusForProject
                { ProjectName = "", Complied = 0, CompliedHigh = 0, CompliedMedium = 0, CompliedLow = 0, NotComplied = 0, NotCompliedHigh = 0, NotCompliedMedium = 0, NotCompliedLow = 0, NotApplicable = 0, NotApplicableHigh = 0, NotApplicableMedium = 0, NotApplicableLow = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphDataOfAuditStatusAsPerContractorType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataOfAuditStatusAsPerContractorType(int UID, int CID, string Role, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<DashboradContractorTypes> objResult = new List<DashboradContractorTypes>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, false);

                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }

                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }

                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    var outputCT = output.GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault());

                    foreach (var item in outputCT)
                    {
                        objResult.Add(new DashboradContractorTypes
                        {
                            ContractorTypeID = item.ContractorID,
                            ContractorType = item.ContractorType,
                            Overdue = output.Where(x => x.Status == "Overdue" && x.ContractorType == item.ContractorType).ToList().Count,
                            Underreview = output.Where(x => x.Status == "Pending Review" && x.ContractorType == item.ContractorType).ToList().Count,
                            Closed = output.Where(x => x.Status == "Closed" && x.ContractorType == item.ContractorType).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphDataForCompliancesContWiseNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataForCompliancesContWiseNew(int UID, string Role, int CID, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusForProject> objResult = new List<ComplianceStatusForProject>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();

                    }
                    if (Role.Equals("VCCON"))//for Vendor
                    {
                        output = output.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    if (Role.Equals("VCAUD"))//for auditor
                    {
                        output = output.Where(x => x.AuditorID == UID).ToList();
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    var outputCT = output.GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault());

                    foreach (var item in outputCT)
                    {
                        objResult.Add(new ComplianceStatusForProject
                        {
                            ProjectName = item.ContractorTypeName,
                            Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusForProject
                { ProjectName = "", Complied = 0, CompliedHigh = 0, CompliedMedium = 0, CompliedLow = 0, NotComplied = 0, NotCompliedHigh = 0, NotCompliedMedium = 0, NotCompliedLow = 0, NotApplicable = 0, NotApplicableHigh = 0, NotApplicableMedium = 0, NotApplicableLow = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("VendorAudit/GraphDataForCompliancesContWise")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataForCompliancesContWise(int UID, string Role, int CID, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusForProject> objResult = new List<ComplianceStatusForProject>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();

                    }
                    if (Role.Equals("VCCON"))//for Vendor
                    {
                        output = output.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }
                    if (Role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();

                    }
                    if (Role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    if (Role.Equals("VCAUD"))//for auditor
                    {
                        output = output.Where(x => x.AuditorID == UID).ToList();
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    var outputCT = output.GroupBy(x => x.ContractorType).Select(x => x.FirstOrDefault());

                    foreach (var item in outputCT)
                    {
                        objResult.Add(new ComplianceStatusForProject
                        {
                            ProjectName = item.ContractorTypeName,
                            Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ContractorTypeName == item.ContractorTypeName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusForProject
                { ProjectName = "", Complied = 0, CompliedHigh = 0, CompliedMedium = 0, CompliedLow = 0, NotComplied = 0, NotCompliedHigh = 0, NotCompliedMedium = 0, NotCompliedLow = 0, NotApplicable = 0, NotApplicableHigh = 0, NotApplicableMedium = 0, NotApplicableLow = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphDataOfAuditStatusAsPerProject")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataOfAuditStatusAsPerProject(int UID, int CID, string Role, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<DashboradContractorTypes> objResult = new List<DashboradContractorTypes>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var output = VendorAuditMaster.GetDashboradCount(UID, CID, Role, false);

                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }

                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }

                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    var outputCT = output.GroupBy(x => x.ProjectName).Select(x => x.FirstOrDefault());

                    foreach (var item in outputCT)
                    {
                        objResult.Add(new DashboradContractorTypes
                        {
                            ProjectID = item.ProjectID,
                            ContractorType = item.ProjectName,
                            Overdue = output.Where(x => x.Status == "Overdue" && x.ProjectName == item.ProjectName).ToList().Count,
                            Underreview = output.Where(x => x.Status == "Pending Review" && x.ProjectName == item.ProjectName).ToList().Count,
                            Closed = output.Where(x => x.Status == "Closed" && x.ProjectName == item.ProjectName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GraphDataForCompliancesProjectWiseForSubContractor")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GraphDataForCompliancesProjectWiseForSubContractor(int UID, string Role, int CID, string StartDateDetail, string EndDateDetail, string Location, string ProjectID, string Period)
        {
            List<ComplianceStatusForProject> objResult = new List<ComplianceStatusForProject>();
            DateTime FromFinancialYearSummary = DateTime.Now;
            DateTime ToFinancialYearSummary = DateTime.Now;

            List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result> output = new List<Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad_Result>();
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    output = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleListdashborad(CID, -1, -1)
                              where row.CompliancechecklistStatus != null
                              select row).ToList();

                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn >= sd)).ToList();

                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        output = output.Where(entry => (entry.ScheduleOn <= Ld)).ToList();
                    }

                    if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                    {
                        System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                        if (ProjectIDs.Count != 0)
                        {
                            output = output.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        }
                    }
                    if (Location != "[]" && !string.IsNullOrEmpty(Location))
                    {
                        System.Collections.Generic.IList<int?> Locations = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(Location);
                        if (Locations.Count != 0)
                        {
                            output = output.Where(x => Locations.Contains(x.LocationID)).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(Period))
                    {

                        int period = Convert.ToInt32(Period);

                        if (period == 0) //Current YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 1) //Current + Previous YTD
                        {
                            if (DateTime.Today.Month > 3)
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-1);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                DateTime dtprev = DateTime.Now.AddYears(-2);
                                string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                                FromFinancialYearSummary = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        else if (period == 2) //All
                        {
                            string dtfrom = Convert.ToString("01-01-1900");
                            string dtto = Convert.ToString("01-01-1900");
                            FromFinancialYearSummary = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ToFinancialYearSummary = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (period == 3)// Last Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-30);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 4)//Last 3 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-90);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 5)//Last 6 Month
                        {
                            FromFinancialYearSummary = DateTime.Now.AddDays(-180);
                            ToFinancialYearSummary = DateTime.Now;
                        }
                        else if (period == 6)//This Month
                        {
                            DateTime now = DateTime.Now;
                            FromFinancialYearSummary = new DateTime(now.Year, now.Month, 1);
                            ToFinancialYearSummary = DateTime.Now;
                        }

                        if (FromFinancialYearSummary == null && ToFinancialYearSummary == null)
                        {
                            if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                            {
                                output = output.Where(entry => entry.ScheduleOn >= FromFinancialYearSummary && entry.ScheduleOn <= ToFinancialYearSummary).ToList();
                            }
                        }

                        if (FromFinancialYearSummary.Year != 1900 && ToFinancialYearSummary.Year != 1900)
                        {
                            output = (from row in output
                                      where row.ScheduleOn >= FromFinancialYearSummary
                                      && row.ScheduleOn <= ToFinancialYearSummary
                                      select row).ToList();
                        }
                    }

                    if (Role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CID
                                           && row.UserID == UID
                                           && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        output = output.Where(x => ProjectList.Contains(x.ProjectID)).ToList();
                    }
                    else if (Role.Equals("VCPD"))//for Director
                    {
                        output = output.Where(x => x.ProjectDirector == UID).ToList();
                    }
                    else if (Role.Equals("VCPH"))//for Head
                    {
                        output = output.Where(x => x.ProjectHead == UID).ToList();
                    }
                    else
                    {
                        if (Role.Equals("VCCON"))
                        {
                            output = output.Where(x => x.ProjectSubContSpocID == UID && x.IsShowContractor == 1).ToList();
                        }
                        else if (Role.Equals("VCAUD"))
                        {
                            output = output.Where(x => x.AuditorID == UID).ToList();
                        }
                        else
                        {
                            output.Clear();
                        }
                    }


                    var distinctProject = output.GroupBy(x => x.ProjectName).Distinct().Select(x => x.FirstOrDefault());

                    foreach (var item in distinctProject)
                    {
                        objResult.Add(new ComplianceStatusForProject
                        {
                            ProjectName = item.ProjectName,
                            ProjectID = item.ProjectID,
                            Complied = output.Where(x => x.ComplianceStatusID == 1 && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedHigh = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedMedium = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            CompliedLow = output.Where(x => x.ComplianceStatusID == 1 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotComplied = output.Where(x => x.ComplianceStatusID == 2 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedHigh = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedMedium = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotCompliedLow = output.Where(x => x.ComplianceStatusID == 2 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicable = output.Where(x => x.ComplianceStatusID == 3 && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableHigh = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "High" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableMedium = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Medium" && x.ProjectName == item.ProjectName).ToList().Count,
                            NotApplicableLow = output.Where(x => x.ComplianceStatusID == 3 && x.RiskCategory == "Low" && x.ProjectName == item.ProjectName).ToList().Count,
                            PeriodStartDate = FromFinancialYearSummary,
                            PeriodEndDate = ToFinancialYearSummary,
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objResult.Add(new ComplianceStatusForProject
                { ProjectName = "", Complied = 0, CompliedHigh = 0, CompliedMedium = 0, CompliedLow = 0, NotComplied = 0, NotCompliedHigh = 0, NotCompliedMedium = 0, NotCompliedLow = 0, NotApplicable = 0, NotApplicableHigh = 0, NotApplicableMedium = 0, NotApplicableLow = 0 });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        #region vishal report

        [Route("VendorAudit/GetAllContractorLicense")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllContractorLicense(int CID, int userID, string role)
        {
            // List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> objResult = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
            List<LicenseDTO> newlicenseDTO = new List<LicenseDTO>();
            List<ProjectMasterDTOGrid> ListOfProject = null;
            ProjectMasterDTOGrid ContractDetailsNew = null;
            int counter = 1;

            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    if (role.Equals("VCCON"))
                    {

                        var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                     join row1 in entities.Vendor_ContractorProjectMapping
                                                     on Prject.ID equals row1.ProjectID
                                                     join row2 in entities.Vendor_ContractorMaster
                                                     on row1.ContractorID equals row2.ID
                                                     //join row3 in entities.Vendor_ContractorMaster
                                                     //on row1.UnderContractorID equals row3.ID
                                                     where Prject.CustID == CID && (row1.ProjectContSpocID == userID || row1.ProjectContSpocID == userID || row2.AvacomUserID == userID)
                                                     select new ProjectMasterDTOGrid
                                                     {
                                                         PID = Prject.ID,
                                                         PMappingID = row1.ID,
                                                         Name = Prject.Name,
                                                         Location = Prject.Location,
                                                         LocationID = Prject.LocationID,
                                                         ContractorName = row2.ContractorName,
                                                         ContractorID = row2.ID,
                                                         ProjectOffice = Prject.Name,

                                                     }).ToList();



                        LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                        var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CID);

                        List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                          .Select(o => new LicenseDTO
                          {
                              ProjectMappingID = o.ProjectMappingID,
                              prjID = o.ProjectID,
                              LicID = o.LicTypeID,
                              Validity = o.Validity,
                              Remark = o.Remark,
                              LicenseNumber = o.RegNo,
                              FileName = o.FileName,
                              FilePath = o.FilePath

                          }).ToList();
                        foreach (var item in Licenselist)
                        {
                            if (item.LicID != null)
                                item.License = GetLicenseNameByID(item.LicID);
                        }

                        Licenselist = Licenselist.Where(x => x.ProjectMappingID != 0).ToList();

                        Licenselist = Licenselist.OrderBy(x => x.ProjectMappingID).ToList();

                        ListOfProject = new List<ProjectMasterDTOGrid>();
                        foreach (var item in Licenselist)
                        {

                            for (int i = 0; i < ContractorDetailsList.Count; i++)
                            {
                                if (ContractorDetailsList[i].PMappingID == item.ProjectMappingID)
                                {
                                    ContractDetailsNew = new ProjectMasterDTOGrid();

                                    ContractDetailsNew.SrNo = counter;
                                    ContractDetailsNew.Name = ContractorDetailsList[i].Name;
                                    ContractDetailsNew.PID = ContractorDetailsList[i].PID;
                                    ContractDetailsNew.Location = ContractorDetailsList[i].Location;
                                    ContractDetailsNew.LocationID = ContractorDetailsList[i].LocationID;
                                    ContractDetailsNew.ContractorName = ContractorDetailsList[i].ContractorName;
                                    ContractDetailsNew.ContractorID = ContractorDetailsList[i].ContractorID;
                                    ContractDetailsNew.ProjectOffice = ContractorDetailsList[i].ProjectOffice;
                                    ContractDetailsNew.License = item.License;
                                    ContractDetailsNew.Validity = GetConvertedDate(item.Validity);
                                    ContractDetailsNew.Remark = item.Remark;
                                    ContractDetailsNew.LicenseNumber = item.LicenseNumber;
                                    ContractDetailsNew.FileName = item.FileName;
                                    ContractDetailsNew.FilePath = item.FilePath;
                                    ListOfProject.Add(ContractDetailsNew);
                                    counter = counter + 1;
                                }
                            }
                        }
                    }
                    if (role.Equals("VCAUD"))//for Auditor
                    {

                        var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                     join row1 in entities.Vendor_ContractorProjectMapping
                                                     on Prject.ID equals row1.ProjectID
                                                     join row2 in entities.Vendor_ContractorMaster
                                                     on row1.ContractorID equals row2.ID
                                                     join row3 in entities.Vendor_ContractorProjectMappingAssignment
                                                     on row1.ID equals row3.ContractorProjectMappingID
                                                     where Prject.CustID == CID && row3.UserID == userID
                                                     select new ProjectMasterDTOGrid
                                                     {
                                                         PID = Prject.ID,
                                                         PMappingID = row1.ID,
                                                         Name = Prject.Name,
                                                         Location = Prject.Location,
                                                         LocationID = Prject.LocationID,
                                                         ContractorName = row2.ContractorName,
                                                         ContractorID = row2.ID,
                                                         ProjectOffice = Prject.Name,
                                                     }).ToList();


                        LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                        var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CID);

                        List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                          .Select(o => new LicenseDTO
                          {
                              ProjectMappingID = o.ProjectMappingID,
                              prjID = o.ProjectID,
                              LicID = o.LicTypeID,
                              Validity = o.Validity,
                              Remark = o.Remark,
                              LicenseNumber = o.RegNo,
                              FileName = o.FileName,
                              FilePath = o.FilePath
                          }).ToList();
                        foreach (var item in Licenselist)
                        {
                            if (item.LicID != null)
                                item.License = GetLicenseNameByID(item.LicID);
                        }

                        Licenselist = Licenselist.Where(x => x.ProjectMappingID != 0).ToList();

                        Licenselist = Licenselist.OrderBy(x => x.ProjectMappingID).ToList();

                        ListOfProject = new List<ProjectMasterDTOGrid>();
                        foreach (var item in Licenselist)
                        {

                            for (int i = 0; i < ContractorDetailsList.Count; i++)
                            {
                                if (ContractorDetailsList[i].PMappingID == item.ProjectMappingID)
                                {
                                    ContractDetailsNew = new ProjectMasterDTOGrid();

                                    ContractDetailsNew.SrNo = counter;
                                    ContractDetailsNew.Name = ContractorDetailsList[i].Name;
                                    ContractDetailsNew.PID = ContractorDetailsList[i].PID;
                                    ContractDetailsNew.Location = ContractorDetailsList[i].Location;
                                    ContractDetailsNew.LocationID = ContractorDetailsList[i].LocationID;
                                    ContractDetailsNew.ContractorName = ContractorDetailsList[i].ContractorName;
                                    ContractDetailsNew.ContractorID = ContractorDetailsList[i].ContractorID;
                                    ContractDetailsNew.ProjectOffice = ContractorDetailsList[i].ProjectOffice;
                                    ContractDetailsNew.License = item.License;
                                    ContractDetailsNew.Validity = GetConvertedDate(item.Validity);
                                    ContractDetailsNew.Remark = item.Remark;
                                    ContractDetailsNew.LicenseNumber = item.LicenseNumber;
                                    ContractDetailsNew.FileName = item.FileName;
                                    ContractDetailsNew.FilePath = item.FilePath;
                                    ListOfProject.Add(ContractDetailsNew);
                                    counter = counter + 1;
                                }
                            }
                        }

                    }
                    if (role.Equals("VCMNG"))//for MGMT
                    {
                        var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                     join row1 in entities.Vendor_ContractorProjectMapping
                                                     on Prject.ID equals row1.ProjectID
                                                     join row2 in entities.Vendor_ContractorMaster
                                                     on row1.ContractorID equals row2.ID
                                                     join row3 in entities.Vendor_ProjectMGMTMapping
                                                     on row1.ProjectID equals row3.ProjectId
                                                     where Prject.CustID == CID && row3.UserID == userID
                                                     select new ProjectMasterDTOGrid
                                                     {
                                                         PID = Prject.ID,
                                                         PMappingID = row1.ID,
                                                         Name = Prject.Name,
                                                         Location = Prject.Location,
                                                         LocationID = Prject.LocationID,
                                                         ContractorName = row2.ContractorName,
                                                         ContractorID = row2.ID,
                                                         ProjectOffice = Prject.Name,
                                                     }).ToList();

                        LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                        var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CID);

                        List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                          .Select(o => new LicenseDTO
                          {
                              ProjectMappingID = o.ProjectMappingID,
                              prjID = o.ProjectID,
                              LicID = o.LicTypeID,
                              Validity = o.Validity,
                              Remark = o.Remark,
                              LicenseNumber = o.RegNo,
                              FileName = o.FileName,
                              FilePath = o.FilePath

                          }).ToList();
                        foreach (var item in Licenselist)
                        {
                            if (item.LicID != null)
                                item.License = GetLicenseNameByID(item.LicID);
                        }

                        Licenselist = Licenselist.Where(x => x.ProjectMappingID != 0).ToList();

                        Licenselist = Licenselist.OrderBy(x => x.ProjectMappingID).ToList();

                        ListOfProject = new List<ProjectMasterDTOGrid>();
                        foreach (var item in Licenselist)
                        {

                            for (int i = 0; i < ContractorDetailsList.Count; i++)
                            {
                                if (ContractorDetailsList[i].PMappingID == item.ProjectMappingID)
                                {
                                    ContractDetailsNew = new ProjectMasterDTOGrid();

                                    ContractDetailsNew.SrNo = counter;
                                    ContractDetailsNew.Name = ContractorDetailsList[i].Name;
                                    ContractDetailsNew.PID = ContractorDetailsList[i].PID;
                                    ContractDetailsNew.Location = ContractorDetailsList[i].Location;
                                    ContractDetailsNew.LocationID = ContractorDetailsList[i].LocationID;
                                    ContractDetailsNew.ContractorName = ContractorDetailsList[i].ContractorName;
                                    ContractDetailsNew.ContractorID = ContractorDetailsList[i].ContractorID;
                                    ContractDetailsNew.ProjectOffice = ContractorDetailsList[i].ProjectOffice;
                                    ContractDetailsNew.License = item.License;
                                    ContractDetailsNew.Validity = GetConvertedDate(item.Validity);
                                    ContractDetailsNew.Remark = item.Remark;
                                    ContractDetailsNew.LicenseNumber = item.LicenseNumber;
                                    ContractDetailsNew.FileName = item.FileName;
                                    ContractDetailsNew.FilePath = item.FilePath;
                                    ListOfProject.Add(ContractDetailsNew);
                                    counter = counter + 1;
                                }
                            }
                        }

                    }
                    if (role.Equals("VCPH"))//for Project Head
                    {
                        var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                     join row1 in entities.Vendor_ContractorProjectMapping
                                                     on Prject.ID equals row1.ProjectID
                                                     join row2 in entities.Vendor_ContractorMaster
                                                     on row1.ContractorID equals row2.ID
                                                     //join row3 in entities.Vendor_ProjectMGMTMapping
                                                     //on row1.ProjectID equals row3.ProjectId
                                                     where Prject.CustID == CID && Prject.Head == userID
                                                     select new ProjectMasterDTOGrid
                                                     {
                                                         PID = Prject.ID,
                                                         PMappingID = row1.ID,
                                                         Name = Prject.Name,
                                                         Location = Prject.Location,
                                                         LocationID = Prject.LocationID,
                                                         ContractorName = row2.ContractorName,
                                                         ContractorID = row2.ID,
                                                         ProjectOffice = Prject.Name,
                                                     }).ToList();

                        LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                        var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CID);

                        List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                          .Select(o => new LicenseDTO
                          {
                              ProjectMappingID = o.ProjectMappingID,
                              prjID = o.ProjectID,
                              LicID = o.LicTypeID,
                              Validity = o.Validity,
                              Remark = o.Remark,
                              LicenseNumber = o.RegNo,
                              FileName = o.FileName,
                              FilePath = o.FilePath
                          }).ToList();
                        foreach (var item in Licenselist)
                        {
                            if (item.LicID != null)
                                item.License = GetLicenseNameByID(item.LicID);
                        }

                        Licenselist = Licenselist.Where(x => x.ProjectMappingID != 0).ToList();

                        Licenselist = Licenselist.OrderBy(x => x.ProjectMappingID).ToList();

                        ListOfProject = new List<ProjectMasterDTOGrid>();
                        foreach (var item in Licenselist)
                        {

                            for (int i = 0; i < ContractorDetailsList.Count; i++)
                            {
                                if (ContractorDetailsList[i].PMappingID == item.ProjectMappingID)
                                {
                                    ContractDetailsNew = new ProjectMasterDTOGrid();

                                    ContractDetailsNew.SrNo = counter;
                                    ContractDetailsNew.Name = ContractorDetailsList[i].Name;
                                    ContractDetailsNew.PID = ContractorDetailsList[i].PID;
                                    ContractDetailsNew.Location = ContractorDetailsList[i].Location;
                                    ContractDetailsNew.LocationID = ContractorDetailsList[i].LocationID;
                                    ContractDetailsNew.ContractorName = ContractorDetailsList[i].ContractorName;
                                    ContractDetailsNew.ContractorID = ContractorDetailsList[i].ContractorID;
                                    ContractDetailsNew.ProjectOffice = ContractorDetailsList[i].ProjectOffice;
                                    ContractDetailsNew.License = item.License;
                                    ContractDetailsNew.Validity = GetConvertedDate(item.Validity);
                                    ContractDetailsNew.Remark = item.Remark;
                                    ContractDetailsNew.LicenseNumber = item.LicenseNumber;
                                    ContractDetailsNew.FileName = item.FileName;
                                    ContractDetailsNew.FilePath = item.FilePath;
                                    ListOfProject.Add(ContractDetailsNew);
                                    counter = counter + 1;
                                }
                            }
                        }

                    }
                    if (role.Equals("VCPD"))//for Project Director
                    {
                        var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                     join row1 in entities.Vendor_ContractorProjectMapping
                                                     on Prject.ID equals row1.ProjectID
                                                     join row2 in entities.Vendor_ContractorMaster
                                                     on row1.ContractorID equals row2.ID
                                                     //join row3 in entities.Vendor_ProjectMGMTMapping
                                                     //on row1.ProjectID equals row3.ProjectId
                                                     where Prject.CustID == CID && Prject.Director == userID
                                                     select new ProjectMasterDTOGrid
                                                     {
                                                         PID = Prject.ID,
                                                         PMappingID = row1.ID,
                                                         Name = Prject.Name,
                                                         Location = Prject.Location,
                                                         LocationID = Prject.LocationID,
                                                         ContractorName = row2.ContractorName,
                                                         ContractorID = row2.ID,
                                                         ProjectOffice = Prject.Name,
                                                     }).ToList();

                        LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                        var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CID);

                        List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                          .Select(o => new LicenseDTO
                          {
                              ProjectMappingID = o.ProjectMappingID,
                              prjID = o.ProjectID,
                              LicID = o.LicTypeID,
                              Validity = o.Validity,
                              Remark = o.Remark,
                              LicenseNumber = o.RegNo,
                              FileName = o.FileName,
                              FilePath = o.FilePath
                          }).ToList();
                        foreach (var item in Licenselist)
                        {
                            if (item.LicID != null)
                                item.License = GetLicenseNameByID(item.LicID);
                        }

                        Licenselist = Licenselist.Where(x => x.ProjectMappingID != 0).ToList();

                        Licenselist = Licenselist.OrderBy(x => x.ProjectMappingID).ToList();

                        ListOfProject = new List<ProjectMasterDTOGrid>();
                        foreach (var item in Licenselist)
                        {

                            for (int i = 0; i < ContractorDetailsList.Count; i++)
                            {
                                if (ContractorDetailsList[i].PMappingID == item.ProjectMappingID)
                                {
                                    ContractDetailsNew = new ProjectMasterDTOGrid();

                                    ContractDetailsNew.SrNo = counter;
                                    ContractDetailsNew.Name = ContractorDetailsList[i].Name;
                                    ContractDetailsNew.PID = ContractorDetailsList[i].PID;
                                    ContractDetailsNew.Location = ContractorDetailsList[i].Location;
                                    ContractDetailsNew.LocationID = ContractorDetailsList[i].LocationID;
                                    ContractDetailsNew.ContractorName = ContractorDetailsList[i].ContractorName;
                                    ContractDetailsNew.ContractorID = ContractorDetailsList[i].ContractorID;
                                    ContractDetailsNew.ProjectOffice = ContractorDetailsList[i].ProjectOffice;
                                    ContractDetailsNew.License = item.License;
                                    ContractDetailsNew.Validity = GetConvertedDate(item.Validity);
                                    ContractDetailsNew.Remark = item.Remark;
                                    ContractDetailsNew.LicenseNumber = item.LicenseNumber;
                                    ContractDetailsNew.FileName = item.FileName;
                                    ContractDetailsNew.FilePath = item.FilePath;
                                    ListOfProject.Add(ContractDetailsNew);
                                    counter = counter + 1;
                                }
                            }
                        }

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ListOfProject).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ListOfProject).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/CreateContractorLicense")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public List<string> CreateContractorLicense()
        {
            List<string> result = new List<string>();
            List<long> LocationIDs = new List<long>();
            var cId = HttpContext.Current.Request.Form["CustID"];
            int CustID = Convert.ToInt32(cId);

            var UId = HttpContext.Current.Request.Form["UserID"];
            int UserID = Convert.ToInt32(UId);

            var role1 = HttpContext.Current.Request.Form["role"];
            string role = role1.ToString();

            var locationid = HttpContext.Current.Request.Form["LocationIDs"];

            if (locationid != "")
            {
                var newloca = (locationid ?? "").Split(',').Select<string, int>(int.Parse);
                foreach (var item in newloca)
                {
                    LocationIDs.Add(item);
                }
            }
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        if (role.Equals("VCCON"))
                        {
                            List<LicenseDTO> newlicenseDTO = new List<LicenseDTO>();
                            var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                         join row1 in entities.Vendor_ContractorProjectMapping
                                                         on Prject.ID equals row1.ProjectID
                                                         join row2 in entities.Vendor_ContractorMaster
                                                         on row1.ContractorID equals row2.ID
                                                         //join row3 in entities.Vendor_ContractorMaster
                                                         //on row1.UnderContractorID equals row3.ID
                                                         where Prject.CustID == CustID && row2.AvacomUserID == UserID
                                                         select new ProjectMasterDTO
                                                         {
                                                             PID = Prject.ID,
                                                             PMappingID = row1.ID,
                                                             Name = Prject.Name,
                                                             Location = Prject.Location,
                                                             LocationID = Prject.LocationID,
                                                             ContractorName = row2.ContractorName,
                                                             ProjectOffice = Prject.Name,
                                                         }).ToList();

                            if (LocationIDs.Count > 0)
                                ContractorDetailsList = ContractorDetailsList.Where(x => LocationIDs.Contains(x.LocationID)).ToList();

                            var LicenseDetailsList = (from row in entities.Vendor_LicenseType
                                                      where row.CustomerID == CustID && row.IsDeleted == false
                                                      select row.Name).ToList();


                            LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                            var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CustID);

                            List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                              .Select(o => new LicenseDTO
                              {
                                  ProjectMappingID = o.ProjectMappingID,
                                  prjID = o.ProjectID,
                                  LicID = o.LicTypeID,
                                  Validity = o.Validity,
                                  Remark = o.Remark,
                                  LicenseNumber = o.RegNo
                              }).ToList();
                            foreach (var item in Licenselist)
                            {
                                if (item.LicID != null)
                                    item.License = GetLicenseNameByID(item.LicID);
                            }


                            var counter = 1;

                            foreach (var x in ContractorDetailsList)
                            {
                                x.SrNo = counter++;
                            }
                            string FileName = "ContractorLicenseReport_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xlsx";
                            var createFileFile = ConfigurationManager.AppSettings["ContractorLicenseReport"] + "\\" + CustID + "\\";
                            if (!Directory.Exists(createFileFile))
                                Directory.CreateDirectory(createFileFile);

                            var file = new FileInfo(fileName: @createFileFile + FileName);

                            //if (File.Exists(createFileFile + "ContractorLicenseReport.xlsx"))
                            //{
                            //    file.Delete();
                            //    file = new FileInfo(fileName: @createFileFile + "ContractorLicenseReport.xlsx");
                            //}
                            SaveExelFile(ContractorDetailsList, file, LicenseDetailsList, Licenselist);
                            result.Add("success");
                            result.Add(FileName);
                        }
                        if (role.Equals("VCAUD"))//for Auditor
                        {
                            List<LicenseDTO> newlicenseDTO = new List<LicenseDTO>();
                            var ContractorDetailsList = (from Prject in entities.Vendor_ProjectMaster
                                                         join row1 in entities.Vendor_ContractorProjectMapping
                                                         on Prject.ID equals row1.ProjectID
                                                         join row2 in entities.Vendor_ContractorMaster
                                                         on row1.ContractorID equals row2.ID
                                                         join row3 in entities.Vendor_ContractorProjectMappingAssignment
                                                         on row1.ID equals row3.ContractorProjectMappingID
                                                         where Prject.CustID == CustID && row3.UserID == UserID
                                                         select new ProjectMasterDTO
                                                         {
                                                             PID = Prject.ID,
                                                             PMappingID = row1.ID,
                                                             Name = Prject.Name,
                                                             Location = Prject.Location,
                                                             LocationID = Prject.LocationID,
                                                             ContractorName = row2.ContractorName,
                                                             ProjectOffice = Prject.Name,
                                                         }).ToList();

                            if (LocationIDs.Count > 0)
                                ContractorDetailsList = ContractorDetailsList.Where(x => LocationIDs.Contains(x.LocationID)).ToList();

                            var LicenseDetailsList = (from row in entities.Vendor_LicenseType
                                                      where row.CustomerID == CustID && row.IsDeleted == false
                                                      select row.Name).ToList();


                            LicenseDTO LicenseFileDataDetails = new LicenseDTO();
                            var LicenseFileDataDetailsList = entities.Vendor_SP_GetFileData(CustID);

                            List<LicenseDTO> Licenselist = LicenseFileDataDetailsList.AsEnumerable()
                              .Select(o => new LicenseDTO
                              {
                                  ProjectMappingID = o.ProjectMappingID,
                                  prjID = o.ProjectID,
                                  LicID = o.LicTypeID,
                                  Validity = o.Validity,
                                  Remark = o.Remark,
                                  LicenseNumber = o.RegNo
                              }).ToList();
                            foreach (var item in Licenselist)
                            {
                                if (item.LicID != null)
                                    item.License = GetLicenseNameByID(item.LicID);
                            }


                            var counter = 1;

                            foreach (var x in ContractorDetailsList)
                            {
                                x.SrNo = counter++;
                            }

                            //var createFileFile = ConfigurationManager.AppSettings["ContractorLicenseReport"] + "\\" + CustID + "\\";
                            //if (!Directory.Exists(createFileFile))
                            //    Directory.CreateDirectory(createFileFile);

                            //var file = new FileInfo(fileName: @createFileFile + "ContractorLicenseReport.xlsx");

                            string FileName = "ContractorLicenseReport_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xlsx";
                            var createFileFile = ConfigurationManager.AppSettings["ContractorLicenseReport"] + "\\" + CustID + "\\";
                            if (!Directory.Exists(createFileFile))
                                Directory.CreateDirectory(createFileFile);

                            var file = new FileInfo(fileName: @createFileFile + FileName);

                            //if (File.Exists(createFileFile + "ContractorLicenseReport.xlsx"))
                            //{
                            //    file.Delete();
                            //    file = new FileInfo(fileName: @createFileFile + "ContractorLicenseReport.xlsx");
                            //}
                            SaveExelFile(ContractorDetailsList, file, LicenseDetailsList, Licenselist);
                            result.Add("success");
                            result.Add(FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
            }
            return result;
        }
        
        [Route("VendorAudit/DownloaddContractorLicense")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DownloaddContractorLicense(string Name, int CustID)
        {
            string path = string.Empty;
            try
            {
                if (Name.Contains("ContractorLicenseReport"))
                {
                    path = ConfigurationManager.AppSettings["ContractorLicenseReport"] + "\\" + CustID + "\\" + Name;
                }
                else
                {
                    path = ConfigurationManager.AppSettings["AuditorCloseReport"] + "\\" + CustID + "\\" + Name;
                }
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = Name
                };
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new HttpResponseMessage()
                {
                    Content = new StringContent("File Not Found", Encoding.UTF8, "application/json")
                };
            }
        }

        public string GetLicenseNameByID(int? id)
        {
            string result = string.Empty;
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    string LicenceName = (from name in entities.Vendor_LicenseType
                                          where name.ID == id
                                          select name.Name).FirstOrDefault();
                    result = LicenceName;
                }
            }
            catch
            {
                result = string.Empty;
            }


            return result;
        }

        private void SaveExelFile(List<ProjectMasterDTO> contractorDetailsList, FileInfo file, List<string> LicenseDetailsList, List<LicenseDTO> LicenseFileDataDetailsList)
        {
            try
            {
                using (var package = new ExcelPackage(file))
                {
                    var worksheet = package.Workbook.Worksheets.Add(Name: "ContractorLicenseReport");

                    var range = worksheet.Cells[Address: "A1"].LoadFromCollection(contractorDetailsList, PrintHeaders: true);


                    string[] stringarr = new string[4] { "License", "Validity", "Remark", "LICENSE NUMBER" };

                    int start = 9;

                    int strword = 0;
                    for (int i = 0; i < LicenseDetailsList.Count; i++)
                    {
                        foreach (var item in stringarr)
                        {

                            var range2 = worksheet.Cells[1, start].LoadFromText(LicenseDetailsList[i] + " " + stringarr[strword]);

                            start = start + 1;
                            strword = strword + 1;
                            if (strword > 3) strword = 0;
                            range2.AutoFitColumns();
                        }
                    }

                    int colData = 9;
                    int rowData = 2;
                    int endofCoulmn = worksheet.Dimension.End.Column;
                    for (int i = 0; i < contractorDetailsList.Count; i++)
                    {
                        foreach (var item in LicenseFileDataDetailsList)
                        {


                            for (int j = 9; j <= endofCoulmn; j++)
                            {

                                if (contractorDetailsList[i].PMappingID == item.ProjectMappingID) //|| (item.ProjectMappingID == 0 && contractorDetailsList[i].PID == item.prjID))//(contractorDetailsList[i].PID == item.ProjectMappingID)
                                {


                                    int columnStart = 9;
                                    int columnEnd = worksheet.Dimension.Columns;
                                    string cellRange = columnStart.ToString() + ":" + columnEnd.ToString();
                                    var searchCell = from cell in worksheet.Cells[1, 9, 1, columnEnd]
                                                     where cell.Value.ToString() == item.License.Trim() + " License"
                                                     select cell.Start.Column;

                                    int rowNum = searchCell.First();
                                    colData = rowNum;
                                    var range3 = worksheet.Cells[rowData, colData].LoadFromText(item.License);
                                    colData = colData + 1;
                                    var range4 = worksheet.Cells[rowData, colData].LoadFromText(GetConvertedDate(item.Validity));
                                    colData = colData + 1;
                                    var range5 = worksheet.Cells[rowData, colData].LoadFromText(item.Remark);
                                    colData = colData + 1;
                                    var range6 = worksheet.Cells[rowData, colData].LoadFromText(item.LicenseNumber);
                                    colData = colData + 1;
                                    //j = colData -1;
                                    range3.AutoFitColumns();
                                    range4.AutoFitColumns();
                                    range5.AutoFitColumns();
                                    range6.AutoFitColumns();
                                }
                                else
                                {

                                    var range3 = worksheet.Cells[rowData, colData].LoadFromText("NA");
                                    colData = colData + 1;


                                    var range4 = worksheet.Cells[rowData, colData].LoadFromText("NA");
                                    colData = colData + 1;
                                    var range5 = worksheet.Cells[rowData, colData].LoadFromText("NA");
                                    colData = colData + 1;
                                    var range6 = worksheet.Cells[rowData, colData].LoadFromText("NA");
                                    colData = colData + 1;
                                    j = colData - 1;
                                    range3.AutoFitColumns();
                                    range4.AutoFitColumns();
                                    range5.AutoFitColumns();
                                    range6.AutoFitColumns();
                                }
                            }
                        }
                        rowData = rowData + 1;
                        colData = 9;
                    }

                    for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
                    {

                        if (worksheet.Cells[1, col].Value == null)
                        {
                            worksheet.DeleteColumn(col, (worksheet.Dimension.End.Column - (col - 1)));
                        }
                    }


                    worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Font.Bold = true;
                    worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column].Style.Fill.BackgroundColor.SetColor(Color.Yellow);

                    worksheet.DeleteColumn(1, 3);
                    range.AutoFitColumns();
                    package.SaveAs(file);
                }
            }
            catch (Exception ex)
            {

            }
        }
        
        public string GetConvertedDate(DateTime? date)
        {
            string convertedDate = string.Empty;
            string result = string.Empty;
            try
            {

                convertedDate = String.Format("{0:dd-MM-yyyy}", date);
                result = convertedDate;

            }
            catch
            {
                result = "NA";
            }

            return result;
        }

        #endregion

        #region Vendor_Report

        [Route("VendorAudit/GetVendorAuditForCloseReport")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetVendorAuditForCloseReport(int CustID, int UID, string role, string location, string StartDate, string EndDate, string Status, string ProjectID, string Period)
        {
            List<Vendor_Sp_GetContractProjectMappingScheduleListReport_Result> lstMaster = new List<Vendor_Sp_GetContractProjectMappingScheduleListReport_Result>();
            List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result> lstDetails = new List<Vendor_Sp_GetContractProjectStepMappingScheduleList_Result>();
         
            string FolderPath = ConfigurationManager.AppSettings["AuditorCloseReport"] + "\\" + CustID + "\\";
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }
            string FileName = "AuditCloseReport_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xlsx";
            string FilePath = FolderPath + FileName;
            string[] splitMonth = Period.Split(new char[0]);
        
            try
            {
                List<long?> objProjectMappingID = new List<long?>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    lstMaster = entities.Vendor_Sp_GetContractProjectMappingScheduleListReport(CustID).Where(x => x.AuditCheckListStatusID == 4 && x.Frequency == "M").ToList();

                    if (role.Equals("VCCON"))//for Vendor
                    {
                        lstMaster = lstMaster.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();
                    }

                    if (role.Equals("VCMNG"))//for MGMT
                    {
                        var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                           where row.CustID == CustID
                                           && row.UserID == UID
                                              && row.IsDeleted == false
                                           select row.ProjectId).ToList();

                        lstMaster = lstMaster.Where(x => ProjectList.Contains(x.ProjectID)).ToList();
                    }

                    if (role.Equals("VCAUD"))//for Auditor
                    {
                        lstMaster = lstMaster.Where(x => x.AuditorID == UID).ToList();
                    }

                    if (role.Equals("VCPD"))//for Director
                    {
                        lstMaster = lstMaster.Where(x => x.ProjectDirector == UID).ToList();
                    }

                    if (role.Equals("VCPH"))//for Head
                    {
                        lstMaster = lstMaster.Where(x => x.ProjectHead == UID).ToList();
                    }

                    lstDetails = entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CustID, -1, -1).ToList();
                }
                objProjectMappingID = lstMaster.Select(x => (long?)x.ContractorProjectMappingID).Distinct().ToList();

                lstDetails = (from lstTD in objProjectMappingID
                              join TD in lstDetails on lstTD.Value equals TD.ContractProjMappingID
                              select TD).Distinct().ToList();

                if (location != "[]" && location != null)
                {
                    System.Collections.Generic.IList<int> locationIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                    if (locationIds.Count != 0)
                    {
                        lstDetails = lstDetails.Where(x => locationIds.Contains(x.LocationID)).ToList();
                        lstMaster = lstMaster.Where(x => locationIds.Contains(x.LocationID)).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(StartDate))
                {
                    DateTime StartDateDetail = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    lstDetails = lstDetails.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                    lstMaster = lstMaster.Where(entry => (entry.ScheduleOn >= StartDateDetail)).ToList();
                }
                if (!string.IsNullOrEmpty(EndDate))
                {
                    DateTime EnDDateDetail = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    lstDetails = lstDetails.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                    lstMaster = lstMaster.Where(entry => (entry.ScheduleOn <= EnDDateDetail)).ToList();
                }
                if (ProjectID != "[]" && !string.IsNullOrEmpty(ProjectID))
                {
                    System.Collections.Generic.IList<string> ProjectIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(ProjectID);
                    if (ProjectIDs.Count != 0)
                    {
                        lstDetails = lstDetails.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                        lstMaster = lstMaster.Where(x => ProjectIDs.Contains(x.ProjectName)).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(Period))
                {
                    lstDetails = lstDetails.Where(x => x.Status == "Closed" && x.Period == Period).OrderBy(m => m.ProjectName).ToList();
                    lstMaster = lstMaster.Where(x => x.ForMonth == Period).ToList();
                }
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        int totColumn = 0;
                        ExcelWorksheet workSheet = exportPackge.Workbook.Worksheets.Add("Audit Report");

                        workSheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
                        workSheet.Row(1).Style.TextRotation = 90;

                        workSheet.Row(2).Height = 35;
                        workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        workSheet.Row(2).Style.Font.Bold = true;

                        workSheet.Cells[2, 2, 2, 3].Value = "Contractor/Sub Conntractor";
                        workSheet.Cells[2, 2, 2, 3].Merge = true;
                        workSheet.Cells[2, 4].Value = "Services";
                        workSheet.Cells[2, 5].Value = "";
                        workSheet.Cells[2, 6].Value = "";
                        workSheet.Cells[2, 7].Value = "";
                        var range = workSheet.Cells[2, 1, 2, 7];
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Color colFromHexM = System.Drawing.ColorTranslator.FromHtml("#A6A6A6");
                        range.Style.Fill.BackgroundColor.SetColor(colFromHexM);
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        workSheet.Row(3).Height = 120;
                        workSheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Row(3).Style.TextRotation = 90;
                        workSheet.Row(3).Style.Font.Bold = true;
                        workSheet.Row(3).Style.Font.Size = 10;
                        workSheet.Cells[3, 1].Value = "SR.No";
                        workSheet.Cells[3, 2].Value = "Main Contractor";
                        workSheet.Cells[3, 3].Value = "Stakeholders";
                        workSheet.Cells[3, 4].Value = "Period of Audit";
                        workSheet.Cells[3, 5].Value = "Project Name";
                        workSheet.Cells[3, 6].Value = "Facility Name";
                        workSheet.Cells[3, 7].Value = "Total HC";

                        var range0 = workSheet.Cells[3, 1, 3, 7];
                        range0.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#DA9694");
                        range0.Style.Fill.BackgroundColor.SetColor(colFromHex);
                        workSheet.Column(1).Width = 10;
                        workSheet.Column(2).Width = 25;
                        workSheet.Column(3).Width = 20;
                        workSheet.Column(4).Width = 10;
                        workSheet.Column(5).Width = 15;
                        workSheet.Column(6).Width = 15;

                        #region Part First

                        #region ActStart
                        var lstActDetails = lstDetails.Select(x => new { x.ActName, x.ActID }).Distinct().ToList();
                        int loopA = 8;
                        foreach (var item1 in lstActDetails)
                        {
                            int loopC = 0;

                            var lstComplianceDetails = lstDetails.Where(x => x.ActID == item1.ActID)
                                                          .Select(x => new { x.ComplainceHeader, x.ComplianceID, x.RiskCategory }).Distinct().ToList();
                            int ic = ((lstComplianceDetails.Count()) - 1);
                            workSheet.Cells[2, loopA, 2, loopA + loopC].Value = item1.ActName;

                            //Style                           
                            var range1 = workSheet.Cells[2, loopA, 2, loopA + loopC];
                            range1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            Color colFromHexTop = System.Drawing.ColorTranslator.FromHtml("#A6A6A6");
                            range1.Style.Fill.BackgroundColor.SetColor(colFromHexTop);
                            range1.Style.WrapText = true;
                            range1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            range1.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //merge
                            workSheet.Cells[2, loopA, 2, loopA + loopC + ic].Merge = true;
                            //workSheet.Cells[2, loopA, 2, loopA + loopC + ic].AutoFitColumns();
                            //Total Compliance Count


                            foreach (var item2 in lstComplianceDetails)
                            {
                                workSheet.Cells[3, loopA + loopC].Value = item2.ComplainceHeader;
                                workSheet.Cells[1, loopA + loopC].Value = item2.RiskCategory;
                                //Style
                                var range2 = workSheet.Cells[3, loopA + loopC];
                                range2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                Color colFromHexM1 = System.Drawing.ColorTranslator.FromHtml("#DA9694");
                                range2.Style.Fill.BackgroundColor.SetColor(colFromHexM1);
                                workSheet.Column(loopA + loopC).Width = 12;
                                var rangeR = workSheet.Cells[1, loopA + loopC];
                                rangeR.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                Color colFromHexRisk = System.Drawing.ColorTranslator.FromHtml("#BDD7EE");
                                rangeR.Style.Fill.BackgroundColor.SetColor(colFromHexRisk);

                                int recordIndex = 4;
                                var lstExcelMaster = lstMaster.Select(x => new { x.ContractorID, x.ContractorName, x.SubcontractorName, x.ForMonth, x.ProjectName, x.Location, x.LocationID, x.ScheduleOnID, x.ContractorProjectMappingID }).Distinct().ToList();
                                foreach (var item in lstExcelMaster)
                                {
                                    workSheet.Cells[recordIndex, 1].Value = (recordIndex - 3).ToString();
                                    workSheet.Cells[recordIndex, 2].Value = item.ContractorName;
                                    workSheet.Cells[recordIndex, 3].Value = item.SubcontractorName;
                                    workSheet.Cells[recordIndex, 4].Value = item.ForMonth;
                                    workSheet.Cells[recordIndex, 5].Value = item.ProjectName;
                                    workSheet.Cells[recordIndex, 6].Value = item.Location;
                                    workSheet.Cells[recordIndex, 7].Value = "";


                                    ////LstDetails
                                    int loopD = 0;
                                    var lstExcelDetails = lstDetails.Where(x => x.ActID == item1.ActID && x.ComplianceID == item2.ComplianceID && x.ContractorID == item.ContractorID && x.ContractProjMappingScheduleOnID == item.ScheduleOnID && x.ContractProjMappingID == item.ContractorProjectMappingID)
                                                          .Select(x => new { x.ComplianceStatusID }).Distinct().ToList();
                                    foreach (var item3 in lstExcelDetails)
                                    {
                                        if (item3.ComplianceStatusID == 1)
                                        {
                                            workSheet.Cells[recordIndex, loopA + loopC + loopD].Value = "C";
                                            //Style
                                            var rangeD = workSheet.Cells[recordIndex, loopA + loopC + loopD];
                                            rangeD.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            rangeD.Style.Fill.BackgroundColor.SetColor(Color.Green);


                                        }
                                        else if (item3.ComplianceStatusID == 2)
                                        {
                                            workSheet.Cells[recordIndex, loopA + loopC + loopD].Value = "N/C";
                                            //Style
                                            var rangeD = workSheet.Cells[recordIndex, loopA + loopC + loopD];
                                            rangeD.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            rangeD.Style.Fill.BackgroundColor.SetColor(Color.Red);

                                        }
                                        else if (item3.ComplianceStatusID == 3)
                                        {
                                            workSheet.Cells[recordIndex, loopA + loopC + loopD].Value = "N/A";
                                            //Style
                                            var rangeD = workSheet.Cells[recordIndex, loopA + loopC + loopD];
                                            rangeD.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            Color colFromHexNA = System.Drawing.ColorTranslator.FromHtml("#FBBE25");
                                            rangeD.Style.Fill.BackgroundColor.SetColor(colFromHexNA);

                                        }
                                        loopD++;

                                    }
                                    //END                                    
                                    recordIndex++;
                                }
                                loopC++;
                            }
                            //SET Merge Value
                            loopA = loopA + ic;
                            loopA++;
                        }
                        #endregion

                        //Percentage
                        workSheet.Cells[3, loopA].Value = "Compliance (%)";
                        workSheet.Cells[3, loopA + 1].Value = "Non Compliance (%)";
                        workSheet.Cells[3, loopA + 2].Value = "Not Applicable Compliance (%)";
                        workSheet.Cells[3, loopA + 3].Value = "Remarks from CBRE IFM partner";
                        workSheet.Column(loopA).Width = 12;
                        workSheet.Column(loopA + 1).Width = 12;
                        workSheet.Column(loopA + 2).Width = 12;
                        workSheet.Column(loopA + 3).Width = 20;

                        var rangePer = workSheet.Cells[3, loopA, 3, loopA + 2];
                        rangePer.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Color colFromHexPer = System.Drawing.ColorTranslator.FromHtml("#DA9694");
                        rangePer.Style.Fill.BackgroundColor.SetColor(colFromHexPer);

                        var rangeRCBRE = workSheet.Cells[3, loopA + 3];
                        rangeRCBRE.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Color colFromHexCBRE = System.Drawing.ColorTranslator.FromHtml("#FBBE25");
                        rangeRCBRE.Style.Fill.BackgroundColor.SetColor(colFromHexCBRE);
                        rangeRCBRE.Style.TextRotation = 0;
                        rangeRCBRE.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        rangeRCBRE.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rangeRCBRE.Style.WrapText = true;
                        int NoofRow = workSheet.Dimension.End.Row;
                        for (int i = 4; i <= NoofRow; i++)
                        {
                            //Per
                            decimal CCount = 0;
                            decimal NCCount = 0;
                            decimal NACCount = 0;
                            decimal PerForC;
                            decimal PerForNC;
                            decimal PerForNAC;
                            decimal TotalCCount = 0;
                            for (int j = 8; j <= loopA; j++)
                            {
                                var rangeValue = workSheet.Cells[i, j];
                                if (Convert.ToString(rangeValue.Value) == "C")
                                {
                                    CCount = CCount + 1;
                                    TotalCCount = TotalCCount + 1;
                                }
                                else if (Convert.ToString(rangeValue.Value) == "N/C")
                                {
                                    NCCount = NCCount + 1;
                                    TotalCCount = TotalCCount + 1;
                                }
                                else if (Convert.ToString(rangeValue.Value) == "N/A")
                                {
                                    NACCount = NACCount + 1;
                                    TotalCCount = TotalCCount + 1;
                                }
                            }
                            if (TotalCCount > 0)
                            {
                                PerForC = Math.Round(((CCount * 100) / TotalCCount), 2);
                                PerForNC = Math.Round(((NCCount * 100) / TotalCCount), 2);
                                PerForNAC = Math.Round(((NACCount * 100) / TotalCCount), 2);

                                workSheet.Cells[i, loopA].Value = PerForC + "%";
                                workSheet.Cells[i, loopA + 1].Value = PerForNC + "%";
                                workSheet.Cells[i, loopA + 2].Value = PerForNAC + "%";
                            }
                            workSheet.Cells[i, loopA + 3].Value = "";
                        }
                        //END 
                        #endregion
                        #region Part Second
                        #region ActStart

                        loopA = loopA + 4;
                        foreach (var item1 in lstActDetails)
                        {
                            int loopC = 0;

                            var lstComplianceDetails = lstDetails.Where(x => x.ActID == item1.ActID)
                                                          .Select(x => new { x.ComplainceHeader, x.ComplianceID, x.RiskCategory }).Distinct().ToList();
                            int ic = ((lstComplianceDetails.Count()) - 1);
                            workSheet.Cells[2, loopA, 2, loopA + loopC].Value = item1.ActName;

                            //Style                           
                            var range1 = workSheet.Cells[2, loopA, 2, loopA + loopC];
                            range1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            Color colFromHexTop = System.Drawing.ColorTranslator.FromHtml("#A6A6A6");
                            range1.Style.Fill.BackgroundColor.SetColor(colFromHexTop);
                            range1.Style.WrapText = true;
                            range1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            range1.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //merge
                            workSheet.Cells[2, loopA, 2, loopA + loopC + ic].Merge = true;
                            //workSheet.Cells[2, loopA, 2, loopA + loopC + ic].AutoFitColumns();
                            //Total Compliance Count


                            foreach (var item2 in lstComplianceDetails)
                            {
                                workSheet.Cells[3, loopA + loopC].Value = item2.ComplainceHeader;
                                workSheet.Cells[1, loopA + loopC].Value = item2.RiskCategory;
                                //Style
                                var range2 = workSheet.Cells[3, loopA + loopC];
                                range2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                Color colFromHexM1 = System.Drawing.ColorTranslator.FromHtml("#BDD7EE");
                                range2.Style.Fill.BackgroundColor.SetColor(colFromHexM1);
                                workSheet.Column(loopA + loopC).Width = 12;
                                var rangeR = workSheet.Cells[1, loopA + loopC];
                                rangeR.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                Color colFromHexRisk = System.Drawing.ColorTranslator.FromHtml("#BDD7EE");
                                rangeR.Style.Fill.BackgroundColor.SetColor(colFromHexRisk);

                                int recordIndex = 4;
                                var lstExcelMaster = lstMaster.Select(x => new { x.ContractorID, x.ContractorName, x.ForMonth, x.Location, x.LocationID, x.ScheduleOnID, x.ContractorProjectMappingID }).Distinct().ToList();
                                foreach (var item in lstExcelMaster)
                                {
                                    ////LstDetails
                                    int loopD = 0;
                                    var lstExcelDetails = lstDetails.Where(x => x.ComplianceID == item2.ComplianceID && x.ContractorID == item.ContractorID && x.ContractProjMappingScheduleOnID == item.ScheduleOnID && x.ContractProjMappingID == item.ContractorProjectMappingID && x.ActID == item1.ActID)
                                                          .Select(x => new { x.ComplianceChecklistReamrk }).Distinct().ToList();
                                    foreach (var item3 in lstExcelDetails)
                                    {
                                        workSheet.Cells[recordIndex, loopA + loopC + loopD].Value = item3.ComplianceChecklistReamrk;
                                        loopD++;
                                    }
                                    //END                                    
                                    recordIndex++;
                                }
                                loopC++;
                            }
                            //SET Merge Value
                            loopA = loopA + ic;
                            loopA++;
                        }
                        totColumn = loopA;
                        #endregion
                        #endregion

                        using (ExcelRange col = workSheet.Cells[1, 1, 4 + lstMaster.Count, totColumn])
                        {
                            workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells.Style.Font.Name = "Calibri";
                            workSheet.Cells.Style.Font.Size = 9;

                            //workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        }
                        FileInfo excelFile = new FileInfo(FilePath);
                        exportPackge.SaveAs(excelFile);
                    }
                    catch (Exception ex)
                    {
                        FilePath = "";
                    }
                }
                //END
                List<string> lstobj = new List<string>();
                lstobj.Add(FilePath.ToString());
                lstobj.Add(FileName.ToString());
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject("").ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        int GetLastUsedRow(ExcelWorksheet sheet)
        {
            if (sheet.Dimension == null) { return 0; } // In case of a blank sheet
            var row = sheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }
       
        #endregion

        [Route("VendorAudit/SaveFileData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SaveContractorMasterFile(HttpRequestMessage request, int CustID, int UserID, string fileMasterName)
        {
            string message = string.Empty;
            var httpRequest = HttpContext.Current.Request;
            HttpPostedFile file = null;

            try
            {
                if (httpRequest.Files.Count > 0)
                {
                    file = httpRequest.Files[0];
                    if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string fileName = file.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];

                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                if (fileMasterName == "ContractorMaster")
                                {
                                    message = VendorAuditMaster.ProcessContractorFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "UserMaster")
                                {
                                    message = VendorAuditMaster.ProcessUserFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "ProjectMaster")
                                {
                                    message = VendorAuditMaster.ProcessProjectFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else if (fileMasterName == "Contractor-ProjectMapping")
                                {
                                    message = VendorAuditMaster.ProcessProjectMappingFileData(package, fileWithoutExtn, CustID, UserID);
                                }
                                else
                                {
                                    return request.CreateResponse(HttpStatusCode.HttpVersionNotSupported, "error");
                                }
                            }
                        }
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.OK, "WrongFileFormat");
                    }
                }
                return request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new HttpResponseMessage()
                {
                    Content = new StringContent("error", Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/EntityBulkUpload")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage EntityBulkUpload(HttpRequestMessage request, int CustID, int UserID)
        {
            string message = string.Empty;
            var httpRequest = HttpContext.Current.Request;
            HttpPostedFile file = null;

            try
            {
                if (httpRequest.Files.Count > 0)
                {
                    file = httpRequest.Files[0];
                    if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string fileName = file.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];

                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                message = VendorAuditMaster.ProcessEntityFileData(package, fileWithoutExtn, CustID, UserID);
                            }
                        }
                    }
                }
                return request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new HttpResponseMessage()
                {
                    Content = new StringContent("error", Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("VendorAudit/GetAuidtDataPointWithParameter")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAuidtDataPointWithParameter(int CustID, long ProjectContractMappingID, int scheduleonID, int ProjectID, string Period)
        {
            try
            {
                List<ParameterWithvalueForDataPointss> finalResult = new List<ParameterWithvalueForDataPointss>();
                List<long> CMResult = new List<long>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var ObjCM = (from row in entities.Vendor_AuditDataPointParameter
                                 where row.ContractorProjectMappingID == ProjectContractMappingID
                                 && row.ScheduleOnID == scheduleonID
                                 && row.CustomerID == CustID
                                 && row.ProjectID == ProjectID
                                 && row.ForMonth == Period
                                 && row.IsDeleted == false
                                 select row).ToList();

                    var objDP = (from row in entities.Vendor_DataPoints
                                 where row.CustID == CustID
                                 && row.IsDeleted == false
                                 select row).ToList();

                    foreach(var items in objDP)
                    {
                        ParameterWithvalueForDataPointss obj = new ParameterWithvalueForDataPointss();

                        var result = ObjCM.Where(x => x.Description == items.Description && x.DescType == items.DescType).Select(x=>x.DescriptionValue).FirstOrDefault();

                        obj.Description = items.Description;
                        obj.DescType = items.DescType;
                        obj.DescriptionValue = result;

                        if (ObjCM.Count == objDP.Count)
                        {
                            obj.DescriptionExists = "1";
                        }
                        else
                        {
                            obj.DescriptionExists = "0";

                        }
                        finalResult.Add(obj);
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(finalResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
