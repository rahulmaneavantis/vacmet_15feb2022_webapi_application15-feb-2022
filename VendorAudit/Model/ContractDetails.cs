﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.VendorAudit.Model
{
    public class DashboradContractorTypes
    {
        public string ContractorType { get; set; }
        public string AuditStatus { get; set; }
        public int Open { get; set; }
        public int Overdue { get; set; }
        public int Underreview { get; set; }
        public int Closed { get; set; }
        public int ProjectID { get; set; }
        public int ContractorTypeID { get; set; }
        public Nullable<DateTime> PeriodStartDate { get; set; }
        public Nullable<DateTime> PeriodEndDate { get; set; }
    }
  
    public class UserDetails
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string Designation { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> DeactivatedOn { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Nullable<long> ReportingToID { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }

        public Nullable<System.DateTime> LastLoginTime { get; set; }
        public Nullable<System.DateTime> ChangPasswordDate { get; set; }
        public Nullable<bool> ChangePasswordFlag { get; set; }
        public Nullable<System.DateTime> NotLongerLoginDate { get; set; }
        public int WrongAttempt { get; set; }
        public Nullable<bool> IsHead { get; set; }
        public Nullable<long> AuditorID { get; set; }
        public string IsAuditHeadOrMgr { get; set; }
        public Nullable<bool> SendEmail { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
        public Nullable<int> LawyerFirmID { get; set; }
        public Nullable<bool> IsExternal { get; set; }
        public Nullable<int> LitigationRoleID { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public Nullable<System.DateTime> AuditStartPeriod { get; set; }
        public Nullable<System.DateTime> AuditEndPeriod { get; set; }
        public Nullable<int> ContractRoleID { get; set; }
        public string UserName { get; set; }
        public Nullable<int> LicenseRoleID { get; set; }
        public string PAN { get; set; }
        public Nullable<int> VendorRoleID { get; set; }
        public Nullable<bool> DesktopRestrict { get; set; }
        public string IMEINumber { get; set; }
        public Nullable<bool> IMEIChecked { get; set; }
        public string EnType { get; set; }
        public Nullable<bool> MobileAccess { get; set; }
        public Nullable<int> SecretarialRoleID { get; set; }
        public Nullable<int> HRRoleID { get; set; }
        public Nullable<int> InsiderRoleID { get; set; }
        public Nullable<bool> SSOAccess { get; set; }
        public Nullable<int> Cer_OwnerRoleID { get; set; }
        public Nullable<int> Cer_OfficerRoleID { get; set; }
        public string LogOutURL { get; set; }
        public Nullable<int> CreatedFrom { get; set; }
        public Nullable<int> IsCertificateVisible { get; set; }

        public string Status { get; set; }

    }
    public class RoleDTO
    {

        public int ID { get; set; }
        public string Name { get; set; }


    }
    public class ProjectDTO
    {
        public int PID { get; set; }
        // Other field you may need from the Product entity
    }
    public class ContractDetails
    {
        public int PID { get; set; }
        public int ContractorID { get; set; }
        public string ContractorName { get; set; }
        public string SPOCName { get; set; }
        public string SPOCEmailID { get; set; }
        public string SPOCMobileNo { get; set; }

        public string NatureOfWork { get; set; }

        public int ContractorTypeID { get; set; }

        public string ProjectMappingName { get; set; }

        public int ProjectMappingID { get; set; }

        public int HeadCount { get; set; }
        public int LocationID { get; set; }
        public int StateID { get; set; }
        public string LocationName { get; set; }

        public string ISContractor { get; set; }
        
        public string IsDisableValues { get; set; }

    }
    public partial class Vendor_ContractorProjectMappingNew
    {
        public int ID { get; set; }
        public int CustID { get; set; }
        public int ContractorID { get; set; }
        public int ProjectID { get; set; }
        public int IsSubcontract { get; set; }
        public Nullable<int> UnderContractorID { get; set; }
        public string Frequency { get; set; }
        public System.DateTime DueDate { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<System.DateTime> Activationdate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> ClosureDate { get; set; }
        public Nullable<int> Isdisable { get; set; }
        public int DueDays { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public int IsShowContractor { get; set; }
        public Nullable<System.DateTime> ContractStartDate { get; set; }
        public Nullable<System.DateTime> ContractEndDate { get; set; }
        public Nullable<long> ProjectContSpocID { get; set; }
        public Nullable<long> ProjectSubContSpocID { get; set; }

        public string SPOCEmailID { get; set; }
        public string SPOCName { get; set; }
        public string SPOCContactNumber { get; set; }
        public string NatureOfWork { get; set; }

        public string SubContractorEmail { get; set; }


    }

    public class CustomerLocationDetails
    {
        public string Address { get; set; }
        public int StateID { get; set; }
        public int CityID { get; set; }

    }
}