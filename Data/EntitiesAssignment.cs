//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EntitiesAssignment
    {
        public int ID { get; set; }
        public long UserID { get; set; }
        public long BranchID { get; set; }
        public long ComplianceCatagoryID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
