//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_MeetingAttendance
    {
        public long Id { get; set; }
        public long MeetingParticipantId { get; set; }
        public long MeetingId { get; set; }
        public string Attendance { get; set; }
        public Nullable<int> DesignationId { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int Createdby { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public string RSVP { get; set; }
        public string RSVP_Reason { get; set; }
    }
}
