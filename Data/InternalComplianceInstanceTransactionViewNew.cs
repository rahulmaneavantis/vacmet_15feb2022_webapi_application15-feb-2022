//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalComplianceInstanceTransactionViewNew
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
        public string ApproverName { get; set; }
        public Nullable<byte> Risk { get; set; }
        public string ShortDescription { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public Nullable<int> CustomerBranchStatus { get; set; }
        public string Branch { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public long ScheduledOnID { get; set; }
        public string ForMonth { get; set; }
        public Nullable<long> ComplianceTransactionID { get; set; }
        public Nullable<int> ComplianceStatusID { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> StatusChangedOn { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public string ComCategoryName { get; set; }
        public Nullable<int> ComplianceTypeId { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<long> PerformerID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsUpcomingNotDeleted { get; set; }
        public string ActName { get; set; }
        public Nullable<int> ActID { get; set; }
        public Nullable<System.DateTime> PerformerDated { get; set; }
        public Nullable<System.DateTime> ReviewerDated { get; set; }
        public string RiskCategory { get; set; }
        public string OriginalPerformerName { get; set; }
        public string OriginalReviewerName { get; set; }
        public string ReportName { get; set; }
        public string ShortForm { get; set; }
        public string PenaltyDescription { get; set; }
        public string DetailedDescription { get; set; }
    }
}
