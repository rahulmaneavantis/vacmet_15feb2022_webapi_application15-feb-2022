//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class Cont_SP_AssignMilestoneDetailNew_Result
    {
        public Nullable<long> RowID { get; set; }
        public long ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long DeptID { get; set; }
        public long UserID { get; set; }
        public System.DateTime DueDate { get; set; }
        public long StatusID { get; set; }
        public string Remark { get; set; }
        public string DeptName { get; set; }
        public string AssignUserName { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public long MilestoneMasterID { get; set; }
        public long ContractTemplateID { get; set; }
    }
}
