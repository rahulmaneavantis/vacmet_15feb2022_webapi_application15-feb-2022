//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_Track_User_Activities
    {
        public long Track_ID { get; set; }
        public string UserID { get; set; }
        public string Page { get; set; }
        public string Actitvity { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string SessionID { get; set; }
        public string Client { get; set; }
        public string Type { get; set; }
        public string RecordID { get; set; }
        public string ProfileID { get; set; }
    }
}
