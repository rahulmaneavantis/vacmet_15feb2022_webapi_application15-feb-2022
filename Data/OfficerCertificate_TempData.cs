//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class OfficerCertificate_TempData
    {
        public string ForMonth { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public long OwnerUserID { get; set; }
        public long OfficerUserID { get; set; }
        public long CustomerID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceScheduleonID { get; set; }
        public long ComplianceID { get; set; }
        public long ComplianceStatusID { get; set; }
        public string SubmittedFlag { get; set; }
        public long CerScheduleonID { get; set; }
        public long PerRevId { get; set; }
        public long RoleId { get; set; }
    }
}
