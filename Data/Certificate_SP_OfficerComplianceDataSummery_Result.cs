//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class Certificate_SP_OfficerComplianceDataSummery_Result
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public Nullable<byte> Risk { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public int ComplianceStatusID { get; set; }
        public int RoleID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsUpcomingNotDeleted { get; set; }
        public long UserID { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public long ScheduledOnID { get; set; }
        public Nullable<bool> EventFlag { get; set; }
        public Nullable<bool> ComplinceVisible { get; set; }
        public Nullable<int> ComplianceTypeId { get; set; }
        public string ShortDescription { get; set; }
        public int ActID { get; set; }
        public string Description { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
        public string Status { get; set; }
        public int NewStatusID { get; set; }
        public string beforeSubmitStatus { get; set; }
        public long OldStatusID { get; set; }
        public string ActName { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string RiskCategory { get; set; }
        public string User { get; set; }
        public Nullable<System.DateTime> PerformerDated { get; set; }
        public Nullable<System.DateTime> ReviewerDated { get; set; }
        public string OriginalPerformerName { get; set; }
        public string OriginalReviewerName { get; set; }
        public Nullable<long> DepartmentID { get; set; }
        public string SequenceID { get; set; }
        public string Sections { get; set; }
        public string ComplianceType { get; set; }
        public string PerformerRemark { get; set; }
        public string ReviewerRemark { get; set; }
        public string BankName { get; set; }
        public string ChallanNo { get; set; }
        public string ChallanAmount { get; set; }
        public Nullable<System.DateTime> Challanpaiddate { get; set; }
        public Nullable<decimal> Penalty { get; set; }
        public string SubmittedFlag { get; set; }
        public long OwnerUserID { get; set; }
        public long OfficerUserID { get; set; }
        public long CerScheduleonID { get; set; }
        public string CerForMonth { get; set; }
        public long PerRevId { get; set; }
    }
}
