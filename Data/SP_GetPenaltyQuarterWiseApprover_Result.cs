//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class SP_GetPenaltyQuarterWiseApprover_Result
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public Nullable<byte> Risk { get; set; }
        public Nullable<byte> NonComplianceType { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public int ComplianceStatusID { get; set; }
        public int RoleID { get; set; }
        public Nullable<decimal> Penalty { get; set; }
        public Nullable<decimal> Interest { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsUpcomingNotDeleted { get; set; }
        public string PenaltySubmit { get; set; }
        public long UserID { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public long ScheduledOnID { get; set; }
        public Nullable<decimal> totalvalue { get; set; }
        public string ActName { get; set; }
        public string BranchName { get; set; }
        public string ShortDescription { get; set; }
        public int ActID { get; set; }
        public string UserName { get; set; }
        public Nullable<bool> IsPenaltySave { get; set; }
        public string Remarks { get; set; }
    }
}
