//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class SP_SubDetailLockingDayRev_Result
    {
        public long ID { get; set; }
        public long DetailOfLockingDayID { get; set; }
        public long ScheduleOnID { get; set; }
        public System.DateTime DueDate { get; set; }
        public int DueDay { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string Remark { get; set; }
    }
}
