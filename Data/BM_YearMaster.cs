//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_YearMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BM_YearMaster()
        {
            this.BM_Meetings = new HashSet<BM_Meetings>();
        }
    
        public long FYID { get; set; }
        public string Type { get; set; }
        public string FYText { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public bool IsForHistorical { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BM_Meetings> BM_Meetings { get; set; }
    }
}
