//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class SP_NotificationReport_Result
    {
        public string TypeOfMail { get; set; }
        public long UserID { get; set; }
        public System.DateTime SentOn { get; set; }
        public string UserName { get; set; }
    }
}
