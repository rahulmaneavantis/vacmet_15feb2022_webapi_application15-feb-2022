//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.Data
{
    using System;
    
    public partial class sp_ComplianceInstanceAssignmentViewDetails_Result
    {
        public int ComplianceAssignmentID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public string ShortDescription { get; set; }
        public int CustomerBranchID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Section { get; set; }
        public int CustomerID { get; set; }
        public string Branch { get; set; }
        public string User { get; set; }
        public string Performer { get; set; }
        public string Reviewer { get; set; }
        public int ActID { get; set; }
        public string Name { get; set; }
        public string ComplianceTypeName { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public int ComplianceTypeID { get; set; }
        public System.DateTime StartDate { get; set; }
        public string ComplianceCategoryName { get; set; }
        public string Frequency { get; set; }
    }
}
