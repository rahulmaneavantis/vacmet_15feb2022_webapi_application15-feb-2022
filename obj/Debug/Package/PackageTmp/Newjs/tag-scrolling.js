﻿
        var hidWidth;
var scrollBarWidths = 40;

var widthOfList = function () {
    debugger;
    var itemsWidth = 0;
    $('.item').each(function () {
        var itemWidth = $(this).outerWidth();
        itemsWidth += itemWidth;
    });
    //alert(itemsWidth);
    return itemsWidth;
};

var widthOfHidden = function () {
    return (($('.wrapper').outerWidth()) - widthOfList() - getLeftPosi()) - scrollBarWidths;
};

var getLeftPosi = function () {            
    if ($('div.divFileTags') != null || $('div.divFileTags') != undefined)
        return $('div.divFileTags').position().left;
    else
        return 0;
};

var reAdjust = function () {
    debugger;

    var $elem = $('div.divFileTags');
    if ($elem != null || $elem != undefined) {
        var newScrollLeft = $elem.scrollLeft(),
            width = $elem.outerWidth();

        var scrollWidth = 0;

        if ($elem.get(0) != null || $elem.get(0)!=undefined)
            scrollWidth = $elem.get(0).scrollWidth;

        if (scrollWidth - newScrollLeft == width) {
            $('.scroller-right').fadeOut('slow');
        }
        else
            $('.scroller-right').fadeIn('slow');
                
        if ($elem.scrollLeft() <= 0) {
            $('.scroller-left').fadeOut('slow');
        }
        else {
            $('.scroller-left').fadeIn('slow');
        }
    }
    //var outerW=$('.wrapper').outerWidth();
    //var listW = widthOfList();
    //if (outerW < listW) {
    //    $('.scroller-right').show();
    //}
    //else {
    //    $('.scroller-right').hide();               
    //}

}

//reAdjust();

$(window).on('resize', function (e) {
    reAdjust();
});

$(document).ready(function () {
    $("#rightArrow").click(function () {            
        $('div.divFileTags').animate({ scrollLeft: "+=1000" }, 'slow', function () {
            reAdjust();
        });
    });

    $("#leftArrow").click(function () {            
        $('div.divFileTags').animate({ scrollLeft: "-=1000" }, 'slow', function () {
            reAdjust();
        });
    });
});

$('div.divFileTags').on("mousewheel", function (e, delta) {
    this.scrollLeft -= (50 * 40);
    e.preventDefault();
});