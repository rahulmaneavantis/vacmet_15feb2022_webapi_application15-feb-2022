﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadMyDocuments.aspx.cs" Inherits="AppWebApplication.Download_View.DownloadMyDocuments" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style>
        .k-window-titlebar.k-header {
            display: none;
        }
        div.k-window-content {
            position: relative;
            height: 100%;
            padding: .58em;
            overflow: auto;
            outline: 0;
            margin-top: -20px;
        }
    </style>
    <script>
        function fopendocfileLitigation(file) {
            $("#divdocviewer").kendoWindow({
                width: "91%",
                height: "480px",
                title: "Document Viewer",
                content: "docviewer.aspx?docurl=" + file,
                visible: false,
                scrollable: false,
                iframe: false,
                draggable: false,
                position: {
                    top: "2%",
                    left: "8.7%"
                },
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ]
            }).data("kendoWindow").open();

            $('#divdocviewer').parent().find('.k-window-title').css('color', 'deepskyblue');
        }
        function NoDcoumentAlert() {
            alert("No Document Available for Download.");
        }

    </script>
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
            <div id="divViewDocument22" style="float:left;width:9%">
                    <table width="100%" style="text-align: left;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptLitigationVersionView" runat="server" OnItemCommand="rptLitigationVersionView_ItemCommand"
                                                OnItemDataBound="rptLitigationVersionView_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th><b>File Name</b></th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Container.ItemIndex+1 %>.
                                                            <asp:UpdatePanel ID="UpLinkbuttonbold" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ContractProjectMappingId") + ","+ Eval("FileName")+ ","+ Eval("ScheduleOnId")%>'
                                                                        ID="lblDocumentVersionView1" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                                        Text='<%# Eval("FileName").ToString().Trim().Length > 12 ? Eval("FileName").ToString().Substring(0,12) + "..." : Eval("FileName").ToString().Trim() %>' CausesValidation="false"></asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
        <div id="divdocviewer"></div>
    </form>
</body>
</html>
