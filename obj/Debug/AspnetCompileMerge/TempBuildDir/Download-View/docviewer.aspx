﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="docviewer.aspx.cs" Inherits="AppWebApplication.Download_View.docviewer" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <GleamTech:DocumentViewer runat="server" Width="100%" ID="doccontrol" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
            DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />
    </div>   
    </form>
</body>
</html>
