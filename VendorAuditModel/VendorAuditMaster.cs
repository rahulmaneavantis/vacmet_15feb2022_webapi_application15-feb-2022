﻿using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using AppWebApplication.Models;
using AppWebApplication.VendorAudit.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using VendorAuditData;

namespace AppWebApplication.VendorAuditModel
{
    public class VendorAuditMaster
    {
        public static bool Delete_PerDoc(int ID)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    var queryResult = (from row in entities.Vendor_tbl_FileTransactionData
                                       where row.ID == ID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        internal static string ProcessEntityFileData(ExcelPackage package, string fileName, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["EntityBulkUpload"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);
                    List<String> LogErrors = new List<string>();

                    List<CustomerBranch> LstObj = new List<CustomerBranch>();
                    //List<CustomerBranch> LstObjtest = new List<CustomerBranch>();


                    if (noOfRow > 1)
                    {
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;
                            CustomerBranch obj = new CustomerBranch();
                            //Entity Name
                            string entityName = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (LstObj.Count > 0)
                            {
                                foreach (var item in LstObj)
                                {
                                    if (item.Name != null)
                                    {
                                        if (entityName == item.Name.Trim())
                                        {
                                            LogErrors.Add("Entity Name : " + entityName + " already exist at Row-" + count);
                                        }
                                    }
                                }
                            }


                            if (string.IsNullOrEmpty(entityName))
                            {
                                LogErrors.Add("Required Entity Name at Row - " + count);
                            }
                            else
                            {
                                if (CheckEntityNameExist(entityName, CustID) == false)
                                {
                                    LogErrors.Add("Entity Name : " + entityName + " already exist at Row-" + count);
                                    obj.Name = entityName;
                                }
                                else
                                    obj.Name = entityName;
                            }
                            //Sub Entity Type
                            string subEntityType = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (string.IsNullOrEmpty(subEntityType))
                            {
                                LogErrors.Add("Required Sub Entity Type at Row - " + count);
                            }
                            else
                            {

                                int type = GetSubEntityTypeByName(subEntityType);


                                if (type > 0)
                                    obj.Type = Convert.ToByte(type);
                                else
                                {
                                    LogErrors.Add("Sub Entity Type is not found. Please check at Row - " + count);
                                }
                            }
                            //Type
                            string entityType = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(entityType))
                            {
                                LogErrors.Add("Required Type at Row - " + count);
                            }
                            else
                            {
                                int comType = GetEntityCompanyTypeByName(entityType);
                                if (comType > 0)
                                    obj.ComType = comType;
                                else
                                {
                                    LogErrors.Add("Type is not found. Please check at Row - " + count);
                                }
                            }

                            //Legal Entity Type
                            string legalEntityType = xlWorksheet.Cells[i, 4].Text.Trim();
                            if (string.IsNullOrEmpty(legalEntityType))
                            {
                                LogErrors.Add("Required Legal Entity Type at Row - " + count);
                            }
                            else
                            {
                                int legalEnityType = GetLegalEntityTypebyName(legalEntityType);
                                if (legalEnityType > 0)
                                    obj.LegalEntityTypeID = legalEnityType;
                                else
                                {
                                    LogErrors.Add("Legal Entity Type is not found. Please check at Row - " + count);
                                }
                            }
                            //Legal Relationship
                            string legalRelationType = xlWorksheet.Cells[i, 5].Text.Trim();

                            if (!string.IsNullOrEmpty(legalRelationType))
                            {
                                if (legalRelationType == "Parent")
                                    obj.LegalRelationShipID = 0;
                                else if (legalRelationType == "Subsidiary Company")
                                    obj.LegalRelationShipID = 1;
                                else if (legalRelationType == "Joint Venture")
                                    obj.LegalRelationShipID = 2;
                                else
                                {
                                    LogErrors.Add("Legal Relationship is not found. Please check at Row - " + count);
                                }
                            }

                            //Industry Type
                            string industryType = xlWorksheet.Cells[i, 6].Text.Trim();
                            if (string.IsNullOrEmpty(industryType))
                            {
                                LogErrors.Add("Required Industry Type at Row - " + count);
                            }
                            else
                            {
                                List<int> indus = new List<int>();
                                indus = GetEntityIndustryByName(industryType);

                                if (indus != null)
                                {
                                    if (indus.Count > 0)
                                        obj.Territory = string.Join(",", indus.Select(n => n.ToString()).ToArray());
                                    else
                                    {
                                        LogErrors.Add("Industry Type is not found. Please check at Row - " + count);
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("One of Industry Type is not found. Please check at Row - " + count);
                                }
                            }
                            //State
                            string state = xlWorksheet.Cells[i, 7].Text.Trim();
                            if (string.IsNullOrEmpty(state))
                            {
                                LogErrors.Add("Required State at Row - " + count);
                            }
                            else
                            {
                                int stateID = GetStateByName(state);
                                if (stateID > 0)
                                    obj.StateID = stateID;
                                else
                                {
                                    LogErrors.Add("State is not found. Please check at Row - " + count);
                                }
                            }


                            //City
                            string city = xlWorksheet.Cells[i, 8].Text.Trim();
                            if (string.IsNullOrEmpty(state))
                            {
                                LogErrors.Add("Required City at Row - " + count);
                            }
                            else
                            {
                                int cityID = GetCityByName(city);
                                if (cityID > 0)
                                    obj.CityID = cityID;
                                else
                                {
                                    LogErrors.Add("City is not found. Please check at Row - " + count);
                                }
                            }



                            //Address 1
                            string address = xlWorksheet.Cells[i, 9].Text.Trim();
                            if (string.IsNullOrEmpty(address))
                            {
                                LogErrors.Add("Required Address 1 at Row - " + count);
                            }
                            else
                                obj.AddressLine1 = address;

                            //Address 2

                            obj.AddressLine2 = xlWorksheet.Cells[i, 10].Text.Trim();

                            //Contact Person
                            string contactPerson = xlWorksheet.Cells[i, 11].Text.Trim();
                            if (string.IsNullOrEmpty(contactPerson))
                            {
                                LogErrors.Add("Required Contact Person at Row - " + count);
                            }
                            else
                                obj.ContactPerson = contactPerson;

                            //Contact Person Email
                            string contactPersonEmail = xlWorksheet.Cells[i, 12].Text.Trim();
                            if (string.IsNullOrEmpty(contactPersonEmail))
                            {
                                LogErrors.Add("Required Contact Person Email at Row - " + count);
                            }
                            else
                            {
                                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                Match match = regex.Match(contactPersonEmail);
                                if (match.Success)
                                    obj.EmailID = contactPersonEmail;
                                else
                                    LogErrors.Add("Contact Person Email ID is not valid. Please check at Row - " + count);

                            }
                            //LandLine No
                            string landLineNo = xlWorksheet.Cells[i, 13].Text.Trim();
                            obj.Landline = landLineNo;

                            //Pin Code
                            string pinCode = xlWorksheet.Cells[i, 14].Text.Trim();
                            obj.PinCode = pinCode;

                            //Mobile Number
                            string mobileNumber = xlWorksheet.Cells[i, 15].Text.Trim();

                            if (!string.IsNullOrEmpty(mobileNumber))
                            {

                                if (mobileNumber.Length != 10)
                                    LogErrors.Add("Contact Person mobile number is not valid. Please check at Row - " + count);

                                else
                                    obj.Mobile = mobileNumber;
                            }



                            //Status
                            string Status = xlWorksheet.Cells[i, 16].Text.Trim();
                            if (string.IsNullOrEmpty(contactPersonEmail))
                            {
                                LogErrors.Add("Required Status at Row - " + count);
                            }
                            else
                            {
                                if (Status == "Inactive")
                                    obj.Status = 0;
                                else if (Status == "Active")
                                    obj.Status = 1;
                                else if (Status == "Suspended")
                                    obj.Status = 2;
                                else
                                {
                                    LogErrors.Add("Status is not found. Please check at Row - " + count);
                                }
                            }

                            //Person Responsible Applicable
                            string personResponsible = xlWorksheet.Cells[i, 17].Text.Trim();

                            if (!string.IsNullOrEmpty(personResponsible) && personResponsible != "Yes" && personResponsible != "No")
                            {
                                LogErrors.Add("Person Responsible Applicable should be Yes or No. Please check at Row - " + count);
                            }

                            if (personResponsible == "Yes")
                                obj.AuditPR = true;
                            else
                                obj.AuditPR = false;

                            LstObj.Add(obj);
                        }

                        //LstObj.Add(obj);

                        if (LogErrors.Count <= 0)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                foreach (var addEntity in LstObj)
                                {
                                    CustomerBranch objEntity = new CustomerBranch();
                                    objEntity.ID = 0;
                                    objEntity.Name = addEntity.Name;
                                    objEntity.Type = addEntity.Type;
                                    objEntity.ComType = addEntity.ComType;
                                    objEntity.LegalEntityTypeID = addEntity.LegalEntityTypeID;
                                    objEntity.LegalRelationShipID = addEntity.LegalRelationShipID;
                                    objEntity.Industry = 0;
                                    objEntity.StateID = addEntity.StateID;
                                    objEntity.CityID = addEntity.CityID;
                                    objEntity.CreatedOn = DateTime.Today;
                                    objEntity.CreatedBy = UserID;
                                    objEntity.CustomerID = CustID;
                                    objEntity.IsDeleted = false;
                                    objEntity.AddressLine1 = addEntity.AddressLine1;
                                    objEntity.AddressLine2 = addEntity.AddressLine2;
                                    objEntity.ContactPerson = addEntity.ContactPerson;
                                    objEntity.EmailID = addEntity.EmailID;
                                    objEntity.Landline = addEntity.Landline;
                                    objEntity.PinCode = addEntity.PinCode;
                                    objEntity.Mobile = addEntity.Mobile;
                                    objEntity.Status = addEntity.Status;
                                    objEntity.AuditPR = addEntity.AuditPR;
                                    objEntity.Territory = addEntity.Territory;
                                    //entities.CustomerBranches.Add(addEntity);
                                    //entities.SaveChanges();


                                    entities.CustomerBranches.Add(objEntity);
                                    entities.SaveChanges();


                                    //addEntity.Industry
                                    var det = new CustomerBranchIndustryMapping();
                                    det.CustomerBranchID = objEntity.ID;
                                    det.IsActive = true;
                                    det.EditedBy = Convert.ToInt32(addEntity.CreatedBy);
                                    det.EditedDate = DateTime.Now;


                                    List<int> StrArray = addEntity.Territory.Split(',').Select(int.Parse).ToList();

                                    //StrArray = StrArray.Distinct().ToArray();
                                    foreach (var item in StrArray)
                                    {

                                        det.IndustryID = item;
                                        entities.CustomerBranchIndustryMappings.Add(det);
                                        entities.SaveChanges();
                                        message = "success";

                                    }


                                    using (AuditControlEntities mst_entities = new AuditControlEntities())
                                    {
                                        mst_CustomerBranch customerBranch1 = new mst_CustomerBranch()
                                        {
                                            Name = addEntity.Name,
                                            Type = addEntity.Type,
                                            ComType = Convert.ToByte(addEntity.ComType),
                                            AddressLine1 = addEntity.AddressLine1.Trim(),
                                            AddressLine2 = addEntity.AddressLine2.Trim(),
                                            StateID = Convert.ToInt32(addEntity.StateID),
                                            CityID = Convert.ToInt32(addEntity.CityID),
                                            //Others = tbxOther.Text.Trim(),
                                            PinCode = addEntity.PinCode,
                                            Industry = Convert.ToInt32(addEntity.Industry),
                                            ContactPerson = addEntity.ContactPerson.Trim(),
                                            Landline = addEntity.Landline,
                                            Mobile = addEntity.Mobile,
                                            EmailID = addEntity.EmailID.Trim(),
                                            CustomerID = CustID,
                                            ParentID = addEntity.ParentID,
                                            Status = Convert.ToInt32(addEntity.Status),

                                            IsDeleted = addEntity.IsDeleted,
                                            LegalRelationShipID = addEntity.LegalRelationShipID,
                                            LegalEntityTypeID = addEntity.LegalEntityTypeID,
                                            AuditPR = addEntity.AuditPR,
                                            CreatedOn = DateTime.Today,
                                        };
                                        //mst_entities.Entry(customerBranch1).State = System.Data.Entity.EntityState.Modified;
                                        mst_entities.mst_CustomerBranch.Add(customerBranch1);
                                        mst_entities.SaveChanges();
                                    }

                                    message = "success";

                                }

                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, fileName, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        message = "norecord";
                    }
                }
                else
                {
                    message = "nofile";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }

        public static int GetSubEntityTypeByName(string SubEntityName)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var subEntity = entities.NodeTypes;

                NodeType subEntitynew = subEntity.Where(x => x.Name == SubEntityName).ToList().FirstOrDefault();


                if (subEntitynew == null)
                {
                    return 0;
                }
                else
                    return subEntitynew.ID;
            }
        }

        public static int GetEntityCompanyTypeByName(string EntityCompanyType)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                int ComanyType = (from row in entities.CompanyTypes
                                  where row.ID <= 3 && row.Name == EntityCompanyType
                                  select row.ID).FirstOrDefault();

                //var ComanyType = entities.CompanyTypes;

                return ComanyType;
            }
        }


        public static int GetLegalEntityTypebyName(string LegalEntityName)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var legalEntityType = entities.M_LegalEntityType;

                M_LegalEntityType legalEntity = legalEntityType.Where(x => x.EntityTypeName == LegalEntityName).ToList().FirstOrDefault();

                if (legalEntity == null)
                {
                    return 0;
                }
                else
                    return legalEntity.ID;
            }
        }


        public static List<int> GetEntityIndustryByName(string IndustryName)
        {

            var StrArray = IndustryName.Split(',');

            List<int> newIndusID = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Industries;

                foreach (var item in StrArray)
                {
                    Industry industrEntitynew = industries.Where(x => x.Name == item).ToList().FirstOrDefault();

                    if (industrEntitynew != null)
                        newIndusID.Add(industrEntitynew.ID);
                    else
                    {
                        newIndusID = null;
                    }
                }

                return newIndusID;
            }
        }

        public static int GetStateByName(string State)
        {
            try
            {

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int stateID = (from row in entities.States
                                   where row.Name == State && row.IsDeleted == false
                                   select row.ID).Single();

                    return stateID;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static int GetCityByName(string City)
        {
            try
            {

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    entities.Configuration.LazyLoadingEnabled = false;
                    int cityID = (from row in entities.Vendor_CityMaster
                                  where row.Name == City && row.IsDeleted == false
                                  select row.ID).Single();

                    return cityID;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


        internal static List<ContractDetails> GetContractDetail(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();

                foreach (var row in ContractorObj)
                {
                    ContractDetails Details = new ContractDetails();
                    Details.PID = row.PID;
                    Details.ContractorID = row.ID;
                    Details.ContractorName = row.ContractorName;
                    Details.SPOCName = row.SpocName;
                    Details.SPOCEmailID = row.SpocEmail;
                    Details.SPOCMobileNo = row.SpocContactNo;
                    Details.NatureOfWork = row.NatureOfWork;
                    Details.ContractorTypeID = (int)row.ContractorType;
                    ContractorDetailsList.Add(Details);
                }
                ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                return ContractorDetailsList.ToList();
            }
        }

        internal static bool IsNumber(string DueDays)
        {
            bool IsNumber = true;
            char[] array = DueDays.ToCharArray();
            foreach (var c in array)
            {
                if (!Char.IsNumber(c)) return IsNumber = false;
            }
            return IsNumber;
        }

        internal static List<Vendor_ContractorProjectMapping> GetSavedMappings(int CustID)
        {
            List<Vendor_ContractorProjectMapping> SavedMappings = new List<Vendor_ContractorProjectMapping>();
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                SavedMappings = (from row in entities.Vendor_ContractorProjectMapping
                                 where row.CustID == CustID && row.IsDelete == false
                                 select row).ToList();
            }
            return SavedMappings;
        }

        internal static string ProcessProjectMappingFileData(ExcelPackage package, string fileWithoutExtn, int custID, int userID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["Contractor-ProjectMapping"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);

                    if (noOfRow >= 2)
                    {
                        List<String> LogErrors = new List<string>();
                        List<Vendor_ContractorProjectMappingNew> LstObj = new List<Vendor_ContractorProjectMappingNew>();
                        List<ContractDetails> ContractDetails = GetContractDetail(custID);
                        List<Vendor_SP_GetProjectMaster_Result> ProjectMaster = GetProjectMaster(custID);
                        List<Vendor_ContractorProjectMapping> SavedMappings = GetSavedMappings(custID);


                        string[] freqArray = new string[] { "Monthly", "Quarterly", "Half Yearly", "Yearly", "One Time" };

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            Vendor_ContractorProjectMappingNew obj = new Vendor_ContractorProjectMappingNew();

                            //ContractorName
                            string ContractorMailID = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(ContractorMailID)) LogErrors.Add("Required Contractor EmailID at Row-" + count);
                            else
                            {
                                var contract = ContractDetails.Where(x => x.SPOCEmailID == ContractorMailID).Select(x => x).SingleOrDefault();
                                if (contract == null)
                                    LogErrors.Add("Enter valid contractor/subcontractor EmailID at Row-" + count);
                                else obj.ContractorID = contract.ContractorID;
                            }

                            //Project/Office Name
                            string ProjectName = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (string.IsNullOrEmpty(ProjectName)) LogErrors.Add("Required Name at Row-" + count);
                            else
                            {
                                int ProjectID = 0;
                                foreach (var project in ProjectMaster)
                                {
                                    if (project.Name.Trim() == ProjectName) ProjectID = project.ID;
                                }
                                if (ProjectID > 0) obj.ProjectID = ProjectID;
                                else LogErrors.Add("Enter valid Project Name at Row-" + count);

                                var mapping = SavedMappings.Where(x => x.ProjectID == ProjectID && x.ContractorID == obj.ContractorID).Select(y => y).FirstOrDefault();
                                if (mapping != null) LogErrors.Add("Project and Contractor mapping already exist at Row-" + count);
                            }

                            //IsSubContractor
                            string IsSubContractor = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(IsSubContractor)) LogErrors.Add("Required IsSubContractor(Yes/No) at Row-" + count);
                            else
                            {
                                if (IsSubContractor == "Yes")
                                {
                                    obj.IsSubcontract = 1;

                                    //SubContractor EmailID
                                    string SubContractorEmailID = xlWorksheet.Cells[i, 4].Text.Trim();
                                    if (string.IsNullOrEmpty(SubContractorEmailID)) LogErrors.Add("Required SubContractor Email at Row-" + count);
                                    else
                                    {
                                        int SubContractorID = ContractDetails.Where(x => x.SPOCEmailID.Trim() == SubContractorEmailID && x.ContractorID != obj.ContractorID).Select(x => x.ContractorID).SingleOrDefault();
                                        if (SubContractorID > 0) obj.UnderContractorID = SubContractorID;
                                        else
                                        {
                                            //Commented by Vishal
                                            //LogErrors.Add("Enter valid SubContractor Email at Row-" + count);

                                            ////Added by Vishal
                                            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                            Match match = regex.Match(SubContractorEmailID);

                                            if (match.Success)
                                            {

                                                obj.SubContractorEmail = SubContractorEmailID;
                                            }
                                            else
                                            {
                                                LogErrors.Add("Please enter valid Sub Contractor Email ID at Row-" + count);
                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    obj.IsSubcontract = 0;
                                    obj.UnderContractorID = null;
                                }
                            }

                            //SPOCEmail
                            string SPOCEmail = xlWorksheet.Cells[i, 5].Text.Trim();
                            if (string.IsNullOrEmpty(SPOCEmail)) LogErrors.Add("Required SPOC Email ID at Row-" + count);
                            else
                            {
                                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                Match match = regex.Match(SPOCEmail);
                                if (match.Success)
                                {

                                    obj.SPOCEmailID = SPOCEmail;

                                    //SPOC Name
                                    string SPOCName = xlWorksheet.Cells[i, 6].Text.Trim();
                                    if (string.IsNullOrEmpty(SPOCName)) LogErrors.Add("Required SPOC Name at Row-" + count);
                                    else
                                    {
                                        obj.SPOCName = SPOCName;
                                    }
                                    //SPOC Contact Number
                                    string SPOCNumber = xlWorksheet.Cells[i, 7].Text.Trim();
                                    if (string.IsNullOrEmpty(SPOCNumber)) LogErrors.Add("Required SPOC Contact Number at Row-" + count);
                                    else
                                    {
                                        obj.SPOCContactNumber = SPOCNumber;
                                    }
                                    //Nature Of Work
                                    //string NatureOfWork = xlWorksheet.Cells[i, 8].Text.Trim();
                                    //if (string.IsNullOrEmpty(SPOCNumber)) LogErrors.Add("Required SPOC Contact Number at Row-" + count);
                                    //else
                                    //{
                                    //    obj.NatureOfWork = NatureOfWork;
                                    //}
                                }
                                else
                                {
                                    LogErrors.Add("Please enter valid SPOC Email ID at Row-" + count);
                                }
                            }

                            //Frequency of compliance submission
                            string freq = xlWorksheet.Cells[i, 8].Text.Trim();
                            if (string.IsNullOrEmpty(freq)) LogErrors.Add("Required Frequency of submission at Row-" + count);
                            else
                            {
                                if (freqArray.Contains(freq))
                                {
                                    if (freq == "Monthly") obj.Frequency = "M";
                                    else if (freq == "Quarterly") obj.Frequency = "Q";
                                    else if (freq == "Half Yearly") obj.Frequency = "H";
                                    else if (freq == "Yearly") obj.Frequency = "Y";
                                    else if (freq == "One Time") obj.Frequency = "O";
                                }
                                else LogErrors.Add("Please enter valid frequency at Row-" + count);
                            }

                            //Due Days
                            string DueDays = xlWorksheet.Cells[i, 9].Text.Trim();
                            if (obj.Frequency != "O")
                            {
                                if (string.IsNullOrEmpty(DueDays)) LogErrors.Add("Required Due Days at Row-" + count);
                                else
                                {
                                    bool ValidNumber = IsNumber(DueDays);
                                    if (ValidNumber)
                                    {
                                        if (Convert.ToInt32(DueDays) >= 1 && Convert.ToInt32(DueDays) <= 31) obj.DueDays = Convert.ToInt32(DueDays);
                                        else LogErrors.Add("Enter days between 1 & 31 only at Row-" + count);
                                    }
                                    else LogErrors.Add("Due days should be Numeric with non decimal values at Row-" + count);
                                }
                            }

                            //Start Date and End Date
                            string StartDate = xlWorksheet.Cells[i, 10].Text.Trim();
                            string EndDate = xlWorksheet.Cells[i, 11].Text.Trim();
                            if (obj.Frequency == "O")
                            {
                                if (string.IsNullOrEmpty(StartDate)) LogErrors.Add("Required Start Date at Row-" + count);
                                else
                                {
                                    string date = StartDate;
                                    DateTime parsed;

                                    bool valid = DateTime.TryParseExact(date, "dd-MM-yyyy",
                                                                        CultureInfo.InvariantCulture,
                                                                        DateTimeStyles.None,
                                                                        out parsed);
                                    if (valid)
                                    {
                                        obj.StartDate = Convert.ToDateTime(parsed);
                                        obj.DueDate = Convert.ToDateTime(parsed);
                                    }
                                    else LogErrors.Add("Enter the Start date in valid format as given in the headers at Row-" + count);
                                }

                                if (string.IsNullOrEmpty(EndDate)) LogErrors.Add("Required End Date at Row-" + count);
                                else
                                {
                                    string date = EndDate;
                                    DateTime parsed;

                                    bool valid = DateTime.TryParseExact(date, "dd-MM-yyyy",
                                                                        CultureInfo.InvariantCulture,
                                                                        DateTimeStyles.None,
                                                                          out parsed);
                                    //DateTime x = DateTime.ParseExact(date, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    if (valid) obj.endDate = Convert.ToDateTime(parsed);
                                    else LogErrors.Add("Enter the end date in valid format as given in the headers at Row-" + count);
                                    if (DateTime.Compare(Convert.ToDateTime(obj.endDate), Convert.ToDateTime(obj.StartDate)) <= 0)
                                        LogErrors.Add("End date should not be earliear or equal to start date at Row-" + count);
                                }
                            }
                            else
                            {
                                obj.DueDate = DateTime.Now;
                                obj.StartDate = null;
                                obj.endDate = null;
                                if (!string.IsNullOrEmpty(StartDate)) LogErrors.Add("Start date and End date is only applicable in the case of One Time Frequency at Row -" + count);
                                if (!string.IsNullOrEmpty(EndDate)) LogErrors.Add("Start date and End date is only applicable in the case of One Time Frequency at Row -" + count);
                            }

                            LstObj.Add(obj);
                        }

                        if (LogErrors.Count <= 0)
                        {
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                foreach (var obj in LstObj)
                                {

                                    long SpocContUserID = VendorAuditMaster.GetUserIdByEmail(obj.SPOCEmailID);
                                    long SpocSubContUserID = VendorAuditMaster.GetUserIdByEmail(obj.SubContractorEmail);

                                    int RoleID = UserManagement.GetRoleIdByCode("VCCON");

                                    if (SpocContUserID == 0)
                                    {
                                        SpocContUserID = UserManagement.UpdateUserDetail(obj.SPOCEmailID, obj.SPOCName, obj.SPOCContactNumber, custID, RoleID, userID, true);
                                    }

                                    if (SpocSubContUserID == 0)
                                    {
                                        SpocSubContUserID = UserManagement.UpdateUserDetail(obj.SubContractorEmail, obj.SubContractorEmail, "", custID, RoleID, userID, true);
                                    }

                                    Vendor_ContractorProjectMapping newobj = new Vendor_ContractorProjectMapping();
                                    newobj.CustID = custID;
                                    newobj.IsDelete = false;
                                    newobj.CreatedBy = userID;
                                    newobj.CreatedOn = DateTime.Now;
                                    newobj.UpdatedBy = userID;
                                    newobj.UpdatedOn = DateTime.Now;
                                    newobj.Activationdate = DateTime.Now;

                                    newobj.ID = obj.ID;
                                    //newobj.CustID = obj.CustID;
                                    newobj.ContractorID = obj.ContractorID;
                                    newobj.ProjectID = obj.ProjectID;
                                    newobj.IsSubcontract = obj.IsSubcontract;
                                    newobj.UnderContractorID = obj.UnderContractorID;
                                    newobj.Frequency = obj.Frequency;
                                    newobj.DueDate = obj.DueDate;
                                    newobj.StartDate = obj.StartDate;
                                    newobj.ClosureDate = obj.ClosureDate;
                                    newobj.Isdisable = obj.Isdisable;
                                    newobj.DueDays = obj.DueDays;
                                    newobj.endDate = obj.endDate;
                                    newobj.IsShowContractor = obj.IsShowContractor;
                                    newobj.ContractStartDate = obj.ContractStartDate;
                                    newobj.ContractEndDate = obj.ContractEndDate;
                                    newobj.ProjectContSpocID = SpocContUserID;
                                    newobj.ProjectSubContSpocID = SpocSubContUserID;

                                    entities.Vendor_ContractorProjectMapping.Add(newobj);

                                    entities.SaveChanges();
                                    int mappingId = Convert.ToInt32(newobj.ID);
                                    //int custbranchid = 1;
                                    VendorAuditMaster.updateVendor_Schedule(mappingId, false, newobj.Frequency, Convert.ToDateTime(newobj.DueDate), newobj.DueDays);
                                }
                                entities.SaveChanges();
                                message = "success";
                            }

                        }

                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, fileWithoutExtn, out path);
                            message = name;
                        }

                    }
                    else
                    {
                        message = "EmptySheet";
                    }
                }
                else
                {
                    message = "SheetNameError";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }
            return message;
        }

        static int GetLastUsedRow(ExcelWorksheet sheet)
        {
            if (sheet.Dimension == null) { return 0; } // In case of a blank sheet
            var row = sheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }

        public static bool IsValidEmailID(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        internal static List<string> GetUserEmail()
        {
            var obj = new List<string>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return obj = entities.Users
                             .Where(x => x.IsDeleted == false)
                             .Select(x => x.Email).ToList();
            }
        }

        internal static List<string> GetAuditEmail()
        {
            var obj = new List<string>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                return obj = entities.mst_User
                             .Where(x => x.IsDeleted == false)
                             .Select(x => x.Email).ToList();
            }
        }

        internal static List<string> GetUserContacts()
        {
            var obj = new List<string>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return obj = entities.Users
                            .Where(x => x.IsDeleted == false)
                            .Select(x => x.ContactNumber).ToList();
            }
        }

        internal static List<string> GetAuditUserContacts()
        {
            var obj = new List<string>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                return obj = entities.mst_User
                        .Where(x => x.IsDeleted == false)
                        .Select(x => x.ContactNumber).ToList();
            }
        }

        internal static bool IsValidPhoneNumber(string contact)
        {
            bool IsValid = true;
            char[] array = contact.ToCharArray();
            if (array.Length < 10 || array.Length > 10) return IsValid = false;
            foreach (var c in array)
            {
                if (!Char.IsNumber(c)) return IsValid = false;
            }
            return IsValid;
        }
        
        public static string WriteLog(List<string> Errors, string ExcelName, out string file)
        {
            try
            {
                //errorLogFilename = "";

                // errorLogFilename = @"\RLCS_ErrorLog_"+ ExcelName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";               

                string logDirectoryPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["LogFileVaibhav"]);

                string dateFolder = Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");

                string directoryPath = logDirectoryPath + "\\" + dateFolder;
                string errorLogFilename = @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                // errorLogFilename = dateFolder + @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                file = dateFolder + errorLogFilename;
                string filePath = directoryPath + errorLogFilename;

                if (Errors != null && Errors.Count > 0)
                {
                    using (StreamWriter stwriter = new StreamWriter(filePath, true))
                    {
                        stwriter.WriteLine("-------------------Error Log Start-----------as on " + DateTime.Now.ToString("hh:mm tt"));
                        stwriter.WriteLine("Excel Name :" + ExcelName);
                        for (int i = 0; i < Errors.Count; i++)
                        {
                            stwriter.WriteLine(Errors[i].ToString());
                        }
                        Errors = new List<string>();
                        stwriter.WriteLine("-------------------End----------------------------");
                    }
                }
                return file;

            }
            catch (Exception ex)
            {
                file = "";
                return file;
            }
        }
        
        internal static string ProcessUserFileData(ExcelPackage package, string fileWithoutExtn, int custID, int userID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["UserMaster"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);
                    if (noOfRow >= 2)
                    {
                        List<string> LogErrors = new List<string>();
                        List<User> LstUserObj = new List<User>();
                        List<string> Roles = new List<string> { "Project Director", "Project Head", "VCA Company Admin", "Auditor", "Management" };
                        List<Vendor_SP_GetRoleList_Result> roleIDs = GetRole();
                        string UserName = UserManagement.GetByUsername(userID);
                        List<string> EmailIds = GetUserEmail();
                        List<string> AuditEmailIds = GetAuditEmail();
                        List<string> Contacts = GetUserContacts();
                        List<string> AuditUserContacts = GetAuditUserContacts();
                        List<string> xlEmailIDs = new List<string>();
                        List<string> xlContacts = new List<string>();

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            User user = new User();

                            //firstName
                            string firstName = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(firstName)) LogErrors.Add("Required First Name at Row-" + count);
                            else
                            {
                                user.FirstName = firstName;
                                user.UserName = firstName;
                            }

                            //lastName
                            string lastName = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (string.IsNullOrEmpty(lastName)) LogErrors.Add("Required Last Name at Row-" + count);
                            else
                            {
                                user.LastName = lastName;
                            }

                            //EmailID
                            string emailID = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(emailID)) LogErrors.Add("Required Email ID at Row-" + count);
                            else
                            {
                                bool isvalid = IsValidEmailID(emailID);
                                if (!isvalid) LogErrors.Add("Enter valid emailID at Row-" + count);
                                bool isexist1 = EmailIds.Contains(emailID);
                                bool isexist2 = AuditEmailIds.Contains(emailID);
                                bool isExist = isexist1 || isexist2;
                                if (isExist) LogErrors.Add("EmailID already exists at Row-" + count);
                                if (isvalid && !isExist)
                                {
                                    if (!xlEmailIDs.Contains(emailID))
                                    {
                                        user.Email = emailID;
                                        xlEmailIDs.Add(emailID);
                                    }
                                    else
                                    {
                                        LogErrors.Add("This emailID is entered more than once at Row-" + count);
                                    }
                                }
                            }

                            //contact
                            string contact = xlWorksheet.Cells[i, 4].Text.Trim();
                            if (string.IsNullOrEmpty(contact)) LogErrors.Add("Required contact No. at Row-" + count);
                            else
                            {
                                bool isvalid = IsValidPhoneNumber(contact);
                                if (!isvalid) LogErrors.Add("Enter valid contact no. at Row-" + count);
                                bool isexist1 = Contacts.Contains(contact);
                                bool isexist2 = AuditUserContacts.Contains(contact);
                                bool isexist = isexist1 || isexist2;
                                if (isexist) LogErrors.Add("Contact no. already exist at Row-" + count);
                                if (isvalid && !isexist)
                                {
                                    if (!xlContacts.Contains(contact))
                                    {
                                        user.ContactNumber = contact;
                                        xlContacts.Add(contact);
                                    }
                                    else
                                    {
                                        LogErrors.Add("This contact has been entered more than once at Row-" + count);
                                    }

                                }
                            }

                            //roleID
                            string role = xlWorksheet.Cells[i, 5].Text.Trim();
                            if (string.IsNullOrEmpty(role)) LogErrors.Add("Required Role at Row-" + count);
                            else
                            {
                                if (Roles.Contains(role))
                                {
                                    int roleID = (from row in roleIDs
                                                  where row.Name == role
                                                  select row.ID).SingleOrDefault();
                                    user.RoleID = roleID;
                                    user.VendorAuditRoleID = roleID;
                                }
                                else
                                {
                                    LogErrors.Add("Insert roles only mentioned in the header at Row-" + count);
                                }
                            }

                            LstUserObj.Add(user);
                        }

                        if (LogErrors.Count <= 0)
                        {
                            foreach (var user in LstUserObj)
                            {
                                List<UserParameterValue> parameters = new List<UserParameterValue>();

                                user.Designation = "";
                                user.Address = "";
                                user.IsHead = false;
                                user.CustomerID = custID;
                                user.PAN = "";
                                user.CreatedBy = userID;
                                user.CreatedByText = UserName;
                                string passwordText = Util.CreateRandomPassword(10);
                                user.Password = Util.CalculateMD5Hash(passwordText);
                                string PasswordMessage = "true";
                                int resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), PasswordMessage);

                                if (resultValue > 0)
                                {
                                    List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

                                    mst_User mst_user = new mst_User();
                                    mst_user.FirstName = user.FirstName;
                                    mst_user.LastName = user.LastName;
                                    mst_user.Email = user.Email;
                                    mst_user.ContactNumber = user.ContactNumber;
                                    mst_user.RoleID = user.RoleID;
                                    mst_user.VendorAuditRoleID = user.VendorAuditRoleID;
                                    mst_user.Designation = "";
                                    mst_user.Address = "";
                                    mst_user.IsHead = false;
                                    mst_user.CustomerID = custID;
                                    mst_user.PAN = "";
                                    mst_user.CreatedBy = userID;
                                    mst_user.CreatedByText = UserName;
                                    mst_user.Password = Util.CalculateMD5Hash(passwordText);
                                    var result = UserManagementRisk.Create(mst_user, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message,true);

                                    if (!result)
                                    {
                                        UserManagement.deleteUser(resultValue);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            #region EmailTrigger
                                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                            Customer customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));

                                            string message1 = Properties.Settings.Default.EmailTemplate_UserCreationAlert
                                                .Replace("@User", user.FirstName)
                                                .Replace("@loginName", user.Email)
                                                .Replace("@Customer", customerStatus.Name)
                                                .Replace("@password", passwordText)
                                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ConfigurationManager.AppSettings["SenderEmailAddress"].ToString());
                                            EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { user.Email }), null, null, "Temporary Password", message1);
                                            //SendGridEmailManager.SendGridMail(SenderEmailAddress, "Teamlease Regtech", new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message1);
                                            #endregion
                                        }
                                        catch (Exception e)
                                        { }
                                    }
                                }
                            }
                            message = "success";
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, fileWithoutExtn, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        return message = "EmptySheet";
                    }
                }
                else
                {
                    return message = "SheetNameError";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }
            return message;
        }
               
        public static int GetProjectStatusID(string status, int CustID, int UserID)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_ProjectStatus
                      where row.Status == status && row.CustID == CustID
                      select row.Id).SingleOrDefault();
                if (id == 0)
                {
                    Vendor_ProjectStatus obj = new Vendor_ProjectStatus();
                    obj.Status = status;
                    obj.CustID = CustID;
                    obj.IsDeleted = false;
                    obj.CreatedBy = UserID;
                    obj.CreatedOn = DateTime.Now;
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;

                    entities.Vendor_ProjectStatus.Add(obj);
                    entities.SaveChanges();
                    id = obj.Id;
                }
            }
            return id;
        }

        public static int GetProjectCategoryID(string Category, int CustID, int UserID)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_ProjectCategorization
                      where row.Category == Category && row.CustID == CustID
                      select row.Id).SingleOrDefault();
                if (id == 0)
                {
                    Vendor_ProjectCategorization obj = new Vendor_ProjectCategorization();
                    obj.Category = Category;
                    obj.CustID = CustID;
                    obj.IsDeleted = false;
                    obj.CreatedBy = UserID;
                    obj.CreatedOn = DateTime.Now;
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;

                    entities.Vendor_ProjectCategorization.Add(obj);
                    entities.SaveChanges();
                    id = obj.Id;
                }
            }
            return id;
        }

        public static int GetProjectElementID(string Element, int CustID, int UserID)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_ProjectElement
                      where row.Element == Element && row.CustID == CustID
                      select row.Id).SingleOrDefault();
                if (id == 0)
                {
                    Vendor_ProjectElement obj = new Vendor_ProjectElement();
                    obj.Element = Element;
                    obj.CustID = CustID;
                    obj.IsDeleted = false;
                    obj.CreatedBy = UserID;
                    obj.CreatedOn = DateTime.Now;
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;

                    entities.Vendor_ProjectElement.Add(obj);
                    entities.SaveChanges();
                    id = obj.Id;
                }
            }
            return id;
        }

        public static int GetSiteStatusID(string SiteStatus, int CustID, int UserID)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_SiteStatus
                      where row.Sitestatus == SiteStatus && row.CustID == CustID
                      select row.Id).SingleOrDefault();
                if (id == 0)
                {
                    Vendor_SiteStatus obj = new Vendor_SiteStatus();
                    obj.Sitestatus = SiteStatus;
                    obj.CustID = CustID;
                    obj.IsDeleted = false;
                    obj.CreatedBy = UserID;
                    obj.CreatedOn = DateTime.Now;
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;

                    entities.Vendor_SiteStatus.Add(obj);
                    entities.SaveChanges();
                    id = obj.Id;
                }
            }
            return id;
        }

        public static List<CustomerBranch> GetLocations(int CustId)
        {
            List<CustomerBranch> branch = new List<CustomerBranch>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return branch = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false
                                 && row.CustomerID == CustId
                                 select row).ToList();
            }
        }

        public static List<Vendor_ProjectMaster> GetProjectDetails(int custID)
        {
            List<Vendor_ProjectMaster> ProjectMaster = new List<Vendor_ProjectMaster>();
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                return ProjectMaster = (from row in entities.Vendor_ProjectMaster
                                        where row.CustID == custID
                                        select row).ToList();
            }
        }

        internal static int GetProjectOfficeID(string ProjectOfficeName, int CustID, int userid)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_ProjectOfficeName
                      where row.Name == ProjectOfficeName && row.CustID == CustID
                      select row.Id).SingleOrDefault();
                if (id == 0)
                {
                    Vendor_ProjectOfficeName obj = new Vendor_ProjectOfficeName();
                    obj.Name = ProjectOfficeName;
                    obj.CustID = CustID;
                    obj.IsDeleted = false;
                    obj.CreatedBy = userid;
                    obj.CreatedOn = DateTime.Now;
                    obj.UpdatedBy = userid;
                    obj.UpdatedOn = DateTime.Now;

                    entities.Vendor_ProjectOfficeName.Add(obj);
                    entities.SaveChanges();
                    id = obj.Id;
                }
            }
            return id;
        }

        internal static string ProcessProjectFileData(ExcelPackage package, string fileWithoutExtn, int custID, int userID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["ProjectMaster"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);

                    if (noOfRow >= 2)
                    {
                        List<string> LogErrors = new List<string>();
                        List<Vendor_ProjectMaster> LstObj = new List<Vendor_ProjectMaster>();
                        List<RoleDTO> States = GetStateMaster();
                        List<Vendor_SP_GetCityData_Result> Cities = GetCityMaster(0, 0);
                        List<CustomerBranch> LocationList = GetLocations(custID);
                        List<User> Directors = GetDirectors(custID);
                        List<User> Heads = GetHeads(custID);
                        List<Vendor_ProjectMaster> ProjectMaster = GetProjectDetails(custID);

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            Vendor_ProjectMaster obj = new Vendor_ProjectMaster();

                            //Project/office
                            string ProjectOffice = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(ProjectOffice)) LogErrors.Add("Required Project/Office at Row-" + count);
                            else
                            {
                                obj.Projectoffice = ProjectOffice;
                                int id = GetProjectOfficeID(ProjectOffice, custID, userID);
                                if (id > 0) obj.ProjectOfficeID = id;
                            }

                            //status
                            string status = xlWorksheet.Cells[i, 2].Text.Trim();
                            //if (string.IsNullOrEmpty(status)) LogErrors.Add("Required status at Row-" + count);
                            if (string.IsNullOrEmpty(status)) { }
                            else obj.Status = GetProjectStatusID(status, custID, userID);

                            //name
                            string name = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(name)) LogErrors.Add("Required name at Row-" + count);
                            else
                            {
                                int id = ProjectMaster.Where(x => x.Name.Trim() == name).Select(x => x.ID).SingleOrDefault();
                                if (id > 0) LogErrors.Add("Project name already exists at Row-" + count);
                                else obj.Name = name;
                            }

                            //jurisdiction
                            //string jurisdiction = xlWorksheet.Cells[i, 4].Text.Trim();
                            //if (string.IsNullOrEmpty(jurisdiction)) LogErrors.Add("Required jurisdiction at Row-" + count);
                            //else obj.Jurisdiction = jurisdiction;

                            //categorization
                            string category = xlWorksheet.Cells[i, 4].Text.Trim();
                            //if (string.IsNullOrEmpty(category)) LogErrors.Add("Required category at Row-" + count);
                            if (string.IsNullOrEmpty(category)) { }
                            else obj.CatID = GetProjectCategoryID(category, custID, userID);

                            //element
                            string element = xlWorksheet.Cells[i, 5].Text.Trim();
                            //if (string.IsNullOrEmpty(element)) LogErrors.Add("Required element at Row-" + count);
                            if (string.IsNullOrEmpty(element)) { }
                            else obj.ElementId = GetProjectElementID(element, custID, userID);

                            ////state
                            //string state = xlWorksheet.Cells[i, 6].Text.Trim();
                            //if (string.IsNullOrEmpty(state)) LogErrors.Add("Required state at Row-" + count);
                            //else
                            //{
                            //    int stateID = (from row in States
                            //                   where row.Name.Trim() == state
                            //                   select row.ID).FirstOrDefault();
                            //    if (stateID > 0) obj.StateID = stateID;
                            //    else LogErrors.Add("Please enter valid state at Row-" + count);
                            //}

                            ////city
                            //string city = xlWorksheet.Cells[i, 8].Text.Trim();
                            //if (string.IsNullOrEmpty(city)) LogErrors.Add("Required city at Row-" + count);
                            //else
                            //{
                            //    int cityID = (from row in Cities
                            //                  where row.CityName.Trim() == city && row.StateId == obj.StateID
                            //                  select row.CityID).FirstOrDefault();
                            //    if (cityID > 0) obj.CityID = cityID;
                            //    else LogErrors.Add("Please enter valid city at Row-" + count);
                            //}

                            //location
                            string location = xlWorksheet.Cells[i, 6].Text.Trim();
                            if (string.IsNullOrEmpty(location)) LogErrors.Add("Required location at Row-" + count);
                            else
                            {
                                var LocationId = (from row in LocationList
                                                  where row.Name.Trim() == location
                                                  select row).Distinct().FirstOrDefault();

                                if (LocationId != null)
                                {
                                    obj.Location = location;
                                    obj.LocationID = LocationId.ID;
                                    obj.StateID = LocationId.StateID;
                                    obj.CityID = LocationId.CityID;
                                    obj.LocationAdd = LocationId.AddressLine1;
                                }
                                else LogErrors.Add("Please enter valid location at Row-" + count);
                            }

                            ////location address
                            //string LocationAddress = xlWorksheet.Cells[i, 10].Text.Trim();
                            //if (string.IsNullOrEmpty(LocationAddress)) LogErrors.Add("Required location Address at Row-" + count);
                            //else
                            //{
                            //    int id = (from row in LocationList
                            //              where row.AddressLine1.Trim() == LocationAddress && row.Name.Trim() == obj.Location
                            //              select row.ID).FirstOrDefault();
                            //    if (id > 0) obj.LocationAdd = LocationAddress;
                            //    else LogErrors.Add("Please enter valid location address at Row-" + count);
                            //}

                            //site status
                            //string SiteStatus = xlWorksheet.Cells[i, 11].Text.Trim();
                            //if (string.IsNullOrEmpty(SiteStatus)) LogErrors.Add("Required SiteStatus at Row-" + count);
                            //else obj.SiteStatus = GetSiteStatusID(SiteStatus, custID, userID);

                            //director
                            string director = xlWorksheet.Cells[i, 7].Text.Trim();
                            if (string.IsNullOrEmpty(director)) LogErrors.Add("Required Director at Row-" + count);
                            else
                            {
                                int id = (int)Directors.Where(x => x.Email.Trim() == director).Select(x => x.ID).SingleOrDefault();
                                if (id > 0) obj.Director = id;
                                else LogErrors.Add("Enter valid director at Row-" + count);
                            }

                            //Head
                            string Head = xlWorksheet.Cells[i, 8].Text.Trim();
                            if (string.IsNullOrEmpty(Head)) LogErrors.Add("Required Head at Row-" + count);
                            else
                            {
                                int id = (int)Heads.Where(x => x.Email.Trim() == Head).Select(x => x.ID).SingleOrDefault();
                                if (id > 0) obj.Head = id;
                                else LogErrors.Add("Enter valid Head at Row-" + count);
                            }

                            //Authority Name
                            //string AuthorityName = xlWorksheet.Cells[i, 14].Text.Trim();
                            //if (string.IsNullOrEmpty(AuthorityName)) obj.AuthorityName = "";
                            //else obj.AuthorityName = AuthorityName;

                            ////Authority MailID
                            //string AuthorityMailID = xlWorksheet.Cells[i, 15].Text.Trim();
                            //if (string.IsNullOrEmpty(AuthorityMailID)) obj.AuthorityMail = "";
                            //else
                            //{
                            //    bool IsValid = IsValidEmailID(AuthorityMailID);
                            //    if (IsValid)
                            //        obj.AuthorityMail = AuthorityMailID;
                            //    else LogErrors.Add("Please enter valid EmailID at Row-" + count);
                            //}

                            ////Authority Contact no.
                            //string AuthorityContact = xlWorksheet.Cells[i, 16].Text.Trim();
                            //if (string.IsNullOrEmpty(AuthorityContact)) obj.AuthorityContactNo = "";
                            //else
                            //{
                            //    bool IsValid = IsValidPhoneNumber(AuthorityContact);
                            //    if (IsValid)
                            //        obj.AuthorityContactNo = AuthorityContact;
                            //    else LogErrors.Add("Please enter valid Contact Number at Row-" + count);
                            //}

                            LstObj.Add(obj);
                        }

                        if (LogErrors.Count <= 0)
                        {
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                foreach (var obj in LstObj)
                                {
                                    obj.CustID = custID;
                                    obj.IsDelete = false;
                                    obj.CreatedBy = userID;
                                    obj.CreatedOn = DateTime.Now;
                                    obj.UpdatedBy = userID;
                                    obj.UpdatedOn = DateTime.Now;
                                    obj.Isdisable = false;
                                    obj.SiteStatus = 0;
                                    entities.Vendor_ProjectMaster.Add(obj);
                                }
                                entities.SaveChanges();
                                message = "success";
                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, fileWithoutExtn, out path);
                            message = name;
                        }
                    }
                    else
                    {
                        message = "EmptySheet";
                    }
                }
                else
                {
                    message = "SheetNameError";
                }
            }
            catch (Exception ex)
            {
                message = "error";
            }
            return message;
        }

        internal static string ProcessContractorFileData(ExcelPackage package, string fileWithoutExtn, int CustID, int UserID)
        {
            string message = string.Empty;
            int count = 0;

            ExcelWorksheet xlWorksheet = package.Workbook.Worksheets["ContractorMaster"];

            try
            {
                if (xlWorksheet != null)
                {
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);

                    if (noOfRow >= 2)
                    {
                        List<String> LogErrors = new List<string>();
                        List<Vendor_ContractorMaster> LstObj = new List<Vendor_ContractorMaster>();
                        List<string> EmailIds = GetUserEmail();
                        List<string> AuditEmailIds = GetAuditEmail();
                        List<string> Contacts = GetUserContacts();
                        List<string> AuditUserContacts = GetAuditUserContacts();
                        List<string> xlEmailIds = new List<string>();
                        List<string> xlContact = new List<string>();

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;

                            Vendor_ContractorMaster contractor = new Vendor_ContractorMaster();

                            //name
                            string name = xlWorksheet.Cells[i, 1].Text.Trim();
                            if (string.IsNullOrEmpty(name)) LogErrors.Add("Required Contractor Name at Row-" + count);
                            else contractor.ContractorName = name;


                            //SPOC Name
                            string SPOCName = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (!string.IsNullOrEmpty(SPOCName)) contractor.SpocName = SPOCName;
                            else
                            {
                                LogErrors.Add("Required SPOC Name at Row-" + count);
                                contractor.SpocName = "";
                            }

                            //emailID
                            string emailID = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(emailID)) LogErrors.Add("Required SPOC emailID at Row-" + count);
                            else
                            {
                                bool isvalid = IsValidEmailID(emailID);
                                if (!isvalid) LogErrors.Add("Enter valid SPOC emailID at Row-" + count);
                                bool isexist1 = EmailIds.Contains(emailID);
                                bool isexist2 = AuditEmailIds.Contains(emailID);
                                bool isExist = isexist1 || isexist2;
                                if (isExist) LogErrors.Add("SPOC EmailID already exists at Row-" + count);
                                if (isvalid && !isExist)
                                {
                                    if (!xlEmailIds.Contains(emailID))
                                    {
                                        contractor.SpocEmail = emailID;
                                        xlEmailIds.Add(emailID);
                                    }
                                    else
                                    {
                                        LogErrors.Add("This email is entered more than once at Row-" + count);
                                    }
                                }
                            }

                            //contact
                            string contact = xlWorksheet.Cells[i, 4].Text.Trim();
                            if (string.IsNullOrEmpty(contact)) LogErrors.Add("Required SPOC contact No. at Row-" + count);
                            else
                            {
                                bool isvalid = IsValidPhoneNumber(contact);
                                if (!isvalid) LogErrors.Add("Enter valid SPOC contact no. at Row-" + count);
                                bool isexist1 = Contacts.Contains(contact);
                                bool isexist2 = AuditUserContacts.Contains(contact);
                                bool isexist = isexist1 || isexist2;
                                if (isexist) LogErrors.Add("SPOC Contact no. already exist at Row-" + count);
                                if (isvalid && !isexist)
                                {
                                    if (!xlContact.Contains(contact))
                                    {
                                        contractor.SpocContactNo = contact;
                                        xlContact.Add(contact);
                                    }
                                    else LogErrors.Add("This contact is entered more than once at Row-" + count);
                                }
                            }

                            //contractor type
                            string contractorType = xlWorksheet.Cells[i, 5].Text.Trim();
                            if (string.IsNullOrEmpty(contractorType)) LogErrors.Add("Required contractor type at Row-" + count);
                            else
                            {
                                int id = GetContractID(contractorType, CustID, UserID);
                                if (id > 0) contractor.ContractorType = id;
                                else LogErrors.Add("Enter valid contractor type at Row-" + count);
                            }

                            //nature of work
                            string natureOfWork = xlWorksheet.Cells[i, 6].Text.Trim();
                            if (!string.IsNullOrEmpty(natureOfWork)) contractor.NatureOfWork = natureOfWork;

                            LstObj.Add(contractor);
                        }

                        if (LogErrors.Count <= 0)
                        {
                            using (VendorAuditEntities entities = new VendorAuditEntities())
                            {
                                foreach (var contractor in LstObj)
                                {
                                    int RoleID = UserManagement.GetRoleIdByCode("VCCON");
                                    int avacomuserid = UserManagement.UpdateUserDetail(contractor.SpocEmail, contractor.SpocName, contractor.SpocContactNo, CustID, RoleID, UserID,false);

                                    if (avacomuserid > 0)
                                    {
                                        contractor.PID = 0;
                                        contractor.CustID = CustID;
                                        contractor.CreatedBy = UserID;
                                        contractor.CreatedOn = DateTime.Now;
                                        contractor.UpdatedBy = UserID;
                                        contractor.UpdatedOn = DateTime.Now;
                                        contractor.IsDelete = false;
                                        contractor.AvacomUserID = avacomuserid;
                                    }
                                    entities.Vendor_ContractorMaster.Add(contractor);
                                }
                                entities.SaveChanges();
                                message = "success";
                            }
                        }
                        else
                        {
                            string path = string.Empty;
                            string name = WriteLog(LogErrors, fileWithoutExtn, out path);
                            message = name;
                        }

                    }
                    else
                    {
                        message = "EmptySheet";
                    }
                }
                else
                {
                    message = "SheetNameError";
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                message = "error";
            }

            return message;
        }

        public static int GetContractID(string contractorType, int CustID, int UserID)
        {
            int id = 0;

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                id = (from row in entities.Vendor_ContractType
                      where row.Name == contractorType && row.CustomerID == CustID
                      select row.ID).SingleOrDefault();
            }
            return id;
        }
       
        public static bool CheckEntityNameExist(string EntityName, int CustID)
        {
            bool result = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var checkEntityName = (from row in entities.CustomerBranches
                                           select row).ToList();
                    bool containsItem = checkEntityName.Any(item => item.Name.Trim().ToLower() == EntityName.Trim().ToLower() && item.CustomerID == CustID);
                    if (containsItem)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static int ExistsDocumentReturnVersion(Vendor_tbl_FileTransactionData newDocumentRecord)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var query = (from row in entities.Vendor_tbl_FileTransactionData
                             where row.FileName == newDocumentRecord.FileName
                             && row.ContractProjectMappingId == newDocumentRecord.ContractProjectMappingId
                             && row.ScheduleOnId == newDocumentRecord.ScheduleOnId
                             && row.ComplianceId == newDocumentRecord.ComplianceId
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }
        public static long CreateVendorDocument(VendorDocument tempComplianceDocument)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.VendorDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static long CreateContractorDocument(Vendor_tbl_FileTransactionData tempDocument)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Vendor_tbl_FileTransactionData.Add(tempDocument);
                entities.SaveChanges();
                if (tempDocument.ID > 0)
                {
                    return tempDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static long GetUserIdByEmail(string Email)
        {
            long UserID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var contQuery = (from row in entities.Users
                                 where row.IsDeleted == false
                                 && row.Email.Equals(Email)
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    UserID = contQuery.ID;
                }
            }
            return UserID;
        }

        public static List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> GetDashboradCount(int UID, int CID, string Role, bool SubContractor)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Role.Equals("VCCON"))//for Vendor
                {
                    var List = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CID)
                                select row).ToList();

                    if (SubContractor)
                        List = List.Where(x => x.ProjectSubContSpocID == UID && x.IsShowContractor == 1).ToList();
                    else
                        List = List.Where(x => x.ProjectContSpocID == UID || (x.ProjectSubContSpocID == UID && x.IsShowContractor == 1)).ToList();

                    return List;
                }
                else if (Role.Equals("VCMNG"))//for MGMT
                {
                    var List = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CID)
                                select row).ToList();

                    var ProjectList = (from row in entities.Vendor_ProjectMGMTMapping
                                       where row.CustID == CID
                                       && row.UserID == UID
                                        && row.IsDeleted == false
                                       select row.ProjectId).ToList();

                    List = List.Where(x => ProjectList.Contains(x.ProjectID)).ToList();

                    return List;
                }
                else if (Role.Equals("VCAUD"))//for Auditor
                {
                    var List = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CID)
                                select row).Where(x => x.AuditorID == UID).ToList();

                    return List;
                }
                else if (Role.Equals("VCPD"))//for Director
                {
                    var List = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CID)
                                select row).Where(x => x.ProjectDirector == UID).ToList();

                    return List;
                }
                else if (Role.Equals("VCPH"))//for Head
                {
                    var List = (from row in entities.Vendor_Sp_GetContractProjectMappingScheduleList(CID)
                                select row).Where(x => x.ProjectHead == UID).ToList();

                    return List;
                }
                else
                {
                    List<Vendor_Sp_GetContractProjectMappingScheduleList_Result> List = new List<Vendor_Sp_GetContractProjectMappingScheduleList_Result>();
                    return List;
                }
           }
        }

        public static string GetMonthIndexFromAbbreviatedMonth(string monthValue)
        {
            string rlcsMonthValue = string.Empty;
            int monthIndex = 0;
            string[] MonthNames = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
            monthIndex = Array.IndexOf(MonthNames, monthValue) + 1;

            if (monthIndex != 0)
            {
                if (monthIndex < 10)
                    rlcsMonthValue = "0" + monthIndex;
                else
                    rlcsMonthValue = monthIndex.ToString();
            }

            return rlcsMonthValue;
        }
        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return string.Empty;
            return source.Substring(source.Length - tail_length);
        }
        public static Tuple<string, string> Get_RLCSMonthYear(int frequency, string strPeriod)
        {
            Tuple<string, string> tupleRLCSMonthYear = new Tuple<string, string>(string.Empty, string.Empty);

            try
            {
                string rlcsMonthValue = string.Empty;
                string rlcsYearValue = string.Empty;

                if (frequency == 0) //Monthly
                {
                    string monthValue = strPeriod.Substring(0, 3).Trim();
                    rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                    string yearValue = strPeriod.Substring(4, 2).Trim();
                    rlcsYearValue = "20" + yearValue;
                }
                else if (frequency == 1 || frequency == 2) //Quarterly  //Half Yearly
                {
                    string lastMonthinQuarter = GetLast(strPeriod, 6);

                    if (!string.IsNullOrEmpty(lastMonthinQuarter))
                    {
                        string monthValue = lastMonthinQuarter.Substring(0, 3).Trim();
                        rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                        string yearValue = lastMonthinQuarter.Substring(4, 2).Trim();
                        rlcsYearValue = "20" + yearValue;
                    }
                }
                else if (frequency == 3) //Annual
                {
                    if (!string.IsNullOrEmpty(strPeriod))
                    {
                        if (strPeriod.Trim().ToUpper().Contains("CY"))
                        {
                            rlcsMonthValue = "12";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                        if (strPeriod.Trim().ToUpper().Contains("FY"))
                        {
                            rlcsMonthValue = "03";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                    }
                }
                else if (frequency == 5) //BiAnnual
                {
                    if (strPeriod.Contains("FY"))
                    {
                        rlcsMonthValue = "03";
                    }
                    else if (strPeriod.Contains("CY"))
                    {
                        rlcsMonthValue = "12";
                    }

                    rlcsYearValue = "20" + GetLast(strPeriod, 2);
                }

                tupleRLCSMonthYear = Tuple.Create(rlcsMonthValue, rlcsYearValue);
                return tupleRLCSMonthYear;
            }
            catch (Exception ex)
            {
                return tupleRLCSMonthYear;
            }
        }
       
        public static bool UpdateAssignedAuditor(int UserID, int CustID, int assignmentUserId, int MappingID)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                Vendor_ContractorProjectMappingAssignment obj = (from row in entities.Vendor_ContractorProjectMappingAssignment
                                                                 where row.ContractorProjectMappingID == MappingID
                                                                 select row).FirstOrDefault();

                if (obj != null)
                {
                    obj.RoleID = 4;
                    obj.UserID = assignmentUserId;
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    output = true;
                }
                else
                {
                    Vendor_ContractorProjectMappingAssignment obj1 = new Vendor_ContractorProjectMappingAssignment();
                    obj1.ContractorProjectMappingID = MappingID;
                    obj1.UserID = assignmentUserId;
                    obj1.RoleID = 4;
                    obj1.CreatedBy = UserID;
                    obj1.CreatedOn = DateTime.Now;
                    entities.Vendor_ContractorProjectMappingAssignment.Add(obj1);

                    entities.SaveChanges();
                    output = true;
                }
                return output;
            }
        }
        
        public static void CreateOneTimeScheduleOn(DateTime scheduledOn, long MappingId, string freq, long createdByID, string creatdByName, int ContID, int PID)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    int flagactivation = 0;
                    DateTime curruntDate;
                    curruntDate = DateTime.UtcNow;
                    if (scheduledOn < curruntDate)
                    {
                        #region OneTime Frequancy 

                        long ScheduleOnID = (from row in entities.Vendor_ScheduleOn
                                             where row.ContractProjectMappingID == MappingId && row.ScheduleOn == scheduledOn
                                             select row.ID).SingleOrDefault();

                        if (ScheduleOnID <= 0)
                        {

                            if (flagactivation == 0)
                            {
                                Vendor_ContractorProjectMapping data = (from row in entities.Vendor_ContractorProjectMapping
                                                                        where row.ID == MappingId
                                                                        select row).FirstOrDefault();
                                if (data != null)
                                {
                                    data.Activationdate = scheduledOn;
                                    entities.SaveChanges();
                                    flagactivation = 1;
                                }
                            }
                            Vendor_ScheduleOn complianceScheduleon = new Vendor_ScheduleOn();
                            complianceScheduleon.ContractProjectMappingID = MappingId;
                            complianceScheduleon.ScheduleOn = scheduledOn;
                            complianceScheduleon.ForMonth = "";
                            complianceScheduleon.ForPeriod = 0;
                            complianceScheduleon.IsActive = true;
                            complianceScheduleon.ProjectID = PID;
                            complianceScheduleon.ContractorID = ContID;

                            entities.Vendor_ScheduleOn.Add(complianceScheduleon);
                            entities.SaveChanges();
                            
                            Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                            {
                                ContractProjectMappingId = MappingId,
                                StatusId = 1,
                                Remarks = "New compliance assigned.",
                                CreatedBy = createdByID,
                                CreatedByText = creatdByName,
                                StatusChangedOn = DateTime.Now,
                                ComplianceScheduleOnID = complianceScheduleon.ID
                            };
                            CreateTransaction(transaction);

                            List<int> templatedetails = (from row in entities.Vendor_ComplianceMapping
                                                         where row.ContractorProjectMappingID == MappingId
                                                         && row.TemplateID != null && row.TemplateID != -1
                                                            //&& row.CustID == CustomerID
                                                            && row.IsDeleted == false
                                                         select (int)row.TemplateID).ToList();

                            List<int> Compliancedetails1 = (from row in entities.Vendor_AssignedTemplate
                                                            where templatedetails.Contains(row.TemplateID)
                                                            && row.IsDeleted == false
                                                            select (int)row.ComplianceID).ToList();

                            List<int> Compliancedetails2 = (from row in entities.Vendor_ComplianceMapping
                                                            where row.ContractorProjectMappingID == MappingId
                                                            && row.ComplainceID != null
                                                               //&& row.CustID == CustomerID
                                                               && row.IsDeleted == false
                                                            select (int)row.ComplainceID).ToList();

                            List<int> Compliancedetails = Compliancedetails1.Union(Compliancedetails2).ToList();
                            Compliancedetails = Compliancedetails.Distinct().ToList();
                            //List<int> Compliancedetails = (from row in entities.Vendor_ComplianceMapping
                            //                               where row.ContractorProjectMappingID == MappingId
                            //                               select (int)row.ComplainceID).ToList();
                            foreach (var item in Compliancedetails)
                            {
                                Vendor_ContractProjMappingStepMapping obb = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                             where row.ContractProjMappingID == MappingId
                                                                             && row.ContractProjMappingScheduleOnID == complianceScheduleon.ID
                                                                             && row.ComplianceID == item
                                                                             select row).FirstOrDefault();
                                if (obb != null)
                                {
                                    obb.IsActive = true;
                                }
                                else
                                {
                                    Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                    {
                                        ContractProjMappingID = MappingId,
                                        ContractProjMappingScheduleOnID = complianceScheduleon.ID,
                                        ComplianceID = item,
                                        IsActive = true
                                    };
                                    entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                    entities.SaveChanges();

                                    Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                    {
                                        ContractProjectMappingId = MappingId,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned.",
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusChangedOn = DateTime.Now,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        ComplianceID = item,
                                        ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                    };
                                    CreateTransactioncompliance(transaction1);
                                }                                
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }

        public static void CreateScheduleOn(DateTime scheduledOn, long MappingId, string freq, long createdByID, string creatdByName,int ContID,int PID,int Frqint,byte frq)
        {
            try
            {
                if (freq == "Y")
                    freq = "A";

                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    int flagactivation = 0;
                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    
                    curruntDate = DateTime.UtcNow;
                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (true)
                        {
                            #region Normal Frequancy 

                            if (true)
                            {
                                nextDateMonth = GetNextDate(freq, nextDate, MappingId, false, frq);
                            }

                            long ScheduleOnID = (from row in entities.Vendor_ScheduleOn
                                                 where row.ContractProjectMappingID == MappingId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                //if (flagactivation == 0)
                                //{
                                //    Vendor_ContractorProjectMapping data = (from row in entities.Vendor_ContractorProjectMapping
                                //                                            where row.ID == MappingId
                                //                                            select row).FirstOrDefault();
                                //    if (data != null)
                                //    {
                                //        data.Activationdate = scheduledOn;
                                //        entities.SaveChanges();
                                //        flagactivation = 1;
                                //    }
                                //}

                                Vendor_ScheduleOn complianceScheduleon = new Vendor_ScheduleOn();
                                complianceScheduleon.ContractProjectMappingID = MappingId;
                                complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.ProjectID = PID;
                                complianceScheduleon.ContractorID = ContID;

                                entities.Vendor_ScheduleOn.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                Vendor_ContractMappingTransaction transaction = new Vendor_ContractMappingTransaction()
                                {
                                    ContractProjectMappingId = MappingId,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned.",
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusChangedOn = DateTime.Now,
                                    ComplianceScheduleOnID = complianceScheduleon.ID
                                };
                                CreateTransaction(transaction);

                                List<int> templatedetails = (from row in entities.Vendor_ComplianceMapping
                                                             where row.ContractorProjectMappingID == MappingId
                                                             && row.TemplateID != null && row.TemplateID != -1
                                                             //&& row.CustID == CustomerID
                                                                && row.IsDeleted == false
                                                             select (int)row.TemplateID).ToList();

                                List<int> Compliancedetails1 = (from row in entities.Vendor_AssignedTemplate
                                                                where templatedetails.Contains(row.TemplateID)
                                                                && row.IsDeleted == false
                                                                select (int)row.ComplianceID).ToList();

                                List<int> Compliancedetails2 = (from row in entities.Vendor_ComplianceMapping
                                                                where row.ContractorProjectMappingID == MappingId
                                                                && row.ComplainceID != null
                                                                //&& row.CustID == CustomerID
                                                                   && row.IsDeleted == false
                                                                select (int)row.ComplainceID).ToList();

                                List<int> Compliancedetails = Compliancedetails1.Union(Compliancedetails2).ToList();
                                Compliancedetails = Compliancedetails.Distinct().ToList();

                                //List<int> Compliancedetails = (from row in entities.Vendor_ComplianceMapping
                                //                               where row.ContractorProjectMappingID == MappingId
                                //                               select (int)row.ComplainceID).ToList();
                                foreach (var item in Compliancedetails)
                                {
                                    Vendor_ContractProjMappingStepMapping obb = (from row in entities.Vendor_ContractProjMappingStepMapping
                                                                                 where row.ContractProjMappingID == MappingId
                                                                                 && row.ContractProjMappingScheduleOnID == complianceScheduleon.ID
                                                                                 && row.ComplianceID == item
                                                                                 select row).FirstOrDefault();
                                    if (obb != null)
                                    {
                                        obb.IsActive = true;
                                    }
                                    else
                                    {
                                        Vendor_ContractProjMappingStepMapping objmappcompliance = new Vendor_ContractProjMappingStepMapping()
                                        {
                                            ContractProjMappingID = MappingId,
                                            ContractProjMappingScheduleOnID = complianceScheduleon.ID,
                                            ComplianceID = item,
                                            IsActive = true
                                        };
                                        entities.Vendor_ContractProjMappingStepMapping.Add(objmappcompliance);
                                        entities.SaveChanges();
                                        Vendor_ContractMappingComplianceTransaction transaction1 = new Vendor_ContractMappingComplianceTransaction()
                                        {
                                            ContractProjectMappingId = MappingId,
                                            StatusId = 1,
                                            Remarks = "New compliance assigned.",
                                            CreatedBy = createdByID,
                                            CreatedByText = creatdByName,
                                            StatusChangedOn = DateTime.Now,
                                            ComplianceScheduleOnID = complianceScheduleon.ID,
                                            ComplianceID = item,
                                            ContractProjMappingStepMappingID = (int)objmappcompliance.ID
                                        };
                                        CreateTransactioncompliance(transaction1);
                                    }                                   
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
        public static bool CreateTransactioncompliance(Vendor_ContractMappingComplianceTransaction transaction)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    transaction.Dated = DateTime.Now;
                    entities.Vendor_ContractMappingComplianceTransaction.Add(transaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        //public static bool CreateTransactioncompliance(Vendor_ContractMappingComplianceTransaction transaction)
        //{
        //    try
        //    {
        //        using (VendorAuditEntities entities = new VendorAuditEntities())
        //        {
        //            using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    transaction.Dated = DateTime.Now;
        //                    entities.Vendor_ContractMappingComplianceTransaction.Add(transaction);
        //                    entities.SaveChanges();
        //                    dbtransaction.Commit();

        //                    return true;
        //                }
        //                catch (Exception)
        //                {
        //                    dbtransaction.Rollback();
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static bool CreateTransaction(Vendor_ContractMappingTransaction transaction)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    transaction.Dated = DateTime.Now;
                    entities.Vendor_ContractMappingTransaction.Add(transaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        //public static bool CreateTransaction(Vendor_ContractMappingTransaction transaction)
        //{
        //    try
        //    {
        //        using (VendorAuditEntities entities = new VendorAuditEntities())
        //        {
        //            using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    transaction.Dated = DateTime.Now;
        //                    entities.Vendor_ContractMappingTransaction.Add(transaction);
        //                    entities.SaveChanges();
        //                    dbtransaction.Commit();
                            
        //                    return true;
        //                }
        //                catch (Exception)
        //                {
        //                    dbtransaction.Rollback();
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {              
        //        return false;
        //    }
        //}
        public static List<Vendor_Schedule> GetScheduleByComplianceID(long ProjectContractMappingID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var scheduleList = (from row in entities.Vendor_Schedule
                                    where row.ProjectContractMappingID == ProjectContractMappingID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static Vendor_ScheduleOn GetLastScheduleOnByInstance(long ContractProjectMappingID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var scheduleObj = (from row in entities.Vendor_ScheduleOn
                                   where row.ContractProjectMappingID == ContractProjectMappingID
                                   orderby row.ScheduleOn descending, row.ID descending
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }
        public static DateTime? GetComplianceInstanceScheduledon(long ComplianceInstanceID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                DateTime ScheduleOnID = (from row in entities.Vendor_ContractorProjectMapping
                                         where row.ID == ComplianceInstanceID
                                         select row.DueDate).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }

            }
        }
        public static Tuple<DateTime, string, long> GetNextDate(string Freq,DateTime scheduledOn, long ComplianceInstancemappID, bool iseffectivedate, byte frq)
        {
            var complianceSchedule = GetScheduleByComplianceID(ComplianceInstancemappID);
          
            var lastSchedule = GetLastScheduleOnByInstance(ComplianceInstancemappID);
            DateTime? getinstancescheduleondate = scheduledOn;// new DateTime(2021, 01, 01, 10, 30, 50);
            //DateTime? getinstancescheduleondate = scheduledOn; //GetComplianceInstanceScheduledon(ComplianceInstancemappID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (Freq == "M")
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (Freq == "H")
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (Freq == "A")//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (Freq == "Q")//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }

                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), frq);
                }
            }
            else
            {
                #region Monthly
                if (Freq == "M")
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (Freq == "H")
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (Freq == "A")//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (Freq == "Q")//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            

            if (Freq != "M" || Freq != "Q")
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (Freq == "A")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (Freq == "Q")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (Freq == "M")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (Freq == "H")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if(Freq == "A")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                   else if (Freq == "Q")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (Freq == "M")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (Freq == "H")
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (Freq == "A")
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (Freq == "Q")
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (Freq == "M")
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (Freq == "H")
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {

                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                                    }
                                }
                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02"
                || row.SpecialDate.Substring(2, 2) == "03"
                || row.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "07"
                || row.SpecialDate.Substring(2, 2) == "08"
                || row.SpecialDate.Substring(2, 2) == "09"
                || row.SpecialDate.Substring(2, 2) == "10"
                || row.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (Freq == "A")
                    {
                        complianceScheduleDates.Add(complianceSchedule.Where(entry =>
                        entry.SpecialDate.Substring(2, 2) == "02"
                        || entry.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "07"
                        || entry.SpecialDate.Substring(2, 2) == "08"
                        || entry.SpecialDate.Substring(2, 2) == "09"
                        || entry.SpecialDate.Substring(2, 2) == "10"
                        || entry.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                    if (Freq == "H")
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (Freq == "M")
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (Freq == "Q")
                        {
                            if (Freq == "Q")
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && Freq == "A").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && Freq == "H").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (Freq == "M")
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frq);
                }
            }
            else if (Freq == "Q")
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frq);
                }
            }
            else if (Freq == "H")
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frq);
                }
            }
            else if (Freq == "A")
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frq);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frq);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (Freq == "Q")
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstancemappID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (Freq == "H")
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstancemappID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (Freq == "A")
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstancemappID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (Freq == "M")
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstancemappID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));                    
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), frq);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 0:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;

                case 4:

                    if (forMonth == 1)
                    {
                        forMonthName = 9;
                    }
                    else
                    {
                        forMonthName = forMonthName - 4;
                    }

                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }

        public static long GetMappingContractProjectID(long ContractID, long ProjectId)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                long MappingID = (from row in entities.Vendor_ContractorProjectMapping
                                     where row.ContractorID == ContractID 
                                     && row.ProjectID == ProjectId
                                     && row.IsDelete == false
                                     select row.ID).FirstOrDefault();
                return MappingID;
            }
        }
        public static long GetScheduledOnPresentOrNot(long ContractProjectMappingID, DateTime ScheduledOn)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                long ScheduleOnID = (from row in entities.Vendor_ScheduleOn
                                     where row.ContractProjectMappingID == ContractProjectMappingID && row.ScheduleOn == ScheduledOn
                                     && row.IsActive == true 
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }            
        }

        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:
                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        if (date.Month >= 4)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else if (date.Month >= 3)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else
                        {
                            startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                            endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        }

                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year2 = (date.AddYears(-1)).ToString("yy");
                        }
                        else
                        {
                            year2 = (date).ToString("yy");
                        }

                    }
                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency)
        {
            long NextPeriod;
            switch (Frequency)
            {
                case 0:
                    if (LastforMonth == 12)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 1;
                    break;

                case 1:
                    if (LastforMonth == 10)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 3;
                    break;

                case 2:
                    if (LastforMonth == 7)
                        NextPeriod = 1;
                    else if (LastforMonth == 10)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 6;
                    break;

                case 3:
                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 4:

                    if (LastforMonth == 9)
                        NextPeriod = 1;
                    else if (LastforMonth == 12)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 4;

                    break;
                case 5:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 6:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }

        public static List<Vendor_SP_ContractorProjectMapping_Result> GetContractTypeForFilterAssingment(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_ContractorProjectMapping(CID)
                           select row).ToList();
                return lst.ToList();
            }
        }

        public static bool DeleteContractProjectMappingMaster(int UID, int CID, int CId)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CId > 0)
                {
                    Vendor_ContractorProjectMapping obj = (from row in entities.Vendor_ContractorProjectMapping
                                                           where row.ID == CId
                                              && row.CustID == CID
                                                   select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool updatestatusContractorMaster(int UID, int CID, int CId,int status)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CId > 0)
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.ID == CId
                                              && row.CustID == CID
                                                   select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Isdisable = status;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool DeleteContractorMaster(int UID, int CID, int CId)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CId > 0)
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.ID == CId
                                              && row.CustID == CID
                                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        
        public static bool DeleteProjectMaster(int UID, int CID, int PId)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (PId > 0)
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.ID == PId
                                              && row.CustID == CID
                                               select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool UpdateProjectofficeMaster(int UID, int CID, int Id, string Name)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_ProjectOfficeName obj = (from row in entities.Vendor_ProjectOfficeName
                                                    where row.Id == Id
                                              && row.CustID == CID
                                                 select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Name = Name;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ProjectOfficeName obj = (from row in entities.Vendor_ProjectOfficeName
                                                    where row.Name == Name
                                                 && row.CustID == CID
                                                 select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ProjectOfficeName obj1 = new Vendor_ProjectOfficeName();
                        obj1.Name = Name;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustID = CID;
                        entities.Vendor_ProjectOfficeName.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistProjectOfficeMaster(int UID, int CID, int Id, string Name)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id == 0)
                {
                    Vendor_ProjectOfficeName obj = (from row in entities.Vendor_ProjectOfficeName
                                                 where row.Name == Name
                                               && row.CustID == CID
                                               && row.IsDeleted == false
                                                 select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ProjectOfficeName obj = (from row in entities.Vendor_ProjectOfficeName
                                                    where row.Name == Name
                                               && row.CustID == CID
                                               && row.Id != Id
                                               && row.IsDeleted == false
                                                 select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }
        public static List<Vendor_Sp_GetAllScheduleList_Result> AllScheduleList(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_Sp_GetAllScheduleList(CID)
                           select row).ToList();

                return lst.ToList();
            }
        }
        public static List<Vendor_Sp_GetAllUnScheduleList_Result> AllUnScheduleList(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_Sp_GetAllUnScheduleList(CID)
                           select row).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_Sp_GetAllAssignmentList_Result> AllAssignmentList(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_Sp_GetAllAssignmentList(CID)
                           select row).ToList();
                
                return lst.ToList();
            }
        }
        public static List<Vendor_Sp_GetAllUnAssignmentList_Result> AllUnAssignmentList(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_Sp_GetAllUnAssignmentList(CID)
                           select row).ToList();

                return lst.ToList();
            }
        }
        
        public static List<Vendor_SP_GetLocation_Result> GetProjectLocationMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetLocation(CID)                           
                           select row).OrderBy(x => x.Name).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_ProjectOfficeName> GetProjectOfficeMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_ProjectOfficeName
                           where row.IsDeleted == false
                           && row.CustID == CID
                           select row).OrderBy(x=>x.Name).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_SP_GetProjectcontractorMapping_Result> GetContractMappingDetail(int CID, int ID,int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<Vendor_SP_GetProjectcontractorMapping_Result> ContractorDetailsList = new List<Vendor_SP_GetProjectcontractorMapping_Result>();

                ContractorDetailsList = (from Contractor in entities.Vendor_SP_GetProjectcontractorMapping(CID, ID)
                                         where Contractor.ContractorID == ContID
                                         select Contractor).ToList();
                return ContractorDetailsList.ToList();
            }
        }

        public static bool UpdateMappingClouserDate(int UID, int CID, int ProjectId, int ContractID,int MappingID, DateTime ClouserDate, int Type)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (MappingID > 0)
                {
                    Vendor_ContractorProjectMapping obj = (from row in entities.Vendor_ContractorProjectMapping
                                                           where row.ID == MappingID
                                              && row.CustID == CID
                                                select row).FirstOrDefault();
                    if (obj != null)
                    {
                        if (Type == 1)// for disable
                        {
                            if (Convert.ToDateTime(ClouserDate) < DateTime.Now)
                            {
                                obj.Isdisable = 1;
                            }
                            else
                            {
                                obj.Isdisable = 0;
                            }
                            obj.ClosureDate = Convert.ToDateTime(ClouserDate);
                            obj.StartDate = null;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;
                            obj.CustID = CID;

                            entities.SaveChanges();

                            Vendor_ProjectContractMappingStatuslog obj1 = new Vendor_ProjectContractMappingStatuslog();
                            obj1.CustID = CID;
                            obj1.ProjectID = ProjectId;
                            obj1.MappingID = MappingID;
                            obj1.ContractID = ContractID;
                            obj1.IsDisable = true;
                            obj1.StatusDate = Convert.ToDateTime(ClouserDate);
                            obj1.CreatedBy = UID;
                            obj1.CreatedOn = DateTime.Now;
                            obj1.UpdatedBy = UID;
                            obj1.UpdatedOn = DateTime.Now;
                            obj1.IsDelete = false;
                            entities.Vendor_ProjectContractMappingStatuslog.Add(obj1);
                            entities.SaveChanges();
                            
                            #region for disable
                            var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CID)
                                        select row).ToList();

                            var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                                  select row).ToList();

                            DateTime D1 = ClouserDate.AddMonths(1);//ClouserDate.AddDays(30);                            
                            DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                            List = List.Where(x => x.ContractorProjectMappingID == MappingID && x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                            foreach (var item in List)
                            {
                                int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                     && x.ContractProjMappingID == item.ContractorProjectMappingID
                                     && x.ComplianceStatusID != null).ToList().Count;

                                if (cnt > 1)
                                {

                                }
                                else
                                {
                                    Vendor_ScheduleOn scheobj = (from row in entities.Vendor_ScheduleOn
                                                                 where row.ContractProjectMappingID == item.ContractorProjectMappingID
                                                                  && row.ID == item.ScheduleOnID
                                                                 select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = false;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            output = true;
                        }
                        else// for Enable
                        {
                            if (Convert.ToDateTime(ClouserDate) < DateTime.Now)
                            {
                                obj.Isdisable = 0;
                            }
                            else
                            {
                                obj.Isdisable = 1;
                            }
                            //obj.Isdisable = false;
                            obj.StartDate = Convert.ToDateTime(ClouserDate);
                            obj.ClosureDate = null;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;
                            obj.CustID = CID;

                            entities.SaveChanges();

                            Vendor_ProjectContractMappingStatuslog obj1 = new Vendor_ProjectContractMappingStatuslog();
                            obj1.CustID = CID;
                            obj1.ProjectID = ProjectId;
                            obj1.MappingID = MappingID;
                            obj1.ContractID = ContractID;
                            obj1.IsDisable = false;
                            obj1.StatusDate = Convert.ToDateTime(ClouserDate);
                            obj1.CreatedBy = UID;
                            obj1.CreatedOn = DateTime.Now;
                            obj1.UpdatedBy = UID;
                            obj1.UpdatedOn = DateTime.Now;
                            obj1.IsDelete = false;
                            entities.Vendor_ProjectContractMappingStatuslog.Add(obj1);
                            entities.SaveChanges();

                            #region for Enable
                            var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CID)
                                        select row).ToList();

                            var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                                  select row).ToList();

                            DateTime D1 = ClouserDate.AddMonths(1);//ClouserDate.AddDays(30);    
                            DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                            List = List.Where(x => x.ContractorProjectMappingID == MappingID && x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();

                            foreach (var item in List)
                            {
                                int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                     && x.ContractProjMappingID == item.ContractorProjectMappingID
                                     && x.ComplianceStatusID != null).ToList().Count;

                                if (cnt > 1)
                                {

                                }
                                else
                                {
                                    Vendor_ScheduleOn scheobj = (from row in entities.Vendor_ScheduleOn
                                                                 where row.ContractProjectMappingID == item.ContractorProjectMappingID
                                                                  && row.ID == item.ScheduleOnID
                                                                 select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            output = true;
                        }
                    }
                }
                return output;
            }
        }

        public static bool UpdateClouserDate(int UID, int CID, int ProjectId, DateTime ClouserDate,int Type)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ProjectId > 0)
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.ID == ProjectId
                                              && row.CustID == CID
                                                select row).FirstOrDefault();
                    if (obj != null)
                    {
                        if (Type == 1)// for disable
                        {
                            if (Convert.ToDateTime(ClouserDate) < DateTime.Now)
                            {
                                obj.Isdisable = true;
                            }
                            else
                            {
                                obj.Isdisable = false;
                            }
                            obj.ClosureDate = Convert.ToDateTime(ClouserDate);
                            obj.StartDate = null;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;
                            obj.CustID = CID;
                            
                            entities.SaveChanges();
                            
                            Vendor_ProjectStatuslog obj1 = new Vendor_ProjectStatuslog();
                            obj1.CustID = CID;
                            obj1.ProjectID = ProjectId;
                            obj1.IsDisable = true;
                            obj1.StatusDate = Convert.ToDateTime(ClouserDate);
                            obj1.CreatedBy = UID;
                            obj1.CreatedOn = DateTime.Now;
                            obj1.UpdatedBy = UID;
                            obj1.UpdatedOn = DateTime.Now;
                            obj1.IsDelete = false;
                            entities.Vendor_ProjectStatuslog.Add(obj1);
                            entities.SaveChanges();
                            
                            #region for disable
                            var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CID)
                                        select row).ToList();

                            var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                                  select row).ToList();

                            DateTime D1 = ClouserDate.AddMonths(1);//ClouserDate.AddDays(30);    
                            DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                            List = List.Where(x => x.ProjectID == ProjectId && x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();
                            foreach (var item in List)
                            {

                                int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                && x.ContractProjMappingID == item.ContractorProjectMappingID
                                && x.ComplianceStatusID != null).ToList().Count;

                                if (cnt > 1)
                                {             
                                                           
                                }
                                else
                                {
                                    Vendor_ScheduleOn scheobj = (from row in entities.Vendor_ScheduleOn
                                                                 where row.ProjectID == ProjectId
                                                                  && row.ContractProjectMappingID == item.ContractorProjectMappingID
                                                                  && row.ID == item.ScheduleOnID
                                                                 select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = false;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            output = true;
                        }
                        else// for Enable
                        {
                            if (Convert.ToDateTime(ClouserDate) < DateTime.Now)
                            {
                                obj.Isdisable = false;
                            }
                            else
                            {
                                obj.Isdisable = true;
                            }
                            //obj.Isdisable = false;
                            obj.StartDate = Convert.ToDateTime(ClouserDate);
                            obj.ClosureDate = null;
                            obj.UpdatedBy = UID;
                            obj.UpdatedOn = DateTime.Now;
                            obj.CustID = CID;

                            entities.SaveChanges();
                           
                            Vendor_ProjectStatuslog obj1 = new Vendor_ProjectStatuslog();
                            obj1.CustID = CID;
                            obj1.ProjectID = ProjectId;
                            obj1.IsDisable = false;
                            obj1.StatusDate = Convert.ToDateTime(ClouserDate);
                            obj1.CreatedBy = UID;
                            obj1.CreatedOn = DateTime.Now;
                            obj1.UpdatedBy = UID;
                            obj1.UpdatedOn = DateTime.Now;
                            obj1.IsDelete = false;
                            entities.Vendor_ProjectStatuslog.Add(obj1);
                            entities.SaveChanges();
                            
                            #region for Enable
                            var List = (from row in entities.Vendor_Sp_GetContractProjectAuditsceduledetail(CID)
                                        select row).ToList();

                            var Listcompliance = (from row in entities.Vendor_Sp_GetContractProjectStepMappingScheduleList(CID, -1, -1)
                                                  select row).ToList();

                            DateTime D1 = ClouserDate.AddMonths(1);//ClouserDate.AddDays(30);    
                            DateTime Datevalue = new DateTime(D1.Year, D1.Month, 1);

                            List = List.Where(x => x.ProjectID == ProjectId && x.AuditCheckListStatusID == 1 && x.ScheduleOn >= Datevalue).ToList();
                            foreach (var item in List)
                            {
                                int cnt = Listcompliance.Where(x => x.ContractProjMappingScheduleOnID == item.ScheduleOnID
                                         && x.ContractProjMappingID == item.ContractorProjectMappingID
                                         && x.ComplianceStatusID != null).ToList().Count;

                                if (cnt > 1)
                                {

                                }
                                else
                                {
                                    Vendor_ScheduleOn scheobj = (from row in entities.Vendor_ScheduleOn
                                                                 where row.ProjectID == ProjectId
                                                                  && row.ContractProjectMappingID == item.ContractorProjectMappingID
                                                                  && row.ID == item.ScheduleOnID
                                                                 select row).FirstOrDefault();
                                    if (scheobj != null)
                                    {
                                        scheobj.IsActive = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            output = true;
                        }
                    }                    
                }
                return output;
            }
        }

        public static List<Vendor_Frequencyschedule> GetFrequencyschedule(string value)
        {
            List<Vendor_Frequencyschedule> List = new List<Vendor_Frequencyschedule>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                List = (from row in entities.Vendor_Frequencyschedule
                        where row.Value == value
                        select row).ToList();

                return List;
            }
        }

        public static List<Vendor_Year> GetYearList()
        {
            List<Vendor_Year> List = new List<Vendor_Year>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                List = (from row in entities.Vendor_Year
                                select row).OrderByDescending(x=>x.Year).ToList();
                
                return List;
            }
        }
        public static List<UserDetails> GetMGMTUserList(int CID)
        {
            List<UserDetails> userFinalList = new List<UserDetails>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                var userList = (from row in entities.Vendor_Sp_GetMGMTUserList(CID)
                                select row).OrderBy(x => x.FirstName).ToList();

                foreach (var item in userList)
                {
                    UserDetails finalData = new UserDetails();
                    finalData.ID = item.ID;
                    finalData.UserName = item.FirstName + " " + item.LastName;
                    finalData.RoleID = (int)item.VendorAuditRoleID;
                    finalData.ContactNumber = item.ContactNumber;
                    finalData.RoleName = GetRoleNameByRoleID((int)item.VendorAuditRoleID);
                    finalData.Email = item.Email;
                    userFinalList.Add(finalData);
                }
                return userFinalList;
            }
        }
        public static List<UserDetails> GetAuditorUserList(int CID)
        {
            List<UserDetails> userFinalList = new List<UserDetails>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                var userList = (from row in entities.Vendor_Sp_GetAuditorUserList(CID)
                                select row).OrderBy(x => x.FirstName).ToList();
                
                foreach (var item in userList)
                {
                    UserDetails finalData = new UserDetails();
                    finalData.ID = item.ID;
                    finalData.UserName = item.UserName;
                    finalData.RoleID = (int)item.VendorAuditRoleID;
                    finalData.ContactNumber = item.ContactNumber;
                    finalData.RoleName = GetRoleNameByRoleID((int)item.VendorAuditRoleID);
                    finalData.Email = item.Email;
                    userFinalList.Add(finalData);
                }
                return userFinalList;
            }
        }
        public static List<Vendor_SP_GetContractorUserMasterList_Result> GetContractorUserList(int CID)
        {
            List<Vendor_SP_GetContractorUserMasterList_Result> userFinalList = new List<Vendor_SP_GetContractorUserMasterList_Result>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                userFinalList = (from row in entities.Vendor_SP_GetContractorUserMasterList(CID)
                                select row).ToList();
                                
                return userFinalList;
            }
        }

        public static List<UserDetails> GetUserList(int CID)
        {
            List<UserDetails> userFinalList = new List<UserDetails>();

            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                var userList = (from row in entities.Vendor_SP_GetUserMasterList(CID)
                                select row).ToList();

                foreach (var item in userList)
                {
                    string sts = "Active";
                    if (item.IsDeleted == true)
                        sts = "Inactive";

                    UserDetails finalData = new UserDetails();
                    finalData.ID = item.ID;
                    finalData.FirstName = item.FirstName;
                    finalData.LastName = item.LastName;
                    finalData.UserName = item.FirstName + ' ' + item.LastName;
                    if (item.VendorAuditRoleID != null)
                    {
                        finalData.RoleID = (int)item.VendorAuditRoleID;
                        finalData.RoleName = GetRoleNameByRoleID((int)item.VendorAuditRoleID);
                    }
                    else
                    {
                        finalData.RoleID = -1;
                        finalData.RoleName = "";
                    }
                    finalData.ContactNumber = item.ContactNumber;
                    
                    finalData.Email = item.Email;
                    finalData.Status = sts;
                    finalData.WrongAttempt = item.WrongAttempt;
                    userFinalList.Add(finalData);
                }
                return userFinalList;
            }            
        }
        public static int GetProjectMappingPID(int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var getName = (from row in entities.Vendor_ContractorProjectMapping
                               join row1 in entities.Vendor_ProjectMaster
                               on row.ProjectID equals row1.ID
                               where row.ContractorID == ContID
                                && row.IsDelete == false
                               && row1.IsDelete == false
                               select row1.ID).FirstOrDefault();
                if (getName != null)
                {
                    return getName;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static int GetHeadCountID(int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var getName = (from row in entities.Vendor_ContractorProjectMapping
                               join row1 in entities.Vendor_ProjectMaster
                               on row.ProjectID equals row1.ID
                               where row.ContractorID == ContID
                               && row.IsDelete == false
                               && row1.IsDelete == false
                               select row.ID).FirstOrDefault();
                if (getName != null)
                {
                    var filedetails = (from row in entities.Vendor_tbl_FileData
                                where row.ProjectID == 0
                                && row.IsDeleted == false
                                && row.IsMapping == 1
                                && row.ProjectMappingID == getName
                                select row).ToList();
                    int output = 0;
                    foreach (var item in filedetails)
                    {
                        output = output + item.HeadCount;
                    }
                    return output;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static int GetProjectMappingID(int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var getName = (from row in entities.Vendor_ContractorProjectMapping
                               join row1 in entities.Vendor_ProjectMaster
                               on row.ProjectID equals row1.ID
                               where row.ContractorID == ContID
                               && row.IsDelete == false
                               && row1.IsDelete == false
                               select row.ID).FirstOrDefault();
                if (getName != null)
                {
                    return getName;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static string GetUserNameDetails(int UID,int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.CustomerID == CID
                            && row.IsDeleted == false
                            && row.ID == UID
                            select row).FirstOrDefault();
                if (user != null)
                {
                    return user.FirstName + ' ' + user.LastName;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<CustomerBranch> GetLocationDetails(int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getName = (from row in entities.CustomerBranches
                               where row.CustomerID == CustID                                
                               && row.IsDeleted == false
                               select row).ToList();
                if (getName != null)
                {
                    return getName;
                }
                else
                {
                    return null;
                }
            }
        }
        
        public static string GetProjectMappingName(int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var getName = (from row in entities.Vendor_ContractorProjectMapping
                               join row1 in entities.Vendor_ProjectMaster
                               on row.ProjectID equals row1.ID
                               where row.ContractorID == ContID
                                && row.IsDelete == false
                               && row1.IsDelete == false
                               select row1.Name).FirstOrDefault();
                if (getName != null)
                {
                    return getName;
                }
                else
                {
                    return null;
                }
            }
        }

        public static string GetRoleNameByRoleID(int RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getRole = (from row in entities.Roles
                               where row.ID == RoleID
                               select row.Name).FirstOrDefault();
                if (getRole != null)
                {
                    return getRole;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<Vendor_SP_GetRoleList_Result> GetRole()
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetRoleList(0)
                           select row).ToList();

                return lst.ToList();
            }
        }

        public static bool updateVendor_Schedule(int mappingId,bool deleteOldData,string Freq,DateTime Duedate,int duedays)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (true)
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.Vendor_Schedule
                                   where row.ProjectContractMappingID == mappingId
                                   select row.ID).ToList();
                        ids.ForEach(entry =>
                        {
                            Vendor_Schedule schedule = (from row in entities.Vendor_Schedule
                                                        where row.ID == entry
                                                           select row).FirstOrDefault();

                            entities.Vendor_Schedule.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    int step = 1;
                    int start = 1;
                    if (Freq == "M")
                    {
                        step = 1;
                    }
                    if (Freq == "Q")
                    {
                        step = 3;
                    }
                    if (Freq == "H")
                    {
                        start = 4;
                        step = 6;
                    }
                    if (Freq == "Y")
                    {
                        start = 4;//new
                        step = 12;
                    }
                    int SpecialMonth;
                    for (int month = start; month <= 12; month += step)
                    {
                        Vendor_Schedule complianceShedule = new Vendor_Schedule();
                        complianceShedule.ProjectContractMappingID = mappingId;
                        complianceShedule.ForMonth = month;

                        SpecialMonth = month;
                        if (Freq == "M" && SpecialMonth == 12)
                        {
                            SpecialMonth = 1;
                        }
                        else if (Freq == "Q" && SpecialMonth == 10)
                        {
                            SpecialMonth = 1;
                        }
                        else if (Freq == "H" && SpecialMonth == 10)//7
                        {
                            SpecialMonth = 4;
                        }
                        else if ((Freq == "Y") && SpecialMonth == 4)//1 new
                        {
                            SpecialMonth = 4;
                        }
                        else
                        {
                            SpecialMonth = SpecialMonth + step;
                        }

                        int lastdate = duedays;
                        if (Convert.ToInt32(Duedate.Day.ToString("D2")) > lastdate)
                        {
                            complianceShedule.SpecialDate = lastdate.ToString("D2") + SpecialMonth.ToString("D2");
                        }
                        else
                        {
                            complianceShedule.SpecialDate = lastdate.ToString("D2") + SpecialMonth.ToString("D2");
                        }

                        //int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                        //if (Convert.ToInt32(Duedate.Day.ToString("D2")) > lastdate)
                        //{
                        //    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                        //}
                        //else
                        //{
                        //    complianceShedule.SpecialDate = Duedate.Day.ToString("D2") + SpecialMonth.ToString("D2");
                        //}

                        entities.Vendor_Schedule.Add(complianceShedule);

                    }

                    entities.SaveChanges();
                }
            }
            return output;            
        }
        public static bool DeleteContractorUser(int UserID, int CustID, int ID)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ID > 0)
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.ID == ID
                                     && row.CustID == CustID
                                                   select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.Isdisable = 1;
                        obj.UpdatedBy = UserID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool DeleteUser(int UserID, int CustID, int UID)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (UID > 0)
                {
                    User obj = (from row in entities.Users
                                where row.ID == UID
                                     && row.CustomerID == CustID
                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = true;                        
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool EnableUser(int UserID, int CustID, int UID)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (UID > 0)
                {
                    User obj = (from row in entities.Users
                                where row.ID == UID
                                     && row.CustomerID == CustID
                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool DeleteProjectDocument(int UID, int CID, int PId,int FID)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (PId > 0 && FID > 0)
                {
                    Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                               where row.ID == FID
                                             && row.CustomerID == CID
                                             //&& row.ProjectID == PId
                                               select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static long CreateVendorDocument(Vendor_tbl_FileData tempDocument)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                entities.Vendor_tbl_FileData.Add(tempDocument);
                entities.SaveChanges();
                if (tempDocument.ID > 0)
                {
                    return tempDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static bool CheckExistProjectMaster(int ProjectID, int CID, string Name)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ProjectID == 0)
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.Name == Name
                                               && row.CustID == CID
                                               && row.IsDelete == false
                                             select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.Name == Name
                                               && row.CustID == CID
                                               && row.ID != ProjectID
                                               && row.IsDelete == false
                                             select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }
        
        public static bool UpdateProjectMaster(int ProjectID, int UserId, int CustomerID, string Projectoffice, int status,
            string name, string Jurisdiction, int catid, int elementid, int stateid, int cityid, string location, string locadd
            , int sitestatus, int director, int head, string authname, string authmail, string authcontact, int ProjectoffID,
            int locationID)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ProjectID > 0)
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.ID == ProjectID
                                              && row.CustID == CustomerID
                                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.CustID = CustomerID;
                        obj.Projectoffice = Projectoffice;
                        obj.Status = status;
                        obj.Name = name;
                        obj.Jurisdiction = Jurisdiction;
                        obj.CatID = catid;
                        obj.ElementId = elementid;
                        obj.StateID = stateid;
                        obj.CityID = cityid;
                        obj.Location = location;
                        obj.LocationAdd = locadd;
                        obj.SiteStatus = sitestatus;
                        obj.Director = director;
                        obj.Head = head;
                        obj.AuthorityName = authname;
                        obj.AuthorityMail = authmail;
                        obj.AuthorityContactNo = authcontact;
                        obj.IsDelete = false;
                        obj.CreatedBy = UserId;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UserId;
                        obj.UpdatedOn = DateTime.Now;
                        obj.Isdisable = false;
                        obj.LocationID = locationID;
                        obj.ProjectOfficeID = ProjectoffID;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.Name == name
                                                 && row.CustID == CustomerID
                                                select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ProjectMaster obj1 = new Vendor_ProjectMaster();
                        obj1.CustID = CustomerID;
                        obj1.Projectoffice = Projectoffice;
                        obj1.Status = status;
                        obj1.Name = name;
                        obj1.Jurisdiction = Jurisdiction;
                        obj1.CatID = catid;
                        obj1.ElementId = elementid;
                        obj1.StateID = stateid;
                        obj1.CityID = cityid;
                        obj1.Location = location;
                        obj1.LocationAdd = locadd;
                        obj1.SiteStatus = sitestatus;
                        obj1.Director = director;
                        obj1.Head = head;
                        obj1.AuthorityName = authname;
                        obj1.AuthorityMail = authmail;
                        obj1.AuthorityContactNo = authcontact;
                        obj1.IsDelete = false;
                        obj1.CreatedBy = UserId;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UserId;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.Isdisable = false;
                        obj1.LocationID = locationID;
                        obj1.ProjectOfficeID = ProjectoffID;
                        entities.Vendor_ProjectMaster.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UserId;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDelete = false;
                        obj.Isdisable = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static List<Vendor_SP_GetUserList_Result> GetUserMaster(int CID,string Code)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetUserList(CID)                           
                           select row).ToList();
                if (lst.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Code))
                    {
                        lst = lst.Where(x => Code.Contains(x.Code)).ToList();
                    }
                }
                return lst.ToList();
            }
        }

        public static bool UpdateSiteStatusMaster(int UID, int CID, int SSId, string SSName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (SSId > 0)
                {
                    Vendor_SiteStatus obj = (from row in entities.Vendor_SiteStatus
                                             where row.Id == SSId
                                              && row.CustID == CID
                                                 select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Sitestatus = SSName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_SiteStatus obj = (from row in entities.Vendor_SiteStatus
                                             where row.Sitestatus == SSName
                                                 && row.CustID == CID
                                                 select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_SiteStatus obj1 = new Vendor_SiteStatus();
                        obj1.Sitestatus = SSName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustID = CID;
                        entities.Vendor_SiteStatus.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool UpdateElementMaster(int UID, int CID, int EId, string EName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (EId > 0)
                {
                    Vendor_ProjectElement obj = (from row in entities.Vendor_ProjectElement
                                                 where row.Id == EId
                                              && row.CustID == CID
                                                        select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Element = EName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ProjectElement obj = (from row in entities.Vendor_ProjectElement
                                                 where row.Element == EName
                                                 && row.CustID == CID
                                                        select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ProjectElement obj1 = new Vendor_ProjectElement();
                        obj1.Element = EName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustID = CID;
                        entities.Vendor_ProjectElement.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool CheckExistSiteStatusMaster(int UID, int CID, int SSId, string SSName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (SSId == 0)
                {
                    Vendor_SiteStatus obj = (from row in entities.Vendor_SiteStatus
                                             where row.Sitestatus == SSName.Trim()
                                               && row.CustID == CID
                                               && row.IsDeleted == false
                                                 select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_SiteStatus obj = (from row in entities.Vendor_SiteStatus
                                             where row.Sitestatus == SSName.Trim()
                                               && row.CustID == CID
                                               && row.Id != SSId
                                               && row.IsDeleted == false
                                                 select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }
        
        public static bool CheckExistElementMaster(int UID, int CID, int EId, string EName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (EId == 0)
                {
                    Vendor_ProjectElement obj = (from row in entities.Vendor_ProjectElement
                                                 where row.Element == EName.Trim()
                                               && row.CustID == CID
                                               && row.IsDeleted == false
                                                        select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ProjectElement obj = (from row in entities.Vendor_ProjectElement
                                                 where row.Element == EName.Trim()
                                               && row.CustID == CID
                                               && row.Id != EId
                                               && row.IsDeleted == false
                                                        select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }
        
        public static bool UpdateCategoryMaster(int UID, int CID, int catId, string catName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (catId > 0)
                {
                    Vendor_ProjectCategorization obj = (from row in entities.Vendor_ProjectCategorization
                                                        where row.Id == catId
                                              && row.CustID == CID
                                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Category = catName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ProjectCategorization obj = (from row in entities.Vendor_ProjectCategorization
                                                        where row.Category == catName
                                                 && row.CustID == CID
                                                select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ProjectCategorization obj1 = new Vendor_ProjectCategorization();
                        obj1.Category = catName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustID = CID;
                        entities.Vendor_ProjectCategorization.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistCategoryMaster(int UID, int CID, int catId, string catName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (catId == 0)
                {
                    Vendor_ProjectCategorization obj = (from row in entities.Vendor_ProjectCategorization
                                                        where row.Category == catName.Trim()
                                               && row.CustID == CID
                                               && row.IsDeleted == false
                                                select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ProjectCategorization obj = (from row in entities.Vendor_ProjectCategorization
                                                        where row.Category == catName.Trim()
                                               && row.CustID == CID
                                               && row.Id != catId
                                               && row.IsDeleted == false
                                                select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistContractorMaster(int UID, int CID, string Email,int ID)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ID == 0)
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.SpocEmail == Email
                                                   && row.CustID == CID
                                                   && row.IsDelete == false
                                                   select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.SpocEmail == Email
                                          && row.CustID == CID
                                          && row.ID != ID
                                          && row.IsDelete == false
                                                   select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }

        public static bool UpdatestatusMaster(int UID, int CID, int statusId, string statusName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (statusId > 0)
                {
                    Vendor_ProjectStatus obj = (from row in entities.Vendor_ProjectStatus
                                                where row.Id == statusId
                                              && row.CustID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Status = statusName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ProjectStatus obj = (from row in entities.Vendor_ProjectStatus
                                                where row.Status == statusName
                                                 && row.CustID == CID
                                                select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ProjectStatus obj1 = new Vendor_ProjectStatus();
                        obj1.Status = statusName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustID = CID;
                        entities.Vendor_ProjectStatus.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static List<Vendor_ProjectCategorization> GetProjectCategoryMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_ProjectCategorization
                           where row.IsDeleted == false
                           && row.CustID == CID
                           select row).OrderBy(x => x.Category).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_ProjectElement> GetProjectElementMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_ProjectElement
                           where row.IsDeleted == false
                           && row.CustID == CID
                           select row).OrderBy(x => x.Element).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_SiteStatus> GetProjectSitestatusMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SiteStatus
                           where row.IsDeleted == false
                           && row.CustID == CID
                           select row).OrderBy(x => x.Sitestatus).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_ProjectStatus> GetProjectStatusMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_ProjectStatus
                           where row.IsDeleted == false
                           && row.CustID == CID
                           select row).OrderBy(x=>x.Status).ToList();

                return lst.ToList();
            }
        }

        public static bool CheckExistCityMaster(int UID, int CID, int CityId, int SID, string CityName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CityId == 0)
                {
                    Vendor_CityMaster obj = (from row in entities.Vendor_CityMaster
                                             where row.Name == CityName.Trim()
                                              && row.StateId == SID
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_CityMaster obj = (from row in entities.Vendor_CityMaster
                                             where row.Name == CityName.Trim()
                                                && row.StateId == SID
                                               && row.ID != CityId
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }

        public static bool DeleteContTypeMaster(int UID, int CID, int Id)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_ContractType obj = (from row in entities.Vendor_ContractType
                                               where row.ID == Id
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool checkExistforContractorMaster(int ContractorID, int CID)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (ContractorID > 0)
                {
                    Vendor_ContractorProjectMapping obj = (from row in entities.Vendor_ContractorProjectMapping
                                                           where (row.ContractorID == ContractorID
                                                           || (row.UnderContractorID == ContractorID && row.IsShowContractor == 1))
                                                           && row.CustID == CID
                                                           select row).FirstOrDefault();

                    if (obj != null)
                    {
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool checkExistforCityMaster(int UID, int CID, int Id)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_ProjectMaster obj = (from row in entities.Vendor_ProjectMaster
                                                where row.CityID == Id
                                               select row).FirstOrDefault();

                    if (obj != null)
                    {
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool checkExistforCityMasterinbranch(int UID, int CID, int Id)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Id > 0)
                {
                    CustomerBranch obj = (from row in entities.CustomerBranches
                                          where row.CityID == Id                                          
                                                select row).FirstOrDefault();

                    if (obj != null)
                    {
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool checkExistforContractTypeMaster(int UID, int CID, int Id)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_ContractorMaster obj = (from row in entities.Vendor_ContractorMaster
                                                   where row.ContractorType == Id
                                               && row.CustID == CID
                                               select row).FirstOrDefault();

                    if (obj != null)
                    {
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool checkExistforLicMaster(int UID, int CID, int Id)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_tbl_FileData obj = (from row in entities.Vendor_tbl_FileData
                                              where row.LicTypeID == Id
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        output = true;
                    }
                }
                return output;
            }
        }
        public static bool DeleteLicMaster(int UID, int CID, int Id)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (Id > 0)
                {
                    Vendor_LicenseType obj = (from row in entities.Vendor_LicenseType
                                              where row.ID == Id
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = true;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistStatusMaster(int UID, int CID, int StatusId, string StatusName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (StatusId == 0)
                {
                    Vendor_ProjectStatus obj = (from row in entities.Vendor_ProjectStatus
                                                where row.Status == StatusName.Trim()
                                               && row.CustID == CID
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ProjectStatus obj = (from row in entities.Vendor_ProjectStatus
                                                where row.Status == StatusName.Trim()
                                               && row.CustID == CID
                                               && row.Id != StatusId
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistContractTypeMaster(int UID, int CID, int CTId, string ContractType)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CTId == 0)
                {
                    Vendor_ContractType obj = (from row in entities.Vendor_ContractType
                                               where row.Name == ContractType.Trim()
                                               && row.CustomerID == CID
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_ContractType obj = (from row in entities.Vendor_ContractType
                                               where row.Name == ContractType
                                               && row.CustomerID == CID
                                               && row.ID != CTId
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }

        public static bool CheckExistLicMaster(int UID, int CID, int LicId, string LicName)
        {
            bool output = true;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (LicId == 0)
                {
                    Vendor_LicenseType obj = (from row in entities.Vendor_LicenseType
                                              where row.Name == LicName.Trim()
                                               && row.CustomerID == CID
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                else
                {
                    Vendor_LicenseType obj = (from row in entities.Vendor_LicenseType
                                              where row.Name == LicName.Trim()
                                               && row.CustomerID == CID
                                               && row.ID != LicId
                                               && row.IsDeleted == false
                                              select row).FirstOrDefault();
                    if (obj != null)
                    {
                        output = false;
                    }
                }
                return output;
            }
        }
        
        public static bool UpdateLicMaster(int UID, int CID, int LicId, string LicName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (LicId > 0)
                {
                    Vendor_LicenseType obj = (from row in entities.Vendor_LicenseType
                                              where row.ID == LicId
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Name = LicName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustomerID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_LicenseType obj = (from row in entities.Vendor_LicenseType
                                             where row.Name == LicName
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_LicenseType obj1 = new Vendor_LicenseType();
                        obj1.Name = LicName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustomerID = CID;
                        entities.Vendor_LicenseType.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool UpdateContTypeMaster(int UID, int CID, int CTId, string CTName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CTId > 0)
                {
                    Vendor_ContractType obj = (from row in entities.Vendor_ContractType
                                               where row.ID == CTId
                                              && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Name = CTName;
                        obj.CreatedBy = UID;
                        obj.CreatedOn = DateTime.Now;
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.CustomerID = CID;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_ContractType obj = (from row in entities.Vendor_ContractType
                                               where row.Name == CTName
                                               && row.CustomerID == CID
                                              select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_ContractType obj1 = new Vendor_ContractType();
                        obj1.Name = CTName;
                        obj1.CreatedBy = UID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;
                        obj1.IsDeleted = false;
                        obj1.CustomerID = CID;
                        entities.Vendor_ContractType.Add(obj1);

                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        obj.UpdatedBy = UID;
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static List<ContractDetails> GetContractDetail(int CID, int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     && Contractor.ID == ContID
                                     select Contractor).OrderBy(x=>x.ContractorName).ToList();

                foreach (var row in ContractorObj)
                {
                    ContractDetails Details = new ContractDetails();
                    Details.PID = row.PID;
                    Details.ContractorID = row.ID;
                    Details.ContractorName = row.ContractorName;
                    Details.SPOCName = row.SpocName;
                    Details.SPOCEmailID = row.SpocEmail;
                    Details.SPOCMobileNo = row.SpocContactNo;
                    Details.NatureOfWork = row.NatureOfWork;
                    Details.ContractorTypeID = (int)row.ContractorType;
                    ContractorDetailsList.Add(Details);
                }
                ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                return ContractorDetailsList.ToList();
            }
        }

        public static List<ContractDetails> GetdrpContractMaster(int CID,int ContID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     && Contractor.ID != ContID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();
               
                foreach (var row in ContractorObj)
                {
                    ContractDetails Details = new ContractDetails();
                    Details.PID = row.PID;
                    Details.ContractorID = row.ID;
                    Details.ContractorName = row.ContractorName;
                    Details.SPOCName = row.SpocName;
                    Details.SPOCEmailID = row.SpocEmail;
                    Details.SPOCMobileNo = row.SpocContactNo;
                    Details.NatureOfWork = row.NatureOfWork;
                    Details.ContractorTypeID = (int)row.ContractorType;
                    ContractorDetailsList.Add(Details);
                }
                ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                return ContractorDetailsList.ToList();
            }
        }

        public static List<Vendor_ContractorMaster> GetContractMasterList(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<Vendor_ContractorMaster> ContractorDetailsList = new List<Vendor_ContractorMaster>();

                ContractorDetailsList = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();
                               
                return ContractorDetailsList.ToList();
            }
        }
        public static List<ContractDetails> GetContractMasterformapping(int pid, int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();
                if (pid > 0)
                {
                    ContractorObj = ContractorObj.Where(x => x.PID == pid).ToList();
                }

                if (ContractorObj.Count > 0)
                {
                    List<Vendor_ContractorProjectMapping> mappinglist = (from d in entities.Vendor_ContractorProjectMapping
                                                                         where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                         select d).ToList();

                    mappinglist = mappinglist.Where(x => x.Isdisable == 0 || x.Isdisable == null).ToList();

                    List<Vendor_ProjectMaster> projectmasterlst = (from d in entities.Vendor_ProjectMaster
                                                                   where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                         && d.Isdisable == false
                                                                   select d).ToList();

                    List<Vendor_tbl_FileData> filedetails = (from d in entities.Vendor_tbl_FileData
                                                             where d.CustomerID == CID
                                                                        && d.IsDeleted == false
                                                                        && d.IsMapping == 1
                                                                        && d.ProjectID == 0
                                                             select d).ToList();

                    var locationdetails = GetLocationDetails(CID);

                    foreach (var row in ContractorObj)
                    {
                        var mappinglistx = mappinglist.Where(x => x.ContractorID == row.ID).ToList();
                        var getValuelst = (from d in mappinglist
                                           join e in projectmasterlst
                                           on d.ProjectID equals e.ID
                                           where d.ContractorID == row.ID
                                            && d.IsDelete == false
                                           && e.IsDelete == false
                                           select new
                                           {
                                               ProjectID = d.ProjectID,
                                               ProjectMappingID = d.ID,
                                               ProjectName = e.Name,
                                               LID = e.LocationID,
                                               SID = e.StateID,
                                               ISContractor = d.IsSubcontract,
                                               Isdisable = d.Isdisable
                                           }).ToList();

                        string disval = "Active";
                        if (getValuelst.Count > 0)
                        {
                            foreach (var getValue in getValuelst)
                            {
                                disval = "Active";
                                if (getValue.Isdisable == 1)
                                    disval = "Inactive";

                                //if (row.Isdisable == 1)
                                //    disval = "Inactive";

                                int ProjectID = 0;
                                int ProjectMappingID = 0;
                                string ProjectName = "";
                                int HeadCount = 0;
                                int LocationID = 0;
                                string LocationName = "";
                                int StateID = 0;
                                string ISContractor = "";


                                if (getValue != null)
                                {
                                    ProjectID = getValue.ProjectID;
                                    ProjectMappingID = getValue.ProjectMappingID;
                                    ProjectName = getValue.ProjectName;
                                    LocationID = getValue.LID;
                                    StateID = (int)getValue.SID;
                                    if (getValue.ISContractor > 0)
                                        ISContractor = "Sub-Contractor";
                                    else
                                        ISContractor = "Contractor";

                                    var getlocations = (from d in locationdetails
                                                        where d.ID == LocationID
                                                         && d.IsDeleted == false
                                                        select d).FirstOrDefault();

                                    if (getlocations != null)
                                    {
                                        LocationName = getlocations.Name;
                                    }

                                    var getFiles = (from d in filedetails
                                                    where d.ProjectMappingID == ProjectMappingID
                                                     && d.IsDeleted == false
                                                    select d).ToList();

                                    foreach (var item in getFiles)
                                    {
                                        HeadCount = HeadCount + item.HeadCount;
                                    }
                                }

                                ContractDetails Details = new ContractDetails();
                                Details.PID = ProjectID; // GetProjectMappingPID((int)row.ID);
                                Details.ContractorID = row.ID;
                                Details.ContractorName = row.ContractorName;
                                Details.SPOCName = row.SpocName;
                                Details.SPOCEmailID = row.SpocEmail;
                                Details.SPOCMobileNo = row.SpocContactNo;
                                Details.NatureOfWork = row.NatureOfWork;
                                Details.ContractorTypeID = (int)row.ContractorType;
                                Details.ProjectMappingName = ProjectName; // GetProjectMappingName((int)row.ID);
                                Details.ProjectMappingID = ProjectMappingID; // GetProjectMappingID((int)row.ID);
                                Details.HeadCount = HeadCount; // GetHeadCountID((int)row.ID);
                                Details.LocationID = LocationID;
                                Details.LocationName = LocationName;
                                Details.StateID = StateID;
                                Details.ISContractor = ISContractor;
                                Details.IsDisableValues = disval;

                                ContractorDetailsList.Add(Details);
                            }
                        }
                        else
                        {
                            //int ProjectID = 0;
                            //int ProjectMappingID = 0;
                            //string ProjectName = "";
                            //int HeadCount = 0;
                            //int LocationID = 0;
                            //string LocationName = "";
                            //int StateID = 0;
                            //string ISContractor = "";

                            //ContractDetails Details = new ContractDetails();
                            //Details.PID = ProjectID; // GetProjectMappingPID((int)row.ID);
                            //Details.ContractorID = row.ID;
                            //Details.ContractorName = row.ContractorName;
                            //Details.SPOCName = row.SpocName;
                            //Details.SPOCEmailID = row.SpocEmail;
                            //Details.SPOCMobileNo = row.SpocContactNo;
                            //Details.NatureOfWork = row.NatureOfWork;
                            //Details.ContractorTypeID = (int)row.ContractorType;
                            //Details.ProjectMappingName = ProjectName; // GetProjectMappingName((int)row.ID);
                            //Details.ProjectMappingID = ProjectMappingID; // GetProjectMappingID((int)row.ID);
                            //Details.HeadCount = HeadCount; // GetHeadCountID((int)row.ID);
                            //Details.LocationID = LocationID;
                            //Details.LocationName = LocationName;
                            //Details.StateID = StateID;
                            //Details.ISContractor = ISContractor;
                            //Details.IsDisableValues = disval;

                            //ContractorDetailsList.Add(Details);
                        }
                    }

                    ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                }
                return ContractorDetailsList.ToList();
            }
        }

        public static List<ContractDetails> GetContractMaster(int pid, int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();

                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();
                if (pid > 0)
                {
                    ContractorObj = ContractorObj.Where(x => x.PID == pid).ToList();
                }

                if (ContractorObj.Count > 0)
                {
                    List<Vendor_ContractorProjectMapping> mappinglist = (from d in entities.Vendor_ContractorProjectMapping
                                                                         where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                         select d).ToList();

                    List<Vendor_ProjectMaster> projectmasterlst = (from d in entities.Vendor_ProjectMaster
                                                                   where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                         && d.Isdisable == false
                                                                   select d).ToList();

                    List<Vendor_tbl_FileData> filedetails = (from d in entities.Vendor_tbl_FileData
                                                             where d.CustomerID == CID
                                                                        && d.IsDeleted == false
                                                                        && d.IsMapping == 1
                                                                        && d.ProjectID == 0
                                                             select d).ToList();

                    var locationdetails = GetLocationDetails(CID);

                    foreach (var row in ContractorObj)
                    {
                        var mappinglistx = mappinglist.Where(x => x.ContractorID == row.ID).ToList();
                        var getValuelst = (from d in mappinglist
                                           join e in projectmasterlst
                                           on d.ProjectID equals e.ID
                                           where d.ContractorID == row.ID
                                            && d.IsDelete == false
                                           && e.IsDelete == false
                                           select new
                                           {
                                               ProjectID = d.ProjectID,
                                               ProjectMappingID = d.ID,
                                               ProjectName = e.Name,
                                               LID = e.LocationID,
                                               SID = e.StateID,
                                               ISContractor = d.IsSubcontract,
                                               Isdisable = d.Isdisable
                                           }).ToList();

                        string disval = "Active";
                        if (getValuelst.Count > 0)
                        {
                            foreach (var getValue in getValuelst)
                            {
                                disval = "Active";
                                if (getValue.Isdisable == 1)
                                    disval = "Inactive";
                                
                                //if (row.Isdisable == 1)
                                //    disval = "Inactive";

                                int ProjectID = 0;
                                int ProjectMappingID = 0;
                                string ProjectName = "";
                                int HeadCount = 0;
                                int LocationID = 0;
                                string LocationName = "";
                                int StateID = 0;
                                string ISContractor = "";


                                if (getValue != null)
                                {
                                    ProjectID = getValue.ProjectID;
                                    ProjectMappingID = getValue.ProjectMappingID;
                                    ProjectName = getValue.ProjectName;
                                    LocationID = getValue.LID;
                                    StateID = (int)getValue.SID;
                                    if (getValue.ISContractor > 0)
                                        ISContractor = "Sub-Contractor";
                                    else
                                        ISContractor = "Contractor";

                                    var getlocations = (from d in locationdetails
                                                        where d.ID == LocationID
                                                         && d.IsDeleted == false
                                                        select d).FirstOrDefault();

                                    if (getlocations != null)
                                    {
                                        LocationName = getlocations.Name;
                                    }

                                    var getFiles = (from d in filedetails
                                                    where d.ProjectMappingID == ProjectMappingID
                                                     && d.IsDeleted == false
                                                    select d).ToList();

                                    foreach (var item in getFiles)
                                    {
                                        HeadCount = HeadCount + item.HeadCount;
                                    }
                                }

                                ContractDetails Details = new ContractDetails();
                                Details.PID = ProjectID; // GetProjectMappingPID((int)row.ID);
                                Details.ContractorID = row.ID;
                                Details.ContractorName = row.ContractorName;
                                Details.SPOCName = row.SpocName;
                                Details.SPOCEmailID = row.SpocEmail;
                                Details.SPOCMobileNo = row.SpocContactNo;
                                Details.NatureOfWork = row.NatureOfWork;
                                Details.ContractorTypeID = (int)row.ContractorType;
                                Details.ProjectMappingName = ProjectName; // GetProjectMappingName((int)row.ID);
                                Details.ProjectMappingID = ProjectMappingID; // GetProjectMappingID((int)row.ID);
                                Details.HeadCount = HeadCount; // GetHeadCountID((int)row.ID);
                                Details.LocationID = LocationID;
                                Details.LocationName = LocationName;
                                Details.StateID = StateID;
                                Details.ISContractor = ISContractor;
                                Details.IsDisableValues = disval;

                                ContractorDetailsList.Add(Details);
                            }
                        }
                        else
                        {
                            //string disval = "Active";
                            //if (row.Isdisable == 1)
                            //    disval = "Inactive";

                            int ProjectID = 0;
                            int ProjectMappingID = 0;
                            string ProjectName = "";
                            int HeadCount = 0;
                            int LocationID = 0;
                            string LocationName = "";
                            int StateID = 0;
                            string ISContractor = "";

                            ContractDetails Details = new ContractDetails();
                            Details.PID = ProjectID; // GetProjectMappingPID((int)row.ID);
                            Details.ContractorID = row.ID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SpocName;
                            Details.SPOCEmailID = row.SpocEmail;
                            Details.SPOCMobileNo = row.SpocContactNo;
                            Details.NatureOfWork = row.NatureOfWork;
                            Details.ContractorTypeID = (int)row.ContractorType;
                            Details.ProjectMappingName = ProjectName; // GetProjectMappingName((int)row.ID);
                            Details.ProjectMappingID = ProjectMappingID; // GetProjectMappingID((int)row.ID);
                            Details.HeadCount = HeadCount; // GetHeadCountID((int)row.ID);
                            Details.LocationID = LocationID;
                            Details.LocationName = LocationName;
                            Details.StateID = StateID;
                            Details.ISContractor = ISContractor;
                            Details.IsDisableValues = disval;

                            ContractorDetailsList.Add(Details);
                        }
                    } 
                    
                    ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                }
                return ContractorDetailsList.ToList();
            }
        }
        
        public static List<ContractDetails> GetContractMasterold(int pid,int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                
                var ContractorObj = (from Contractor in entities.Vendor_ContractorMaster
                                     where Contractor.IsDelete == false
                                     && Contractor.CustID == CID
                                     select Contractor).OrderBy(x => x.ContractorName).ToList();
                if (pid > 0)
                {
                    ContractorObj = ContractorObj.Where(x => x.PID == pid).ToList();
                }

                if (ContractorObj.Count > 0)
                {
                    List<Vendor_ContractorProjectMapping> mappinglist = (from d in entities.Vendor_ContractorProjectMapping
                                                                         where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                         select d).ToList();

                    List<Vendor_ProjectMaster> projectmasterlst = (from d in entities.Vendor_ProjectMaster
                                                                   where d.CustID == CID
                                                                         && d.IsDelete == false
                                                                   select d).ToList();

                    List<Vendor_tbl_FileData> filedetails = (from d in entities.Vendor_tbl_FileData
                                                             where d.CustomerID == CID
                                                                        && d.IsDeleted == false
                                                                        && d.IsMapping == 1
                                                                        && d.ProjectID == 0
                                                             select d).ToList();

                    var locationdetails = GetLocationDetails(CID);

                    foreach (var row in ContractorObj)
                    {
                        string disval = "Active";
                        if (row.Isdisable == 1)
                            disval = "Inactive";

                        int ProjectID = 0;
                        int ProjectMappingID = 0;
                        string ProjectName = "";
                        int HeadCount = 0;
                        int LocationID = 0;
                        string LocationName = "";
                        int StateID = 0;
                        string ISContractor = "";

                        var getValue = (from d in mappinglist
                                        join e in projectmasterlst
                                        on d.ProjectID equals e.ID
                                        where d.ContractorID == row.ID
                                         && d.IsDelete == false
                                        && e.IsDelete == false
                                        select new
                                        {
                                            ProjectID = d.ProjectID,
                                            ProjectMappingID = d.ID,
                                            ProjectName = e.Name,
                                            LID = e.LocationID,
                                            SID = e.StateID,
                                            ISContractor = d.IsSubcontract
                                        }).FirstOrDefault();

                        if (getValue != null)
                        {
                            ProjectID = getValue.ProjectID;
                            ProjectMappingID = getValue.ProjectMappingID;
                            ProjectName = getValue.ProjectName;
                            LocationID = getValue.LID;
                            StateID = (int)getValue.SID;
                            if (getValue.ISContractor > 0)                            
                                ISContractor = "Sub-Contractor";                           
                            else
                                ISContractor = "Contractor";

                            var getlocations = (from d in locationdetails
                                                where d.ID == LocationID
                                                 && d.IsDeleted == false
                                                select d).FirstOrDefault();

                            if (getlocations != null)
                            {
                                LocationName = getlocations.Name;
                            }

                            var getFiles = (from d in filedetails
                                            where d.ProjectMappingID == ProjectMappingID
                                             && d.IsDeleted == false
                                            select d).ToList();

                            foreach (var item in getFiles)
                            {
                                HeadCount = HeadCount + item.HeadCount;
                            }
                        }

                        ContractDetails Details = new ContractDetails();
                        Details.PID = ProjectID; // GetProjectMappingPID((int)row.ID);
                        Details.ContractorID = row.ID;
                        Details.ContractorName = row.ContractorName;
                        Details.SPOCName = row.SpocName;
                        Details.SPOCEmailID = row.SpocEmail;
                        Details.SPOCMobileNo = row.SpocContactNo;
                        Details.NatureOfWork = row.NatureOfWork;
                        Details.ContractorTypeID = (int)row.ContractorType;
                        Details.ProjectMappingName = ProjectName; // GetProjectMappingName((int)row.ID);
                        Details.ProjectMappingID = ProjectMappingID; // GetProjectMappingID((int)row.ID);
                        Details.HeadCount = HeadCount; // GetHeadCountID((int)row.ID);
                        Details.LocationID = LocationID;
                        Details.LocationName = LocationName;
                        Details.StateID = StateID;
                        Details.ISContractor = ISContractor;
                        Details.IsDisableValues = disval;
                        
                        ContractorDetailsList.Add(Details);
                    }

                    ContractorDetailsList = ContractorDetailsList.OrderBy(x => x.ContractorName).ToList();
                }
                return ContractorDetailsList.ToList();
            }
        }

        public static bool DeleteCityMaster(int UID, int CID, int CityId)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CityId > 0)
                {
                    Vendor_CityMaster obj = (from row in entities.Vendor_CityMaster
                                             where row.ID == CityId
                                             //&& row.CustId == CID
                                             select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.IsDeleted = true;
                        //obj.UpdatedBy = UID;
                        //obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }

        public static bool UpdateCityMaster(int UID,int CID,int CityId,int SID,string CityName)
        {
            bool output = false;
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                if (CityId > 0)
                {
                    Vendor_CityMaster obj = (from row in entities.Vendor_CityMaster
                                             where row.ID == CityId
                                             select row).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.Name = CityName;
                        obj.StateId = SID;
                        obj.IsDeleted = false;
                        //obj.CreatedBy = UID;
                        //obj.CreatedOn = DateTime.Now;
                        //obj.UpdatedBy = UID;
                        //obj.UpdatedOn = DateTime.Now;
                        //obj.CustId = CID;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                else
                {
                    Vendor_CityMaster obj = (from row in entities.Vendor_CityMaster
                                             where row.StateId == SID
                                             && row.Name == CityName
                                             select row).FirstOrDefault();

                    if (obj == null)
                    {
                        Vendor_CityMaster obj1 = new Vendor_CityMaster();
                        obj1.Name = CityName;
                        obj1.StateId = SID;
                        obj1.IsDeleted = false;
                        //obj1.CreatedBy = UID;
                        //obj1.CreatedOn = DateTime.Now;
                        //obj1.UpdatedBy = UID;
                        //obj1.UpdatedOn = DateTime.Now;
                        //obj1.CustId = CID;
                        entities.Vendor_CityMaster.Add(obj1);
                        entities.SaveChanges();
                        output = true;
                    }
                    else
                    {
                        //obj.UpdatedBy = UID;
                        //obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
        }
  
        public static List<Vendor_ContractType> GetContractTypeMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_ContractType
                           where row.IsDeleted == false
                           && row.CustomerID == CID
                           select row).OrderBy(x => x.Name).ToList();
                return lst.ToList();
            }
        }
        public static List<Vendor_LicenseType> GetLicenseMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_LicenseType
                           where row.IsDeleted == false
                           && row.CustomerID == CID
                           select row).OrderBy(x => x.Name).ToList();
                return lst.ToList();
            }
        }
        
        public static List<Vendor_SP_GetFileData_Result> GetProjectFiles(int CID,int PID,int Type)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetFileData(CID)
                           select row).ToList();

                if (Type == 0)
                {
                    lst = lst.Where(x => x.ProjectID == PID && x.IsMapping == 0 && x.ProjectMappingID == 0).ToList();
                }
                if (Type == 1)
                {
                    lst = lst.Where(x => x.ProjectMappingID == PID && x.IsMapping == 1 && x.ProjectID == 0).ToList();
                }
                return lst.ToList();
            }
        }
        public static List<Vendor_SP_GetProjectMaster_Result> GetProjectMasterbylocationids(int CID,List<int> locaionIDs)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetProjectMaster(CID)
                           select row).OrderBy(x => x.Name).ToList();

                if (locaionIDs.Count > 0)
                {
                    lst = lst.Where(x => locaionIDs.Contains(x.LocationID)).ToList();
                }
                return lst.ToList();
            }
        }
        public static List<Vendor_SP_GetProjectMaster_Result> GetProjectMaster(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetProjectMaster(CID)
                           select row).OrderBy(x => x.Name).ToList();
                
                return lst.ToList();
            }
        }
        public static List<Vendor_SP_GetCityData_Result> GetCityMaster(int CID,int StateID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetCityData()
                           select row).ToList();

                if (StateID > 0)
                {
                    lst = lst.Where(x => x.StateId == StateID).ToList();
                }
                return lst.ToList();
            }
        }

        public static List<Vendor_CheckListStatus> GetchecklistStatusMaster()
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_CheckListStatus                          
                           select row).ToList();
                return lst.ToList();
            }
        }

        public static List<RoleDTO> GetStateMaster()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lst = (from row in entities.States
                           where row.IsDeleted == false
                           select new RoleDTO
                           {
                               ID = row.ID,
                               Name = row.Name
                           }).OrderBy(s => s.Name).ToList();
                return lst.ToList();
            }
        }

        public static List<Vendor_SP_GetEntitiesMasterList_Result> GetVendorEntities(int CID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetEntitiesMasterList(CID)
                           select row).OrderBy(x => x.Name).ToList();

                return lst.ToList();
            }
        }

        public static List<Vendor_SP_GetEntitiesMasterBranchList_Result> GetEntitiesBranches(int CID, int ParentId)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var lst = (from row in entities.Vendor_SP_GetEntitiesMasterBranchList(CID, ParentId)
                           select row).OrderBy(x => x.Name).ToList();

                return lst.ToList();
            }
        }

        public static List<Industry> GetEntityIndustry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Industries;

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<NodeType> GetSubEntityType()
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var industries = entities.NodeTypes;

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<CompanyType> GetEntityCompanyType()
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var ComanyType = (from row in entities.CompanyTypes
                                  where row.ID <= 3
                                  select row).ToList();

                //var ComanyType = entities.CompanyTypes;

                return ComanyType.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<M_LegalEntityType> GetLegalEntityType()
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var industries = entities.M_LegalEntityType;

                return industries.OrderBy(entry => entry.EntityTypeName).ToList();
            }
        }

        public static CustomerBranch GetCustomerBranchDetailsBasedOnCustomerId(string BranchId)
        {
            long Bid = Convert.ToInt64(BranchId);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return entities.CustomerBranches.Where(x => x.ID == Bid).FirstOrDefault();


            }
        }

        public static string SaveEntityDetails(CustomerBranch DetailsObj)
        {
            var result = "";
            try
            {
                long LastInsertedId = 0;
                bool IsNewRecord = false;
                if (DetailsObj.ID == 0)
                {
                    IsNewRecord = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (IsNewRecord)
                    {
                        if (CheckEntityNameExist(DetailsObj.Name, DetailsObj.CustomerID))
                        {
                            DetailsObj.CreatedOn = DateTime.Now;

                            entities.CustomerBranches.Add(DetailsObj);
                            entities.SaveChanges();


                            var det = new CustomerBranchIndustryMapping();
                            det.CustomerBranchID = DetailsObj.ID;
                            det.IsActive = true;
                            det.EditedBy = Convert.ToInt32(DetailsObj.CreatedBy);
                            det.EditedDate = DateTime.Now;
                            var StrArray = DetailsObj.Territory.Split(',');

                            StrArray = StrArray.Distinct().ToArray();
                            foreach (var item in StrArray)
                            {
                                if (item != null && item != "")
                                {
                                    det.IndustryID = Convert.ToInt32(item);
                                    entities.CustomerBranchIndustryMappings.Add(det);
                                    entities.SaveChanges();
                                }
                            }



                            result = "Success";
                        }
                        else
                        {
                            result = "Exist";
                        }


                    }
                    else
                    {
                        CustomerBranch detail = entities.CustomerBranches.Find(DetailsObj.ID);
                        if (detail != null)
                        {
                            detail.Name = DetailsObj.Name;
                            detail.Type = DetailsObj.Type;
                            detail.AddressLine1 = DetailsObj.AddressLine1;
                            detail.AddressLine2 = DetailsObj.AddressLine2;
                            detail.StateID = DetailsObj.StateID;
                            detail.CityID = DetailsObj.CityID;
                            detail.Industry = DetailsObj.Industry;
                            detail.IsDeleted = DetailsObj.IsDeleted;
                            detail.ContactPerson = DetailsObj.ContactPerson;
                            detail.Landline = DetailsObj.Landline;
                            detail.Mobile = DetailsObj.Mobile;
                            detail.EmailID = DetailsObj.EmailID;
                            detail.PinCode = DetailsObj.PinCode;
                            detail.LegalRelationShipID = DetailsObj.LegalRelationShipID;

                            detail.Status = DetailsObj.Status;
                            detail.LegalEntityTypeID = DetailsObj.LegalEntityTypeID;
                            detail.ComType = DetailsObj.ComType;
                            detail.AuditPR = DetailsObj.AuditPR;
                            entities.Entry(detail).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();

                            entities.CustomerBranchIndustryMappings.RemoveRange(entities.CustomerBranchIndustryMappings.Where(x => x.CustomerBranchID == DetailsObj.ID));
                            entities.SaveChanges();

                            var det = new CustomerBranchIndustryMapping();
                            det.CustomerBranchID = DetailsObj.ID;
                            det.IsActive = true;
                            det.EditedBy = Convert.ToInt32(DetailsObj.CreatedBy);
                            det.EditedDate = DateTime.Now;
                            var StrArray = DetailsObj.Territory.Split(',');

                            StrArray = StrArray.Distinct().ToArray();
                            foreach (var item in StrArray)
                            {
                                if (item != null && item != "")
                                {
                                    det.IndustryID = Convert.ToInt32(item);
                                    entities.CustomerBranchIndustryMappings.Add(det);
                                    entities.SaveChanges();
                                }

                            }
                            result = "Updated";


                        }


                    }
                }
                using (AuditControlEntities mst_entities = new AuditControlEntities())
                {
                    if (IsNewRecord && (result == "Success" || result == "Updated"))
                    {

                        mst_CustomerBranch customerBranch1 = new mst_CustomerBranch()
                        {
                            ID = 0,
                            Name = DetailsObj.Name,
                            Type = DetailsObj.Type,
                            ComType = DetailsObj.ComType,
                            AddressLine1 = DetailsObj.AddressLine1,
                            AddressLine2 = DetailsObj.AddressLine2,
                            StateID = Convert.ToInt32(DetailsObj.StateID),
                            CityID = Convert.ToInt32(DetailsObj.CityID),
                            //Others = tbxOther.Text.Trim(),
                            PinCode = DetailsObj.PinCode,
                            Industry = Convert.ToInt32(DetailsObj.Industry),
                            ContactPerson = DetailsObj.ContactPerson.Trim(),
                            Landline = DetailsObj.Landline,
                            Mobile = DetailsObj.Mobile,
                            EmailID = DetailsObj.EmailID.Trim(),
                            CustomerID = DetailsObj.CustomerID,
                            ParentID = DetailsObj.ParentID,
                            Status = Convert.ToInt32(DetailsObj.Status),

                            IsDeleted = DetailsObj.IsDeleted,
                            LegalRelationShipID = DetailsObj.LegalRelationShipID,
                            LegalEntityTypeID = DetailsObj.LegalEntityTypeID,
                            AuditPR = DetailsObj.AuditPR,
                            CreatedOn = DetailsObj.CreatedOn
                        };
                        //mst_entities.Entry(customerBranch1).State = System.Data.Entity.EntityState.Modified;
                        mst_entities.mst_CustomerBranch.Add(customerBranch1);
                        mst_entities.SaveChanges();
                    }
                    else
                    {

                        mst_CustomerBranch customerBranch1 = new mst_CustomerBranch()
                        {
                            ID = DetailsObj.ID,
                            Name = DetailsObj.Name,
                            Type = DetailsObj.Type,
                            ComType = Convert.ToByte(DetailsObj.ComType),
                            AddressLine1 = DetailsObj.AddressLine1,
                            AddressLine2 = DetailsObj.AddressLine2,
                            StateID = Convert.ToInt32(DetailsObj.StateID),
                            CityID = Convert.ToInt32(DetailsObj.CityID),
                            //Others = tbxOther.Text.Trim(),
                            PinCode = DetailsObj.PinCode,
                            Industry = Convert.ToInt32(DetailsObj.Industry),
                            ContactPerson = DetailsObj.ContactPerson.Trim(),
                            Landline = DetailsObj.Landline,
                            Mobile = DetailsObj.Mobile,
                            EmailID = DetailsObj.EmailID.Trim(),
                            CustomerID = DetailsObj.CustomerID,
                            ParentID = DetailsObj.ParentID,
                            Status = Convert.ToInt32(DetailsObj.Status),

                            IsDeleted = DetailsObj.IsDeleted,
                            LegalRelationShipID = DetailsObj.LegalRelationShipID,
                            LegalEntityTypeID = DetailsObj.LegalEntityTypeID,
                            AuditPR = DetailsObj.AuditPR,
                            CreatedOn = DateTime.Today



                        };
                        //mst_entities.Entry(customerBranch1).State = System.Data.Entity.EntityState.Modified;
                        //mst_entities.mst_CustomerBranch.Add(customerBranch1);
                        //mst_entities.SaveChanges();

                        mst_entities.Entry(customerBranch1).State = System.Data.Entity.EntityState.Modified;
                        mst_entities.SaveChanges();


                    }
                }



            }
            catch (Exception Ex)
            {
                return "error";
            }
            return result;
        }

        public static void DeleteEntityDetails(CustomerBranch DetailsObj)
        {
            try
            {
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    int cb = Convert.ToInt32(DetailsObj.CreatedBy);
                    entities.sp_Vendor_DeleteEntityDetails(DetailsObj.CustomerID, DetailsObj.ID, cb, DetailsObj.IsDeleted);
                }
                using (AuditControlEntities mst_entities = new AuditControlEntities())
                {
                    int cb = Convert.ToInt32(DetailsObj.CreatedBy);
                    mst_entities.sp_Vendor_DeleteEntityDetailsInMST(DetailsObj.CustomerID, DetailsObj.ID, cb, DetailsObj.IsDeleted);

                }
            }
            catch (Exception Ex)
            {

            }
        }

        //public static string SaveEntityDetails(CustomerBranch DetailsObj)
        //{
        //    var result = "";
        //    try
        //    {
        //        long LastInsertedId = 0;
        //        bool IsNewRecord = false;
        //        if (DetailsObj.ID == 0)
        //        {
        //            IsNewRecord = true;
        //        }
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (IsNewRecord)
        //            {
        //                if (CheckEntityNameExist(DetailsObj.Name,DetailsObj.CustomerID))
        //                {
        //                    DetailsObj.CreatedOn = DateTime.Now;

        //                    entities.CustomerBranches.Add(DetailsObj);
        //                    entities.SaveChanges();



        //                    var det = new CustomerBranchIndustryMapping();
        //                    det.CustomerBranchID = DetailsObj.ID;
        //                    det.IsActive = true;
        //                    det.EditedBy = Convert.ToInt32(DetailsObj.CreatedBy);
        //                    det.EditedDate = DateTime.Now;
        //                    var StrArray = DetailsObj.Territory.Split(',');

        //                    StrArray = StrArray.Distinct().ToArray();
        //                    foreach (var item in StrArray)
        //                    {
        //                        if (item != null && item != "")
        //                        {
        //                            det.IndustryID = Convert.ToInt32(item);
        //                            entities.CustomerBranchIndustryMappings.Add(det);
        //                            entities.SaveChanges();
        //                        }
        //                    }
        //                    result = "success";
        //                }
        //                else
        //                {
        //                    result = "Exist";
        //                }


        //            }
        //            else
        //            {
        //                CustomerBranch detail = entities.CustomerBranches.Find(DetailsObj.ID);
        //                if (detail != null)
        //                {
        //                    detail.Name = DetailsObj.Name;
        //                    detail.Type = DetailsObj.Type;
        //                    detail.AddressLine1 = DetailsObj.AddressLine1;
        //                    detail.AddressLine2 = DetailsObj.AddressLine2;
        //                    detail.StateID = DetailsObj.StateID;
        //                    detail.CityID = DetailsObj.CityID;
        //                    detail.Industry = DetailsObj.Industry;
        //                    detail.IsDeleted = DetailsObj.IsDeleted;
        //                    detail.ContactPerson = DetailsObj.ContactPerson;
        //                    detail.Landline = DetailsObj.Landline;
        //                    detail.Mobile = DetailsObj.Mobile;
        //                    detail.EmailID = DetailsObj.EmailID;
        //                    detail.PinCode = DetailsObj.PinCode;
        //                    detail.LegalRelationShipID = DetailsObj.LegalRelationShipID;

        //                    detail.Status = DetailsObj.Status;
        //                    detail.LegalEntityTypeID = DetailsObj.LegalEntityTypeID;
        //                    detail.ComType = DetailsObj.ComType;
        //                    detail.AuditPR = DetailsObj.AuditPR;
        //                    entities.Entry(detail).State = System.Data.Entity.EntityState.Modified;
        //                    entities.SaveChanges();

        //                    entities.CustomerBranchIndustryMappings.RemoveRange(entities.CustomerBranchIndustryMappings.Where(x => x.CustomerBranchID == DetailsObj.ID));
        //                    entities.SaveChanges();

        //                    var det = new CustomerBranchIndustryMapping();
        //                    det.CustomerBranchID = DetailsObj.ID;
        //                    det.IsActive = true;
        //                    det.EditedBy = Convert.ToInt32(DetailsObj.CreatedBy);
        //                    det.EditedDate = DateTime.Now;
        //                    var StrArray = DetailsObj.Territory.Split(',');

        //                    StrArray = StrArray.Distinct().ToArray();
        //                    foreach (var item in StrArray)
        //                    {
        //                        if (item != null && item != "")
        //                        {
        //                            det.IndustryID = Convert.ToInt32(item);
        //                            entities.CustomerBranchIndustryMappings.Add(det);
        //                            entities.SaveChanges();
        //                        }

        //                    }
        //                    result = "Updated";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        return "error";
        //    }
        //    return result;
        //}

        //public static void DeleteEntityDetails(CustomerBranch DetailsObj)
        //{
        //    try
        //    {
        //        using (VendorAuditEntities entities = new VendorAuditEntities())
        //        {
        //            int cb = Convert.ToInt32(DetailsObj.CreatedBy);
        //            entities.sp_Vendor_DeleteEntityDetails(DetailsObj.CustomerID, DetailsObj.ID, cb, DetailsObj.IsDeleted);
        //        }
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //}

        public static List<SP_Audit_EntityBreadScrum_Result> GetVendorCustomerBreadScrum(int CustomerId, int ParentId)
        {
            var Lst = new List<SP_Audit_EntityBreadScrum_Result>();
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                Lst = entities.SP_Audit_EntityBreadScrum(CustomerId, ParentId).ToList();
            }
            return Lst;
        }


        internal static List<User> GetDirectors(int CustID)
        {
            List<User> Directors = new List<User>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Directors = (from row in entities.Users
                             where row.CustomerID == CustID && row.VendorAuditRoleID == 39 && row.IsActive == true && row.IsDeleted == false
                             //where row.CustomerID == CustID && row.VendorAuditRoleID == 46 && row.IsActive == true && row.IsDeleted == false
                             select row).ToList();
            }
            return Directors;
        }

        internal static List<User> GetHeads(int CustID)
        {
            List<User> Heads = new List<User>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Heads = (from row in entities.Users
                             where row.CustomerID == CustID && row.VendorAuditRoleID == 40 && row.IsActive == true && row.IsDeleted == false
                         //where row.CustomerID == CustID && row.VendorAuditRoleID == 47 && row.IsActive == true && row.IsDeleted == false
                         select row).ToList();
            }
            return Heads;
        }
    }
}