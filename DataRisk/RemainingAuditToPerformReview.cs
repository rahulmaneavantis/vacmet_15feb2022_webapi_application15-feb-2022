//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class RemainingAuditToPerformReview
    {
        public int CustomerID { get; set; }
        public long CustomerBranchID { get; set; }
        public string Branch { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public string VerticalName { get; set; }
        public string FinancialYear { get; set; }
        public string ForMonth { get; set; }
        public Nullable<System.DateTime> ExpectedStartDate { get; set; }
        public Nullable<System.DateTime> PlannedStartDate { get; set; }
        public Nullable<System.DateTime> RemindDate { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int RoleID { get; set; }
        public string Role { get; set; }
        public Nullable<long> AuditID { get; set; }
    }
}
