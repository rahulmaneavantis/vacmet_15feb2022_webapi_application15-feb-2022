//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditTriggerLog
    {
        public long ID { get; set; }
        public string Product { get; set; }
        public string TriggerType { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<System.DateTime> TriggerOn { get; set; }
    }
}
