//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditKickOffImplementationFillView
    {
        public long CustomerBranchId { get; set; }
        public string Branch { get; set; }
        public string VerticalName { get; set; }
        public int VerticalId { get; set; }
        public string FinancialYear { get; set; }
        public string ForPeriod { get; set; }
        public long ProcessId { get; set; }
        public string ProcessName { get; set; }
        public Nullable<long> ExternalAuditorId { get; set; }
        public string AssignedTo { get; set; }
        public string AssignTO { get; set; }
        public int CustomerID { get; set; }
        public Nullable<long> AuditID { get; set; }
    }
}
