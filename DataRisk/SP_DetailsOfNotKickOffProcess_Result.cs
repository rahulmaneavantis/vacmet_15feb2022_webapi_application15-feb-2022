//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    
    public partial class SP_DetailsOfNotKickOffProcess_Result
    {
        public int Id { get; set; }
        public long Process { get; set; }
        public int CustomerBranchId { get; set; }
        public string ISAHQMP { get; set; }
        public string FinancialYear { get; set; }
        public string TermName { get; set; }
        public bool TermStatus { get; set; }
        public Nullable<int> PhaseCount { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<int> VerticalID { get; set; }
    }
}
