//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class FileData_Risk
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public bool IsDeleted { get; set; }
        public long ProcessID { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public long RiskCreationId { get; set; }
        public string TestUploadName { get; set; }
        public string TestFilePath { get; set; }
        public string TestFileKey { get; set; }
        public string TestVersion { get; set; }
        public Nullable<System.DateTime> TestVersionDate { get; set; }
        public string TestVersionComment { get; set; }
        public long CustomerBranchId { get; set; }
        public string FinancialYear { get; set; }
        public long AuditInstanceId { get; set; }
        public string EnType { get; set; }
        public Nullable<long> FileSize { get; set; }
    }
}
