//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalAuditStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InternalAuditStatu()
        {
            this.InternalAuditStatusTransitions = new HashSet<InternalAuditStatusTransition>();
            this.InternalAuditStatusTransitions1 = new HashSet<InternalAuditStatusTransition>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InternalAuditStatusTransition> InternalAuditStatusTransitions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InternalAuditStatusTransition> InternalAuditStatusTransitions1 { get; set; }
    }
}
