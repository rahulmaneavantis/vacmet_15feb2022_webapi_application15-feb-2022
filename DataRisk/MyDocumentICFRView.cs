//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class MyDocumentICFRView
    {
        public long UserID { get; set; }
        public int RoleID { get; set; }
        public long CustomerBranchId { get; set; }
        public string FinancialYear { get; set; }
        public string ForMonth { get; set; }
        public long ProcessId { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public long RiskCreationId { get; set; }
        public string ControlNo { get; set; }
        public string ActivityDescription { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
    }
}
