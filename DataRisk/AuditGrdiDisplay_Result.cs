//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    
    public partial class AuditGrdiDisplay_Result
    {
        public long ID { get; set; }
        public long RiskCategoryCreationId { get; set; }
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public string SampleDownloadFileName { get; set; }
        public long ProcessID { get; set; }
        public Nullable<long> SubProcessID { get; set; }
        public Nullable<long> CustomerBranchId { get; set; }
    }
}
