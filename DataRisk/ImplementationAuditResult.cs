//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class ImplementationAuditResult
    {
        public int ID { get; set; }
        public long AuditScheduleOnID { get; set; }
        public string FinancialYear { get; set; }
        public string ForPeriod { get; set; }
        public long CustomerBranchId { get; set; }
        public bool IsDeleted { get; set; }
        public string ProcessWalkthrough { get; set; }
        public string ActualWorkDone { get; set; }
        public string Population { get; set; }
        public string Sample { get; set; }
        public Nullable<long> PersonResponsible { get; set; }
        public long UserID { get; set; }
        public long RoleID { get; set; }
        public Nullable<long> ResultID { get; set; }
        public long ImplementationInstance { get; set; }
        public string ManagementResponse { get; set; }
        public Nullable<System.DateTime> TimeLine { get; set; }
        public string FixRemark { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> ImplementationStatus { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public Nullable<long> ProcessId { get; set; }
        public bool ISACTIVE { get; set; }
        public Nullable<long> AuditID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string AuditorRemark { get; set; }
    }
}
