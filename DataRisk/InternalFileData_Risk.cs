//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalFileData_Risk
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public bool IsDeleted { get; set; }
        public long ProcessID { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public string AdditionalUploadName { get; set; }
        public string AdditionalFilePath { get; set; }
        public string AdditionalFileKey { get; set; }
        public string AdditionalVersion { get; set; }
        public Nullable<System.DateTime> AdditionalVersionDate { get; set; }
        public string AdditionalVersionComment { get; set; }
        public long CustomerBranchId { get; set; }
        public string FinancialYear { get; set; }
        public long ATBDId { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<long> AuditID { get; set; }
        public string EnType { get; set; }
        public Nullable<long> FileSize { get; set; }
        public string TypeOfFile { get; set; }
        public string UploadedDocumentFlag { get; set; }
    }
}
