//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalAuditInstance
    {
        public long ID { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int CustomerBranchID { get; set; }
        public long ProcessId { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public Nullable<long> AuditID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
