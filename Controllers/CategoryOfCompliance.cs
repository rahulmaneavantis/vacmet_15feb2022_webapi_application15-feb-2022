﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Controllers
{
    public class CategoryOfCompliance
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class SequenceOfCompliance
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}