﻿using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using AppWebApplication.Models;
using AvantisManagementMailService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using static AppWebApplication.Models.RLCSManagement;

namespace AppWebApplication.Controllers
{
    public class RLCSController : ApiController
    {
        public static string key = "avantis";
        public static string getDecryptedText(string text, string type)
        {
            string DecryptedString = string.Empty;
            text = text.Replace(" ", "+");
            var encryptedBytes = Convert.FromBase64String(text);
            if (type.Equals("Android"))
            {
                try
                {
                    DecryptedString = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
            }
            else if (type.Equals("IOS"))
            {
                try
                {
                    DecryptedString = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
            }
            return DecryptedString;
        }

        [Route("RLCS/RLCSGetUserLogin")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSGetUserLogin([FromBody]GetUserDetail obj)
        {
            try
            {
                string LoginFromUser = string.Empty;
                string authkey = string.Empty;
                string profileIDEncrypted = string.Empty;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string Password = getDecryptedText(obj.Password.ToString(), CallerAgent);

                if (CallerAgent.Equals("Android"))
                {
                    LoginFromUser = "A";
                }
                else if (CallerAgent.Equals("IOS"))
                {
                    LoginFromUser = "I";
                }

                string baseAddress = ConfigurationManager.AppSettings["FetchUrlDetail"].ToString();
                List<RLCSLoginDetails> people = new List<RLCSLoginDetails>();
                string DeviceIDIMEI = obj.DeviceIDIMEI.ToString();
                string setAccessToken = string.Empty;

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(LoginFromUser))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        RLCS_User_Mapping user = null;
                        if (RLCSUserManagement.IsValidUserID(UserName.Trim(), Password.Trim(), out user))
                        //if (RLCSUserManagement.IsValidUserID(UserName.Trim(), Util.CalculateMD5Hash(Password.Trim()), out user))
                        {
                            if (user != null)
                            {
                                if (user.IsActive)
                                {
                                    int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                    using (var client = new HttpClient())
                                    {
                                        var form = new Dictionary<string, string>
                                            {
                                                {"grant_type", "password"},
                                                {"username", user.Email},
                                                {"password", user.Password},
                                            };
                                        var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                                        var token = tokenResponse.Content.ReadAsAsync<Models.Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                                        if (string.IsNullOrEmpty(token.Error))
                                        {
                                            setAccessToken = "bearer " + token.AccessToken;
                                        }
                                        else
                                        {
                                            setAccessToken = "bearer " + token.Error;
                                        }
                                    }
                                    string Notificationstatus = string.Empty;
                                    string UserEmail = string.Empty;

                                    var chknotification = (from row in entities.MaintainDeviceIDs
                                                           where row.DeviceIDIMEI == DeviceIDIMEI
                                                           && row.Status == "Active"
                                                           && row.UserID != user.AVACOM_UserID
                                                           select row).FirstOrDefault();
                                    if (chknotification != null)
                                    {
                                        string checkexsitemail = (from row in entities.Users
                                                                  where row.ID == chknotification.UserID
                                                                  select row.Email).FirstOrDefault();
                                        Notificationstatus = "True";
                                        UserEmail = checkexsitemail;
                                    }
                                    else
                                    {
                                        Notificationstatus = "False";
                                        UserEmail = "No";
                                    }
                                    MaintainLoginDetail objData = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.AVACOM_UserID),
                                        Email = user.Email,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = DeviceIDIMEI,//ipaddress,
                                        MACAddress = DeviceIDIMEI,//Macaddress                                        
                                        LoginFrom = LoginFromUser,
                                        ProfileID=user.ProfileID
                                    };
                                    RLCSUserManagement.Create(objData);
                                    string UserContact = "No";
                                    if (user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                    {
                                        UserContact = "Yes";
                                    }

                                    #region Authkey
                                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                                    string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                                    if (!string.IsNullOrEmpty(user.ProfileID))
                                    {
                                        profileIDEncrypted = CryptographyHandler.encrypt(user.ProfileID.Trim(), TLConnectKey);
                                    }

                                    if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(user.ProfileID))
                                    {
                                        try
                                        {
                                            authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, user.ProfileID, TLConnectKey);
                                        }
                                        catch (Exception ex)
                                        {
                                            //userProfileID = string.Empty;
                                            authkey = string.Empty;
                                        }
                                    }
                                    #endregion

                                    people.Add(new RLCSLoginDetails { status = "True", msg = "Successfully Login.", tokendetail = setAccessToken, CurrentEmailName = user.FirstName + " " + user.LastName, Notificationstatus = Notificationstatus, UserEmail = UserEmail, UserContact = UserContact, UserRole = user.AVACOM_UserRole, ProfileID = user.ProfileID, authKey = authkey, ProfileID_Encrypted = profileIDEncrypted });

                                    MaintainLoginDetail objData1 = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.AVACOM_UserID),
                                        //Email = UserName,
                                        Email = user.Email,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = obj.DeviceIDIMEI,
                                        MACAddress = obj.DeviceIDIMEI,
                                        LoginFrom = LoginFromUser,
                                        ProfileID=user.ProfileID
                                    };
                                    UserManagement.LoginCreateDetail(objData1);

                                    RLCSUserManagement.LastLoginUpdate(Convert.ToInt32(user.AVACOM_UserID));
                                  }
                                else
                                {
                                    people.Add(new RLCSLoginDetails { status = "False", msg = "Your Account is Disabled.", tokendetail = setAccessToken, CurrentEmailName = "", Notificationstatus = "False", UserEmail = "No", UserContact = "No", UserRole = "", ProfileID = "", authKey = authkey, ProfileID_Encrypted = profileIDEncrypted });
                                }
                            }
                            else
                            {
                                people.Add(new RLCSLoginDetails { status = "False", msg = "User record not available.", tokendetail = setAccessToken, CurrentEmailName = "", Notificationstatus = "False", UserEmail = "No", UserContact = "No", UserRole = "", ProfileID = "", authKey = authkey, ProfileID_Encrypted = profileIDEncrypted });
                            }
                        }
                        else
                        {
                            people.Add(new RLCSLoginDetails { status = "False", msg = "Please enter valid username or password.", tokendetail = setAccessToken, CurrentEmailName = "", Notificationstatus = "False", UserEmail = "No", UserContact = "No", UserRole = "", ProfileID = "", authKey = authkey, ProfileID_Encrypted = profileIDEncrypted });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(people).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("RLCS/MaintainDeviceID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MaintainDeviceID([FromBody]RLCSDeviceDetails devicedetailObj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                string DeviceToken = string.Empty;
                if (!string.IsNullOrEmpty(devicedetailObj.DeviceToken))
                {
                    DeviceToken = devicedetailObj.DeviceToken.Trim();

                    var CallerAgent = HttpContext.Current.Request.UserAgent;
                    string UserName = getDecryptedText(devicedetailObj.UserName.ToString(), CallerAgent);
                    string ISproduct = "H";
                   
                    if (!string.IsNullOrEmpty(UserName))
                    {
                        RLCS_User_Mapping user = null;
                        if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                        {
                            if (user != null)
                            {

                                int userID = Convert.ToInt32(user.AVACOM_UserID);
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    if (devicedetailObj.flag.Equals("No"))
                                    {
                                        var checkactive = (from row in entities.MaintainDeviceIDs
                                                           where row.Product == ISproduct
                                                           && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                           && row.UserID == userID
                                                           && row.Status == "Active"
                                                           select row).FirstOrDefault();

                                        if (checkactive == null)
                                        {
                                            MaintainDeviceID obj = new MaintainDeviceID();
                                            obj.UserID = userID;
                                            obj.DeviceIDIMEI = devicedetailObj.DeviceIDIMEI;
                                            obj.DeviceToken = DeviceToken;
                                            //obj.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                            obj.Status = "Active";
                                            obj.Product = ISproduct;
                                            entities.MaintainDeviceIDs.Add(obj);
                                            entities.SaveChanges();
                                        }
                                        else
                                        {
                                            var updatefortoken = (from row in entities.MaintainDeviceIDs
                                                                  where row.Product == ISproduct
                                                                  && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                                  && row.UserID == userID
                                                                  && row.Status == "Active"
                                                                  select row).FirstOrDefault();
                                            if (updatefortoken != null)
                                            {
                                                updatefortoken.DeviceToken = DeviceToken;
                                            }
                                            entities.SaveChanges();
                                        }
                                    }
                                    else if (devicedetailObj.flag.Equals("Yes"))
                                    {
                                        var checkactive = (from row in entities.MaintainDeviceIDs
                                                           where row.Product == ISproduct
                                                                && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                                && row.Status == "Active"
                                                           select row).ToList();
                                        if (checkactive.Count > 0)
                                        {
                                            foreach (var item in checkactive)
                                            {
                                                int UserIDforInactive = item.UserID;
                                                if (UserIDforInactive != null)
                                                {
                                                    var updateforstatus = (from row in entities.MaintainDeviceIDs
                                                                           where row.Product == ISproduct
                                                                           && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                                           && row.UserID == UserIDforInactive
                                                                           && row.Status == "Active"
                                                                           select row).FirstOrDefault();
                                                    if (updateforstatus != null)
                                                    {
                                                        updateforstatus.Status = "InActive";
                                                    }
                                                    entities.SaveChanges();
                                                }
                                            }
                                        }
                                        MaintainDeviceID obj = new MaintainDeviceID();
                                        obj.UserID = userID;
                                        obj.DeviceIDIMEI = devicedetailObj.DeviceIDIMEI;
                                        obj.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                        obj.Status = "Active";
                                        obj.Product = ISproduct;
                                        entities.MaintainDeviceIDs.Add(obj);
                                        entities.SaveChanges();
                                    }
                                    else if (devicedetailObj.flag.Equals("NotificationNo"))
                                    {
                                        var checkactive = (from row in entities.MaintainDeviceIDs
                                                           where row.Product == ISproduct
                                                           && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                           && row.Status == "Active"
                                                           select row).FirstOrDefault();
                                        if (checkactive != null)
                                        {
                                            checkactive.DeviceToken = DeviceToken;
                                        }
                                        entities.SaveChanges();
                                    }
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }
                    }
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Token should not be empty." });
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSDashboradCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSDashboradCount([FromBody]DashboradCountInput obj)
        {
            try
            {                
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                int UpcomingCount = 0;
                int duetodayCount = 0;
                int OverdueCount = 0;
                int Compliance = 0;

                List<RLCSDashboradCount> count = new List<RLCSDashboradCount>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {                  
                        RLCS_User_Mapping user = null;
                        if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                        {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            ProfileID = user.ProfileID;

                            List<int> assignedbranchIDs = new List<int>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["RLCSCacheClearTime"]);

                                string CacheName = "CacheGetAssignedLocationBranch_" + customerID + "_" + UserID + "_" + ProfileID;
                                var userAssignedBranchList = (List<SP_RLCS_GetAssignedLocationBranches_Result>)HttpContext.Current.Cache[CacheName];
                                if (userAssignedBranchList == null)
                                {
                                    entities.Database.CommandTimeout = 180;
                                    userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, UserID, ProfileID)).ToList();
                                    HttpContext.Current.Cache.Insert(CacheName, userAssignedBranchList, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                }
                                //entities.Database.CommandTimeout = 180;
                                //var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, UserID, ProfileID)).ToList();


                                if (userAssignedBranchList != null)
                                {
                                    if (userAssignedBranchList.Count > 0)
                                    {
                                        assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                    }

                                    string CacheName1 = "CacheComplianceInstanceTransactionCount_" + customerID + "_" + UserID + "_" + ProfileID;
                                    var MastertransactionsQuery = (List<SP_RLCS_ComplianceInstanceTransactionCount_Result>)HttpContext.Current.Cache[CacheName1];
                                    if (MastertransactionsQuery == null)
                                    {
                                        entities.Database.CommandTimeout = 180;

                                        MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(customerID), UserID, ProfileID)
                                          .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                                        HttpContext.Current.Cache.Insert(CacheName1, MastertransactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                    }

                                    //entities.Database.CommandTimeout = 180;
                                    //var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(customerID), UserID, ProfileID)
                                    //  .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                                    if (MastertransactionsQuery.Count > 0)
                                    {
                                        if (assignedbranchIDs.Count > 0)
                                            MastertransactionsQuery = MastertransactionsQuery.Where(row => assignedbranchIDs.Contains(row.CustomerBranchID)).ToList();

                                        //Upcoming                                     
                                        UpcomingCount = Convert.ToInt32(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Upcoming").Count());

                                        //DueToday                                     
                                        duetodayCount = Convert.ToInt32(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "DueToday").Count());

                                        //Overdue                                     
                                        OverdueCount = Convert.ToInt32(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Overdue").Count());

                                    }
                                }
                                List<SP_RLCS_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_RLCS_GetManagementCompliancesSummary_Result>();

                                string CacheName2 = "CacheMGMTComplianceSummary_" + customerID + "_" + UserID + "_" + ProfileID;
                                MasterManagementCompliancesSummaryQuery = (List<SP_RLCS_GetManagementCompliancesSummary_Result>)HttpContext.Current.Cache[CacheName2];
                                if (MasterManagementCompliancesSummaryQuery == null)
                                {
                                    entities.Database.CommandTimeout = 180;

                                    MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(UserID, customerID, ProfileID)).ToList();

                                    HttpContext.Current.Cache.Insert(CacheName2, MasterManagementCompliancesSummaryQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                }



                                //MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(UserID, customerID, ProfileID)).ToList();

                                //Compliance Count
                                if (MasterManagementCompliancesSummaryQuery != null)
                                {
                                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                                    {
                                        var complianceCount = MasterManagementCompliancesSummaryQuery.Select(entry => entry.ComplianceID).Distinct().Count();
                                        Compliance = Convert.ToInt32(complianceCount);
                                    }
                                }

                                count.Add(new RLCSDashboradCount { Compliance = Compliance, DueToday = duetodayCount, Upcoming = UpcomingCount, Overdue = OverdueCount });
                            }
                        }
                        else
                        {
                            count.Add(new RLCSDashboradCount { Compliance = Compliance, DueToday = duetodayCount, Upcoming = UpcomingCount, Overdue = OverdueCount });
                        }
                        }
                        else
                        {
                            count.Add(new RLCSDashboradCount { Compliance = Compliance, DueToday = duetodayCount, Upcoming = UpcomingCount, Overdue = OverdueCount });
                        }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(count).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/GetComplianceDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetComplianceDetails([FromBody]ComplianceDetailInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            //ProfileID = user.ProfileID;
                            //int ActId = -1;
                            int CategoryID = -1;
                            bool IsApprover = false;
                            List<long> Branchlist = new List<long>();
                            if (obj.locationId != -1)
                            {
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                            }

                            ComplianceDetails = RLCSManagement.GetComplianceDetails(customerID, Branchlist, "RLCS", Convert.ToInt32(obj.locationId), obj.ActId, CategoryID, UserID, ProfileID, IsApprover).ToList();

                            if (ComplianceDetails.Count > 0)
                            {

                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = ComplianceDetails.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.page - 1);
                                //var canPage = skip < total;
                                ComplianceDetails = ComplianceDetails.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ComplianceDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/GetDuetodayUpcomingOverdueDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetDuetodayUpcomingOverdueDetail([FromBody]DetailInputduetodayUpcomingOverdue obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                List<SP_RLCS_ComplianceInstanceTransactionCount_Result> lstRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            ProfileID = user.ProfileID;
                            List<long> Branchlist = new List<long>();
                            if (obj.locationId != -1)
                            {
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                            }

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                entities.Database.CommandTimeout = 180;
                                var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, UserID, ProfileID)
                                  .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                                lstRecords = RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, obj.filterType);
                                if (lstRecords.Count > 0)
                                {                                    
                                    if (obj.ActId != -1)
                                    {
                                        lstRecords = lstRecords.Where(x => x.ActID == obj.ActId).ToList();
                                    }
                                    if (Branchlist.Count > 0)
                                    {
                                        lstRecords = lstRecords.Where(x => Branchlist.Contains(x.CustomerBranchID)).ToList();
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(obj.locationId) > 0)
                                        {
                                            lstRecords = lstRecords.Where(x => x.CustomerBranchID.Equals(Convert.ToInt32(obj.locationId))).ToList();
                                        }
                                    }
                                }
                                if (lstRecords.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstRecords.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (obj.page - 1);
                                    //var canPage = skip < total;
                                    lstRecords = lstRecords.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstRecords).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/RLCSAssignedLocation")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSAssignedLocation([FromBody]DashboradCountInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                List<SP_RLCS_GetAssignedLocationBranches_Result> location = new List<SP_RLCS_GetAssignedLocationBranches_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            ProfileID = user.ProfileID;
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                entities.Database.CommandTimeout = 180;
                                location = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, UserID, ProfileID)).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(location).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
            
       // [Authorize]
        [Route("RLCS/RLCSAssignedAct")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSAssignedAct([FromBody]DashboradCountInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                
                List<ActDetails> act = new List<ActDetails>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            ProfileID = user.ProfileID;
                            List<long> Branchlist = new List<long>();
                            if (obj.locationId != -1)
                            {
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                            }
                            act = RLCSManagement.GetAllAssignedActs_RLCS(customerID, UserID, -1, ProfileID, Branchlist);
                        }                       
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(act).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSAssignedCompliancebyActID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSAssignedCompliancebyActID([FromBody]AssignmentComplianceByActId obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                List<SP_RLCS_ComplianceInstanceAssignmentViewDetailsByActId_Result> AssignmentCompliance = new List<SP_RLCS_ComplianceInstanceAssignmentViewDetailsByActId_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);                          
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                AssignmentCompliance = entities.SP_RLCS_ComplianceInstanceAssignmentViewDetailsByActId(Convert.ToInt32(user.AVACOM_UserID), Convert.ToInt32(obj.ActId), customerID, "", ProfileID).ToList();
                            }
                            if (AssignmentCompliance.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = AssignmentCompliance.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.Page - 1);
                                //var canPage = skip < total;
                                AssignmentCompliance = AssignmentCompliance.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(AssignmentCompliance).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/RLCSDashboardSummary")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSDashboardSummary([FromBody]DashboradComplianceSummaryCount obj)
        {
            try
            {
                List<GetCompliancesSummary> objGetCompliancesSummary = new List<GetCompliancesSummary>();

                long totalPieCompletedcount = 0;
                long totalPieNotCompletedCount = 0;
                long totalPieAfterDueDatecount = 0;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                #region Get Count Detail
                                int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                int UserID = Convert.ToInt32(user.AVACOM_UserID);

                                DateTime? FromDate = null;
                                DateTime? EndDateF = null;
                                List<long> Branchlist = new List<long>();
                                if (obj.locationId != -1)
                                {
                                    Branchlist.Clear();
                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.locationId)).ToList();
                                }
                                // int CustomerbranchID = -1;

                                int year = Convert.ToInt32(obj.FYyear);
                                DateTime FIFromDate = new DateTime(year, 1, 1);
                                DateTime FIEndDate = new DateTime(year, 12, 31);

                                string perFunctionChart = string.Empty;
                                string perFunctionPieChart = string.Empty;
                                string perRiskChart = string.Empty;

                                string tempperFunctionChart = perFunctionChart;
                                string tempperFunctionPieChart = perFunctionPieChart;
                                string tempperRiskChart = perRiskChart;

                                List<SP_RLCS_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_RLCS_GetManagementCompliancesSummary_Result>();


                                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["RLCSCacheClearTime"]);
                                string CacheName = "CacheMGMTComplianceSummary_" + customerid + "_" + UserID + "_" + ProfileID;
                                MasterManagementCompliancesSummaryQuery = (List<SP_RLCS_GetManagementCompliancesSummary_Result>)HttpContext.Current.Cache[CacheName];
                                if (MasterManagementCompliancesSummaryQuery == null)
                                {
                                    entities.Database.CommandTimeout = 180;
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(UserID, customerid, ProfileID)).ToList();

                                    HttpContext.Current.Cache.Insert(CacheName, MasterManagementCompliancesSummaryQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                }


                                
                                
                                //entities.Database.CommandTimeout = 180;
                                //MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(UserID, customerid, ProfileID)).ToList();

                                long totalPieCompletedHIGH = 0;
                                long totalPieCompletedMEDIUM = 0;

                                long totalPieAfterDueDateHIGH = 0;
                                long totalPieAfterDueDateMEDIUM = 0;

                                long totalPieNotCompletedHIGH = 0;
                                long totalPieNotCompletedMEDIUM = 0;

                                long totalPieAfterDueDateLOW = 0;
                                long totalPieCompletedLOW = 0;
                                long totalPieNotCompletedLOW = 0;

                                if (MasterManagementCompliancesSummaryQuery != null)
                                {
                                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                                    {
                                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                        {
                                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                                        }

                                        if (Branchlist.Count > 0)
                                        {
                                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                        }

                                        List<SP_RLCS_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_RLCS_GetManagementCompliancesSummary_Result>();
                                        DateTime EndDate = DateTime.Today.Date;
                                        DateTime? PassEndValue;

                                        if (FromDate != null && EndDateF != null)
                                        {
                                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                 where row.CustomerID == customerid
                                                                 && row.ScheduledOn >= FromDate
                                                                 && row.ScheduledOn <= EndDateF
                                                                 select row).ToList();

                                            PassEndValue = EndDateF;
                                        }
                                        else if (FromDate != null)
                                        {
                                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                 where row.CustomerID == customerid
                                                                 && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                                                 select row).ToList();

                                            PassEndValue = EndDate;
                                        }
                                        else if (EndDateF != null)
                                        {
                                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                 where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                                                 select row).ToList();


                                            PassEndValue = EndDateF;
                                        }
                                        else
                                        {

                                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                            {
                                                //For Financial Year
                                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                     where row.CustomerID == customerid
                                                                     && row.ScheduledOn >= FIFromDate
                                                                     && row.ScheduledOn <= FIEndDate
                                                                     select row).ToList();

                                            }
                                            else
                                            {
                                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                     where row.CustomerID == customerid
                                                                     && row.ScheduledOn <= FIEndDate
                                                                     select row).ToList();
                                            }


                                            PassEndValue = FIEndDate;
                                        }


                                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                        {
                                            PassEndValue = FIEndDate;
                                            FromDate = FIFromDate;
                                        }

                                        transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                                        //PieChart                         

                                        //long totalPieCompletedHIGH = 0;
                                        //long totalPieCompletedMEDIUM = 0;

                                        //long totalPieAfterDueDateHIGH = 0;
                                        //long totalPieAfterDueDateMEDIUM = 0;

                                        //long totalPieNotCompletedHIGH = 0;
                                        //long totalPieNotCompletedMEDIUM = 0;

                                        //long totalPieAfterDueDateLOW = 0;
                                        //long totalPieCompletedLOW = 0;
                                        //long totalPieNotCompletedLOW = 0;

                                        long highcount;
                                        long mediumCount;
                                        long lowcount;
                                        long totalcount;
                                        DataTable table = new DataTable();
                                        table.Columns.Add("ID", typeof(int));
                                        table.Columns.Add("Catagory", typeof(string));
                                        table.Columns.Add("High", typeof(long));
                                        table.Columns.Add("Medium", typeof(long));
                                        table.Columns.Add("Low", typeof(long));

                                        string listCategoryId = "";
                                        List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();

                                        CatagoryList = RLCSManagement.GetNewAll(UserID, obj.locationId, Branchlist);


                                        string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                                        foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                                        {
                                            listCategoryId += ',' + cc.Id.ToString();
                                            highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                            mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                            lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                            totalcount = highcount + mediumCount + lowcount;

                                            if (totalcount != 0)
                                            {
                                                //High
                                                long AfterDueDatecountHigh;
                                                long CompletedCountHigh;
                                                long NotCompletedcountHigh;

                                                AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                                CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                                NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                                //per Status pie Chart
                                                totalPieCompletedcount += CompletedCountHigh;
                                                totalPieNotCompletedCount += NotCompletedcountHigh;
                                                totalPieAfterDueDatecount += AfterDueDatecountHigh;


                                                //pie drill down
                                                totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                                                totalPieCompletedHIGH += CompletedCountHigh;
                                                totalPieNotCompletedHIGH += NotCompletedcountHigh;

                                                //Medium
                                                long AfterDueDatecountMedium;
                                                long CompletedCountMedium;
                                                long NotCompletedcountMedium;

                                                AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                                CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                                NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();


                                                //per Status pie Chart
                                                totalPieCompletedcount += CompletedCountMedium;
                                                totalPieNotCompletedCount += NotCompletedcountMedium;
                                                totalPieAfterDueDatecount += AfterDueDatecountMedium;


                                                //pie drill down 
                                                totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                                                totalPieCompletedMEDIUM += CompletedCountMedium;
                                                totalPieNotCompletedMEDIUM += NotCompletedcountMedium;

                                                //Low
                                                long AfterDueDatecountLow;
                                                long CompletedCountLow;
                                                long NotCompletedcountLow;


                                                AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                                CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                                NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                                //per Status pie Chart
                                                totalPieCompletedcount += CompletedCountLow;
                                                totalPieNotCompletedCount += NotCompletedcountLow;
                                                totalPieAfterDueDatecount += AfterDueDatecountLow;

                                                //pie drill down
                                                totalPieAfterDueDateLOW += AfterDueDatecountLow;
                                                totalPieCompletedLOW += CompletedCountLow;
                                                totalPieNotCompletedLOW += NotCompletedcountLow;

                                            }
                                        }
                                    }
                                }

                                objGetCompliancesSummary.Add(new GetCompliancesSummary
                                {
                                    AfterDueDate = Convert.ToInt32(totalPieAfterDueDatecount),
                                    InTime = Convert.ToInt32(totalPieCompletedcount),
                                    NotCompleted = Convert.ToInt32(totalPieNotCompletedCount),

                                    NotCompletedHigh = Convert.ToInt32(totalPieNotCompletedHIGH),
                                    AfterDueDateHigh = Convert.ToInt32(totalPieAfterDueDateHIGH),
                                    InTimeHigh = Convert.ToInt32(totalPieCompletedHIGH),

                                    NotCompletedMedium = Convert.ToInt32(totalPieNotCompletedMEDIUM),
                                    AfterDueDateMedium = Convert.ToInt32(totalPieAfterDueDateMEDIUM),
                                    InTimeMedium = Convert.ToInt32(totalPieCompletedMEDIUM),

                                    NotCompletedLow = Convert.ToInt32(totalPieNotCompletedLOW),
                                    AfterDueDateLow = Convert.ToInt32(totalPieAfterDueDateLOW),
                                    InTimeLow = Convert.ToInt32(totalPieCompletedLOW),
                                });
                                #endregion
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objGetCompliancesSummary).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/RLCSDashboardMonthwiseSummary")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSDashboardMonthwiseSummary([FromBody]DashboradComplianceSummaryCount obj)
        {
            try
            {
                List<GetCompliancesMonthWiseSummary> resultObj = new List<GetCompliancesMonthWiseSummary>();
               
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                #region Get all data
                                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["RLCSCacheClearTime"]);

                                string seriesData_GraphMonthlyCompliancePie = string.Empty;
                                string seriesData_GraphMonthlyComplianceColumn = string.Empty;

                                string ChartStackedMonthly_Category = string.Empty;

                                string CompliedCountList = string.Empty;
                                string NotCompliedCountList = string.Empty;
                                string PieCompliedCountList = string.Empty;
                                string PieNotCompliedCountList = string.Empty;
                                string downloadFilePath = string.Empty;
                                int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                int UserID = Convert.ToInt32(user.AVACOM_UserID);

                                List<long> Branchlist = new List<long>();
                                if (obj.locationId != -1)
                                {
                                    Branchlist.Clear();
                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.locationId)).ToList();
                                }
                                List<int> assignedbranchIDs = new List<int>();

                                string CacheName = "CacheGetAssignedLocationBranch_" + customerid + "_" + UserID + "_" + ProfileID;
                                var userAssignedBranchList = (List<SP_RLCS_GetAssignedLocationBranches_Result>)HttpContext.Current.Cache[CacheName];
                                if (userAssignedBranchList == null)
                                {
                                    entities.Database.CommandTimeout = 180;
                                    userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerid, UserID, ProfileID)).ToList();
                                    HttpContext.Current.Cache.Insert(CacheName, userAssignedBranchList, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                }
                                //entities.Database.CommandTimeout = 180;
                                //var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerid, UserID, ProfileID)).ToList();



                                if (userAssignedBranchList.Count > 0)
                                {
                                    assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                }


                                string CacheName1 = "CacheGetMonthlyComplianceSummary_" + customerid;
                                var lstRecords = (List<SP_RLCS_GetMonthlyComplianceSummary_Result>)HttpContext.Current.Cache[CacheName1];
                                if (lstRecords == null)
                                {
                                    lstRecords = (entities.SP_RLCS_GetMonthlyComplianceSummary(customerid)).ToList();
                                    HttpContext.Current.Cache.Insert(CacheName1, lstRecords, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                }

                                //var lstRecords = (entities.SP_RLCS_GetMonthlyComplianceSummary(customerid)).ToList();
                                if (lstRecords != null)
                                {
                                    if (lstRecords.Count > 0)
                                    {
                                        if (assignedbranchIDs.Count > 0)
                                        {
                                            string CacheName2 = "CacheGetAssignedEntitiesy_" + customerid + "_" + UserID + "_" + ProfileID;
                                            var lstAssignedEntitiesRecords = (List<SP_RLCS_GetAssignedEntities_Result>)HttpContext.Current.Cache[CacheName2];
                                            if (lstAssignedEntitiesRecords == null)
                                            {
                                                lstAssignedEntitiesRecords = (entities.SP_RLCS_GetAssignedEntities(customerid, UserID, ProfileID)).ToList();
                                                HttpContext.Current.Cache.Insert(CacheName2, lstAssignedEntitiesRecords, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                                            }

                                            //var lstAssignedEntitiesRecords = (entities.SP_RLCS_GetAssignedEntities(customerid, UserID, ProfileID)).ToList();

                                            if (lstAssignedEntitiesRecords.Count > 0)
                                            {
                                                var lstClientIDs = lstAssignedEntitiesRecords.Select(row => row.CM_ClientID.Trim()).ToList();

                                                if (lstClientIDs.Count > 0)
                                                {
                                                    lstRecords = lstRecords.Where(row => row.ClientID != null && lstClientIDs.Contains(row.ClientID.Trim())).ToList();

                                                    if (Branchlist.Count > 0)
                                                        lstRecords = lstRecords.Where(entry => entry.AVACOM_BranchID != null && Branchlist.Contains((long)entry.AVACOM_BranchID)).ToList();

                                                    lstRecords = lstRecords.Where(x => x.Year.Contains(Convert.ToString(obj.FYyear))).Distinct().ToList();

                                                    var lstpieRecords = lstRecords;
                                                    if (lstRecords.Count > 0)
                                                        lstRecords = lstRecords.Where(row => row.Month != null || row.Month != "").OrderBy(row => Convert.ToInt32(row.Month)).ToList();

                                                    if (lstpieRecords.Count > 0)
                                                    {
                                                        lstpieRecords = lstpieRecords.Where(row => row.Month != null || row.Month != "").OrderByDescending(row => Convert.ToInt32(row.Month)).ToList();

                                                        string monthName = string.Empty;

                                                        int tillMonth = 12;
                                                        if (obj.FYyear == DateTime.Now.Year)
                                                            tillMonth = DateTime.Now.Month;

                                                        for (int i = 1; i <= tillMonth; i++)
                                                        {
                                                            var monthWiseRecords = lstRecords.Where(row => Convert.ToInt32(row.Month) == i).ToList();

                                                            monthName = GetMonthName(i);

                                                            if (!string.IsNullOrEmpty(monthName))
                                                            {
                                                                int compliedCount = Convert.ToInt32(monthWiseRecords.Select(row => row.CompliedNo).Sum());
                                                                int notCompliedCount = Convert.ToInt32(monthWiseRecords.Select(row => row.NotCompliedNo).Sum());

                                                                int compliedCountvalue = 0;
                                                                int NotcompliedCountvalue = 0;
                                                                if (compliedCount > 0 || notCompliedCount > 0)
                                                                {
                                                                    if (compliedCount > 0)
                                                                    {
                                                                        compliedCountvalue = (int)Math.Round((double)(100 * compliedCount) / (compliedCount + notCompliedCount));
                                                                    }
                                                                    if (notCompliedCount > 0)
                                                                    {
                                                                        NotcompliedCountvalue = (int)Math.Round((double)(100 * notCompliedCount) / (compliedCount + notCompliedCount));
                                                                    }

                                                                    resultObj.Add(new GetCompliancesMonthWiseSummary
                                                                    {
                                                                        NotComplied = NotcompliedCountvalue,
                                                                        Complied = compliedCountvalue,
                                                                        Month = monthName,
                                                                        Year = Convert.ToString(obj.FYyear)
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(resultObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("RLCS/SetPinToken")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SetPinToken([FromBody]SetPinUserName obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);

                List<SetRLCSPinToken> SetPinTokenObj = new List<SetRLCSPinToken>();

                string setAccessToken = string.Empty;

                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            if (user.IsActive)
                            {
                                using (var client = new HttpClient())
                                {
                                    string baseAddress = ConfigurationManager.AppSettings["FetchUrlDetail"].ToString();

                                    var form = new Dictionary<string, string>
                                            {
                                                {"grant_type", "password"},
                                                {"username", user.Email},
                                                {"password", user.Password},
                                            };
                                    var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                                    var token = tokenResponse.Content.ReadAsAsync<Models.Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                                    if (string.IsNullOrEmpty(token.Error))
                                    {
                                        setAccessToken = "bearer " + token.AccessToken;
                                    }
                                    else
                                    {
                                        setAccessToken = "bearer " + token.Error;
                                    }

                                    SetPinTokenObj.Add(new SetRLCSPinToken { status = "True", setAccessToken = setAccessToken });
                                }
                            }
                            else
                            {
                                SetPinTokenObj.Add(new SetRLCSPinToken { status = "False", setAccessToken = "0" });
                            }
                        }
                        else
                        {
                            SetPinTokenObj.Add(new SetRLCSPinToken { status = "False", setAccessToken = "0" });
                        }
                    }
                    else
                    {
                        SetPinTokenObj.Add(new SetRLCSPinToken { status = "False", setAccessToken = "0" });
                    }
                }
                else
                {
                    SetPinTokenObj.Add(new SetRLCSPinToken { status = "False", setAccessToken = "0" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(SetPinTokenObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        public bool ChangePassword(User user, string SenderEmailAddress, string message)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        User userToUpdate = new User();

                        userToUpdate = (from entry in entities.Users
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        //userToUpdate.ChangePasswordFlag = true;
                        userToUpdate.ChangePasswordFlag = false;

                        entities.SaveChanges();

                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();
                        return false;
                    }
                }
            }
        }

        public static bool ChangePassword(mst_User user, string SenderEmailAddress, string message)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.ChangePasswordFlag = false;
                        entities.SaveChanges();
                        //EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);

                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        private string RLCSSendNotificationEmail(RLCS_User_Mapping user, string passwordText)
        {
            try
            {
                if (user != null)
                {
                    string ReplyEmailAddressName = "";
                    ReplyEmailAddressName = "Avantis";
                    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string url = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                    if (Urloutput != null)
                    {
                        url = Urloutput.URL;
                    }
                    else
                    {
                        url = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                                           .Replace("@Username", user.UserID)
                                           .Replace("@User", username)
                                           .Replace("@PortalURL", Convert.ToString(url))
                                           .Replace("@Password", passwordText)
                                           .Replace("@From", ReplyEmailAddressName)
                                           .Replace("@URL", Convert.ToString(url));
                    //string message = Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                    //                        .Replace("@Username", user.UserID)
                    //                        .Replace("@User", username)
                    //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                    //                        .Replace("@Password", passwordText)
                    //                        .Replace("@From", ReplyEmailAddressName)
                    //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    return message;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
            return null;
        }

        //[Route("RLCS/GetForgotPassword")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetForgotPassword([FromBody]SetPinUserName obj)
        //{
        //    try
        //    {
        //        var CallerAgent = HttpContext.Current.Request.UserAgent;
        //        string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);

        //        List<MessageDetails> msg = new List<MessageDetails>();

        //        if (!string.IsNullOrEmpty(UserName))
        //        {
        //            if (UserName.Trim().Contains('@'))
        //            {
        //                #region If EmailID 
        //                User user = UserManagement.GetByUsername(UserName.Trim());
        //                if (user != null)
        //                {                            
        //                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(user.ID));

        //                    string passwordText = Util.CreateRandomPassword(10);
        //                    user.Password = Util.CalculateMD5Hash(passwordText);
        //                    string message = NotificationEmailClass.SendNotificationEmail(user, passwordText);

        //                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

        //                    bool result = ChangePassword(user, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //                    bool result1 = ChangePassword(mstuser, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);

        //                    if (result && result1)
        //                    {
        //                        UserManagement.WrongAttemptCountUpdate(user.ID);
        //                        //Send Email on User Mail Id.
        //                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);

        //                        msg.Add(new MessageDetails { Messagestatus = "True", Messagedetail = "Temporary password sent to your Registered email address" });
        //                    }
        //                    else
        //                    {
        //                        msg.Add(new MessageDetails { Messagestatus = "False", Messagedetail = "Something went wrong. Please try again." });
        //                    }
        //                }
        //                else
        //                {
        //                    msg.Add(new MessageDetails { Messagestatus = "False", Messagedetail = "Enter valid email/ email is not registered with us." });
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                #region If NO EmailID 
        //                RLCS_User_Mapping user = UserManagement.GetRLCSByID(UserName.Trim());
        //                if (user != null)
        //                {
        //                    string passwordText = Util.CreateRandomPassword(10);
        //                    user.Password = Util.CalculateMD5Hash(passwordText);

        //                    string message = RLCSSendNotificationEmail(user, passwordText);
        //                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
        //                    bool result = UserManagement.RLCSChangePassword(user, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //                    if (result)
        //                    {
        //                        //Send Email on User Mail Id.
        //                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);
        //                        msg.Add(new MessageDetails { Messagestatus = "True", Messagedetail = "Temporary password sent to your Registered email address" });
        //                    }
        //                    else
        //                    {
        //                        msg.Add(new MessageDetails { Messagestatus = "False", Messagedetail = "Something went wrong. Please try again." });
        //                    }
        //                }
        //                else
        //                {
        //                    msg.Add(new MessageDetails { Messagestatus = "False", Messagedetail = "Enter valid User Name." });
        //                }
        //                #endregion
        //            }
        //        }
        //        else
        //        {
        //            msg.Add(new MessageDetails { Messagestatus = "False", Messagedetail = "User Name does not exist." });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(msg).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        //[Authorize]
        //[Route("RLCS/RLCSMyDocumentHistorical")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage RLCSMyDocumentHistorical([FromBody]MydocumentHistorical obj)
        //{
        //    try
        //    {
        //        List<SP_RLCS_GetBranchList_ReturnChallan_Historical_Result> lstBranchList_ReturnChallans = new List<SP_RLCS_GetBranchList_ReturnChallan_Historical_Result>();

        //        var CallerAgent = HttpContext.Current.Request.UserAgent;
        //        string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
        //        string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

        //        if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
        //        {                    
        //            RLCS_User_Mapping user = null;
        //            if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
        //            {
        //                if (user != null)
        //                {
        //                    int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
        //                    int UserID = Convert.ToInt32(user.AVACOM_UserID);

        //                    using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
        //                    {
        //                        lstBranchList_ReturnChallans = dbcontext.SP_RLCS_GetBranchList_ReturnChallan_Historical(customerid, UserID, obj.scopeID, ProfileID).ToList();
        //                    }

        //                    if (lstBranchList_ReturnChallans.Count > 0)
        //                    {
        //                        if (obj.LocationID != -1)
        //                        {
        //                            List<long> Branchlist = new List<long>();
        //                            Branchlist.Clear();
        //                            Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
        //                            if (Branchlist.Count > 0)
        //                            {
        //                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
        //                            }
        //                        }

        //                        if (!string.IsNullOrEmpty(obj.MonthId))
        //                        {
        //                            lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
        //                        }
        //                        if (!string.IsNullOrEmpty(obj.YearID))
        //                        {
        //                            lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
        //                        }
        //                    }
        //                    if (lstBranchList_ReturnChallans.Count > 0)
        //                    {
        //                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.OrderBy(x => x.AVACOM_BranchName).ToList();

        //                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

        //                        var total = lstBranchList_ReturnChallans.Count;
        //                        var pageSize = PageSizeDyanamic;
        //                        var skip = pageSize * (obj.page - 1);
        //                        //var canPage = skip < total;
        //                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Skip(skip).Take(pageSize).ToList();
        //                    }
        //                }
        //            }                    
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(lstBranchList_ReturnChallans).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}
        [Authorize]
        [Route("RLCS/RLCSMyDocumentHistorical")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyDocumentHistorical([FromBody]MydocumentHistorical obj)
        {
            try
            {
                List<SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew_Result> lstBranchList_ReturnChallans = new List<SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);

                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                            {
                                lstBranchList_ReturnChallans = dbcontext.SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew(customerid, UserID, obj.scopeID, ProfileID).ToList();
                            }

                            if (lstBranchList_ReturnChallans.Count > 0)
                            {
                                if (obj.LocationID != -1)
                                {
                                    List<long> Branchlist = new List<long>();
                                    Branchlist.Clear();
                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                    if (Branchlist.Count > 0)
                                    {
                                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                    }
                                }

                                if (!string.IsNullOrEmpty(obj.MonthId))
                                {
                                    lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                }
                                if (!string.IsNullOrEmpty(obj.YearID))
                                {
                                    lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                }
                            }
                            if (lstBranchList_ReturnChallans.Count > 0)
                            {
                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.OrderBy(x => x.AVACOM_BranchName).ToList();

                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = lstBranchList_ReturnChallans.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.page - 1);
                                //var canPage = skip < total;
                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstBranchList_ReturnChallans).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("RLCS/RLCSMyDocumentRegister")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyDocumentRegister([FromBody]MydocumentHistorical obj)
        {
            try
            {
                List<SP_RLCS_GetBranchList_Register_DocumentsNew_Result> lstBranchList_Registers = new List<SP_RLCS_GetBranchList_Register_DocumentsNew_Result>();
                              
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {                   
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                lstBranchList_Registers = entities.SP_RLCS_GetBranchList_Register_DocumentsNew(customerid, UserID, ProfileID).ToList();

                                if (lstBranchList_Registers.Count > 0)
                                {
                                    if (obj.LocationID != -1)
                                    {
                                        List<long> Branchlist = new List<long>();
                                        Branchlist.Clear();
                                        Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                        if (Branchlist.Count > 0)
                                        {
                                            lstBranchList_Registers = lstBranchList_Registers.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(obj.MonthId))
                                    {
                                        lstBranchList_Registers = lstBranchList_Registers.Where(x => x.LM_PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                    }
                                    if (!string.IsNullOrEmpty(obj.YearID))
                                    {
                                        lstBranchList_Registers = lstBranchList_Registers.Where(x => x.LM_PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                    }
                                }
                                if (lstBranchList_Registers.Count > 0)
                                {
                                    lstBranchList_Registers = lstBranchList_Registers.OrderBy(x => x.AVACOM_BranchName).ToList();
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstBranchList_Registers.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (obj.page - 1);
                                    //var canPage = skip < total;
                                    lstBranchList_Registers = lstBranchList_Registers.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                    }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstBranchList_Registers).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyDocumentReturns")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyDocumentReturns([FromBody]MydocumentHistorical obj)
        {
            try
            {
                List<SP_RLCS_GetBranchList_Returns_Documents_Result> lstBranchList_Returns = new List<SP_RLCS_GetBranchList_Returns_Documents_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {                   
                        RLCS_User_Mapping user = null;
                        if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                        {
                            if (user != null)
                            {
                                int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                int UserID = Convert.ToInt32(user.AVACOM_UserID);

                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    lstBranchList_Returns = entities.SP_RLCS_GetBranchList_Returns_Documents(UserID, customerid, ProfileID).ToList();

                                    if (lstBranchList_Returns.Count > 0)
                                    {
                                        if (obj.LocationID != -1)
                                        {
                                            List<long> Branchlist = new List<long>();
                                            Branchlist.Clear();
                                            Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                            if (Branchlist.Count > 0)
                                            {
                                                lstBranchList_Returns = lstBranchList_Returns.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(obj.MonthId))
                                        {
                                            lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                        }
                                        if (!string.IsNullOrEmpty(obj.YearID))
                                        {
                                            lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                        }
                                    }

                                    if (lstBranchList_Returns.Count > 0)
                                    {
                                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                        var total = lstBranchList_Returns.Count;
                                        var pageSize = PageSizeDyanamic;
                                        var skip = pageSize * (obj.page - 1);
                                        //var canPage = skip < total;
                                        lstBranchList_Returns = lstBranchList_Returns.Skip(skip).Take(pageSize).ToList();
                                    }
                                }
                            }
                        }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstBranchList_Returns).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        //[Route("RLCS/RLCSMyDocumentReturnsNew")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage RLCSMyDocumentReturnsNew([FromBody]MydocumentHistorical obj)
        //{
        //    try
        //    {
        //        List<ReturnsNew> returnsNewOP = new List<ReturnsNew>();

        //        List<SP_RLCS_GetBranchList_ReturnChallan_Historical_Result> lstBranchList_ReturnChallans = new List<SP_RLCS_GetBranchList_ReturnChallan_Historical_Result>();
        //        List<SP_RLCS_GetBranchList_Returns_Documents_Result> lstBranchList_Returns = new List<SP_RLCS_GetBranchList_Returns_Documents_Result>();

        //        var CallerAgent = HttpContext.Current.Request.UserAgent;
        //        string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
        //        string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

        //        if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
        //        {
        //            RLCS_User_Mapping user = null;
        //            if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
        //            {
        //                if (user != null)
        //                {
        //                    int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
        //                    int UserID = Convert.ToInt32(user.AVACOM_UserID);

        //                    int ReturnsMonth = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Month"]);
        //                    int ReturnsYear = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Year"]);

        //                    if (!string.IsNullOrEmpty(obj.MonthId) && !string.IsNullOrEmpty(obj.YearID))
        //                    {
        //                        if (Convert.ToInt32(obj.MonthId) > ReturnsMonth && Convert.ToInt32(obj.YearID) >= ReturnsYear)
        //                        {
        //                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //                            {
        //                                lstBranchList_Returns = entities.SP_RLCS_GetBranchList_Returns_Documents(UserID, customerid, ProfileID).ToList();

        //                                if (lstBranchList_Returns.Count > 0)
        //                                {
        //                                    if (obj.LocationID != -1)
        //                                    {
        //                                        List<long> Branchlist = new List<long>();
        //                                        Branchlist.Clear();
        //                                        Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
        //                                        if (Branchlist.Count > 0)
        //                                        {
        //                                            lstBranchList_Returns = lstBranchList_Returns.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
        //                                        }
        //                                    }

        //                                    if (!string.IsNullOrEmpty(obj.MonthId))
        //                                    {
        //                                        lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
        //                                    }
        //                                    if (!string.IsNullOrEmpty(obj.YearID))
        //                                    {
        //                                        lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
        //                                    }
        //                                }

        //                                if (lstBranchList_Returns.Count > 0)
        //                                {
        //                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

        //                                    var total = lstBranchList_Returns.Count;
        //                                    var pageSize = PageSizeDyanamic;
        //                                    var skip = pageSize * (obj.page - 1);
        //                                    //var canPage = skip < total;
        //                                    lstBranchList_Returns = lstBranchList_Returns.Skip(skip).Take(pageSize).ToList();
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
        //                            {
        //                                lstBranchList_ReturnChallans = dbcontext.SP_RLCS_GetBranchList_ReturnChallan_Historical(customerid, UserID, obj.scopeID, ProfileID).ToList();
        //                            }

        //                            if (lstBranchList_ReturnChallans.Count > 0)
        //                            {
        //                                if (obj.LocationID != -1)
        //                                {
        //                                    List<long> Branchlist = new List<long>();
        //                                    Branchlist.Clear();
        //                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
        //                                    if (Branchlist.Count > 0)
        //                                    {
        //                                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
        //                                    }
        //                                }
        //                                if (!string.IsNullOrEmpty(obj.MonthId))
        //                                {
        //                                    lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
        //                                }
        //                                if (!string.IsNullOrEmpty(obj.YearID))
        //                                {
        //                                    lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
        //                                }
        //                            }
        //                            if (lstBranchList_ReturnChallans.Count > 0)
        //                            {
        //                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.OrderBy(x => x.AVACOM_BranchName).ToList();

        //                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

        //                                var total = lstBranchList_ReturnChallans.Count;
        //                                var pageSize = PageSizeDyanamic;
        //                                var skip = pageSize * (obj.page - 1);
        //                                //var canPage = skip < total;
        //                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Skip(skip).Take(pageSize).ToList();
        //                            }
        //                        }
        //                        returnsNewOP.Add(new ReturnsNew { HistoricalData = lstBranchList_ReturnChallans, NewData = lstBranchList_Returns });
        //                    }
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(returnsNewOP).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize]
        [Route("RLCS/RLCSMyDocumentReturnsNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyDocumentReturnsNew([FromBody]MydocumentHistorical obj)
        {
            try
            {
                List<ReturnsNew> returnsNewOP = new List<ReturnsNew>();

                List<SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew_Result> lstBranchList_ReturnChallans = new List<SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew_Result>();
                List<SP_RLCS_GetBranchList_Returns_Documents_Result> lstBranchList_Returns = new List<SP_RLCS_GetBranchList_Returns_Documents_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);

                            int ReturnsMonth = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Month"]);
                            int ReturnsYear = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Year"]);

                            if (!string.IsNullOrEmpty(obj.MonthId) && !string.IsNullOrEmpty(obj.YearID))
                            {
                                if (Convert.ToInt32(obj.MonthId) > ReturnsMonth && Convert.ToInt32(obj.YearID) >= ReturnsYear)
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        lstBranchList_Returns = entities.SP_RLCS_GetBranchList_Returns_Documents(UserID, customerid, ProfileID).ToList();

                                        if (lstBranchList_Returns.Count > 0)
                                        {
                                            if (obj.LocationID != -1)
                                            {
                                                List<long> Branchlist = new List<long>();
                                                Branchlist.Clear();
                                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                                if (Branchlist.Count > 0)
                                                {
                                                    lstBranchList_Returns = lstBranchList_Returns.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(obj.MonthId))
                                            {
                                                lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                            }
                                            if (!string.IsNullOrEmpty(obj.YearID))
                                            {
                                                lstBranchList_Returns = lstBranchList_Returns.Where(x => x.LM_PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                            }
                                        }

                                        if (lstBranchList_Returns.Count > 0)
                                        {
                                            int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                            var total = lstBranchList_Returns.Count;
                                            var pageSize = PageSizeDyanamic;
                                            var skip = pageSize * (obj.page - 1);
                                            //var canPage = skip < total;
                                            lstBranchList_Returns = lstBranchList_Returns.Skip(skip).Take(pageSize).ToList();
                                        }
                                    }
                                }
                                else
                                {
                                    using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                                    {
                                        lstBranchList_ReturnChallans = dbcontext.SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew(customerid, UserID, obj.scopeID, ProfileID).ToList();
                                    }

                                    if (lstBranchList_ReturnChallans.Count > 0)
                                    {
                                        if (obj.LocationID != -1)
                                        {
                                            List<long> Branchlist = new List<long>();
                                            Branchlist.Clear();
                                            Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                            if (Branchlist.Count > 0)
                                            {
                                                lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(obj.MonthId))
                                        {
                                            lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                        }
                                        if (!string.IsNullOrEmpty(obj.YearID))
                                        {
                                            lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Where(x => x.PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                        }
                                    }
                                    if (lstBranchList_ReturnChallans.Count > 0)
                                    {
                                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.OrderBy(x => x.AVACOM_BranchName).ToList();

                                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                        var total = lstBranchList_ReturnChallans.Count;
                                        var pageSize = PageSizeDyanamic;
                                        var skip = pageSize * (obj.page - 1);
                                        //var canPage = skip < total;
                                        lstBranchList_ReturnChallans = lstBranchList_ReturnChallans.Skip(skip).Take(pageSize).ToList();
                                    }
                                }
                                returnsNewOP.Add(new ReturnsNew { HistoricalData = lstBranchList_ReturnChallans, NewData = lstBranchList_Returns });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(returnsNewOP).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyDocumentChallans")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyDocumentChallans([FromBody]MydocumentHistorical obj)
        {
            try
            {
                List<SP_RLCS_GetBranchList_Challans_DocumentsNew_Result> lstBranchList_Challans = new List<SP_RLCS_GetBranchList_Challans_DocumentsNew_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {                    
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                if (!string.IsNullOrEmpty(obj.challanType))
                                {
                                    lstBranchList_Challans = entities.SP_RLCS_GetBranchList_Challans_DocumentsNew(UserID, customerid, obj.challanType, ProfileID).ToList();

                                    if (lstBranchList_Challans.Count > 0)
                                    {
                                        if (obj.LocationID != -1)
                                        {
                                            //List<long> Branchlist = new List<long>();
                                            //Branchlist.Clear();
                                            //Branchlist = RLCSBranchlist.GetAllHierarchy(customerid, Convert.ToInt32(obj.LocationID)).ToList();
                                            //if (Branchlist.Count > 0)
                                            //{
                                            //    lstBranchList_Challans = lstBranchList_Challans.Where(x => Branchlist.Contains(Convert.ToInt32(x.AVACOM_BranchID))).ToList();
                                            //}
                                        }
                                        if (!string.IsNullOrEmpty(obj.MonthId))
                                        {
                                            lstBranchList_Challans = lstBranchList_Challans.Where(x => x.LM_PayrollMonth.Trim().ToString().Equals(obj.MonthId)).ToList();
                                        }
                                        if (!string.IsNullOrEmpty(obj.YearID))
                                        {
                                            lstBranchList_Challans = lstBranchList_Challans.Where(x => x.LM_PayrollYear.Trim().ToString().Equals(obj.YearID)).ToList();
                                        }
                                    }
                                    if (lstBranchList_Challans.Count > 0)
                                    {
                                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                        var total = lstBranchList_Challans.Count;
                                        var pageSize = PageSizeDyanamic;
                                        var skip = pageSize * (obj.page - 1);
                                        //var canPage = skip < total;
                                        lstBranchList_Challans = lstBranchList_Challans.Skip(skip).Take(pageSize).ToList();
                                    }
                                }
                            }
                        }
                    }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstBranchList_Challans).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/RLCSHistoricalDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSHistoricalDocumentList([FromBody]AllDocumentList obj)
        {
            try
            {
                List<SP_RLCS_GetHistoricalFileDetails_Result> lstdocuments = new List<SP_RLCS_GetHistoricalFileDetails_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {                    
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            if (!string.IsNullOrEmpty(obj.RecordID) && !string.IsNullOrEmpty(obj.scopeID))
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    lstdocuments = entities.SP_RLCS_GetHistoricalFileDetails(Convert.ToInt32(obj.RecordID), obj.scopeID).ToList();
                                }
                            }
                        }
                    }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstdocuments).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSNewDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSNewDocumentList([FromBody]AllDocumentList obj)
        {
            try
            {
                List<SP_RLCS_GetNewFileDetails_Result> lstdocuments = new List<SP_RLCS_GetNewFileDetails_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {                   
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            if (!string.IsNullOrEmpty(obj.RecordID))
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    lstdocuments = entities.SP_RLCS_GetNewFileDetails(Convert.ToInt32(obj.RecordID)).ToList();
                                }
                            }
                        }
                    }                    
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstdocuments).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("RLCS/RLCSDocumentPath")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSDocumentPath([FromBody]DocumentPathInput obj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID); 
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            string pathforandriod = PublicClassZip.FetchRLCSDocumentPath(Convert.ToInt32(obj.FileID), UserID, customerID, obj.FileType, CallerAgent.ToString());
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("RLCS/RLCSMyReportMonthlyComplianceSummary")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyReportMonthlyComplianceSummary([FromBody]ReportMonthlyComplianceSummary obj)
        {
            try
            {
                List<GetCompliancesMonthlySummary> resultObj = new List<GetCompliancesMonthlySummary>();
                List<GetClientCompliancesMonthlySummary> ClientresultObj = new List<GetClientCompliancesMonthlySummary>();
                List<GetResultOnboardingComplianceSummary> result = new List<GetResultOnboardingComplianceSummary>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            #region Get Detail
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                var Record = (entities.SP_RLCS_GetMonthlyComplianceSummary(customerID)).Distinct().ToList();
                                if (Record != null)
                                {
                                    if (Record.Count > 0)
                                    {
                                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, Convert.ToInt32(user.AVACOM_UserID), ProfileID)).ToList();
                                        if (userAssignedBranchList != null)
                                        {
                                            if (userAssignedBranchList.Count > 0)
                                            {
                                                List<int> assignedbranchIDs1 = new List<int>();
                                                assignedbranchIDs1 = userAssignedBranchList.Select(row => row.ID).ToList();
                                                Record = Record.Where(row => row.AVACOM_BranchID != null && assignedbranchIDs1.Contains((int)row.AVACOM_BranchID)).Distinct().ToList();

                                                Record = Record.Where(x => x.Year == Convert.ToString(obj.year)).Where(row => row.Month == Convert.ToString(obj.Month)).Distinct().ToList();

                                                #region Client detail
                                                int ValueComplied = 0;
                                                int ValueNotComplied = 0;
                                                foreach (var item in Record)
                                                {
                                                    ValueComplied = ValueComplied + Convert.ToInt32(item.CompliedNo);
                                                    ValueNotComplied = ValueNotComplied + Convert.ToInt32(item.NotCompliedNo);
                                                }
                                                if (ValueComplied > 0 || ValueNotComplied > 0)
                                                {
                                                    int ClientComplied = 0;
                                                    int ClientNotComplied = 0;
                                                    if (ValueComplied > 0)
                                                    {
                                                        ClientComplied = (int)Math.Round((double)(100 * ValueComplied) / (ValueComplied + ValueNotComplied));
                                                    }
                                                    if (ValueNotComplied > 0)
                                                    {
                                                        ClientNotComplied = (int)Math.Round((double)(100 * ValueNotComplied) / (ValueComplied + ValueNotComplied));
                                                    }

                                                    ClientresultObj.Add(new GetClientCompliancesMonthlySummary
                                                    {
                                                        NotComplied = ClientNotComplied,
                                                        Complied = ClientComplied
                                                    });
                                                }
                                                #endregion
                                                                                               
                                                #region location Compliance Summary

                                                List<int> assignedbranchIDs = new List<int>();
                                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                                if (assignedbranchIDs.Count > 0)
                                                    Record = Record.Where(row => row.AVACOM_BranchID != null && assignedbranchIDs.Contains((int)row.AVACOM_BranchID)).Distinct().ToList();

                                                List<long> Branchlist = new List<long>();
                                                if (obj.locationId != -1)
                                                {
                                                    Branchlist.Clear();
                                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                                                }

                                                if (Branchlist.Count > 0)
                                                    Record = Record.Where(row => row.AVACOM_BranchID != null && Branchlist.Contains((long)row.AVACOM_BranchID)).Distinct().ToList();

                                                foreach (var item in Record)
                                                {
                                                    string name = item.SM_Name + '/' + item.Branch;
                                                                                      
                                                    int compliedCount = Convert.ToInt32(item.CompliedNo);
                                                    int notCompliedCount = Convert.ToInt32(item.NotCompliedNo);

                                                    int compliedCountvalue = 0;
                                                    int NotcompliedCountvalue = 0;
                                                    if (compliedCount > 0 || notCompliedCount > 0)
                                                    {
                                                        if (compliedCount > 0)
                                                        {
                                                            compliedCountvalue = (int)Math.Round((double)(100 * compliedCount) / (compliedCount + notCompliedCount));
                                                        }
                                                        if (notCompliedCount > 0)
                                                        {
                                                            NotcompliedCountvalue = (int)Math.Round((double)(100 * notCompliedCount) / (compliedCount + notCompliedCount));
                                                        }

                                                        resultObj.Add(new GetCompliancesMonthlySummary
                                                        {
                                                            NotComplied = NotcompliedCountvalue,
                                                            Complied = compliedCountvalue,
                                                            Branch = name
                                                        });
                                                    }
                                                }
                                                #endregion

                                                result.Add(new GetResultOnboardingComplianceSummary
                                                {
                                                    Clientdata = ClientresultObj,
                                                    locationdata = resultObj
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyReportOnBoardingSummary")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyReportOnBoardingSummary([FromBody]ReportMonthlyComplianceSummary obj)
        {
            try
            {
                List<GetCompliancesMonthlySummary> resultObj = new List<GetCompliancesMonthlySummary>();
                List<GetClientCompliancesMonthlySummary> ClientresultObj = new List<GetClientCompliancesMonthlySummary>();
                List<GetResultOnboardingComplianceSummary> result = new List<GetResultOnboardingComplianceSummary>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {

                            #region Get Detail
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                                var Record = (entities.SP_GetOnBoardingHealthCheckSummary(customerID)).ToList();

                                if (Record != null)
                                {
                                    #region Client detail
                                    int ValueComplied = 0;
                                    int ValueNotComplied = 0;
                                    foreach (var item in Record)
                                    {
                                        ValueComplied = ValueComplied + Convert.ToInt32(item.CompliedNo);
                                        ValueNotComplied = ValueNotComplied + Convert.ToInt32(item.NotCompliedNo);
                                    }
                                    if (ValueComplied > 0 || ValueNotComplied > 0)
                                    {
                                        int ClientComplied = 0;
                                        int ClientNotComplied = 0;
                                        if (ValueComplied > 0)
                                        {
                                            ClientComplied = (int)Math.Round((double)(100 * ValueComplied) / (ValueComplied + ValueNotComplied));
                                        }
                                        if (ValueNotComplied > 0)
                                        {
                                            ClientNotComplied = (int)Math.Round((double)(100 * ValueNotComplied) / (ValueComplied + ValueNotComplied));
                                        }

                                        ClientresultObj.Add(new GetClientCompliancesMonthlySummary
                                        {
                                            NotComplied = ClientNotComplied,
                                            Complied = ClientComplied
                                        });
                                    }
                                    #endregion


                                    #region Location Details
                                    List<long> Branchlist = new List<long>();
                                    if (obj.locationId != -1)
                                    {
                                        Branchlist.Clear();
                                        Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                                    }

                                    if (Branchlist.Count > 0)
                                        Record = Record.Where(row => Branchlist.Contains((long)row.AVACOM_BranchID) && row.AVACOM_BranchID != null).ToList();

                                    foreach (var item in Record)
                                    {
                                        string name = item.SM_Name + '/' + item.Branch;

                                        int compliedCount = Convert.ToInt32(item.CompliedNo);
                                        int notCompliedCount = Convert.ToInt32(item.NotCompliedNo);

                                        int compliedCountvalue = 0;
                                        int NotcompliedCountvalue = 0;
                                        if (compliedCount > 0 || notCompliedCount > 0)
                                        {
                                            if (compliedCount > 0)
                                            {
                                                compliedCountvalue = (int)Math.Round((double)(100 * compliedCount) / (compliedCount + notCompliedCount));
                                            }
                                            if (notCompliedCount > 0)
                                            {
                                                NotcompliedCountvalue = (int)Math.Round((double)(100 * notCompliedCount) / (compliedCount + notCompliedCount));
                                            }

                                            resultObj.Add(new GetCompliancesMonthlySummary
                                            {
                                                NotComplied = NotcompliedCountvalue,
                                                Complied = compliedCountvalue,
                                                Branch = name
                                            });
                                        }
                                    }
                                    #endregion

                                    result.Add(new GetResultOnboardingComplianceSummary
                                    {
                                        Clientdata = ClientresultObj,
                                        locationdata = resultObj
                                    });
                                }
                            }
                            #endregion
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
               
        [Route("RLCS/RLCSGetAll_OneTimeActivityMaster")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSGetAll_OneTimeActivityMaster()
        {
            try
            {                
                List<RLCS_OneTimeActivitiesMaster_Mapping> lstOneTimeActivites = new List<RLCS_OneTimeActivitiesMaster_Mapping>();

                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    lstOneTimeActivites = dbcontext.RLCS_OneTimeActivitiesMaster_Mapping.Where(row => row.CM_Status != null && row.CM_Status.Trim().ToUpper().Equals("A")
                    && row.CM_ActivityGoup.Trim().ToUpper().Equals("Liaison Activity")).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstOneTimeActivites).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/GetRegistrationRepository")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetRegistrationRepository([FromBody]RegistrationRepositoryInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                List<SP_RLCS_GetRegistrationsRepository_Result> lstRegistrationRepository = new List<SP_RLCS_GetRegistrationsRepository_Result>();
                
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                            {
                                lstRegistrationRepository = dbcontext.SP_RLCS_GetRegistrationsRepository(customerID, Convert.ToInt32(user.AVACOM_UserID), ProfileID).ToList();
                            }
                            if (lstRegistrationRepository.Count > 0)
                            {
                                if (obj.LocationID != -1)
                                {
                                    List<long> Branchlist = new List<long>();
                                    Branchlist.Clear();
                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.LocationID)).ToList();

                                    lstRegistrationRepository = lstRegistrationRepository.Where(x => Branchlist.Contains(Convert.ToInt64(x.AVACOM_BranchID))).ToList();
                                }
                                if (!string.IsNullOrEmpty(obj.ActivityID))
                                {
                                    lstRegistrationRepository = lstRegistrationRepository.Where(x => x.RR_DocID == obj.ActivityID).ToList();
                                }

                                if (lstRegistrationRepository.Count > 0)
                                {
                                    lstRegistrationRepository = lstRegistrationRepository.OrderBy(x => x.AVACOM_BranchName).ToList();

                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstRegistrationRepository.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (obj.page - 1);
                                    //var canPage = skip < total;
                                    lstRegistrationRepository = lstRegistrationRepository.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstRegistrationRepository).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/GetAll_NewRegistrationLicenseDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAll_NewRegistrationLicenseDetails([FromBody]RegistrationRepositoryInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                List<SP_RLCS_GetNewRegistrationsDetail_Result> lstNewRegistration = new List<SP_RLCS_GetNewRegistrationsDetail_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                            {
                                lstNewRegistration = dbcontext.SP_RLCS_GetNewRegistrationsDetail(customerID, Convert.ToInt32(user.AVACOM_UserID), ProfileID).ToList();
                            }
                            if (lstNewRegistration.Count > 0)
                            {
                                if (obj.LocationID != -1)
                                {
                                    List<long> Branchlist = new List<long>();
                                    Branchlist.Clear();
                                    Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.LocationID)).ToList();

                                    lstNewRegistration = lstNewRegistration.Where(x => Branchlist.Contains(Convert.ToInt64(x.AVACOM_BranchID))).ToList();
                                }
                                if (!string.IsNullOrEmpty(obj.ActivityID))
                                {
                                    lstNewRegistration = lstNewRegistration.Where(x => x.OA_ActivityID == obj.ActivityID).ToList();
                                }

                                if (lstNewRegistration.Count > 0)
                                {
                                    lstNewRegistration = lstNewRegistration.OrderBy(x => x.AVACOM_BranchName).ToList();

                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstNewRegistration.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (obj.page - 1);
                                   // var canPage = skip < total;
                                    lstNewRegistration = lstNewRegistration.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstNewRegistration).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyNotice")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyNotice([FromBody]RLCSMyNoticeInput obj)
        {
            try
            {
                List<SP_RLCS_NoticeAssignedInstance_Result> lstNoticeDetails = new List<SP_RLCS_NoticeAssignedInstance_Result>();
              
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            List<long> Branchlist = new List<long>();
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            if (obj.locationId != -1)
                            {   
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.locationId)).ToList();
                            }
                            #region Get Detail
                        
                            lstNoticeDetails = RLCSManagement.GetInspectionNoticeList(Convert.ToInt32(customerID), Convert.ToInt32(user.AVACOM_UserID), ProfileID, Branchlist, obj.StatusType, obj.NoticeType);
                            if (lstNoticeDetails.Count > 0)
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(customerID), Convert.ToInt32(user.AVACOM_UserID), ProfileID)).ToList();

                                    if (userAssignedBranchList != null)
                                    {
                                        if (userAssignedBranchList.Count > 0)
                                        {
                                            List<int> assignedbranchIDs = new List<int>();
                                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                            if (assignedbranchIDs.Count > 0)
                                                lstNoticeDetails = lstNoticeDetails.Where(Entry => assignedbranchIDs.Contains(Entry.CustomerBranchID)).ToList();

                                        }
                                    }
                                }

                                if (lstNoticeDetails.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstNoticeDetails.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (obj.Page - 1);
                                    //var canPage = skip < total;
                                    lstNoticeDetails = lstNoticeDetails.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                            #endregion
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstNoticeDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyNoticeDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyNoticeDocument([FromBody]RLCSMyDocumentNotice obj)
        {
            try
            {
                List<SP_RLCS_NoticeDocuments_Result> lstNoticeDocs = new List<SP_RLCS_NoticeDocuments_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);                
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            List<long> Branchlist = new List<long>();
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);

                            #region Get Detail
                            lstNoticeDocs = RLCSManagement.GetNoticeDocuments(Convert.ToInt32(obj.noticeInstanceID), customerID, "");
                            if (lstNoticeDocs.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = lstNoticeDocs.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.Page - 1);
                                //var canPage = skip < total;
                                lstNoticeDocs = lstNoticeDocs.Skip(skip).Take(pageSize).ToList();
                            }
                            #endregion
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstNoticeDocs).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSMyNoticeDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSMyNoticeDetail([FromBody]RLCSMyDocumentNotice obj)
        {
            try
            {
                List<RLCSMyDocumentNoticeOutput> lstNoticeDtls = new List<RLCSMyDocumentNoticeOutput>();

                string Type = string.Empty;
                string ReceiptDate = string.Empty;
                string ReferenceNumber = string.Empty;
                string Notice_Title = string.Empty;
                string Close_Date = string.Empty;
                string Act = string.Empty;
                string Location = string.Empty;
                int ActId = -1;
                int LocationId = -1;

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            List<long> Branchlist = new List<long>();
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);

                            var noticeRecord = RLCSManagement.GetNoticeByID(Convert.ToInt32(obj.noticeInstanceID));

                            if (noticeRecord != null)
                            {

                                if (noticeRecord.NoticeType != null)
                                {
                                    if (noticeRecord.NoticeType.ToString() == "I")
                                        Type = "I";
                                    else if (noticeRecord.NoticeType.ToString() == "O")
                                        Type = "O";
                                }

                                if (noticeRecord.NoticeDate != null)
                                {
                                    ReceiptDate = noticeRecord.NoticeDate.ToString();
                                }


                                if (noticeRecord.RefNo != null)
                                {
                                    ReferenceNumber = noticeRecord.RefNo.ToString();
                                }

                                if (noticeRecord.NoticeTitle != null)
                                {
                                    Notice_Title = noticeRecord.NoticeTitle.ToString();
                                }

                                var StatusDetails = RLCSManagement.GetNoticeStatusDetail(Convert.ToInt32(obj.noticeInstanceID));

                                if (StatusDetails != null)
                                {
                                    Close_Date = StatusDetails.CloseDate.ToString();
                                }

                                var ListofAct = RLCSManagement.GetListOfAct(Convert.ToInt32(obj.noticeInstanceID), 2);
                                if (ListofAct.Count > 0)
                                {
                                    ActId = ListofAct.Select(c => c.ActID).FirstOrDefault();
                                    if (ActId > 0)
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            var ActDetail = (from row in entities.Acts
                                                             where row.ID == ActId
                                                             && row.IsDeleted == false
                                                             select row).FirstOrDefault();
                                            if (ActDetail != null)
                                            {
                                                Act = ActDetail.Name;
                                            }
                                        }
                                    }

                                }


                                if (noticeRecord.CustomerBranchID != 0)
                                {
                                    LocationId = Convert.ToInt32(noticeRecord.CustomerBranchID);
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        var BranchDetail = (from row in entities.CustomerBranches
                                                            where row.ID == noticeRecord.CustomerBranchID
                                                            && row.IsDeleted == false
                                                            select row).FirstOrDefault();
                                        if (BranchDetail != null)
                                        {
                                            Location = BranchDetail.Name;
                                        }
                                    }

                                }
                            }

                                lstNoticeDtls.Add(new RLCSMyDocumentNoticeOutput
                                {
                                    Type = Type,
                                    Receipt_Date = ReceiptDate,
                                    Reference_No= ReferenceNumber,
                                    Close_Date = Close_Date,
                                    Act = Act,
                                    ActId= ActId,
                                    Location = Location,
                                    LocationId= LocationId,
                                    Notice_Title = Notice_Title,
                                });
                            }                        
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstNoticeDtls).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSSummaryDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSSummaryDetail([FromBody]RLCSSummaryDetailInput obj)
        {
            try
            {
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int riskid = -1;
                            bool IsApprover = false;

                            DateTime FromDate= DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime Enddate= DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(obj.Year))
                            {
                                int year = Convert.ToInt32(obj.Year);
                                FromDate = new DateTime(year, 1, 1);
                                Enddate = new DateTime(year, 12, 31);
                            }

                            List<int?> CategoryIds = new List<int?>();
                            if (obj.CategoryId == -1)
                            {                               
                                CategoryIds.Add(5);
                                CategoryIds.Add(2);
                            }
                            else if (obj.CategoryId == 5)
                            {
                                CategoryIds.Add(5);
                            }
                            else if (obj.CategoryId == 2)
                            {                                
                                CategoryIds.Add(2);
                            }

                            List<long> Branchlist = new List<long>();

                            #region Risk
                            if (!string.IsNullOrEmpty(obj.Risk))
                            {
                                if (obj.Risk.Trim().Equals("High"))
                                {
                                    riskid = 0;
                                }
                                else if (obj.Risk.Trim().Equals("Medium"))
                                {
                                    riskid = 1;
                                }
                                else if (obj.Risk.Trim().Equals("Low"))
                                {
                                    riskid = 2;
                                }
                            }
                            #endregion

                            #region attrubute
                            List<int> statusIds = new List<int>();
                            if (!string.IsNullOrEmpty(obj.Flag))
                            {
                                if (obj.Flag.Trim().Equals("Closed Delayed"))
                                {
                                    statusIds.Add(5);
                                    statusIds.Add(9);
                                }
                                else if (obj.Flag.Trim().Equals("Closed Timely"))
                                {
                                    statusIds.Add(4);
                                    statusIds.Add(7);
                                }
                                else if (obj.Flag.Trim().Equals("Not Completed"))
                                {
                                    statusIds.Add(1);
                                    statusIds.Add(10);
                                    statusIds.Add(8);
                                    statusIds.Add(6);
                                    statusIds.Add(2);
                                    statusIds.Add(3);
                                }                                
                                else
                                {
                                    statusIds.Add(1);
                                    statusIds.Add(6);
                                    statusIds.Add(10);
                                    statusIds.Add(4);
                                    statusIds.Add(5);
                                    statusIds.Add(7);
                                    statusIds.Add(9);
                                    statusIds.Add(8);
                                    statusIds.Add(2);
                                    statusIds.Add(3);
                                }
                            }
                            #endregion

                            if (obj.LocationId != -1)
                            {
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.LocationId)).ToList();
                                detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerID, Branchlist,Convert.ToInt32(obj.LocationId) , statusIds, "Function", CategoryIds, riskid, obj.Flag, FromDate, Enddate, Convert.ToInt32(user.AVACOM_UserID), IsApprover);
                            }
                            else
                            {                                
                                detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerID, Branchlist, statusIds, "Function", CategoryIds, riskid, obj.Flag, FromDate, Enddate, Convert.ToInt32(user.AVACOM_UserID), IsApprover);                                                              
                            }

                            if (detailView.Count > 0)
                            {
                                detailView = (from s in detailView
                                              orderby s.Risk
                                              select s).ToList();

                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = detailView.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.Page - 1);
                                //var canPage = skip < total;
                                detailView = detailView.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(detailView).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSSummaryDetailNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSSummaryDetailNew([FromBody]RLCSSummaryDetailInput obj)
        {
            try
            {
                List<RLCSSummaryOutputDetails> Output = new List<RLCSSummaryOutputDetails>();
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int riskid = -1;
                            bool IsApprover = false;

                            DateTime FromDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime Enddate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(obj.Year))
                            {
                                int year = Convert.ToInt32(obj.Year);
                                FromDate = new DateTime(year, 1, 1);
                                Enddate = new DateTime(year, 12, 31);
                            }
                            List<long> Branchlist = new List<long>();

                            #region Category
                            List<int?> CategoryIds = new List<int?>();
                            CategoryIds.Add(5);
                            CategoryIds.Add(2);

                            //List<int?> CategoryIds = new List<int?>();
                            //if (obj.CategoryId == -1)
                            //{
                            //    CategoryIds.Add(5);
                            //    CategoryIds.Add(2);
                            //}
                            //else if (obj.CategoryId == 5)
                            //{
                            //    CategoryIds.Add(5);
                            //}
                            //else if (obj.CategoryId == 2)
                            //{
                            //    CategoryIds.Add(2);
                            //}
                            #endregion
                            
                            #region Risk
                            //if (!string.IsNullOrEmpty(obj.Risk))
                            //{
                            //    if (obj.Risk.Trim().Equals("High"))
                            //    {
                            //        riskid = 0;
                            //    }
                            //    else if (obj.Risk.Trim().Equals("Medium"))
                            //    {
                            //        riskid = 1;
                            //    }
                            //    else if (obj.Risk.Trim().Equals("Low"))
                            //    {
                            //        riskid = 2;
                            //    }
                            //}
                            #endregion

                            #region status flag
                            List<int> statusIds = new List<int>();
                            if (!string.IsNullOrEmpty(obj.Flag))
                            {
                                if (obj.Flag.Trim().Equals("Closed Delayed"))
                                {
                                    statusIds.Add(5);
                                    statusIds.Add(9);
                                }
                                else if (obj.Flag.Trim().Equals("Closed Timely"))
                                {
                                    statusIds.Add(4);
                                    statusIds.Add(7);
                                }
                                else if (obj.Flag.Trim().Equals("Not Completed"))
                                {
                                    statusIds.Add(1);
                                    statusIds.Add(10);
                                    statusIds.Add(8);
                                    statusIds.Add(6);
                                    statusIds.Add(2);
                                    statusIds.Add(3);
                                }
                                else
                                {
                                    statusIds.Add(1);
                                    statusIds.Add(6);
                                    statusIds.Add(10);
                                    statusIds.Add(4);
                                    statusIds.Add(5);
                                    statusIds.Add(7);
                                    statusIds.Add(9);
                                    statusIds.Add(8);
                                    statusIds.Add(2);
                                    statusIds.Add(3);
                                }
                            }
                            #endregion

                            if (obj.LocationId != -1)
                            {
                                Branchlist.Clear();
                                Branchlist = RLCSBranchlist.GetAllHierarchy(customerID, Convert.ToInt32(obj.LocationId)).ToList();
                                detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerID, Branchlist, Convert.ToInt32(obj.LocationId), statusIds, "Function", CategoryIds, riskid, obj.Flag, FromDate, Enddate, Convert.ToInt32(user.AVACOM_UserID), IsApprover);
                            }
                            else
                            {
                                detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerID, Branchlist, statusIds, "Function", CategoryIds, riskid, obj.Flag, FromDate, Enddate, Convert.ToInt32(user.AVACOM_UserID), IsApprover);
                            }
                                                       
                            if (detailView.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(obj.Risk))
                                {
                                    if (obj.Risk.Trim().Equals("High"))
                                    {
                                        detailView = detailView.Where(entry => entry.Risk == 0).ToList();
                                        // riskid = 0;
                                    }
                                    else if (obj.Risk.Trim().Equals("Medium"))
                                    {
                                        detailView = detailView.Where(entry => entry.Risk == 1).ToList();
                                        // riskid = 1;
                                    }
                                    else if (obj.Risk.Trim().Equals("Low"))
                                    {
                                        detailView = detailView.Where(entry => entry.Risk == 2).ToList();
                                        //riskid = 2;
                                    }
                                }

                                //int hrcount = 0;
                                //int Financecount = 0;

                                int hrcount = detailView.Where(x => x.ComplianceCategoryId == 2).ToList().Count;
                                int Financecount = detailView.Where(x => x.ComplianceCategoryId == 5).ToList().Count;

                                if (obj.CategoryId == 2 || obj.CategoryId == 5)
                                {
                                    detailView = detailView.Where(entry => entry.ComplianceCategoryId == obj.CategoryId).ToList();
                                }

                                int HighCount = detailView.Where(entry => entry.Risk == 0).ToList().Count;
                                int MediumCount = detailView.Where(entry => entry.Risk == 1).ToList().Count;
                                int LowCount = detailView.Where(entry => entry.Risk == 2).ToList().Count;
                                
                                detailView = (from s in detailView
                                              orderby s.Risk
                                              select s).ToList();

                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = detailView.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (obj.Page - 1);
                                detailView = detailView.Skip(skip).Take(pageSize).ToList();

                                Output.Add(new RLCSSummaryOutputDetails
                                {
                                    ComplianceSummaryDetail = detailView,

                                    HighCount = HighCount,
                                    MediumCount = MediumCount,
                                    LowCount = LowCount,

                                    hrcount = hrcount,
                                    Financecount = Financecount
                                });
                            }                                                    
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Output).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSComplianceDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSComplianceDocumentList([FromBody]AllComplianceDocumentList obj)
        {
            try
            {
                List<GetComplianceDocumentsView> lstdocuments = new List<GetComplianceDocumentsView>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            if (!string.IsNullOrEmpty(obj.ScheduledOnID))
                            {
                                long SID = Convert.ToInt64(obj.ScheduledOnID);
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    lstdocuments = (from row in entities.GetComplianceDocumentsViews
                                                    where row.ScheduledOnID == SID
                                                    select row).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstdocuments).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/RLCSComplianceDocumentPath")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RLCSComplianceDocumentPath([FromBody]AllComplianceDocumentPath obj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            string pathforandriod = PublicClassZip.FetchRLCSComplianceDocumentPath(Convert.ToInt32(obj.FileID), Convert.ToInt32(user.AVACOM_UserID), customerID, Convert.ToInt64(obj.ScheduledOnID));
                            if (!string.IsNullOrEmpty(pathforandriod))
                            {
                                string pathforandriodoutput = pathforandriod;
                                pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                            }
                            else {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("RLCS/GetAssignedEntities")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAssignedEntities([FromBody]DashboradCountInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);

                List<SP_RLCS_GetAssignedEntities_Result> lstAssignedEntities = new List<SP_RLCS_GetAssignedEntities_Result>();

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            
                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                            {
                                lstAssignedEntities = dbcontext.SP_RLCS_GetAssignedEntities(customerID, Convert.ToInt32(user.AVACOM_UserID), ProfileID).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedEntities).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/GetInvoices")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetInvoices([FromBody]GetInvoiceInput obj)
        {
            try
            {
                var CallerAgent = HttpContext.Current.Request.UserAgent;
            
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
            
                List<RLCS_Invoice_Master> lstInvoices = new List<RLCS_Invoice_Master>();

                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);

                            using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                            {
                                lstInvoices = dbcontext.RLCS_Invoice_Master.Where(row => row.IsActive == true && row.ClientID.Trim().Equals(obj.clientID)).ToList();

                                if (lstInvoices.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(obj.type))
                                    {
                                        if (obj.type.Trim().Equals("O"))
                                        {
                                            lstInvoices = lstInvoices.Where(row => row.TotalInvoiceAmount != null && row.InvoiceCollectedAmount != null
                                             && (row.TotalInvoiceAmount - row.InvoiceCollectedAmount) > 10).ToList();
                                        }
                                        else if (obj.type.Trim().Equals("C"))
                                        {
                                            lstInvoices = lstInvoices.Where(row => row.InvoiceMonth != null && row.InvoiceYear != null)
                                               .OrderByDescending(row => row.InvoiceMonth).ThenByDescending(row => row.InvoiceYear).Take(1).ToList();
                                        }
                                    }
                                    if (lstInvoices.Count > 0)
                                    {
                                      
                                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                        var total = lstInvoices.Count;
                                        var pageSize = PageSizeDyanamic;
                                        var skip = pageSize * (obj.PageNumber - 1);
                                        lstInvoices = lstInvoices.Skip(skip).Take(pageSize).ToList();
                                    }
                                }
                            }
                        }
                    }
                }            
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstInvoices).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/GetInvoiceDocumentPath")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetInvoiceDocumentPath([FromBody]GetInvoiceDocumentInput obj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            if (!string.IsNullOrEmpty(obj.clientID) && !string.IsNullOrEmpty(obj.MonthID)
                                && !string.IsNullOrEmpty(obj.YearID) && !string.IsNullOrEmpty(obj.InvoiceNumber))
                            {

                                try
                                {
                                    string directoryPath = string.Empty;
                                    long userid = Convert.ToInt64(user.AVACOM_UserID);
                                    
                                    string urlAddress = ConfigurationManager.AppSettings["PathForInvoice"].ToString();
                                    //string urlAddress = "http://complianceapi.teamlease.com/api/Masters/GenerateInvoicePath?ClientId=";
                                    urlAddress = urlAddress + obj.clientID + "&Month=" + obj.MonthID + "&Year=" + obj.YearID + "&InvoiceNumber=" + obj.InvoiceNumber;

                                    string FileStoreLocation = "\\TempFiles\\RLCSInvoice\\" + userid + "\\" + Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");
                                    directoryPath = ConfigurationManager.AppSettings["destinationPath"].ToString() + FileStoreLocation;
                                    string fileName = obj.clientID + "_Invoicedocument.pdf";

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    directoryPath = directoryPath + @"\" + fileName;


                                    using (WebClient client = new WebClient())
                                    {
                                        client.DownloadFile(urlAddress, directoryPath);
                                        client.Dispose();
                                    }

                                    FileStoreLocation = FileStoreLocation.Substring(FileStoreLocation.LastIndexOf("TempFiles"));
                                    FileStoreLocation = FileStoreLocation.Replace("\\", "/");
                                    string Destinationfinalpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString() + FileStoreLocation + "/" + fileName;

                                    UpdateNotificationObj.Add(new UpdateNotification { Message = Destinationfinalpath });
                                }
                                catch
                                {
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "There Are No Invoice SoftCopies Found for the Requested Parameter" });
                                }
                            }
                            else {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Input data should not be Empty" });
                            }
                        }
                        else {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "User Not Valid" });
                        }
                    }
                    else {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "User Not Valid" });
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/GetEntityDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetEntityDetail([FromBody]GetEntityInput GetEntityInputobj)
        {
            try
            {
                List<Profile_BasicInfo> Entityoutputobj = new List<Profile_BasicInfo>();
                
                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(GetEntityInputobj.UserName.ToString(), CallerAgent);
                string userProfileID = getDecryptedText(GetEntityInputobj.ProfileID.ToString(), CallerAgent); 
                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                            string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];
                            string authKey = string.Empty;
                            try
                            {
                                authKey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                            }
                            catch (Exception ex)
                            {
                                authKey = string.Empty;
                            }

                            string encryptedProfileID = string.Empty;

                            if (!string.IsNullOrEmpty(TLConnectKey))
                            {
                                if (!string.IsNullOrEmpty(userProfileID))
                                {
                                    encryptedProfileID = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                                }
                            }                         

                            string requestUrl = TLConnectAPIUrl;
                            requestUrl += "Dashboard/GetProfile_BasicInformationByProfileID?profileID=" + userProfileID;

                            string responseData = RLCSManagement.InvokeWithoutAuthKey("GET", requestUrl, authKey, encryptedProfileID, "");

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                var _objProfileInfo = JsonConvert.DeserializeObject<Profile_BasicInfo>(responseData);

                                if (_objProfileInfo != null)
                                {
                                    if (_objProfileInfo.lstBasicInfrmation != null)
                                    {
                                        Entityoutputobj.Add(new Profile_BasicInfo { ClientCount = _objProfileInfo.ClientCount, ProfileName = _objProfileInfo.ProfileName, Designation = _objProfileInfo.Designation, lstBasicInfrmation = _objProfileInfo.lstBasicInfrmation });
                                    }
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Entityoutputobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("RLCS/UpdateTrackUserActivities")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateTrackUserActivities([FromBody]GetUpdateUserTrak obj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                bool SaveSuccess = false;
                var CallerAgent = HttpContext.Current.Request.UserAgent;

                string requestJWT = HttpContext.Current.Request.Headers["Authorization"].ToString();
                
             
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);

                Library.WriteErrorLog("Start Date Time-" + DateTime.Now.ToString());
                Library.WriteErrorLog("UserName-" + UserName);
                Library.WriteErrorLog("Activity-" + obj.Activity);
                Library.WriteErrorLog("PageName-" + obj.PageName);
                Library.WriteErrorLog("ProfileID-" + obj.ProfileID);
                Library.WriteErrorLog("RecordID-" + obj.RecordID);
                Library.WriteErrorLog("RequestJWT-" + requestJWT);
                Library.WriteErrorLog("End Date Time-" + DateTime.Now.ToString());

                if (!string.IsNullOrEmpty(UserName))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerID = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);

                            string Activity = string.Empty;
                            if (!string.IsNullOrEmpty(obj.Activity))
                            {
                                Activity = obj.Activity;
                            }
                            string RecordID = string.Empty;
                            if (!string.IsNullOrEmpty(obj.RecordID))
                            {
                                RecordID = obj.RecordID;
                            }
                            string PageName = string.Empty;
                            if (!string.IsNullOrEmpty(obj.PageName))
                            {
                                PageName = obj.PageName;
                            }
                            string ProfileID = string.Empty;
                            if (!string.IsNullOrEmpty(obj.ProfileID))
                            {
                                //ProfileID = obj.ProfileID;
                                ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                            }
                            RLCS_Track_User_Activities newRecord = new RLCS_Track_User_Activities()
                            {
                                UserID = Convert.ToString(user.AVACOM_UserID),
                                Client = Convert.ToString(customerID),
                                Page = PageName,
                                Actitvity = Activity,
                                SessionID = requestJWT.Trim(),
                                RecordID = RecordID,
                                Type = CallerAgent,
                                ProfileID= ProfileID
                            };
                            SaveSuccess = RLCSManagement.CreateUpdateTrack_User_Activities(newRecord);
                        }
                    }
                }
                if (SaveSuccess)
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("Error Message Date-" + DateTime.Now.ToString());
                Library.WriteErrorLog("Error Message-" + ex.StackTrace.ToString());
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("RLCS/MaintainDeviceIDNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MaintainDeviceIDNew([FromBody]RLCSDeviceDetails devicedetailObj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                string DeviceToken = string.Empty;
                if (!string.IsNullOrEmpty(devicedetailObj.DeviceToken))
                {
                    DeviceToken = devicedetailObj.DeviceToken.Trim();

                    var CallerAgent = HttpContext.Current.Request.UserAgent;

                    string ProfileID = getDecryptedText(devicedetailObj.ProfileID.ToString(), CallerAgent);
                    string ISproduct = "H";

                    if (!string.IsNullOrEmpty(ProfileID))
                    {
                        RLCS_User_Mapping user = null;
                        if (RLCSUserManagement.IsValidProfileID(ProfileID.Trim(), out user))
                        {
                            if (user != null)
                            {
                                int userID = Convert.ToInt32(user.AVACOM_UserID);
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {

                                    MaintainDeviceID checkactive = (from row in entities.MaintainDeviceIDs
                                                                    where row.Product == ISproduct
                                                                    && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                                    && row.UserID == userID
                                                                    && row.ProfileID.ToUpper() == ProfileID.ToUpper()
                                                                    && row.Status == "Active"
                                                                    select row).FirstOrDefault();
                                    if (checkactive != null)
                                    {
                                        checkactive.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        MaintainDeviceID obj = new MaintainDeviceID();
                                        obj.UserID = userID;
                                        obj.DeviceIDIMEI = devicedetailObj.DeviceIDIMEI;
                                        obj.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                        obj.Status = "Active";
                                        obj.Product = ISproduct;
                                        obj.ProfileID = user.ProfileID;
                                        entities.MaintainDeviceIDs.Add(obj);
                                        entities.SaveChanges();
                                    }
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }
                    }
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "Token should not be empty." });
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("RLCS/PushNotification")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PushNotification([FromBody]PushNotofication devicedetailObj)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                bool flag = false;
                string ProfileID = devicedetailObj.ProfileID.ToString();

                if (!string.IsNullOrEmpty(ProfileID))
                {
                    List<string> details = new List<string>();
                    details.Clear();

                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidProfileID(ProfileID.Trim(), out user))
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var checkactive = (from row in entities.MaintainDeviceIDs
                                               where row.Product == "H"
                                               && row.ProfileID.ToUpper() == ProfileID.ToUpper()
                                               && row.Status == "Active"
                                               select row).ToList();

                            foreach (var item in checkactive)
                            {
                                if (!string.IsNullOrEmpty(item.DeviceToken))
                                {
                                    details.Add(item.DeviceToken);
                                }
                            }
                        }

                        details = details.Where(x => x != "null").ToList();
                        details = details.Distinct().ToList();
                        string[] arr1 = details.Select(i => i.ToString()).ToArray();
                        if (arr1.Length > 0)
                        {
                            string MobileNotificationApplicationID = ConfigurationManager.AppSettings["MobileCompVidhiNotificationApplicationID"];
                            string MobileNotificationSenderID = ConfigurationManager.AppSettings["MobileCompVidhiNotificationSenderID"];
                            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                            tRequest.Method = "post";
                            tRequest.ContentType = "application/json";
                            var data = new
                            {
                                registration_ids = arr1,//for multiple
                                notification = new
                                {
                                    body = devicedetailObj.Message.ToString(),
                                    title = devicedetailObj.Title.ToString()
                                }
                            };

                            var serializer = new JavaScriptSerializer();
                            var json = serializer.Serialize(data);
                            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                            tRequest.Headers.Add(string.Format("Authorization: key={0}", MobileNotificationApplicationID));
                            tRequest.Headers.Add(string.Format("Sender: id={0}", MobileNotificationSenderID));
                            tRequest.ContentLength = byteArray.Length;
                            using (Stream dataStream = tRequest.GetRequestStream())
                            {
                                dataStream.Write(byteArray, 0, byteArray.Length);
                                using (WebResponse tResponse = tRequest.GetResponse())
                                {
                                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                    {
                                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                        {
                                            String sResponseFromServer = tReader.ReadToEnd();
                                            string str = sResponseFromServer;
                                            flag = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (flag)
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Notification Send Successfully." });
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }



       [Authorize]
        [Route("RLCS/getNotificationData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage getNotificationData(string Email, int ddlNotificationType)
        {
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<NotificationDetails> NotificationDetailsObj = new List<NotificationDetails>();
                List<Data.SP_GetUserNotificationsByUserID_Result> Notifications = new List<Data.SP_GetUserNotificationsByUserID_Result>();
                int userID = -1;

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.RLCS_User_Mapping
                                                  where (row.UserID == Email || row.UserID == EmailIOS)
                                                    && row.IsActive == true
                                                    //&& row.IsDeleted == false
                                                  select row.AVACOM_UserID).FirstOrDefault());
                    }
                    if (userID != -1)
                    {
                        if (ddlNotificationType == -1)
                            Notifications = ComplianceManagement.GetAllUserNotificationNew(userID).ToList();
                        else
                            Notifications = ComplianceManagement.GetAllUserNotificationNew(userID).Where(Entry => Entry.IsRead == Convert.ToBoolean(Convert.ToInt32(ddlNotificationType))).OrderByDescending(entry => entry.UpdatedOn).ToList();
                    }
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Notifications = Notifications.Where(x => x.Type.Equals("Register Expiry")).ToList();
                        if (Notifications.Count > 0)
                        {
                            foreach (var item in Notifications)
                            {
                                string ComplianceShortDetail = "RLCS Registration Expiry Update";

                                NotificationDetailsObj.Add(new NotificationDetails
                                {
                                    ActID = 0,
                                    ComplianceID = 0,
                                    ID = item.ID,
                                    IsRead = item.IsRead,
                                    NotificationID = item.NotificationID,
                                    Remark = item.Remark,
                                    ShortDetail = ComplianceShortDetail,
                                    Type = item.Type,
                                    UpdatedOn = item.UpdatedOn,
                                    UserID = item.UserID
                                });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(NotificationDetailsObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        //[Authorize]
        [Route("RLCS/updateNotificationData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage updateNotificationData(string Email, int NotificationID)
        {
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                int userID = -1;

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.RLCS_User_Mapping
                                                  where (row.UserID == Email || row.UserID == EmailIOS)
                                                    && row.IsActive == true
                                                  // && row.IsDeleted == false
                                                  select row.AVACOM_UserID).FirstOrDefault());
                    }
                    if (userID != -1)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<long> NotificationsDetails = new List<long>();
                            NotificationsDetails.Add(Convert.ToInt32(NotificationID));

                            if (NotificationsDetails != null)
                            {
                                ComplianceManagement.SetReadToNotification(NotificationsDetails, userID);
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //By Kunal for Critical_Document Requirment
        //[Authorize]
        [Route("RLCS/GetCriticalDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCriticalDocumentList([FromBody] MyCriticalDocuments obj)
        {
            try
            {
                List<object> _traversedObjList = new List<object>();
                List<CriticalDocumentList> _criticalDocumentsList = new List<CriticalDocumentList>();
                var _criticalDocumentList = new List<CriticalDocumentList>();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                string UserName = getDecryptedText(obj.UserName.ToString(), CallerAgent);
                string ProfileID = getDecryptedText(obj.ProfileID.ToString(), CallerAgent);
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ProfileID))
                {
                    RLCS_User_Mapping user = null;
                    if (RLCSUserManagement.IsValidUserName(UserName.Trim(), out user))
                    {
                        if (user != null)
                        {
                            int customerid = RLCSUserManagement.GetCustomerIDRLCS(user.CustomerID);
                            int UserID = Convert.ToInt32(user.AVACOM_UserID);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {

                                CriticalDocumentList _obj = new CriticalDocumentList();
                                var lstObj = entities.SP_GetCriticalDocumentsDetails(customerid, UserID, ProfileID).ToList();

                                foreach (var data in lstObj)
                                {

                                    bool isExist = _traversedObjList.Contains(data.FolderID);
                                    if (!isExist)
                                    {
                                        var _object = new CriticalDocumentList()
                                        {

                                            CustomerID = (int)data.CustomerID,
                                            ID = (int)data.ID,
                                            FolderName = data.FolderName,
                                            ParentID = data.ParentID,
                                            isFile = false
                                        };

                                        _traversedObjList.Add(data.FolderID);
                                        _criticalDocumentList.Add(_object);
                                    }
                                    if (data.FileID != null)
                                    {
                                        string myFile = data.FileName;
                                        string extension = Path.GetExtension(myFile);
                                        var _object = new CriticalDocumentList()
                                        {
                                            CustomerID = (int)data.CustomerID,
                                            ParentID = (int)data.ID,
                                            ID = (int)data.FileID,
                                            FileName = data.FileName,
                                            // FilePath = data.FilePath,
                                            FilePath = ConfigurationManager.AppSettings["AVACOM_Portal_URL"].ToString() + data.FilePath.Remove(0, 1) + data.FileKey + extension,
                                            isFile = true
                                        };
                                        _criticalDocumentList.Add(_object);
                                    }
                                }

                            }

                        }
                    }
                }
                if (_criticalDocumentList.Count > 0)
                {
                    _criticalDocumentsList = _criticalDocumentList.BuildTree();
                    return new HttpResponseMessage()
                    {

                        Content = new StringContent(JArray.FromObject(_criticalDocumentsList).ToString(), Encoding.UTF8, "application/json")
                    };
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(_criticalDocumentsList).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
