﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Controllers
{
    public class TypeOfCompliance
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class checklistRemark
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class ComplianceRemark
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}