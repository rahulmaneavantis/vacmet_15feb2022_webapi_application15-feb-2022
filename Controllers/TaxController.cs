﻿using AppWebApplication.Data;
using AppWebApplication.DataLitigation;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AppWebApplication.Controllers
{
    public class TaxController : ApiController
    {
        public class CombineWorkspace
        {
            public List<SP_IndirectCaseWorkspaceReport_Result> IndirectCase { get; set; }
            public List<SP_CommercialCaseReportWithCustomParameter_Result> CommercialCase { get; set; }
        }

        [Route("Tax/KendoUserInfo")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoUserInfo(int CustId)
        {
            try
            {
                List<object> dataSource = new List<object>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var userList = LitigationUserManagement.GetAllUser(CustId, string.Empty);

                    foreach (View_DisplayUserWithRating user in userList)
                    {
                        dataSource.Add(new
                        {
                            user.ID,
                            user.FirstName,
                            user.LastName,
                            user.Email,
                            user.ContactNumber,
                            ComRole = UserManagement.GetRoleByID(Convert.ToInt32(user.RoleID)).Name,
                            Role = user.LitigationRoleID != null ? UserManagement.GetRoleByID(Convert.ToInt32(user.LitigationRoleID)).Name : "",
                            user.IsActive,
                            user.Rating
                        });
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(dataSource).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Add_CCaseResult")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_CCaseResult(string CaseStatus, string CloseDate, string Remarks, int CustomerID, int CCaseID, string IViewState, int Id, int CreatedBy)
        {

            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(CaseStatus))
                {
                    result = TaxationMethods.AddCCaseResult(CaseStatus, CloseDate, Remarks, CustomerID, CCaseID, IViewState, Id, CreatedBy);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }



        [Route("Tax/KendoCCAuditLogGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCCAuditLogGrid(string CaseID)
        {
            try
            {
                List<tax_tbl_AuditDetails> obj = new List<tax_tbl_AuditDetails>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    if (CaseID != "0" && !string.IsNullOrEmpty(CaseID))
                    {
                        obj = (from row in entities.tax_tbl_AuditDetails
                               select row).ToList();
                        if (obj.Count > 0)
                        {
                            obj = obj.Where(x => x.CaseInstanceID == CaseID).OrderByDescending(entry => entry.ID).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/KendoCCaseStatusDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCCaseStatusDetails(int CustId, long CCaseID)
        {
            try
            {
                List<tax_tbl_CommercialCaseStatus> objfp = new List<tax_tbl_CommercialCaseStatus>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    if (CCaseID != 0)
                    {
                        objfp = (from row in entities.tax_tbl_CommercialCaseStatus
                                 where row.CCaseInstanceID == CCaseID
                                 && row.IsDeleted == false && row.CustomerID == CustId
                                 select row).OrderBy(entry => entry.ID).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objfp).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/Delete_CCStatusDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_CCStatusDetails(string ID, string DeletedBy)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    bool result = TaxationMethods.DeleteCCStatusDetails(Convert.ToInt32(ID), DeletedBy);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoCCDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCCDocumentList(int CCaseID)
        {
            try
            {
                List<tax_tbl_CommercialCaseFileData> objcases = new List<tax_tbl_CommercialCaseFileData>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_CommercialCaseFileData
                                where row.DocType == "CD"
                                && row.CommercialCaseInstanceID == CCaseID
                                && row.DocumentType != null
                                && row.IsDeleted == false
                                select row).OrderBy(entry => entry.DocType).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Delete_CCaseDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_CCaseDocuments(string FileID, string DeletedBy)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(FileID))
                {
                    bool result = TaxationMethods.DeleteCCaseDocuments(Convert.ToInt32(FileID), DeletedBy);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Delete_AuditObservationDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_AuditObservationDetails(string ID, string DeletedBy)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    bool result = TaxationMethods.DeleteAuditObservationDetails(Convert.ToInt32(ID), DeletedBy);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Delete_ActionTakenDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_ActionTakenDetails(string ID, string DeletedBy)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    bool result = TaxationMethods.DeleteActionTakenDetails(Convert.ToInt32(ID), DeletedBy);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoFPRFollowUpGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoFPRFollowUpGrid(int CustId)
        {
            try
            {
                List<tax_tbl_FPRFollowUp> objcases = new List<tax_tbl_FPRFollowUp>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_FPRFollowUp
                                where row.FPRFollowUp != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoBusinessPartnerGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoBusinessPartnerGrid(int CustId)
        {
            try
            {
                List<tax_tbl_BusinessPartner> objcases = new List<tax_tbl_BusinessPartner>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_BusinessPartner
                                where row.BusinessPartner != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.BusinessPartner).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoProductsGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoProductsGrid(int CustId)
        {
            try
            {
                List<tax_tbl_Products> objcases = new List<tax_tbl_Products>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_Products
                                where row.Products != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoFPRSectionGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoFPRSectionGrid(int CustId)
        {
            try
            {
                List<tax_tbl_FPRSection> objcases = new List<tax_tbl_FPRSection>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_FPRSection
                                where row.FPRSection != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoUploadCCDoc")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoUploadCCDoc(MultipartDataMediaFormatter.Infrastructure.FormData files, string CCaseID, int UserID, string DocType, int ResponseID, string DocumentType)
        {
            string Result = "";
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                string directoryPath = string.Empty;
                long ICaseIDConverted = Convert.ToInt64(CCaseID);

                tax_tbl_CommercialCaseFileData objCC = new tax_tbl_CommercialCaseFileData()
                {
                    CommercialCaseInstanceID = Convert.ToInt64(CCaseID),
                    CreatedBy = UserID,
                    IsDeleted = false,
                    DocType = DocType,
                    ResponseID = ResponseID,
                    DocumentType = DocumentType
                };

                objCC.FileName = files.Files[0].Value.FileName;

                //Get Document Version
                var caseDocVersion = TaxationMethods.ExistsCCaseDocumentReturnVersion(objCC);

                caseDocVersion++;
                objCC.Version = caseDocVersion + ".0";
                var url = ConfigurationManager.AppSettings["DocumentPathApp"];

                directoryPath = url + "Commercial Case/" + Convert.ToInt32(CCaseID) + "/" + objCC.Version;

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                Guid fileKey1 = Guid.NewGuid();
                string finalPath1 = System.IO.Path.Combine(directoryPath, fileKey1 + System.IO.Path.GetExtension(files.Files[0].Value.FileName));

                Byte[] bytes = files.Files[0].Value.Buffer;

                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                objCC.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                objCC.FileKey = fileKey1.ToString();
                objCC.VersionDate = DateTime.Now;
                objCC.CreatedOn = DateTime.Now;
                objCC.FileSize = Convert.ToInt64(100);
                Tax_SaveDocFiles(fileList);
                int FileID = TaxationMethods.CreateCCaseDocumentMappingGetID(objCC);
                if (FileID != 0)
                {
                    Result = "Successfully Added";
                    TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), UserID, "Case Documents uploaded", DateTime.Now.ToString("dd-MM-yyyy"));
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }


        [Route("Tax/Add_PHDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_PHDetails(string ICaseID, string HearingForum, string hearingDate, string HearingDescr, string NextHearingDate, string ReminderDate, int CustomerID, int UserID, int PHID, string ViewState)
        {

            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(ICaseID))
                {
                    result = TaxationMethods.AddPHDetails(ICaseID, HearingForum, hearingDate, HearingDescr, NextHearingDate, ReminderDate, CustomerID, UserID, PHID, ViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoUploadIndirectCaseHearingDocs")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoUploadIndirectCaseHearingDocs(MultipartDataMediaFormatter.Infrastructure.FormData files, string ICaseID, int UserID, int ForumId, int ResponseID)
        {
            string Result = "";
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                string directoryPath = string.Empty;
                long ICaseIDConverted = Convert.ToInt64(ICaseID);

                tax_tbl_TaxFileData objIC = new tax_tbl_TaxFileData()
                {
                    IndirectCaseInstanceID = ICaseIDConverted,
                    ForumID = ForumId,
                    IsDeleted = false,
                    EnType = "M",
                    ResponseID = ResponseID
                };

                objIC.FileName = files.Files[0].Value.FileName;

                //Get Document Version
                var caseDocVersion = TaxationMethods.ExistsICaseDocumentReturnVersion(objIC);

                caseDocVersion++;
                objIC.Version = caseDocVersion + ".0";
                var url = ConfigurationManager.AppSettings["DocumentPathApp"];

                directoryPath = url + "Indirect Case/" + Convert.ToInt32(ICaseID) + "/" + objIC.Version;

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                Guid fileKey1 = Guid.NewGuid();
                string finalPath1 = System.IO.Path.Combine(directoryPath, fileKey1 + System.IO.Path.GetExtension(files.Files[0].Value.FileName));

                Byte[] bytes = files.Files[0].Value.Buffer;

                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                objIC.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                objIC.FileKey = fileKey1.ToString();
                objIC.VersionDate = DateTime.Now;
                objIC.CreatedOn = DateTime.Now;
                objIC.FileSize = Convert.ToInt64(100);
                Tax_SaveDocFiles(fileList);
                int FileID = TaxationMethods.CreateICaseDocumentMappingGetID(objIC);
                if (FileID != 0)
                {
                    Result = "Successfully Added";
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }

        public static void Tax_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(System.IO.File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }


        [Route("Tax/KendoPhDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoPhDetails(int CustId, string ICaseID)
        {

            try
            {
                List<SP_ICHearingDetails_Result> objfp = new List<SP_ICHearingDetails_Result>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    if (ICaseID != "0" && ICaseID != null && ICaseID != "")
                    {
                        objfp = (from row in entities.SP_ICHearingDetails(CustId)
                                 select row).OrderBy(entry => entry.ID).ToList();
                        if (objfp.Count > 0 && ICaseID != "0" && ICaseID != null)
                        {
                            objfp = objfp.Where(x => x.IndirectCaseInstanceID == ICaseID).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objfp).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoProtestMoneyList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoProtestMoneyList(int CustId, string ICaseID, int ForumID)
        {
            try
            {
                List<SP_ICDepositDetails_Result> objcases = new List<SP_ICDepositDetails_Result>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.SP_ICDepositDetails(CustId)
                                where row.ICaseID == ICaseID
                                && row.ForumID == ForumID
                                select row).OrderByDescending(entry => entry.ForumID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/KendoForumPendingAt")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoForumPendingAt(int CustId)
        {
            try
            {
                List<tax_tbl_ForumPendingAt> objfp = new List<tax_tbl_ForumPendingAt>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objfp = (from row in entities.tax_tbl_ForumPendingAt
                             where row.ForumLocation != null && row.IsDeleted == false
                             && row.CustomerID == CustId
                             select row).OrderBy(row => row.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objfp).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoCategoryList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCategoryList(int CustId)
        {
            try
            {
                List<tax_tbl_TaxCategory> objcases = new List<tax_tbl_TaxCategory>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.tax_tbl_TaxCategory
                                where row.TaxCategory != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoWorkspace")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoWorkspace(int CustId, string BranchID, string Status, string CaseType, string Role, string ContingentLiability)
        {
            try
            {
                List<CombineWorkspace> objResult = new List<CombineWorkspace>();
                List<SP_IndirectCaseWorkspaceReport_Result> objIC = new List<SP_IndirectCaseWorkspaceReport_Result>();
                List<SP_CommercialCaseReportWithCustomParameter_Result> objCC = new List<SP_CommercialCaseReportWithCustomParameter_Result>();

                using (LitigationDataContainer entities = new LitigationDataContainer())
                {

                    if (CaseType == "5")
                    {
                        objIC = (from row in entities.SP_IndirectCaseWorkspaceReport(CustId)
                                 where row.ICaseNo != null
                                 select row).OrderByDescending(entry => entry.ICaseInstanceID).ToList();

                        if (ContingentLiability != "CL")
                        {
                            objIC = objIC.Where(x => x.CreatedForContingentLiability != "true").ToList();
                        }
                        else
                        {
                            objIC = objIC.Where(x => (x.ContingentAmount != "" && x.ContingentAmount != null && x.ContingentAmount != "0") || (x.PreDepositTotal != null && x.PreDepositTotal != 0) || (x.UnderProtestTotal != null && x.UnderProtestTotal != 0)).ToList();
                        }

                        if (BranchID != "[]" && !string.IsNullOrEmpty(BranchID))
                        {
                            if (Role == "EXCT")
                            {
                                System.Collections.Generic.IList<string> BranchIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(BranchID);
                                if (BranchIDs.Count != 0)
                                {

                                    objIC = objIC.Where(item => BranchIDs.Contains(item.CustomerBranchID)).ToList();
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(Status))
                        {
                            if (Status == "1")
                            {
                                objIC = objIC.Where(x => x.OCStatus == "Open").ToList();
                            }
                            else if (Status == "2")
                            {
                                objIC = objIC.Where(x => x.OCStatus == "Closed").ToList();
                            }
                            else if (Status == "3")
                            {
                                objIC = objIC.Where(x => x.OCStatus == "Merged").ToList();
                            }
                        }
                        if (objIC.Count > 0)
                        {
                            objResult.Add(new CombineWorkspace {IndirectCase = objIC, CommercialCase = objCC });
                        }
                    }
                    else
                    {
                        objCC = (from row in entities.SP_CommercialCaseReportWithCustomParameter(CustId)
                                 select row).OrderByDescending(entry => entry.CCaseInstanceID).ToList();
                        if (BranchID != "[]" && !string.IsNullOrEmpty(BranchID))
                        {
                            if (Role == "EXCT")
                            {
                                System.Collections.Generic.IList<string> BranchIDs = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(BranchID);
                                if (BranchIDs.Count != 0)
                                {

                                    objCC = objCC.Where(item => BranchIDs.Contains(item.CustomerBranchID)).ToList();
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(Status))
                        {
                            if (Status == "1")
                            {
                                objCC = objCC.Where(x => x.OCStatus == "Open").ToList();
                            }
                            else if (Status == "2")
                            {
                                objCC = objCC.Where(x => x.OCStatus == "Closed").ToList();
                            }
                        }
                        if (objCC.Count > 0)
                        {
                            objResult.Add(new CombineWorkspace {  IndirectCase = objIC, CommercialCase = objCC });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Add_ICaseDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_ICaseDetails(int ForumID, string OpenDate, string TaxCategory, string CategoryNature, string CustomerBranchID, string CaseType, string DemandPeriod,
            string Assessee, string AssesseeName, string ReasonCl, string WhetherPassthrough, string CustomerSCN, string CaseBrief, string DealingOfficerName, string DealingOfficerCPF,
            string HeadFinance, string ContingentAmount, string SAPNo, string ConsultantName, string ConsultantAddress, int CustomerID, int ICaseID, string IViewState, int UserID, string MergeCaseNos, string CreatedByCommercial, string CreatedBy)
        {
            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(CustomerBranchID))
                {
                    result = TaxationMethods.AddICaseDetails(ForumID, OpenDate, TaxCategory, CategoryNature, CustomerBranchID, CaseType, DemandPeriod, Assessee, AssesseeName, ReasonCl, WhetherPassthrough,
                     CustomerSCN, CaseBrief, DealingOfficerName, DealingOfficerCPF, HeadFinance, ContingentAmount, SAPNo, ConsultantName, ConsultantAddress, CustomerID, ICaseID, IViewState, UserID, MergeCaseNos, CreatedByCommercial, CreatedBy);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Add_ProtestMoney")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_ProtestMoney(string ICaseID, int CustomerID, string ViewState, string ID, int ForumID, string PreDepositAmount, string DatePreDepositAmount, string PreDepositRemarks, string UnderProtestAmount, string DateUnderProtestAmount, string AmtPaidProtestRemarks)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {

                if (!string.IsNullOrEmpty(ICaseID))
                {
                    bool result = TaxationMethods.AddProtestMoney(ForumID, PreDepositAmount, DatePreDepositAmount, PreDepositRemarks, UnderProtestAmount, DateUnderProtestAmount, AmtPaidProtestRemarks, ICaseID, CustomerID, 0, ViewState);

                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/Add_DetailsInDeptForum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_DetailsInDeptForum(string DIN_No, string DIN_Date, string IssuingAuthority, string DateRecieptSCN, string GroundsSCN,
             string Disputed_Tax_Amount, string Disputed_Penalty, string Disputed_Interest, string Disputed_Fine, string Total_Disputed_Amount, string Remarks,
             string OIONoBySCN, string OIODateBySCN, string txtOIOIssuingSCN, string OIOReceiptDate, string GroundforpassingOIOBydeptSCN, string drdSCNAppealFiledYN, string drpappealfilledwith,
             string ApealFilledDate, string RepliedfiledByDate, string RepliedfiledOnDate, string OngcDefence, string RepliedfiledWith, string OIOTaxAmount, string OIOPenalty, string OIOInterest,
             string OIOFine, string OIOTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                string result = "";
                if (ICaseID != 0)
                {
                    result = TaxationMethods.AddDetailsInDeptForum(DIN_No, DIN_Date, IssuingAuthority, DateRecieptSCN, GroundsSCN, Disputed_Tax_Amount, Disputed_Penalty,
                        Disputed_Interest, Disputed_Fine, Total_Disputed_Amount, Remarks, OIONoBySCN, OIODateBySCN, txtOIOIssuingSCN, OIOReceiptDate,
                        GroundforpassingOIOBydeptSCN, drdSCNAppealFiledYN, drpappealfilledwith, ApealFilledDate, RepliedfiledByDate, RepliedfiledOnDate, OngcDefence, RepliedfiledWith,
                        OIOTaxAmount, OIOPenalty, OIOInterest, OIOFine, OIOTotalAmount, CustomerID, ICaseID, IViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        [Route("Tax/Add_DetailsInAppellateForum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_DetailsInAppellateForum(string OIODINNo, string OIODINDate, string OIOIssuingAuthority, string OIOOrderReceiptDate,
         string OIOAppealFiledBy, string OIOAppealNo, string OIOAppealDate, string OIOGrounds, string OIADinNoByOIO, string OIADinDateByOIO, string OIAIssuingAuthByOIO,
         string OIADinReceiptDateByOIO, string drdAppealFiledYNByOIOYN, string OIOappealfiledwith, string OIOAppealfiledbydate, string OIATaxAmount, string OIAPenalty, string OIAInterest, string OIAFine, string OIATotalAmount,
         string OIOAppealTaxAmount, string OIOAppealInterest, string OIOAppealTaxPenalty, string OIOAppealFine, string OIOAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {

            try
            {
                string result = "";
                if (ICaseID != 0)
                {
                    result = TaxationMethods.AddDetailsInAppellateForum(OIODINNo, OIODINDate, OIOIssuingAuthority, OIOOrderReceiptDate, OIOAppealFiledBy, OIOAppealNo, OIOAppealDate,
                        OIOGrounds, OIADinNoByOIO, OIADinDateByOIO, OIAIssuingAuthByOIO, OIADinReceiptDateByOIO, drdAppealFiledYNByOIOYN, OIOappealfiledwith, OIOAppealfiledbydate, OIATaxAmount, OIAPenalty, OIAInterest,
                        OIAFine, OIATotalAmount, OIOAppealTaxAmount, OIOAppealInterest, OIOAppealTaxPenalty, OIOAppealFine, OIOAppealTotalAmount, CustomerID, ICaseID, IViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/Add_DetailsInTribunalForum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_DetailsInTribunalForum(string OIADINNo, string OIADINDate, string OIAIssuingAuthority, string OIAOrderReceiptDate,
               string OIAAppealFiledBy, string OIAAppealNo, string OIAAppealDate, string OIAGrounds, string HCDinNoByOIA, string HCDinDateByOIA, string HCIssuingAuthByOIA, string HCDinReceiptDateByOIA,
                string drdAppealFiledYNByOIAYN, string OIAappealfiledwith, string OIAAppealfiledbydate, string TribunalTaxAmount, string TribunalPenalty, string TribunalInterest, string TribunalFine,
                string TribunalTotalAmount, string TribunalAppealTaxAmount, string TribunalAppealInterest, string TribunalAppealTaxPenalty, string TribunalAppealFine, string TribunalAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                string result = "";
                if (ICaseID != 0)
                {
                    result = TaxationMethods.AddDetailsInTribunalForum(OIADINNo, OIADINDate, OIAIssuingAuthority, OIAOrderReceiptDate,
               OIAAppealFiledBy, OIAAppealNo, OIAAppealDate, OIAGrounds, HCDinNoByOIA, HCDinDateByOIA, HCIssuingAuthByOIA, HCDinReceiptDateByOIA,
               drdAppealFiledYNByOIAYN, OIAappealfiledwith, OIAAppealfiledbydate, TribunalTaxAmount, TribunalPenalty, TribunalInterest, TribunalFine, TribunalTotalAmount,
               TribunalAppealTaxAmount, TribunalAppealInterest, TribunalAppealTaxPenalty, TribunalAppealFine, TribunalAppealTotalAmount, CustomerID, ICaseID, IViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/Add_DetailsInHCForum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_DetailsInHCForum(string HCDINNo, string HCDINDate, string HCIssuingAuthority, string HCOrderReceiptDate,
              string HCAppealFiledBy, string HCAppealNo, string HCAppealDate, string HCGrounds, string SCDinNoByHC, string SCDinDateByHC, string SCIssuingAuthByHC, string SCDinReceiptDateByHC,
               string drdAppealFiledByHCYN, string HCappealfiledwith, string HCAppealfiledbydate, string HCTaxAmount, string HCPenalty, string HCInterest, string HCFine, string HCTotalAmount,
               string HCAppealTaxAmount, string HCAppealInterest, string HCAppealTaxPenalty, string HCAppealFine, string HCAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                string result = "";
                if (ICaseID != 0)
                {
                    result = TaxationMethods.AddDetailsInHCForum(HCDINNo, HCDINDate, HCIssuingAuthority, HCOrderReceiptDate,
               HCAppealFiledBy, HCAppealNo, HCAppealDate, HCGrounds, SCDinNoByHC, SCDinDateByHC, SCIssuingAuthByHC, SCDinReceiptDateByHC,
               drdAppealFiledByHCYN, HCappealfiledwith, HCAppealfiledbydate, HCTaxAmount, HCPenalty, HCInterest, HCFine, HCTotalAmount,
               HCAppealTaxAmount, HCAppealInterest, HCAppealTaxPenalty, HCAppealFine, HCAppealTotalAmount, CustomerID, ICaseID, IViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/Add_DetailsInSCForum")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_DetailsInSCForum(string SCDINNo, string SCDINDate, string SCIssuingAuthority, string SCOrderReceiptDate,
            string SCAppealFiledBy, string SCAppealNo, string SCAppealDate, string SCGrounds, string SCOrderNo, string dateSCOrder, string SCTaxAmount, string SCPenalty, string SCInterest, string SCFine, string SCTotalAmount,
            string SCAppealTaxAmount, string SCAppealInterest, string SCAppealTaxPenalty, string SCAppealFine, string SCAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                string result = "";
                if (ICaseID != 0)
                {
                    result = TaxationMethods.AddDetailsInSCForum(SCDINNo, SCDINDate, SCIssuingAuthority, SCOrderReceiptDate, SCAppealFiledBy, SCAppealNo, SCAppealDate,
                      SCGrounds, SCOrderNo, dateSCOrder, SCTaxAmount, SCPenalty, SCInterest, SCFine, SCTotalAmount, SCAppealTaxAmount, SCAppealInterest, SCAppealTaxPenalty, SCAppealFine, SCAppealTotalAmount, CustomerID, ICaseID, IViewState);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }



        [Route("Tax/Add_ActionTakenDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_ActionTakenDetails(string dateCaseUpdated, string Discussion, string Suggession, string CCaseID, int CustomerID, string ViewState, int ID, string CCaseNo, int CreatedBy)
        {

            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(CCaseID))
                {
                    result = TaxationMethods.AddActionTakenDetails(dateCaseUpdated, Discussion, Suggession, CCaseID, CustomerID, ViewState, ID, CCaseNo, CreatedBy);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        [Route("Tax/Add_CCaseDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_CCaseDetails(string ForumID, string Forum, string ForumOthers, string Location, string FPR, string FPRFollow, string BusinessPartner, string CustomerCode, string BPCategory, string Product, string CaseYear,
        string PeriodFrom, string PeriodTo, string CaseBrief, string PersonIncharge, string DealingOfficer, string ConcernedDirectorDD, string DisputeDetails, string PresentStatus, string Refrences,
        string AccountedForCurrent, string NotAccountedForCurrent, string TotalCurrent, string InterestNature, string MGONature, string Othersnature,
        string TotalNature, string ProvisionCreated, string ProvisionAmount, string SAPDocNos, string AmountRecovered, string InitialDisputedAmt,
        string DisputePendingAgainstForum, string CaseLasthearingDate, string CaseFilingDate, string CaseNexthearingDate, string PresentStatusCase,
        int CustomerID, int CCaseID, string IViewState, int UserID, string UpdatedBy)
        {
            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(Location))
                {
                    result = TaxationMethods.AddCCaseDetails(ForumID, Forum, ForumOthers, Location, FPR, FPRFollow, BusinessPartner, CustomerCode, BPCategory, Product, CaseYear, PeriodFrom, PeriodTo, CaseBrief, PersonIncharge,
                        DealingOfficer, ConcernedDirectorDD, DisputeDetails, PresentStatus, Refrences, AccountedForCurrent, NotAccountedForCurrent, TotalCurrent, InterestNature,
                        MGONature, Othersnature, TotalNature, ProvisionCreated, ProvisionAmount, SAPDocNos, AmountRecovered, InitialDisputedAmt,
                        DisputePendingAgainstForum, CaseLasthearingDate, CaseFilingDate, CaseNexthearingDate, PresentStatusCase, CustomerID, CCaseID, IViewState, UserID, UpdatedBy);
                }

                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoAuditObserReplyDocDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoAuditObserReplyDocDetails(int CustId, string CCaseID, int ID, string Type)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var objcases = (from row in entities.SP_CCAuditObservation(CustId)
                                    where row.CCaseID == CCaseID && row.ID == ID
                                    select new { Name = row.AuditObserFileName, Name2 = row.AuditReplyFileName, ID = row.ID }).ToList();
                    if (Type == "AuditObser")
                    {
                        objcases = objcases.Where(x => x.Name != "").ToList();
                    }
                    else
                    {
                        objcases = objcases.Where(x => x.Name2 != "").ToList();
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoActionDocDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoActionDocDetails(int CustId, string CCaseID, int ID)
        {
            try
            {
                List<SP_CCActionTakenDetails_Result> objfp = new List<SP_CCActionTakenDetails_Result>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    if (!string.IsNullOrEmpty(CCaseID) && CCaseID != "0")
                    {
                        objfp = (from row in entities.SP_CCActionTakenDetails(CustId)
                                 where row.ID == ID && row.FileName != ""
                                 select row).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objfp).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoAuditObservation")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoAuditObservation(int CustId, string CCaseID)
        {
            try
            {
                List<SP_CCAuditObservation_Result> objcases = new List<SP_CCAuditObservation_Result>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.SP_CCAuditObservation(CustId)
                                where row.CCaseID == CCaseID
                                select row).OrderByDescending(entry => entry.ObservationRecordDate).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Tax/KendoActionTaken")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoActionTaken(int CustId, string CCaseID)
        {
            try
            {
                List<SP_CCActionTakenDetails_Result> objcases = new List<SP_CCActionTakenDetails_Result>();
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objcases = (from row in entities.SP_CCActionTakenDetails(CustId)
                                where row.CCaseID == CCaseID
                                select row).OrderByDescending(entry => entry.dateCaseUpdated).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Tax/Add_AuditObservationDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_AuditObservationDetails(string AuditObserveYN, string ObservationRecordDate, string AuditType, string AuditTypeOthers, string StatusAuditObservartion, string CCaseID, int CustomerID, string ViewState, int ID, string CCaseNo, int CreatedBy)
        {

            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(CCaseID))
                {
                    result = TaxationMethods.AddAuditObservationDetails(AuditObserveYN, ObservationRecordDate, AuditType, AuditTypeOthers, StatusAuditObservartion, CCaseID, CustomerID, ViewState, ID, CCaseNo, CreatedBy);
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

    }
}