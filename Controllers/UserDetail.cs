﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Controllers
{
    public class SearchDetailOutput
    {
        public List<SP_DailyUpdateNewsLetter_Result> detailView { get; set; }
        public int count { get; set; }
    }
    public class SearchDetail
    {
        public string ActID { get; set; }
        public int pagenumber { get; set; }
        public string ComplianceType { get; set; }
        public string ComplianceCategory { get; set; }
        public string State { get; set; }
    }

    public class AllSearchDetail
    {
        public string Email { get; set; }
        public string ActID { get; set; }       
        public int pagenumber { get; set; }
        public string Filter { get; set; }
    }

    public class UserDetail
    {
        public string Email { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string States { get; set; }
        public string ActName { get; set; }
        public string Filter { get; set; }
        public int pagenumber { get; set; }
        public string CategoryId { get; set; }
    }
    public class MyFavouriteDetails
    {
        public string Email { get; set; }
        public long ItemID { get; set; }
        public string CType { get; set; }
        public bool IsMyFavourite { get; set; }
    }
    public class DeviceDetails
    {
        public string Email { get; set; }
        public string DeviceIDIMEI { get; set; }
        public string DeviceToken { get; set; }
        public string flag { get; set; }
    }
    public class GetUserInfo
    {
        public string UserInfo { get; set; }
    }
}