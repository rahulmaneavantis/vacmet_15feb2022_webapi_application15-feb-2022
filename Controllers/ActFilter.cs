﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Controllers
{
    public class ActFilter
    {
        public string Email { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string SearchValue { get; set; }
    }
}