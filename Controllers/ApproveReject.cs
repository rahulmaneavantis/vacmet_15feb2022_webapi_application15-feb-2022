﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppWebApplication.Controllers
{
    public class ApproveReject
    {
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("condition")]
        public string condition { get; set; }

        [JsonProperty("StatusID")]
        public string StatusID { get; set; }

        [JsonProperty("ScheduledOnID")]
        public string ScheduledOnID { get; set; }

        [JsonProperty("ComplianceInstanceID")]
        public string ComplianceInstanceID { get; set; }

        [JsonProperty("statusChangeOnDate")]
        public string statusChangeOnDate { get; set; }

        [JsonProperty("remark")]
        public string remark { get; set; }
        
        [JsonProperty("txtInterestReview")]
        public string txtInterestReview { get; set; }

        [JsonProperty("txtPenaltyReview")]
        public string txtPenaltyReview { get; set; }

        [JsonProperty("txtValueAsPerSystem")]
        public string txtValueAsPerSystem { get; set; }

        [JsonProperty("txtValueAsPerReturn")]
        public string txtValueAsPerReturn { get; set; }

        [JsonProperty("txtLiabilityPaid")]
        public string txtLiabilityPaid { get; set; }

        [JsonProperty("lblNatureofcomplianceID")]
        public string lblNatureofcomplianceID { get; set; }

        [JsonProperty("chkPenaltySaveReviewchk")]
        public string chkPenaltySaveReviewchk { get; set; }


    }
}

