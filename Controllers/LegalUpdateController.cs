﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Configuration;
using System.Reflection;
using AppWebApplication.Models;
using System.Web.Security;
using System.Net.Http.Formatting;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Globalization;
using System.IO;
using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business;
using Nest;
using System.Web;
using StackExchange.Redis;

namespace AppWebApplication.Controllers
{
    public class LegalUpdateController : ApiController
    {
        public static int WrongOTPCount = 0;
        public static string key = "avantis";

        #region  ALL And Assigned Acts
        //[Authorize]
        [Route("LegalUpdate/GetLegalUpdateAssignedActCounts")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAssignedActCounts([FromBody]UserDetail UserDetailObj)
        {
            List<Count> cnt = new List<Count>();
            try
            {
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                string categoryname = string.Empty;                

                List<CategorywiseCount> actcountbj = new List<CategorywiseCount>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (UserDetailObj.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = UserDetailObj.pagenumber;
                    }
                    int pagesize = page * 15;
                    int from = pagesize - 15;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            List<int> ids = (from row in entities.Research_ActAssignment
                                             where row.UserId == user.ID
                                             select (int)row.ActId).ToList();

                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var actsettings = new ConnectionSettings(node);
                            actsettings.DefaultIndex("actdataindex")
                                       .BasicAuthentication(eusername, epassword)
                                       .RequestTimeout(TimeSpan.FromMinutes(1));
                            actsettings.DisableDirectStreaming();
                            actsettings.PrettyJson().DisableDirectStreaming();
                            var actclient = new ElasticClient(actsettings);

                            ISearchResponse<ComplianceManagement.ActListNew> actsearchresults = null;
                            if (ids.Count > 0)
                            {
                                actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                                .Query(q => q
                                .Terms(b => b
                                .Field(p => p.actid)
                                .Terms<int>(ids)))
                                .From(from) // apply paging
                                .Size(2000)); // limit to page size
                            }
                            SearchObject searchobj = new SearchObject();
                            MainRootObject objmainrootobject = new MainRootObject();
                            using (MemoryStream mStream = new MemoryStream())
                            {
                                if (actsearchresults != null)
                                {
                                    actclient.Serializer.Serialize(actsearchresults, mStream);
                                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                    search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                                    objmainrootobject.rootobject1 = item;
                                    searchobj.actlist = objmainrootobject.rootobject1.hits.hits;

                                    int Count = searchobj.actlist.Count();

                                    cnt.Add(new Count { CountValue = Count });
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(cnt).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }


        //[Authorize]
        [Route("LegalUpdate/GetLegalUpdateAssignedActCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAssignedActCount([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                string categoryname = string.Empty;
                int categorycount = 0;

                List<CategorywiseCount> actcountbj = new List<CategorywiseCount>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (UserDetailObj.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = UserDetailObj.pagenumber;
                    }
                    int pagesize = page * 15;
                    int from = pagesize - 15;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            List<long> ids = (from row in entities.Research_ActAssignment
                                             where row.UserId == user.ID
                                              && row.IsActive == false
                                              select (long)row.ActId).ToList();

                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var actsettings = new ConnectionSettings(node);
                            actsettings.DefaultIndex("actdataindex")
                                       .BasicAuthentication(eusername, epassword)
                                       .RequestTimeout(TimeSpan.FromMinutes(1));
                            actsettings.DisableDirectStreaming();
                            actsettings.PrettyJson().DisableDirectStreaming();
                            var actclient = new ElasticClient(actsettings);


                            ISearchResponse<ComplianceManagement.ActListNew> actsearchresults = null;
                            if (ids.Count > 0)
                            {
                                actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                                .Query(q => q
                                .Terms(b => b
                                .Field(p => p.actid)
                                .Terms<long>(ids)))
                                .From(from) // apply paging
                                .Size(2000)); // limit to page size
                            }
                            SearchObject searchobj = new SearchObject();
                            MainRootObject objmainrootobject = new MainRootObject();
                            using (MemoryStream mStream = new MemoryStream())
                            {
                                if (actsearchresults != null)
                                {
                                    actclient.Serializer.Serialize(actsearchresults, mStream);
                                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                    search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                                    objmainrootobject.rootobject1 = item;
                                    searchobj.actlist = objmainrootobject.rootobject1.hits.hits;
                                    var categorylist = searchobj.actlist.Select(entry => entry._source.compliancecategoryname).Distinct().ToList();
                                    if (categorylist != null)
                                    {
                                        foreach (var catitem in categorylist)
                                        {
                                            categoryname = "";
                                            categorycount = 0;
                                            categoryname = catitem;
                                            categorycount = searchobj.actlist.Where(entry => entry._source.compliancecategoryname.ToUpper().Trim() == catitem.ToUpper().Trim()).ToList().Count();
                                            actcountbj.Add(new CategorywiseCount
                                            {
                                                CategoryName = categoryname.ToString(),
                                                CategoryCount = categorycount
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(actcountbj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        //[Authorize]
        [Route("LegalUpdate/GetLegalUpdateAssignedActs")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAssignedActs([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                int userID = -1;
                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);
                var actsettings = new ConnectionSettings(node);
                actsettings.DefaultIndex("actdataindex")
                           .BasicAuthentication(eusername, epassword)
                           .RequestTimeout(TimeSpan.FromMinutes(1));
                actsettings.DisableDirectStreaming();
                actsettings.PrettyJson().DisableDirectStreaming();
                var actclient = new ElasticClient(actsettings);

                
                ISearchResponse<ComplianceManagement.ActListNew> actsearchresults = null;
                SearchObject searchobj = new SearchObject();
                MainRootObject objmainrootobject = new MainRootObject();
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (UserDetailObj.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = UserDetailObj.pagenumber;
                    }
                    int pagesize = page * 10;
                    int from = pagesize - 10;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {
                            List<int> ids = (from row in entities.Research_ActAssignment
                                             where row.UserId == userID 
                                             && row.IsActive==false
                                             select (int)row.ActId).ToList();

                            List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                                where row.UserID == userID && row.CType == "Act"
                                                && row.IsMyFavourite == false
                                                select (int)row.ItemID).ToList();

                            if (ids.Count > 0)
                            {                               
                                var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ActListNew>, QueryContainer>>();

                                // Assigned Acts
                                if (ids.Any())
                                {
                                    filters.Add(fq => fq.Terms(t => t.Field(f => f.actid).Terms(ids)));
                                }
                                if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                                {
                                    if (UserDetailObj.Category.Any())
                                    {
                                        filters.Add(fq => fq.Terms(t => t.Field(f => f.compliancecategoryname.Trim().ToLower()).Terms(UserDetailObj.Category.ToLower().Split(' '))));
                                    }
                                }
                                 if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                                {
                                    if (UserDetailObj.Type.Any())
                                    {
                                        filters.Add(fq => fq.Terms(t => t.Field(f => f.compliancetypename.Trim().ToLower()).Terms(UserDetailObj.Type.ToLower().Split(' '))));
                                    }
                                }
                                 if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                                {
                                    if (UserDetailObj.States.Any())
                                    {
                                        filters.Add(fq => fq.Terms(t => t.Field(f => f.state.Trim().ToLower()).Terms(UserDetailObj.States.ToLower().Split(' '))));
                                    }
                                }
                                 if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                                {
                                    if (UserDetailObj.ActName.Any())
                                    {
                                        filters.Add(fq => fq.Terms(t => t.Field(f => f.actname.Trim().ToLower()).Terms(UserDetailObj.ActName.ToLower().Split(' '))));
                                    }
                                }
                                 if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                                {
                                    string[] str = null;
                                    str = UserDetailObj.Filter.Trim().ToLower().Split(' ');
                                    if (str.Any())
                                    {
                                        filters.Add(fq => fq.Terms(t => t.Field(f => f.actname.Trim().ToLower()).Terms(str)));
                                    }
                                }
                               

                                actsearchresults =
                                actclient.Search<ComplianceManagement.ActListNew>(x => x.Query(q => q
                                .Bool(bq => bq.Filter(filters)))
                                .From(from) // apply paging
                                .Size(10) // limit to page size
                                );
                                                                     
                                if (actsearchresults != null)
                                {
                                    using (MemoryStream mStream = new MemoryStream())
                                    {
                                        actclient.Serializer.Serialize(actsearchresults, mStream);
                                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                        search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                                        objmainrootobject.rootobject1 = item;
                                        searchobj.actlist = objmainrootobject.rootobject1.hits.hits;

                                    }
                                    List<search.Hit> masterlist = new List<search.Hit>();
                                    masterlist = objmainrootobject.rootobject1.hits.hits;

                                    //// searchobj.actlist = objmainrootobject.rootobject1.hits.hits.Where(x => x._source.State !=null && x._source.State.Contains(UserDetailObj.States)).ToList();
                                    //if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                                    //{
                                    //    masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
                                    //}
                                    //if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                                    //{
                                    //    masterlist = masterlist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
                                    //}
                                    //if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                                    //{
                                    //    masterlist = masterlist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
                                    //}
                                    //if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                                    //{
                                    //    masterlist = masterlist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
                                    //}
                                    //if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                                    //{
                                    //    masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
                                    //                   || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
                                    //                   || x._source.compliancetypename.Contains(UserDetailObj.Filter)
                                    //                   || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
                                    //                   ).ToList();

                                    //}
                                    foreach (var item in masterlist.Where(w => actids.Contains(w._source.actid)))
                                    {
                                        item._source.IsMyFavourite = 0;
                                    }
                                    searchobj.actlist = masterlist.ToList();

                                }// actsearchresults != null end
                            }//ids.Count end
                        }//userID != -1 end
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(searchobj.actlist).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        [Route("LegalUpdate/GetLegalUpdateAllActs")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAllActs([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();
                int userID = -1;
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (UserDetailObj.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = UserDetailObj.pagenumber;
                    }
                    int pagesize = page * 10;
                    int from = pagesize - 10;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {
                            List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                                where row.UserID == userID && row.CType == "Act"
                                                && row.IsMyFavourite == false
                                                select (int)row.ItemID).ToList();


                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var actsettings = new ConnectionSettings(node);
                            actsettings.DefaultIndex("actdataindex")
                                       .BasicAuthentication(eusername, epassword)
                                       .RequestTimeout(TimeSpan.FromMinutes(1));
                            actsettings.DisableDirectStreaming();
                            actsettings.PrettyJson().DisableDirectStreaming();
                            var actclient = new ElasticClient(actsettings);                        

                            #region ActQueryResult
                            ISearchResponse<ComplianceManagement.ActListNew> actsearchresults;
                            actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                            .Query(q => q.MatchAll())
                            .From(from) // apply paging
                            .Size(10) // limit to page size
                            );

                            if (actsearchresults != null)
                            {
                                using (MemoryStream mStream = new MemoryStream())
                                {
                                    actclient.Serializer.Serialize(actsearchresults, mStream);
                                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                    search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                                    objmainrootobject.rootobject1 = item;
                                }
                            }
                            #endregion


                            #region ComplianceQueryResult
                            var compliancesettings = new ConnectionSettings(node);
                            compliancesettings.DefaultIndex("compliancedataindex");
                            compliancesettings.DisableDirectStreaming();
                            compliancesettings.PrettyJson().DisableDirectStreaming();
                            var complianceclient = new ElasticClient(compliancesettings);
                            ISearchResponse<ComplianceManagement.ComplianceListNew> compliancesearchresults;

                            compliancesearchresults = complianceclient.Search<ComplianceManagement.ComplianceListNew>(s => s
                            .From(from) // apply paging
                            .Size(10) // limit to page size
                            .Query(q => q.MatchAll()));

                            if (compliancesearchresults != null)
                            {
                                using (MemoryStream mStream = new MemoryStream())
                                {
                                    complianceclient.Serializer.Serialize(compliancesearchresults, mStream);
                                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                    SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                                    objmainrootobject.rootobject2 = item;
                                }
                            }
                            #endregion

                            using (MemoryStream mStream = new MemoryStream())
                            {
                                complianceclient.Serializer.Serialize(objmainrootobject, mStream);
                                var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                MainRootObject item = JsonConvert.DeserializeObject<MainRootObject>(rawQueryText);
                                searchobj.Compliacelist = item.rootobject2.hits.hits;


                                List<search.Hit> masterlist = new List<search.Hit>();
                                masterlist = objmainrootobject.rootobject1.hits.hits;

                                if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                                {
                                    masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
                                }
                                if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                                {
                                    masterlist = masterlist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
                                }
                                if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                                {
                                    masterlist = masterlist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
                                }
                                if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                                {
                                    masterlist = masterlist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
                                }
                                if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                                {
                                    masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
                                    || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
                                                                || x._source.compliancetypename.Contains(UserDetailObj.Filter)
                                                                || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
                                                                ).ToList();
                                }
                                foreach (var lstitem in masterlist.Where(w => actids.Contains(w._source.actid)))
                                {
                                    lstitem._source.IsMyFavourite = 0;
                                }
                                searchobj.actlist = masterlist.ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        #endregion

        #region Daily Update
       // [Authorize]
        [Route("LegalUpdate/GetLegalUpdateDailyupdateData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateDailyupdateData([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                MainRootObject objmainrootobject = new MainRootObject();
                SearchDailyUpdateObject searchobj = new SearchDailyUpdateObject();
                List<DailyUpdateSearch.Hit> masterAlllist = new List<DailyUpdateSearch.Hit>();
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (UserDetailObj.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = UserDetailObj.pagenumber;
                    }
                    int pagesize = page * 10;
                    int from = pagesize - 10;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int userID = Convert.ToInt32((from row in entities.Users
                                                      where (row.Email == Email || row.Email == EmailIOS)
                                                        && row.IsActive == true
                                                        && row.IsDeleted == false
                                                      select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {

                            List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                                where row.UserID == userID && row.CType == "DailyUpdate"
                                                && row.IsMyFavourite == false
                                                select (int)row.ItemID).ToList();

                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var dailyupdatesettings = new ConnectionSettings(node);
                            dailyupdatesettings.DefaultIndex("dailyupdatesindexwithparam")
                                               .BasicAuthentication(eusername, epassword)
                                               .RequestTimeout(TimeSpan.FromMinutes(1));
                            dailyupdatesettings.DisableDirectStreaming();
                            dailyupdatesettings.PrettyJson().DisableDirectStreaming();
                            var dailyupdateclient = new ElasticClient(dailyupdatesettings);


                            ISearchResponse<ComplianceManagement.DailyUpdatesListParameter> dailyupdatesearchresults;

                            if (!string.IsNullOrEmpty(UserDetailObj.Category))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                              .From(from) // apply paging
                                              .Size(10) // limit to page size
                                              .Query(q => q // define query
                                              .MultiMatch(mp => mp // of type MultiMatch
                                              .Query(UserDetailObj.Category) // pass text
                                              .Fields(f => f // define fields to search against
                                              .Fields(f1 => f1.compliancecategoryname))))
                                              .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));

                            }
                            else if (!string.IsNullOrEmpty(UserDetailObj.CategoryId))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                           .From(from) // apply paging
                                           .Size(10) // limit to page size
                                           .Query(q => q // define query
                                           .MultiMatch(mp => mp // of type MultiMatch
                                           .Query(UserDetailObj.CategoryId) // pass text
                                           .Fields(f => f // define fields to search against
                                           .Fields(f1 => f1.compliancecategoryid))))
                                           .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
                            }
                            else if (!string.IsNullOrEmpty(UserDetailObj.Type))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                             .From(from) // apply paging
                                             .Size(10) // limit to page size
                                             .Query(q => q // define query
                                             .MultiMatch(mp => mp // of type MultiMatch
                                             .Query(UserDetailObj.Type) // pass text
                                             .Fields(f => f // define fields to search against
                                             .Fields(f1 => f1.compliancetypename))))
                                             .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));

                            }
                            else if (!string.IsNullOrEmpty(UserDetailObj.States))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                            .From(from) // apply paging
                                            .Size(10) // limit to page size
                                            .Query(q => q // define query
                                            .MultiMatch(mp => mp // of type MultiMatch
                                            .Query(UserDetailObj.States) // pass text
                                            .Fields(f => f // define fields to search against
                                            .Fields(f1 => f1.state))))
                                            .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
                            }
                            else if (!string.IsNullOrEmpty(UserDetailObj.ActName))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                          .From(from) // apply paging
                                          .Size(10) // limit to page size
                                          .Query(q => q // define query
                                          .MultiMatch(mp => mp // of type MultiMatch
                                          .Query(UserDetailObj.ActName) // pass text
                                          .Fields(f => f // define fields to search against
                                          .Fields(f1 => f1.actname))))
                                          .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
                            }
                            else if (!string.IsNullOrEmpty(UserDetailObj.Filter))
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                          .From(from) // apply paging
                                          .Size(10) // limit to page size
                                          .Query(q => q // define query
                                          .MultiMatch(mp => mp // of type MultiMatch
                                          .Query(UserDetailObj.Filter) // pass text
                                          .Fields(f => f // define fields to search against
                                          .Fields(f1 => f1.actname, f2 => f2.compliancecategoryname, f3 => f3.compliancetypename, f4 => f4.state, f5 => f5.title, f6 => f6.dailyupdateid))))
                                          .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
                            }
                            else
                            {
                                dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                                        .Query(q => q.MatchAll())
                                                        .From(from) // apply paging
                                                        .Size(10) // limit to page size     
                                                        .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
                            }
                                                      
                            if (dailyupdatesearchresults != null)
                            {
                                using (MemoryStream mStream = new MemoryStream())
                                {
                                    dailyupdateclient.Serializer.Serialize(dailyupdatesearchresults, mStream);
                                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                    DailyUpdateSearch.RootObject item = JsonConvert.DeserializeObject<DailyUpdateSearch.RootObject>(rawQueryText);
                                    objmainrootobject.rootobject3 = item;
                                }
                            }

                            masterAlllist = objmainrootobject.rootobject3.hits.hits;
                            if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                            {
                                masterAlllist = masterAlllist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
                            }
                            if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                            {                               
                                masterAlllist = masterAlllist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
                            }
                            if (!(string.IsNullOrEmpty(UserDetailObj.CategoryId)))
                            {
                                masterAlllist = masterAlllist.Where(x => x._source.compliancecategoryid == Convert.ToInt32(UserDetailObj.CategoryId)).ToList();                                
                            }
                            if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                            {
                                masterAlllist = masterAlllist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
                            }
                            if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                            {
                                masterAlllist = masterAlllist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
                            }
                            if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                            {
                                masterAlllist = masterAlllist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
                                                                || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
                                                                || x._source.title.Contains(UserDetailObj.Filter)
                                                                || x._source.compliancetypename.Contains(UserDetailObj.Filter)
                                                                || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
                                                                || (Convert.ToString(x._source.dailyupdateid) != null && Convert.ToString(x._source.dailyupdateid).Contains(UserDetailObj.Filter))
                                                                ).ToList();
                            }
                            foreach (var item in masterAlllist.Where(w => actids.Contains(w._source.dailyupdateid)))
                            {
                                item._source.ismyfavourite = 0;
                            }
                            var today = masterAlllist.Where(entry => entry._source.createddate.Date == DateTime.Today.Date).ToList();
                            DateTime EndDate = DateTime.Today.Date;
                            DateTime FromDate = DateTime.Today.AddDays(-7);
                            var weekly = masterAlllist.Where(entry => entry._source.createddate.Date >= FromDate.Date && entry._source.createddate.Date.Date <= EndDate).ToList();

                            DateTime EndDatem = DateTime.Today.Date;
                            DateTime FromDatem = DateTime.Today.AddDays(-30);

                            var monthly = masterAlllist.Where(entry => entry._source.createddate.Date >= FromDatem.Date && entry._source.createddate.Date.Date <= EndDatem).ToList();
                            searchobj.Alllist = masterAlllist.ToList();
                            searchobj.Todaylist = today.ToList();
                            searchobj.Weeklylist = weekly.ToList();
                            searchobj.Monthlylist = monthly.ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //public HttpResponseMessage GetLegalUpdateDailyupdateData([FromBody]UserDetail UserDetailObj)
        //{
        //    try
        //    {
        //        MainRootObject objmainrootobject = new MainRootObject();
        //        SearchDailyUpdateObject searchobj = new SearchDailyUpdateObject();
        //        List<DailyUpdateSearch.Hit> masterAlllist = new List<DailyUpdateSearch.Hit>();
        //        string Email = string.Empty;
        //        if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
        //        {
        //            Email = UserDetailObj.Email.ToString();
        //        }
        //        Email = Email.Replace(" ", "+");
        //        var encryptedBytes = Convert.FromBase64String(Email);
        //        try
        //        {
        //            Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
        //        }
        //        catch { }
        //        string EmailIOS = string.Empty;
        //        try
        //        {
        //            EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
        //        }
        //        catch { }
        //        if (!string.IsNullOrEmpty(Email))
        //        {
        //            int page = 0;
        //            if (UserDetailObj.pagenumber == 0)
        //            {
        //                page = 1;
        //            }
        //            else
        //            {
        //                page = UserDetailObj.pagenumber;
        //            }
        //            int pagesize = page * 10;
        //            int from = pagesize - 10;
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                int userID = Convert.ToInt32((from row in entities.Users
        //                                              where (row.Email == Email || row.Email == EmailIOS)
        //                                                && row.IsActive == true
        //                                                && row.IsDeleted == false
        //                                              select row.ID).FirstOrDefault());

        //                if (userID != -1)
        //                {

        //                    List<int> actids = (from row in entities.LegalUpdates_MyFavorites
        //                                        where row.UserID == userID && row.CType == "DailyUpdate"
        //                                        && row.IsMyFavourite == false
        //                                        select (int)row.ItemID).ToList();

        //                    string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
        //                    var node = new Uri(elasticsearchconnectionstring);
        //                    var dailyupdatesettings = new ConnectionSettings(node);
        //                    dailyupdatesettings.DefaultIndex("dailyupdatesindexwithparam");
        //                    dailyupdatesettings.DisableDirectStreaming();
        //                    dailyupdatesettings.PrettyJson().DisableDirectStreaming();
        //                    var dailyupdateclient = new ElasticClient(dailyupdatesettings);

        //                    ISearchResponse<ComplianceManagement.DailyUpdatesListParameter> dailyupdatesearchresults;

        //                    if (!string.IsNullOrEmpty(UserDetailObj.Category))
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                      .From(from) // apply paging
        //                                      .Size(10) // limit to page size
        //                                      .Query(q => q // define query
        //                                      .MultiMatch(mp => mp // of type MultiMatch
        //                                      .Query(UserDetailObj.Category) // pass text
        //                                      .Fields(f => f // define fields to search against
        //                                      .Fields(f1 => f1.compliancecategoryname))))
        //                                      .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
        //                    }
        //                    else if (!string.IsNullOrEmpty(UserDetailObj.Type))
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                     .From(from) // apply paging
        //                                     .Size(10) // limit to page size
        //                                     .Query(q => q // define query
        //                                     .MultiMatch(mp => mp // of type MultiMatch
        //                                     .Query(UserDetailObj.Type) // pass text
        //                                     .Fields(f => f // define fields to search against
        //                                     .Fields(f1 => f1.compliancetypename))))
        //                                     .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));

        //                    }
        //                    else if (!string.IsNullOrEmpty(UserDetailObj.States))
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                    .From(from) // apply paging
        //                                    .Size(10) // limit to page size
        //                                    .Query(q => q // define query
        //                                    .MultiMatch(mp => mp // of type MultiMatch
        //                                    .Query(UserDetailObj.States) // pass text
        //                                    .Fields(f => f // define fields to search against
        //                                    .Fields(f1 => f1.state))))
        //                                    .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
        //                    }
        //                    else if (!string.IsNullOrEmpty(UserDetailObj.ActName))
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                  .From(from) // apply paging
        //                                  .Size(10) // limit to page size
        //                                  .Query(q => q // define query
        //                                  .MultiMatch(mp => mp // of type MultiMatch
        //                                  .Query(UserDetailObj.ActName) // pass text
        //                                  .Fields(f => f // define fields to search against
        //                                  .Fields(f1 => f1.actname))))
        //                                  .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
        //                    }
        //                    else if (!string.IsNullOrEmpty(UserDetailObj.Filter))
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                  .From(from) // apply paging
        //                                  .Size(10) // limit to page size
        //                                  .Query(q => q // define query
        //                                  .MultiMatch(mp => mp // of type MultiMatch
        //                                  .Query(UserDetailObj.Filter) // pass text
        //                                  .Fields(f => f // define fields to search against
        //                                  .Fields(f1 => f1.actname, f2 => f2.compliancecategoryname, f3 => f3.compliancetypename, f4 => f4.state, f5 => f5.title))))
        //                                  .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));
        //                    }
        //                    else
        //                    {
        //                        dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
        //                                                .Query(q => q.MatchAll())
        //                                                .From(from) // apply paging
        //                                                .Size(10) // limit to page size     
        //                                                .Sort(ss => ss.Descending(p => p.intdaliyupdatedate))); 
        //                    }

        //                    if (dailyupdatesearchresults != null)
        //                    {
        //                        using (MemoryStream mStream = new MemoryStream())
        //                        {
        //                            dailyupdateclient.Serializer.Serialize(dailyupdatesearchresults, mStream);
        //                            var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
        //                            DailyUpdateSearch.RootObject item = JsonConvert.DeserializeObject<DailyUpdateSearch.RootObject>(rawQueryText);
        //                            objmainrootobject.rootobject3 = item;
        //                        }
        //                    }

        //                    masterAlllist = objmainrootobject.rootobject3.hits.hits;
        //                    if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
        //                    {
        //                        masterAlllist = masterAlllist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
        //                    }
        //                    if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
        //                    {
        //                        masterAlllist = masterAlllist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
        //                    }
        //                    if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
        //                    {
        //                        masterAlllist = masterAlllist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
        //                    }
        //                    if (!(string.IsNullOrEmpty(UserDetailObj.States)))
        //                    {
        //                        masterAlllist = masterAlllist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
        //                    }
        //                    if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
        //                    {
        //                        masterAlllist = masterAlllist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
        //                            || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
        //                               || x._source.title.Contains(UserDetailObj.Filter)
        //                                                        || x._source.compliancetypename.Contains(UserDetailObj.Filter)
        //                                                        || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
        //                                                        ).ToList();
        //                    }
        //                    foreach (var item in masterAlllist.Where(w => actids.Contains(w._source.dailyupdateid)))
        //                    {
        //                        item._source.ismyfavourite = 0;
        //                    }
        //                    var today = masterAlllist.Where(entry => entry._source.createddate.Date == DateTime.Today.Date).ToList();
        //                    DateTime EndDate = DateTime.Today.Date;
        //                    DateTime FromDate = DateTime.Today.AddDays(-7);
        //                    var weekly = masterAlllist.Where(entry => entry._source.createddate.Date >= FromDate.Date && entry._source.createddate.Date.Date <= EndDate).ToList();

        //                    DateTime EndDatem = DateTime.Today.Date;
        //                    DateTime FromDatem = DateTime.Today.AddDays(-30);

        //                    var monthly = masterAlllist.Where(entry => entry._source.createddate.Date >= FromDatem.Date && entry._source.createddate.Date.Date <= EndDatem).ToList();
        //                    searchobj.Alllist = masterAlllist.ToList();
        //                    searchobj.Todaylist = today.ToList();
        //                    searchobj.Weeklylist = weekly.ToList();
        //                    searchobj.Monthlylist = monthly.ToList();
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LogLibrary.WriteErrorLog(ex.Message);
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}
        #endregion

        #region Compliance
        [Authorize]
        [Route("LegalUpdate/GetElasticSearchCompliance")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetElasticSearchCompliance(string searchKey, int pagenumber)
        {
            try
            {
                int page = 0;
                if (pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;

                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);
                var settings = new ConnectionSettings(node);
                settings.DefaultIndex("compliancedataindex")
                        .BasicAuthentication(eusername, epassword)
                        .RequestTimeout(TimeSpan.FromMinutes(1));
                settings.DisableDirectStreaming();
                settings.PrettyJson().DisableDirectStreaming();
                var client = new ElasticClient(settings);





                #region ComplianceQueryResult
                ISearchResponse<ComplianceManagement.ComplianceListNew> searchResults;
                if (string.IsNullOrEmpty(searchKey))
                {
                    searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                      .Query(q => q.MatchAll())
                       .From(from) // apply paging
                       .Size(10) // limit to page size
                      );
                }
                else
                {
                    searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                       .Query(q => q
                       .Bool(b => b
                       .Should(
                               bs => bs.Match(r => r.Field("actid").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("shortdescription").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("referencematerialtext").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("sections").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("penaltydescription").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("description").Query(searchKey.Trim())),
                               bs => bs.Match(r => r.Field("sampleformlink").Query(searchKey.Trim()))
                              )
                           )
                        )
                       .From(from) // apply paging
                       .Size(10) // limit to page size
                     );
                }
                #endregion

                List<SearchCompliance.Hit> listobj = new List<SearchCompliance.Hit>();
                using (MemoryStream mStream = new MemoryStream())
                {
                    client.Serializer.Serialize(searchResults, mStream);
                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                    SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                    listobj = item.hits.hits;
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(listobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("LegalUpdate/GetElasticSearchComplianceActID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetElasticSearchComplianceActID(string searchKey, int pagenumber)
        {
            try
            {

                int page = 0;
                if (pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;

                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);
                var settings = new ConnectionSettings(node);
                settings.DefaultIndex("compliancedataindex")
                        .BasicAuthentication(eusername, epassword)
                        .RequestTimeout(TimeSpan.FromMinutes(1));
                settings.DisableDirectStreaming();
                settings.PrettyJson().DisableDirectStreaming();
                var client = new ElasticClient(settings);



                #region ComplianceQueryResult
                ISearchResponse<ComplianceManagement.ComplianceListNew> searchResults;
                if (string.IsNullOrEmpty(searchKey))
                {
                    searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                      .Query(q => q.MatchAll())
                       .From(from) // apply paging
                       .Size(10) // limit to page size
                      );
                }
                else
                {
                    searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                       .Query(q => q
                       .Bool(b => b
                       .Should(
                               bs => bs.Match(r => r.Field("actid").Query(searchKey.Trim()))
                              )
                           )
                        )
                        .From(from) // apply paging
                        .Size(10) // limit to page size
                     );
                }
                #endregion

                List<SearchCompliance.Hit> listobj = new List<SearchCompliance.Hit>();
                using (MemoryStream mStream = new MemoryStream())
                {
                    client.Serializer.Serialize(searchResults, mStream);
                    var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                    SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                    listobj = item.hits.hits;
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(listobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("LegalUpdate/GetElasticSearchComplianceWithActID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetElasticSearchComplianceWithActID([FromBody]AllSearchDetail allsearchdetail)
        {
            try
            {
                List<SearchCompliance.Hit> masterlistobj = new List<SearchCompliance.Hit>();
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(allsearchdetail.Email)))
                {
                    Email = allsearchdetail.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (allsearchdetail.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = allsearchdetail.pagenumber;
                    }
                    int pagesize = page * 10;
                    int from = pagesize - 10;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int userID = Convert.ToInt32((from row in entities.Users
                                                      where (row.Email == Email || row.Email == EmailIOS)
                                                        && row.IsActive == true
                                                        && row.IsDeleted == false
                                                      select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {

                            List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                                where row.UserID == userID && row.CType == "Compliance"
                                                && row.IsMyFavourite == false
                                                select (int)row.ItemID).ToList();

                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var settings = new ConnectionSettings(node);
                            settings.DefaultIndex("compliancedataindex")
                                    .BasicAuthentication(eusername, epassword)
                                    .RequestTimeout(TimeSpan.FromMinutes(1));
                            settings.DisableDirectStreaming();
                            settings.PrettyJson().DisableDirectStreaming();
                            var client = new ElasticClient(settings);




                            #region ComplianceQueryResult
                            ISearchResponse<ComplianceManagement.ComplianceListNew> searchResults;
                            if (!(string.IsNullOrEmpty(allsearchdetail.ActID)) && !string.IsNullOrEmpty(allsearchdetail.Filter))
                            {
                                string[] str = null;
                                str = allsearchdetail.Filter.Trim().ToLower().Split(' ');
                                var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ComplianceListNew>, QueryContainer>>();
                                if (allsearchdetail.ActID.Any())
                                {
                                    filters.Add(fq => fq.Terms(t => t.Field(f => f.actid).Terms(allsearchdetail.ActID)));
                                }
                                if (str.Any())
                                {
                                    filters.Add(fq => fq.Terms(t => t.Field(f => f.shortdescription.Trim().ToLower()).Terms(str)));
                                }

                                searchResults =
                                     client.Search<ComplianceManagement.ComplianceListNew>(x => x.Query(q => q
                                     .Bool(bq => bq.Filter(filters))));
                            }
                            else if (!(string.IsNullOrEmpty(allsearchdetail.ActID)))
                            {
                                searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                                            .From(from) // apply paging
                                            .Size(10) // limit to page size
                                            .Query(q => q
                                                .Match(m => m
                                                    .Field(f => f.actid)
                                                    .Query(allsearchdetail.ActID)
                                                )
                                            ));
                            }
                            else
                            {
                                searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                                  .Query(q => q.MatchAll())
                                   .From(from) // apply paging
                                   .Size(10) // limit to page size
                                  );
                            }
                            #endregion

                            using (MemoryStream mStream = new MemoryStream())
                            {
                                client.Serializer.Serialize(searchResults, mStream);
                                var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                                masterlistobj = item.hits.hits;
                            }
                            foreach (var item in masterlistobj.Where(w => actids.Contains(w._source.complianceid)))
                            {
                                item._source.ismyfavourite = 0;
                            }
                            //if (!(string.IsNullOrEmpty(allsearchdetail.Filter)))
                            //{                    
                            //    masterlistobj = masterlistobj.Where(x => x._source.shortdescription.Contains(allsearchdetail.Filter)
                            //                        || x._source.sections.Contains(allsearchdetail.Filter)
                            //                        || x._source.compliancesampleform.Contains(allsearchdetail.Filter)                                        
                            //                        ).ToList();
                            //}       
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(masterlistobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("LegalUpdate/GetLegalUpdateAllData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAllData([FromBody]UserDetail UserDetailObj)
        {
            try
            {




                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();
                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);

                int page = 0;
                if (UserDetailObj.pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = UserDetailObj.pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;

                #region Act
                var actsettings = new ConnectionSettings(node);
                actsettings.DefaultIndex("actdataindex")
                            .BasicAuthentication(eusername, epassword)
                            .RequestTimeout(TimeSpan.FromMinutes(1));
                actsettings.DisableDirectStreaming();
                actsettings.PrettyJson().DisableDirectStreaming();
                var actclient = new ElasticClient(actsettings);
                ISearchResponse<ComplianceManagement.ActListNew> actsearchresults;


                if (!string.IsNullOrEmpty(UserDetailObj.Filter))
                {
                    string[] str = null;
                    str = UserDetailObj.Filter.Trim().ToLower().Split(' ');
                    var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ActListNew>, QueryContainer>>();
                    if (str.Any())
                    {
                        filters.Add(fq => fq.Terms(t => t.Field(f => f.actname.Trim().ToLower()).Terms(str)));
                    }
                    actsearchresults =
                         actclient.Search<ComplianceManagement.ActListNew>(x => x.Query(q => q
                         .Bool(bq => bq.Filter(filters))));
                }
                else
                {
                    actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                    .Query(q => q.MatchAll())
                    .From(from) // apply paging
                    .Size(10) // limit to page size
                    );
                }

                if (actsearchresults != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        actclient.Serializer.Serialize(actsearchresults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                        objmainrootobject.rootobject1 = item;
                    }
                }

                List<search.Hit> masterActHitlist = new List<search.Hit>();
                masterActHitlist = objmainrootobject.rootobject1.hits.hits;

                if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                {
                    masterActHitlist = masterActHitlist.Where(x => x._source.actname.Trim().ToLower().Contains(UserDetailObj.ActName.Trim().ToLower())).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                {
                    masterActHitlist = masterActHitlist.Where(x => x._source.compliancecategoryname.Trim().ToLower().Contains(UserDetailObj.Category.Trim().ToLower())).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                {
                    masterActHitlist = masterActHitlist.Where(x => x._source.compliancetypename.Trim().ToLower().Contains(UserDetailObj.Type.Trim().ToLower())).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                {
                    masterActHitlist = masterActHitlist.Where(x => x._source.state != null && x._source.state.Trim().ToLower().Contains(UserDetailObj.States.Trim().ToLower())).ToList();
                }
                //if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                //{
                //    masterActHitlist = masterActHitlist.Where(x => x._source.actname.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //        || x._source.compliancecategoryname.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //                                    || x._source.compliancetypename.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //                                    || (x._source.state != null && x._source.state.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower()))
                //                                    ).ToList();
                //}
                #endregion

                #region Compliance
                var compliancesettings = new ConnectionSettings(node);
                compliancesettings.DefaultIndex("compliancedataindex")
                                .BasicAuthentication(eusername, epassword)
                                .RequestTimeout(TimeSpan.FromMinutes(1));
                compliancesettings.DisableDirectStreaming();
                compliancesettings.PrettyJson().DisableDirectStreaming();
                var complianceclient = new ElasticClient(compliancesettings);
                ISearchResponse<ComplianceManagement.ComplianceListNew> compliancesearchresults;
                if (!string.IsNullOrEmpty(UserDetailObj.Filter))
                {
                    string[] str = null;
                    str = UserDetailObj.Filter.Trim().ToLower().Split(' ');
                    var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ComplianceListNew>, QueryContainer>>();
                    if (str.Any())
                    {
                        filters.Add(fq => fq.Terms(t => t.Field(f => f.shortdescription.Trim().ToLower()).Terms(str)));
                    }
                    compliancesearchresults =
                         complianceclient.Search<ComplianceManagement.ComplianceListNew>(x => x.Query(q => q
                         .Bool(bq => bq.Filter(filters))));
                }
                else
                {
                    compliancesearchresults = complianceclient.Search<ComplianceManagement.ComplianceListNew>(s => s
                                            .Query(q => q.MatchAll())
                                            .From(from) // apply paging
                                            .Size(10) // limit to page size
                                            );
                }
                if (compliancesearchresults != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        complianceclient.Serializer.Serialize(compliancesearchresults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                        objmainrootobject.rootobject2 = item;
                    }
                }

                List<SearchCompliance.Hit> masterComplianceHitlist = new List<SearchCompliance.Hit>();
                masterComplianceHitlist = objmainrootobject.rootobject2.hits.hits;
                //if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                //{
                //    masterComplianceHitlist = masterComplianceHitlist.Where(x => x._source.description.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //    || x._source.compliancesampleform.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //                                                            || x._source.shortdescription.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //                                                            || x._source.sections.Trim().ToLower().Contains(UserDetailObj.Filter.Trim().ToLower())
                //                                                            ).ToList();
                //}
                #endregion

                #region DailyUpdate
                var dailyupdatesettings = new ConnectionSettings(node);
                dailyupdatesettings.DefaultIndex("dailyupdatesindexwithparam")
                                    .BasicAuthentication(eusername, epassword)
                                    .RequestTimeout(TimeSpan.FromMinutes(1));
                dailyupdatesettings.DisableDirectStreaming();
                dailyupdatesettings.PrettyJson().DisableDirectStreaming();
                var dailyupdateclient = new ElasticClient(dailyupdatesettings);
                ISearchResponse<ComplianceManagement.DailyUpdatesListParameter> dailyupdatesearchresults;

                if (!string.IsNullOrEmpty(UserDetailObj.Filter))
                {
                    string[] str = null;
                    str = UserDetailObj.Filter.Trim().ToLower().Split(' ');
                    var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.DailyUpdatesListParameter>, QueryContainer>>();
                    if (str.Any())
                    {
                        filters.Add(fq => fq.Terms(t => t.Field(f => f.title.Trim().ToLower()).Terms(str)));
                    }
                    dailyupdatesearchresults =
                         dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(x => x.Query(q => q
                         .Bool(bq => bq.Filter(filters))));
                }
                else
                {
                    dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                                .Query(q => q.MatchAll())
                                                .From(from) // apply paging
                                                .Size(10) // limit to page size
                                                 );
                }
                if (dailyupdatesearchresults != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        dailyupdateclient.Serializer.Serialize(dailyupdatesearchresults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        DailyUpdateSearch.RootObject item = JsonConvert.DeserializeObject<DailyUpdateSearch.RootObject>(rawQueryText);
                        objmainrootobject.rootobject3 = item;
                    }
                }

                List<DailyUpdateSearch.Hit> masterdailyupdateHitlist = new List<DailyUpdateSearch.Hit>();
                masterdailyupdateHitlist = objmainrootobject.rootobject3.hits.hits;

                if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                {
                    masterdailyupdateHitlist = masterdailyupdateHitlist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                {
                    masterdailyupdateHitlist = masterdailyupdateHitlist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                {
                    masterdailyupdateHitlist = masterdailyupdateHitlist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
                }
                if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                {
                    masterdailyupdateHitlist = masterdailyupdateHitlist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
                }
                //if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                //{
                //    masterdailyupdateHitlist = masterdailyupdateHitlist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
                //        || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
                //                                    || x._source.compliancetypename.Contains(UserDetailObj.Filter)
                //                                    || x._source.title.Contains(UserDetailObj.Filter)
                //                                    || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
                //                                    ).ToList();
                //}
                #endregion


                searchobj.actlist = masterActHitlist.ToList();
                searchobj.Compliacelist = masterComplianceHitlist.ToList();
                searchobj.DailyUpdateActlist = masterdailyupdateHitlist.ToList();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("LegalUpdate/GetComplianceDetailByComplainceId")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetComplianceDetailByComplainceId([FromBody]AllSearchDetail allsearchdetail)
        {
            try
            {
                List<SearchCompliance.Hit> masterlistobj = new List<SearchCompliance.Hit>();
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(allsearchdetail.Email)))
                {
                    Email = allsearchdetail.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    int page = 0;
                    if (allsearchdetail.pagenumber == 0)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = allsearchdetail.pagenumber;
                    }
                    int pagesize = page * 10;
                    int from = pagesize - 10;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int userID = Convert.ToInt32((from row in entities.Users
                                                      where (row.Email == Email || row.Email == EmailIOS)
                                                        && row.IsActive == true
                                                        && row.IsDeleted == false
                                                      select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {

                            List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                                where row.UserID == userID && row.CType == "Compliance"
                                                && row.IsMyFavourite == false
                                                select (int)row.ItemID).ToList();

                            string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                            string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                            string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                            var node = new Uri(elasticsearchconnectionstring);
                            var settings = new ConnectionSettings(node);
                            settings.DefaultIndex("compliancedataindex")
                                    .BasicAuthentication(eusername, epassword)
                                    .RequestTimeout(TimeSpan.FromMinutes(1));
                            settings.DisableDirectStreaming();
                            settings.PrettyJson().DisableDirectStreaming();
                            var client = new ElasticClient(settings);

                            #region ComplianceQueryResult
                            ISearchResponse<ComplianceManagement.ComplianceListNew> searchResults;
                            if (!string.IsNullOrEmpty(allsearchdetail.Filter))
                            {
                                string[] str = null;
                                str = allsearchdetail.Filter.Trim().ToLower().Split(' ');
                                var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ComplianceListNew>, QueryContainer>>();
                                if (str.Any())
                                {
                                    filters.Add(fq => fq.Terms(t => t.Field(f => f.complianceid).Terms(str)));
                                }
                                searchResults =
                                     client.Search<ComplianceManagement.ComplianceListNew>(x => x.Query(q => q
                                     .Bool(bq => bq.Filter(filters))));
                            }
                            else
                            {
                                searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                                  .Query(q => q.MatchAll())
                                   .From(from) // apply paging
                                   .Size(10) // limit to page size
                                  );
                            }
                            #endregion

                            using (MemoryStream mStream = new MemoryStream())
                            {
                                client.Serializer.Serialize(searchResults, mStream);
                                var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                                SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                                masterlistobj = item.hits.hits;
                            }
                            foreach (var item in masterlistobj.Where(w => actids.Contains(w._source.complianceid)))
                            {
                                item._source.ismyfavourite = 0;
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(masterlistobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        #endregion

        #region NewsLetter
        [Route("LegalUpdate/GetLegalUpdateNewsletterData")]
        public HttpResponseMessage GetLegalUpdateNewsletterData(int ID, int pagenumber)
        {
            try
            {

                int page = 0;
                if (pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;

                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();
                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);
                var newslettersettings = new ConnectionSettings(node);
                newslettersettings.DefaultIndex("newsletterindex")
                                   .BasicAuthentication(eusername, epassword)
                                   .RequestTimeout(TimeSpan.FromMinutes(1));
                newslettersettings.DisableDirectStreaming();
                newslettersettings.PrettyJson().DisableDirectStreaming();
                var newsletterclient = new ElasticClient(newslettersettings);

                ISearchResponse<ComplianceManagement.NewsLetterListNew> newslettersearchresults;
                newslettersearchresults = newsletterclient.Search<ComplianceManagement.NewsLetterListNew>(s => s
                .Query(q => q.MatchAll())
                .From(from) // apply paging
                .Size(10) // limit to page size
                .Sort(ss => ss.Descending(p => p.intnewsletterdate)));

                if (newslettersearchresults != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        newsletterclient.Serializer.Serialize(newslettersearchresults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        NewsLetterSearch.RootObject item = JsonConvert.DeserializeObject<NewsLetterSearch.RootObject>(rawQueryText);
                        objmainrootobject.rootobject4 = item;
                    }
                }

                List<NewsLetterSearch.Hit> masterHitlist = new List<NewsLetterSearch.Hit>();
                masterHitlist = objmainrootobject.rootobject4.hits.hits;

                if (ID != -1)
                {
                    masterHitlist = masterHitlist.Where(x => x._source.ID == ID).ToList();
                }
                searchobj.actlist = new List<search.Hit>();
                searchobj.Compliacelist = new List<SearchCompliance.Hit>();
                searchobj.DailyUpdateActlist = new List<DailyUpdateSearch.Hit>();
                searchobj.NewsLetterlist = masterHitlist.ToList();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("LegalUpdate/GetLegalUpdateAllNewsletterData")]
        public HttpResponseMessage GetLegalUpdateAllNewsletterData(string searchKey, int pagenumber)
        {
            try
            {

                int page = 0;
                if (pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;
                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();
                string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                var node = new Uri(elasticsearchconnectionstring);
                var newslettersettings = new ConnectionSettings(node);
                newslettersettings.DefaultIndex("newsletterindex")
                                  .BasicAuthentication(eusername, epassword)
                                  .RequestTimeout(TimeSpan.FromMinutes(1));
                newslettersettings.DisableDirectStreaming();
                newslettersettings.PrettyJson().DisableDirectStreaming();
                var newsletterclient = new ElasticClient(newslettersettings);

                ISearchResponse<ComplianceManagement.NewsLetterListNew> newslettersearchresults;
                newslettersearchresults = newsletterclient.Search<ComplianceManagement.NewsLetterListNew>(s => s
              .Query(q => q.MatchAll())
              .From(from) // apply paging
              .Size(10) // limit to page size
              .Sort(ss => ss.Descending(p => p.intnewsletterdate)));

                if (newslettersearchresults != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        newsletterclient.Serializer.Serialize(newslettersearchresults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        NewsLetterSearch.RootObject item = JsonConvert.DeserializeObject<NewsLetterSearch.RootObject>(rawQueryText);
                        objmainrootobject.rootobject4 = item;
                    }
                }
                List<NewsLetterSearch.Hit> masterHitlist = new List<NewsLetterSearch.Hit>();
                masterHitlist = objmainrootobject.rootobject4.hits.hits;
                if (!(string.IsNullOrEmpty(searchKey)))
                {
                    masterHitlist = masterHitlist.Where(x => x._source.Title.Contains(searchKey)
                                   || x._source.Description.Contains(searchKey)
                                   || x._source.FileName.Contains(searchKey)
                                   || x._source.DocFileName.Contains(searchKey)
                                   ).ToList();
                    masterHitlist = masterHitlist.Where(entry => entry._source.CreatedDate != null).ToList();
                    masterHitlist = masterHitlist.OrderByDescending(x => x._source.CreatedDate).ToList();
                }

                searchobj.actlist = new List<search.Hit>();
                searchobj.Compliacelist = new List<SearchCompliance.Hit>();
                searchobj.DailyUpdateActlist = new List<DailyUpdateSearch.Hit>();
                searchobj.NewsLetterlist = masterHitlist.ToList();
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #region MyFavorite
        public static long CreateLegalUpdatesMyFavorite(LegalUpdates_MyFavorites objLUMF)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.LegalUpdates_MyFavorites.Add(objLUMF);
                    entities.SaveChanges();
                }

                return objLUMF.ID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static bool ExistsLegalUpdatesMyFavorite(LegalUpdates_MyFavorites objLUMF)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LegalUpdates_MyFavorites
                             where row.UserID == objLUMF.UserID
                                && row.ItemID == objLUMF.ItemID
                                && row.CType.ToUpper().Trim().Equals(objLUMF.CType.ToUpper().Trim())
                                && row.IsDelete == false
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool UpdateVendorData(LegalUpdates_MyFavorites objLUMF)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    LegalUpdates_MyFavorites ObjQuery = (from row in entities.LegalUpdates_MyFavorites
                                                         where row.UserID == objLUMF.UserID
                                                         && row.ItemID == objLUMF.ItemID
                                                         && row.CType.ToUpper().Trim().Equals(objLUMF.CType.ToUpper().Trim())
                                                         && row.IsDelete == false
                                                         select row).FirstOrDefault();
                    if (ObjQuery != null)
                    {
                        ObjQuery.IsMyFavourite = objLUMF.IsMyFavourite;
                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }

        [Authorize]
        [Route("LegalUpdate/GetLegalUpdateMyFavouriteAddOrRemove")]
        [System.Web.Http.AcceptVerbs("POST")]
        public HttpResponseMessage GetLegalUpdateMyFavouriteAddOrRemove([FromBody]MyFavouriteDetails FavouriteDetailsObj)
        {
            string sucess = string.Empty;
            try
            {
                int Userid = -1;
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(FavouriteDetailsObj.Email)))
                {
                    Email = FavouriteDetailsObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(FavouriteDetailsObj.CType) && FavouriteDetailsObj.ItemID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Userid = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());

                        LegalUpdates_MyFavorites objlumyf = new LegalUpdates_MyFavorites()
                        {
                            UserID = Userid,
                            ItemID = FavouriteDetailsObj.ItemID,
                            CType = FavouriteDetailsObj.CType,
                            IsMyFavourite = FavouriteDetailsObj.IsMyFavourite,
                            IsDelete = false
                        };
                        if (ExistsLegalUpdatesMyFavorite(objlumyf))
                        {
                            CreateLegalUpdatesMyFavorite(objlumyf);
                            sucess = "Done";
                        }
                        else
                        {
                            UpdateVendorData(objlumyf);
                            sucess = "Update";
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sucess);
            }
            catch (Exception ex)
            {
                sucess = "Error";
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Request.CreateResponse(HttpStatusCode.OK, sucess);
            }
        }


        //[Authorize]
        [Route("LegalUpdate/GetLegalUpdateMyFavouriteDetails")]
        [System.Web.Http.AcceptVerbs("POST")]
        public HttpResponseMessage GetLegalUpdateMyFavouriteDetails([FromBody]MyFavouriteDetails FavouriteDetailsObj)
        {
            string sucess = string.Empty;
            try
            {
                List<Sp_GetLegalUpdateMyFavouriteDetails_Result> AllComData = new List<Sp_GetLegalUpdateMyFavouriteDetails_Result>();
                int Userid = -1;
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(FavouriteDetailsObj.Email)))
                {
                    Email = FavouriteDetailsObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Userid = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());


                        AllComData = ComplianceManagement.GetMyFavouriteDetails(Userid);
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(AllComData).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                sucess = "Error";
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Request.CreateResponse(HttpStatusCode.OK, sucess);
            }
        }
        #endregion


        #region created for ashwani 

        #region 31 Jan 2019
        [Route("LegalUpdate/GetElasticSearchComplianceWithAllActID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetElasticSearchComplianceWithAllActID([FromBody]AllSearchDetail allsearchdetail)
        {
            try
            {
                List<SearchCompliance.Hit> masterlistobj = new List<SearchCompliance.Hit>();

                int page = 0;
                if (allsearchdetail.pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = allsearchdetail.pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                        where row.CType == "Compliance"
                                        && row.IsMyFavourite == false
                                        select (int)row.ItemID).ToList();//row.UserID == userID &&

                    string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                    string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                    string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                    var node = new Uri(elasticsearchconnectionstring);
                    var settings = new ConnectionSettings(node);
                    settings.DefaultIndex("compliancedataindex")
                            .BasicAuthentication(eusername, epassword)
                            .RequestTimeout(TimeSpan.FromMinutes(1));
                    settings.DisableDirectStreaming();
                    settings.PrettyJson().DisableDirectStreaming();
                    var client = new ElasticClient(settings);

                    #region ComplianceQueryResult
                    ISearchResponse<ComplianceManagement.ComplianceListNew> searchResults;
                    if (!(string.IsNullOrEmpty(allsearchdetail.ActID)) && !string.IsNullOrEmpty(allsearchdetail.Filter))
                    {
                        string[] str = null;
                        str = allsearchdetail.Filter.Trim().ToLower().Split(' ');
                        var filters = new List<Func<QueryContainerDescriptor<ComplianceManagement.ComplianceListNew>, QueryContainer>>();
                        if (allsearchdetail.ActID.Any())
                        {
                            filters.Add(fq => fq.Terms(t => t.Field(f => f.actid).Terms(allsearchdetail.ActID)));
                        }
                        if (str.Any())
                        {
                            filters.Add(fq => fq.Terms(t => t.Field(f => f.shortdescription.Trim().ToLower()).Terms(str)));
                        }

                        searchResults =
                             client.Search<ComplianceManagement.ComplianceListNew>(x => x.Query(q => q
                             .Bool(bq => bq.Filter(filters))));
                    }
                    else if (!(string.IsNullOrEmpty(allsearchdetail.ActID)))
                    {
                        //     searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                        //  .Query(q => q
                        //  .Bool(b => b
                        //  .Should(
                        //          bs => bs.Match(r => r.Field("actid").Query(allsearchdetail.ActID.Trim()))
                        //         )
                        //      )
                        //   )
                        //  .From(from) // apply paging
                        //  .Size(10) // limit to page size
                        //);
                        searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                                    .From(from) // apply paging
                                    .Size(10) // limit to page size
                                    .Query(q => q
                                        .Match(m => m
                                            .Field(f => f.actid)
                                            .Query(allsearchdetail.ActID)
                                        )
                                    ));
                    }
                    else
                    {
                        searchResults = client.Search<ComplianceManagement.ComplianceListNew>(s => s
                          .Query(q => q.MatchAll())
                           .From(from) // apply paging
                           .Size(10) // limit to page size
                          );
                    }
                    #endregion

                    using (MemoryStream mStream = new MemoryStream())
                    {
                        client.Serializer.Serialize(searchResults, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                        masterlistobj = item.hits.hits;
                    }
                    foreach (var item in masterlistobj.Where(w => actids.Contains(w._source.complianceid)))
                    {
                        item._source.ismyfavourite = 0;
                    }
                    //if (!(string.IsNullOrEmpty(allsearchdetail.Filter)))
                    //{                    
                    //    masterlistobj = masterlistobj.Where(x => x._source.shortdescription.Contains(allsearchdetail.Filter)
                    //                        || x._source.sections.Contains(allsearchdetail.Filter)
                    //                        || x._source.compliancesampleform.Contains(allsearchdetail.Filter)                                        
                    //                        ).ToList();
                    //}       

                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(masterlistobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("LegalUpdate/GetLegalUpdateAllActsNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAllActsNew([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();

                int page = 0;
                if (UserDetailObj.pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = UserDetailObj.pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<int> actids = (from row in entities.Acts
                                        where row.IsDeleted == false
                                        select (int)row.ID).ToList();//row.UserID == userID 


                    string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                    string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                    string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                    var node = new Uri(elasticsearchconnectionstring);
                    var actsettings = new ConnectionSettings(node);
                    actsettings.DefaultIndex("actdataindex")
                                .BasicAuthentication(eusername, epassword)
                                .RequestTimeout(TimeSpan.FromMinutes(1));
                    actsettings.DisableDirectStreaming();
                    actsettings.PrettyJson().DisableDirectStreaming();
                    var actclient = new ElasticClient(actsettings);
                    #region ActQueryResult
                    ISearchResponse<ComplianceManagement.ActListNew> actsearchresults;
                    actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                    .Query(q => q.MatchAll())
                    .From(from) // apply paging
                    .Size(10) // limit to page size
                    );

                    if (actsearchresults != null)
                    {
                        using (MemoryStream mStream = new MemoryStream())
                        {
                            actclient.Serializer.Serialize(actsearchresults, mStream);
                            var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                            search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                            objmainrootobject.rootobject1 = item;
                        }
                    }
                    #endregion


                    #region ComplianceQueryResult
                    var compliancesettings = new ConnectionSettings(node);
                    compliancesettings.DefaultIndex("compliancedataindex");
                    compliancesettings.DisableDirectStreaming();
                    compliancesettings.PrettyJson().DisableDirectStreaming();
                    var complianceclient = new ElasticClient(compliancesettings);
                    ISearchResponse<ComplianceManagement.ComplianceListNew> compliancesearchresults;

                    compliancesearchresults = complianceclient.Search<ComplianceManagement.ComplianceListNew>(s => s
                    .From(from) // apply paging
                    .Size(10) // limit to page size
                    .Query(q => q.MatchAll()));

                    if (compliancesearchresults != null)
                    {
                        using (MemoryStream mStream = new MemoryStream())
                        {
                            complianceclient.Serializer.Serialize(compliancesearchresults, mStream);
                            var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                            SearchCompliance.RootObject item = JsonConvert.DeserializeObject<SearchCompliance.RootObject>(rawQueryText);
                            objmainrootobject.rootobject2 = item;
                        }
                    }
                    #endregion

                    using (MemoryStream mStream = new MemoryStream())
                    {
                        complianceclient.Serializer.Serialize(objmainrootobject, mStream);
                        var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                        MainRootObject item = JsonConvert.DeserializeObject<MainRootObject>(rawQueryText);
                        searchobj.Compliacelist = item.rootobject2.hits.hits;


                        List<search.Hit> masterlist = new List<search.Hit>();
                        masterlist = objmainrootobject.rootobject1.hits.hits;

                        if (!(string.IsNullOrEmpty(UserDetailObj.ActName)))
                        {
                            masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.ActName)).ToList();
                        }
                        if (!(string.IsNullOrEmpty(UserDetailObj.Category)))
                        {
                            masterlist = masterlist.Where(x => x._source.compliancecategoryname.Contains(UserDetailObj.Category)).ToList();
                        }
                        if (!(string.IsNullOrEmpty(UserDetailObj.Type)))
                        {
                            masterlist = masterlist.Where(x => x._source.compliancetypename.Contains(UserDetailObj.Type)).ToList();
                        }
                        if (!(string.IsNullOrEmpty(UserDetailObj.States)))
                        {
                            masterlist = masterlist.Where(x => x._source.state != null && x._source.state.Contains(UserDetailObj.States)).ToList();
                        }
                        if (!(string.IsNullOrEmpty(UserDetailObj.Filter)))
                        {
                            masterlist = masterlist.Where(x => x._source.actname.Contains(UserDetailObj.Filter)
                            || x._source.compliancecategoryname.Contains(UserDetailObj.Filter)
                                                        || x._source.compliancetypename.Contains(UserDetailObj.Filter)
                                                        || (x._source.state != null && x._source.state.Contains(UserDetailObj.Filter))
                                                        ).ToList();
                        }
                        foreach (var lstitem in masterlist.Where(w => actids.Contains(w._source.actid)))
                        {
                            lstitem._source.IsMyFavourite = 0;
                        }
                        searchobj.actlist = masterlist.ToList();
                    }

                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion


        #region 1 Feb 2019

        [Route("LegalUpdate/GetLegalUpdateNewsByActId")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateNewsByActId([FromBody]SearchDetail obj)
        {
            try
            {

                List<SearchDetailOutput> output = new List<SearchDetailOutput>();
                List<SP_DailyUpdateNewsLetter_Result> detailView = new List<SP_DailyUpdateNewsLetter_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    detailView = entities.SP_DailyUpdateNewsLetter().ToList();
                }
                if (detailView.Count > 0)
                {
                    if (!string.IsNullOrEmpty(obj.ActID))
                    {
                        int Actid = Convert.ToInt32(obj.ActID);
                        detailView = detailView.Where(x => x.ActID == Actid).ToList();
                    }

                    if (!string.IsNullOrEmpty(obj.ComplianceType))
                    {
                        detailView = detailView.Where(x => x.ComplianceTypeName == obj.ComplianceType).ToList();
                    }
                    if (!string.IsNullOrEmpty(obj.ComplianceCategory))
                    {
                        detailView = detailView.Where(x => x.ComplianceCategoryName == obj.ComplianceCategory).ToList();
                    }
                    if (!string.IsNullOrEmpty(obj.State))
                    {
                        detailView = detailView.Where(x => x.State == obj.State).ToList();
                    }

                    int Countdata = detailView.Count;

                    if (detailView.Count > 0)
                    {
                        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                        var total = detailView.Count;
                        var pageSize = PageSizeDyanamic;
                        var skip = pageSize * (obj.pagenumber - 1);
                        detailView = detailView.Skip(skip).Take(pageSize).ToList();

                        output.Add(new SearchDetailOutput { detailView = detailView, count = Countdata });
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #endregion
        [Route("LegalUpdate/MaintainDeviceID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MaintainDeviceID([FromBody]DeviceDetails devicedetailObj)
        {
            try
            {
                string ISproduct = "L";
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(devicedetailObj.Email)))
                {
                    Email = devicedetailObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                int userID = -1;
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());

                        if (userID != -1)
                        {
                            if (devicedetailObj.flag.Equals("No"))
                            {
                                var checkactive = (from row in entities.MaintainDeviceIDs
                                                   where row.Product == ISproduct
                                                   && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                   && row.UserID == userID
                                                   && row.Status == "Active"
                                                   select row).FirstOrDefault();

                                if (checkactive == null)
                                {
                                    MaintainDeviceID obj = new MaintainDeviceID();
                                    obj.UserID = userID;
                                    obj.DeviceIDIMEI = devicedetailObj.DeviceIDIMEI;
                                    obj.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                    obj.Status = "Active";
                                    obj.Product = ISproduct;
                                    entities.MaintainDeviceIDs.Add(obj);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var updatefortoken = (from row in entities.MaintainDeviceIDs
                                                          where row.Product == ISproduct
                                                          && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                          && row.UserID == userID
                                                          && row.Status == "Active"
                                                          select row).FirstOrDefault();
                                    if (updatefortoken != null)
                                    {
                                        updatefortoken.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                    }
                                    entities.SaveChanges();
                                }
                            }
                            else if (devicedetailObj.flag.Equals("Yes"))
                            {
                                var checkactive = (from row in entities.MaintainDeviceIDs
                                                   where row.Product == ISproduct
                                                        && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                        && row.Status == "Active"
                                                   select row).ToList();
                                if (checkactive.Count > 0)
                                {
                                    foreach (var item in checkactive)
                                    {
                                        int UserIDforInactive = item.UserID;
                                        if (UserIDforInactive != null)
                                        {
                                            var updateforstatus = (from row in entities.MaintainDeviceIDs
                                                                   where row.Product == ISproduct
                                                                   && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                                   && row.UserID == UserIDforInactive
                                                                   && row.Status == "Active"
                                                                   select row).FirstOrDefault();
                                            if (updateforstatus != null)
                                            {
                                                updateforstatus.Status = "InActive";
                                            }
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                MaintainDeviceID obj = new MaintainDeviceID();
                                obj.UserID = userID;
                                obj.DeviceIDIMEI = devicedetailObj.DeviceIDIMEI;
                                obj.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                obj.Status = "Active";
                                obj.Product = ISproduct;
                                entities.MaintainDeviceIDs.Add(obj);
                                entities.SaveChanges();
                            }
                            else if (devicedetailObj.flag.Equals("NotificationNo"))
                            {
                                var checkactive = (from row in entities.MaintainDeviceIDs
                                                   where row.Product == ISproduct
                                                   && row.DeviceIDIMEI == devicedetailObj.DeviceIDIMEI
                                                   && row.Status == "Active"
                                                   select row).FirstOrDefault();
                                if (checkactive != null)
                                {
                                    checkactive.DeviceToken = devicedetailObj.DeviceToken.Trim();
                                }
                                entities.SaveChanges();
                            }
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }




        #region Daily update for Teamlease

        [Route("LegalUpdate/GetUserDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetUserDetail([FromBody]GetUserInfo obj)
        {
            try
            {
                string baseAddress = ConfigurationManager.AppSettings["FetchUrlDetail"].ToString();
                string DailyUpdateUserKey = ConfigurationManager.AppSettings["RLCSDailyUpdateUser"].ToString();
                List<AuthKeyDetails> people = new List<AuthKeyDetails>();

                string setAccessToken = string.Empty;

                if (!string.IsNullOrEmpty(obj.UserInfo))
                {
                    if (DailyUpdateUserKey.Equals(obj.UserInfo))
                    {
                        using (var client = new HttpClient())
                        {
                            var form = new Dictionary<string, string>
                                            {
                                                {"grant_type", "password"},
                                                {"username", obj.UserInfo},
                                                {"password", obj.UserInfo},
                                            };
                            var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                            var token = tokenResponse.Content.ReadAsAsync<Models.Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                            if (string.IsNullOrEmpty(token.Error))
                            {
                                setAccessToken = "bearer " + token.AccessToken;
                            }
                            else
                            {
                                setAccessToken = "bearer " + token.Error;
                            }
                        }
                        people.Add(new AuthKeyDetails { status = "True", tokendetail = setAccessToken, MessageDetail = "Token generated successfully." });
                    }
                    else
                    {
                        people.Add(new AuthKeyDetails { status = "False", tokendetail = setAccessToken, MessageDetail = "Wrong User." });
                    }
                }
                else
                {
                    people.Add(new AuthKeyDetails { status = "False", tokendetail = setAccessToken, MessageDetail = "User should not be empty." });
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(people).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("LegalUpdate/GetLegalUpdateDataForTeamLease")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateDataForTeamLease([FromBody]UserDetail UserDetailObj)
        {
            try
            {
                MainRootObject objmainrootobject = new MainRootObject();
                SearchDailyUpdateTLObject searchobj = new SearchDailyUpdateTLObject();
                List<DailyUpdateSearch.Hit> masterAlllist = new List<DailyUpdateSearch.Hit>();

                int page = 0;
                if (UserDetailObj.pagenumber == 0)
                {
                    page = 1;
                }
                else
                {
                    page = UserDetailObj.pagenumber;
                }
                int pagesize = page * 10;
                int from = pagesize - 10;

                string Category = "Human Resource";
                if (!string.IsNullOrEmpty(Category))
                {
                    string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                    string eusername = ConfigurationManager.AppSettings["ElasticUserName"];
                    string epassword = ConfigurationManager.AppSettings["ElasticPassword"];
                    var node = new Uri(elasticsearchconnectionstring);
                    var dailyupdatesettings = new ConnectionSettings(node);
                    dailyupdatesettings.DefaultIndex("dailyupdatesindexwithparam")
                                        .BasicAuthentication(eusername, epassword)
                                        .RequestTimeout(TimeSpan.FromMinutes(1));
                    dailyupdatesettings.DisableDirectStreaming();
                    dailyupdatesettings.PrettyJson().DisableDirectStreaming();
                    var dailyupdateclient = new ElasticClient(dailyupdatesettings);

                    ISearchResponse<ComplianceManagement.DailyUpdatesListParameter> dailyupdatesearchresults;

                    dailyupdatesearchresults = dailyupdateclient.Search<ComplianceManagement.DailyUpdatesListParameter>(s => s
                                        .From(from) // apply paging
                                        .Size(10) // limit to page size
                                        .Query(q => q // define query
                                        .MultiMatch(mp => mp // of type MultiMatch
                                        .Query(Category) // pass text
                                        .Fields(f => f // define fields to search against
                                        .Fields(f1 => f1.compliancecategoryname))))
                                        .Sort(ss => ss.Descending(p => p.intdaliyupdatedate)));

                    if (dailyupdatesearchresults != null)
                    {
                        using (MemoryStream mStream = new MemoryStream())
                        {
                            dailyupdateclient.Serializer.Serialize(dailyupdatesearchresults, mStream);
                            var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                            DailyUpdateSearch.RootObject item = JsonConvert.DeserializeObject<DailyUpdateSearch.RootObject>(rawQueryText);
                            objmainrootobject.rootobject3 = item;
                        }
                    }

                    masterAlllist = objmainrootobject.rootobject3.hits.hits;
                    if (!(string.IsNullOrEmpty(Category)))
                    {
                        masterAlllist = masterAlllist.Where(x => x._source.compliancecategoryname.Contains(Category)).ToList();
                    }
                    searchobj.HRData = masterAlllist.ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion


        [Route("LegalUpdate/UpdateNewUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateNewUser([FromBody]LegalUpdateRegister LUR)
        {
            try
            {
                List<LegalUpdateAns> ans = new List<LegalUpdateAns>();
                string CompanyName = LUR.CompanyName;
                string Email = LUR.Email.ToString();
                string FirstName = LUR.FirstName.ToString();
                string LastName = LUR.LastName.ToString();
                string ContactNumber = LUR.ContactNumber.ToString();
                string CGSTNumber = LUR.GSTNumber;
                string CRegNumber = LUR.CRegNumber;
                string UserPAN = LUR.UserPAN;
                int Flag = Convert.ToInt32(LUR.Flag);//1 for Corporate and 2 for Individual
                
                int CustId = Convert.ToInt32(ConfigurationManager.AppSettings["LegalUpdateAvantisCustomerId"]);
                
                if (Flag == 1)
                {
                    CustId = -1;
                    Customer customer = new Customer()
                    {
                        Name = CompanyName,
                        Address = "NA",
                        BuyerName = "NA",
                        BuyerContactNumber = ContactNumber,
                        LocationType = -1,
                        Status = 1,
                        IComplianceApplicable = 0,
                        TaskApplicable = 0,
                        IsServiceProvider = false,
                        GSTNum = CGSTNumber,
                        CRegNum = CRegNumber,
                        ServiceProviderID=95                        
                    };

                    mst_Customer mst_customer = new mst_Customer()
                    {
                        Name = CompanyName,
                        Address = "NA",
                        BuyerName = "NA",
                        BuyerContactNumber = ContactNumber,
                        LocationType = -1,
                        Status = 1,
                        IComplianceApplicable = 0,
                        TaskApplicable = 0,
                        GSTNum = CGSTNumber,
                        CRegNum = CRegNumber,
                        IsServiceProvider = false,
                        ServiceProviderID = 95
                    };
                    int CustresultValue = 0;
                    bool Custresult = true;
                    bool CustflagEmailExist = true;
                    if (CustomerManagement.Exists(customer))
                    {
                        CustflagEmailExist = false;
                    }
                    if (UserManagementRisk.Exists(mst_customer))
                    {
                        CustflagEmailExist = false;
                    }
                    if (CustflagEmailExist)
                    {
                        CustresultValue = CustomerManagement.Create(customer);
                        if (CustresultValue > 0)
                        {
                            Custresult = CustomerManagement.Create(mst_customer);
                            if (Custresult == false)
                            {
                                CustomerManagement.deleteCustReset(CustresultValue);
                            }
                            else
                            {
                                CustId = CustresultValue;
                            }
                        }
                    }
                    else
                    {
                        ans.Add(new LegalUpdateAns { Ans = "Company Name already exists.", tokendetail = "" });
                    }
                }
                if (CustId != -1)
                {
                    string CustName = string.Empty;
                    string Address = "NA";
                    int RoleId = Convert.ToInt32(ConfigurationManager.AppSettings["LegalUpdateAvantisRoleID"]);

                    CustName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustId));

                    List<UserParameterValue> parameters = new List<UserParameterValue>();
                    List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

                    User user = new User()
                    {
                        FirstName = FirstName.ToString(),
                        LastName = LastName.ToString(),
                        Designation = "",
                        Email = Email.ToString(),
                        ContactNumber = ContactNumber,
                        Address = Address,
                        RoleID = RoleId,
                        IsHead = false,
                        CustomerID = CustId,
                        PAN = UserPAN
                    };

                    mst_User mstUser = new mst_User()
                    {
                        FirstName = FirstName.ToString(),
                        LastName = LastName.ToString(),
                        Designation = "",
                        Email = Email.ToString(),
                        ContactNumber = ContactNumber,
                        Address = Address,
                        RoleID = RoleId,
                        IsExternal = false,
                        IsHead = false,
                        CustomerID = CustId,
                        PAN = UserPAN
                    };
                    int resultValue = 0;
                    bool result = false;
                    bool flagEmailExist = true;
                    bool flagContactExist = true;
                    bool emailExists;
                    bool ContactExist;
                    UserManagement.ContactExists(user, out ContactExist);
                    if (ContactExist)
                    {
                        flagContactExist = false;
                    }
                    UserManagementRisk.ContactExists(mstUser, out ContactExist);
                    if (ContactExist)
                    {
                        flagContactExist = false;
                    }
                    if (flagContactExist)
                    {
                        UserManagement.Exists(user, out emailExists);
                        if (emailExists)
                        {
                            flagEmailExist = false;
                        }
                        UserManagementRisk.Exists(mstUser, out emailExists);
                        if (emailExists)
                        {
                            flagEmailExist = false;
                        }
                        if (flagEmailExist)
                        {
                            user.CreatedBy = CustId;
                            user.CreatedByText = CustName;
                            string passwordText = Util.CreateRandomPassword(10);
                            user.Password = Util.CalculateMD5Hash(passwordText);
                            string message = "true";
                            //string message = SendNotificationEmail(user, passwordText);
                            mstUser.CreatedBy = CustId;
                            mstUser.CreatedByText = CustName;
                            mstUser.Password = Util.CalculateMD5Hash(passwordText);

                            resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                            if (resultValue > 0)
                            {
                                result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message,true);
                                if (result == false)
                                {
                                    UserManagement.deleteUser(resultValue);
                                }
                            }
                            if (result == true)
                            {
                                widget swid = new widget()
                                {
                                    UserId = (Int32)user.ID,
                                    Performer = false,
                                    Reviewer = false,
                                    PerformerLocation = false,
                                    ReviewerLocation = false,
                                    DailyUpdate = false,
                                    NewsLetter = false,
                                    ComplianceSummary = false,
                                    FunctionSummary = false,
                                    RiskCriteria = false,
                                    EventOwner = false,
                                    PenaltySummary = false,
                                    TaskSummary = false,
                                    ReviewerTaskSummary = false,
                                };
                                result = UserManagement.Create(swid);
                                if (result)
                                {
                                    Random random = new Random();
                                    int value = random.Next(1000000);
                                    long Contact;

                                    VerifyOTP OTPData = new VerifyOTP()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        EmailId = Email.Trim(),
                                        OTP = Convert.ToInt32(value),
                                        CreatedOn = DateTime.Now,
                                        IsVerified = false
                                    };
                                    bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                    if (OTPresult)
                                    {
                                        OTPData.MobileNo = Contact;
                                    }
                                    else
                                    {
                                        OTPData.MobileNo = 0;
                                    }
                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                    {
                                        try
                                        {
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                        }
                                        catch (Exception ex)
                                        {
                                            result = false;
                                        }
                                    }
                                    // EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
                                }
                                else
                                {
                                    UserManagement.deleteUser(resultValue);
                                    UserManagementRisk.deleteMstUser(resultValue);
                                    if (Flag == 1)
                                    {
                                        CustomerManagement.deleteCustReset(CustId);
                                        CustomerManagement.deleteMstCustReset(CustId);
                                    }
                                }
                            }
                            if (result)
                            {
                                string baseAddress = ConfigurationManager.AppSettings["FetchUrlDetail"].ToString();                                
                                string setAccessToken = string.Empty;

                                using (var client = new HttpClient())
                                {
                                    var form = new Dictionary<string, string>
                                            {
                                                {"grant_type", "password"},
                                                {"username", user.Email},
                                                {"password", user.Password},
                                            };
                                    var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                                    var token = tokenResponse.Content.ReadAsAsync<Models.Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                                    if (string.IsNullOrEmpty(token.Error))
                                    {
                                        setAccessToken = "bearer " + token.AccessToken;
                                    }
                                    else
                                    {
                                        setAccessToken = "bearer " + token.Error;
                                    }
                                }

                                ans.Add(new LegalUpdateAns { Ans = "Registered Successfully", tokendetail = setAccessToken });
                            }
                            else
                            {
                                ans.Add(new LegalUpdateAns { Ans = "Registration unsuccessfull", tokendetail = "" });
                            }
                        }
                        else
                        {
                            if (Flag == 1)
                            {
                                CustomerManagement.deleteCustReset(CustId);
                                CustomerManagement.deleteMstCustReset(CustId);
                            }
                            ans.Add(new LegalUpdateAns { Ans = "User Email already exists.", tokendetail = "" });
                        }
                    }
                    else
                    {
                        if (Flag == 1)
                        {
                            CustomerManagement.deleteCustReset(CustId);
                            CustomerManagement.deleteMstCustReset(CustId);
                        }
                        ans.Add(new LegalUpdateAns { Ans = "Contact Number already exists.", tokendetail = "" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ans).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        //[Route("LegalUpdate/UpdateNewUser")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage UpdateNewUser([FromBody]LegalUpdateRegister LUR)
        //{
        //    try
        //    {
        //        List<LegalUpdateAns> ans = new List<LegalUpdateAns>();
        //        string CompanyName = LUR.CompanyName;// "New New Avantis Softech";
        //        string Email = LUR.Email.ToString();// "Sushant@avantis.info";
        //        string FirstName = LUR.FirstName.ToString();// "sushant";
        //        string LastName = LUR.LastName.ToString();// "Khot";
        //        string ContactNumber = LUR.ContactNumber.ToString();// "0000000000";
        //        string CGSTNumber = LUR.GSTNumber;
        //        string CRegNumber = LUR.CRegNumber;
        //        string UserPAN = LUR.UserPAN;
        //        int Flag = Convert.ToInt32(LUR.Flag);//1 for Corporate and 2 for Individual


        //        int CustId = Convert.ToInt32(ConfigurationManager.AppSettings["LegalUpdateAvantisCustomerId"]);


        //        if (Flag == 1)
        //        {
        //            CustId = -1;
        //            Customer customer = new Customer()
        //            {
        //                Name = CompanyName,
        //                Address = "NA",
        //                BuyerName = "NA",
        //                BuyerContactNumber = ContactNumber,
        //                LocationType = -1,
        //                Status = 1,
        //                IComplianceApplicable = 0,
        //                TaskApplicable = 0,
        //                IsServiceProvider = true,
        //                GSTNum = CGSTNumber,
        //                CRegNum = CRegNumber
        //            };

        //            mst_Customer mst_customer = new mst_Customer()
        //            {
        //                Name = CompanyName,
        //                Address = "NA",
        //                BuyerName = "NA",
        //                BuyerContactNumber = ContactNumber,
        //                LocationType = -1,
        //                Status = 1,
        //                IComplianceApplicable = 0,
        //                TaskApplicable = 0,
        //                GSTNum = CGSTNumber,
        //                CRegNum = CRegNumber
        //            };
        //            int CustresultValue = 0;
        //            bool Custresult = true;
        //            bool CustflagEmailExist = true;
        //            if (CustomerManagement.Exists(customer))
        //            {
        //                CustflagEmailExist = false;
        //            }
        //            if (UserManagementRisk.Exists(mst_customer))
        //            {
        //                CustflagEmailExist = false;
        //            }
        //            if (CustflagEmailExist)
        //            {
        //                CustresultValue = CustomerManagement.Create(customer);
        //                if (CustresultValue > 0)
        //                {
        //                    Custresult = CustomerManagement.Create(mst_customer);
        //                    if (Custresult == false)
        //                    {
        //                        CustomerManagement.deleteCustReset(CustresultValue);
        //                    }
        //                    else
        //                    {
        //                        CustId = CustresultValue;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                ans.Add(new LegalUpdateAns { Ans = "Company Name already exists." });
        //            }
        //        }
        //        if (CustId != -1)
        //        {
        //            string CustName = string.Empty;
        //            string Address = "NA";
        //            int RoleId = Convert.ToInt32(ConfigurationManager.AppSettings["LegalUpdateAvantisRoleID"]);

        //            CustName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustId));

        //            List<UserParameterValue> parameters = new List<UserParameterValue>();
        //            List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

        //            User user = new User()
        //            {
        //                FirstName = FirstName.ToString(),
        //                LastName = LastName.ToString(),
        //                Designation = "",
        //                Email = Email.ToString(),
        //                ContactNumber = ContactNumber,
        //                Address = Address,
        //                RoleID = RoleId,
        //                IsHead = false,
        //                CustomerID = CustId,
        //                PAN = UserPAN
        //            };

        //            mst_User mstUser = new mst_User()
        //            {
        //                FirstName = FirstName.ToString(),
        //                LastName = LastName.ToString(),
        //                Designation = "",
        //                Email = Email.ToString(),
        //                ContactNumber = ContactNumber,
        //                Address = Address,
        //                RoleID = RoleId,
        //                IsExternal = false,
        //                IsHead = false,
        //                CustomerID = CustId,
        //                PAN = UserPAN
        //            };
        //            int resultValue = 0;
        //            bool result = false;
        //            bool flagEmailExist = true;
        //            bool emailExists;
        //            UserManagement.Exists(user, out emailExists);
        //            if (emailExists)
        //            {
        //                flagEmailExist = false;
        //            }
        //            UserManagementRisk.Exists(mstUser, out emailExists);
        //            if (emailExists)
        //            {
        //                flagEmailExist = false;
        //            }
        //            if (flagEmailExist)
        //            {
        //                user.CreatedBy = CustId;
        //                user.CreatedByText = CustName;
        //                string passwordText = "admin@123";// Util.CreateRandomPassword(10);
        //                user.Password = Util.CalculateMD5Hash(passwordText);
        //                string message = SendNotificationEmail(user, passwordText);

        //                mstUser.CreatedBy = CustId;
        //                mstUser.CreatedByText = CustName;
        //                mstUser.Password = Util.CalculateMD5Hash(passwordText);

        //                resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //                if (resultValue > 0)
        //                {
        //                    result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //                    if (result == false)
        //                    {
        //                        UserManagement.deleteUser(resultValue);
        //                    }
        //                }
        //                if (result == true)
        //                {
        //                    widget swid = new widget()
        //                    {
        //                        UserId = (Int32)user.ID,
        //                        Performer = false,
        //                        Reviewer = false,
        //                        PerformerLocation = false,
        //                        ReviewerLocation = false,
        //                        DailyUpdate = false,
        //                        NewsLetter = false,
        //                        ComplianceSummary = false,
        //                        FunctionSummary = false,
        //                        RiskCriteria = false,
        //                        EventOwner = false,
        //                        PenaltySummary = false,
        //                        TaskSummary = false,
        //                        ReviewerTaskSummary = false,
        //                    };
        //                    result = UserManagement.Create(swid);
        //                    if (result)
        //                    {
        //                        Random random = new Random();
        //                        int value = random.Next(1000000);
        //                        long Contact;

        //                        VerifyOTP OTPData = new VerifyOTP()
        //                        {
        //                            UserId = Convert.ToInt32(user.ID),
        //                            EmailId = Email.Trim(),
        //                            OTP = Convert.ToInt32(value),
        //                            CreatedOn = DateTime.Now,
        //                            IsVerified = false
        //                        };
        //                        bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
        //                        if (OTPresult)
        //                        {
        //                            OTPData.MobileNo = Contact;
        //                        }
        //                        else
        //                        {
        //                            OTPData.MobileNo = 0;
        //                        }
        //                        VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
        //                        if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
        //                        {
        //                            try
        //                            {
        //                                SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                result = false;
        //                            }
        //                        }
        //                        // EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
        //                    }
        //                    else
        //                    {

        //                        UserManagement.deleteUser(resultValue);
        //                        UserManagementRisk.deleteMstUser(resultValue);
        //                        if (Flag == 1)
        //                        {
        //                            CustomerManagement.deleteCustReset(CustId);
        //                            CustomerManagement.deleteMstCustReset(CustId);
        //                        }
        //                    }
        //                }
        //                if (result)
        //                {
        //                    ans.Add(new LegalUpdateAns { Ans = "Registered Successfully" });
        //                }
        //                else
        //                {
        //                    ans.Add(new LegalUpdateAns { Ans = "Registration unsuccessfull" });
        //                }
        //            }
        //            else
        //            {
        //                if (Flag == 1)
        //                {
        //                    CustomerManagement.deleteCustReset(CustId);
        //                    CustomerManagement.deleteMstCustReset(CustId);
        //                }
        //                ans.Add(new LegalUpdateAns { Ans = "User Email already exists." });
        //            }
        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(ans).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "Avantis Legal Update";
                //if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                //{
                //    ReplyEmailAddressName = "Avantis";
                //}
                //else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                //{
                //    ReplyEmailAddressName = "Avantis";
                //}
                //else
                //{
                //    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                //    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                //}

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = "true";
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;
        }

        [Route("LegalUpdate/OTPVerify")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage OTPVerify([FromBody]LegalUpdateOTP LUOtp)
        {
            try
            {
                
                string Email = LUOtp.Email.ToString();
                int OTP = -1;
                if (LUOtp.OTP != null)
                {
                    OTP = Convert.ToInt32(LUOtp.OTP);
                }
                string IsTrial = string.Empty;
                string IsSubscription = string.Empty;

                string TPD = string.Empty;
                string SSD = string.Empty;
                string SED = string.Empty;

                List<Int32> roles = new List<Int32>();
                List<AnsDetailsOTPVerify> ans = new List<AnsDetailsOTPVerify>();
                List<AnsLeaglDetailsOTPVerify> output = new List<AnsLeaglDetailsOTPVerify>();
                
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                int userID = -1;
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email || row.Email == EmailIOS)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());
                    }

                    if (userID != -1)
                    {
                        if (VerifyOTPManagement.Exists(Convert.ToInt32(OTP), Convert.ToInt32(userID)))
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(userID));

                            VerifyOTP OTPData = VerifyOTPManagement.GetByID(userID);
                            int a = Convert.ToInt32(DateTime.Now.ToString("hhmm"));
                            int b = Convert.ToInt32(OTPData.CreatedOn.Value.ToString("hhmm"));
                            int c = a - b;

                            if (c <= 30)
                            {
                                VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(userID), Convert.ToInt32(OTP));
                                if (user.RoleID == 13)
                                {
                                    SubscriptionDetail SubscriptionData = UserManagement.GetSubscriptionDetailByID(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));

                                    if (SubscriptionData != null)
                                    {
                                        if (SubscriptionData.TrialPack != null)
                                        {
                                            IsTrial = Convert.ToString(SubscriptionData.TrialPack);
                                        }
                                        if (SubscriptionData.Subscription != null)
                                        {
                                            IsSubscription = Convert.ToString(SubscriptionData.Subscription);
                                        }
                                        if (SubscriptionData.TrailPackDate != null)
                                        {
                                            TPD = Convert.ToString(SubscriptionData.Subscription);
                                        }
                                        if (SubscriptionData.SubscriptionStartDate != null)
                                        {
                                            SSD = Convert.ToString(SubscriptionData.SubscriptionStartDate);
                                        }
                                        if (SubscriptionData.SubscriptionEndDate != null)
                                        {
                                            SED = Convert.ToString(SubscriptionData.SubscriptionEndDate);
                                        }
                                        roles.Clear();
                                        roles.Add(13);
                                        output.Add(new AnsLeaglDetailsOTPVerify
                                        {
                                            Msg = "OTP Verify",
                                            IsTrial = IsTrial,
                                            TrialStartDdate = TPD,
                                            IsSubscription = IsSubscription,
                                            SubscriptionStartDate = SSD,
                                            SubscriptionEndDate = SED,
                                            RoleID = roles
                                        });
                                    }
                                    else
                                    {
                                        if (ExistUserActAssign(Convert.ToInt32(Convert.ToInt32(user.ID))))
                                        {
                                            roles.Clear();
                                            roles.Add(13);
                                            output.Add(new AnsLeaglDetailsOTPVerify
                                            {
                                                Msg = "OTP Verified and Acts Assigned.",
                                                IsTrial = IsTrial,
                                                TrialStartDdate = TPD,
                                                IsSubscription = IsSubscription,
                                                SubscriptionStartDate = SSD,
                                                SubscriptionEndDate = SED,
                                                RoleID = roles
                                            });
                                        }
                                        else
                                        {
                                            roles.Clear();
                                            roles.Add(13);
                                            output.Add(new AnsLeaglDetailsOTPVerify
                                            {
                                                Msg = "OTP Verify But Data Not Available",
                                                IsTrial = IsTrial,
                                                TrialStartDdate = TPD,
                                                IsSubscription = IsSubscription,
                                                SubscriptionStartDate = SSD,
                                                SubscriptionEndDate = SED,
                                                RoleID = roles
                                            });
                                            //string passwordText = Util.CreateRandomPassword(10);
                                            //string Encryptedpassword = Util.CalculateMD5Hash(passwordText);
                                            //if (UserManagement.UpdateUserPassword(Convert.ToInt32(user.ID), Encryptedpassword.ToString()))
                                            //{
                                            //    if (user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                            //    {
                                            //        try
                                            //        {
                                            //            //SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(passwordText) + ".  " + "Thank you, " + "Avantis Team.");
                                            //        }
                                            //        catch (Exception ex)
                                            //        {

                                            //        }
                                            //    }
                                            //    output.Add(new AnsLeaglDetailsOTPVerify
                                            //    {
                                            //        Msg = "OTP Verify But Data Not Available",
                                            //        IsTrial = IsTrial,
                                            //        TrialStartDdate = TPD,
                                            //        IsSubscription = IsSubscription,
                                            //        SubscriptionStartDate = SSD,
                                            //        SubscriptionEndDate = SED,
                                            //    });
                                            //}
                                            //else
                                            //{
                                            //    output.Add(new AnsLeaglDetailsOTPVerify
                                            //    {
                                            //        Msg = "OTP Verify But Password Not Update",
                                            //        IsTrial = IsTrial,
                                            //        TrialStartDdate = TPD,
                                            //        IsSubscription = IsSubscription,
                                            //        SubscriptionStartDate = SSD,
                                            //        SubscriptionEndDate = SED
                                            //    });
                                            //}
                                        }
                                    }
                                }
                                else {
                                    roles.Clear();
                                    roles.Add(0);
                                    output.Add(new AnsLeaglDetailsOTPVerify
                                    {
                                        Msg = "OTP Verified but Role not match.",
                                        IsTrial = IsTrial,
                                        TrialStartDdate = TPD,
                                        IsSubscription = IsSubscription,
                                        SubscriptionStartDate = SSD,
                                        SubscriptionEndDate = SED,
                                        RoleID = roles
                                    });
                                }
                            }
                            else
                            {
                                roles.Clear();
                                roles.Add(0);
                                output.Add(new AnsLeaglDetailsOTPVerify
                                {
                                    Msg = "OTP Expired.",
                                    IsTrial = IsTrial,
                                    TrialStartDdate = TPD,
                                    IsSubscription = IsSubscription,
                                    SubscriptionStartDate = SSD,
                                    SubscriptionEndDate = SED,
                                    RoleID = roles
                                });
                            }
                        }
                        else
                        {
                            if (WrongOTPCount < 3)
                            {
                                roles.Clear();
                                roles.Add(0);
                                output.Add(new AnsLeaglDetailsOTPVerify
                                {
                                    Msg = "Invalid OTP, Please Enter Again.",
                                    IsTrial = IsTrial,
                                    TrialStartDdate = TPD,
                                    IsSubscription = IsSubscription,
                                    SubscriptionStartDate = SSD,
                                    SubscriptionEndDate = SED,
                                    RoleID = roles
                                });
                                WrongOTPCount++;
                            }
                            else
                            {
                                roles.Clear();
                                roles.Add(0);
                                output.Add(new AnsLeaglDetailsOTPVerify
                                {
                                    Msg = "Your account is locked, Please unlock your account or contact to administrator.",
                                    IsTrial = IsTrial,
                                    TrialStartDdate = TPD,
                                    IsSubscription = IsSubscription,
                                    SubscriptionStartDate = SSD,
                                    SubscriptionEndDate = SED,
                                    RoleID = roles
                                });

                                UserManagement.WrongAttemptCountUpdate(userID);
                                UserManagementRisk.WrongAttemptCountUpdate(userID);
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //// [Authorize]
        //[Route("LegalUpdate/OTPVerify")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage OTPVerify([FromBody]LegalUpdateOTP LUOtp)
        //{
        //    try
        //    {
        //        string Email = LUOtp.Email.ToString();
        //        int OTP = -1;
        //        if (LUOtp.OTP != null)
        //        {
        //            OTP = Convert.ToInt32(LUOtp.OTP);
        //        }
        //        string IsTrial = string.Empty;
        //        string IsSubscription = string.Empty;

        //        string TPD = string.Empty;
        //        string SSD = string.Empty;
        //        string SED = string.Empty;

        //        List<Int32> roles = new List<Int32>();
        //        List<AnsDetailsOTPVerify> ans = new List<AnsDetailsOTPVerify>();
        //        List<AnsLeaglDetailsOTPVerify> output = new List<AnsLeaglDetailsOTPVerify>();


        //        Email = Email.Replace(" ", "+");
        //        var encryptedBytes = Convert.FromBase64String(Email);
        //        try
        //        {
        //            Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
        //        }
        //        catch { }

        //        string EmailIOS = string.Empty;
        //        try
        //        {
        //            EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
        //        }
        //        catch { }
        //        int userID = -1;
        //        if (!string.IsNullOrEmpty(Email))
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                userID = Convert.ToInt32((from row in entities.Users
        //                                          where (row.Email == Email || row.Email == EmailIOS)
        //                                            && row.IsActive == true
        //                                            && row.IsDeleted == false
        //                                          select row.ID).FirstOrDefault());
        //            }

        //            if (userID != -1)
        //            {
        //                if (VerifyOTPManagement.Exists(Convert.ToInt32(OTP), Convert.ToInt32(userID)))
        //                {
        //                    User user = UserManagement.GetByID(Convert.ToInt32(userID));

        //                    VerifyOTP OTPData = VerifyOTPManagement.GetByID(userID);
        //                    int a = Convert.ToInt32(DateTime.Now.ToString("hhmm"));
        //                    int b = Convert.ToInt32(OTPData.CreatedOn.Value.ToString("hhmm"));
        //                    int c = a - b;

        //                    if (c <= 30)
        //                    {
        //                        VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(userID), Convert.ToInt32(OTP));
        //                        if (user.RoleID == 13)
        //                        {
        //                            roles.Clear();
        //                            roles.Add(13);

        //                            SubscriptionDetail SubscriptionData = UserManagement.GetSubscriptionDetailByID(Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID));

        //                            if (SubscriptionData != null)
        //                            {
        //                                if (SubscriptionData.TrialPack != null)
        //                                {
        //                                    IsTrial = Convert.ToString(SubscriptionData.TrialPack);
        //                                }
        //                                if (SubscriptionData.Subscription != null)
        //                                {
        //                                    IsSubscription = Convert.ToString(SubscriptionData.Subscription);
        //                                }
        //                                if (SubscriptionData.TrailPackDate != null)
        //                                {
        //                                    TPD = Convert.ToString(SubscriptionData.Subscription);
        //                                }
        //                                if (SubscriptionData.SubscriptionStartDate != null)
        //                                {
        //                                    SSD = Convert.ToString(SubscriptionData.SubscriptionStartDate);
        //                                }
        //                                if (SubscriptionData.SubscriptionEndDate != null)
        //                                {
        //                                    SED = Convert.ToString(SubscriptionData.SubscriptionEndDate);
        //                                }
        //                                output.Add(new AnsLeaglDetailsOTPVerify
        //                                {
        //                                    Msg = "OTP Verify",
        //                                    IsTrial = IsTrial,
        //                                    TrialStartDdate = TPD,
        //                                    IsSubscription = IsSubscription,
        //                                    SubscriptionStartDate = SSD,
        //                                    SubscriptionEndDate = SED
        //                                });
        //                            }
        //                            else
        //                            {
        //                                output.Add(new AnsLeaglDetailsOTPVerify
        //                                {
        //                                    Msg = "OTP Verify But Data Not Available",
        //                                    IsTrial = IsTrial,
        //                                    TrialStartDdate = TPD,
        //                                    IsSubscription = IsSubscription,
        //                                    SubscriptionStartDate = SSD,
        //                                    SubscriptionEndDate = SED
        //                                });
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        output.Add(new AnsLeaglDetailsOTPVerify
        //                        {
        //                            Msg = "OTP Expired.",
        //                            IsTrial = IsTrial,
        //                            TrialStartDdate = TPD,
        //                            IsSubscription = IsSubscription,
        //                            SubscriptionStartDate = SSD,
        //                            SubscriptionEndDate = SED
        //                        });
        //                    }
        //                }
        //                else
        //                {
        //                    if (WrongOTPCount < 3)
        //                    {
        //                        output.Add(new AnsLeaglDetailsOTPVerify
        //                        {
        //                            Msg = "Invalid OTP, Please Enter Again.",
        //                            IsTrial = IsTrial,
        //                            TrialStartDdate = TPD,
        //                            IsSubscription = IsSubscription,
        //                            SubscriptionStartDate = SSD,
        //                            SubscriptionEndDate = SED
        //                        });
        //                        WrongOTPCount++;
        //                    }
        //                    else
        //                    {
        //                        output.Add(new AnsLeaglDetailsOTPVerify
        //                        {
        //                            Msg = "Your account is locked, Please unlock your account or contact to administrator.",
        //                            IsTrial = IsTrial,
        //                            TrialStartDdate = TPD,
        //                            IsSubscription = IsSubscription,
        //                            SubscriptionStartDate = SSD,
        //                            SubscriptionEndDate = SED
        //                        });

        //                        UserManagement.WrongAttemptCountUpdate(userID);
        //                        UserManagementRisk.WrongAttemptCountUpdate(userID);
        //                    }
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

            
        [Route("LegalUpdate/UpdateSubscriptionDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateSubscriptionDetail([FromBody]UpdateSubscription obj)
        {
            try
            {
                List<LegalUpdateAns> ans = new List<LegalUpdateAns>();
                string Email = obj.Email;
                string IsTrial = obj.IsTrial;
                string IsSubscription = obj.IsSubscription;
                DateTime? SubscriptionEndDate = null;
                DateTime? TrailPackDate = null;
                DateTime? SubscriptionStartDate = null;
                bool TrailFlag = false;
                bool subscriptionFlag = false;
                if (IsTrial.Equals("True"))
                {
                    TrailFlag = true;
                    subscriptionFlag = false;
                    TrailPackDate = DateTime.Now;
                }
                else if (IsSubscription.Equals("True"))
                {
                    TrailFlag = false;
                    subscriptionFlag = true;
                    if (!string.IsNullOrEmpty(obj.SubscriptionEndDate))
                    {
                        SubscriptionEndDate = DateTime.ParseExact(obj.SubscriptionEndDate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty(obj.SubscriptionStartDate))
                    {
                        SubscriptionStartDate = DateTime.ParseExact(obj.SubscriptionStartDate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                int userID = -1;
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int ResultValue = 0;
                            bool CustflagEmailExist = true;
                            SubscriptionDetail subscription = new SubscriptionDetail()
                            {
                                UId = Convert.ToInt32(user.ID),
                                CId = Convert.ToInt32(user.CustomerID),
                                TrialPack = TrailFlag,
                                TrailPackDate = TrailPackDate,
                                Subscription = subscriptionFlag,
                                SubscriptionStartDate = SubscriptionStartDate,
                                SubscriptionEndDate = SubscriptionEndDate
                            };
                            if (CustomerManagement.Exists(subscription))
                            {
                                CustflagEmailExist = false;
                            }

                            if (CustflagEmailExist)
                            {
                                ResultValue = CustomerManagement.Create(subscription);
                                if (ResultValue > 0)
                                {
                                    ans.Add(new LegalUpdateAns { Ans = "Add detail successfully." });
                                }
                                else
                                {
                                    ans.Add(new LegalUpdateAns { Ans = "Not Add detail successfully." });
                                }
                            }
                            else
                            {
                                ans.Add(new LegalUpdateAns { Ans = "Detail Already Exist" });
                            }
                        }
                    }
                }



                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ans).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("LegalUpdate/UpdateSelfConfiguration")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateSelfConfiguration([FromBody]UpdateSelfConfigurationModel config)
        {
            try
            {
                string Email = config.Email;
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                LegalUpdateAns ans = new LegalUpdateAns();
                List<Act> ActList = new List<Act>();
                List<UserActList> output = new List<UserActList>();
                List<ComplianceCategory> categoryList = new List<ComplianceCategory>();

                List<int?> stateobj = new List<int?>();
                List<int?> categoryobj = new List<int?>();

                if (config.state.Count > 0)
                {
                    foreach (var item in config.state)
                    {
                        if (item > 0)
                        {
                            stateobj.Add(item);
                        }
                    }
                }

                if (config.category.Count > 0)
                {
                    foreach (var item in config.category)
                    {
                        if (item > 0)
                        {
                            categoryobj.Add(item);
                        }
                    }
                }

                List<StateDetail> Stateobj = new List<StateDetail>();
                List<ComplianceCategory> ComplianceCategoryobj = new List<ComplianceCategory>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.Email == Email || row.Email == EmailIOS)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();

                    if (user != null)
                    {
                        var customerID = user.CustomerID;
                        var Userid = user.ID;

                        output = (from row in entities.ElasticSearchActViews                                  
                                  select new UserActList()
                                  {
                                      ID = row.ID,
                                      Name = row.Name,
                                      StateID = row.StateID,
                                      CategoryID = row.ComplianceCategoryId
                                  }).ToList();

                        //output = (from row in entities.Acts
                        //          where row.IsDeleted == false
                        //          select new UserActList()
                        //          {
                        //              ID = row.ID,
                        //              Name = row.Name,
                        //              StateID = row.StateID,
                        //              CategoryID = row.ComplianceCategoryId
                        //          }).ToList();

                        if (stateobj.Count > 0)
                        {
                            output = output.Where(x => stateobj.Contains(x.StateID)).ToList();
                        }
                        if (categoryobj.Count > 0)
                        {
                            output = output.Where(x => config.category.Contains(x.CategoryID)).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("LegalUpdate/SetUserSelectedActs")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SetUserSelectedActs([FromBody]UserConfiguredList userConfigList)
        {
            try
            {
                string Email = userConfigList.Email;
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<LegalUpdateAns> ans = new List<LegalUpdateAns>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.Email == Email || row.Email == EmailIOS)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();

                    if (user != null)
                    {
                        int ActIdsCount = Convert.ToInt32(userConfigList.Acts.Count);
                        List<int> ActId = new List<int>();

                        for (int i = 0; i < userConfigList.Acts.Count; i++)
                        {
                            if (Convert.ToInt32(userConfigList.Acts[i].ToString()) != -1 && Convert.ToInt32(userConfigList.Acts[i].ToString()) > 0)
                            {
                                ActId.Add(Convert.ToInt32(userConfigList.Acts[i].ToString()));
                            }
                        }
                        int flag = 0;
                        if (ActId.Count > 0)
                        {
                            foreach (var item in ActId)
                            {
                                if (!(ExistActAssign(item, Convert.ToInt32(user.ID))))
                                {
                                    Research_ActAssignment obj = new Research_ActAssignment();
                                    obj.ActId = item;
                                    obj.UserId = Convert.ToInt32(user.ID);
                                    obj.CustomerId = Convert.ToInt32(user.CustomerID);
                                    obj.Createdby = Convert.ToInt32(user.ID);
                                    obj.CreatedOn = DateTime.Now;
                                    obj.IsActive = false;
                                    entities.Research_ActAssignment.Add(obj);
                                    entities.SaveChanges();
                                    flag = 1;
                                }
                            }
                            if (flag == 1)
                            {
                                ans.Add(new LegalUpdateAns { Ans = "Successfully Configured" });
                            }
                            else
                            {
                                ans.Add(new LegalUpdateAns { Ans = "Act Already assigned." });
                            }
                        }
                        else
                        {
                            ans.Add(new LegalUpdateAns { Ans = "Select Act" });
                        }
                    }
                    else
                    {
                        ans.Add(new LegalUpdateAns { Ans = "User Not Available" });
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(ans).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        public static bool ExistActAssign(int ActId, int Uid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Research_ActAssignment
                            where row.ActId == ActId && row.UserId == Uid
                            && row.IsActive == false
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        public static bool ExistUserActAssign(int Uid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Research_ActAssignment
                            where row.UserId == Uid
                            && row.IsActive == false
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        
        [Route("LegalUpdate/SetTrialDays")]
        [System.Web.Http.AcceptVerbs("GET")]
        public HttpResponseMessage SetTrialDays()
        {
            try
            {
                List<SetTrialDaysModel> setTrialDays = new List<SetTrialDaysModel>();

                long no_of_days = Convert.ToInt64(ConfigurationManager.AppSettings["TrialDay"]);
                DateTime start_date = DateTime.Now;
                DateTime end_date = start_date.AddDays(no_of_days);

                setTrialDays.Add(new SetTrialDaysModel
                {
                    no_of_days = no_of_days,
                    start_date = start_date,
                    end_date = end_date
                });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(setTrialDays).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };

            }
        }
        
        [Route("LegalUpdate/LegalUpdateGetUserLogin")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LegalUpdateGetUserLogin([FromBody]GetEmail obj)
        {
            try
            {
                string Email = obj.Email.ToString();
                string DeviceIDIMEI = obj.DeviceIDIMEI.ToString();

                var CallerAgent = HttpContext.Current.Request.UserAgent;
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                if (CallerAgent.Equals("Android"))
                {
                    try
                    {
                        Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        Email = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                    }
                    catch { }
                }
                bool Success = false;
                string UserContact = "No";
                string baseAddress = ConfigurationManager.AppSettings["FetchUrlDetail"].ToString();
                List<LoginDetails> people = new List<LoginDetails>();

                string setAccessToken = string.Empty;

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where row.Email == Email
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.IsActive)
                            {
                                int value;
                                if (CallerAgent.Equals("Android"))
                                {
                                    Random random = new Random();
                                    value = random.Next(1000000);
                                }
                                else
                                {
                                    //iOS Relese
                                    string IsiOSCheck = ConfigurationManager.AppSettings["IsiOSCheck"];
                                    if (IsiOSCheck == "1") // Relese
                                    {
                                            Random random = new Random();
                                            value = random.Next(1000000);
                                    }
                                    else // Review
                                    {
                                        if (Email == "sachin@avantis.info")
                                        {
                                            Random random = new Random();
                                            value = 1234;
                                        }
                                        else
                                        {
                                            Random random = new Random();
                                            value = random.Next(1000000);
                                        }
                                    }
                                }
                                long Contact;
                              
                                VerifyOTP OTPData = new VerifyOTP()
                                {
                                    UserId = Convert.ToInt32(user.ID),
                                    EmailId = Email.Trim(),
                                    OTP = Convert.ToInt32(value),
                                    CreatedOn = DateTime.Now,
                                    IsVerified = false
                                };
                                bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                if (OTPresult)
                                {
                                    OTPData.MobileNo = Contact;
                                }
                                else
                                {
                                    OTPData.MobileNo = 0;
                                }
                                VerifyOTPManagement.Create(OTPData);
                                //Insert Data in OTP Table.
                                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                try
                                {
                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for TeamLease Regtech login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team TeamLease Regtech");
                                    //SendGridEmailManager.SendGridMail(SenderEmailAddress, "Teamlease Regtech", new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for TeamLease Regtech login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team TeamLease Regtech");
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                }
                                if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                {
                                    try
                                    {
                                        UserContact = "Yes";
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                        Success = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        Success = false;
                                    }
                                }
                                if (Success)
                                {
                                    using (var client = new HttpClient())
                                    {
                                        var form = new Dictionary<string, string>
                                            {
                                                {"grant_type", "password"},
                                                {"username", user.Email},
                                                {"password", user.Password},
                                            };
                                        var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                                        var token = tokenResponse.Content.ReadAsAsync<Models.Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                                        if (string.IsNullOrEmpty(token.Error))
                                        {
                                            setAccessToken = "bearer " + token.AccessToken;
                                        }
                                        else
                                        {
                                            setAccessToken = "bearer " + token.Error;
                                        }
                                    }
                                    string Notificationstatus = string.Empty;
                                    string UserEmail = string.Empty;
                                   
                                        var chknotification = (from row in entities.MaintainDeviceIDs
                                                               where row.DeviceIDIMEI == DeviceIDIMEI
                                                               && row.Status == "Active"
                                                               && row.UserID != user.ID
                                                               select row).FirstOrDefault();
                                        if (chknotification != null)
                                        {
                                            string checkexsitemail = (from row in entities.Users
                                                                      where row.ID == chknotification.UserID
                                                                      select row.Email).FirstOrDefault();
                                            Notificationstatus = "True";
                                            UserEmail = checkexsitemail;
                                        }
                                        else
                                        {
                                            Notificationstatus = "False";
                                            UserEmail = "No";
                                        }
                                   
                                        if (UserEmail != null)
                                        {
                                            people.Add(new LoginDetails { status = "True", msg = "OTP", tokendetail = setAccessToken, CurrentEmailName = user.FirstName + " " + user.LastName, Notificationstatus = Notificationstatus, UserEmail = UserEmail, UserContact = UserContact });
                                        }
                                        else
                                        {
                                            people.Add(new LoginDetails { status = "True", msg = "OTP", tokendetail = setAccessToken, CurrentEmailName = user.FirstName + " " + user.LastName, Notificationstatus = Notificationstatus, UserEmail = "No", UserContact = UserContact });
                                        }
                                        UserManagement.LastLoginUpdate(Email.Trim());
                                       // ResendOTPobj.Add(new ResendOTP { status = "OTP sent successfully." });
                                }
                                else
                                {
                                    people.Add(new LoginDetails { status = "False", msg = "Mobile Number is not available.", tokendetail = setAccessToken, CurrentEmailName = user.FirstName + " " + user.LastName, Notificationstatus = "False", UserEmail = "No", UserContact = UserContact });
                                    //ResendOTPobj.Add(new ResendOTP { status = "False" });
                                }
                            }
                        }
                        else
                        {
                            people.Add(new LoginDetails { status = "False", msg = "User not exist.", tokendetail = setAccessToken, CurrentEmailName = "", Notificationstatus = "False", UserEmail = "No", UserContact = UserContact });
                            //ResendOTPobj.Add(new ResendOTP { status = "User not exist." });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(people).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}
