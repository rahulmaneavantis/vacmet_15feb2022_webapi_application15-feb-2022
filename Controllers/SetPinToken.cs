﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppWebApplication.Controllers
{
    public class SetPinToken
    {
        public string status { get; set; }
        public string DifferenceDate { get; set; }
        public string UpdatedChangePassword { get; set; }        
        public string setAccessToken { get; set; }
    }
}
