﻿using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon;
using Newtonsoft.Json;

namespace AppWebApplication.Controllers
{
    public class LitigationController : ApiController
    {
        //public static string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
        public static string key = "avantis";


        [Route("Litigation/GetDocumentsByDocTypeID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetDocumentsByDocTypeID(long noticeCaseInstanceID, string DocType)
        {

            List<int> AllDocumentid;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (DocType == "A")
                {
                    AllDocumentid = (from row in entities.tbl_LitigationFileData
                                     where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                     && row.IsDeleted == false
                                     select row.ID).ToList();

                }
                else
                {
                    AllDocumentid = (from row in entities.tbl_LitigationFileData
                                     where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                     && row.DocType == DocType
                                     && row.IsDeleted == false
                                     select row.ID).ToList();
                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(AllDocumentid).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/AdvocateBillAuditLog")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AdvocateBillAuditLog(long CaseInstanceID, string InvoiceNo)
        {
            try
            {
                List<tbl_CaseAdvocateBillAuditLog> objcases = new List<tbl_CaseAdvocateBillAuditLog>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objcases = (from row in entities.tbl_CaseAdvocateBillAuditLog
                                where row.IsDeleted == false
                                && row.InvoiceNo.Trim() == InvoiceNo.Trim()
                                && row.NoticeCaseInstanceID == CaseInstanceID
                                select row).OrderBy(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/KendoRiskData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoRiskData(int CustId, int RiskTypeID, int StatusID, string TypeName,int UserID)
        {
            try
            {
                List<View_LitigationDashboard> objcases = new List<View_LitigationDashboard>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objcases = (from row in entities.View_LitigationDashboard
                                where row.RiskType != null && row.RiskTypeID != null
                                && row.RiskTypeID != -1
                                && row.CustomerID == CustId
                                select row).Distinct().ToList();

                    if (TypeName != "-1" && TypeName != "0" && TypeName != "1" && TypeName != "Both")
                    {
                        objcases = objcases.Where(entry => entry.TypeName == TypeName).ToList();
                    }
                    if (StatusID != -1 && StatusID != 0)
                    {
                        objcases = objcases.Where(entry => entry.TxnStatusID == StatusID).ToList();
                    }
                    if (RiskTypeID != -1 && RiskTypeID != 0)
                    {
                        objcases = objcases.Where(entry => entry.RiskTypeID == RiskTypeID).ToList();
                    }

                    var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(CustId), UserID, "", -1);
                    if (caseList.Count > 0)
                    {
                        objcases = objcases.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                    }
                    else
                    {
                        objcases = objcases.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                    }

                    objcases = objcases.Where(entry => entry.RoleID == 3).ToList();

                    if (objcases.Count > 0)
                    {
                        objcases = (from g in objcases
                                    group g by new
                                    {
                                        g.Type,
                                        g.NoticeInstanceID,
                                        g.NoticeTitle,
                                        g.NoticeRiskID,
                                        g.NoticeType,
                                        g.NoticeTypeName,
                                        g.TxnStatusID,
                                        g.TotalPayment,
                                        g.CustomerID,
                                        g.CustomerBranchID,
                                        g.BranchName,
                                        g.DepartmentID,
                                        g.DeptName,
                                        g.PartyID,
                                        g.PartyName,
                                        g.NoticeCategoryID,
                                        g.NoticeCategory,
                                        g.CaseStageID,
                                        g.CaseStage,
                                        g.AgeingDays,
                                        g.RiskType,
                                        g.RiskTypeID,
                                        g.Status,
                                        g.TypeName,
                                        g.NoticeDetailDesc,
                                        g.RefNo
                                    } into GCS
                                    select new View_LitigationDashboard()
                                    {
                                        Type = GCS.Key.Type,
                                        NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                        NoticeType = GCS.Key.NoticeType,
                                        NoticeTypeName = GCS.Key.NoticeTypeName,
                                        NoticeTitle = GCS.Key.NoticeTitle,
                                        NoticeRiskID = GCS.Key.NoticeRiskID,
                                        TxnStatusID = GCS.Key.TxnStatusID,
                                        TotalPayment = GCS.Key.TotalPayment,
                                        CustomerBranchID = GCS.Key.CustomerBranchID,
                                        BranchName = GCS.Key.BranchName,
                                        DepartmentID = GCS.Key.DepartmentID,
                                        DeptName = GCS.Key.DeptName,
                                        PartyID = GCS.Key.PartyID,
                                        PartyName = GCS.Key.PartyName,
                                        NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                        NoticeCategory = GCS.Key.NoticeCategory,
                                        CaseStageID = GCS.Key.CaseStageID,
                                        CaseStage = GCS.Key.CaseStage,
                                        AgeingDays = GCS.Key.AgeingDays,
                                        CustomerID = GCS.Key.CustomerID,
                                        RiskType = GCS.Key.RiskType,
                                        RiskTypeID = GCS.Key.RiskTypeID,
                                        Status = GCS.Key.Status,
                                        TypeName = GCS.Key.TypeName,
                                        NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                        RefNo = GCS.Key.RefNo
                                    }).ToList();
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        [Route("Litigation/KendoAdvbillApproverEmail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoAdvbillApproverEmail(int CustId)
        {
            try
            {
                List<tbl_Advcatebillapprovers> objcases = new List<tbl_Advcatebillapprovers>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objcases = (from row in entities.tbl_Advcatebillapprovers
                                where row.ApproverEmail != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(x => x.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/Delete_AdvbillApproverEmail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_AdvbillApproverEmail(int ID, string ApproverEmail, string ApproverLevel, string IsDeleted, string CreatedOn, int CreatedBy, string UpdatedBy, string UpdatedOn, int CustomerID)
        {

            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ApproverEmail))
                {
                    bool result = LitigationManagement.DeleteAdvbillApproverEmail(ID, CustomerID);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = ApproverEmail });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = ApproverEmail });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Litigation/Edit_AdvbillApproverEmail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Edit_AdvbillApproverEmail(int ID, string ApproverEmail, string ApproverLevel, string IsDeleted, string CreatedOn, int CreatedBy, string UpdatedBy, string UpdatedOn, int CustomerID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ApproverEmail))
                {
                    bool result = LitigationManagement.EditAdvbillApproverEmail(ApproverEmail.Trim(), CreatedBy, CustomerID, ID, "1");
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = ApproverEmail });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = ApproverEmail });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/Add_AdvbillApproverEmail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_AdvbillApproverEmail(int CreatedBy, int CustomerID,string ID, string ApproverLevel, string ApproverEmail)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ApproverEmail))
                {
                    bool result = LitigationManagement.AddAdvbillApproverEmail(ApproverEmail.Trim(), ApproverLevel, CreatedBy, CustomerID,Convert.ToInt32(ID), "0");
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = ApproverEmail });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = ApproverEmail });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/RoleTypes")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage RoleTypes(int CustId)
        {
            try
            {
                List<OutputDetail> obj = new List<OutputDetail>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Roles = (from row in entities.Roles
                                 where row.IsForLitigation != null
                                 select row).ToList();
                    Roles = Roles.Where(x => x.IsForLitigation == true).ToList();

                    if (Roles.Count > 0)
                    {
                        obj = (from row in Roles
                               select new OutputDetail()
                               {
                                   Name = row.Name,
                               }).ToList();
                    }

                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        [Route("Litigation/KendoNoticeStage")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoNoticeStage(int CustId)
        {
            try
            {
                List<tbl_NoticeStage> objcases = new List<tbl_NoticeStage>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objcases = (from row in entities.tbl_NoticeStage
                                where row.NoticeStage != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderByDescending(entry => entry.ID).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("Litigation/Edit_NoticeStages")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Edit_NoticeStages(int ID, string NoticeStage, string IsDeleted, string CreatedOn, int CreatedBy, string UpdatedBy, string UpdatedOn, int CustomerID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(NoticeStage))
                {
                    bool result = LitigationManagement.AddNoticeStages(NoticeStage.Trim(), CreatedBy, CustomerID, ID, "1");
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = NoticeStage });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = NoticeStage });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/Add_NoticeStages")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_NoticeStages(int CreatedBy, int CustomerID, string ID, string NoticeStage)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(NoticeStage))
                {
                    bool result = LitigationManagement.AddNoticeStages(NoticeStage.Trim(), CreatedBy, CustomerID, Convert.ToInt32(ID), "0");
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = NoticeStage });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = NoticeStage });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/Delete_NoticeStagesByID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_NoticeStagesByID(string ID, string NoticeStage, string IsDeleted, string CreatedOn, int CreatedBy, string UpdatedBy, string UpdatedOn, int CustomerID)
        {

            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    bool result = LitigationManagement.DeleteNoticeStagesByID(Convert.ToInt32(ID), CustomerID);
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True", MessageType = NoticeStage });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False", MessageType = NoticeStage });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/MailAuthorizationUserListKendo")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MailAuthorizationUserListKendo(int CustId)
        {
            try
            {
                List<SP_LitiUserDetails_Result> viewUserDetails = new List<SP_LitiUserDetails_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    viewUserDetails = (from row in entities.SP_LitiUserDetails(CustId)
                                       where row.UserType != null
                                       select row).ToList();
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(viewUserDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        [Authorize]
        [Route("Litigation/LitigationUserList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LitigationUserList(int CustId)
        {
            try
            {
                List<SP_LitiUserDetails_Result> viewUserDetails = new List<SP_LitiUserDetails_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    viewUserDetails = (from row in entities.SP_LitiUserDetails(CustId)
                                       where row.UserType != null
                                       && row.LitigationRole!=null
                                       && row.LitigationRole != ""
                                       select row).ToList();
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(viewUserDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        //[Authorize]
        [Route("Litigation/EditMailAuthorizationUserList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage EditMailAuthorizationUserList(Nullable<int> ID, string MailServicesID, string UserName, string Email, string Role, string Code, string UserType, string LitigationRole)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                bool result = false;
                if (!string.IsNullOrEmpty(MailServicesID))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var UserDetails = (from row in entities.tbl_LitiMailAllowedOrNot
                                           where row.UserID == ID
                                           select row).FirstOrDefault();

                        if (UserDetails != null)
                        {

                            if (UserName == "enable")
                            {

                                var splitValuesEnable = MailServicesID.Split(',');

                                for (var i = 0; i < splitValuesEnable.Length; i++)
                                {
                                    if (!UserDetails.MailServicesID.Contains(splitValuesEnable[i]))
                                    {
                                        UserDetails.MailServicesID = UserDetails.MailServicesID + splitValuesEnable[i] + ",";
                                    }
                                }

                            }
                            if (UserName == "disable")
                            {
                                var splitValuesDisable = MailServicesID.Split(',');

                                for (var i = 0; i < splitValuesDisable.Length; i++)
                                {
                                    if (UserDetails.MailServicesID.Contains(splitValuesDisable[i]))
                                    {
                                        UserDetails.MailServicesID = UserDetails.MailServicesID.Replace((splitValuesDisable[i] + ','), "");
                                    }
                                }
                            }

                            entities.SaveChanges();
                            result = true;
                        }
                        else
                        {
                            if (UserName == "enable")
                            {
                                tbl_LitiMailAllowedOrNot obj = new tbl_LitiMailAllowedOrNot()
                                {
                                    UserID = ID,
                                    MailServicesID = MailServicesID + ",",
                                    IsDeleted = false
                                };
                                entities.tbl_LitiMailAllowedOrNot.Add(obj);
                                entities.SaveChanges();
                                result = true;
                            }
                        }
                    }

                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/GetCalenderComplianceDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCalenderComplianceDetails(long UserID, long customerID,
        string datevalue, string FlagIsApp)
        {
            try
            {
                var TodayDatetime = DateTime.Now;
                var TodayDate = TodayDatetime.Date;
                DateTime date = new DateTime();
                if (!string.IsNullOrEmpty(datevalue))
                {
                    date = Convert.ToDateTime(datevalue);
                }
                List<View_LitigationCaseResponse> viewHearingDetails = new List<View_LitigationCaseResponse>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    if (!string.IsNullOrEmpty(datevalue))
                    {
                        if (date < TodayDate)
                        {
                            viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                  //&& row.ResponseDate != date
                                                  && row.ReminderDate == date
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                            if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
                                viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
                            else // In case of MGMT or CADMN
                            {
                                viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                            }

                        }
                        else
                        {
                            viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                  && row.ReminderDate == date
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                            if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
                                viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
                            else // In case of MGMT or CADMN
                            {
                                viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                            }
                        }
                    }
                    else
                    {
                        viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                              where row.CustomerID == customerID
                                              && row.ReminderDate != null
                                              //  && row.ReminderDate >= TodayDate
                                              && row.TxnStatusID != 3 //Only Open Cases
                                              select row).ToList();
                        if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
                            viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
                        else // In case of MGMT or CADMN
                        {
                            viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                        }

                        var ChekNextHearing = (from row in viewHearingDetails
                                               where row.CustomerID == customerID
                                               && row.ReminderDate != null
                                                && row.ReminderDate >= TodayDate
                                               && row.TxnStatusID != 3 //Only Open Cases
                                               select row).ToList();
                        if (ChekNextHearing.Count == 0)
                        {


                            viewHearingDetails = (from row in viewHearingDetails
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null

                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                        }
                        else
                        {
                            viewHearingDetails = (from row in viewHearingDetails
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                    && row.ReminderDate >= TodayDate
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                        }
                    }
                }


                if (viewHearingDetails.Count > 0)
                {
                    viewHearingDetails = (from g in viewHearingDetails
                                          group g by new
                                          {
                                              g.NoticeCaseInstanceID,
                                              g.Title,
                                              g.CaseRefNo,
                                              g.CourtID,
                                              g.CourtName,
                                              g.CaseType,
                                              g.CustomerID,
                                              g.CustomerBranchID,
                                              g.BranchName,
                                              g.DepartmentID,
                                              g.CaseRiskID,
                                              g.PartyID,
                                              g.PartyName,
                                              // g.Party,
                                              g.TxnStatusID,
                                              g.ReminderDate,
                                              g.lstAssignedTo,
                                              g.ResponseID
                                          } into GCS
                                          select new View_LitigationCaseResponse()
                                          {
                                              NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                              Title = GCS.Key.Title,
                                              CaseRefNo = GCS.Key.CaseRefNo,
                                              CourtID = GCS.Key.CourtID,
                                              CourtName = GCS.Key.CourtName,
                                              CaseType = GCS.Key.CaseType,
                                              CustomerID = GCS.Key.CustomerID,
                                              CustomerBranchID = GCS.Key.CustomerBranchID,
                                              BranchName = GCS.Key.BranchName,
                                              DepartmentID = GCS.Key.DepartmentID,
                                              CaseRiskID = GCS.Key.CaseRiskID,
                                              PartyID = GCS.Key.PartyID,
                                              PartyName = GCS.Key.PartyName,
                                              // Party = GCS.Key.Party,
                                              TxnStatusID = GCS.Key.TxnStatusID,
                                              ReminderDate = GCS.Key.ReminderDate,
                                              lstAssignedTo = GCS.Key.lstAssignedTo,
                                              ResponseID = GCS.Key.ResponseID
                                          }).ToList();
                    int statusflag = 2;
                    //if ( IsEntityassignmentCustomer == Convert.ToString(customerID))
                    ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    if (isexist != null)
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), Convert.ToInt32(UserID), FlagIsApp, statusflag);
                        //if (caseList.Count > 0)
                        //{
                            viewHearingDetails = viewHearingDetails.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(viewHearingDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }


        //[Authorize]
        //[Route("Litigation/GetCalenderComplianceDetails")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetCalenderComplianceDetails(long UserID, long customerID,
        //string datevalue, string FlagIsApp)
        //{
        //    try
        //    {
        //        var TodayDatetime = DateTime.Now;
        //        var TodayDate = TodayDatetime.Date;
        //        DateTime date = new DateTime();
        //        if (!string.IsNullOrEmpty(datevalue))
        //        {
        //            date = Convert.ToDateTime(datevalue);
        //        }
        //        List<View_LitigationCaseResponse> viewHearingDetails = new List<View_LitigationCaseResponse>();

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            if (!string.IsNullOrEmpty(datevalue))
        //            {
        //                if (date < TodayDate)
        //                {
        //                    viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                          && row.ResponseDate != date
        //                                          && row.ReminderDate == date
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                    if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                        viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                    else // In case of MGMT or CADMN
        //                    {
        //                        viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                    }


        //                }
        //                else
        //                {
        //                    viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                          && row.ReminderDate == date
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                    if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                        viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                    else // In case of MGMT or CADMN
        //                    {
        //                        viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                      where row.CustomerID == customerID
        //                                      && row.ReminderDate != null
        //                                      //  && row.ReminderDate >= TodayDate
        //                                      && row.TxnStatusID != 3 //Only Open Cases
        //                                      select row).ToList();
        //                if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                    viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                else // In case of MGMT or CADMN
        //                {
        //                    viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                }

        //                var ChekNextHearing = (from row in viewHearingDetails
        //                                       where row.CustomerID == customerID
        //                                       && row.ReminderDate != null
        //                                        && row.ReminderDate >= TodayDate
        //                                       && row.TxnStatusID != 3 //Only Open Cases
        //                                       select row).ToList();
        //                if (ChekNextHearing.Count == 0)
        //                {


        //                    viewHearingDetails = (from row in viewHearingDetails
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null

        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                }
        //                else
        //                {
        //                    viewHearingDetails = (from row in viewHearingDetails
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                            && row.ReminderDate >= TodayDate
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                }
        //            }
        //        }


        //        if (viewHearingDetails.Count > 0)
        //        {
        //            viewHearingDetails = (from g in viewHearingDetails
        //                                  group g by new
        //                                  {
        //                                      g.NoticeCaseInstanceID,
        //                                      g.Title,
        //                                      g.CaseRefNo,
        //                                      g.CourtID,
        //                                      g.CourtName,
        //                                      g.CaseType,
        //                                      g.CustomerID,
        //                                      g.CustomerBranchID,
        //                                      g.BranchName,
        //                                      g.DepartmentID,
        //                                      g.CaseRiskID,
        //                                      g.PartyID,
        //                                      g.PartyName,
        //                                      // g.Party,
        //                                      g.TxnStatusID,
        //                                      g.ReminderDate,
        //                                      g.lstAssignedTo,
        //                                      g.ResponseID
        //                                  } into GCS
        //                                  select new View_LitigationCaseResponse()
        //                                  {
        //                                      NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                                      Title = GCS.Key.Title,
        //                                      CaseRefNo = GCS.Key.CaseRefNo,
        //                                      CourtID = GCS.Key.CourtID,
        //                                      CourtName = GCS.Key.CourtName,
        //                                      CaseType = GCS.Key.CaseType,
        //                                      CustomerID = GCS.Key.CustomerID,
        //                                      CustomerBranchID = GCS.Key.CustomerBranchID,
        //                                      BranchName = GCS.Key.BranchName,
        //                                      DepartmentID = GCS.Key.DepartmentID,
        //                                      CaseRiskID = GCS.Key.CaseRiskID,
        //                                      PartyID = GCS.Key.PartyID,
        //                                      PartyName = GCS.Key.PartyName,
        //                                      // Party = GCS.Key.Party,
        //                                      TxnStatusID = GCS.Key.TxnStatusID,
        //                                      ReminderDate = GCS.Key.ReminderDate,
        //                                      lstAssignedTo = GCS.Key.lstAssignedTo,
        //                                      ResponseID = GCS.Key.ResponseID
        //                                  }).ToList();
        //        }
        //        if (FlagIsApp == "MGMT")
        //        {
        //            var caseList = CustomerBranchManagement.GetAssignedCaseNoticeLitigation(Convert.ToInt32(customerID), Convert.ToInt32(UserID), FlagIsApp);
        //            if (caseList.Count > 0)
        //            {
        //                viewHearingDetails = viewHearingDetails.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(viewHearingDetails).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }

        //}



        //[Authorize] 
        //[Route("Litigation/GetCalenderComplianceDetails")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetCalenderComplianceDetails(long UserID, long customerID,
        //string datevalue, string FlagIsApp)
        //{
        //    try
        //    {
        //        var TodayDatetime = DateTime.Now;
        //        var TodayDate = TodayDatetime.Date;
        //        DateTime date = new DateTime();
        //        if (!string.IsNullOrEmpty(datevalue))
        //        {
        //            date = Convert.ToDateTime(datevalue);
        //        }
        //        List<View_LitigationCaseResponse> viewHearingDetails = new List<View_LitigationCaseResponse>();

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            if (!string.IsNullOrEmpty(datevalue))
        //            {
        //                if (date < TodayDate)
        //                {
        //                    viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                          && row.ResponseDate != date
        //                                          && row.ReminderDate == date
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                    if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                        viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                    else // In case of MGMT or CADMN
        //                    {
        //                        viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                    }

        //                }
        //                else
        //                {
        //                    viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                          && row.ReminderDate == date
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                    if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                        viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                    else // In case of MGMT or CADMN
        //                    {
        //                        viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                viewHearingDetails = (from row in entities.View_LitigationCaseResponse
        //                                      where row.CustomerID == customerID
        //                                      && row.ReminderDate != null
        //                                      //  && row.ReminderDate >= TodayDate
        //                                      && row.TxnStatusID != 3 //Only Open Cases
        //                                      select row).ToList();
        //                if (FlagIsApp != "MGMT" && FlagIsApp != "CADMN")
        //                    viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID)).ToList();
        //                else // In case of MGMT or CADMN
        //                {
        //                    viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
        //                }

        //                var ChekNextHearing = (from row in viewHearingDetails
        //                                       where row.CustomerID == customerID
        //                                       && row.ReminderDate != null
        //                                        && row.ReminderDate >= TodayDate
        //                                       && row.TxnStatusID != 3 //Only Open Cases
        //                                       select row).ToList();
        //                if (ChekNextHearing.Count == 0)
        //                {


        //                    viewHearingDetails = (from row in viewHearingDetails
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null

        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                }
        //                else
        //                {
        //                    viewHearingDetails = (from row in viewHearingDetails
        //                                          where row.CustomerID == customerID
        //                                          && row.ReminderDate != null
        //                                            && row.ReminderDate >= TodayDate
        //                                          && row.TxnStatusID != 3 //Only Open Cases
        //                                          select row).ToList();
        //                }
        //            }
        //        }


        //        if (viewHearingDetails.Count > 0)
        //        {
        //            viewHearingDetails = (from g in viewHearingDetails
        //                                  group g by new
        //                                  {
        //                                      g.NoticeCaseInstanceID,
        //                                      g.Title,
        //                                      g.CaseRefNo,
        //                                      g.CourtID,
        //                                      g.CourtName,
        //                                      g.CaseType,
        //                                      g.CustomerID,
        //                                      g.CustomerBranchID,
        //                                      g.BranchName,
        //                                      g.DepartmentID,
        //                                      g.CaseRiskID,
        //                                      g.PartyID,
        //                                      g.PartyName,
        //                                      // g.Party,
        //                                      g.TxnStatusID,
        //                                      g.ReminderDate,
        //                                      g.lstAssignedTo,
        //                                      g.ResponseID
        //                                  } into GCS
        //                                  select new View_LitigationCaseResponse()
        //                                  {
        //                                      NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                                      Title = GCS.Key.Title,
        //                                      CaseRefNo = GCS.Key.CaseRefNo,
        //                                      CourtID = GCS.Key.CourtID,
        //                                      CourtName = GCS.Key.CourtName,
        //                                      CaseType = GCS.Key.CaseType,
        //                                      CustomerID = GCS.Key.CustomerID,
        //                                      CustomerBranchID = GCS.Key.CustomerBranchID,
        //                                      BranchName = GCS.Key.BranchName,
        //                                      DepartmentID = GCS.Key.DepartmentID,
        //                                      CaseRiskID = GCS.Key.CaseRiskID,
        //                                      PartyID = GCS.Key.PartyID,
        //                                      PartyName = GCS.Key.PartyName,
        //                                      // Party = GCS.Key.Party,
        //                                      TxnStatusID = GCS.Key.TxnStatusID,
        //                                      ReminderDate = GCS.Key.ReminderDate,
        //                                      lstAssignedTo = GCS.Key.lstAssignedTo,
        //                                      ResponseID = GCS.Key.ResponseID
        //                                  }).ToList();
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(viewHearingDetails).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }

        //}

        [Authorize] 
        [Route("Litigation/myDocumentFiltersFiletag")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myDocumentFiltersFiletag(int customerID, int litigationInstanceID, int UserID, string Role, string ddtype)
        {
            List<myfiletag> objcourt = new List<myfiletag>();

            try
            {
                if (ddtype == "C")
                {
                    
                    var lstTagsCase = LitigationDocumentManagement.GetDistinctFileTagsCaseMyDocument(customerID, litigationInstanceID, UserID, Role, ddtype);
                    if (lstTagsCase.Count > 0)
                    {
                        objcourt = (from row in lstTagsCase
                                    select new myfiletag()
                                    {
                                        filetag = row.FileTag
                                    }).ToList();
                    }
                }
                if (ddtype == "N")
                {
                    var lstTags = LitigationDocumentManagement.GetDistinctFileTagsNoticeMyDocument(customerID, litigationInstanceID, UserID, Role, ddtype);

                    if (lstTags.Count > 0)
                    {
                        objcourt = (from row in lstTags
                                    select new myfiletag()
                                    {
                                        filetag = row.FileTag
                                    }).ToList();
                    }
                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcourt).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/TypeList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TypeList(int CutomerID, string Type)
        {
            try
            {
                bool CaseResult = LitigationDocumentManagement.CheckForClientNew(CutomerID, "CaseNoticeLabel");
                List<tbl_TypeMaster> caseTypeList = new List<tbl_TypeMaster>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (Type == "2")
                    {
                        if (CaseResult == true)
                        {
                            caseTypeList = (from row in entities.tbl_TypeMaster
                                            where row.IsActive == true
                                            && row.TypeCode == "Re"
                                            select row).ToList();
                        }
                        else
                        {
                            caseTypeList = (from row in entities.tbl_TypeMaster
                                            where row.IsActive == true
                                                  && row.TypeCode == "Re"
                                            select row).ToList();
                            caseTypeList = caseTypeList.Where(entry => entry.customerID == null).ToList();
                        }

                    }
                    else
                    {
                        caseTypeList = (from row in entities.tbl_TypeMaster
                                        where row.IsActive == true
                                              && row.TypeCode == "Re"
                                        select row).ToList();
                        caseTypeList = caseTypeList.Where(entry => entry.customerID == null).ToList();
                    }


                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(caseTypeList).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        //[Route("Litigation/TypeList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage TypeList(int CutomerID, string Type)
        //{
        //    try
        //    {
        //        bool CaseResult = LitigationDocumentManagement.CheckForClientNew(CutomerID, "CaseNoticeLabel");
        //        List<tbl_TypeMaster> caseTypeList = new List<tbl_TypeMaster>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (Type == "2")
        //            {
        //                if (CaseResult == true)
        //                {
        //                    caseTypeList = (from row in entities.tbl_TypeMaster
        //                                    where row.IsActive == true
        //                                    && row.TypeCode == "Re"
        //                                    select row).ToList();
        //                }
        //                else
        //                {
        //                    caseTypeList = (from row in entities.tbl_TypeMaster
        //                                    where row.IsActive == true
        //                                          && row.TypeCode == "Re"
        //                                    select row).ToList();
        //                    caseTypeList = caseTypeList.Where(entry => entry.customerID == null).ToList();
        //                }

        //            }
        //            else
        //            {
        //                caseTypeList = (from row in entities.tbl_TypeMaster
        //                                where row.IsActive == true
        //                                      && row.TypeCode == "Re"
        //                                select row).ToList();
        //                caseTypeList = caseTypeList.Where(entry => entry.customerID == null).ToList();
        //            }


        //            return new HttpResponseMessage()
        //            {
        //                Content = new StringContent(JArray.FromObject(caseTypeList).ToString(), Encoding.UTF8, "application/json")
        //            };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        [Authorize]
        [Route("Litigation/UpcomingDashBoardHearing")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpcomingDashBoardHearing([FromBody] UpcomingdashBoardHearings hearingdashobj)
        {
            string Email = hearingdashobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<View_LitigationCaseResponse> View_LitigationCaseResponseobj = new List<View_LitigationCaseResponse>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();
                            int branchID = -1;
                            int partyID = -1;
                            int deptID = -1;
                            int catID = -1;
                            int riskID = -1;
                            int stageID = -1;
                            int noticeCaseStatus = -1;


                            string noticeCaseType = string.Empty;
                            string filterNoticeOrCase = string.Empty;
                            var TodayDatetime = DateTime.Now;
                            var TodayDate = TodayDatetime.Date;


                            if (hearingdashobj.Dt != "null")
                            {
                                DateTime date = new DateTime();
                                date = Convert.ToDateTime(hearingdashobj.Dt);
                                if (date < TodayDate)
                                {
                                    View_LitigationCaseResponseobj = (from row in entities.View_LitigationCaseResponse
                                                                      where row.CustomerID == customerId
                                                                      && row.ReminderDate != null
                                                                      && row.ResponseDate != date
                                                                      && row.ReminderDate == date
                                                                      && row.TxnStatusID != 3 //Only Open Cases
                                                                      select row).ToList();

                                }
                                else
                                {
                                    View_LitigationCaseResponseobj = (from row in entities.View_LitigationCaseResponse
                                                                      where row.CustomerID == customerId
                                                                      && row.ReminderDate != null
                                                                      && row.ReminderDate == date
                                                                      && row.TxnStatusID != 3 //Only Open Cases
                                                                      select row).ToList();
                                }
                            }
                            else
                            {
                                View_LitigationCaseResponseobj = (from row in entities.View_LitigationCaseResponse
                                                                  where row.CustomerID == customerId
                                                                  && row.ReminderDate != null
                                                                  //&& row.ReminderDate >= TodayDate
                                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                                  select row).ToList();
                            }

                            var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerId), branchID);

                            if (RoleCheck != "MGMT" && RoleCheck != "CADMN")
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => (entry.UserID == Convert.ToInt32(user.ID) && entry.RoleID == 3) || (entry.OwnerID == Convert.ToInt32(user.ID))).ToList();
                            else // In case of MGMT or CADMN 
                            {
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.RoleID == 3).ToList();
                            }
                            if (View_LitigationCaseResponseobj.Count > 0)
                            {
                                View_LitigationCaseResponseobj = (from g in View_LitigationCaseResponseobj
                                                                  group g by new
                                                                  {
                                                                      g.NoticeCaseInstanceID,
                                                                      g.Title,
                                                                      g.CaseRefNo,
                                                                      g.CourtID,
                                                                      g.CourtName,
                                                                      g.CaseType,
                                                                      g.CustomerID,
                                                                      g.CustomerBranchID,
                                                                      g.BranchName,
                                                                      g.DepartmentID,
                                                                      g.CaseRiskID,
                                                                      g.PartyID,
                                                                      g.PartyName,
                                                                      // g.Party,
                                                                      g.TxnStatusID,
                                                                      g.ReminderDate,
                                                                      g.lstAssignedTo,
                                                                      g.ResponseID,
                                                                      g.Description,
                                                                      //g.AssignedTo,
                                                                      g.Department
                                                                  } into GCS
                                                                  select new View_LitigationCaseResponse()
                                                                  {
                                                                      NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                                                      Title = GCS.Key.Title,
                                                                      CaseRefNo = GCS.Key.CaseRefNo,
                                                                      CourtID = GCS.Key.CourtID,
                                                                      CourtName = GCS.Key.CourtName,
                                                                      CaseType = GCS.Key.CaseType,
                                                                      CustomerID = GCS.Key.CustomerID,
                                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                                      BranchName = GCS.Key.BranchName,
                                                                      DepartmentID = GCS.Key.DepartmentID,
                                                                      CaseRiskID = GCS.Key.CaseRiskID,
                                                                      PartyID = GCS.Key.PartyID,
                                                                      PartyName = GCS.Key.PartyName,
                                                                      // Party = GCS.Key.Party,
                                                                      TxnStatusID = GCS.Key.TxnStatusID,
                                                                      ReminderDate = GCS.Key.ReminderDate,
                                                                      lstAssignedTo = GCS.Key.lstAssignedTo,
                                                                      ResponseID = GCS.Key.ResponseID,
                                                                      Description = GCS.Key.Description,
                                                                      //AssignedTo = GCS.Key.AssignedTo,
                                                                      Department = GCS.Key.Department
                                                                  }).ToList();
                            }

                            if (branchList.Count > 0)
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                            if (partyID != -1)
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                            if (deptID != -1)
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.DepartmentID == deptID).ToList();

                            if (riskID != -1 && riskID != 0)
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.CaseRiskID == riskID).ToList();

                            if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.CaseType == noticeCaseType).ToList();

                            if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                            {
                                if (noticeCaseStatus == 3) //3--Closed otherwise Open
                                    View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                                else
                                    View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.TxnStatusID < 3).ToList();
                            }

                            if (View_LitigationCaseResponseobj.Count > 0)
                            {
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.OrderBy(entry => entry.ReminderDate).ToList();
                            }

                            //GetDashboardUpcomingHearing(View_LitigationCaseResponseobj, Convert.ToInt32(user.ID), RoleCheck, 3, filterNoticeOrCase, noticeCaseType, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);
                            //if (View_LitigationCaseResponseobj.Count > 0)
                            //{
                            //    GetDashboardUpcomingHearing(View_LitigationCaseResponseobj, Convert.ToInt32(user.ID), RoleCheck, 3, filterNoticeOrCase, noticeCaseType, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);
                            //        int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                            //        var total = View_LitigationCaseResponseobj.Count;
                            //        var pageSize = PageSizeDyanamic;
                            //        var skip = pageSize * (hearingdashobj.PageID - 1);
                            //        var canPage = skip < total;
                            //    View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Skip(skip).Take(pageSize).ToList();

                            //}
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(View_LitigationCaseResponseobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        public void GetDashboardUpcomingHearing(List<View_LitigationCaseResponse> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {

                if (viewRecords.Count > 0)
                {
                    viewRecords = (from g in viewRecords
                                   group g by new
                                   {
                                       g.NoticeCaseInstanceID,
                                       g.Title,
                                       g.CaseRefNo,
                                       g.CourtID,
                                       g.CourtName,
                                       g.CaseType,
                                       g.CustomerID,
                                       g.CustomerBranchID,
                                       g.BranchName,
                                       g.DepartmentID,
                                       g.CaseRiskID,
                                       g.PartyID,
                                       g.PartyName,
                                       // g.Party,
                                       g.TxnStatusID,
                                       g.ReminderDate,
                                       g.lstAssignedTo,
                                       g.ResponseID,
                                    
                                   } into GCS
                                   select new View_LitigationCaseResponse()
                                   {
                                       NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                       Title = GCS.Key.Title,
                                       CaseRefNo = GCS.Key.CaseRefNo,
                                       CourtID = GCS.Key.CourtID,
                                       CourtName = GCS.Key.CourtName,
                                       CaseType = GCS.Key.CaseType,
                                       CustomerID = GCS.Key.CustomerID,
                                       CustomerBranchID = GCS.Key.CustomerBranchID,
                                       BranchName = GCS.Key.BranchName,
                                       DepartmentID = GCS.Key.DepartmentID,
                                       CaseRiskID = GCS.Key.CaseRiskID,
                                       PartyID = GCS.Key.PartyID,
                                       PartyName = GCS.Key.PartyName,
                                       // Party = GCS.Key.Party,
                                       TxnStatusID = GCS.Key.TxnStatusID,
                                       ReminderDate = GCS.Key.ReminderDate,
                                       lstAssignedTo = GCS.Key.lstAssignedTo,
                                       ResponseID = GCS.Key.ResponseID,
                                    
                                   }).ToList();
                }

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    viewRecords = viewRecords.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.CaseRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.CaseType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    viewRecords = viewRecords.OrderBy(entry => entry.ReminderDate).ToList();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        [Authorize]
        [Route("Litigation/myReminderAndroid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myReminderAndroid([FromBody] myreminderclass reminderobj)
        {
            string Email = reminderobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<View_LitigationReminderView> View_LitigationReminderViewObj = new List<View_LitigationReminderView>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddltype = reminderobj.ddltype;
                    int InstanceID = reminderobj.InstanceID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            int RoleId = Convert.ToInt32(user.RoleID);
                            string loggedInUserRole = (from row in entities.Roles
                                                       where row.ID == RoleId
                                                       select row.Code).FirstOrDefault();

                            View_LitigationReminderViewObj = (from row in entities.View_LitigationReminderView
                                                              where row.CustomerID == customerId
                                                              //&& row.UserID == user.ID
                                                              select row).ToList();

                            if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                                View_LitigationReminderViewObj = View_LitigationReminderViewObj.Where(entry => entry.UserID == user.ID).ToList();

                            if (View_LitigationReminderViewObj.Count > 0)
                            {
                                if (InstanceID != -1)              
                                    View_LitigationReminderViewObj = View_LitigationReminderViewObj.Where(x => x.InstanceID==InstanceID).ToList();

                                if (ddltype == 0)//notice                
                                    View_LitigationReminderViewObj = View_LitigationReminderViewObj.Where(x => x.Type.Equals("N")).ToList();

                                if (ddltype == 1)//Case                
                                    View_LitigationReminderViewObj = View_LitigationReminderViewObj.Where(x => x.Type.Equals("C")).ToList();

                                if (ddltype == 2)//task                {
                                    View_LitigationReminderViewObj = View_LitigationReminderViewObj.Where(x => x.Type.Equals("T")).ToList();
                            }

                            if (View_LitigationReminderViewObj.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = View_LitigationReminderViewObj.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (reminderobj.PageID - 1);
                                var canPage = skip < total;
                                View_LitigationReminderViewObj = View_LitigationReminderViewObj.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(View_LitigationReminderViewObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/myReminderFilterListAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myReminderFilterListAndriod([FromBody] myreminderFilterclass reminderFilterobj)
        {
            string Email = reminderFilterobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<OutputDetail> query2 = new List<OutputDetail>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddltype = reminderFilterobj.ddltype;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            if (ddltype == 0)//notice    
                            {
                                var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerid, Convert.ToInt32(user.ID), RoleCheck, RoleId, string.Empty);
                                if (lstNoticeDetails.Count > 0)
                                {
                                    query2 = (from row in lstNoticeDetails
                                              select new OutputDetail()
                                              {
                                                  ID = Convert.ToInt32(row.NoticeInstanceID),
                                                  Name = row.NoticeTitle.ToString(),
                                              }).ToList();
                                }
                            }


                            if (ddltype == 1)//Case   
                            {
                                var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerid, Convert.ToInt32(user.ID), RoleCheck, RoleId);
                                if (lstCaseDetails.Count > 0)
                                {
                                    query2 = (from row in lstCaseDetails
                                              select new OutputDetail()
                                              {
                                                  ID = Convert.ToInt32(row.CaseInstanceID),
                                                  Name = row.CaseTitle.ToString(),
                                              }).ToList();
                                }
                            }

                            if (ddltype == 2)//task                {
                            {
                                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(Convert.ToInt32(user.ID), RoleCheck, 3, customerid);
                                if (lstTaskDetails.Count > 0)
                                {
                                    query2 = (from row in lstTaskDetails
                                              select new OutputDetail()
                                              {
                                                  ID = Convert.ToInt32(row.TaskID),
                                                  Name = row.TaskTitle.ToString(),
                                              }).ToList();
                                }

                            }


                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

       
        [Route("Litigation/myDocumentAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myDocumentAndriod([FromBody]mydocumentclass mydocobj)
        {
            string Email = mydocobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<mydocumenttypeAndriod> query2 = new List<mydocumenttypeAndriod>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddltype = mydocobj.ddltype;
                    int ddlstatus = mydocobj.ddlstatus;
                    string dept = mydocobj.dept.ToString();
                    string location = mydocobj.location;
                    int IOType = mydocobj.IOType;
                    int partyid = mydocobj.partyid;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();


                            if (ddltype == 1)//case    
                            {
                                var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(customerid, Convert.ToInt32(user.ID), RoleCheck, RoleId, string.Empty, string.Empty, ddlstatus, string.Empty);

                                if (DocList.Count > 0)
                                {
                                    query2 = (from row in DocList
                                              select new mydocumenttypeAndriod()
                                              {
                                                  TypeName = row.TypeName,
                                                  RefNo = row.RefNo,
                                                  Title = row.Title,
                                                  PartyName = row.PartyName,
                                                  PartyID = row.PartyID,
                                                  DepartmentID = row.DepartmentID.ToString(),
                                                  CustomerBranchID = row.CustomerBranchID.ToString(),
                                                  Status = row.Status.ToString(),
                                                  InstanceID = row.NoticeCaseInstanceID.ToString()
                                              }).ToList();
                                }

                            }
                            if (ddltype == 0)//Notice    
                            {
                                var DocList1 = LitigationDocumentManagement.GetAllDocumentListofNotice(customerid, Convert.ToInt32(user.ID), RoleCheck, RoleId, string.Empty, string.Empty, ddlstatus, string.Empty);

                                if (DocList1.Count > 0)
                                {
                                    query2 = (from row in DocList1
                                              select new mydocumenttypeAndriod()
                                              {
                                                  TypeName = row.TypeName,
                                                  RefNo = row.RefNo,
                                                  Title = row.Title,
                                                  PartyName = row.PartyName,
                                                  PartyID = row.PartyID,
                                                  DepartmentID = row.DepartmentID.ToString(),
                                                  CustomerBranchID = row.CustomerBranchID.ToString(),
                                                  Status = row.Status.ToString(),
                                                  InstanceID = row.NoticeCaseInstanceID.ToString()

                                              }).ToList();
                                }

                            }
                            if (ddltype == 2)//Task    
                            {
                                var DocList2 = LitigationTaskManagement.GetAssignedTaskListforTag(customerid, Convert.ToInt32(user.ID), RoleCheck, RoleId, string.Empty, string.Empty, ddlstatus);

                                if (DocList2.Count > 0)
                                {
                                    query2 = (from row in DocList2
                                              select new mydocumenttypeAndriod()
                                              {
                                                  Priority = row.Priority,
                                                  Task = row.TaskTitle,
                                                  Task_Description = row.TaskDesc,
                                                  Due_date = row.ScheduleOnDate,
                                                  Status = row.Status,
                                                  Assigned_To = row.AssignToName,
                                                  InstanceID = row.TaskID.ToString()
                                              }).ToList();
                                }

                            }

                            if (dept != "-1")
                            {
                                query2 = query2.Where(entry => (entry.DepartmentID == dept)).ToList();
                            }
                            if (location != "-1")
                            {
                                query2 = query2.Where(entry => (entry.CustomerBranchID == location)).ToList();
                            }
                            if (IOType != -1)
                            {
                                if (ddltype == 0)//Notice   
                                {
                                    if (IOType == 1)
                                    {
                                        var Type = "Inward";
                                        query2 = query2.Where(entry => (entry.TypeName == Type)).ToList();
                                    }
                                    if (IOType == 0)
                                    {
                                        var Type = "Outward";
                                        query2 = query2.Where(entry => (entry.TypeName == Type)).ToList();
                                    }
                                }
                                else if (ddltype == 1)//case  
                                {
                                    if (IOType == 1)
                                    {
                                        var Type = "Defendant";
                                        query2 = query2.Where(entry => (entry.TypeName == Type)).ToList();
                                    }
                                    if (IOType == 0)
                                    {
                                        var Type = "Plaintiff";
                                        query2 = query2.Where(entry => (entry.TypeName == Type)).ToList();
                                    }
                                }
                            }
                            if (partyid != -1)
                            {
                                query2 = query2.Where(entry => (entry.PartyID == partyid.ToString())).ToList();
                            }
                            if (query2.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = query2.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mydocobj.PageID - 1);
                                var canPage = skip < total;
                                query2 = query2.Skip(skip).Take(pageSize).ToList();

                            }

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

       
        [Route("Litigation/MyNoticeWorkSpaceAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyNoticeWorkSpaceAndriod([FromBody]mynoticewcclass mynoticewcobj)
        {
            string Email = mynoticewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<View_NoticeAssignedInstanceNew> objResult = new List<View_NoticeAssignedInstanceNew>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddlstatus = mynoticewcobj.ddlstatus;
                    int IOType = mynoticewcobj.IOType;
                    int dept = mynoticewcobj.dept;
                    int location = mynoticewcobj.location;
                    string partyid = mynoticewcobj.PartyID.ToString();
                    int CaseTypeID = mynoticewcobj.CaseTypeID;
                    int OwnerID = mynoticewcobj.OwnerID;
                    int InstanceID = -1;
                    if (!string.IsNullOrEmpty(mynoticewcobj.InstanceID))
                    {
                        InstanceID = Convert.ToInt32(mynoticewcobj.InstanceID);
                    }
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            DateTime Fromdate = new DateTime();
                            DateTime Todate = new DateTime();
                            string FY = mynoticewcobj.FY.ToString();
                            int flagDate = 0;
                            int AflagDate = 0;
                            DateTime AFromdate = new DateTime();
                            DateTime ATodate = new DateTime();


                            if (FY != "0" && !String.IsNullOrEmpty(FY))
                            {
                                string[] y = FY.Split('-');
                                if (y.Length > 1)
                                {
                                    if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                                    {
                                        string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                                        string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                                        Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        flagDate = 1;
                                    }
                                }
                            }

                            if (RoleCheck.Equals("AUD"))
                            {
                                AflagDate = 1;
                                RoleCheck = "MGMT";
                                User uobj = UserManagement.GetByID(Convert.ToInt32(user.ID));
                                if (uobj != null)
                                {
                                    AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                                    ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                                }
                            }

                            objResult = NoticeManagement.GetAssignedNoticeListNew(Convert.ToInt32(user.ID), RoleCheck, 3, customerid, ddlstatus);
                            if (flagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.CreatedOn >= Fromdate && entry.CreatedOn <= Todate)).ToList();
                            }
                            if (AflagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.CreatedOn >= AFromdate && entry.CreatedOn <= ATodate)).ToList();
                            }
                            if (IOType != -1)
                            {
                                if (IOType == 1)
                                {
                                    var Type = "I";
                                    objResult = objResult.Where(entry => (entry.NoticeType == Type)).ToList();
                                }
                                if (IOType == 0)
                                {
                                    var Type = "O";
                                    objResult = objResult.Where(entry => (entry.NoticeType == Type)).ToList();
                                }
                            }
                            if (dept != -1)
                            {
                                objResult = objResult.Where(entry => (entry.DepartmentID == dept)).ToList();
                            }
                            if (location != -1)
                            {
                                objResult = objResult.Where(entry => (entry.CustomerBranchID == location)).ToList();
                            }
                            if (partyid != "-1")
                            {
                                objResult = objResult.Where(entry => (entry.PartyID == partyid)).ToList();
                            }
                            if (CaseTypeID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.NoticeCategoryID == CaseTypeID)).ToList();
                            }
                            if (OwnerID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.OwnerID == OwnerID)).ToList();
                            }
                            if (InstanceID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.NoticeInstanceID == InstanceID)).ToList();
                            }
                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mynoticewcobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();

                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

       
        [Route("Litigation/MyCaseWorkSpaceAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyCaseWorkSpaceAndriod([FromBody]mycasewcclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<View_CaseAssignedInstanceNew> objResult = new List<View_CaseAssignedInstanceNew>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddlstatus = mycasewcobj.ddlstatus;
                    int IOType = mycasewcobj.IOType;
                    int dept = mycasewcobj.dept;
                    int location = mycasewcobj.location;
                    string partyid = mycasewcobj.PartyID.ToString();
                    int CaseTypeID = mycasewcobj.CaseTypeID;
                    int OwnerID = mycasewcobj.OwnerID;
                    int InstanceID = -1;
                    if (!string.IsNullOrEmpty(mycasewcobj.InstanceID))
                    {
                        InstanceID = Convert.ToInt32(mycasewcobj.InstanceID);
                    }

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            DateTime Fromdate = new DateTime();
                            DateTime Todate = new DateTime();
                            string FY = mycasewcobj.FY.ToString();
                            int flagDate = 0;
                            int AflagDate = 0;
                            DateTime AFromdate = new DateTime();
                            DateTime ATodate = new DateTime();


                            if (FY != "0" && !String.IsNullOrEmpty(FY))
                            {
                                string[] y = FY.Split('-');
                                if (y.Length > 1)
                                {
                                    if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                                    {
                                        string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                                        string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                                        Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        flagDate = 1;
                                    }
                                }
                            }

                            if (RoleCheck.Equals("AUD"))
                            {
                                AflagDate = 1;
                                RoleCheck = "MGMT";
                                User uobj = UserManagement.GetByID(Convert.ToInt32(user.ID));
                                if (uobj != null)
                                {
                                    AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                                    ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                                }
                            }

                            objResult = CaseManagement.GetAssignedCaseListNew(Convert.ToInt32(user.ID), RoleCheck, 3, customerid, ddlstatus);
                            if (flagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                                //objResult = objResult.Where(entry => (entry.CreatedOn >= Fromdate && entry.CreatedOn <= Todate)).ToList();
                            }
                            if (AflagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                            }
                            if (IOType != -1)
                            {
                                if (IOType == 1)
                                {
                                    var Type = "I";
                                    objResult = objResult.Where(entry => (entry.CaseType == Type)).ToList();
                                }
                                if (IOType == 0)
                                {
                                    var Type = "O";
                                    objResult = objResult.Where(entry => (entry.CaseType == Type)).ToList();
                                }
                            }
                            if (dept != -1)
                            {
                                objResult = objResult.Where(entry => (entry.DepartmentID == dept)).ToList();
                            }
                            if (location != -1)
                            {
                                objResult = objResult.Where(entry => (entry.CustomerBranchID == location)).ToList();
                            }
                            if (partyid != "-1")
                            {
                                objResult = objResult.Where(entry => entry.PartyID.Contains(partyid)).ToList();
                                //objResult = objResult.Where(entry => (entry.PartyID == partyid)).ToList();
                            }
                            if (CaseTypeID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.CaseCategoryID == CaseTypeID)).ToList();
                            }
                            if (OwnerID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.OwnerID == OwnerID)).ToList();
                            }
                            if (InstanceID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.CaseInstanceID == InstanceID)).ToList();
                            }
                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mycasewcobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();

                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/MyTaskWorkSpaceAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskWorkSpaceAndriod([FromBody]mytaskwcclass mytaskwcobj)
        {
            string Email = mytaskwcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                
                List<View_NoticeCaseTaskDetail> objResult = new List<View_NoticeCaseTaskDetail>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int ddlstatus = mytaskwcobj.ddlstatus;
                    string StatusID = mytaskwcobj.StatusID;
                    int TaskID = -1;
                    if (!string.IsNullOrEmpty(mytaskwcobj.TaskID))
                    {
                        TaskID = Convert.ToInt32(mytaskwcobj.TaskID);
                    }
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            DateTime Fromdate = new DateTime();
                            DateTime Todate = new DateTime();
                            string FY = mytaskwcobj.FY.ToString();
                            int flagDate = 0;
                            int AflagDate = 0;
                            DateTime AFromdate = new DateTime();
                            DateTime ATodate = new DateTime();


                            if (FY != "0" && !String.IsNullOrEmpty(FY))
                            {
                                string[] y = FY.Split('-');
                                if (y.Length > 1)
                                {
                                    if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                                    {
                                        string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                                        string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                                        Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        flagDate = 1;
                                    }
                                }
                            }
                            if (RoleCheck.Equals("AUD"))
                            {
                                AflagDate = 1;
                                RoleCheck = "MGMT";
                                User uobj = UserManagement.GetByID(Convert.ToInt32(user.ID));
                                if (uobj != null)
                                {
                                    AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                                    ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                                }
                            }

                            objResult = LitigationTaskManagement.GetAssignedTaskListAmol1(Convert.ToInt32(user.ID), RoleCheck, 3, customerid, ddlstatus);
                            if (flagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.ScheduleOnDate >= Fromdate && entry.ScheduleOnDate <= Todate)).ToList();
                            }
                            if (AflagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.ScheduleOnDate >= AFromdate && entry.ScheduleOnDate <= ATodate)).ToList();
                            }
                            if (mytaskwcobj.priorityID != 0)
                            {
                                objResult = objResult.Where(entry => (entry.PriorityID == mytaskwcobj.priorityID)).ToList();
                            }
                            if (StatusID != "" && StatusID != "B") //B--Both --Inward(I) and Outward(O)
                            {
                                objResult = objResult.Where(entry => (entry.TaskType == StatusID)).ToList();
                            }
                            if (TaskID != -1)
                            {
                                objResult = objResult.Where(entry => (entry.TaskID == TaskID)).ToList();
                            }
                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskwcobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/DashBoardCount")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DashBoardCount([FromBody]mydashboardclass mydashboardobj)
        {
            string Email = mydashboardobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<count> result = new List<count>();


                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            var viewNoticeAssignedInstanceRecords = (from c in entities.View_NoticeAssignedInstance
                                                                     where c.CustomerID == customerId
                                                                     select c).ToList();
                            var ViewCaseAssignedInstanceRecords = (from c in entities.View_CaseAssignedInstance
                                                                   where c.CustomerID == customerId
                                                                   select c).ToList();
                            var ViewTaskRecords = (from row in entities.View_NoticeCaseTaskDetail
                                                   where row.CustomerID == customerId
                                                   && row.IsActive == true
                                                   select row).ToList();

                            string resultNClosed = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 3).ToString();
                            string resultNOpen = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 1).ToString();

                            string CaseOpen = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 1).ToString();
                            string CaseClosed = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 3).ToString();

                            string TaskOpen = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 1).ToString();
                            string TaskClosed = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, Convert.ToInt32(user.ID), RoleCheck, 3, 3).ToString();
                            
                            result.Add(new count
                            {
                                NoticeOpen = Convert.ToInt32(resultNOpen),
                                NoticeClosed = Convert.ToInt32(resultNClosed),
                                CaseOpen = Convert.ToInt32(CaseOpen),
                                CaseClosed = Convert.ToInt32(CaseClosed),
                                TaskOpen = Convert.ToInt32(TaskOpen),
                                TaskClosed = Convert.ToInt32(TaskClosed)
                            });                            
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/DashBoardTask")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DashBoardTask([FromBody]mydashboardtaskclass mydashboardtaskobj)
        {
            string Email = mydashboardtaskobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<View_NoticeCaseTaskDetail> result = new List<View_NoticeCaseTaskDetail>();


                if (!string.IsNullOrEmpty(Email))
                {
                    var TaskType = mydashboardtaskobj.TaskType.ToString();
                    var InstanceID = mydashboardtaskobj.InstanceID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            
                            var ViewTaskRecords = (from row in entities.View_NoticeCaseTaskDetail
                                                   where row.CustomerID == customerId
                                                   && row.IsActive == true
                                                   select row).ToList();

                            result = LitigationTaskManagement.GetDashboardTaskList(ViewTaskRecords, Convert.ToInt32(user.ID), RoleCheck, 3);
                            if (TaskType != "-1")
                            {
                                result = result.Where(entry => (entry.TaskType == TaskType)).ToList();
                            }
                            if (InstanceID != -1)
                            {
                                result = result.Where(entry => (entry.NoticeCaseInstanceID == InstanceID)).ToList();
                            }

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/MyReportAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyReportAndriod([FromBody]myreportclass myreportobj)
        {
            string Email = myreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<MyReportLitigation> objResult = new List<MyReportLitigation>();

                List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
                List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();
                if (!string.IsNullOrEmpty(Email))
                {
                    int StatusFlag = myreportobj.StatusFlag;
                    int StatusType = myreportobj.StatusType;
                    int dept = myreportobj.dept;
                    int location = myreportobj.location;
                    int IOType = myreportobj.IOType;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            DateTime Fromdate = new DateTime();
                            DateTime Todate = new DateTime();

                            int flagDate = 0;
                            int AflagDate = 0;
                            string FY = myreportobj.FY.ToString();
                            DateTime AFromdate = new DateTime();
                            DateTime ATodate = new DateTime();



                            if (FY != "0" && !String.IsNullOrEmpty(FY))
                            {
                                string[] y = FY.Split('-');
                                if (y.Length > 1)
                                {
                                    if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                                    {
                                        string dtfrom = Convert.ToString(Convert.ToInt32(y[0]) + "-" + "01" + "-04");
                                        string dtto = Convert.ToString(Convert.ToInt32(y[1]) + "-" + "03" + "-31");
                                        Fromdate = DateTime.ParseExact(dtfrom, "yyyy-mm-dd", CultureInfo.InvariantCulture);
                                        Todate = DateTime.ParseExact(dtto, "yyyy-mm-dd", CultureInfo.InvariantCulture);
                                        flagDate = 1;
                                    }
                                }
                            }

                            if (RoleCheck.Equals("AUD"))
                            {
                                AflagDate = 1;
                                RoleCheck = "MGMT";
                                User uobj = UserManagement.GetByID(Convert.ToInt32(user.ID));
                                if (uobj != null)
                                {
                                    AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                                    ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                                }
                            }
                            if (StatusFlag == 0)//Notice
                            {
                                objNotice = LitigationDocumentManagement.GetNoticeReportData(customerid, Convert.ToInt32(user.ID), RoleCheck, StatusType);

                                if (flagDate == 1)
                                {
                                    objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                                }
                                if (AflagDate == 1)
                                {
                                    objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                                }
                                if (dept != -1)
                                {
                                    objNotice = objNotice.Where(entry => (entry.DepartmentID == dept)).ToList();
                                }
                                if (location != -1)
                                {
                                    objNotice = objNotice.Where(entry => (entry.CustomerBranchID == location)).ToList();
                                }
                                if (IOType != -1)
                                {
                                    if (IOType == 1)
                                    {
                                        var Type = "I";
                                        objNotice = objNotice.Where(entry => (entry.NoticeType == Type)).ToList();
                                    }
                                    if (IOType == 0)
                                    {
                                        var Type = "O";
                                        objNotice = objNotice.Where(entry => (entry.NoticeType == Type)).ToList();
                                    }
                                }
                                if (objNotice.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = objNotice.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (myreportobj.PageID - 1);
                                    var canPage = skip < total;
                                    objNotice = objNotice.Skip(skip).Take(pageSize).ToList();

                                }

                                objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                            }
                            else if (StatusFlag == 1)//Case
                            {
                                objCase = LitigationDocumentManagement.GetAllCaseReportData(customerid, Convert.ToInt32(user.ID), RoleCheck, StatusType);

                                if (flagDate == 1)
                                {
                                    objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                                }
                                if (AflagDate == 1)
                                {
                                    objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                                }
                                if (dept != -1)
                                {
                                    objCase = objCase.Where(entry => (entry.DepartmentID == dept)).ToList();
                                }
                                if (location != -1)
                                {
                                    objCase = objCase.Where(entry => (entry.CustomerBranchID == location)).ToList();
                                }
                                if (IOType != -1)
                                {
                                    if (IOType == 1)
                                    {
                                        var Type = "I";
                                        objCase = objCase.Where(entry => (entry.CaseType == Type)).ToList();
                                    }
                                    if (IOType == 0)
                                    {
                                        var Type = "O";
                                        objCase = objCase.Where(entry => (entry.CaseType == Type)).ToList();
                                    }
                                }
                                if (objCase.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = objCase.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (myreportobj.PageID - 1);
                                    var canPage = skip < total;
                                    objCase = objCase.Skip(skip).Take(pageSize).ToList();

                                }

                                objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                    
                            }

                        }

                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/MyTaskReportAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskReportAndriod([FromBody]mytaskreportclass mytaskreportobj)
        {
            string Email = mytaskreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<View_NoticeCaseTaskDetail> objResult = new List<View_NoticeCaseTaskDetail>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int taskStatus = mytaskreportobj.taskStatus;
                    string StatusID = mytaskreportobj.StatusID;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            DateTime Fromdate = new DateTime();
                            DateTime Todate = new DateTime();

                            int flagDate = 0;
                            int AflagDate = 0;
                            string FY = mytaskreportobj.FY.ToString();
                            DateTime AFromdate = new DateTime();
                            DateTime ATodate = new DateTime();


                            if (FY != "0" && !String.IsNullOrEmpty(FY))
                            {
                                string[] y = FY.Split('-');
                                if (y.Length > 1)
                                {
                                    if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                                    {
                                        string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                                        string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                                        Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        flagDate = 1;
                                    }
                                }
                            }


                            if (RoleCheck.Equals("AUD"))
                            {
                                AflagDate = 1;
                                RoleCheck = "MGMT";
                                User uobj = UserManagement.GetByID(Convert.ToInt32(user.ID));
                                if (uobj != null)
                                {
                                    AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                                    ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                                }
                            }

                            objResult = LitigationTaskManagement.GetAssignedTaskListAmol(Convert.ToInt32(user.ID), RoleCheck, 3, customerid, taskStatus);
                            if (flagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.ScheduleOnDate >= Fromdate && entry.ScheduleOnDate <= Todate)).ToList();
                            }
                            if (AflagDate == 1)
                            {
                                objResult = objResult.Where(entry => (entry.ScheduleOnDate >= AFromdate && entry.ScheduleOnDate <= ATodate)).ToList();
                            }
                            if (mytaskreportobj.priorityID != 0)
                            {
                                objResult = objResult.Where(entry => (entry.PriorityID == mytaskreportobj.priorityID)).ToList();
                            }
                            if (StatusID != "" && StatusID != "B") //B--Both --Inward(I) and Outward(O)
                            {
                                objResult = objResult.Where(entry => (entry.TaskType == StatusID)).ToList();
                            }
                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskreportobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();

                            }


                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/DeptListAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DeptListAndriod([FromBody]mydeptclass mydeptobj)
        {
            string Email = mydeptobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<DepartmentList> Dobj = new List<DepartmentList>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            Dobj = (from row in entities.Departments
                                    where row.IsDeleted == false
                                    && row.CustomerID == customerId
                                    select new DepartmentList
                                    {
                                        DepartmentID = row.ID,
                                        DepartmentName = row.Name
                                    }).ToList();

                            Dobj = Dobj.OrderBy(entry => entry.DepartmentName).ToList();
                        }

                    }
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/GetLocationListAndriodold")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLocationListAndriodold(string Email)
        {
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(user.CustomerID);
                            var LocationList = CustomerBranchManagement.GetAssignedLocationLitigationList(customerId);
                            var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
                            CFODashboardGraphObj.Add(new CFODashboardGraph
                            {
                                locationList = branches
                            });

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/GetLocationListAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLocationListAndriod([FromBody]mylocationclass mylocationobj)
        {
            string Email = mylocationobj.Email.ToString();
            try
            {
                List<AssignLocationList> AssignLocationListObj = new List<AssignLocationList>();

                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int userId = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);

                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            var LocationList = CustomerBranchManagement.GetAssignedLocationLitigationList(customerId);
                            foreach (var item in LocationList)
                            {
                                string Name = (from row in entities.CustomerBranches
                                               where row.ID == item
                                               select row.Name).FirstOrDefault();
                                AssignLocationListObj.Add(new AssignLocationList
                                {
                                    AssignID = item,
                                    AssignName = Name
                                });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(AssignLocationListObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/myOpponetAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myOpponetAndriod([FromBody]myopponentclass myopponentobj)
        {
            string Email = myopponentobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<mydocdetails> objcourt = new List<mydocdetails>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            var obj = LitigationLaw.GetLCPartyDetails(customerId);
                                    if (obj.Count > 0)
                                    {
                                        objcourt = (from row in obj
                                                    select new mydocdetails()
                                                    {
                                                        ID = Convert.ToInt32(row.ID),
                                                        Name = row.Name.ToString(),
                                                    }).ToList();
                                    }
                                

                            }

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcourt).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/GetOwnerListCaseAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetOwnerListCaseAndriod([FromBody]myownerlistclass myownerlistobj)
        {
            string Email = myownerlistobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Sp_LitigationGetOwnerOfCase_Result> obj = new List<Sp_LitigationGetOwnerOfCase_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int Status = myownerlistobj.Status;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            obj = (from row in entities.Sp_LitigationGetOwnerOfCase(customerId, (int)Status)
                                   select row).ToList();
                        }

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/LegalCaseTypeAndriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LegalCaseTypeAndriod([FromBody]myLegalclass myLegalobj)
        {
            string Email = myLegalobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<tbl_CaseType> obj = new List<tbl_CaseType>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            obj = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(Convert.ToInt64(customerId));
                        }

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/DocumentDetailsAndroid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DocumentDetailsAndroid([FromBody] MyDocumentclass MyDocumentclassobj)
        {
            string Email = MyDocumentclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(Email))
                {
                    var CheckType = MyDocumentclassobj.CheckType;
                    var InstanceID = MyDocumentclassobj.InstanceID;
                    var Type = MyDocumentclassobj.Type;
                    List<string> Doctypes = new List<string>();
                    var TaskID = MyDocumentclassobj.TaskID;


                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            if (Type == "A")
                            {
                                if (CheckType == "N")
                                {
                                    Doctypes.Add("N");
                                    Doctypes.Add("NR");
                                    Doctypes.Add("NT");
                                    //TypeName = "Notice Document";
                                }
                                if (CheckType == "C")
                                {
                                    Doctypes.Add("C");
                                    Doctypes.Add("CH");
                                    Doctypes.Add("CT");
                                    //TypeName = "Case Document";
                                }
                            }
                            else
                            {
                                Doctypes.Add(Type);
                            }

                            string fetchVersion = (from row in entities.tbl_LitigationFileData
                                                   where row.NoticeCaseInstanceID == InstanceID
                                                   && row.IsDeleted == false
                                                   select row.Version).FirstOrDefault();
                            if (!string.IsNullOrEmpty(fetchVersion))
                            {
                                string pathforandriod = PublicClassZip1.fetchpathforLitigationAndriod(InstanceID, Doctypes, fetchVersion, Convert.ToInt32(user.ID), TaskID, Convert.ToInt32(user.CustomerID));
                                if (!string.IsNullOrEmpty(pathforandriod))
                                {
                                    string pathforandriodoutput = pathforandriod;
                                    pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                    pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                    string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                    finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                    UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }

                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/DocumentDetailsAndroidNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DocumentDetailsAndroidNew([FromBody] MyDocumentclass MyDocumentclassobj)
        {
            string Email = MyDocumentclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(Email))
                {
                    var CheckType = MyDocumentclassobj.CheckType;
                    var InstanceID = MyDocumentclassobj.InstanceID;
                    var Type = MyDocumentclassobj.Type;
                    List<string> Doctypes = new List<string>();
                    var TaskID = MyDocumentclassobj.TaskID;


                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            if (Type == "A")
                            {
                                if (CheckType == "N")
                                {
                                    Doctypes.Add("N");
                                    Doctypes.Add("NR");
                                    Doctypes.Add("NT");
                                    //TypeName = "Notice Document";
                                }
                                if (CheckType == "C")
                                {
                                    Doctypes.Add("C");
                                    Doctypes.Add("CH");
                                    Doctypes.Add("CT");
                                    //TypeName = "Case Document";
                                }
                            }
                            else
                            {
                                Doctypes.Add(Type);
                            }

                            string fetchVersion = (from row in entities.tbl_LitigationFileData
                                                   where row.ID == MyDocumentclassobj.FileId
                                                   && row.IsDeleted == false
                                                   select row.Version).FirstOrDefault();
                            if (!string.IsNullOrEmpty(fetchVersion))
                            {
                                string pathforandriod = PublicClassZip1.fetchpathforLitigationAndriodNew(InstanceID, Doctypes, fetchVersion, Convert.ToInt32(user.ID), TaskID,Convert.ToInt32(MyDocumentclassobj.FileId), Convert.ToInt32(user.CustomerID));
                                if (!string.IsNullOrEmpty(pathforandriod))
                                {
                                    string pathforandriodoutput = pathforandriod;
                                    pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                    pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                    string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                    finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                    UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }

                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("Litigation/HearingAndroid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage HearingAndroid([FromBody] HearingClass HearingClassobj)
        {
            string Email = HearingClassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<View_LitigationCaseResponse> View_LitigationCaseResponseobj = new List<View_LitigationCaseResponse>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                           
                            View_LitigationCaseResponseobj = (from row in entities.View_LitigationCaseResponse
                                                              where row.CustomerID == customerId
                                                              select row).ToList();

                           
                            if (HearingClassobj.InstanceID !=-1)
                            {
                                View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(entry => entry.NoticeCaseInstanceID == HearingClassobj.InstanceID).ToList();
                            }
                            if (View_LitigationCaseResponseobj.Count > 0)
                            {
                                View_LitigationCaseResponseobj = (from g in View_LitigationCaseResponseobj
                                                                  group g by new
                                                                  {
                                                                      g.NoticeCaseInstanceID,
                                                                      g.Title,
                                                                      g.CaseRefNo,
                                                                      g.CourtID,
                                                                      g.CourtName,
                                                                      g.CaseType,
                                                                      g.CustomerID,
                                                                      g.CustomerBranchID,
                                                                      g.BranchName,
                                                                      g.DepartmentID,
                                                                      g.CaseRiskID,
                                                                      g.PartyID,
                                                                      g.PartyName,
                                                                      // g.Party,
                                                                      g.TxnStatusID,
                                                                      g.ReminderDate,
                                                                      g.lstAssignedTo,
                                                                      g.ResponseID,
                                                                      g.Description,
                                                                      //g.AssignedTo,
                                                                      g.Department
                                                                  } into GCS
                                                                  select new View_LitigationCaseResponse()
                                                                  {
                                                                      NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                                                      Title = GCS.Key.Title,
                                                                      CaseRefNo = GCS.Key.CaseRefNo,
                                                                      CourtID = GCS.Key.CourtID,
                                                                      CourtName = GCS.Key.CourtName,
                                                                      CaseType = GCS.Key.CaseType,
                                                                      CustomerID = GCS.Key.CustomerID,
                                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                                      BranchName = GCS.Key.BranchName,
                                                                      DepartmentID = GCS.Key.DepartmentID,
                                                                      CaseRiskID = GCS.Key.CaseRiskID,
                                                                      PartyID = GCS.Key.PartyID,
                                                                      PartyName = GCS.Key.PartyName,
                                                                      // Party = GCS.Key.Party,
                                                                      TxnStatusID = GCS.Key.TxnStatusID,
                                                                      ReminderDate = GCS.Key.ReminderDate,
                                                                      lstAssignedTo = GCS.Key.lstAssignedTo,
                                                                      ResponseID = GCS.Key.ResponseID,
                                                                      Description = GCS.Key.Description,
                                                                      //AssignedTo = GCS.Key.AssignedTo,
                                                                      Department = GCS.Key.Department
                                                                  }).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(View_LitigationCaseResponseobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/HearingAndroidNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage HearingAndroidNew([FromBody] HearingClass HearingClassobj)
        {
            string Email = HearingClassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<View_LegalCaseResponseNew> View_LitigationCaseResponseobj = new List<View_LegalCaseResponseNew>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            View_LitigationCaseResponseobj = (from row in entities.View_LegalCaseResponseNew
                                                              where row.CaseInstanceID == HearingClassobj.InstanceID
                                                              && row.IsActive == true
                                                              select row).ToList();

                            if (!string.IsNullOrEmpty(HearingClassobj.Date))
                            {
                                bool check = CheckDate(HearingClassobj.Date);
                                if (check)
                                {
                                    DateTime today = DateTime.ParseExact(HearingClassobj.Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    View_LitigationCaseResponseobj = View_LitigationCaseResponseobj.Where(x => x.ReminderDate == today).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(View_LitigationCaseResponseobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [Authorize] 
        [Route("Litigation/MyDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDocumentList([FromBody]myDocumentclass mytaskreportobj)
        {
            string Email = mytaskreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Sp_Litigation_CaseDocument_Result> objResult = new List<Sp_Litigation_CaseDocument_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int caseinstantid = Convert.ToInt32(mytaskreportobj.InstanceID);
                            string Type = mytaskreportobj.Type;

                            objResult = CaseManagement.GetCaseDocumentMapping(caseinstantid, Type, customerid);

                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskreportobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/MyTaskDocumentList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskDocumentList([FromBody]myDocumentclass mytaskreportobj)
        {
            string Email = mytaskreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<tbl_LitigationFileData> objResult = new List<tbl_LitigationFileData>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int caseinstantid = Convert.ToInt32(mytaskreportobj.InstanceID);
                            string Type = mytaskreportobj.Type;
                            int responseID = mytaskreportobj.TaskID;

                            objResult = (from row in entities.tbl_LitigationFileData
                                         where row.NoticeCaseInstanceID == caseinstantid
                                         && row.DocTypeInstanceID == responseID
                                         && row.DocType.Trim() == Type
                                         && row.IsDeleted == false
                                         select row).ToList();

                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskreportobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();
                            }

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]  
        [Route("Litigation/MyDocumentListNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyDocumentListNew([FromBody]myDocumentclass mytaskreportobj)
        {
            string Email = mytaskreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<tbl_LitigationFileData> objResult = new List<tbl_LitigationFileData>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int caseinstantid = Convert.ToInt32(mytaskreportobj.InstanceID);
                            string Type = mytaskreportobj.Type;

                            List<string> Doctypes = new List<string>();
                            if (!string.IsNullOrEmpty(Type))
                            {
                                if (Type.ToUpper().Trim().Equals("C"))
                                {
                                    Doctypes.Add("C");
                                    Doctypes.Add("CH");
                                    Doctypes.Add("CT");
                                }
                                else if (Type.ToUpper().Trim().Equals("N"))
                                {
                                    Doctypes.Add("N");
                                    Doctypes.Add("NR");
                                    Doctypes.Add("NT");
                                }
                            }
                            objResult = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(caseinstantid), Doctypes);

                            //objResult = CaseManagement.GetCaseDocumentMapping(caseinstantid, Type, customerid);

                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskreportobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
               
        [Route("Litigation/MyTaskDocumentListNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyTaskDocumentListNew([FromBody]myDocumentclass mytaskreportobj)
        {
            string Email = mytaskreportobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<tbl_LitigationFileData> objResult = new List<tbl_LitigationFileData>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerid = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int caseinstantid = Convert.ToInt32(mytaskreportobj.InstanceID);
                            string Type = mytaskreportobj.Type;
                            int responseID = mytaskreportobj.TaskID;

                            objResult = LitigationTaskManagement.GetTaskDocuments(Convert.ToInt64(responseID), 0);

                            //objResult = (from row in entities.tbl_LitigationFileData
                            //             where row.NoticeCaseInstanceID == caseinstantid
                            //             && row.DocTypeInstanceID == responseID
                            //             && row.DocType.Trim() == Type
                            //             && row.IsDeleted == false
                            //             select row).ToList();
                            string CurrentYear = Convert.ToString(DateTime.Now.Year);
                            if (objResult.Count > 0)
                            {
                                if (mytaskreportobj.Flag == "Current")
                                {
                                    objResult = objResult.OrderByDescending(x => x.CreatedOn).Take(1).ToList();
                                    //objResult = objResult.Where(x => x.CreatedOn.Value.Year >= DateTime.Now.Year).ToList();                                
                                }
                                else
                                {
                                    objResult = objResult.OrderByDescending(x => x.CreatedOn).ToList();
                                    objResult.RemoveAt(0);
                                    //objResult = objResult.Where(x => x.CreatedOn.Value.Year < DateTime.Now.Year).ToList();
                                }
                            }
                            if (objResult.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = objResult.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mytaskreportobj.PageID - 1);
                                var canPage = skip < total;
                                objResult = objResult.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/kendomyReminder")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage kendomyReminder(int customerID, int loggedInUserID, string loggedInUserRole, int reminderType, long instanceID)
        {
            try
            {
                List<View_LitigationReminderView> query = new List<View_LitigationReminderView>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    query = (from row in entities.View_LitigationReminderView
                             where row.CustomerID == customerID
                             //&& row.ReminderStatus == 0
                             select row).ToList(); //0-Pending 1-Sent

                    ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    if (isexist != null)
                    {
                    	 if (instanceID != -1)
                            query = query.Where(entry => entry.InstanceID == instanceID).ToList();

                        if (loggedInUserRole != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", reminderType);
                            //if (caseList.Count > 0)
                            //{
                                query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.InstanceID))).ToList();
                            //}
                        }
                    }
                    else
                    {
                        if (instanceID != -1)
                            query = query.Where(entry => entry.InstanceID == instanceID).ToList();

                        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                            query = query.Where(entry => entry.UserID == loggedInUserID).ToList();
                    }
                   


                    if (reminderType != 0)
                    {
                        if (reminderType == 1) //Notice
                            query = query.Where(entry => entry.Type == "N").ToList();
                        else if (reminderType == 2) //Case
                            query = query.Where(entry => entry.Type == "C").ToList();
                        else if (reminderType == 3) //Task
                            query = query.Where(entry => entry.Type == "T").ToList();
                    }

                    if (query.Count > 0)
                        query = query.OrderBy(entry => entry.RemindOn)
                            .ThenBy(entry => entry.Type).ToList();


                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(query).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/kendomyReminderFilterList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage kendomyReminderFilterList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, int ddltype)
        {
            List<OutputDetil> query2 = new List<OutputDetil>();

            try
            {
                if (ddltype == 1)//notice    
                {
                    var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, loggedInUserID, loggedInUserRole, roleID, string.Empty);

                    lstNoticeDetails = lstNoticeDetails.Where(x => x.TxnStatusID == 1).ToList();

                    if (lstNoticeDetails.Count > 0)
                    {

                        query2 = (from row in lstNoticeDetails
                                  select new OutputDetil()
                                  {
                                      ID = Convert.ToInt32(row.NoticeInstanceID),
                                      Name = row.NoticeTitle.ToString(),
                                  }).ToList();
                    }
                }


                if (ddltype == 2)//Case   
                {
                    var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerID, loggedInUserID, loggedInUserRole, roleID);
                    if (lstCaseDetails.Count > 0)
                    {
                        lstCaseDetails = lstCaseDetails.Where(x => x.TxnStatusID == 1).ToList();

                        query2 = (from row in lstCaseDetails
                                  select new OutputDetil()
                                  {
                                      ID = Convert.ToInt32(row.CaseInstanceID),
                                      Name = row.CaseTitle.ToString(),
                                  }).ToList();
                    }
                }

                if (ddltype == 3)//task                
                {
                    var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(loggedInUserID, loggedInUserRole, 3, customerID);
                    if (lstTaskDetails.Count > 0)
                    {
                        lstTaskDetails = lstTaskDetails.Where(x => x.StatusID == 1).ToList();

                        query2 = (from row in lstTaskDetails
                                  select new OutputDetil()
                                  {
                                      ID = Convert.ToInt32(row.TaskID),
                                      Name = row.TaskTitle.ToString(),
                                  }).ToList();
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/Delete_Litigation_ReminderByID")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_Litigation_ReminderByID(string reminderID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(reminderID))
                {
                    bool result = LitigationManagement.DeleteLitigationReminderByID(Convert.ToInt32(reminderID));
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/KendoCategoryList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCategoryList(int CustId)
        {
            try
            {
                List<tbl_CaseType> objcases = new List<tbl_CaseType>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objcases = (from row in entities.tbl_CaseType
                                where row.CaseType != null && row.IsDeleted == false
                                && row.CustomerID == CustId
                                select row).OrderBy(entry => entry.CaseType).ToList();

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcases).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/KendoCourtList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoCourtList(int CustId)
        {
            try
            {
                List<View_CourtMaster> objCourt = new List<View_CourtMaster>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objCourt = (from row in entities.View_CourtMaster
                                where row.CourtName != null &&
                                row.CustomerID == CustId
                                select row).OrderBy(entry => entry.CourtName).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objCourt).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/LegalCaseParty")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LegalCaseParty(string CustomerID)
        {
            List<tbl_PartyDetail> obj = new List<tbl_PartyDetail>();
            try
            {
                if (!string.IsNullOrEmpty(CustomerID))
                {
                    obj = LitigationLaw.GetLCPartyDetails(Convert.ToInt64(CustomerID));
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/MyReport")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyReport(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        {
            try
            {
                DateTime Fromdate = new DateTime();
                DateTime Todate = new DateTime();

                int flagDate = 0;
                int AflagDate = 0;
                DateTime AFromdate = new DateTime();
                DateTime ATodate = new DateTime();

                List<MyReportLitigation> objResult = new List<MyReportLitigation>();

                List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
                List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

                if (CY != "0" && !String.IsNullOrEmpty(CY))
                {
                    string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
                    string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    flagDate = 1;
                }

                if (FY != "0" && !String.IsNullOrEmpty(FY))
                {
                    string[] y = FY.Split('-');
                    if (y.Length > 1)
                    {
                        if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                            string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            flagDate = 1;
                        }
                    }
                }
                else
                {
                    if (MonthId != "All")
                    {
                        flagDate = 1;
                        Todate = DateTime.Now;
                        if (MonthId == "1")
                            Fromdate = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            Fromdate = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            Fromdate = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            Fromdate = DateTime.Now.AddDays(-365);
                    }

                }

                if (FlagIsApp.Equals("AUD"))
                {
                    AflagDate = 1;
                    FlagIsApp = "MGMT";
                    User uobj = UserManagement.GetByID(UserId);
                    if (uobj != null)
                    {
                        AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                        ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                    }
                }
                if (StatusFlag == 1)//Notice
                {
                    objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

                    if (objNotice.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime sd = Convert.ToDateTime(StartDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
                            }
                            else
                            {
                                objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime Ld = Convert.ToDateTime(EndDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
                            }
                            else
                            {
                                objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
                            }
                        }
                    }


                    if (flagDate == 1)
                    {
                        objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                    }
                    if (WLResult != -1)
                    {
                        objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
                    }

                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                    //if (isexist != null)
                    //{
                    //    var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp, StatusFlag);
                    //    //if (assignedLocationList.Count > 0)
                    //    //{
                    //        objNotice = objNotice.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    //}
                    //}




                    objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                }
                else if (StatusFlag == 2)//Case
                {
                    objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

                    if (objCase.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime sd = Convert.ToDateTime(StartDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
                            }
                            else
                            {
                                objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime Ld = Convert.ToDateTime(EndDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
                            }
                            else
                            {
                                objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
                            }
                        }
                    }

                    if (flagDate == 1)
                    {
                        if (StatusType == 1 || StatusType == -1)
                        {
                            objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                        }
                        else
                        {
                            objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
                        }
                    }
                    if (AflagDate == 1)
                    {
                        if (StatusType == 1 || StatusType == -1)
                        {
                            objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                        }
                        else
                        {
                            objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
                        }
                    }
                    if (WLResult != -1)
                    {
                        objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
                    }
                    //if ( IsEntityassignmentCustomer == Convert.ToString(CustomerID))
                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                    //if (isexist != null)
                    //{
                    //    var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp, StatusFlag);
                    //    //if (assignedLocationList.Count > 0)
                    //    //{
                    //        objCase = objCase.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    //}

                    //}


                    objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        //[Route("Litigation/MyReport")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage MyReport(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        //{
        //    try
        //    {
        //        DateTime Fromdate = new DateTime();
        //        DateTime Todate = new DateTime();

        //        int flagDate = 0;
        //        int AflagDate = 0;
        //        DateTime AFromdate = new DateTime();
        //        DateTime ATodate = new DateTime();

        //        List<MyReportLitigation> objResult = new List<MyReportLitigation>();

        //        List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
        //        List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

        //        if (CY != "0" && !String.IsNullOrEmpty(CY))
        //        {
        //            string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
        //            string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
        //            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            flagDate = 1;
        //        }

        //        if (FY != "0" && !String.IsNullOrEmpty(FY))
        //        {
        //            string[] y = FY.Split('-');
        //            if (y.Length > 1)
        //            {
        //                if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
        //                {
        //                    string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
        //                    string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
        //                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    flagDate = 1;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (MonthId != "All")
        //            {
        //                flagDate = 1;
        //                Todate = DateTime.Now;
        //                if (MonthId == "1")
        //                    Fromdate = DateTime.Now.AddDays(-30);

        //                if (MonthId == "3")
        //                    Fromdate = DateTime.Now.AddDays(-90);

        //                if (MonthId == "6")
        //                    Fromdate = DateTime.Now.AddDays(-180);

        //                if (MonthId == "12")
        //                    Fromdate = DateTime.Now.AddDays(-365);
        //            }

        //        }

        //        if (FlagIsApp.Equals("AUD"))
        //        {
        //            AflagDate = 1;
        //            FlagIsApp = "MGMT";
        //            User uobj = UserManagement.GetByID(UserId);
        //            if (uobj != null)
        //            {
        //                AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
        //                ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
        //            }
        //        }
        //        if (StatusFlag == 1)//Notice
        //        {
        //            objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objNotice.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }


        //            if (flagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //            }
        //            if (AflagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //            }
        //            if (WLResult != -1)
        //            {
        //                objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp);
        //            if (assignedLocationList.Count > 0)
        //            {
        //                objNotice = objNotice.Where(entry => assignedLocationList.Contains(entry.CustomerBranchID)).ToList();
        //            }

        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }
        //        else if (StatusFlag == 2)//Case
        //        {
        //            objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objCase.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }

        //            if (flagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
        //                }
        //            }
        //            if (AflagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
        //                }
        //            }
        //            if (WLResult != -1)
        //            {
        //                objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp);
        //            if (assignedLocationList.Count > 0)
        //            {
        //                objCase = objCase.Where(entry => assignedLocationList.Contains(entry.CustomerBranchID)).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize] 
        //[Route("Litigation/MyReport")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage MyReport(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        //{
        //    try
        //    {
        //        DateTime Fromdate = new DateTime();
        //        DateTime Todate = new DateTime();

        //        int flagDate = 0;
        //        int AflagDate = 0;
        //        DateTime AFromdate = new DateTime();
        //        DateTime ATodate = new DateTime();

        //        List<MyReportLitigation> objResult = new List<MyReportLitigation>();

        //        List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
        //        List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

        //        if (CY != "0" && !String.IsNullOrEmpty(CY))
        //        {
        //            string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
        //            string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
        //            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            flagDate = 1;
        //        }

        //        if (FY != "0" && !String.IsNullOrEmpty(FY))
        //        {
        //            string[] y = FY.Split('-');
        //            if (y.Length > 1)
        //            {
        //                if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
        //                {
        //                    string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
        //                    string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
        //                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    flagDate = 1;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (MonthId != "All")
        //            {
        //                flagDate = 1;
        //                Todate = DateTime.Now;
        //                if (MonthId == "1")
        //                    Fromdate = DateTime.Now.AddDays(-30);

        //                if (MonthId == "3")
        //                    Fromdate = DateTime.Now.AddDays(-90);

        //                if (MonthId == "6")
        //                    Fromdate = DateTime.Now.AddDays(-180);

        //                if (MonthId == "12")
        //                    Fromdate = DateTime.Now.AddDays(-365);
        //            }

        //        }

        //        if (FlagIsApp.Equals("AUD"))
        //        {
        //            AflagDate = 1;
        //            FlagIsApp = "MGMT";
        //            User uobj = UserManagement.GetByID(UserId);
        //            if (uobj != null)
        //            {
        //                AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
        //                ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
        //            }
        //        }
        //        if (StatusFlag == 1)//Notice
        //        {
        //            objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objNotice.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }


        //            if (flagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //            }
        //            if (AflagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //            }
        //            if (WLResult != -1)
        //            {
        //                objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }
        //        else if (StatusFlag == 2)//Case
        //        {
        //            objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objCase.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }

        //            if (flagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
        //                }
        //            }
        //            if (AflagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
        //                }
        //            }
        //            if (WLResult != -1)
        //            {
        //                objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        [Authorize] 
        [Route("Litigation/kendomyTaskReport")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage kendomyTaskReport(int customerID, int loggedInUserID, string loggedInUserRole, int taskStatus, int priority, string taskType, string MonthId)
        {
            List<View_NoticeCaseTaskDetail> query = new List<View_NoticeCaseTaskDetail>();
            try
            {
                DateTime Fromdate = new DateTime();
                DateTime Todate = new DateTime();
                int flagDate = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == customerID
                             select row).ToList();

                    //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();

                    ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    if (isexist != null)
                    {
                        if (loggedInUserRole != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerID, loggedInUserID, "", -1);
                            ////if (caseList.Count > 0)
                            ////{
                                query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                            //}
                        }
                    }
                    else
                    {
                        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                            query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();

                    }

                    if (taskStatus != -1)
                    {
                        query = query.Where(entry => entry.StatusID == taskStatus).ToList();

                    }
                    if (priority != -1)
                    {
                        query = query.Where(entry => entry.PriorityID == priority).ToList();

                    }
                    if (taskType != "All")
                    {
                        query = query.Where(entry => entry.TaskType == taskType).ToList();

                    }
                    if (MonthId != "All")
                    {
                        flagDate = 1;
                        Todate = DateTime.Now;
                        if (MonthId == "1")
                            Fromdate = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            Fromdate = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            Fromdate = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            Fromdate = DateTime.Now.AddDays(-365);
                    }
                    if (flagDate == 1)
                    {
                        query = query.Where(entry => (entry.TaskCreatedOn >= Fromdate && entry.TaskCreatedOn <= Todate)).ToList();
                    }

                    if (query.Count > 0)
                    {
                        query = query.OrderBy(entry => entry.TaskType)
                            .ThenBy(entry => entry.ScheduleOnDate)
                            .ThenBy(entry => entry.PriorityID).ToList();
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(query).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/KendoDeptList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoDeptList(int CustId)
        {
            try
            {
                List<DepartmentList> Dobj = new List<DepartmentList>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Dobj = (from row in entities.Departments
                            where row.IsDeleted == false
                            && row.CustomerID == CustId
                            select new DepartmentList
                            {
                                DepartmentID = row.ID,
                                DepartmentName = row.Name
                            }).ToList();

                    Dobj = Dobj.OrderBy(entry => entry.DepartmentName).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/KendoLawyerList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoLawyerList(int CustId)
        {
            try
            {
                List<LawyerList> Dobj = new List<LawyerList>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(CustId);

                    if (lstAllUsers.Count > 0)
                    {

                        Dobj = (from row in lstAllUsers
                                select new LawyerList()
                                {
                                    LawyerName = row.FirstName + ' ' + row.LastName,
                                    LawyertID = row.ID,
                                }).ToList();

                    }

                    Dobj = Dobj.OrderBy(entry => entry.LawyerName).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize] 
        [Route("Litigation/GetLocationList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLocationList(int customerId)///, int userId, string Flag, string IsStatutoryInternal
        {
            try
            {
                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahul(customerId);
                    var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
                    CFODashboardGraphObj.Add(new CFODashboardGraph
                    {
                        locationList = branches
                    });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/GetAssignedLocationList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAssignedLocationList(int customerId, int UserID, string FlagIsApp)///, int userId, string Flag, string IsStatutoryInternal
        {
            try
            {

                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int IsForBranch = UserManagement.ExistBranchAssignment(customerId);
                    if (IsForBranch == 0)
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedEntitiesLocationListLitigationRahulbyuser(customerId, UserID, FlagIsApp);
                        var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = branches
                        });
                    }
                    else
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedEntitiesLocationListLitigationRahulbyuserNew(customerId, UserID, FlagIsApp);
                        List<NameValueHierarchy> obj = new List<NameValueHierarchy>();
                        List<NameValueHierarchy> obj1 = new List<NameValueHierarchy>();

                        var BranchList = (from row in entities.CustomerBranches
                                          where LocationList.Contains(row.ID)
                                          && row.IsDeleted == false
                                          select row).ToList();

                        foreach (var item in BranchList)
                        {
                            obj.Add(new NameValueHierarchy { RparentID = null, ID = item.ID, Children = obj1.ToList(), Level = 0, Name = item.Name });
                        }
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = obj
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        //[Route("Litigation/GetAssignedLocationList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAssignedLocationList(int customerId, int UserID, string FlagIsApp)///, int userId, string Flag, string IsStatutoryInternal
        //{
        //    try
        //    {

        //        List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerId, UserID, FlagIsApp);
        //            var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
        //            CFODashboardGraphObj.Add(new CFODashboardGraph
        //            {
        //                locationList = branches
        //            });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize]
        //[Route("Litigation/GetAssignedLocationList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAssignedLocationList(int customerId, int UserID)
        //{
        //    try
        //    {
        //        List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerId, UserID);
        //            var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
        //            CFODashboardGraphObj.Add(new CFODashboardGraph
        //            {
        //                locationList = branches
        //            });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize] 
        //[Route("Litigation/GetAssignedLocationList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAssignedLocationList(int customerId,int UserID)///, int userId, string Flag, string IsStatutoryInternal
        //{
        //    try
        //    {
        //        List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerId, UserID);
        //            var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
        //            CFODashboardGraphObj.Add(new CFODashboardGraph
        //            {
        //                locationList = branches
        //            });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        [Authorize] 
        [Route("Litigation/GetAssignedLocationListNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAssignedLocationListNew(int customerId, int UserID,int statusID)///, int userId, string Flag, string IsStatutoryInternal
        {
            try
            {
                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int IsForBranch = UserManagement.ExistBranchAssignment(customerId);
                    if (IsForBranch == 0)
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuserNew(customerId, UserID, statusID);
                        var branches = BranchClass.GetAllHierarchySatutory(customerId, LocationList);
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = branches
                        });
                    }
                    else
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuserNew(customerId, UserID, statusID);
                        List<NameValueHierarchy> obj = new List<NameValueHierarchy>();
                        List<NameValueHierarchy> obj1 = new List<NameValueHierarchy>();

                        var BranchList = (from row in entities.CustomerBranches
                                          where LocationList.Contains(row.ID)
                                          && row.IsDeleted == false
                                          select row).ToList();

                        foreach (var item in BranchList)
                        {
                            obj.Add(new NameValueHierarchy { RparentID = null, ID = item.ID, Children = obj1.ToList(), Level = 0, Name = item.Name });
                        }
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = obj
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/myDocumentFilters")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage myDocumentFilters(long CustomerID, int ddltype)
        {
            List<mydocdetails> objcourt = new List<mydocdetails>();
            try
            {
                if (ddltype == 2)
                {
                    var obj = LitigationLaw.GetLCPartyDetails(CustomerID);
                    if (obj.Count > 0)
                    {
                        objcourt = (from row in obj
                                    select new mydocdetails()
                                    {
                                        ID = Convert.ToInt32(row.ID),
                                        Name = row.Name.ToString(),
                                    }).ToList();
                    }
                }
                if (ddltype == 3)
                {
                    var obj2 = CompDeptManagement.GetAllDepartmentMasterList(CustomerID);
                    if (obj2.Count > 0)
                    {
                        objcourt = (from row in obj2
                                    select new mydocdetails()
                                    {
                                        ID1 = Convert.ToInt32(row.ID),
                                        Name1 = row.Name.ToString(),
                                    }).ToList();
                    }
                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objcourt).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }
        [Authorize]
        [Route("Litigation/kendomyDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage kendomyDocument(string customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<string> lstSelectedFileTags, string divid, int status, string TagFilter)
        {
            List<mydocumenttype> query2 = new List<mydocumenttype>();
            try
            {
                if (divid == "C") //case    
                {
                    var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status, TagFilter);

                    if (DocList.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(divid))
                        {
                            if (divid.Equals("C"))
                            {
                                DocList = DocList.Where(x => x.TypeCaseNotice == "C").ToList();
                            }
                        }
                        // int statusflag = 2;
                        ////if ( IsEntityassignmentCustomer == Convert.ToString(customerID))
                        //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                        //if (isexist != null)
                        //{
                        //    var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, statusflag);
                        //    //if (assignedLocationList.Count > 0)
                        //    //{
                        //        DocList = DocList.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //    //}
                        //}





                        query2 = (from row in DocList
                                  select new mydocumenttype()
                                  {
                                      TypeName = row.TypeName,
                                      RefNo = row.RefNo,
                                      Title = row.Title,
                                      PartyName = row.PartyName,
                                      PartyID = row.PartyID,
                                      DepartmentID = row.DepartmentID.ToString(),
                                      CustomerBranchID = row.CustomerBranchID,
                                      Status = row.Status.ToString(),
                                      InstanceID = row.NoticeCaseInstanceID.ToString(),
                                      BranchName = row.BranchName,
                                      FYName = row.FYName,
                                      NoticeCaseInstanceID = row.NoticeCaseInstanceID,
                                      CBU=row.CBU,
                                      Zone=row.Zone,
                                      Region=row.Region,
                                      Territory=row.Territory,
                                      CaseCreator=row.CaseCreator,
                                      EmailId=row.EmailId,
                                      ContactNumber=row.ContactNumber
                                  }).ToList();
                    }

                }
                if (divid == "N")//Notice    
                {
                    var DocList1 = LitigationDocumentManagement.GetAllDocumentListofNotice(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status, TagFilter);
                    //int statusflag = 1;
                    ////if (IsEntityassignmentCustomer == Convert.ToString(customerID))
                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    //if (isexist != null)
                    //{
                    //    var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, statusflag);
                    //    if (assignedLocationList.Count > 0)
                    //    {
                    //        DocList1 = DocList1.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    }
                    //}


                    //if (IsEntityassignmentCustomer == Convert.ToString(customerID))
                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                    //if (isexist != null)
                    //{
                    //    var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, statusflag);
                       
                    //    DocList1 = DocList1.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                       
                    //}

                    if (DocList1.Count > 0)
                    {
                        query2 = (from row in DocList1
                                  select new mydocumenttype()
                                  {
                                      TypeName = row.TypeName,
                                      RefNo = row.RefNo,
                                      Title = row.Title,
                                      PartyName = row.PartyName,
                                      PartyID = row.PartyID,
                                      DepartmentID = row.DepartmentID.ToString(),
                                      CustomerBranchID = row.CustomerBranchID,
                                      Status = row.Status.ToString(),
                                      InstanceID = row.NoticeCaseInstanceID.ToString(),
                                      BranchName = row.BranchName,
                                      FYName = row.FYName,
                                      NoticeCaseInstanceID = row.NoticeCaseInstanceID,
                                      CBU = row.CBU,
                                      Zone = row.Zone,
                                      Region = row.Region,
                                      Territory = row.Territory,
                                      CaseCreator = row.CaseCreator,
                                      EmailId = row.EmailId,
                                      ContactNumber = row.ContactNumber
                                  }).ToList();
                    }

                }
                if (divid == "T")//Task    
                {
                    var DocList2 = LitigationTaskManagement.GetAssignedTaskListforTag(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status);




                    if (DocList2.Count > 0)
                    {
                        query2 = (from row in DocList2
                                  select new mydocumenttype()
                                  {
                                      Priority = row.Priority,
                                      Task = row.TaskTitle,
                                      Task_Description = row.TaskDesc,
                                      Due_date = row.ScheduleOnDate,
                                      Status = row.StatusID.ToString(),
                                      Assigned_To = row.AssignToName,
                                      InstanceID = row.TaskID.ToString(),
                                      CustomerBranchID = row.CustomerBranchID,
                                      FYName = row.FinancialYear,
                                      TaskType = row.TaskType,
                                      NoticeCaseInstanceID = row.NoticeCaseInstanceID
                                  }).ToList();
                    }

                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        //[Authorize]
        //[Route("Litigation/kendomyDocument")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage kendomyDocument(string customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<string> lstSelectedFileTags, string divid, int status, string TagFilter)
        //{
        //    List<mydocumenttype> query2 = new List<mydocumenttype>();
        //    try
        //    {
        //        if (divid == "C") //case    
        //        {
        //            var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status, TagFilter);

        //            if (DocList.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(divid))
        //                {
        //                    if (divid.Equals("C"))
        //                    {
        //                        DocList = DocList.Where(x => x.TypeCaseNotice == "C").ToList();
        //                    }
        //                }
        //                var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole);
        //                if (assignedLocationList.Count > 0)
        //                {
        //                    DocList = DocList.Where(entry => assignedLocationList.Contains(entry.CustomerBranchID)).ToList();
        //                }


        //                query2 = (from row in DocList
        //                          select new mydocumenttype()
        //                          {
        //                              TypeName = row.TypeName,
        //                              RefNo = row.RefNo,
        //                              Title = row.Title,
        //                              PartyName = row.PartyName,
        //                              PartyID = row.PartyID,
        //                              DepartmentID = row.DepartmentID.ToString(),
        //                              CustomerBranchID = row.CustomerBranchID,
        //                              Status = row.Status.ToString(),
        //                              InstanceID = row.NoticeCaseInstanceID.ToString(),
        //                              BranchName = row.BranchName,
        //                              FYName = row.FYName,
        //                              NoticeCaseInstanceID = row.NoticeCaseInstanceID,
        //                          }).ToList();
        //            }

        //        }
        //        if (divid == "N")//Notice    
        //        {
        //            var DocList1 = LitigationDocumentManagement.GetAllDocumentListofNotice(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status, TagFilter);
        //            var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole);
        //            if (assignedLocationList.Count > 0)
        //            {
        //                DocList1 = DocList1.Where(entry => assignedLocationList.Contains(entry.CustomerBranchID)).ToList();
        //            }
        //            if (DocList1.Count > 0)
        //            {
        //                query2 = (from row in DocList1
        //                          select new mydocumenttype()
        //                          {
        //                              TypeName = row.TypeName,
        //                              RefNo = row.RefNo,
        //                              Title = row.Title,
        //                              PartyName = row.PartyName,
        //                              PartyID = row.PartyID,
        //                              DepartmentID = row.DepartmentID.ToString(),
        //                              CustomerBranchID = row.CustomerBranchID,
        //                              Status = row.Status.ToString(),
        //                              InstanceID = row.NoticeCaseInstanceID.ToString(),
        //                              BranchName = row.BranchName,
        //                              FYName = row.FYName,
        //                              NoticeCaseInstanceID = row.NoticeCaseInstanceID
        //                          }).ToList();
        //            }

        //        }
        //        if (divid == "T")//Task    
        //        {
        //            var DocList2 = LitigationTaskManagement.GetAssignedTaskListforTag(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole, RoleID, string.Empty, string.Empty, status);
        //            var assignedLocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole);
        //            if (assignedLocationList.Count > 0)
        //            {
        //                //DocList2 = DocList2.Where(entry => assignedLocationList.Contains(Convert.ToInt32(entry.CustomerBranchID))).ToList();
        //            }
        //            if (DocList2.Count > 0)
        //            {
        //                query2 = (from row in DocList2
        //                          select new mydocumenttype()
        //                          {
        //                              Priority = row.Priority,
        //                              Task = row.TaskTitle,
        //                              Task_Description = row.TaskDesc,
        //                              Due_date = row.ScheduleOnDate,
        //                              Status = row.StatusID.ToString(),
        //                              Assigned_To = row.AssignToName,
        //                              InstanceID = row.TaskID.ToString(),
        //                              CustomerBranchID = row.CustomerBranchID,
        //                              FYName = row.FinancialYear,
        //                              TaskType = row.TaskType,
        //                              NoticeCaseInstanceID = row.NoticeCaseInstanceID
        //                          }).ToList();
        //            }

        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        [Authorize]
        [Route("Litigation/MyCaseNoticeWorkSpace")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyCaseNoticeWorkSpace(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        {
            try
            {
                DateTime Fromdate = new DateTime();
                DateTime Todate = new DateTime();

                int flagDate = 0;
                int AflagDate = 0;
                DateTime AFromdate = new DateTime();
                DateTime ATodate = new DateTime();

                List<MyReportLitigation> objResult = new List<MyReportLitigation>();
                List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
                List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

                if (CY != "0" && !String.IsNullOrEmpty(CY))
                {
                    string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
                    string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    flagDate = 1;
                }

                if (FY != "0" && !String.IsNullOrEmpty(FY))
                {
                    string[] y = FY.Split('-');
                    if (y.Length > 1)
                    {
                        if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                            string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            flagDate = 1;
                        }
                    }
                }
                else
                {
                    if (MonthId != "All")
                    {
                        flagDate = 1;
                        Todate = DateTime.Now;
                        if (MonthId == "1")
                            Fromdate = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            Fromdate = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            Fromdate = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            Fromdate = DateTime.Now.AddDays(-365);
                    }

                }

                if (FlagIsApp.Equals("AUD"))
                {
                    AflagDate = 1;
                    FlagIsApp = "MGMT";
                    User uobj = UserManagement.GetByID(UserId);
                    if (uobj != null)
                    {
                        AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                        ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                    }
                }
                if (StatusFlag == 1)//Notice
                {
                    objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

                    if (objNotice.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime sd = Convert.ToDateTime(StartDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
                            }
                            else
                            {
                                objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime Ld = Convert.ToDateTime(EndDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
                            }
                            else
                            {
                                objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
                            }
                        }
                    }


                    if (flagDate == 1)
                    {
                        objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                    }
                    if (WLResult != -1)
                    {
                        objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
                    }
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(CustomerID))
                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                    //if (isexist != null)
                    //{
                    //    var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp, StatusFlag);
                    //    if (caseList.Count > 0)
                    //    {
                    //        objNotice = objNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    }
                    //    else
                    //    {
                    //        objNotice = objNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    }
                    //}




                    objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                }
                else if (StatusFlag == 2)//Case
                {
                    objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

                    if (objCase.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime sd = Convert.ToDateTime(StartDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
                            }
                            else
                            {
                                objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            //DateTime Ld = Convert.ToDateTime(EndDateDetail);
                            if (StatusType == 1 || StatusType == -1)
                            {
                                objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
                            }
                            else
                            {
                                objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
                            }
                        }
                    }

                    if (flagDate == 1)
                    {
                        if (StatusType == 1 || StatusType == -1)
                        {
                            objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
                        }
                        else
                        {
                            objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
                        }
                    }
                    if (AflagDate == 1)
                    {
                        if (StatusType == 1 || StatusType == -1)
                        {
                            objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
                        }
                        else
                        {
                            objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
                        }
                    }
                    if (WLResult != -1)
                    {
                        objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
                    }

                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();

                    //if ( IsEntityassignmentCustomer == Convert.ToString(CustomerID))
                    //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                    //if (isexist != null)
                    //{
                    //    var assignedcaseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(CustomerID, UserId, FlagIsApp, StatusFlag);

                    //    if (assignedcaseList.Count > 0)
                    //    {
                    //        objCase = objCase.Where(entry => assignedcaseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    }
                    //    else
                    //    {
                    //        objCase = objCase.Where(entry => assignedcaseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    //    }
                    //}



                    objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
                }


                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        // [Authorize]
        //[Route("Litigation/MyCaseNoticeWorkSpace")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage MyCaseNoticeWorkSpace(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        //{
        //    try
        //    {
        //        DateTime Fromdate = new DateTime();
        //        DateTime Todate = new DateTime();

        //        int flagDate = 0;
        //        int AflagDate = 0;
        //        DateTime AFromdate = new DateTime();
        //        DateTime ATodate = new DateTime();

        //        List<MyReportLitigation> objResult = new List<MyReportLitigation>();
        //        List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
        //        List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

        //        if (CY != "0" && !String.IsNullOrEmpty(CY))
        //        {
        //            string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
        //            string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
        //            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            flagDate = 1;
        //        }

        //        if (FY != "0" && !String.IsNullOrEmpty(FY))
        //        {
        //            string[] y = FY.Split('-');
        //            if (y.Length > 1)
        //            {
        //                if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
        //                {
        //                    string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
        //                    string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
        //                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    flagDate = 1;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (MonthId != "All")
        //            {
        //                flagDate = 1;
        //                Todate = DateTime.Now;
        //                if (MonthId == "1")
        //                    Fromdate = DateTime.Now.AddDays(-30);

        //                if (MonthId == "3")
        //                    Fromdate = DateTime.Now.AddDays(-90);

        //                if (MonthId == "6")
        //                    Fromdate = DateTime.Now.AddDays(-180);

        //                if (MonthId == "12")
        //                    Fromdate = DateTime.Now.AddDays(-365);
        //            }

        //        }

        //        if (FlagIsApp.Equals("AUD"))
        //        {
        //            AflagDate = 1;
        //            FlagIsApp = "MGMT";
        //            User uobj = UserManagement.GetByID(UserId);
        //            if (uobj != null)
        //            {
        //                AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
        //                ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
        //            }
        //        }
        //        if (StatusFlag == 1)//Notice
        //        {
        //            objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objNotice.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }


        //            if (flagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //            }
        //            if (AflagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //            }
        //            if (WLResult != -1)
        //            {
        //                objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            var caseList = CustomerBranchManagement.GetAssignedCaseNoticeLitigation(CustomerID, UserId, FlagIsApp);
        //            if (caseList.Count > 0)
        //            {
        //                objNotice = objNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }
        //        else if (StatusFlag == 2)//Case
        //        {
        //            objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objCase.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }

        //            if (flagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
        //                }
        //            }
        //            if (AflagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
        //                }
        //            }
        //            if (WLResult != -1)
        //            {
        //                objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
        //            }

        //            var assignedcaseList = CustomerBranchManagement.GetAssignedCaseNoticeLitigation(CustomerID, UserId, FlagIsApp);
        //            if (assignedcaseList.Count > 0)
        //            {
        //                objCase = objCase.Where(entry => assignedcaseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize]
        //[Route("Litigation/MyCaseNoticeWorkSpace")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage MyCaseNoticeWorkSpace(int UserId, int CustomerID, string FlagIsApp, string MonthId, int StatusFlag, string FY, int StatusType,/*string CourtId,string CategoryID,*/ string StartDateDetail, string EndDateDetail, string CY, int WLResult, string AssingnedLawyer)
        //{
        //    try
        //    {
        //        DateTime Fromdate = new DateTime();
        //        DateTime Todate = new DateTime();

        //        int flagDate = 0;
        //        int AflagDate = 0;
        //        DateTime AFromdate = new DateTime();
        //        DateTime ATodate = new DateTime();

        //        List<MyReportLitigation> objResult = new List<MyReportLitigation>();
        //        List<sp_LitigationNoticeReportWithCustomParameter_Result> objNotice = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
        //        List<SP_LitigationCaseReportWithCustomParameter_Result> objCase = new List<SP_LitigationCaseReportWithCustomParameter_Result>();

        //        if (CY != "0" && !String.IsNullOrEmpty(CY))
        //        {
        //            string dtfrom = Convert.ToString("01" + "-01" + "-" + Convert.ToInt32(CY));
        //            string dtto = Convert.ToString("31" + "-12" + "-" + Convert.ToInt32(CY));
        //            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            flagDate = 1;
        //        }

        //        if (FY != "0" && !String.IsNullOrEmpty(FY))
        //        {
        //            string[] y = FY.Split('-');
        //            if (y.Length > 1)
        //            {
        //                if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
        //                {
        //                    string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
        //                    string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
        //                    Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    flagDate = 1;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (MonthId != "All")
        //            {
        //                flagDate = 1;
        //                Todate = DateTime.Now;
        //                if (MonthId == "1")
        //                    Fromdate = DateTime.Now.AddDays(-30);

        //                if (MonthId == "3")
        //                    Fromdate = DateTime.Now.AddDays(-90);

        //                if (MonthId == "6")
        //                    Fromdate = DateTime.Now.AddDays(-180);

        //                if (MonthId == "12")
        //                    Fromdate = DateTime.Now.AddDays(-365);
        //            }

        //        }

        //        if (FlagIsApp.Equals("AUD"))
        //        {
        //            AflagDate = 1;
        //            FlagIsApp = "MGMT";
        //            User uobj = UserManagement.GetByID(UserId);
        //            if (uobj != null)
        //            {
        //                AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
        //                ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
        //            }
        //        }
        //        if (StatusFlag == 1)//Notice
        //        {
        //            objNotice = LitigationDocumentManagement.GetNoticeReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objNotice.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objNotice = objNotice.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }


        //            if (flagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //            }
        //            if (AflagDate == 1)
        //            {
        //                objNotice = objNotice.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //            }
        //            if (WLResult != -1)
        //            {
        //                objNotice = objNotice.Where(x => x.WLResult == WLResult).ToList();
        //            }
        //            var caseList = CustomerBranchManagement.GetAssignedCaseNoticeLitigation(CustomerID, UserId, FlagIsApp);
        //            if (caseList.Count > 0)
        //            {
        //                objNotice = objNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }
        //        else if (StatusFlag == 2)//Case
        //        {
        //            objCase = LitigationDocumentManagement.GetAllCaseReportData(CustomerID, UserId, FlagIsApp, StatusType, AssingnedLawyer);

        //            if (objCase.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(StartDateDetail))
        //                {
        //                    DateTime sd = DateTime.ParseExact(StartDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime sd = Convert.ToDateTime(StartDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate >= sd)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate >= sd)).ToList();
        //                    }
        //                }
        //                if (!string.IsNullOrEmpty(EndDateDetail))
        //                {
        //                    DateTime Ld = DateTime.ParseExact(EndDateDetail, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        //                    //DateTime Ld = Convert.ToDateTime(EndDateDetail);
        //                    if (StatusType == 1 || StatusType == -1)
        //                    {
        //                        objCase = objCase.Where(entry => (entry.OpenDate <= Ld)).ToList();
        //                    }
        //                    else
        //                    {
        //                        objCase = objCase.Where(entry => (entry.CloseDate <= Ld)).ToList();
        //                    }
        //                }
        //            }

        //            if (flagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= Fromdate && entry.OpenDate <= Todate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= Fromdate && entry.CloseDate <= Todate)).ToList();
        //                }
        //            }
        //            if (AflagDate == 1)
        //            {
        //                if (StatusType == 1 || StatusType == -1)
        //                {
        //                    objCase = objCase.Where(entry => (entry.OpenDate >= AFromdate && entry.OpenDate <= ATodate)).ToList();
        //                }
        //                else
        //                {
        //                    objCase = objCase.Where(entry => (entry.CloseDate >= AFromdate && entry.CloseDate <= ATodate)).ToList();
        //                }
        //            }
        //            if (WLResult != -1)
        //            {
        //                objCase = objCase.Where(x => x.WLResult == WLResult).ToList();
        //            }

        //            var assignedcaseList = CustomerBranchManagement.GetAssignedCaseNoticeLitigation(CustomerID, UserId, FlagIsApp);
        //            if (assignedcaseList.Count > 0)
        //            {
        //                objCase = objCase.Where(entry => assignedcaseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
        //            }
        //            objResult.Add(new MyReportLitigation { Notice = objNotice, Case = objCase });
        //        }


        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        [Authorize] 
        [Route("Litigation/Delete_CaseNotice_WorkSpace")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Delete_CaseNotice_WorkSpace(string UserID, string CaseInstanceID, string Type)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                bool result = false;
                if (!string.IsNullOrEmpty(UserID) && !string.IsNullOrEmpty(CaseInstanceID))
                {
                    int InstanceID = Convert.ToInt32(CaseInstanceID);
                    if (Type != null && Type == "C")
                    {
                        result = CaseManagement.DeleteCaseByID(InstanceID, Convert.ToInt32(UserID));
                    }
                    if (Type != null && Type == "N")
                    {
                        result = NoticeManagement.DeleteNoticeByID(InstanceID, Convert.ToInt32(UserID));
                    }
                    if (result)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            entities.sp_DeleteMISSummaryAndParticulars(InstanceID);
                        }
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

      
        [Route("Litigation/CreateTaskDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage CreateTaskDetail(string Email, string TaskTitle, string TaskDescription, string DueDate, string Priority, string ExpectedOutcome, int InternalUser, int ExternalUser, string Remark)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        bool saveSuccess = false;
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            int RoleId = Convert.ToInt32(user.RoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();
                            string UserName = user.FirstName + " " + user.LastName;

                            tbl_TaskScheduleOn newRecord = new tbl_TaskScheduleOn();

                            newRecord.IsActive = true;
                            newRecord.NoticeCaseInstanceID = 0;
                            newRecord.ScheduleOnDate = GetDate(DueDate);
                            newRecord.TaskTitle = TaskTitle.Trim();
                            newRecord.TaskDesc = TaskDescription.Trim();
                            newRecord.ExpOutcome = ExpectedOutcome;
                            newRecord.StatusID = 1;
                            newRecord.CustomerID = customerId;
                            newRecord.CreatedBy = Convert.ToInt32(user.ID);
                            newRecord.CreatedByText = UserName;
                            newRecord.LinkCreatedOn = DateTime.Now;
                            newRecord.URLExpired = false;

                            if (InternalUser != -1)
                                newRecord.AssignTo = InternalUser;

                            if (ExternalUser != -1)
                                newRecord.AssignTo = ExternalUser;

                            if (!String.IsNullOrEmpty(Priority))
                                newRecord.PriorityID = Convert.ToInt32(Priority);

                            if (Remark != "")
                                newRecord.Remark = Remark;

                            saveSuccess = false;
                            newRecord.TaskType = "T";
                            if (!LitigationTaskManagement.ExistNoticeCaseTaskTitle(TaskTitle.Trim(), "C", (long)newRecord.NoticeCaseInstanceID, customerId))
                            {                               
                                saveSuccess = LitigationTaskManagement.CreateTask(newRecord);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("T", Convert.ToInt32(0), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerId), Convert.ToInt32(user.ID), "New Task Created", true);

                                    var httpRequest = HttpContext.Current.Request;
                                    saveSuccess = UploadFormSixteenRead(httpRequest, Convert.ToInt32(newRecord.NoticeCaseInstanceID), Convert.ToInt32(newRecord.ID), Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), UserName);
                                }
                            }
                            else
                            {
                                saveSuccess = false;
                                UpdateNotificationObj.Clear();
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Task with same title already exists." });
                            }

                            if (saveSuccess)
                            {
                                string url = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(customerId));
                                if (Urloutput != null)
                                {
                                    url = Urloutput.URL;
                                }
                                else
                                {
                                    url = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }

                                string accessURL = string.Empty;
                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerId))
                                    accessURL = Convert.ToString(url) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                  CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                                  "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                                else
                                    accessURL = Convert.ToString(url);
                                //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerId))
                                //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                //  CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                                //  "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                                //else
                                //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);

                                if (saveSuccess)
                                {
                                    //saveSuccess = SendTaskAssignmentMail(newRecord, accessURL, UserName, customerId);

                                    newRecord.AccessURL = accessURL;
                                    saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);                                   
                                    UpdateNotificationObj.Clear();
                                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                                }
                            }                           
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                UpdateNotificationObj.Clear();
                UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy, int customerID)
        {
            try
            {
                User TaskAssignedUserDetail = null;
                
                #region Mail Data
                //User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                //string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                if (!string.IsNullOrEmpty(Convert.ToString(taskRecord.AssignTo)))
                {
                    TaskAssignedUserDetail = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                }
                var Locations = string.Empty;

                string TaskTitleMerge = taskRecord.TaskTitle;
                string FinalTaskTitle = string.Empty;
                if (TaskTitleMerge.Length > 50)
                {
                    FinalTaskTitle = TaskTitleMerge.Substring(0, 50);
                    FinalTaskTitle = FinalTaskTitle + "...";
                }
                else
                {
                    FinalTaskTitle = TaskTitleMerge;
                }

                #endregion end mail

                if (taskRecord != null)
                {
                    User UserAssigeed = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                    var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));

                    if (UserAssigeed != null)
                    {
                        if (UserAssigeed.Email != null && UserAssigeed.Email != "")
                        {
                            string username = string.Format("{0} {1}", UserAssigeed.FirstName, UserAssigeed.LastName);

                            string url = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(customerID));
                            if (Urloutput != null)
                            {
                                url = Urloutput.URL;
                            }
                            else
                            {
                                url = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = Properties.Settings.Default.EmailTemplate_Litigation_IndependentTaskAssignment
                                                                      .Replace("@User", username)
                                                                      .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                                      .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                                      .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                                      .Replace("@From", cname.Trim())
                                                                      .Replace("@PortalURL", Convert.ToString(url));

                            //string message = Properties.Settings.Default.EmailTemplate_Litigation_IndependentTaskAssignment
                            //                                            .Replace("@User", username)
                            //                                            .Replace("@TaskTitle", taskRecord.TaskTitle)
                            //                                            .Replace("@TaskDesc", taskRecord.TaskDesc)
                            //                                            .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                            //                                            .Replace("@From", cname.Trim())
                            //                                            .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { TaskAssignedUserDetail.Email }), null, null, "Litigation Notification Task Assigned - " + FinalTaskTitle, message);//FinalNoticeTitle

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public static bool UploadFormSixteenRead(HttpRequest CurrentRequest, int NoticeCaseInstanceID, int TaskID, int UserID, int customerID, string User)
        {
            var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
            if (AWSData != null)
            {
                #region AWS
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        if (TaskID > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                            for (int i = 0; i < CurrentRequest.Files.Count; i++)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = NoticeCaseInstanceID,
                                    DocTypeInstanceID = TaskID,
                                    CreatedBy = UserID,
                                    CreatedByText = User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "T"
                                };

                                //string directoryPath = "";
                                string fileName = "";
                                HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');
                                    
                                    fileName = uploadedFile.FileName;
                                  
                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                    caseTaskDocVersion++;
                                    objTaskDoc.Version = caseTaskDocVersion + ".0";
                                    
                                    string FileLocationPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(NoticeCaseInstanceID) + "\\" + "Task\\" + objTaskDoc.Version;
                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, FileLocationPath);
                                    if (!di.Exists)
                                    {
                                        di.Create();
                                    }
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    string storagedrive = ConfigurationManager.AppSettings["UploaddestinationPath"];

                                    string p_strPath = string.Empty;
                                    string dirpath = string.Empty;
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    p_strPath = @"" + storagedrive + "/" + UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                    dirpath = @"" + storagedrive + "/" + UserID + "/" + FileDate;
                                    
                                    if (!Directory.Exists(dirpath))
                                    {
                                        Directory.CreateDirectory(dirpath);
                                    }
                                    FileStream objFileStrm = File.Create(p_strPath);
                                    objFileStrm.Close();
                                    File.WriteAllBytes(p_strPath, bytes);
                                    Guid fileKey1 = Guid.NewGuid();
                                    string AWSpath = FileLocationPath + "\\" + uploadedFile.FileName;
                                    
                                    objTaskDoc.FileName = fileName;
                                    objTaskDoc.FilePath = FileLocationPath.Replace(@"\", "/");
                                    objTaskDoc.FileKey = fileKey1.ToString();
                                    objTaskDoc.VersionDate = DateTime.Now;
                                    objTaskDoc.CreatedOn = DateTime.Now;
                                    objTaskDoc.FileSize = uploadedFile.ContentLength;
                                    FileInfo localFile = new FileInfo(p_strPath);
                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                    if (!s3File.Exists)
                                    {
                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                        {
                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                        }
                                    }
                                    //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);
                                    Filelist1.Clear();
                                }
                            }//End For Each      
                            if (saveSuccess)
                            {
                                saveSuccess = true;
                                LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
            else
            {
                #region Normal
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        if (TaskID > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                            for (int i = 0; i < CurrentRequest.Files.Count; i++)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = NoticeCaseInstanceID,
                                    DocTypeInstanceID = TaskID,
                                    CreatedBy = UserID,
                                    CreatedByText = User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "T"
                                };

                                string directoryPath = "";
                                string fileName = "";
                                HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');

                                    //if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                    //{
                                    fileName = uploadedFile.FileName;
                                    //}

                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                    caseTaskDocVersion++;
                                    objTaskDoc.Version = caseTaskDocVersion + ".0";

                                    string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                    string FileLocationPath = "/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(NoticeCaseInstanceID) + "/Task/" + objTaskDoc.Version;

                                    string FileLocationPathforconvert = (FileLocationPath).ToString();
                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                    directoryPath = destinationPath + FileLocationPathforconvert;
                                    Directory.CreateDirectory(directoryPath);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objTaskDoc.FileName = fileName;
                                    objTaskDoc.FilePath = "~" + FileLocationPath; //directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objTaskDoc.FileKey = fileKey1.ToString();
                                    objTaskDoc.VersionDate = DateTime.Now;
                                    objTaskDoc.CreatedOn = DateTime.Now;
                                    objTaskDoc.FileSize = uploadedFile.ContentLength;

                                    DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);
                                    Filelist1.Clear();
                                }
                            }//End For Each      
                            if (saveSuccess)
                            {
                                saveSuccess = true;
                                LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
           
        }

        [Authorize] 
        [Route("Litigation/UpdateReminderDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpdateReminderDetails([FromBody]AddReminder Remindobj)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                string Email = Remindobj.Email.ToString();

                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            if (!string.IsNullOrEmpty(Remindobj.Flag))
                            {
                                if (Remindobj.Flag.Equals("DELETE"))
                                {
                                    if (Remindobj.ReminderID != null && Remindobj.ReminderID > 0)
                                    {
                                        int reminderID = Convert.ToInt32(Remindobj.ReminderID);

                                        bool deleteSuccess = LitigationManagement.DeleteLitigationReminderByID(reminderID);
                                        if (deleteSuccess)
                                        {
                                            UpdateNotificationObj.Clear();
                                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder Deleted Successfully." });
                                        }
                                    }
                                    else
                                    {
                                        UpdateNotificationObj.Clear();
                                        UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder Deleted not Successfully." });
                                    }
                                }
                                else
                                {
                                    int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                                    int RoleId = Convert.ToInt32(user.RoleID);
                                    string RoleCheck = (from row in entities.Roles
                                                        where row.ID == RoleId
                                                        select row.Code).FirstOrDefault();
                                    string UserName = user.FirstName + " " + user.LastName;

                                    string selectedType = Remindobj.type;

                                    if (selectedType == "T")
                                    {
                                        if (!string.IsNullOrEmpty(Remindobj.InstanceID))
                                        {
                                            if (Remindobj.InstanceID != "-1")
                                            {
                                                selectedType = LitigationTaskManagement.GetTaskTypebyID(Convert.ToInt32(Remindobj.InstanceID));
                                            }
                                        }
                                        else
                                        {
                                            selectedType = "T";
                                        }
                                    }

                                    tbl_LitigationCustomReminder newRecord = new tbl_LitigationCustomReminder();

                                    newRecord.Type = selectedType;
                                    newRecord.InstanceID = Convert.ToInt32(Remindobj.InstanceID);
                                    newRecord.UserID = Convert.ToInt32(user.ID);
                                    newRecord.ReminderTitle = Remindobj.Reminder;
                                    newRecord.Description = Remindobj.Description;
                                    newRecord.RemindOn = GetDate(Remindobj.RemindDate); // Convert.ToDateTime(Remindobj.RemindDate);
                                    newRecord.Status = 0;
                                    newRecord.IsDeleted = false;
                                    newRecord.CreatedBy = Convert.ToInt32(user.ID);

                                    if (!String.IsNullOrEmpty(Remindobj.Remark))
                                        newRecord.Remark = Remindobj.Remark;

                                    bool saveSuccess = false;
                                    if (Remindobj.ReminderID != null && Remindobj.ReminderID > 0)
                                    {
                                        long reminderID = Convert.ToInt32(Remindobj.ReminderID);

                                        if (reminderID != 0)
                                        {
                                            newRecord.ID = reminderID;
                                            newRecord.UpdatedBy = Convert.ToInt32(user.ID);

                                            if (!LitigationManagement.ExistsLitigationReminder(newRecord, newRecord.ID))
                                                saveSuccess = LitigationManagement.UpdateLitigationReminder(newRecord);
                                            else
                                            {
                                                saveSuccess = false;
                                                UpdateNotificationObj.Clear();
                                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder with same details already exists." });
                                            }

                                            if (saveSuccess)
                                            {
                                                saveSuccess = false;
                                                UpdateNotificationObj.Clear();
                                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder Updated Successfully." });

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!LitigationManagement.ExistsLitigationReminder(newRecord, 0))
                                        {
                                            saveSuccess = LitigationManagement.CreateLitigationReminder(newRecord);
                                            if (saveSuccess)
                                            {
                                                UpdateNotificationObj.Clear();
                                                UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder Saved Successfully" });
                                            }
                                        }
                                        else
                                        {
                                            UpdateNotificationObj.Clear();
                                            UpdateNotificationObj.Add(new UpdateNotification { Message = "Reminder with same details already exists" });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                UpdateNotificationObj.Clear();
                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/TitlelistusingType")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage TitlelistusingType([FromBody]myreminderTitleclass myobj)
        {
            List<OutputList> Dobj = new List<OutputList>();

            string Email = myobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            var branchList = new List<int>();
                            int partyID = -1;
                            int deptID = -1;
                            int Status = -1;
                            int priorityID = -1;
                            string Type = string.Empty;
                            int RoleId = Convert.ToInt32(user.LitigationRoleID);
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            if (myobj.Type == "N") //N--Notice
                            {
                                var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerId, Convert.ToInt32(user.ID), RoleCheck, 3, branchList, partyID, deptID, Status, Type);
                                Dobj = (from row in lstNoticeDetails
                                        select new OutputList
                                        {
                                            ID = row.NoticeInstanceID,
                                            Name = row.NoticeTitle
                                        }).ToList();
                            }
                            if (myobj.Type == "C") //C--Case
                            {
                                var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerId, Convert.ToInt32(user.ID), RoleCheck, 3, branchList, partyID, deptID, Status, Type);
                                Dobj = (from row in lstCaseDetails
                                        select new OutputList
                                        {
                                            ID = row.CaseInstanceID,
                                            Name = row.CaseTitle
                                        }).ToList();
                            }
                            if (myobj.Type == "T") //T--Task
                            {
                                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(Convert.ToInt32(user.ID), RoleCheck, 3, priorityID, partyID, deptID, Status, Type, customerId);

                                Dobj = (from row in lstTaskDetails
                                        select new OutputList
                                        {
                                            ID = row.TaskID,
                                            Name = row.TaskTitle
                                        }).ToList();
                            }
                            Dobj = Dobj.OrderBy(entry => entry.Name).ToList();

                        }

                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                //List<ErrorDetails> errordetail = new List<ErrorDetails>();
                //errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/BindUser")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindUser([FromBody]myreminderTitleclass myobj)
        {
            List<OutputList> Dobj = new List<OutputList>();

            string Email = myobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            
                            var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerId);

                            if (lstAllUsers.Count > 0)
                                lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();                           

                            if (myobj.Type == "I") //Internal User
                            {
                                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);
                                Dobj = (from row in internalUsers
                                        select new OutputList
                                        {
                                            ID = row.ID,
                                            Name = row.Name
                                        }).ToList();
                                
                            }
                            if (myobj.Type == "E") //External User
                            {
                                var lawyerAndExternalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);
                                                               
                                Dobj = (from row in lawyerAndExternalUsers
                                        select new OutputList
                                        {
                                            ID = row.ID,
                                            Name = row.Name
                                        }).ToList();
                            }
                            Dobj = Dobj.OrderBy(entry => entry.Name).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                //List<ErrorDetails> errordetail = new List<ErrorDetails>();
                //errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/getCaseList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage getCaseList([FromBody]getCaseList objCL)
        {
            try
            {
                string Email = objCL.Email.ToString();
                List<CaseLitDetails> query = new List<CaseLitDetails>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                        int RoleId = Convert.ToInt32(user.LitigationRoleID);
                        string RoleCheck = (from row in entities.Roles
                                            where row.ID == RoleId
                                            select row.Code).FirstOrDefault();

                        if (user != null)
                        {
                            var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerId, Convert.ToInt32(user.ID), RoleCheck, RoleId);
                            if (lstCaseDetails.Count > 0)
                            {
                                lstCaseDetails = lstCaseDetails.Where(x => x.TxnStatusID == 1).ToList();

                                query = (from row in lstCaseDetails
                                         select new CaseLitDetails()
                                         {
                                             ID = Convert.ToInt32(row.CaseInstanceID),
                                             Name = row.CaseTitle.ToString(),
                                         }).ToList();
                            }

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/AddHearingDate")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddHearingDate([FromBody] AddHearingDate hearingobj)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

            try
            {
                bool Successresult = false;
                string Email = hearingobj.Email.ToString();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                            {
                                CaseNoticeInstanceID = hearingobj.CaseInstanceID,                                
                                HearingDate = GetDate(hearingobj.Dt),
                                //HearingDate = Convert.ToDateTime(hearingobj.Dt),
                                CustomerID = customerId,
                                IsDeleted = false,
                                CreatedBy = Convert.ToInt32(user.ID),
                            };
                            var result = CaseManagement.GetExistsRefNo(objNewRefNo);


                            if (result != -1)
                            {
                                objNewRefNo.HearingRefNo = "Hearing-" + hearingobj.Dt;

                                var newID = CaseManagement.CreateNewRefNo(objNewRefNo);
                                if (newID > 0)
                                {
                                    Successresult = true;
                                    LitigationManagement.CreateAuditLog("C", hearingobj.CaseInstanceID, "tbl_CaseHearingRef", "Add", customerId, Convert.ToInt32(user.ID), "Case Hearing Reference Created", true);
                                }
                            }

                        }
                    }
                    if (Successresult == true)
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    else
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize] 
        [Route("Litigation/GetAllHearingDate")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllHearingDate([FromBody] GetAllHearingDate dateobj)
        {
            string Email = dateobj.Email.ToString();
            try
            {
                List<AllHearingDate> query2 = new List<AllHearingDate>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            var DateObj = CaseManagement.GetAllRefNo(customerId, Convert.ToInt32(dateobj.CaseInstanceID));


                            if (DateObj.Count > 0)
                            {
                                query2 = (from row in DateObj
                                          select new AllHearingDate()
                                          {
                                              HearingRefNo = row.HearingRefNo,
                                              ID = row.ID
                                          }).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
               
        [Route("Litigation/AddHearingDetails")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddHearingDetails(string EmailID, int CaseInstanceID, int HearingRefNoID, string Description, string NextHearingDate, string RemindMeOn, string Remarks)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            
            try
            {
                bool Successresult = false;
                bool validateData = false;
                tbl_CaseHearingRef refNoDetail = null;
                long newResponseID = 0;
                string Email = EmailID.ToString();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            string UserName = user.FirstName + " " + user.LastName;
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            if (!string.IsNullOrEmpty(Description))
                            {
                                var refID = Convert.ToInt32(HearingRefNoID);

                                if (refID != 0)
                                {
                                    refNoDetail = CaseManagement.GetRefNoDetail(refID);

                                    if (refNoDetail != null)
                                        if (refNoDetail.HearingDate != null)
                                            validateData = true;
                                }
                            }
                            if (validateData)
                            {
                                tbl_LegalCaseResponse newRecord = new tbl_LegalCaseResponse()
                                {
                                    IsActive = true,
                                    CaseInstanceID = CaseInstanceID,
                                    Description = Description,
                                    CreatedBy = Convert.ToInt32(user.ID),
                                    CreatedByText = user.FirstName + ' ' + user.LastName,
                                };
                                if (!string.IsNullOrEmpty(NextHearingDate))
                                {
                                    newRecord.ReminderDate = GetDate(NextHearingDate);
                                }

                                if (!string.IsNullOrEmpty(RemindMeOn))
                                {
                                    newRecord.ReminderMeONDate = GetDate(RemindMeOn); ;
                                }


                                if (refNoDetail != null)
                                {
                                    newRecord.RefID = refNoDetail.ID;

                                    if (refNoDetail.HearingDate != null)
                                    {
                                        newRecord.ResponseDate = refNoDetail.HearingDate;
                                    }
                                }

                                if (!string.IsNullOrEmpty(Remarks))
                                    newRecord.Remark = Remarks;

                                newResponseID = CaseManagement.CreateCaseResponseLog(newRecord);
                                bool saveSuccess = false;
                                if (newResponseID > 0)
                                {
                                    LitigationManagement.CreateAuditLog("CH", CaseInstanceID, "tbl_LegalCaseResponse", "Add", customerId, Convert.ToInt32(user.ID), "Hearing Created", true);
                                    var httpRequest = HttpContext.Current.Request;
                                    saveSuccess = UploadFileHearing(httpRequest, Convert.ToInt32(CaseInstanceID), Convert.ToInt32(newRecord.ID), Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), UserName);
                                   // Successresult = true;
                                }
                                if (saveSuccess)
                                {
                                    Successresult = true;
                                }
                            }
                        }
                    }
                    if (Successresult == true)
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    else
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/GetAllHearing")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllHearing([FromBody] GetAllHearing objHearing)
        {
            string Email = objHearing.Email.ToString();
            try
            {
                List<View_LegalCaseResponse> query2 = new List<View_LegalCaseResponse>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            query2 = CaseManagement.GetCaseResponseDetails(objHearing.CaseInstanceID);
                            if (query2.Count > 0)
                            {
                                query2 = query2.OrderByDescending(entry => entry.HearingDate).ToList();
                            }

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query2).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Litigation/GetAllTask")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllTask([FromBody] GetAllTask objTask)
        {
            try
            {
                string Email = objTask.Email.ToString();
                List<View_NoticeCaseTaskDetail> query = new List<View_NoticeCaseTaskDetail>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            query = LitigationTaskManagement.GetTaskDetails(objTask.CaseInstanceID, "C");
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(query).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

     
        [Route("Litigation/GetAllDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllDocuments([FromBody] GetAllDocuments objDoc)
        {
            try
            {
                string Email = objDoc.Email.ToString();
                List<CaseDocumentDetail> lstCaseDocs = new List<CaseDocumentDetail>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            var lstCaseDocs1 = (from row in entities.SP_LitigationCaseDocumentfor_tag(customerId)
                                                select row).ToList();

                            if (objDoc.CaseInstanceID != 0)
                            {
                                lstCaseDocs1 = lstCaseDocs1.Where(x => x.NoticeCaseInstanceID == objDoc.CaseInstanceID).ToList();
                            }

                            if (lstCaseDocs1.Count > 0)
                            {
                                lstCaseDocs = (from g in lstCaseDocs1
                                               group g by new
                                               {
                                                   g.NoticeCaseInstanceID,
                                                   g.TypeCaseNotice,
                                                   g.FileName,
                                                   g.FileID,
                                                   g.TypeName,
                                                   g.RefNo,
                                                   g.Title,
                                                   g.Description,
                                                   g.BranchName,
                                                   g.DeptName,
                                                   g.OwnerID,
                                                   g.Owner,
                                                   //g.AssignedUser,
                                                   // g.Party,
                                                   //g.AssignedUserRoleID,
                                                   //g.AssignedUserName,
                                                   g.CustomerID,
                                                   g.DepartmentID,
                                                   g.CustomerBranchID,
                                                   //g.AssignedTo,
                                                   g.Status,
                                                   g.IsDeleted,
                                                   g.Filetag,
                                                   g.FYName,
                                                   g.DocumentCount,
                                                   g.PartyID,
                                                   g.PartyName,
                                                   g.Createdate
                                               } into GCS
                                               select new CaseDocumentDetail
                                               {
                                                   NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                                   TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                                   FileName = GCS.Key.FileName,
                                                   FileID = GCS.Key.FileID,
                                                   TypeName = GCS.Key.TypeName,
                                                   RefNo = GCS.Key.RefNo,
                                                   Title = GCS.Key.Title,
                                                   Description = GCS.Key.Description,
                                                   BranchName = GCS.Key.BranchName,
                                                   DeptName = GCS.Key.DeptName,
                                                   OwnerID = GCS.Key.OwnerID,
                                                   Owner = GCS.Key.Owner,
                                                   //AssignedUser = GCS.Key.AssignedUser,
                                                   //AssignedUserRoleID = GCS.Key.AssignedUserRoleID,
                                                   //AssignedUserName = GCS.Key.AssignedUserName,
                                                   CustomerID = GCS.Key.CustomerID,
                                                   DepartmentID = GCS.Key.DepartmentID,
                                                   CustomerBranchID = GCS.Key.CustomerBranchID,
                                                   Status = GCS.Key.Status,
                                                   IsDeleted = GCS.Key.IsDeleted,
                                                   Filetag = GCS.Key.Filetag,
                                                   FYName = GCS.Key.FYName,
                                                   DocumentCount = GCS.Key.DocumentCount,
                                                   PartyID = GCS.Key.PartyID,
                                                   PartyName = GCS.Key.PartyName,
                                                   Createdate = GCS.Key.Createdate
                                               }).ToList();
                            }

                            if (lstCaseDocs.Count > 0)
                            {
                                if (objDoc.Flag == "Current")
                                {
                                    lstCaseDocs = lstCaseDocs.OrderByDescending(x => x.Createdate).Take(1).ToList();
                                }
                                else
                                {
                                    var RemoveCusrrentFileId = lstCaseDocs.OrderByDescending(x => x.Createdate).Take(1).FirstOrDefault();
                                    lstCaseDocs.Remove(RemoveCusrrentFileId);
                                }
                            }
                            //if (lstCaseDocs1.Count > 0)
                            //{
                            //    lstCaseDocs = (from g in lstCaseDocs1
                            //                   group g by new
                            //                   {
                            //                       g.NoticeCaseInstanceID,
                            //                       g.TypeCaseNotice,
                            //                       g.FileName,
                            //                       g.FileID,
                            //                       g.TypeName,
                            //                       g.RefNo,
                            //                       g.Title,
                            //                       g.Description,
                            //                       g.BranchName,
                            //                       g.DeptName,
                            //                       g.OwnerID,
                            //                       g.Owner,
                            //                       g.AssignedUser,
                            //                       // g.Party,
                            //                       g.AssignedUserRoleID,
                            //                       g.AssignedUserName,
                            //                       g.CustomerID,
                            //                       g.DepartmentID,
                            //                       g.CustomerBranchID,
                            //                       //g.AssignedTo,
                            //                       g.Status,
                            //                       g.IsDeleted,
                            //                       g.Filetag,
                            //                       g.FYName,
                            //                       g.DocumentCount,
                            //                       g.PartyID,
                            //                       g.PartyName
                            //                   } into GCS
                            //                   select new CaseDocumentDetail
                            //                   {
                            //                       NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                            //                       TypeCaseNotice = GCS.Key.TypeCaseNotice,
                            //                       FileName = GCS.Key.FileName,
                            //                       FileID = GCS.Key.FileID,
                            //                       TypeName = GCS.Key.TypeName,
                            //                       RefNo = GCS.Key.RefNo,
                            //                       Title = GCS.Key.Title,
                            //                       Description = GCS.Key.Description,
                            //                       BranchName = GCS.Key.BranchName,
                            //                       DeptName = GCS.Key.DeptName,
                            //                       OwnerID = GCS.Key.OwnerID,
                            //                       Owner = GCS.Key.Owner,
                            //                       AssignedUser = GCS.Key.AssignedUser,
                            //                       AssignedUserRoleID = GCS.Key.AssignedUserRoleID,
                            //                       AssignedUserName = GCS.Key.AssignedUserName,
                            //                       CustomerID = GCS.Key.CustomerID,
                            //                       DepartmentID = GCS.Key.DepartmentID,
                            //                       CustomerBranchID = GCS.Key.CustomerBranchID,
                            //                       Status = GCS.Key.Status,
                            //                       IsDeleted = GCS.Key.IsDeleted,
                            //                       Filetag = GCS.Key.Filetag,
                            //                       FYName = GCS.Key.FYName,
                            //                       DocumentCount = GCS.Key.DocumentCount,
                            //                       PartyID = GCS.Key.PartyID,
                            //                       PartyName = GCS.Key.PartyName
                            //                   }).ToList();
                            //}
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstCaseDocs).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        ////[Authorize]
        //[Route("Litigation/GetAllDocuments")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAllDocuments([FromBody] GetAllDocuments objDoc)
        //{
        //    try
        //    {
        //        string Email = objDoc.Email.ToString();
        //        List<CaseDocumentDetail> lstCaseDocs = new List<CaseDocumentDetail>();
        //        Email = Email.Replace(" ", "+");
        //        var encryptedBytes = Convert.FromBase64String(Email);
        //        try
        //        {
        //            Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
        //        }
        //        catch { }

        //        string EmailIOS = string.Empty;
        //        try
        //        {
        //            EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
        //        }
        //        catch { }

        //        if (!string.IsNullOrEmpty(Email))
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                User user = (from row in entities.Users
        //                             where (row.Email == Email || row.Email == EmailIOS)
        //                               && row.IsActive == true
        //                               && row.IsDeleted == false
        //                             select row).FirstOrDefault();

        //                if (user != null)
        //                {
        //                    int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

        //                    var lstCaseDocs1 = (from row in entities.SP_LitigationCaseDocumentfor_tag(customerId)
        //                                   select row).ToList();
        //                    if (objDoc.CaseInstanceID != 0)
        //                    {
        //                        lstCaseDocs1 = lstCaseDocs1.Where(x => x.NoticeCaseInstanceID == objDoc.CaseInstanceID).ToList();
        //                    }
        //                    string CurrentYear = Convert.ToString(DateTime.Now.Year) + "-" + Convert.ToInt32(DateTime.Now.Year + 1);

        //                    if (objDoc.Flag == "Current")
        //                    {
        //                        lstCaseDocs1 = lstCaseDocs1.Where(x => x.FYName.Contains(CurrentYear)).ToList();
        //                    }
        //                    else
        //                    {
        //                        var CaseDocs = lstCaseDocs1.Where(x => x.FYName.Contains(CurrentYear)).ToList();

        //                        lstCaseDocs1 = lstCaseDocs1.Except(CaseDocs).ToList();
        //                    }
        //                    if (lstCaseDocs1.Count > 0)
        //                    {


        //                        lstCaseDocs = (from g in lstCaseDocs1
        //                                       group g by new
        //                                       {
        //                                           g.NoticeCaseInstanceID,
        //                                           g.TypeCaseNotice,
        //                                           g.FileName,
        //                                           g.FileID,
        //                                           g.TypeName,
        //                                           g.RefNo,
        //                                           g.Title,
        //                                           g.Description,
        //                                           g.BranchName,
        //                                           g.DeptName,
        //                                           g.OwnerID,
        //                                           g.Owner,
        //                                           g.AssignedUser,
        //                                           // g.Party,
        //                                           g.AssignedUserRoleID,
        //                                           g.AssignedUserName,
        //                                           g.CustomerID,
        //                                           g.DepartmentID,
        //                                           g.CustomerBranchID,
        //                                           //g.AssignedTo,
        //                                           g.Status,
        //                                           g.IsDeleted,
        //                                           g.Filetag,
        //                                           g.FYName,
        //                                           g.DocumentCount,
        //                                           g.PartyID,
        //                                           g.PartyName
        //                                       } into GCS
        //                                       select new CaseDocumentDetail
        //                                       {
        //                                           NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                                           TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                                           FileName = GCS.Key.FileName,
        //                                           FileID = GCS.Key.FileID,
        //                                           TypeName = GCS.Key.TypeName,
        //                                           RefNo = GCS.Key.RefNo,
        //                                           Title = GCS.Key.Title,
        //                                           Description = GCS.Key.Description,
        //                                           BranchName = GCS.Key.BranchName,
        //                                           DeptName = GCS.Key.DeptName,
        //                                           OwnerID = GCS.Key.OwnerID,
        //                                           Owner = GCS.Key.Owner,
        //                                           AssignedUser = GCS.Key.AssignedUser,
        //                                           AssignedUserRoleID = GCS.Key.AssignedUserRoleID,
        //                                           AssignedUserName = GCS.Key.AssignedUserName,
        //                                           CustomerID = GCS.Key.CustomerID,
        //                                           DepartmentID = GCS.Key.DepartmentID,
        //                                           CustomerBranchID = GCS.Key.CustomerBranchID,
        //                                           Status = GCS.Key.Status,
        //                                           IsDeleted = GCS.Key.IsDeleted,
        //                                           Filetag = GCS.Key.Filetag,
        //                                           FYName = GCS.Key.FYName,
        //                                           DocumentCount = GCS.Key.DocumentCount,
        //                                           PartyID = GCS.Key.PartyID,
        //                                           PartyName = GCS.Key.PartyName
        //                                       }).ToList();
        //                    }
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(lstCaseDocs).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize]
        //[Route("Litigation/GetAllDocuments")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAllDocuments([FromBody] GetAllDocuments objDoc)
        //{
        //    try
        //    {
        //        string Email = objDoc.Email.ToString();
        //        List<Sp_Litigation_CaseDocument_Result> lstCaseDocs = new List<Sp_Litigation_CaseDocument_Result>();
        //        Email = Email.Replace(" ", "+");
        //        var encryptedBytes = Convert.FromBase64String(Email);
        //        try
        //        {
        //            Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
        //        }
        //        catch { }

        //        string EmailIOS = string.Empty;
        //        try
        //        {
        //            EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
        //        }
        //        catch { }

        //        if (!string.IsNullOrEmpty(Email))
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                User user = (from row in entities.Users
        //                             where (row.Email == Email || row.Email == EmailIOS)
        //                               && row.IsActive == true
        //                               && row.IsDeleted == false
        //                             select row).FirstOrDefault();

        //                if (user != null)
        //                {
        //                    int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

        //                    lstCaseDocs = (from row in entities.Sp_Litigation_CaseDocument(objDoc.CaseInstanceID, customerId)
        //                                   select row).ToList();

        //                    string CurrentYear = Convert.ToString(DateTime.Now.Year) + "-" + Convert.ToInt32(DateTime.Now.Year + 1);

        //                    if (objDoc.Flag == "Current")
        //                    {
        //                        lstCaseDocs = lstCaseDocs.Where(x => x.FinancialYear.Contains(CurrentYear)).ToList();
        //                    }
        //                    else
        //                    {
        //                        var CaseDocs = lstCaseDocs.Where(x => x.FinancialYear.Contains(CurrentYear)).ToList();

        //                        lstCaseDocs = lstCaseDocs.Except(CaseDocs).ToList();
        //                    }
        //                }
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(lstCaseDocs).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        //[Authorize]

      
        [Route("Litigation/GetAllNoticeDocuments")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllNoticeDocuments([FromBody] GetAllNoticeDocuments objDoc)
        {
            try
            {
                string Email = objDoc.Email.ToString();
                List<NoticeDocument> lstCaseDocs = new List<NoticeDocument>();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            var lstCaseDocs1 = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerId)
                                           select row).ToList();
                            if (objDoc.NoticeInstanceID != 0)
                            {
                                lstCaseDocs1 = lstCaseDocs1.Where(x => x.NoticeCaseInstanceID == objDoc.NoticeInstanceID).ToList();
                            }
                            //lstCaseDocs1 = lstCaseDocs1.Where(x => x.AssignedUser == user.ID).ToList();
                            if (lstCaseDocs1.Count > 0)
                            {
                                lstCaseDocs = (from g in lstCaseDocs1
                                               group g by new
                                               {
                                                   g.NoticeCaseInstanceID,
                                                   g.TypeCaseNotice,
                                                   g.DocumentName,
                                                   g.FileID,
                                                   g.TypeName,
                                                   g.RefNo,
                                                   g.Title,
                                                   g.Description,
                                                   g.BranchName,
                                                   g.DeptName,
                                                   g.OwnerID,
                                                   g.Owner,
                                                   //g.AssignedUser,
                                                   //g.AssignedUserRoleID,
                                                   //g.AssignedUserName,
                                                   g.CustomerID,
                                                   g.DepartmentID,
                                                   g.CustomerBranchID,
                                                   g.Status,
                                                   g.IsDeleted,
                                                   g.Filetag,
                                                   g.FYName,
                                                   g.DocumentCount,
                                                   g.PartyID,
                                                   g.PartyName,
                                                   g.Createdate
                                               } into GCS
                                               select new NoticeDocument
                                               {
                                                   NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                                   TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                                   DocumentName = GCS.Key.DocumentName,
                                                   FileID = GCS.Key.FileID,
                                                   TypeName = GCS.Key.TypeName,
                                                   RefNo = GCS.Key.RefNo,
                                                   Title = GCS.Key.Title,
                                                   Description = GCS.Key.Description,
                                                   BranchName = GCS.Key.BranchName,
                                                   DeptName = GCS.Key.DeptName,
                                                   OwnerID = GCS.Key.OwnerID,
                                                   Owner = GCS.Key.Owner,
                                                   //AssignedUser = GCS.Key.AssignedUser,
                                                   //AssignedUserRoleID = GCS.Key.AssignedUserRoleID,
                                                   //AssignedUserName = GCS.Key.AssignedUserName,
                                                   CustomerID = GCS.Key.CustomerID,
                                                   DepartmentID = GCS.Key.DepartmentID,
                                                   CustomerBranchID = GCS.Key.CustomerBranchID,
                                                   Status = GCS.Key.Status,
                                                   IsDeleted = GCS.Key.IsDeleted,
                                                   Filetag = GCS.Key.Filetag,
                                                   FYName = GCS.Key.FYName,
                                                   DocumentCount = GCS.Key.DocumentCount,
                                                   PartyID = GCS.Key.PartyID,
                                                   PartyName = GCS.Key.PartyName,
                                                   Createdate = GCS.Key.Createdate
                                               }).ToList();
                            }
                            if (lstCaseDocs.Count > 0)
                            {
                                if (objDoc.Flag == "Current")
                                {
                                    lstCaseDocs = lstCaseDocs.OrderByDescending(x => x.Createdate).Take(1).ToList();                                    
                                }
                                else
                                {
                                    var RemoveCusrrentFileId = lstCaseDocs.OrderByDescending(x => x.Createdate).Take(1).FirstOrDefault();
                                    lstCaseDocs.Remove(RemoveCusrrentFileId);
                                }
                            }
                            //if (lstCaseDocs1.Count > 0)
                            //{
                            //    lstCaseDocs = (from g in lstCaseDocs1
                            //                   group g by new
                            //                   {
                            //                       g.NoticeCaseInstanceID,
                            //                       g.TypeCaseNotice,
                            //                       g.DocumentName,
                            //                       g.FileID,
                            //                       g.TypeName,
                            //                       g.RefNo,
                            //                       g.Title,
                            //                       g.Description,
                            //                       g.BranchName,
                            //                       g.DeptName,
                            //                       g.OwnerID,
                            //                       g.Owner,
                            //                       g.AssignedUser,
                            //                       g.AssignedUserRoleID,
                            //                       g.AssignedUserName,
                            //                       g.CustomerID,
                            //                       g.DepartmentID,
                            //                       g.CustomerBranchID,
                            //                       g.Status,
                            //                       g.IsDeleted,
                            //                       g.Filetag,
                            //                       g.FYName,
                            //                       g.DocumentCount,
                            //                       g.PartyID,
                            //                       g.PartyName
                            //                   } into GCS
                            //                   select new NoticeDocument
                            //                   {
                            //                       NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                            //                       TypeCaseNotice = GCS.Key.TypeCaseNotice,
                            //                       DocumentName = GCS.Key.DocumentName,
                            //                       FileID = GCS.Key.FileID,
                            //                       TypeName = GCS.Key.TypeName,
                            //                       RefNo = GCS.Key.RefNo,
                            //                       Title = GCS.Key.Title,
                            //                       Description = GCS.Key.Description,
                            //                       BranchName = GCS.Key.BranchName,
                            //                       DeptName = GCS.Key.DeptName,
                            //                       OwnerID = GCS.Key.OwnerID,
                            //                       Owner = GCS.Key.Owner,
                            //                       AssignedUser = GCS.Key.AssignedUser,
                            //                       AssignedUserRoleID = GCS.Key.AssignedUserRoleID,
                            //                       AssignedUserName = GCS.Key.AssignedUserName,
                            //                       CustomerID = GCS.Key.CustomerID,
                            //                       DepartmentID = GCS.Key.DepartmentID,
                            //                       CustomerBranchID = GCS.Key.CustomerBranchID,
                            //                       Status = GCS.Key.Status,
                            //                       IsDeleted = GCS.Key.IsDeleted,
                            //                       Filetag = GCS.Key.Filetag,
                            //                       FYName = GCS.Key.FYName,
                            //                       DocumentCount = GCS.Key.DocumentCount,
                            //                       PartyID = GCS.Key.PartyID,
                            //                       PartyName = GCS.Key.PartyName
                            //                   }).ToList();
                            //}
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstCaseDocs).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        public static bool UploadFileHearing(HttpRequest CurrentRequest, int NoticeCaseInstanceID, int TaskID, int UserID, int customerID, string User)
        {
            var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
            if (AWSData != null)
            {
                #region AWS
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        if (TaskID > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                            for (int i = 0; i < CurrentRequest.Files.Count; i++)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = NoticeCaseInstanceID,
                                    DocTypeInstanceID = TaskID,
                                    CreatedBy = UserID,
                                    CreatedByText = User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "CH"
                                };

                                //string directoryPath = "";
                                string fileName = "";
                                HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');

                                    fileName = uploadedFile.FileName;

                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                    caseTaskDocVersion++;
                                    objTaskDoc.Version = caseTaskDocVersion + ".0";

                                    string FileLocationPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(NoticeCaseInstanceID) + "\\Hearing\\" + objTaskDoc.Version;

                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, FileLocationPath);
                                    if (!di.Exists)
                                    {
                                        di.Create();
                                    }
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    string storagedrive = ConfigurationManager.AppSettings["UploaddestinationPath"];

                                    string p_strPath = string.Empty;
                                    string dirpath = string.Empty;
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    p_strPath = @"" + storagedrive + "/" + UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                    dirpath = @"" + storagedrive + "/" + UserID + "/" + FileDate;
                                    
                                    if (!Directory.Exists(dirpath))
                                    {
                                        Directory.CreateDirectory(dirpath);
                                    }
                                    FileStream objFileStrm = File.Create(p_strPath);
                                    objFileStrm.Close();
                                    File.WriteAllBytes(p_strPath, bytes);

                                    Guid fileKey1 = Guid.NewGuid();

                                    string AWSpath = FileLocationPath + "\\" + uploadedFile.FileName;
                                    
                                    objTaskDoc.FileName = fileName;
                                    objTaskDoc.FilePath = FileLocationPath.Replace(@"\", "/");
                                    objTaskDoc.FileKey = fileKey1.ToString();
                                    objTaskDoc.VersionDate = DateTime.Now;
                                    objTaskDoc.CreatedOn = DateTime.Now;
                                    objTaskDoc.FileSize = uploadedFile.ContentLength;

                                    FileInfo localFile = new FileInfo(p_strPath);
                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                    if (!s3File.Exists)
                                    {
                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                        {
                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                        }
                                    }
                                    //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);
                                    Filelist1.Clear();
                                }
                            }//End For Each      
                            if (saveSuccess)
                            {
                                saveSuccess = true;
                                LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
            else
            {
                #region Normal
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        if (TaskID > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                            for (int i = 0; i < CurrentRequest.Files.Count; i++)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = NoticeCaseInstanceID,
                                    DocTypeInstanceID = TaskID,
                                    CreatedBy = UserID,
                                    CreatedByText = User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "CH"
                                };

                                string directoryPath = "";
                                string fileName = "";
                                HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');

                                    fileName = uploadedFile.FileName;

                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                    caseTaskDocVersion++;
                                    objTaskDoc.Version = caseTaskDocVersion + ".0";

                                    string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                    string FileLocationPath = "/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(NoticeCaseInstanceID) + "/Hearing/" + objTaskDoc.Version;

                                    string FileLocationPathforconvert = (FileLocationPath).ToString();
                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                    FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                    directoryPath = destinationPath + FileLocationPathforconvert;
                                    Directory.CreateDirectory(directoryPath);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objTaskDoc.FileName = fileName;
                                    objTaskDoc.FilePath = "~" + FileLocationPath; //directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objTaskDoc.FileKey = fileKey1.ToString();
                                    objTaskDoc.VersionDate = DateTime.Now;
                                    objTaskDoc.CreatedOn = DateTime.Now;
                                    objTaskDoc.FileSize = uploadedFile.ContentLength;

                                    DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);
                                    Filelist1.Clear();
                                }
                            }//End For Each      
                            if (saveSuccess)
                            {
                                saveSuccess = true;
                                LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
        }
        
        public static bool UploadFileCase(HttpRequest CurrentRequest, int NoticeCaseInstanceID,int UserID, int customerID, string User,string Flag)
        {
            var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
            if (AWSData != null)
            {
                #region AWS
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                        for (int i = 0; i < CurrentRequest.Files.Count; i++)
                        {
                            tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = NoticeCaseInstanceID,
                                //DocTypeInstanceID = TaskID,
                                CreatedBy = UserID,
                                CreatedByText = User,
                                IsDeleted = false,
                                DocTypeID = DocTypeID,
                                DocType = Flag
                            };

                            //string directoryPath = "";
                            string fileName = "";
                            HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                            if (uploadedFile.ContentLength > 0)
                            {
                                string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');

                                fileName = uploadedFile.FileName;

                                var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objCaseDoc);

                                caseDocVersion++;
                                objCaseDoc.Version = caseDocVersion + ".0";

                                //string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                string FileLocationPath = string.Empty;
                                if (Flag.Equals("C"))
                                {
                                    FileLocationPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(NoticeCaseInstanceID) + "\\CaseDocument\\" + objCaseDoc.Version;
                                    //FileLocationPath = "/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(NoticeCaseInstanceID) + "/CaseDocument/" + objCaseDoc.Version;
                                }
                                else if (Flag.Equals("N"))
                                {
                                    FileLocationPath = "LitigationDocuments\\" + customerID + "\\Notice\\" + Convert.ToInt32(NoticeCaseInstanceID) + "\\NoticeDocument\\" + objCaseDoc.Version;
                                    //FileLocationPath = "/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(NoticeCaseInstanceID) + "/NoticeDocument/" + objCaseDoc.Version;
                                }
                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, FileLocationPath);
                                if (!di.Exists)
                                {
                                    di.Create();
                                }
                                Stream fs = uploadedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                string storagedrive = ConfigurationManager.AppSettings["UploaddestinationPath"];

                                string p_strPath = string.Empty;
                                string dirpath = string.Empty;                                
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                p_strPath = @"" + storagedrive + "/" + UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                dirpath = @"" + storagedrive + "/" + UserID + "/" + FileDate;                                
                                if (!Directory.Exists(dirpath))
                                {
                                    Directory.CreateDirectory(dirpath);
                                }
                                FileStream objFileStrm = File.Create(p_strPath);
                                objFileStrm.Close();
                                File.WriteAllBytes(p_strPath, bytes);

                                Guid fileKey1 = Guid.NewGuid();
                                string AWSpath = FileLocationPath + "\\" + uploadedFile.FileName;
                                
                                objCaseDoc.FileName = fileName;
                                objCaseDoc.FilePath = FileLocationPath.Replace(@"\", "/");
                                objCaseDoc.FileKey = fileKey1.ToString();
                                objCaseDoc.VersionDate = DateTime.Now;
                                objCaseDoc.CreatedOn = DateTime.Now;
                                objCaseDoc.FileSize = uploadedFile.ContentLength;

                                FileInfo localFile = new FileInfo(p_strPath);
                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                if (!s3File.Exists)
                                {
                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                    {
                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                    }
                                }
                                //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);
                                Filelist1.Clear();
                            }
                        }//End For Each      
                        if (saveSuccess)
                        {
                            saveSuccess = true;
                            LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
            else
            {
                #region Normal
                bool saveSuccess = true;
                if (CurrentRequest.Files.Count > 0)
                {
                    try
                    {
                        int DocTypeID = -1;

                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                        for (int i = 0; i < CurrentRequest.Files.Count; i++)
                        {
                            tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = NoticeCaseInstanceID,
                                //DocTypeInstanceID = TaskID,
                                CreatedBy = UserID,
                                CreatedByText = User,
                                IsDeleted = false,
                                DocTypeID = DocTypeID,
                                DocType = Flag
                            };

                            string directoryPath = "";
                            string fileName = "";
                            HttpPostedFile uploadedFile = CurrentRequest.Files[i];

                            if (uploadedFile.ContentLength > 0)
                            {
                                string[] keys1 = CurrentRequest.Files.Keys[i].Split('$');

                                fileName = uploadedFile.FileName;

                                var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objCaseDoc);

                                caseDocVersion++;
                                objCaseDoc.Version = caseDocVersion + ".0";

                                string destinationPath = ConfigurationManager.AppSettings["UploaddestinationPath"].ToString();
                                string FileLocationPath = string.Empty;
                                if (Flag.Equals("C"))
                                {
                                    FileLocationPath = "/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(NoticeCaseInstanceID) + "/CaseDocument/" + objCaseDoc.Version;
                                }
                                else if (Flag.Equals("N"))
                                {
                                    FileLocationPath = "/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(NoticeCaseInstanceID) + "/NoticeDocument/" + objCaseDoc.Version;
                                }
                                string FileLocationPathforconvert = (FileLocationPath).ToString();
                                FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"~", string.Empty).Trim();
                                FileLocationPathforconvert = FileLocationPathforconvert.Replace(@"/", @"\");
                                directoryPath = destinationPath + FileLocationPathforconvert;
                                Directory.CreateDirectory(directoryPath);

                                if (!Directory.Exists(directoryPath))
                                    Directory.CreateDirectory(directoryPath);

                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                Stream fs = uploadedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                objCaseDoc.FileName = fileName;
                                objCaseDoc.FilePath = "~" + FileLocationPath; //directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                objCaseDoc.FileKey = fileKey1.ToString();
                                objCaseDoc.VersionDate = DateTime.Now;
                                objCaseDoc.CreatedOn = DateTime.Now;
                                objCaseDoc.FileSize = uploadedFile.ContentLength;

                                DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);
                                Filelist1.Clear();
                            }
                        }//End For Each      
                        if (saveSuccess)
                        {
                            saveSuccess = true;
                            LitigationManagement.CreateAuditLog("T", Convert.ToInt32(NoticeCaseInstanceID), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(customerID), UserID, "Task Document(s) Uploaded.", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        saveSuccess = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return saveSuccess;
                #endregion
            }
        }

     
        [Route("Litigation/AddCaseNoticeDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddCaseNoticeDocument(string EmailID, int CaseNoticeInstanceID,string Flag)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

            try
            {

                bool Successresult = false;

                string Email = EmailID.ToString();
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            string UserName = user.FirstName + " " + user.LastName;
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            if (Flag.Equals("C"))
                            {
                                LitigationManagement.CreateAuditLog("C", CaseNoticeInstanceID, "tbl_LitigationFileData", "Add", customerId, Convert.ToInt32(user.ID), "Add Document in Case", true);
                            }
                            else if (Flag.Equals("N"))
                            {
                                LitigationManagement.CreateAuditLog("N", CaseNoticeInstanceID, "tbl_LitigationFileData", "Add", customerId, Convert.ToInt32(user.ID), "Add Document in Notice", true);
                            }
                            var httpRequest = HttpContext.Current.Request;
                            Successresult = UploadFileCase(httpRequest, Convert.ToInt32(CaseNoticeInstanceID), Convert.ToInt32(user.ID), Convert.ToInt32(user.CustomerID), UserName, Flag);
                        }
                    }
                    if (Successresult == true)
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    else
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("Litigation/UserList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UserList(int CutomerID, bool Flags)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Users
                                 where row.IsDeleted == false && row.IsActive == true
                                 && row.RoleID != 19
                                 select row);

                    if (CutomerID != -1)
                    {
                        query = query.Where(entry => entry.CustomerID == CutomerID);
                    }

                    if (Flags == true)
                    {
                        query = query.Where(entry => entry.RoleID != 8);
                    }


                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();


                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/GetAssignedEntitiesLocationList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAssignedEntitiesLocationList(int customerId, int UserID, string FlagIsApp)
        {
            try
            {
                List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int IsForBranch = UserManagement.ExistBranchAssignment(customerId);
                    if (IsForBranch == 0)
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedEntitiesLocationListLitigationRahulbyuser(customerId, UserID, FlagIsApp);
                        var branches = BranchClass.GetAllAssignedHierarchySatutory(customerId, LocationList, UserID);
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = branches
                        });
                    }
                    else
                    {
                        var LocationList = CustomerBranchManagement.GetAssignedEntitiesLocationListLitigationRahulbyuserNew(customerId, UserID, FlagIsApp);
                        List<NameValueHierarchy> obj = new List<NameValueHierarchy>();
                        List<NameValueHierarchy> obj1 = new List<NameValueHierarchy>();

                        var BranchList = (from row in entities.CustomerBranches
                                          where LocationList.Contains(row.ID)
                                          && row.IsDeleted == false
                                          select row).ToList();

                        foreach (var item in BranchList)
                        {
                            obj.Add(new NameValueHierarchy { RparentID = null, ID = item.ID, Children = obj1.ToList(), Level = 0, Name = item.Name });
                        }
                        CFODashboardGraphObj.Add(new CFODashboardGraph
                        {
                            locationList = obj
                        });
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Litigation/AdvocateBill")]
        [System.Web.Http.AcceptVerbs("Get", "Post")]
        public HttpResponseMessage AdvocateBill(int CustId, string EmailID, int UserId)
        {
            try
            {
                List<SP_LitigationCaseAdvocateBill_Result> objCase = new List<SP_LitigationCaseAdvocateBill_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var lstApproverType = (from row in entities.tbl_Advcatebillapprovers
                                           where row.IsDeleted == false
                                           && row.ApproverEmail.Trim().ToLower() == EmailID.Trim().ToLower()
                                           && row.CustomerID == CustId
                                           select row.ApproverLevel).FirstOrDefault();

                    objCase = (from row in entities.SP_LitigationCaseAdvocateBill(CustId)
                               select row).ToList();

                    if (lstApproverType != null)
                    {
                        if (lstApproverType.Trim() == "Approver 1")
                        {
                            objCase = objCase.Where(x => !string.IsNullOrEmpty(x.Approver1Email)).ToList();
                            if (objCase.Count > 0)
                            {
                                objCase = objCase.Where(x => x.Approver1Email.Trim().ToLower() == EmailID.Trim().ToLower() || x.CreatedByID == UserId).ToList();
                            }
                        }

                        if (lstApproverType.Trim() == "Approver 2")
                        {
                            if (objCase.Count > 0)
                            {
                                objCase = objCase.Where(x => x.Approver2Email.Trim().ToLower() == EmailID.Trim().ToLower() || x.CreatedByID == UserId).ToList();
                            }
                        }

                        if (lstApproverType.Trim() == "Approver 3")
                        {
                            if (objCase.Count > 0)
                            {
                                objCase = objCase.Where(x => x.Approver3Email.Trim().ToLower() == EmailID.Trim().ToLower() || x.CreatedByID == UserId).ToList();
                            }
                        }
                    }
                    else
                    {
                        objCase = objCase.Where(x => x.CreatedByID == UserId).ToList();
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objCase).ToString(), Encoding.UTF8, "application/json")
                };

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Authorize]
        //[Route("Litigation/AdvocateBill")]
        //[System.Web.Http.AcceptVerbs("Get", "Post")]
        //public HttpResponseMessage AdvocateBill(int CustId, string EmailID, int UserId)
        //{
        //    try
        //    {
        //        List<SP_LitigationCaseAdvocateBill_Result> objCase = new List<SP_LitigationCaseAdvocateBill_Result>();

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            var lstApproverType = (from row in entities.tbl_Advcatebillapprovers
        //                                   where row.IsDeleted == false
        //                                   && row.ApproverEmail.Trim().ToLower() == EmailID.Trim().ToLower()
        //                                   && row.CustomerID == CustId
        //                                   select row.ApproverLevel).FirstOrDefault();

        //            objCase = (from row in entities.SP_LitigationCaseAdvocateBill(CustId)
        //                       select row).ToList();

        //            if (lstApproverType != null)
        //            {
        //                if (lstApproverType.Trim() == "Approver 1")
        //                {
        //                    objCase = objCase.Where(x => !string.IsNullOrEmpty(x.Approver1Email)).ToList();
        //                    if (objCase.Count > 0)
        //                    {
        //                        objCase = objCase.Where(x => x.Approver1Email.Trim().ToLower() == EmailID.Trim().ToLower()).ToList();
        //                    }
        //                }

        //                if (lstApproverType.Trim() == "Approver 2")
        //                {
        //                    objCase = objCase.Where(x => !string.IsNullOrEmpty(x.Approver2Email) && x.Status != "Rejected").ToList();
        //                    if (objCase.Count > 0)
        //                    {
        //                        objCase = objCase.Where(x => x.Approver2Email.Trim().ToLower() == EmailID.Trim().ToLower()).ToList();
        //                    }
        //                }

        //                if (lstApproverType.Trim() == "Approver 3")
        //                {
        //                    objCase = objCase.Where(x => !string.IsNullOrEmpty(x.Approver3Email) && x.Status != "Rejected").ToList();
        //                    if (objCase.Count > 0)
        //                    {
        //                        objCase = objCase.Where(x => x.Approver3Email.Trim().ToLower() == EmailID.Trim().ToLower()).ToList();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                objCase = objCase.Where(x => x.CreatedByID == UserId).ToList();
        //            }
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objCase).ToString(), Encoding.UTF8, "application/json")
        //        };

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}



        [Route("Litigation/Add_RPACaseStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Add_RPACaseStatus(string CourtType, string State, string StateName, string CaseTypeID, string CaseType, string CaseNo, string CaseYear, string CourtName, string Bench, string BenchID, string District, string CreatedBy, string RPAStatus, int UserID, int CustomerID)
        {
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (!string.IsNullOrEmpty(CourtType))
                {
                    var caseyear = Convert.ToInt32(CaseYear);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(CustomerID))
                                     where row.CaseRefNo == CaseNo
                                     && row.CourtName == CourtType
                                     && Convert.ToDateTime(row.OpenDate).Year == caseyear
                                     select row).ToList();

                        var query1 = (from row in entities.tbl_RPACaseStatus
                                      where row.CaseNo == CaseNo
                                      && row.CaseYear == CaseYear
                                      && row.CustomerID == CustomerID
                                      && row.IsDeleted == false
                                      select row).ToList();

                        if (query.Count == 0 && query1.Count == 0)
                        {

                            bool result = LitigationManagement.AddRPACaseStatus(CourtType, State, StateName, CaseTypeID, CaseType, CaseNo, CaseYear, CourtName, Bench, BenchID, District, CreatedBy, RPAStatus, UserID, CustomerID);
                            if (result)
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }
                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        [Route("Litigation/KendoRPAStatusList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoRPAStatusList(int CustId)
        {
            try
            {
                List<int> objRPA = new List<int>();
                List<tbl_RPACaseStatus> objR = new List<tbl_RPACaseStatus>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var objLCInstance = (from row in entities.tbl_LegalCaseInstance
                                         where row.IsDeleted == false
                                         && row.CustomerID == CustId
                                         select row).ToList();

                    foreach (var items in objLCInstance)
                    {
                        var obj12 = (from row in entities.tbl_RPACaseStatus
                                     where row.IsDeleted == false
                                     && row.CustomerID == CustId
                                     && row.CaseNo == items.CaseRefNo
                                     && row.CaseYear == items.CaseYear
                                     select row.ID).FirstOrDefault();
                        if (obj12 != 0)
                        {
                            objRPA.Add(Convert.ToInt32(obj12));
                        }
                    }
                    objR = (from row in entities.tbl_RPACaseStatus
                            where row.IsDeleted == false
                            && row.CustomerID == CustId
                            select row).ToList();

                    if (objRPA.Count > 0)
                    {
                        var rejectStatus = objRPA;
                        var fullList = objR;
                        var rejectList = objR.Where(i => rejectStatus.Contains(i.ID));

                        foreach (var items in rejectList)
                        {
                            items.IsDeleted = true;
                            entities.SaveChanges();
                        }
                        objR = objR.Except(rejectList).ToList();
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(objR).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        [Route("Litigation/RPAStatus")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage KendoRPAStatusList(string CaseNo, string CaseYear, string RPAStatus)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<tbl_RPACaseStatus> obj = new List<tbl_RPACaseStatus>();
                    string Result = "";
                    if (!string.IsNullOrEmpty(CaseNo) && !string.IsNullOrEmpty(CaseYear) && !string.IsNullOrEmpty(RPAStatus))
                    {
                        var obj1 = (from row in entities.tbl_RPACaseStatus
                                    where row.IsDeleted == false
                                    && row.CaseNo.Trim().ToLower() == CaseNo.Trim().ToLower()
                                    && row.CaseYear.Trim().ToLower() == CaseYear.Trim().ToLower()
                                    select row).FirstOrDefault();
                        if (obj1 != null)
                        {
                            obj1.RPAStatus = RPAStatus;
                            entities.SaveChanges();
                            Result = "Successfully Added";
                        }
                        string jsonstring = JsonConvert.SerializeObject(Result);
                        return new HttpResponseMessage()
                        {
                            Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                        };
                    }
                    else
                    {
                        obj = (from row in entities.tbl_RPACaseStatus
                               where row.IsDeleted == false
                               select row).ToList();

                        return new HttpResponseMessage()
                        {
                            Content = new StringContent(JArray.FromObject(obj).ToString(), Encoding.UTF8, "application/json")
                        };
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("Litigation/MailAuthorizationUserList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MailAuthorizationUserList(int CustId, string UserType, string Role)
        {
            try
            {
                List<SP_LitiUserDetails_Result> viewUserDetails = new List<SP_LitiUserDetails_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    viewUserDetails = (from row in entities.SP_LitiUserDetails(CustId)
                                       where row.UserType != null
                                       select row).ToList();
                    if (!string.IsNullOrEmpty(UserType))
                    {
                        viewUserDetails = viewUserDetails.Where(x => x.UserType.ToLower() == UserType.ToLower()).ToList();
                    }
                    if (!string.IsNullOrEmpty(Role))
                    {
                        viewUserDetails = viewUserDetails.Where(x => x.LitigationRole == Role).ToList();
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(viewUserDetails).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }



        //[Authorize]
        //[Route("Litigation/GetAssignedEntitiesLocationList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAssignedEntitiesLocationList(int customerId, int UserID, string FlagIsApp)
        //{
        //    try
        //    {
        //        List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            var LocationList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerId, UserID, FlagIsApp);
        //            var branches = BranchClass.GetAllAssignedHierarchySatutory(customerId, LocationList, UserID);
        //            CFODashboardGraphObj.Add(new CFODashboardGraph
        //            {
        //                locationList = branches
        //            });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        //[Authorize]
        //[Route("Litigation/GetAssignedEntitiesLocationList")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetAssignedEntitiesLocationList(int customerId, int UserID)///, int userId, string Flag, string IsStatutoryInternal
        //{
        //    try
        //    {
        //        List<CFODashboardGraph> CFODashboardGraphObj = new List<CFODashboardGraph>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var LocationList = CustomerBranchManagement.GetAssignedEntitiesLocationListLitigationRahulbyuser(customerId, UserID);
        //            var branches = BranchClass.GetAllAssignedHierarchySatutory(customerId, LocationList, UserID);
        //            CFODashboardGraphObj.Add(new CFODashboardGraph
        //            {
        //                locationList = branches
        //            });
        //        }
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(CFODashboardGraphObj).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


       
    }
}
