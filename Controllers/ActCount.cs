﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Controllers
{
    public class ActCount
    {
        public string CentralHR { get; set; }
        public string CentralFinance { get; set; }
        public string CentralCommercial { get; set; }
        public string CentralCorporate { get; set; }
        public string CentralCustoms { get; set; }
        public string CentralEHS { get; set; }
        public string CentralLocal { get; set; }
        public string CentralRegulatory { get; set; }
        public string CentralInternalCompliance { get; set; }
        public string CentralFEMA { get; set; }
        public string CentralSecretarial { get; set; }
        public string CentralClientSpecific { get; set; }

        public string stateHR { get; set; }
        public string stateFinance { get; set; }
        public string stateCommercial { get; set; }
        public string stateCorporate { get; set; }
        public string stateCustoms { get; set; }
        public string stateEHS { get; set; }
        public string stateLocal { get; set; }
        public string stateRegulatory { get; set; }
        public string stateInternalCompliance { get; set; }
        public string stateFEMA { get; set; }
        public string stateSecretarial { get; set; }
        public string stateClientSpecific { get; set; }
    }
    public class CategorywiseCount
    {
        public string CategoryName { get; set; }
        public int CategoryCount { get; set; }
    }
}