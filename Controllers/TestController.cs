﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using Microsoft.Owin.Security;
using System.Configuration;
using Newtonsoft.Json;
using System.Threading.Tasks;
using AppWebApplication.Models;
using AppWebApplication.Data;

namespace AppWebApplication.Controllers
{
   
    public class TestController : ApiController
    {

      //  [Authorize]
    //    [AllowAnonymous]
       // [HttpGet]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [Route("Test/Postlogin")]
        public HttpResponseMessage Postlogin()
        {
            try
            {
                string chk = null;
                //string baseAddress = "http://avacompcdr.cloudapp.net/androidappservicedata";
                string baseAddress = "http://localhost:51876/";
            
                using (var client = new HttpClient())
                {
                    var form = new Dictionary<string, string>    
                {    
                    {"grant_type", "password"},    
                    {"username", "user"},    
                    {"password", "user"},    
                };
                    var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;

                    var token = tokenResponse.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                    if (string.IsNullOrEmpty(token.Error))
                    {
                        //Console.WriteLine("Token issued is: {0}", token.AccessToken);
                        chk = token.AccessToken;
                    }
                    else
                    {
                        chk = token.AccessToken;
                        //Console.WriteLine("Error : {0}", token.Error);  
                    }
                    //Console.Read();  
                }

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = "right", ErrorDetailMessage = chk });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }
        
        [Authorize]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        //[HttpGet]
        [Route("Test/GetNewsletterData")]
        public HttpResponseMessage GetNewsletterData(int page)
        {
            //int page = 1;

            List<NewsLetter> data = new List<NewsLetter>();

            string chek = "http://prod-avacompc.cloudapp.net/newsletter/";

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                data = (from row in entities.NewsLetters
                                  select row).ToList();

                foreach (var item in data)
                {
                   // string getFilename = chek + item.FileName;
                    item.FilePath = chek + item.FileName;
                    item.DocFilePath = chek + item.DocFileName;
                }
            }
            if (data.Count > 0)
            {
                var total = data.Count;
                var pageSize = 10;              
                var skip = pageSize * (page - 1);
                var canPage = skip < total;

                data = data.Skip(skip).Take(pageSize).ToList();
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
            };        
        }
                       
      //  [Authorize]
        [HttpGet]
        [Route("Test/GetDailyupdateData")]
        public HttpResponseMessage GetDailyupdateData(int page)
        {
          
            List<DailyUpdate> data = new List<DailyUpdate>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                data = (from row in entities.DailyUpdates
                        select row).ToList();
            }
            if (data.Count > 0)
            {
                var total = data.Count;
                var pageSize = 10;              
                var skip = pageSize * (page - 1);
                var canPage = skip < total;

                data = data.Skip(skip).Take(pageSize).ToList();
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
            };
        }
    }
}
