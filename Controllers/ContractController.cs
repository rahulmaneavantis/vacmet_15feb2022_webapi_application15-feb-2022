﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;
using AppWebApplication.Data;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using Newtonsoft.Json.Linq;

namespace AppWebApplication.Controllers
{
    public class ContractController : ApiController
    {
        public static string key = "avantis";

        [Authorize]
        [Route("Contract/ContractTypeCount")]//for Dashboard Count
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTypeCount([FromBody] mydashboardclassContract mydashboardobj)
        {
            string Email = mydashboardobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<ContractTypecount> result = new List<ContractTypecount>();

                List<Sp_Cont_View_ContractDetails_Result> lstContracts = new List<Sp_Cont_View_ContractDetails_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int RoleId = -1;
                            if (user.ContractRoleID != null)
                            {
                                RoleId = Convert.ToInt32(user.ContractRoleID);
                            }
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            if (RoleCheck != "MGMT" && RoleCheck != "CADMN")
                            {
                                int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                                List<Cont_SP_GetAssignedContracts_All_Result> lstContracts1 = (from c in entities.Cont_SP_GetAssignedContracts_All(customerID)
                                                                                               select c).ToList();
                                int RID = 3;
                                lstContracts1 = lstContracts1.Where(entry => (entry.UserID == user.ID && entry.RoleID == RID) || (entry.CreatedBy == user.ID)).ToList();

                                if (lstContracts1.Count > 0)
                                {
                                    lstContracts1 = (from g in lstContracts1
                                                     group g by new
                                                     {
                                                         g.ID, //ContractID
                                                         g.CustomerID,
                                                         g.ContractNo,
                                                         g.ContractTitle,
                                                         g.VendorIDs,
                                                         g.VendorNames,
                                                         g.CustomerBranchID,
                                                         g.BranchName,
                                                         g.DepartmentID,
                                                         g.DeptName,
                                                         g.ContractTypeID,
                                                         g.TypeName,
                                                         g.ContractSubTypeID,
                                                         g.SubTypeName,
                                                         g.EffectiveDate,
                                                         g.ExpirationDate,
                                                         g.CreatedOn,
                                                         g.UpdatedOn,
                                                         g.StatusName
                                                     } into GCS
                                                     select new Cont_SP_GetAssignedContracts_All_Result()
                                                     {
                                                         ID = GCS.Key.ID, //ContractID
                                                         CustomerID = GCS.Key.CustomerID,
                                                         ContractNo = GCS.Key.ContractNo,
                                                         ContractTitle = GCS.Key.ContractTitle,
                                                         VendorIDs = GCS.Key.VendorIDs,
                                                         VendorNames = GCS.Key.VendorNames,
                                                         CustomerBranchID = GCS.Key.CustomerBranchID,
                                                         BranchName = GCS.Key.BranchName,
                                                         DepartmentID = GCS.Key.DepartmentID,
                                                         DeptName = GCS.Key.DeptName,
                                                         ContractTypeID = GCS.Key.ContractTypeID,
                                                         TypeName = GCS.Key.TypeName,
                                                         ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                                         SubTypeName = GCS.Key.SubTypeName,
                                                         EffectiveDate = GCS.Key.EffectiveDate,
                                                         ExpirationDate = GCS.Key.ExpirationDate,
                                                         CreatedOn = GCS.Key.CreatedOn,
                                                         UpdatedOn = GCS.Key.UpdatedOn,
                                                         StatusName = GCS.Key.StatusName
                                                     }).ToList();
                                }

                                if (lstContracts1.Count > 0)
                                {
                                    lstContracts1 = lstContracts1.OrderBy(entry => entry.ExpirationDate).ToList();
                                }
                                
                                string DraftCount = lstContracts1.Where(row => row.StatusName == "Draft").Count().ToString();
                                string PendingReviewCount = lstContracts1.Where(row => row.StatusName == "Pending Review").Count().ToString();
                                string ReviewedCount = lstContracts1.Where(row => row.StatusName == "Review Completed").Count().ToString();
                                string PendingApprovalCount = lstContracts1.Where(row => row.StatusName == "Pending Approval").Count().ToString();
                                string Approvedcount = lstContracts1.Where(row => row.StatusName == "Approval Completed").Count().ToString();
                                string ActiveCount = lstContracts1.Where(row => row.StatusName == "Active").Count().ToString();
                                string ExpiredCount = lstContracts1.Where(row => row.StatusName == "Expired").Count().ToString();
                                string ReviwesCount = ContractTaskManagement.GetCountMyReviews(customerID, Convert.ToInt32(user.ID), RoleCheck).ToString();

                                result = (from row in DraftCount
                                          select new ContractTypecount()
                                          {
                                              DraftCount = Convert.ToInt32(DraftCount),
                                              PendingReviewCount = Convert.ToInt32(PendingReviewCount),
                                              ReviewedCount = Convert.ToInt32(ReviewedCount),
                                              PendingApprovalCount = Convert.ToInt32(PendingApprovalCount),
                                              ApprovedCount = Convert.ToInt32(Approvedcount),
                                              ActiveCount = Convert.ToInt32(ActiveCount),
                                              ExpiredCount = Convert.ToInt32(ExpiredCount),
                                              ReviewesCount = Convert.ToInt32(ReviwesCount)
                                          }).ToList();
                            }
                            else
                            {
                                int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                                lstContracts = (from c in entities.Sp_Cont_View_ContractDetails(customerID)
                                                select c).ToList();


                                string DraftCount = lstContracts.Where(row => row.StatusName == "Draft").Count().ToString();
                                string PendingReviewCount = lstContracts.Where(row => row.StatusName == "Pending Review").Count().ToString();
                                string ReviewedCount = lstContracts.Where(row => row.StatusName == "Review Completed").Count().ToString();
                                string PendingApprovalCount = lstContracts.Where(row => row.StatusName == "Pending Approval").Count().ToString();
                                string Approvedcount = lstContracts.Where(row => row.StatusName == "Approval Completed").Count().ToString();
                                string ActiveCount = lstContracts.Where(row => row.StatusName == "Active").Count().ToString();
                                string ExpiredCount = lstContracts.Where(row => row.StatusName == "Expired").Count().ToString();
                                string ReviwesCount = ContractTaskManagement.GetCountMyReviews(customerID, Convert.ToInt32(user.ID), RoleCheck).ToString();

                                result = (from row in DraftCount
                                          select new ContractTypecount()
                                          {
                                              DraftCount = Convert.ToInt32(DraftCount),
                                              PendingReviewCount = Convert.ToInt32(PendingReviewCount),
                                              ReviewedCount = Convert.ToInt32(ReviewedCount),
                                              PendingApprovalCount = Convert.ToInt32(PendingApprovalCount),
                                              ApprovedCount = Convert.ToInt32(Approvedcount),
                                              ActiveCount = Convert.ToInt32(ActiveCount),
                                              ExpiredCount = Convert.ToInt32(ExpiredCount),
                                              ReviewesCount = Convert.ToInt32(ReviwesCount)
                                          }).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/ContractExpiring")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Contract Expiring Muthoot Dashborad
        public HttpResponseMessage ContractExpiring([FromBody] ContractExpiringclass ContractExpiringclassobj)
        {
            string Email = ContractExpiringclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Sp_Cont_View_ContractDetails_Result> Sp_Cont_View_ContractDetailsObj = new List<Sp_Cont_View_ContractDetails_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int contractStatusID = ContractExpiringclassobj.contractStatusID;
                            int vendorID = ContractExpiringclassobj.vendorID;
                            int deptID = ContractExpiringclassobj.deptID;
                            int branchID = ContractExpiringclassobj.branchID;

                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            if (ContractExpiringclassobj.upcomingDays != 91)
                            {
                                Sp_Cont_View_ContractDetailsObj = (from row in entities.Sp_Cont_View_ContractDetails(customerId)
                                                                   where row.ExpirationDate <= DateTime.Now.AddDays(ContractExpiringclassobj.upcomingDays).Date
                                                                   select row).ToList();
                            }
                            else
                            {
                                Sp_Cont_View_ContractDetailsObj = (from row in entities.Sp_Cont_View_ContractDetails(customerId)
                                                                   where row.ExpirationDate >= DateTime.Now.AddDays(ContractExpiringclassobj.upcomingDays).Date
                                                                   select row).ToList();
                            }

                            if (Sp_Cont_View_ContractDetailsObj.Count > 0)
                            {
                                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerId), branchID);

                                if (branchList.Count > 0)
                                    Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                                if (vendorID != -1)
                                    Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                                if (deptID != -1)
                                    Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.Where(entry => entry.DepartmentID == deptID).ToList();

                                if (contractStatusID != 0 && contractStatusID != -1)
                                {
                                    Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                                }

                                if (Sp_Cont_View_ContractDetailsObj.Count > 0)
                                {
                                    Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.OrderBy(entry => entry.ExpirationDate).ToList();
                                }
                            }

                            if (Sp_Cont_View_ContractDetailsObj.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = Sp_Cont_View_ContractDetailsObj.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (ContractExpiringclassobj.PageID - 1);
                                var canPage = skip < total;
                                Sp_Cont_View_ContractDetailsObj = Sp_Cont_View_ContractDetailsObj.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Sp_Cont_View_ContractDetailsObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/MyWorkspaceContractTask")]//for My Workspace contract Task
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyWorkspaceContractTask([FromBody] myCaseWorkspaceContractTaskclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                int priorityID = mycasewcobj.priorityID;
                string taskStatus = mycasewcobj.taskStatus;

                List<Cont_SP_GetAssignedTasks_All_Result> lstAssignedContracts = new List<Cont_SP_GetAssignedTasks_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int RoleId = -1;
                            int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            if (user.ContractRoleID != null)
                            {
                                RoleId = Convert.ToInt32(user.ContractRoleID);
                            }
                            if (RoleId > 0)
                            {
                                string RoleCheck = (from row in entities.Roles
                                                    where row.ID == RoleId
                                                    select row.Code).FirstOrDefault();

                                lstAssignedContracts = ContractTaskManagement.GetAssignedTaskList(Convert.ToInt32(customerID), Convert.ToInt32(user.ID), RoleCheck, 3, priorityID, taskStatus);

                                if (lstAssignedContracts.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstAssignedContracts.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (mycasewcobj.PageID - 1);
                                    var canPage = skip < total;
                                    lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();

                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/MyWorkspaceContract")]//for My Workspace contract
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyWorkspaceContract([FromBody] myCaseWorkspaceContractclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_SP_GetAssignedContracts_All_Result> lstAssignedContracts = new List<Cont_SP_GetAssignedContracts_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int contractStatusID = mycasewcobj.contractStatusID;
                    int vendorID = mycasewcobj.vendorID;
                    int deptID = mycasewcobj.deptID;
                    int branchID = mycasewcobj.branchID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            long contractTypeID = -1;
                            int RoleId = -1;
                            int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            if (user.ContractRoleID != null)
                            {
                                RoleId = Convert.ToInt32(user.ContractRoleID);
                            }
                            if (RoleId > 0)
                            {
                                string RoleCheck = (from row in entities.Roles
                                                    where row.ID == RoleId
                                                    select row.Code).FirstOrDefault();
                                List<int> branchList = new List<int>();
                                if (branchID != -1 && branchID != 0)
                                {
                                    branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                                }

                                lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), Convert.ToInt32(user.ID), RoleCheck,
                                3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                                if (mycasewcobj.ContractID != -1)
                                {
                                    lstAssignedContracts = lstAssignedContracts.Where(x => x.ID == mycasewcobj.ContractID).ToList();
                                }

                                if (lstAssignedContracts.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstAssignedContracts.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (mycasewcobj.PageID - 1);
                                    var canPage = skip < total;
                                    lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();

                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/MyReportContract")]//for My Report
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage MyReportContract([FromBody] mycaseReportContractclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Cont_SP_GetAssignedContracts_All_Result> lstAssignedContracts = new List<Cont_SP_GetAssignedContracts_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int contractStatusID = mycasewcobj.contractStatusID;
                    int vendorID = mycasewcobj.vendorID;
                    int deptID = mycasewcobj.deptID;
                    int branchID = mycasewcobj.branchID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            long contractTypeID = -1;
                            int RoleId = -1;
                            int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            RoleId = Convert.ToInt32(user.ContractRoleID);
                            if (RoleId > 0)
                            {
                                string RoleCheck = (from row in entities.Roles
                                                    where row.ID == RoleId
                                                    select row.Code).FirstOrDefault();

                                List<int> branchList = new List<int>();
                                if (branchID != -1 && branchID != 0)
                                {
                                    branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                                }
                                lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), Convert.ToInt32(user.ID), RoleCheck, 3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                                if (lstAssignedContracts.Count > 0)
                                {
                                    int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                    var total = lstAssignedContracts.Count;
                                    var pageSize = PageSizeDyanamic;
                                    var skip = pageSize * (mycasewcobj.PageID - 1);
                                    var canPage = skip < total;
                                    lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/MyDocumentContract")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for My Document
        public HttpResponseMessage MyDocumentContract([FromBody] myDocumentContractclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Cont_SP_MyDocuments_All_Result> lstAssignedContracts = new List<Cont_SP_MyDocuments_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    int contractStatusID = mycasewcobj.contractStatusID;
                    int vendorID = mycasewcobj.vendorID;
                    int deptID = mycasewcobj.deptID;
                    int branchID = mycasewcobj.branchID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int RoleId = -1;
                            long contractTypeID = -1;
                            int customerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            if (user.ContractRoleID != null)
                            {
                                RoleId = Convert.ToInt32(user.ContractRoleID);
                            }
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();
                            List<int> branchList = new List<int>();
                            if (branchID != -1 && branchID != 0)
                            {
                                branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                            }
                            List<string> selectedFileTags = new List<string>();

                            lstAssignedContracts = ContractDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), Convert.ToInt32(user.ID), RoleCheck,
                                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID, selectedFileTags);

                            if (lstAssignedContracts.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = lstAssignedContracts.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mycasewcobj.PageID - 1);
                                var canPage = skip < total;
                                lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();

                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        

        [Authorize]
        [Route("Contract/StatusList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Status Filter
        public HttpResponseMessage StatusList([FromBody] StatusContractclass StatusContractclassobj)
        {
            string Email = StatusContractclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<StatusListContract> Statusobj = new List<StatusListContract>();
                bool isForTask = false;

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            Statusobj = (from row in entities.Cont_tbl_StatusMaster
                                         where row.IsDeleted == false
                                         && row.IsForTask == isForTask
                                         select new StatusListContract
                                         {
                                             StatusID = row.ID,
                                             StatusName = row.StatusName
                                         }).ToList();

                            Statusobj = Statusobj.OrderBy(entry => entry.StatusName).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Statusobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/DeptList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Department Filter
        public HttpResponseMessage DeptList([FromBody] mydeptContractclass mydeptobj)
        {
            string Email = mydeptobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<DepartmentListContract> Dobj = new List<DepartmentListContract>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            Dobj = (from row in entities.Departments
                                    where row.IsDeleted == false
                                    select new DepartmentListContract
                                    {
                                        DepartmentID = row.ID,
                                        DepartmentName = row.Name
                                    }).ToList();

                            Dobj = Dobj.OrderBy(entry => entry.DepartmentName).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Dobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/VendorList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Vendor Filter
        public HttpResponseMessage VendorList([FromBody] myVendorContractclass myVendorContractclassobj)
        {
            string Email = myVendorContractclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<VendorListContract> Vobj = new List<VendorListContract>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int CustomerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            Vobj = (from row in entities.Cont_tbl_VendorMaster
                                    where row.CustomerID == CustomerID
                                    && row.IsDeleted == false
                                    select new VendorListContract
                                    {
                                        VendorID = row.ID,
                                        VendorName = row.VendorName
                                    }).ToList();

                            Vobj = Vobj.OrderBy(entry => entry.VendorName).ToList();
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(Vobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/GetLocationList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Location Filter
        public HttpResponseMessage GetLocationList([FromBody]mylocationContractclass mylocationobj)
        {
            string Email = mylocationobj.Email.ToString();
            try
            {
                List<AssignLocationList> AssignLocationListObj = new List<AssignLocationList>();

                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(user.CustomerID);

                            var LocationList = CustomerBranchManagement.GetAssignedLocationLitigationList(customerId);
                            foreach (var item in LocationList)
                            {
                                string Name = (from row in entities.CustomerBranches
                                               where row.ID == item
                                               select row.Name).FirstOrDefault();
                                AssignLocationListObj.Add(new AssignLocationList
                                {
                                    AssignID = item,
                                    AssignName = Name
                                });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(AssignLocationListObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/ContractTaskListPriority")]//for Task List Priority Status Filter
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTaskListPriority()
        {
            try
            {
                List<PriorityType> PriorityTypeobj = new List<PriorityType>();

                PriorityTypeobj.Add(new PriorityType { PriorityName = "All", PriorityID = 0 });
                PriorityTypeobj.Add(new PriorityType { PriorityName = "High", PriorityID = 1 });
                PriorityTypeobj.Add(new PriorityType { PriorityName = "Medium", PriorityID = 2 });
                PriorityTypeobj.Add(new PriorityType { PriorityName = "Low", PriorityID = 3 });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(PriorityTypeobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractTaskListStatus")]//for Task List Status Filter
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTaskListStatus()
        {
            try
            {
                List<StatusType> StatusTypeobj = new List<StatusType>();

                StatusTypeobj.Add(new StatusType { StatusName = "All", StatusID = 0 });
                StatusTypeobj.Add(new StatusType { StatusName = "Upcoming", StatusID = 1 });
                StatusTypeobj.Add(new StatusType { StatusName = "Submitted for Review", StatusID = 2 });
                StatusTypeobj.Add(new StatusType { StatusName = "Submitted for Approval", StatusID = 3 });
                StatusTypeobj.Add(new StatusType { StatusName = "Reviewed/Approved", StatusID = 4 });
                StatusTypeobj.Add(new StatusType { StatusName = "Overdue", StatusID = 5 });
                StatusTypeobj.Add(new StatusType { StatusName = "Closed", StatusID = 6 });

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(StatusTypeobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        [Authorize]
        [Route("Contract/GetMyDocumentListContract")]//for My Document List
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetMyDocumentListContract([FromBody] getmyDocumentPathContractclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Cont_tbl_FileData> lstAssignedContracts = new List<Cont_tbl_FileData>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int contractID = mycasewcobj.ContractID;
                            lstAssignedContracts = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(contractID), 0);

                            if (lstAssignedContracts.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = lstAssignedContracts.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mycasewcobj.PageID - 1);
                                var canPage = skip < total;
                                lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractAllTask")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractAllTask([FromBody] ContractAllTaskclass AllTaskobj)
        {
            string Email = AllTaskobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Cont_tbl_FileData> ContractsDetail = new List<Cont_tbl_FileData>();

                List<getTaskInstanceDetail> TaskOutput = new List<getTaskInstanceDetail>();

                List<Cont_tbl_TaskInstance> lstAssignedTasks = new List<Cont_tbl_TaskInstance>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        int CustomerID = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);


                        if (user != null)
                        {
                            lstAssignedTasks = (from row in entities.Cont_tbl_TaskInstance
                                                where row.ContractID == AllTaskobj.contractID
                                                select row).ToList();

                            if (lstAssignedTasks.Count > 0)
                            {
                                ContractsDetail = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(AllTaskobj.contractID), 0);
                                ContractsDetail = ContractsDetail.Where(x => x.TaskID != null).ToList();

                                TaskOutput = (from row in lstAssignedTasks
                                              select new getTaskInstanceDetail
                                              {
                                                  ID = row.ID,
                                                  ParentTaskID = row.ParentTaskID,
                                                  CustomerID = row.CustomerID,
                                                  ContractID = row.ContractID,
                                                  IsActive = row.IsActive,
                                                  TaskType = row.TaskType,
                                                  TaskTitle = row.TaskTitle,
                                                  TaskDesc = row.TaskDesc,
                                                  AssignOn = row.AssignOn,
                                                  DueDate = row.DueDate,
                                                  PriorityID = row.PriorityID,
                                                  ExpectedOutcome = row.ExpectedOutcome,
                                                  Remark = row.Remark,
                                                  CreatedBy = row.CreatedBy,
                                                  CreatedOn = row.CreatedOn,
                                                  UpdatedBy = row.UpdatedBy,
                                                  UpdatedOn = row.UpdatedOn,
                                                  DeletedBy = row.DeletedBy,
                                                  DeletedOn = row.DeletedOn,
                                                  Priority = ContractDocumentManagement.getPriorityName(row.PriorityID),
                                                  FileDetail = ContractDocumentManagement.GetTaskDocumentsByDocTypeID(ContractsDetail, row.ID)
                                              }).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(TaskOutput).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/GetMyDocumentTaskListContract")]//for My Task Document List
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetMyDocumentTaskListContract([FromBody] getmyTaskDocumentPathContractclass mycasewcobj)
        {
            string Email = mycasewcobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_tbl_FileData> lstAssignedContracts = new List<Cont_tbl_FileData>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {
                            int contractID = mycasewcobj.ContractID;
                            lstAssignedContracts = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(contractID), 0);
                            lstAssignedContracts = lstAssignedContracts.Where(x => x.TaskID != null).ToList();
                            if (lstAssignedContracts.Count > 0)
                            {
                                if (mycasewcobj.TaskID != -1)
                                {
                                    lstAssignedContracts = lstAssignedContracts.Where(x => x.TaskID == mycasewcobj.TaskID).ToList();
                                }
                            }
                            if (lstAssignedContracts.Count > 0)
                            {
                                int PageSizeDyanamic = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

                                var total = lstAssignedContracts.Count;
                                var pageSize = PageSizeDyanamic;
                                var skip = pageSize * (mycasewcobj.PageID - 1);
                                var canPage = skip < total;
                                lstAssignedContracts = lstAssignedContracts.Skip(skip).Take(pageSize).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(lstAssignedContracts).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Contract detail 
        public HttpResponseMessage ContractDetail([FromBody] ContractDetailclass ContractExpiringobj)
        {
            string Email = ContractExpiringobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<GetContractDetailsOutput> ResultObj = new List<GetContractDetailsOutput>();
                //List<Cont_SP_GetContractDetails_Result> ResultObj = new List<Cont_SP_GetContractDetails_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);
                            if (ContractExpiringobj.contractID != -1)
                            {

                                var lstVendorMasters = ContractDocumentManagement.GetVendors_All(customerId);
                                var lstVendorMappingMasters = ContractDocumentManagement.GetVendorsMapping_All(Convert.ToInt32(ContractExpiringobj.contractID));
                                var Department = ContractDocumentManagement.GetAllDepartmentMasterList(customerId);
                                string lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(Convert.ToInt64(ContractExpiringobj.contractID), customerId);
                                var lstContractTypes = ContractManagement.GetContractTypes_All(customerId);

                                //ResultObj = (from row in entities.Cont_SP_GetContractDetails(customerId, ContractExpiringobj.contractID)
                                //             select row).ToList();

                                ResultObj = (from row in entities.Cont_SP_GetContractDetails(customerId, ContractExpiringobj.contractID)
                                             select new GetContractDetailsOutput
                                             {
                                                 ID = row.ID,
                                                 CustomerID = row.CustomerID,
                                                 IsDeleted = row.IsDeleted,
                                                 ContractType = row.ContractType,
                                                 ContractNo = row.ContractNo,
                                                 ContractTitle = row.ContractTitle,
                                                 ContractDetailDesc = row.ContractDetailDesc,
                                                 CustomerBranchID = row.CustomerBranchID,
                                                 DepartmentID = row.DepartmentID,
                                                 ContractTypeID = row.ContractTypeID,
                                                 ContractSubTypeID = row.ContractSubTypeID,
                                                 ProposalDate = row.ProposalDate,
                                                 AgreementDate = row.AgreementDate,
                                                 EffectiveDate = row.EffectiveDate,
                                                 ReviewDate = row.ReviewDate,
                                                 ExpirationDate = row.ExpirationDate,
                                                 NoticeTermNumber = row.NoticeTermNumber,
                                                 NoticeTermType = row.NoticeTermType,
                                                 PaymentTermID = row.PaymentTermID,
                                                 ContractAmt = row.ContractAmt,
                                                 ProductItems = row.ProductItems,
                                                 CreatedOn = row.CreatedOn,
                                                 CreatedBy = row.CreatedBy,
                                                 UpdatedOn = row.UpdatedOn,
                                                 UpdatedBy = row.UpdatedBy,
                                                 ContactPersonOfDepartment = row.ContactPersonOfDepartment,
                                                 PaymentType = row.PaymentType,
                                                 AddNewClause = row.AddNewClause,
                                                 //Product_Specification = row.Product_Specification,
                                                 //Rate_per_product = row.Rate_per_product,
                                                 //GST = row.GST,
                                                 //PaymentTerm = row.PaymentTerm,
                                                 //Delivery_Installation_Period = row.Delivery_Installation_Period,
                                                 //Penalty = row.Penalty,
                                                 //Product_Warranty = row.Product_Warranty,
                                                 ContractStatusID = row.ContractStatusID,
                                                 StatusName = row.StatusName,
                                                 StatusChangeOn = row.StatusChangeOn,
                                                 VendorName = ContractDocumentManagement.GetVendorName(lstVendorMappingMasters, lstVendorMasters, row.ID),
                                                 DeptName = ContractDocumentManagement.GetDeptName(Department, row.DepartmentID),
                                                 OwnerName = lstContractUserAssignment,
                                                 ContractTypeName = ContractDocumentManagement.GetContractTypeName(lstContractTypes, row.ContractTypeID),
                                                 BrancName = ContractManagement.getBranchName(row.CustomerBranchID)
                                             }).ToList();
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ResultObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/DocumentDetailsAndroid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]//for Document Detail Path
        public HttpResponseMessage DocumentDetailsAndroid([FromBody] getContractDetailclass MyDocumentclassobj)
        {
            string Email = MyDocumentclassobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();
                        if (user != null)
                        {

                            string fetchVersion = (from row in entities.Cont_tbl_FileData
                                                   where row.ContractID == MyDocumentclassobj.ContractID
                                                   && row.IsDeleted == false
                                                   && row.ID == MyDocumentclassobj.FileID
                                                   select row.Version).FirstOrDefault();
                            if (!string.IsNullOrEmpty(fetchVersion))
                            {
                                string pathforandriod = PublicClassZip1.FetchPathforContract(Convert.ToInt32(MyDocumentclassobj.ContractID), Convert.ToInt32(MyDocumentclassobj.FileID), fetchVersion, Convert.ToInt32(user.ID));
                                if (!string.IsNullOrEmpty(pathforandriod))
                                {
                                    string pathforandriodoutput = pathforandriod;
                                    pathforandriodoutput = pathforandriodoutput.Substring(pathforandriodoutput.LastIndexOf("TempFiles"));
                                    pathforandriodoutput = pathforandriodoutput.Replace("\\", "/");

                                    string finaloutputforandriodpath = ConfigurationManager.AppSettings["destinationpathfile"].ToString();
                                    finaloutputforandriodpath = finaloutputforandriodpath + pathforandriodoutput;

                                    UpdateNotificationObj.Add(new UpdateNotification { Message = finaloutputforandriodpath });
                                }
                            }
                            else
                            {
                                UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                            }
                        }

                        else
                        {
                            UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractTaskDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractTaskDetail([FromBody] ContractTaskDetailclass Taskobj)
        {
            List<ContractTaskDetailOutput> output = new List<ContractTaskDetailOutput>();
            string Email = Taskobj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                List<Cont_SP_GetTaskDetails_UserRoleWise_Result> query = new List<Cont_SP_GetTaskDetails_UserRoleWise_Result>();
                List<Cont_SP_GetContractDetails_Result> record = new List<Cont_SP_GetContractDetails_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int RoleId = -1;
                            if (user.ContractRoleID != null)
                            {
                                RoleId = Convert.ToInt32(user.ContractRoleID);
                            }
                            string RoleCheck = (from row in entities.Roles
                                                where row.ID == RoleId
                                                select row.Code).FirstOrDefault();

                            int strContractID = Convert.ToInt32(Taskobj.contractID);
                            int strTaskID = Convert.ToInt32(Taskobj.TaskID);
                            int strRoleID = Convert.ToInt32(Taskobj.RoleID);
                            int USerid = Convert.ToInt32(user.ID);

                            int customerId = Convert.ToInt32(UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID);

                            if (strContractID != null && strTaskID != null && USerid != null && strRoleID != null)
                            {
                                if (RoleCheck == "MGMT" || RoleCheck == "CADMN")
                                {
                                    var lstTaskUsers = ContractTaskManagement.GetContractTaskUserAssignment(customerId, strContractID, strTaskID);

                                    if (lstTaskUsers.Where(row => row.UserID == USerid).ToList().Count > 0)
                                        USerid = lstTaskUsers.Where(row => row.UserID == USerid).Select(row => row.UserID).FirstOrDefault();
                                    else
                                        USerid = lstTaskUsers.Select(row => row.UserID).FirstOrDefault();
                                }

                                if (Convert.ToInt32(user.ID) == USerid || (RoleCheck == "MGMT" || RoleCheck == "CADMN"))
                                {
                                    query = ContractTaskManagement.GetTaskDetailsByUserRoleWise(Taskobj.contractID, Taskobj.TaskID, USerid, strRoleID);
                                }

                                record = (from row in entities.Cont_SP_GetContractDetails(customerId, Taskobj.contractID)
                                          select row).ToList();

                                string AssignBy = string.Empty;
                                if (query.Count > 0)
                                {
                                    AssignBy = ContractUserManagement.GetUserNameByUserID(query[0].CreatedBy);
                                }

                                output.Add(new ContractTaskDetailOutput { TaskDetail = query, ContractsDetail = record, TaskAssignName = AssignBy });
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(output).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractMilestoneDetail")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractMilestoneDetail([FromBody] ContractMilestoneDetailclass1 obj)
        {
            string Email = obj.Email.ToString();
            int MilestoneID = -1;
            if (obj.MilestoneID != null)
                MilestoneID = obj.MilestoneID;

            int ContractID = -1;
            if (obj.ContractID != null)
                ContractID = obj.ContractID;

            int StatusID = -1;
            if (obj.StatusID != null)
                StatusID = obj.StatusID;

            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }

                
                List<Cont_SP_AssignMilestoneDetailNew_Result> record = new List<Cont_SP_AssignMilestoneDetailNew_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {                            
                            int USerid = Convert.ToInt32(user.ID);

                            int customerId = Convert.ToInt32(user.CustomerID);

                            record = ContractTaskManagement.GetContractMilestoneDetail(customerId, USerid);
                            
                            if (MilestoneID != -1)
                                record = record.Where(x => x.ID == MilestoneID).ToList();

                            if (ContractID != -1)
                                record = record.Where(x => x.ContractTemplateID == ContractID).ToList();

                            if (StatusID != -1)
                                record = record.Where(x => x.StatusID == StatusID).ToList();

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Authorize]
        [Route("Contract/ContractMilestoneList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractMilestoneList([FromBody] ContractMilestoneDetailclass obj)
        {
            string Email = obj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_SP_AssignMilestone_Result> record = new List<Cont_SP_AssignMilestone_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int USerid = Convert.ToInt32(user.ID);

                            int customerId = Convert.ToInt32(user.CustomerID);

                            record = ContractTaskManagement.GetContractMilestoneList(customerId, USerid);

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractListforMilestone")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractListforMilestone([FromBody] ContractMilestoneDetailclass obj)
        {
            string Email = obj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_SP_AssignContractMilestone_Result> record = new List<Cont_SP_AssignContractMilestone_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int USerid = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);
                            record = ContractTaskManagement.GetContractListformilestone(customerId, USerid);
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractListforReviews")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractListforReviews([FromBody] ContractReviewsDetailclass obj)
        {
            string Email = obj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_SP_GetReviwedContracts_All_Result> record = new List<Cont_SP_GetReviwedContracts_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int RoleID = obj.RoleID;
                            int branchID = -1;
                            int vendorID = -1;
                            int deptID = -1;
                            long contractStatusID = -1;
                            long contractTypeID = -1;
                            int USerid = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);

                            var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerId), branchID);

                            record = ContractTaskManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerId), USerid, RoleID, branchList, vendorID, deptID, contractStatusID, contractTypeID);
                            
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractListRole")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractListRole([FromBody] ContractMilestoneDetailclass obj)
        {
            string Email = obj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<ContractUserRole> record = new List<ContractUserRole>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int USerid = Convert.ToInt32(user.ID);
                            int customerId = Convert.ToInt32(user.CustomerID);
                            var Query = (from row in entities.ContractTemplateAssignments
                                         where row.UserID == USerid
                                         && row.IsActive == true                            
                                         select row).ToList();
                            record.Clear();
                            if (Query.Count > 0)
                            {
                                int Revflag = Query.Where(x => x.RoleID == 4).ToList().Count;
                                int Appflag = Query.Where(x => x.RoleID == 6).ToList().Count;
                                int PrimaryAppflag = Query.Where(x => x.RoleID == 29).ToList().Count;
                                if (Revflag > 0 && Appflag > 0 && PrimaryAppflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 4, Name = "Reviwer" });
                                    record.Add(new ContractUserRole { ID = 6, Name = "Approver" });
                                    record.Add(new ContractUserRole { ID = 29, Name = "Primary Approver" });
                                }
                                else if (Revflag > 0 && Appflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 4, Name = "Reviwer" });
                                    record.Add(new ContractUserRole { ID = 6, Name = "Approver" });
                                }
                                else if (Revflag > 0 && PrimaryAppflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 4, Name = "Reviwer" });
                                    record.Add(new ContractUserRole { ID = 29, Name = "Primary Approver" });
                                    
                                }
                                else if (Appflag > 0 && PrimaryAppflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 6, Name = "Approver" });
                                    record.Add(new ContractUserRole { ID = 29, Name = "Primary Approver" });                                    
                                }
                                else if (PrimaryAppflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 29, Name = "Primary Approver" });
                                    
                                }
                                else if (Revflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 4, Name = "Reviwer" });                                    
                                }
                                else if (Appflag > 0)
                                {
                                    record.Clear();
                                    record.Add(new ContractUserRole { ID = 6, Name = "Approver" });                                    
                                }
                            }
                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("Contract/ContractListAuditLog")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ContractListAuditLog([FromBody] ContractAuditDetailclass obj)
        {
            string Email = obj.Email.ToString();
            try
            {
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }

                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }


                List<Cont_SP_AuditLogs_All_Result> record = new List<Cont_SP_AuditLogs_All_Result>();

                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        User user = (from row in entities.Users
                                     where (row.Email == Email || row.Email == EmailIOS)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                     select row).FirstOrDefault();

                        if (user != null)
                        {
                            int ContractID = obj.ContractID;

                            record = ContractTaskManagement.GetContractAuditLogs_All(Convert.ToInt32(user), ContractID);

                        }
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(record).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}
