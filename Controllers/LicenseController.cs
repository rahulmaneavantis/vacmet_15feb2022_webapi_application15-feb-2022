﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using AppWebApplication.Data;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using Newtonsoft.Json.Linq;

namespace AppWebApplication.Controllers
{
    public class LicenseController : ApiController
    {

        #region License By Bhagawan
        //[Route("License/GetLicenseData")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetLicenseData(int loggedInUserID, int customerID, string loggedInUserRole, string isstatutoryinternal,
        //    string Userdetail, string Statusdetail, string LicenseTypedetail, string Branchdetail, string Deptdetail)//10/12/2021
        //{
        //    try
        //    {
        //        int branchID = -1;
        //        int deptID = -1;
        //        string licenseStatusID = string.Empty;
        //        //long licenseStatusID = -1;
        //        long licenseTypeID = -1;

        //        List<SP_LicenseMyReport_Result> result = new List<SP_LicenseMyReport_Result>();

        //        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
        //        //if (licenseTypeID == -1)//Statutory
        //        //{
        //        //    isstatutoryinternal = "S";
        //        //}
        //        //else //Internal
        //        //{
        //        //    isstatutoryinternal = "I";
        //        //}

        //        result = LicenseMgmt.GetAllReportData(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole,
        //         branchList, deptID, licenseStatusID, licenseTypeID, isstatutoryinternal);

        //        //10/12/2021
        //        if (Userdetail != null)
        //        {
        //            System.Collections.Generic.IList<int> UserIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Userdetail);

        //            if (UserIds.Count != 0)
        //            {
        //                result = result.Where(x => UserIds.Contains(Convert.ToInt32(x.UserID))).ToList();
        //            }
        //        }
        //        if (Statusdetail != null)
        //        {
        //            result = result.Where(x => x.StatusID == Convert.ToInt16(Statusdetail)).ToList();
        //        }
        //        if (LicenseTypedetail != null)
        //        {
        //            result = result.Where(x => x.LicenseTypeID == Convert.ToInt16(LicenseTypedetail)).ToList();
        //        }
        //        if (Branchdetail != null)
        //        {
        //            System.Collections.Generic.IList<int> branchIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Branchdetail);

        //            if (branchIds.Count != 0)
        //            {
        //                result = result.Where(x => branchIds.Contains(Convert.ToInt32(x.CustomerBranchID))).ToList();
        //            }
        //        }
        //        if (Deptdetail != null)
        //        {
        //            System.Collections.Generic.IList<int> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Deptdetail);

        //            if (deptIds.Count != 0)
        //            {
        //                result = result.Where(x => deptIds.Contains(Convert.ToInt32(x.DepartmentID))).ToList();
        //            }
        //        }
        //        //10/12/2021

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        //[Authorize]
        //[Route("License/GetLicenseData")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetLicenseData(int loggedInUserID, int customerID, string loggedInUserRole, string isstatutoryinternal,
        //    string Userdetail, string Statusdetail, string LicenseTypedetail)//10/12/2021
        //{
        //    try
        //    {
        //        int branchID = -1;
        //        int deptID = -1;
        //        string licenseStatusID = string.Empty;
        //        //long licenseStatusID = -1;
        //        long licenseTypeID = -1;

        //        List<SP_LicenseMyReport_Result> result = new List<SP_LicenseMyReport_Result>();

        //        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
        //        //if (licenseTypeID == -1)//Statutory
        //        //{
        //        //    isstatutoryinternal = "S";
        //        //}
        //        //else //Internal
        //        //{
        //        //    isstatutoryinternal = "I";
        //        //}

        //        result = LicenseMgmt.GetAllReportData(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole,
        //         branchList, deptID, licenseStatusID, licenseTypeID, isstatutoryinternal);

        //        //10/12/2021
        //        if (Userdetail != null)
        //        {
        //            System.Collections.Generic.IList<int> UserIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Userdetail);

        //            if (UserIds.Count != 0)
        //            {
        //                result = result.Where(x => UserIds.Contains(Convert.ToInt32(x.UserID))).ToList();
        //            }
        //        }
        //        if (Statusdetail != null)
        //        {
        //            result = result.Where(x => x.StatusID == Convert.ToInt16(Statusdetail)).ToList();
        //        }

        //        if (LicenseTypedetail != null)
        //        {
        //            result = result.Where(x => x.LicenseTypeID == Convert.ToInt16(LicenseTypedetail)).ToList();
        //        }
        //        //10/12/2021

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}


        //[Authorize]
        //[Route("License/GetLicenseData")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetLicenseData(int loggedInUserID, int customerID, string loggedInUserRole, string isstatutoryinternal)
        //{
        //    try
        //    {
        //        int branchID = -1;
        //        int deptID = -1;
        //        string licenseStatusID = string.Empty;
        //        //long licenseStatusID = -1;
        //        long licenseTypeID = -1;

        //        List<SP_LicenseMyReport_Result> result = new List<SP_LicenseMyReport_Result>();

        //        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
        //        //if (licenseTypeID == -1)//Statutory
        //        //{
        //        //    isstatutoryinternal = "S";
        //        //}
        //        //else //Internal
        //        //{
        //        //    isstatutoryinternal = "I";
        //        //}

        //        result = LicenseMgmt.GetAllReportData(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole,
        //         branchList, deptID, licenseStatusID, licenseTypeID, isstatutoryinternal);

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}
        #endregion 

        [Authorize]
        [Route("License/GetLicenseData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLicenseData(int loggedInUserID, int customerID, string loggedInUserRole, string isstatutoryinternal,
            string Userdetail, string Statusdetail, string LicenseTypedetail, string Branchdetail, string Deptdetail)//10/12/2021
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                string licenseStatusID = string.Empty;
                //long licenseStatusID = -1;
                long licenseTypeID = -1;

                List<SP_LicenseMyReport_Result> result = new List<SP_LicenseMyReport_Result>();

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                //if (licenseTypeID == -1)//Statutory
                //{
                //    isstatutoryinternal = "S";
                //}
                //else //Internal
                //{
                //    isstatutoryinternal = "I";
                //}

                result = LicenseMgmt.GetAllReportData(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole,
                 branchList, deptID, licenseStatusID, licenseTypeID, isstatutoryinternal);

                //10/12/2021
                if (Userdetail != null)
                {
                    System.Collections.Generic.IList<int> UserIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Userdetail);

                    if (UserIds.Count != 0)
                    {
                        result = result.Where(x => UserIds.Contains(Convert.ToInt32(x.UserID)) || UserIds.Contains(Convert.ToInt32(x.PerformerID)) || UserIds.Contains(Convert.ToInt32(x.ReviewerID))).ToList();
                    }
                }
                if (Statusdetail != null)
                {
                    result = result.Where(x => x.StatusID == Convert.ToInt16(Statusdetail)).ToList();
                }
                if (LicenseTypedetail != null)
                {
                    result = result.Where(x => x.LicenseTypeID == Convert.ToInt16(LicenseTypedetail)).ToList();
                }
                if (Branchdetail != null)
                {
                    System.Collections.Generic.IList<int> branchIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Branchdetail);

                    if (branchIds.Count != 0)
                    {
                        result = result.Where(x => branchIds.Contains(Convert.ToInt32(x.CustomerBranchID))).ToList();
                    }
                }
                if (Deptdetail != null)
                {
                    System.Collections.Generic.IList<int> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Deptdetail);

                    if (deptIds.Count != 0)
                    {
                        result = result.Where(x => deptIds.Contains(Convert.ToInt32(x.DepartmentID))).ToList();
                    }
                }
                //10/12/2021

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("License/GetWorkspaceLicenseData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetWorkspaceLicenseData(int customerID, int userID, string Flag, string isstatutoryinternal, string licenseStatus, string ClickChangeflag)
        {
            try
            {
                List<MyLicenseDetails> objMy = new List<MyLicenseDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int branchID = -1;
                    int deptID = -1;
                    long licenseTypeID = -1;
                    List<Int32> roles;
                    roles = CustomerBranchManagement.GetAssignedroleid(userID);
                    //if (roles.Contains(3) && roles.Contains(4))
                    //{
                    //    ClickChangeflag = "P";
                    //}
                    //else if (roles.Contains(3))
                    //{
                    //    ClickChangeflag = "P";
                    //}
                    //else if (roles.Contains(4))
                    //{
                    //    ClickChangeflag = "R";
                    //}
                    if (ClickChangeflag == null)
                    {
                        ClickChangeflag = "P";
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                    List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();

                    if (isstatutoryinternal == "S") //statutory
                    {
                        isstatutoryinternal = "S";
                    }
                    else
                    {
                        isstatutoryinternal = "I";
                    }

                    //if (!string.IsNullOrEmpty(licenseStatus))
                    //{
                    //    licenseTypeID = Convert.ToInt32(licenseStatus);
                    //}
                    if (Flag == "MGMT")
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(customerID, userID,
                            branchList, deptID, licenseStatus, licenseTypeID, "MGMT", isstatutoryinternal);
                    }
                    else if (Flag == "CADMN")
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(customerID, userID,
                         branchList, deptID, licenseStatus, licenseTypeID, "CADMN", isstatutoryinternal);
                        if (ClickChangeflag == "P")
                        {
                            MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                        }
                        else if (ClickChangeflag == "R")
                        {
                            MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                        }
                    }
                    else if (roles.Contains(3) && roles.Contains(4))
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(Convert.ToInt32(customerID), userID,
                             branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                        if (ClickChangeflag == "P")
                        {
                            MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                        }
                        else
                        {
                            MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                        }

                    }
                    else if (roles.Contains(3))
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(Convert.ToInt32(customerID), userID,
                          branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    else if (roles.Contains(4))
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(Convert.ToInt32(customerID), userID,
                            branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                    }
                    else
                    {
                        MasterTransction = LicenseMgmt.GetAllLicenseDetails(Convert.ToInt32(customerID), userID,
                         branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    foreach (var item in MasterTransction)
                    {
                        bool result = false;
                        if (isstatutoryinternal == "S")
                        {
                            #region Statutory
                            if (item.Status == "Registered" || item.Status == "Validity Expired")
                            {
                                result = true;
                            }
                            else
                            {
                                if (item.Status == "Applied")
                                {
                                    var RoleID = (from row in entities.ComplianceAssignments
                                                  where row.ComplianceInstanceID == item.ComplianceInstanceID
                                                  && row.UserID == userID
                                                  select row.RoleID).FirstOrDefault();
                                    if (RoleID == 3)
                                    {
                                        result = true;
                                    }
                                }
                                else
                                {
                                    var statusid = (from row in entities.RecentComplianceTransactionViews
                                                    where row.ComplianceScheduleOnID == item.ComplianceScheduleOnID
                                                    select row.ComplianceStatusID).FirstOrDefault();

                                    if (statusid != null)
                                    {
                                        if (ClickChangeflag == "P")
                                        {
                                            var RoleID = (from row in entities.ComplianceAssignments
                                                          where row.ComplianceInstanceID == item.ComplianceInstanceID
                                                          && row.UserID == userID
                                                          && row.RoleID == 3
                                                          select row.RoleID).FirstOrDefault();
                                            if (RoleID != 0)
                                            {
                                                if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                                                {
                                                    result = true;
                                                }
                                                if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                                                {
                                                    result = true;
                                                }
                                            }
                                        }
                                        else if (ClickChangeflag == "R")
                                        {
                                            var RoleID = (from row in entities.ComplianceAssignments
                                                          where row.ComplianceInstanceID == item.ComplianceInstanceID
                                                          && row.UserID == userID
                                                           && row.RoleID == 4
                                                          select row.RoleID).FirstOrDefault();
                                            if (RoleID != 0)
                                            {
                                                if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                                                {
                                                    result = true;
                                                }
                                                else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                                                {
                                                    result = true;
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        else
                        {

                            #region Internal
                            if (item.Status == "Registered" || item.Status == "Validity Expired")
                            {
                                result = true;
                            }
                            else
                            {
                                if (item.Status == "Applied")
                                {
                                    var RoleID = (from row in entities.InternalComplianceAssignments
                                                  where row.InternalComplianceInstanceID == item.ComplianceInstanceID
                                                  && row.UserID == userID
                                                  select row.RoleID).FirstOrDefault();
                                    if (RoleID == 3)
                                    {
                                        result = true;
                                    }
                                }
                                else
                                {
                                    var statusid = (from row in entities.RecentInternalComplianceTransactionViews
                                                    where row.InternalComplianceScheduledOnID == item.ComplianceScheduleOnID
                                                    select row.ComplianceStatusID).FirstOrDefault();

                                    if (statusid != null)
                                    {
                                        if (ClickChangeflag == "P")
                                        {
                                            var RoleID = (from row in entities.InternalComplianceAssignments
                                                          where row.InternalComplianceInstanceID == item.ComplianceInstanceID
                                                          && row.UserID == userID
                                                           && row.RoleID == 3
                                                          select row.RoleID).FirstOrDefault();
                                            if (RoleID != 0)
                                            {
                                                if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                                                {
                                                    result = true;
                                                }
                                                if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                                                {
                                                    result = true;
                                                }
                                            }
                                        }
                                        else if (ClickChangeflag == "R")
                                        {
                                            var RoleID = (from row in entities.InternalComplianceAssignments
                                                          where row.InternalComplianceInstanceID == item.ComplianceInstanceID
                                                          && row.UserID == userID
                                                           && row.RoleID == 4
                                                          select row.RoleID).FirstOrDefault();
                                            if (RoleID != 0)
                                            {
                                                if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                                                {
                                                    result = true;
                                                }
                                                else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                                                {
                                                    result = true;
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        objMy.Add(new MyLicenseDetails { CustomerBrach = item.CustomerBrach, LicensetypeName = item.LicensetypeName, LicenseNo = item.LicenseNo, Licensetitle = item.Licensetitle, ApplicationDate = item.ApplicationDate, PerformerName = item.PerformerName, ReviewerName = item.ReviewerName, DeptName = item.DeptName, EndDate = Convert.ToDateTime(item.EndDate), MGRStatus = item.MGRStatus, StatusResult = result, ComplianceInstanceID = item.ComplianceInstanceID, ComplianceScheduleOnID = item.ComplianceScheduleOnID, CustomerBranchID = item.CustomerBranchID, DepartmentID = item.DepartmentID, LicenseTypeID = item.LicenseTypeID, StatusID = item.StatusID, LicenseID=item.LicenseID, ComplianceID=item.ComplianceID, Status=item.Status});
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objMy).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("License/GetGraphDetailData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetGraphDetailData(long Userid, int Customerid, string FDte, string EDte, string IsFlag, string status, string internalStatutory, int licenseTypeID, int branchId)
        {
            try
            {
                DateTime FromDate;
                DateTime EndDate;
                List<int> statusId = new List<int>();
                List<MyGraphData> objResult = new List<MyGraphData>();
                List<SP_InternalLicenseInstanceTransactionCount_Result> lstInternalLicense = new List<SP_InternalLicenseInstanceTransactionCount_Result>();
                List<SP_LicenseInstanceTransactionCount_Result> lstAssignedLicenses = new List<SP_LicenseInstanceTransactionCount_Result>();
                statusId.Add(4);
                statusId.Add(5);
                statusId.Add(7);
                statusId.Add(9);
                // statusId.Add(12);

                int deptID = -1;

                if (!string.IsNullOrEmpty(FDte.ToString()))
                {
                    FromDate = Convert.ToDateTime(FDte);
                }
                else
                {
                    FromDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(EDte.ToString()))
                {
                    EndDate = Convert.ToDateTime(EDte);
                }
                else
                {
                    EndDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Customerid, branchId);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (internalStatutory == "S")
                    {
                        entities.Database.CommandTimeout = 180;
                        lstAssignedLicenses = (entities.SP_LicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
                        if (branchList.Count > 0)
                            lstAssignedLicenses = lstAssignedLicenses.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                        if (deptID != -1)
                            lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.DepartmentID == deptID).ToList();

                        if (licenseTypeID != 0 && licenseTypeID != -1)
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                        }

                        if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
                        }
                        else if (!FromDate.ToString().Contains("1900"))
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate).ToList();
                        }
                        else if (!EndDate.ToString().Contains("1900"))
                        {

                            lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.EndDate <= EndDate).ToList();
                        }

                        if (lstAssignedLicenses.Count > 0)
                        {
                            lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.EndDate).ToList();
                        }
                        lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.RoleID == 3).ToList();
                        if (!string.IsNullOrEmpty(status))
                        {

                            if (IsFlag == "MGMT" || IsFlag == "CADMN")
                            {
                                //List<int> cstatusId = new List<int>();
                                //cstatusId.Add(1);
                                //cstatusId.Add(2);
                                //cstatusId.Add(3);
                                //cstatusId.Add(6);
                                //cstatusId.Add(8);
                                //cstatusId.Add(10);

                                if (status.Equals("ApplicationOverduebutnotapplied"))
                                {
                                    //lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                                    //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")).ToList();

                                    lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                                        (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();

                                    //lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                                    //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                                    //&& row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                                    //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Expiredappliedbutnotrenewed"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
                                    && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Expired"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired").ToList();

                                    //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired"
                                    //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                                }
                                if (status.Equals("Active"))
                                {
                                    //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active"
                                    //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active").ToList();
                                }
                                if (status.Equals("Terminate"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
                                }
                            }
                            else
                            {
                                if (status.Equals("ApplicationOverduebutnotapplied"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
                                }
                                if (status.Equals("Expiredappliedbutnotrenewed"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
                                    && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Expired"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expired").ToList();
                                }
                                if (status.Equals("Active"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Terminate"))
                                {
                                    lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
                                }
                            }
                        }
                        objResult.Add(new MyGraphData { Statutory = lstAssignedLicenses, Internal = lstInternalLicense });
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 180;
                        lstInternalLicense = (entities.SP_InternalLicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
                        if (branchList.Count > 0)
                            lstInternalLicense = lstInternalLicense.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                        if (deptID != -1)
                            lstInternalLicense = lstInternalLicense.Where(entry => entry.DepartmentID == deptID).ToList();

                        if (licenseTypeID != 0 && licenseTypeID != -1)
                        {
                            lstInternalLicense = lstInternalLicense.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                        }

                        if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
                        {
                            lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
                        }
                        else if (!FromDate.ToString().Contains("1900"))
                        {
                            lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate).ToList();
                        }
                        else if (!EndDate.ToString().Contains("1900"))
                        {

                            lstInternalLicense = lstInternalLicense.Where(entry => entry.EndDate <= EndDate).ToList();
                        }

                        if (lstInternalLicense.Count > 0)
                        {
                            lstInternalLicense = lstInternalLicense.OrderBy(entry => entry.EndDate).ToList();
                        }
                        lstInternalLicense = lstInternalLicense.Where(entry => entry.RoleID == 3).ToList();
                        if (!string.IsNullOrEmpty(status))
                        {

                            if (IsFlag == "MGMT" || IsFlag == "CADMN")
                            {
                                //List<int> cstatusId = new List<int>();
                                //cstatusId.Add(1);
                                //cstatusId.Add(2);
                                //cstatusId.Add(3);
                                //cstatusId.Add(6);
                                //cstatusId.Add(8);
                                //cstatusId.Add(10);

                                if (status.Equals("ApplicationOverduebutnotapplied"))
                                {
                                    //lstInternalLicense = lstInternalLicense.Where(row =>
                                    //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                                    //&& row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                                    //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                                    //lstInternalLicense = lstInternalLicense.Where(row =>
                                    //    (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")).ToList();

                                    lstInternalLicense = lstInternalLicense.Where(row =>
                                       (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();

                                }
                                if (status.Equals("Expiredappliedbutnotrenewed"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied"
                                    && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Expired"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Expired"
                                    ).ToList();

                                }
                                if (status.Equals("Active"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Active"
                                    ).ToList();
                                }
                                if (status.Equals("Terminate"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Terminate").ToList();
                                }
                            }
                            else
                            {
                                if (status.Equals("ApplicationOverduebutnotapplied"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
                                }
                                if (status.Equals("Expiredappliedbutnotrenewed"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Expired"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Expired").ToList();
                                }
                                if (status.Equals("Active"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                }
                                if (status.Equals("Terminate"))
                                {
                                    lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Terminate").ToList();
                                }

                            }
                        }
                        objResult.Add(new MyGraphData { Internal = lstInternalLicense, Statutory = lstAssignedLicenses });
                    }
                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }

        }

        //[Authorize]
        //[Route("License/GetGraphDetailData")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetGraphDetailData(long Userid, int Customerid, string FDte, string EDte, string IsFlag, string status, string internalStatutory, int licenseTypeID, int branchId)
        //{
        //    try
        //    {
        //        DateTime FromDate;
        //        DateTime EndDate;
        //        List<int> statusId = new List<int>();
        //        List<MyGraphData> objResult = new List<MyGraphData>();
        //        List<SP_InternalLicenseInstanceTransactionCount_Result> lstInternalLicense = new List<SP_InternalLicenseInstanceTransactionCount_Result>();
        //        List<SP_LicenseInstanceTransactionCount_Result> lstAssignedLicenses = new List<SP_LicenseInstanceTransactionCount_Result>();
        //        statusId.Add(4);
        //        statusId.Add(5);
        //        statusId.Add(7);
        //        statusId.Add(9);


        //        int deptID = -1;

        //        if (!string.IsNullOrEmpty(FDte.ToString()))
        //        {
        //            FromDate = Convert.ToDateTime(FDte);
        //        }
        //        else
        //        {
        //            FromDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        }
        //        if (!string.IsNullOrEmpty(EDte.ToString()))
        //        {
        //            EndDate = Convert.ToDateTime(EDte);
        //        }
        //        else
        //        {
        //            EndDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        }

        //        var branchList = LitigationManagement.GetAllHierarchy(Customerid, branchId);
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (internalStatutory == "S")
        //            {
        //                entities.Database.CommandTimeout = 180;
        //                lstAssignedLicenses = (entities.SP_LicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
        //                if (branchList.Count > 0)
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //                if (deptID != -1)
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.DepartmentID == deptID).ToList();

        //                if (licenseTypeID != 0 && licenseTypeID != -1)
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
        //                }

        //                if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
        //                }
        //                else if (!FromDate.ToString().Contains("1900"))
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate).ToList();
        //                }
        //                else if (!EndDate.ToString().Contains("1900"))
        //                {

        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.EndDate <= EndDate).ToList();
        //                }

        //                if (lstAssignedLicenses.Count > 0)
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.EndDate).ToList();
        //                }
        //                lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.RoleID == 3).ToList();
        //                if (!string.IsNullOrEmpty(status))
        //                {

        //                    if (IsFlag == "MGMT" || IsFlag == "CADMN")
        //                    {


        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {


        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row =>
        //                                (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();


        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired").ToList();



        //                        }
        //                        if (status.Equals("Active"))
        //                        {

        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active").ToList();
        //                        }
        //                        if (status.Equals("Terminate"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expired").ToList();
        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Terminate"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
        //                        }
        //                    }
        //                }
        //                objResult.Add(new MyGraphData { Statutory = lstAssignedLicenses, Internal = lstInternalLicense });
        //            }
        //            else
        //            {
        //                entities.Database.CommandTimeout = 180;
        //                lstInternalLicense = (entities.SP_InternalLicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
        //                if (branchList.Count > 0)
        //                    lstInternalLicense = lstInternalLicense.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //                if (deptID != -1)
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.DepartmentID == deptID).ToList();

        //                if (licenseTypeID != 0 && licenseTypeID != -1)
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
        //                }

        //                if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
        //                }
        //                else if (!FromDate.ToString().Contains("1900"))
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate).ToList();
        //                }
        //                else if (!EndDate.ToString().Contains("1900"))
        //                {

        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.EndDate <= EndDate).ToList();
        //                }

        //                if (lstInternalLicense.Count > 0)
        //                {
        //                    lstInternalLicense = lstInternalLicense.OrderBy(entry => entry.EndDate).ToList();
        //                }
        //                lstInternalLicense = lstInternalLicense.Where(entry => entry.RoleID == 3).ToList();
        //                if (!string.IsNullOrEmpty(status))
        //                {

        //                    if (IsFlag == "MGMT" || IsFlag == "CADMN")
        //                    {


        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {


        //                            lstInternalLicense = lstInternalLicense.Where(row =>
        //                               (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();

        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Expired"
        //                            ).ToList();

        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Active"
        //                            ).ToList();
        //                        }
        //                        if (status.Equals("Terminate"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Expired").ToList();
        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Terminate"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Terminate").ToList();
        //                        }

        //                    }
        //                }
        //                objResult.Add(new MyGraphData { Internal = lstInternalLicense, Statutory = lstAssignedLicenses });
        //            }
        //        }

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }

        //}

        //[Authorize]
        //[Route("License/GetGraphDetailData")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetGraphDetailData(long Userid, int Customerid, string FDte, string EDte, string IsFlag, string status, string internalStatutory, int licenseTypeID, int branchId)
        //{
        //    try
        //    {
        //        DateTime FromDate;
        //        DateTime EndDate;
        //        List<int> statusId = new List<int>();
        //        List<MyGraphData> objResult = new List<MyGraphData>();
        //        List<SP_InternalLicenseInstanceTransactionCount_Result> lstInternalLicense = new List<SP_InternalLicenseInstanceTransactionCount_Result>();
        //        List<SP_LicenseInstanceTransactionCount_Result> lstAssignedLicenses = new List<SP_LicenseInstanceTransactionCount_Result>();
        //        statusId.Add(4);
        //        statusId.Add(5);
        //        statusId.Add(7);
        //        statusId.Add(9);

        //        int deptID = -1;

        //        if (!string.IsNullOrEmpty(FDte.ToString()))
        //        {
        //            FromDate = Convert.ToDateTime(FDte);
        //        }
        //        else
        //        {
        //            FromDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        }
        //        if (!string.IsNullOrEmpty(EDte.ToString()))
        //        {
        //            EndDate = Convert.ToDateTime(EDte);
        //        }
        //        else
        //        {
        //            EndDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        }

        //        var branchList = LitigationManagement.GetAllHierarchy(Customerid, branchId);
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (internalStatutory == "S")
        //            {
        //                entities.Database.CommandTimeout = 180;
        //                lstAssignedLicenses = (entities.SP_LicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
        //                if (branchList.Count > 0)
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //                if (deptID != -1)
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.DepartmentID == deptID).ToList();

        //                if (licenseTypeID != 0 && licenseTypeID != -1)
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
        //                }

        //                if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
        //                }
        //                else if (!FromDate.ToString().Contains("1900"))
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.StartDate >= FromDate).ToList();
        //                }
        //                else if (!EndDate.ToString().Contains("1900"))
        //                {

        //                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.EndDate <= EndDate).ToList();
        //                }

        //                if (lstAssignedLicenses.Count > 0)
        //                {
        //                    lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.EndDate).ToList();
        //                }
        //                lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.RoleID == 3).ToList();
        //                if (!string.IsNullOrEmpty(status))
        //                {

        //                    if (IsFlag == "MGMT" || IsFlag == "CADMN")
        //                    {
        //                        //List<int> cstatusId = new List<int>();
        //                        //cstatusId.Add(1);
        //                        //cstatusId.Add(2);
        //                        //cstatusId.Add(3);
        //                        //cstatusId.Add(6);
        //                        //cstatusId.Add(8);
        //                        //cstatusId.Add(10);

        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            //lstAssignedLicenses = lstAssignedLicenses.Where(row =>
        //                            //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")).ToList();

        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row =>
        //                                (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();

        //                            //lstAssignedLicenses = lstAssignedLicenses.Where(row =>
        //                            //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
        //                            //&& row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
        //                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired").ToList();

        //                            //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired"
        //                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active"
        //                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active").ToList();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expired").ToList();
        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                    }
        //                }
        //                objResult.Add(new MyGraphData { Statutory = lstAssignedLicenses, Internal = lstInternalLicense });
        //            }
        //            else
        //            {
        //                entities.Database.CommandTimeout = 180;
        //                lstInternalLicense = (entities.SP_InternalLicenseInstanceTransactionCount(Userid, Customerid, IsFlag)).ToList();
        //                if (branchList.Count > 0)
        //                    lstInternalLicense = lstInternalLicense.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //                if (deptID != -1)
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.DepartmentID == deptID).ToList();

        //                if (licenseTypeID != 0 && licenseTypeID != -1)
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
        //                }

        //                if (!FromDate.ToString().Contains("1900") && !EndDate.ToString().Contains("1900"))
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate && entry.EndDate <= EndDate).ToList();
        //                }
        //                else if (!FromDate.ToString().Contains("1900"))
        //                {
        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.StartDate >= FromDate).ToList();
        //                }
        //                else if (!EndDate.ToString().Contains("1900"))
        //                {

        //                    lstInternalLicense = lstInternalLicense.Where(entry => entry.EndDate <= EndDate).ToList();
        //                }

        //                if (lstInternalLicense.Count > 0)
        //                {
        //                    lstInternalLicense = lstInternalLicense.OrderBy(entry => entry.EndDate).ToList();
        //                }
        //                lstInternalLicense = lstInternalLicense.Where(entry => entry.RoleID == 3).ToList();
        //                if (!string.IsNullOrEmpty(status))
        //                {

        //                    if (IsFlag == "MGMT" || IsFlag == "CADMN")
        //                    {
        //                        //List<int> cstatusId = new List<int>();
        //                        //cstatusId.Add(1);
        //                        //cstatusId.Add(2);
        //                        //cstatusId.Add(3);
        //                        //cstatusId.Add(6);
        //                        //cstatusId.Add(8);
        //                        //cstatusId.Add(10);

        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            //lstInternalLicense = lstInternalLicense.Where(row =>
        //                            //(row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
        //                            //&& row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
        //                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

        //                            //lstInternalLicense = lstInternalLicense.Where(row =>
        //                            //    (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")).ToList();

        //                            lstInternalLicense = lstInternalLicense.Where(row =>
        //                               (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();

        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied"
        //                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Expired"
        //                            ).ToList();

        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.MGRStatus == "Active"
        //                            ).ToList();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (status.Equals("ApplicationOverduebutnotapplied"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => (row.Status == "Expiring" || row.Status == "Active")).ToList();
        //                        }
        //                        if (status.Equals("Expiredappliedbutnotrenewed"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }
        //                        if (status.Equals("Expired"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Expired").ToList();
        //                        }
        //                        if (status.Equals("Active"))
        //                        {
        //                            lstInternalLicense = lstInternalLicense.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                        }

        //                    }
        //                }
        //                objResult.Add(new MyGraphData { Internal = lstInternalLicense, Statutory = lstAssignedLicenses });
        //            }
        //        }

        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(objResult).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }

        //}





        [Route("License/GetLicenseDocumentData")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLicenseDocumentData(int loggedInUserID, int customerID, string loggedInUserRole, string isstatutoryinternal)
        {
            try
            {
                int branchID = -1;
                long licenseStatusID = -1;
                long licenseTypeID = -1;

                List<Lic_SP_MyDocuments_All_Result> result = new List<Lic_SP_MyDocuments_All_Result>();

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                result = LicenseDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole,
                    branchList, licenseStatusID, licenseTypeID, isstatutoryinternal);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(result).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("License/StatusList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage StatusList()
        {
            try
            {
                List<Lic_tbl_StatusMaster> statusList = new List<Lic_tbl_StatusMaster>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    statusList = (from row in entities.Lic_tbl_StatusMaster
                                  where row.IsDeleted == false
                                   && row.IsVisibleToUser == true
                                  select row).ToList();

                }

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(statusList).ToString(), Encoding.UTF8, "application/json")
                };


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Authorize]
        [Route("License/LicenseTypeList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage LicenseTypeList(int userID, string role, string isstatutoryinternal)
        {
            try
            {
                List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (role.Equals("CADMN") || role.Equals("IMPT") || role.Equals("MGMT"))
                    {
                        data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(userID, role, isstatutoryinternal);
                    }
                    else
                    {
                        data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(userID, role, isstatutoryinternal);
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}
