﻿using AppWebApplication.Data;
using AppWebApplication.Models;
using com.VirtuosoITech.ComplianceManagement.Business;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using OfficeOpenXml.Drawing.Chart;
using GleamTech.DocumentUltimate;

namespace AppWebApplication.Controllers
{
    public class ExportReportController : Controller
    {

        #region Sachin 

        public ActionResult CompliancecertificatepdfNew(int UserId, int CustomerID, string MonthId, string location
, string risk, string StartDateDetail, string EndDateDetail, string DeptID, string CatID, string DdlUserID, string toUser, string fromUser)
        {
            string ReturnPath = "";
            List<Tuple<string, string, string>> FilePathList = new List<Tuple<string, string, string>>();
            try
            {

                int catid = -1;
                int deptid = -1;
                int ddluserID = -1;

                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                DateTime currentdate1 = DateTime.Now;

                DateTime currentdate = currentdate1.Date;


                if (MonthId != "All")
                {
                    if (MonthId != "All")
                    {
                        if (MonthId == "1")
                            dtfrom = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            dtfrom = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            dtfrom = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            dtfrom = DateTime.Now.AddDays(-365);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(StartDateDetail))
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(EndDateDetail))
                    {
                        dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 360;
                    bool IsTagVisible = CustomerManagement.CheckForClient(CustomerID, "FromAndToTagCertificate");
                    var ParentBrnachName = "";
                    var FromUser = "";
                    var ToUser = "";
                    var designationFrom = "";
                    var designationTo = "";
                    var deptName = "";

                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                 select row.TemplateContent).FirstOrDefault();
                    if (data1 != null)
                    {

                        var data12 = (from row in entities.SP_ComplianceCertificateAll(CustomerID, dtfrom, dtTo, UserId)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {
                            if (IsTagVisible == false)
                            {
                                //if (ddluserID != null && ddluserID != -1)
                                //{
                                //    data12 = data12.Where(x => x.UserID == ddluserID).ToList();
                                //}
                            }
                            else
                            {
                                if (fromUser != "" && fromUser != "-1")
                                {
                                    data12 = data12.Where(x => (x.PerformerID == Convert.ToInt64(fromUser)) || (x.ReviewerID == Convert.ToInt64(fromUser))).ToList();
                                }
                            }

                            if (location != null)
                            {
                                System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);

                                if (IsTagVisible == true)
                                {
                                    if (toUser != "")
                                    {
                                        ToUser = UserName(Convert.ToInt64(toUser));
                                        designationTo = designation(Convert.ToInt64(toUser));
                                    }

                                    if (fromUser != "")
                                    {
                                        FromUser = UserName(Convert.ToInt64(fromUser));
                                        designationFrom = designation(Convert.ToInt64(fromUser));
                                    }

                                    int? ParentBrnachID = 0;
                                    //Get Parent Brnach ID
                                    foreach (var item in Ids)
                                    {

                                        ParentBrnachID = (from row in entities.SP_GetParentBranchesFromChildNode(item, CustomerID)
                                                          where row.ParentID == null
                                                          select row.ID).FirstOrDefault();
                                        break;
                                    }

                                    if (ParentBrnachID != 0)
                                    {
                                        var BranchIDList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                            select row).ToList();

                                        var BranchIDListAssigned = (from row in entities.ComplianceInstances
                                                                    join row1 in entities.ComplianceAssignments
                                                                    on row.ID equals row1.ComplianceInstanceID
                                                                    join row2 in entities.Compliances
                                                                    on row.ComplianceId equals row2.ID
                                                                    where BranchIDList.Contains(row.CustomerBranchID)
                                                                    select row.CustomerBranchID).Distinct().ToList();


                                        if (Ids.Count() >= BranchIDListAssigned.Count())
                                        {
                                            ParentBrnachName = (from row in entities.SP_GetParentBranchesFromChildNode(ParentBrnachID, CustomerID)
                                                                where row.ParentID == null
                                                                select row.Name).FirstOrDefault();

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName + " - " + deptName;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var ParentBrnachNameList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                                        join row1 in entities.CustomerBranches on row equals row1.ID
                                                                        where Ids.Contains((int)row)
                                                                        select row1.Name).ToList();


                                            foreach (var item in ParentBrnachNameList)
                                            {
                                                if (ParentBrnachName == "")
                                                {
                                                    //ParentBrnachName = item + ",";
                                                    ParentBrnachName = item;
                                                }
                                                else
                                                {
                                                    ParentBrnachName = ParentBrnachName + " , " + item;
                                                }

                                            }
                                            ParentBrnachName = ParentBrnachName.Trim(',');

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName +" - " + deptName;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (DeptID != null)
                                        {
                                            deptName = GetDeptName(DeptID);
                                            ParentBrnachName = deptName;
                                        }
                                    }
                                }

                                if (Ids.Count != 0)
                                {
                                    data12 = data12.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                                }
                            }
                            if (DdlUserID != null)
                            {
                                System.Collections.Generic.IList<long?> userIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<long?>>(DdlUserID);
                                if (userIds.Count != 0)
                                {
                                    data12 = data12.Where(x => (x.PerformerID != null && userIds.Contains((long)x.PerformerID)) || (x.ReviewerID != null && userIds.Contains((long)x.ReviewerID)) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();

                                    //data12 = data12.Where(x => userIds.Contains((long)x.PerformerID) || userIds.Contains((long)x.ReviewerID) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();
                                }
                            }
                            if (CatID != null)
                            {
                                System.Collections.Generic.IList<int> catIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(CatID);
                                if (catIds.Count != 0)
                                {
                                    data12 = data12.Where(x => catIds.Contains(x.CategoryID)).ToList();
                                }
                            }
                            if (DeptID != null)
                            {
                                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                                if (deptIds.Count != 0)
                                {
                                    data12 = data12.Where(x => deptIds.Contains((int?)x.DeptID)).ToList();
                                     
                                }
                            }
                            if (risk != null)
                            {
                                System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                                if (riskIds.Count != 0)
                                {
                                    data12 = data12.Where(x => riskIds.Contains((int)x.risk)).ToList();
                                }
                            }


                            #region Complied Act List

                            List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.ActName).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List

                            List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x => x.ActName).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();

                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Short Description");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                //strExportforComplied.Append(@"<Td>");
                                //strExportforComplied.Append(@"Period");
                                //strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DivisionVertical);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    //strExportforComplied.Append(@"<Td>");
                                    //strExportforComplied.Append(eachSection.Formonth);
                                    //strExportforComplied.Append(@"</Td>"); 
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"ComplianceID");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Period");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ComplianceID);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Formonth);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied
                            var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Short Description");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                //strExportforNonComplied.Append(@"<Td>");
                                //strExportforNonComplied.Append(@"Period");
                                //strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DivisionVertical);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    //strExportforNonComplied.Append(@"<Td>");
                                    //strExportforNonComplied.Append(eachSection.Formonth);
                                    //strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"ComplianceID");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Period");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ComplianceID);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Formonth);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion

                            }
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = "";
                            if (EndDateDetail != null)
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                            }
                            else
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                            }
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = "";
                            if (EndDateDetail != null)
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                            }
                            else
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                            }
                            string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());


                            //bool IsTagVisible = CustomerManagement.CheckForClient(CustomerID, "FromAndToTagCertificate");
                            string replacebranchList = "";
                            string replaceFromUserList = "";
                            string replaceToUserList = "";
                            string replaceToDesig = "";
                            string replaceFromDesig = "";
                            if (IsTagVisible == true)
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", ParentBrnachName);
                                replaceToUserList = replacebranchList.Replace("{{ToUser}}", ToUser);
                                replaceFromUserList = replaceToUserList.Replace("{{FromUser}}", FromUser);
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", designationTo);
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", designationFrom);
                            }
                            else
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", " ");
                                replaceFromUserList = replacebranchList.Replace("{{ToUser}}", " ");
                                replaceToUserList = replaceFromUserList.Replace("{{FromUser}}", " ");
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", " ");
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", " ");
                            }

                            string relacebranch = replaceFromDesig.Replace("{{BranchName}}", BranchName);
                            string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));
                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");

                            string strFileName = "ComplianceCertificate";
                            string reportNamepdf = string.Empty;
                            string reportNamedoc = string.Empty;
                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string dirpath = string.Empty;
                            dirpath = storagedrive + "/" + DateTime.Today.Date.ToString("ddMMMyyyy");
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }

                            reportNamedoc = strFileName + "_" + username.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                            reportNamepdf = strFileName + "_" + username.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".pdf";
                            //var fileName = dirpath + "\\" + reportNamedoc;
                            //var fileNamepdf = dirpath + "\\" + reportNamepdf;

                            var fileName = storagedrive + "\\" + reportNamedoc;
                            var fileNamepdf = storagedrive + "\\" + reportNamepdf;

                            byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());

                            FileStream objFileStrm = System.IO.File.Create(fileName);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(fileName, buffer);

                            FilePathList.Add(new Tuple<string, string, string>(fileName, reportNamepdf, fileNamepdf));


                            foreach (Tuple<string, string, string> file1 in FilePathList)
                            {
                                if (DocumentConverter.CanConvert(file1.Item1, DocumentFormat.Pdf))
                                {
                                    var aa = DocumentConverter.Convert(file1.Item1, file1.Item3, DocumentFormat.Pdf);
                                }
                            }
                            ReturnPath = reportNamepdf;
                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }

        #endregion


        protected bool NewColumnsReturn;
        protected bool NewEntityColumn;
        protected bool ComplianceCC;
        public ActionResult ExportReportRequest(int UserId, int CustomerID, string StatusFlag, string FlagIsApp, string MonthId, string FY, string CustomerName, string location
        , string risk, string status, string userDetail, string actDetail, string StartDateDetail, string EndDateDetail
        , string EventName, string EventNature, string SequenceID, bool IsEmailRequestFlag)
        {
            string msg = "False";
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //ExportDetailReportRequest output = (from row in entities.ExportDetailReportRequests
                    //                                  where row.UserID == UserId
                    //                                  && row.CreatedOn == DateTime.Now.Date
                    //                                  select row).FirstOrDefault();
                    //if (output == null)
                    //{
                    var data = ComplianceManagement.CheckExportReportRequest(UserId, CustomerID, FlagIsApp);
                    //var data = ComplianceManagement.CheckExportReportRequest(UserId, CustomerID);

                    if (data == null)
                    {
                        ExportDetailReportRequest obj1 = new ExportDetailReportRequest();
                        obj1.UserID = UserId;
                        obj1.CustID = CustomerID;
                        obj1.RoleFlag = FlagIsApp;
                        obj1.location = location;
                        obj1.risk = risk;
                        obj1.status = status;
                        obj1.userDetail = userDetail;
                        obj1.actDetail = actDetail;
                        obj1.EventName = EventName;
                        obj1.EventNature = EventNature;
                        obj1.SequenceID = SequenceID;
                        obj1.ComplianceType = Convert.ToString(StatusFlag);
                        obj1.IsEmailRequestFlag = IsEmailRequestFlag;
                        obj1.IsEmailSendFlag = false;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.IsActive = true;
                        entities.ExportDetailReportRequests.Add(obj1);
                        entities.SaveChanges();
                    }
                    else
                    {
                        ComplianceManagement.UpdateExportDetailReportRequests(data);

                        ExportDetailReportRequest obj1 = new ExportDetailReportRequest();
                        obj1.UserID = UserId;
                        obj1.CustID = CustomerID;
                        obj1.RoleFlag = FlagIsApp;
                        obj1.location = location;
                        obj1.risk = risk;
                        obj1.status = status;
                        obj1.userDetail = userDetail;
                        obj1.actDetail = actDetail;
                        obj1.EventName = EventName;
                        obj1.EventNature = EventNature;
                        obj1.SequenceID = SequenceID;
                        obj1.ComplianceType = Convert.ToString(StatusFlag);
                        obj1.IsEmailRequestFlag = IsEmailRequestFlag;
                        obj1.IsEmailSendFlag = false;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.IsActive = true;
                        entities.ExportDetailReportRequests.Add(obj1);
                        entities.SaveChanges();
                    }
                }
                return Content(msg);
            }
            catch (Exception ex)
            {
                return Content(msg);
            }
        }


        public ActionResult GetContractTermSheetCommentedFile(string userpath)
        {
            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["DownloadContractInitiatorFile"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename);
            Response.BinaryWrite(DocumentManagement.ReadDocFiles(p_strPath)); // create the file
            Response.Flush(); // send it to the client to download
            HttpContext.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            return View();
        }

        public ActionResult Report(int UserId, int CustomerID, int StatusFlag, string FlagIsApp, string MonthId, string FY, string CustomerName, string location
         , string risk, string status, string userDetail, string actDetail, string StartDateDetail, string EndDateDetail
         , string EventName, string EventNature, string SequenceID)
        {

            try
            {
                string ReturnPath = "";

                DateTime Fromdate = new DateTime();
                DateTime Todate = new DateTime();

                int flagDate = 0;
                int AflagDate = 0;
                DateTime AFromdate = new DateTime();
                DateTime ATodate = new DateTime();

                List<MyReportResult> objResult = new List<MyReportResult>();

                List<SP_Kendo_GetALLStatutoryEBData_Result> objTaskCompDocument = new List<SP_Kendo_GetALLStatutoryEBData_Result>();
                //List<InternalComplianceInstanceTransactionViewNew> objInternal = new List<InternalComplianceInstanceTransactionViewNew>();
                List<SP_Kendo_GetALLInternalEBData_Result> objInternal = new List<SP_Kendo_GetALLInternalEBData_Result>();
                List<SP_kendo_getInternalChecklist_Result> ObjInternalChecklist = new List<SP_kendo_getInternalChecklist_Result>();
                //List<InternalComplianceInstanceCheckListTransactionViewNew> ObjInternalChecklist = new List<InternalComplianceInstanceCheckListTransactionViewNew>();
                List<SP_kendo_getSChecklist_Result> objStatutoryChecklist = new List<SP_kendo_getSChecklist_Result>();
                int customizedid = GetCustomizedCustomerid(CustomerID);
                bool BankNewFields = LitigationDocumentManagement.CheckForClientNew(Convert.ToInt32(CustomerID), "BankDetailsColumn");
                bool RemoveColumn = LitigationDocumentManagement.CheckForClientNew(Convert.ToInt32(CustomerID), "RemovecolumnDeReport");

                if (FY != "0" && !String.IsNullOrEmpty(FY))
                {
                    string[] y = FY.Split('-');
                    if (y.Length > 1)
                    {
                        if (!String.IsNullOrEmpty(y[0]) && !String.IsNullOrEmpty(y[1]))
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + Convert.ToInt32(y[0]));
                            string dtto = Convert.ToString("31" + "-03" + "-" + Convert.ToInt32(y[1]));
                            Fromdate = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Todate = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            flagDate = 1;
                        }
                    }
                }

                else
                {
                    if (MonthId != "All")
                    {
                        flagDate = 1;
                        Todate = DateTime.Now;
                        if (MonthId == "1")
                            Fromdate = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            Fromdate = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            Fromdate = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            Fromdate = DateTime.Now.AddDays(-365);
                    }
                }

                if (FlagIsApp.Equals("AUD"))
                {
                    AflagDate = 1;
                    FlagIsApp = "MGMT";
                    User uobj = UserManagement.GetByID(UserId);
                    if (uobj != null)
                    {
                        AFromdate = Convert.ToDateTime(uobj.AuditStartPeriod);
                        ATodate = Convert.ToDateTime(uobj.AuditEndPeriod);
                    }
                }

                if (StatusFlag == -1 || StatusFlag == 1)
                {
                    if (StatusFlag == -1)
                    {
                        objTaskCompDocument = CannedReportManagement.GetCannedReportDatastatutoryEventBased(CustomerID, UserId, FlagIsApp, "Statutory");
                    }
                    else if (StatusFlag == 1)
                    {
                        objTaskCompDocument = CannedReportManagement.GetCannedReportDatastatutoryEventBased(CustomerID, UserId, FlagIsApp, "Event Based");
                    }

                    if (flagDate == 1)
                    {
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }

                    #region filters
                    if (location != null)
                    {
                        System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                        if (Ids.Count != 0)
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                        }
                    }
                    if (risk != null)
                    {
                        System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                        if (riskIds.Count != 0)
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                        }
                    }
                    if (status != null)
                    {
                        System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                        if (statusIds.Count != 0)
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(x => statusIds.Contains(x.Status)).ToList();
                        }
                    }
                    if (userDetail != null)
                    {
                        System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                        if (usersIds.Count != 0)
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                            //objTaskCompDocument = objTaskCompDocument.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                        }
                    }
                    if (actDetail != null)
                    {
                        System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                        if (atcsIds.Count != 0)
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                        }
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EventName))
                    {
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.EventID == Convert.ToInt64(EventName))).ToList();
                    }
                    if (!string.IsNullOrEmpty(EventNature))
                    {
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.EventScheduleOnID == Convert.ToInt64(EventNature))).ToList();
                    }
                    if (!string.IsNullOrEmpty(SequenceID))
                    {
                        objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                    }

                    #endregion

                    if (objTaskCompDocument.Count > 0)
                    {

                        string ReportName = string.Empty;
                        if (StatusFlag == -1)
                        {
                            ReportName = "Statutory";
                        }
                        if (StatusFlag == 1)
                        {
                            ReportName = "Event Based";
                        }

                        NewColumnsReturn = LitigationDocumentManagement.CheckForClientNew(Convert.ToInt32(CustomerID), "ReturnAmount");
                        NewEntityColumn = LitigationDocumentManagement.CheckForClientNew(Convert.ToInt32(CustomerID), "EntityDetReport");
                        string ReportNameAdd = "Report of " + ReportName + " Compliances";
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                            System.Data.DataTable ExcelData1 = null;
                            DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objTaskCompDocument);
                            DataView view1 = new System.Data.DataView(dataToExport);

                            ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "ParentName", "Branch", "ActName", "Sections", "ComCategoryName", "ComSubTypeName", "ShortForm", "PenaltyDescription", "ShortDescription", "Description", "ComplianceType", "DepartmentName", "EventName", "EventNature", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "ReviewerDated", "OriginalPerformerName", "OriginalReviewerName", "PerformerRemark", "ReviewerRemark", "SequenceID", "ChallanNo", "ChallanAmount", "BankName", "Challanpaiddate", "GSTNumber", "Penalty", "Reviseduedate", "ValuesAsPerReturn");

                            if (StatusFlag == -1)
                            {
                                dataToExport.Columns.Remove("EventName");
                                dataToExport.Columns.Remove("EventNature");

                            }
                            if (StatusFlag == 1)
                            {
                                dataToExport.Columns.Remove("Reviseduedate");
                            }
                            if (NewEntityColumn == false)
                            {
                                dataToExport.Columns.Remove("ParentName");
                            }
                            if (BankNewFields != true)
                            {
                                dataToExport.Columns.Remove("ChallanNo");
                                dataToExport.Columns.Remove("ChallanAmount");
                                dataToExport.Columns.Remove("BankName");
                                dataToExport.Columns.Remove("Challanpaiddate");
                            }
                            if (RemoveColumn == true)
                            {
                                dataToExport.Columns.Remove("ApproverName");
                                dataToExport.Columns.Remove("SequenceID");

                            }
                            exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Value = CustomerName;

                            exWorkSheet1.Cells["A2"].Value = "Report Name:";
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                            exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Cells["B4"].Value = "Entity";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["C4"].Value = "Location";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C4"].AutoFitColumns(50);


                            exWorkSheet1.Cells["D4"].Value = "Act";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["E4"].Value = "Sections";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["F4"].Value = "Category Name";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["G4"].Value = "Sub Category Name";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["H4"].Value = "Short Form";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["I4"].Value = "Penalty Description";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["J4"].Value = "Short Description";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["K4"].Value = "Detail Description";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["L4"].Value = "ComplianceType";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["M4"].Value = "DepartmentName";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["N4"].Value = "EventName";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["O4"].Value = "EventNature";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["P4"].Value = "Period";
                            exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["P4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["P4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Q4"].Value = "Due Date";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["Q4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["R4"].Value = "Close Date";
                            exWorkSheet1.Cells["R4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["R4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["S4"].Value = "Status";
                            exWorkSheet1.Cells["S4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["T4"].Value = "Risk";
                            exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["T4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["U4"].Value = "Performer";
                            exWorkSheet1.Cells["U4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["V4"].Value = "Reviewer";
                            exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["V4"].AutoFitColumns(50);
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["W4"].Value = "";
                                exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["W4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["W4"].Value = "Approver";
                                exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["W4"].AutoFitColumns(50);
                            }

                            if (customizedid == CustomerID)
                            {
                                exWorkSheet1.Cells["X4"].Value = "Actual Date of Performer";
                                exWorkSheet1.Cells["X4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["X4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["X4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["Y4"].Value = "Actual Date of Reviewer";
                                exWorkSheet1.Cells["Y4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["Y4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["Y4"].AutoFitColumns(50);
                            }
                            else
                            {
                                exWorkSheet1.Cells["X4"].Value = "Actual Close Date Performer";
                                exWorkSheet1.Cells["X4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["X4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["X4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["Y4"].Value = "Actual Close Date Reviewer";
                                exWorkSheet1.Cells["Y4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["Y4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["Y4"].AutoFitColumns(50);
                            }


                            //exWorkSheet1.Cells["Y4"].Value = "";
                            //exWorkSheet1.Cells["Y4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["Y4"].Style.Font.Size = 12;
                            //exWorkSheet1.Cells["Y4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Z4"].Value = "Original Performer Name";
                            exWorkSheet1.Cells["Z4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Z4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Z4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["AA4"].Value = "Original Reviewer Name";
                            exWorkSheet1.Cells["AA4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AA4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AA4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["AB4"].Value = "PerformerRemark";
                            exWorkSheet1.Cells["AB4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AB4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AB4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["AC4"].Value = "ReviewerRemark";
                            exWorkSheet1.Cells["AC4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AC4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AC4"].AutoFitColumns(50);
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["AD4"].Value = "";
                                exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AD4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["AD4"].Value = "Label";
                                exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AD4"].AutoFitColumns(30);
                            }


                            if (BankNewFields == true)
                            {


                                //exWorkSheet1.Cells["AD4"].Value = "";
                                //exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                                //exWorkSheet1.Cells["AD4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AE4"].Value = "Challan No";
                                exWorkSheet1.Cells["AE4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AE4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AE4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AF4"].Value = "Challan Amount";
                                exWorkSheet1.Cells["AF4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AF4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AF4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AG4"].Value = "Bank Name";
                                exWorkSheet1.Cells["AG4"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["AG4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["AG4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AG4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AH4"].Value = "Challan Paid Date";
                                exWorkSheet1.Cells["AH4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AH4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["AH4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AH4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AI4"].Value = "GST No.";
                                exWorkSheet1.Cells["AI4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AI4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AI4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AJ4"].Value = "Penalty";
                                exWorkSheet1.Cells["AJ4"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["AJ4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["AJ4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AJ4"].AutoFitColumns(30);

                            }
                            exWorkSheet1.Cells["AK4"].Value = "Extended Date";
                            exWorkSheet1.Cells["AK4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AK4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["AK4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AK4"].AutoFitColumns(30);

                            if (NewColumnsReturn == true)
                            {
                                exWorkSheet1.Cells["AL4"].Value = "Return Amount";
                                exWorkSheet1.Cells["AL4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AL4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AL4"].AutoFitColumns(30);


                            }

                            //exWorkSheet1.Cells["AK4"].Value = "Parent Name";
                            //exWorkSheet1.Cells["AK4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["AK4"].Style.Font.Size = 12;
                            //exWorkSheet1.Cells["AK4"].AutoFitColumns(30);




                            exWorkSheet1.Column(1).Width = 21;
                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Column(2).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(2).Width = 0;
                            }
                            //exWorkSheet1.Column(2).Width = 25;
                            exWorkSheet1.Column(3).Width = 40;
                            exWorkSheet1.Column(4).Width = 20;
                            exWorkSheet1.Column(5).Width = 50;
                            exWorkSheet1.Column(6).Width = 50;
                            exWorkSheet1.Column(7).Width = 50;
                            exWorkSheet1.Column(8).Width = 50;
                            exWorkSheet1.Column(9).Width = 50;
                            exWorkSheet1.Column(10).Width = 20;
                            exWorkSheet1.Column(11).Width = 20;
                            exWorkSheet1.Column(12).Width = 20;


                            if (StatusFlag == 1)
                            {
                                exWorkSheet1.Column(13).Width = 20;
                                exWorkSheet1.Column(14).Width = 30;
                                exWorkSheet1.Column(36).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(13).Width = 0;
                                exWorkSheet1.Column(14).Width = 0;
                                exWorkSheet1.Column(36).Width = 30;
                            }


                            exWorkSheet1.Column(15).Width = 30;
                            exWorkSheet1.Column(16).Width = 30;
                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 30;
                            exWorkSheet1.Column(19).Width = 30;
                            exWorkSheet1.Column(20).Width = 30;
                            exWorkSheet1.Column(21).Width = 30;
                            exWorkSheet1.Column(22).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(23).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(23).Width = 30;
                            }

                            exWorkSheet1.Column(24).Width = 30;
                            exWorkSheet1.Column(25).Width = 30;
                            exWorkSheet1.Column(26).Width = 30;
                            exWorkSheet1.Column(27).Width = 30;
                            exWorkSheet1.Column(28).Width = 30;
                            exWorkSheet1.Column(29).Width = 20;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(30).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(30).Width = 30;
                            }
                            if (BankNewFields != true)
                            {

                                exWorkSheet1.Column(31).Width = 0;
                                exWorkSheet1.Column(32).Width = 0;
                                exWorkSheet1.Column(33).Width = 0;
                                exWorkSheet1.Column(34).Width = 0;
                                exWorkSheet1.Column(35).Width = 0;

                            }
                            else
                            {

                                exWorkSheet1.Column(31).Width = 30;
                                exWorkSheet1.Column(32).Width = 30;
                                exWorkSheet1.Column(33).Width = 30;
                                exWorkSheet1.Column(34).Width = 30;
                                exWorkSheet1.Column(35).Width = 30;
                                exWorkSheet1.Column(36).Width = 30;
                                exWorkSheet1.Column(37).Width = 30;
                                exWorkSheet1.Column(38).Width = 30;
                            }


                            for (int i = 4; i <= ExcelData1.Rows.Count + 4; i++)
                            {
                                string DueDateCell = "O" + i;
                                exWorkSheet1.Cells[DueDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ClosedDateCell = "P" + i;
                                exWorkSheet1.Cells[ClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string DateCell = "Q" + i;
                                exWorkSheet1.Cells[DateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string PerformerDated = "W" + i;
                                exWorkSheet1.Cells[PerformerDated].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ReviewerDated = "X" + i;
                                exWorkSheet1.Cells[ReviewerDated].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string Challanpaiddate = "AG" + i;
                                exWorkSheet1.Cells[Challanpaiddate].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ExtendedDate = "AJ" + i;
                                exWorkSheet1.Cells[ExtendedDate].Style.Numberformat.Format = "dd-MMM-yyyy";



                            }

                            using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 38])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            //Byte[] fileBytes = exportPackge.GetAsByteArray();
                            //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");

                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                            ReturnPath = reportName;
                        }
                    }
                    else
                    {
                        ReturnPath = "No Record Found";
                    }
                }


                else if (StatusFlag == 2 || StatusFlag == 4)
                {
                    objStatutoryChecklist = CannedReportManagement.GetCannedReportDatastatutoryChecklist(CustomerID, UserId, FlagIsApp, StatusFlag);
                    if (flagDate == 1)
                    {
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }

                    #region filters
                    if (location != null)
                    {
                        System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                        if (Ids.Count != 0)
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                        }
                    }
                    if (risk != null)
                    {
                        System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                        if (riskIds.Count != 0)
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                        }
                    }
                    if (status != null)
                    {
                        System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                        if (statusIds.Count != 0)
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(x => statusIds.Contains(x.Status)).ToList();
                        }
                    }
                    if (userDetail != null)
                    {
                        System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                        if (usersIds.Count != 0)
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                            // objStatutoryChecklist = objStatutoryChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                        }
                    }
                    if (actDetail != null)
                    {
                        System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                        if (atcsIds.Count != 0)
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                        }
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EventName))
                    {
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.EventID == Convert.ToInt64(EventName))).ToList();
                    }
                    if (!string.IsNullOrEmpty(EventNature))
                    {
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.EventScheduleOnID == Convert.ToInt64(EventNature))).ToList();
                    }
                    if (!string.IsNullOrEmpty(SequenceID))
                    {
                        objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                    }
                    #endregion

                    if (objStatutoryChecklist.Count > 0)
                    {
                        string ReportName = string.Empty;
                        if (StatusFlag == 2)
                        {
                            ReportName = "Statutory Checklist";
                        }
                        if (StatusFlag == 4)
                        {
                            ReportName = "Event Based Checklist";
                        }
                        string ReportNameAdd = "Report of " + ReportName + " Compliances";
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                            System.Data.DataTable ExcelData1 = null;
                            DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objStatutoryChecklist);
                            DataView view1 = new System.Data.DataView(dataToExport);
                            //ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "ActName", "Sections", "ComCategoryName", "ShortForm", "PenaltyDescription", "ShortDescription", "Description", "ComplianceType", "DepartmentName", "EventName", "EventNature", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "OriginalPerformerName", "OriginalReviewerName", "Remarks", "SequenceID", "ChallanNo", "ChallanAmount", "BankName", "Challanpaiddate");
                            ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "ParentName", "Branch", "ActName", "Sections", "ComCategoryName", "ShortForm", "PenaltyDescription", "ShortDescription", "Description", "ComplianceType", "DepartmentName", "EventName", "EventNature", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "OriginalPerformerName", "OriginalReviewerName", "Remarks", "SequenceID");
                            if (StatusFlag == 2)
                            {
                                dataToExport.Columns.Remove("EventName");
                                dataToExport.Columns.Remove("EventNature");

                            }
                            if (RemoveColumn == true)
                            {
                                dataToExport.Columns.Remove("ApproverName");
                                dataToExport.Columns.Remove("SequenceID");
                            }
                            if (NewEntityColumn == false)
                            {
                                dataToExport.Columns.Remove("ParentName");
                            }
                            exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Value = CustomerName;

                            exWorkSheet1.Cells["A2"].Value = "Report Name:";
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                            exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Cells["B4"].Value = "Entity";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["C4"].Value = "Location";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["D4"].Value = "Act";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["E4"].Value = "Section";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["F4"].Value = "Category Name";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["G4"].Value = "Short Form";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["H4"].Value = "Penalty Description";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["I4"].Value = "Short Description";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["J4"].Value = "Detail Description";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["K4"].Value = "ComplianceType";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["L4"].Value = "Department Name";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["M4"].Value = "Event Name";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["N4"].Value = " Event Nature";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["N4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["N4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["O4"].Value = "Period";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["O4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["O4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O4"].AutoFitColumns(50);


                            exWorkSheet1.Cells["P4"].Value = "Due Date";
                            exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["P4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Q4"].Value = "Close Date";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["Q4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["R4"].Value = "Status";
                            exWorkSheet1.Cells["R4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["S4"].Value = "Risk";
                            exWorkSheet1.Cells["S4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["T4"].Value = "Performer";
                            exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["T4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["U4"].Value = "Reviewer";
                            exWorkSheet1.Cells["U4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U4"].AutoFitColumns(50);

                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["V4"].Value = "Approver";
                                exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["V4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["V4"].Value = "";
                                exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["V4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["W4"].Value = "Actual Close Date Performer";
                            exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["W4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["X4"].Value = "Original Performer Name";
                            exWorkSheet1.Cells["X4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["X4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["X4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Y4"].Value = "Original Reviewer Name";
                            exWorkSheet1.Cells["Y4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Y4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Y4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Z4"].Value = "Remark";
                            exWorkSheet1.Cells["Z4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Z4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Z4"].AutoFitColumns(50);

                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["AA4"].Value = "";
                                exWorkSheet1.Cells["AA4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AA4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AA4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["AA4"].Value = "Label";
                                exWorkSheet1.Cells["AA4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AA4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AA4"].AutoFitColumns(50);
                            }




                            //if (customizedid == CustomerID)
                            //{


                            //    exWorkSheet1.Cells["AA4"].Value = "Challan No";
                            //    exWorkSheet1.Cells["AA4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AA4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AA4"].AutoFitColumns(30);

                            //    exWorkSheet1.Cells["AB4"].Value = "Challan Amount";
                            //    exWorkSheet1.Cells["AB4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AB4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AB4"].AutoFitColumns(30);

                            //    exWorkSheet1.Cells["AC4"].Value = "Bank Name";
                            //    exWorkSheet1.Cells["AC4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AC4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AC4"].AutoFitColumns(30);

                            //    exWorkSheet1.Cells["AD4"].Value = "Challan Paid Date";
                            //    exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AD4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            //    exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AD4"].AutoFitColumns(30);

                            //   }
                            exWorkSheet1.Column(1).Width = 21;
                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Column(2).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(2).Width = 0;
                            }
                            //exWorkSheet1.Column(2).Width = 25;
                            exWorkSheet1.Column(3).Width = 30;
                            exWorkSheet1.Column(4).Width = 30;
                            exWorkSheet1.Column(5).Width = 30;
                            exWorkSheet1.Column(6).Width = 45;
                            exWorkSheet1.Column(7).Width = 45;
                            exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Column(9).Width = 20;
                            exWorkSheet1.Column(10).Width = 20;
                            exWorkSheet1.Column(11).Width = 20;
                            if (StatusFlag == 1)
                            {
                                exWorkSheet1.Column(12).Width = 25;
                                exWorkSheet1.Column(13).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(12).Width = 0;
                                exWorkSheet1.Column(13).Width = 0;
                            }

                            exWorkSheet1.Column(14).Width = 25;
                            exWorkSheet1.Column(15).Width = 30;
                            exWorkSheet1.Column(16).Width = 30;
                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 30;
                            exWorkSheet1.Column(19).Width = 30;
                            exWorkSheet1.Column(20).Width = 30;
                            exWorkSheet1.Column(21).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(22).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(22).Width = 30;
                            }

                            exWorkSheet1.Column(23).Width = 30;
                            exWorkSheet1.Column(24).Width = 30;
                            exWorkSheet1.Column(25).Width = 30;
                            exWorkSheet1.Column(26).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(27).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(27).Width = 30;
                            }

                            exWorkSheet1.Column(28).Width = 30;
                            exWorkSheet1.Column(29).Width = 30;
                            exWorkSheet1.Column(30).Width = 30;
                            exWorkSheet1.Column(31).Width = 30;
                            for (int i = 4; i <= ExcelData1.Rows.Count + 4; i++)
                            {
                                string DueDateCell = "P" + i;
                                exWorkSheet1.Cells[DueDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ClosedDateCell = "Q" + i;
                                exWorkSheet1.Cells[ClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ActualClosedDateCell = "W" + i;
                                exWorkSheet1.Cells[ActualClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string Challanpaiddate = "AD" + i;
                                exWorkSheet1.Cells[Challanpaiddate].Style.Numberformat.Format = "dd-MMM-yyyy";
                            }

                            using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 31])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            //Byte[] fileBytes = exportPackge.GetAsByteArray();
                            //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");

                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                            //reportName = ReportNameAdd + DateTime.Today.Date.ToString("ddMMMyyyyhhmmtt") + ".xlsx";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                            ReturnPath = reportName;
                        }
                    }
                    else
                    {
                        ReturnPath = "No Record Found";
                    }
                }


                else if (StatusFlag == 0)
                {
                    objInternal = InternalCanned_ReportManagement.GetMyReportDataInternal(CustomerID, UserId, FlagIsApp, "Internal");
                    if (flagDate == 1)
                    {
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }
                    #region filters
                    if (location != null)
                    {
                        System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                        if (Ids.Count != 0)
                        {
                            objInternal = objInternal.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                        }
                    }
                    if (risk != null)
                    {
                        System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                        if (riskIds.Count != 0)
                        {
                            objInternal = objInternal.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                        }
                    }
                    if (status != null)
                    {
                        System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                        if (statusIds.Count != 0)
                        {
                            objInternal = objInternal.Where(x => statusIds.Contains(x.Status)).ToList();
                        }
                    }
                    if (userDetail != null)
                    {
                        System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                        if (usersIds.Count != 0)
                        {
                            objInternal = objInternal.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                            //objInternal = objInternal.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                        }
                    }
                    if (actDetail != null)
                    {
                        System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                        if (atcsIds.Count != 0)
                        {
                            objInternal = objInternal.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                        }
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(SequenceID))
                    {
                        objInternal = objInternal.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                    }
                    #endregion

                    if (objInternal.Count > 0)
                    {
                        string ReportNameAdd = "Report of Internal Compliances";
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                            System.Data.DataTable ExcelData1 = null;
                            DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objInternal);
                            DataView view1 = new System.Data.DataView(dataToExport);
                            ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "ParentName", "Branch", "ComCategoryName", "ShortDescription", "DetailedDescription", "ComplianceType", "DepartmentName", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "ReviewerDated", "OriginalPerformerName", "OriginalReviewerName", "PerformerRemark", "ReviewerRemark", "SequenceID", "Reviseduedate");

                            if (NewEntityColumn == false)
                            {
                                dataToExport.Columns.Remove("ParentName");
                            }
                            if (RemoveColumn == true)
                            {
                                dataToExport.Columns.Remove("ApproverName");
                            }

                            exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Value = CustomerName;

                            exWorkSheet1.Cells["A2"].Value = "Report Name:";
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                            exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A4"].Value = "Internal Compliance ID";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Cells["B4"].Value = "Entity";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["C4"].Value = "Location";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["D4"].Value = "Category Name";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["E4"].Value = "Short Description";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["F4"].Value = "Detailed Description";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["G4"].Value = "Compliance Type";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["H4"].Value = "Department Name";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["I4"].Value = "Period";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["J4"].Value = "Due Date";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["K4"].Value = "Close Date";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["L4"].Value = "Status";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["M4"].Value = "Risk";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["N4"].Value = "Performer";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["O4"].Value = "Reviewer";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O4"].AutoFitColumns(50);

                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["P4"].Value = "";
                                exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["P4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["P4"].Value = "Approver";
                                exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["P4"].AutoFitColumns(30);
                            }


                            exWorkSheet1.Cells["Q4"].Value = "Actual Close Date Performer";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["R4"].Value = "Actual Close Date Reviewer";
                            exWorkSheet1.Cells["R4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["S4"].Value = "Original Performer Name";
                            exWorkSheet1.Cells["S4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["T4"].Value = "Original Reviewer Name";
                            exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["T4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["U4"].Value = "PerformerRemark";
                            exWorkSheet1.Cells["U4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["V4"].Value = "ReviewerRemark";
                            exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["V4"].AutoFitColumns(50);

                            //if (NewColumnsExtendedDuedate == true)
                            //{
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["W4"].Value = "";
                                exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["W4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["W4"].Value = "Label";
                                exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["W4"].AutoFitColumns(30);
                            }



                            exWorkSheet1.Cells["X4"].Value = "Extended Due date";
                            exWorkSheet1.Cells["X4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["X4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["X4"].AutoFitColumns(30);
                            //}
                            //else
                            //{
                            //    exWorkSheet1.Cells["AJ4"].Value = " ";
                            //    exWorkSheet1.Cells["AJ4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AJ4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AJ4"].AutoFitColumns(30);
                            //}



                            exWorkSheet1.Column(1).Width = 25;
                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Column(2).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(2).Width = 0;
                            }
                            //exWorkSheet1.Column(2).Width = 25;
                            exWorkSheet1.Column(3).Width = 30;
                            exWorkSheet1.Column(4).Width = 45;
                            exWorkSheet1.Column(5).Width = 45;
                            exWorkSheet1.Column(6).Width = 25;
                            exWorkSheet1.Column(7).Width = 25;
                            exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Column(9).Width = 20;
                            exWorkSheet1.Column(10).Width = 20;
                            exWorkSheet1.Column(11).Width = 20;
                            exWorkSheet1.Column(12).Width = 45;
                            exWorkSheet1.Column(13).Width = 45;
                            exWorkSheet1.Column(14).Width = 45;
                            exWorkSheet1.Column(15).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(16).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(16).Width = 30;
                            }

                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 30;
                            exWorkSheet1.Column(19).Width = 30;
                            exWorkSheet1.Column(20).Width = 30;
                            exWorkSheet1.Column(21).Width = 30;
                            exWorkSheet1.Column(22).Width = 30;
                            exWorkSheet1.Column(23).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(24).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(24).Width = 30;
                            }

                            exWorkSheet1.Column(25).Width = 30;

                            for (int i = 4; i <= ExcelData1.Rows.Count + 4; i++)
                            {
                                string Cell = "J" + i;
                                exWorkSheet1.Cells[Cell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string DueDateCell = "I" + i;
                                exWorkSheet1.Cells[DueDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ClosedDateCell = "K" + i;
                                exWorkSheet1.Cells[ClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string PerfomrerDateCell = "Q" + i;
                                exWorkSheet1.Cells[PerfomrerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ReviewerDateCell = "R" + i;
                                exWorkSheet1.Cells[ReviewerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string Reviseduedatecell = "X" + i;
                                exWorkSheet1.Cells[Reviseduedatecell].Style.Numberformat.Format = "dd-MMM-yyyy";
                            }

                            using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 25])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            //Byte[] fileBytes = exportPackge.GetAsByteArray();
                            //return File(fileBytes, "application/vnd.ms-excel", "Internal.xlsx");

                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                            //reportName = ReportNameAdd + DateTime.Today.Date.ToString("ddMMMyyyyhhmmtt") + ".xlsx";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                            ReturnPath = reportName;
                        }
                    }
                    else
                    {
                        ReturnPath = "No Record Found";
                    }

                }

                else if (StatusFlag == 3)
                {
                    ObjInternalChecklist = InternalCanned_ReportManagement.GetMyReportDataInternalChecklist(CustomerID, UserId, FlagIsApp, 3);
                    if (flagDate == 1)
                    {
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }
                    #region filters
                    if (location != null)
                    {
                        System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                        if (Ids.Count != 0)
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                        }
                    }
                    if (risk != null)
                    {
                        System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                        if (riskIds.Count != 0)
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                        }
                    }
                    if (status != null)
                    {
                        System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                        if (statusIds.Count != 0)
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(x => statusIds.Contains(x.Status)).ToList();
                        }
                    }
                    if (userDetail != null)
                    {
                        System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                        if (usersIds.Count != 0)
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                            //ObjInternalChecklist = ObjInternalChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                        }
                    }
                    if (actDetail != null)
                    {
                        System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                        if (atcsIds.Count != 0)
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                        }
                    }
                    if (!string.IsNullOrEmpty(StartDateDetail))
                    {
                        DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(EndDateDetail))
                    {
                        DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                    }
                    if (!string.IsNullOrEmpty(SequenceID))
                    {
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                    }
                    #endregion

                    if (ObjInternalChecklist.Count > 0)
                    {
                        string ReportNameAdd = "Report of Internal CheckList Compliances";
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                            System.Data.DataTable ExcelData1 = null;
                            DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(ObjInternalChecklist);
                            DataView view1 = new System.Data.DataView(dataToExport);
                            ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "ParentName", "Branch", "ComCategoryName", "ShortDescription", "DetailedDescription", "ComplianceType", "DepartmentName", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "ReviewerDated", "OriginalPerformerName", "OriginalReviewerName", "Remarks", "SequenceID");

                            if (NewEntityColumn == false)
                            {
                                dataToExport.Columns.Remove("ParentName");
                            }
                            if (RemoveColumn == true)
                            {
                                dataToExport.Columns.Remove("ApproverName");
                                dataToExport.Columns.Remove("SequenceID");
                            }
                            exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Value = CustomerName;

                            exWorkSheet1.Cells["A2"].Value = "Report Name:";
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                            exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A4"].Value = "Internal Compliance ID";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Cells["B4"].Value = "Entity";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["C4"].Value = "Location";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["D4"].Value = "Category Name";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["E4"].Value = "Short Description";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["F4"].Value = "Detailed Description";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["G4"].Value = "Compliance Type";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["H4"].Value = "Department Name";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["I4"].Value = "Period";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["I4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["J4"].Value = "Due Date";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["J4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["K4"].Value = "Close Date";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["K4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["L4"].Value = "Status";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["M4"].Value = "Risk";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["N4"].Value = "Performer";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["O4"].Value = "Reviewer";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O4"].AutoFitColumns(50);

                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["P4"].Value = "";
                                exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["P4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["P4"].Value = "Approver";
                                exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["P4"].AutoFitColumns(30);
                            }


                            exWorkSheet1.Cells["Q4"].Value = "Actual Close Date Performer";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["R4"].Value = "Actual Close Date Reviewer";
                            exWorkSheet1.Cells["R4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["S4"].Value = " Original Performer Name";
                            exWorkSheet1.Cells["S4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["T4"].Value = "Original Reviewer Name";
                            exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["T4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["U4"].Value = "Remark";
                            exWorkSheet1.Cells["U4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U4"].AutoFitColumns(30);

                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["V4"].Value = "";
                                exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["V4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["V4"].Value = "Label";
                                exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["V4"].AutoFitColumns(30);
                            }


                            exWorkSheet1.Column(1).Width = 25;
                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Column(2).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(2).Width = 0;
                            }
                            //exWorkSheet1.Column(2).Width = 25;
                            exWorkSheet1.Column(3).Width = 30;
                            exWorkSheet1.Column(4).Width = 45;
                            exWorkSheet1.Column(5).Width = 25;
                            exWorkSheet1.Column(6).Width = 25;
                            exWorkSheet1.Column(7).Width = 25;
                            exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Column(9).Width = 20;
                            exWorkSheet1.Column(10).Width = 20;
                            exWorkSheet1.Column(11).Width = 20;
                            exWorkSheet1.Column(12).Width = 45;
                            exWorkSheet1.Column(13).Width = 45;
                            exWorkSheet1.Column(14).Width = 45;
                            exWorkSheet1.Column(15).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(16).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(16).Width = 30;
                            }

                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 30;
                            exWorkSheet1.Column(19).Width = 30;
                            exWorkSheet1.Column(20).Width = 30;
                            exWorkSheet1.Column(21).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(22).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(22).Width = 30;
                            }

                            for (int i = 4; i <= ExcelData1.Rows.Count + 4; i++)
                            {
                                string DueDateCell = "J" + i;
                                exWorkSheet1.Cells[DueDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ClosedDateCell = "K" + i;
                                exWorkSheet1.Cells[ClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string PerfomrerDateCell = "Q" + i;
                                exWorkSheet1.Cells[PerfomrerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ReviewerDateCell = "R" + i;
                                exWorkSheet1.Cells[ReviewerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";
                            }

                            using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 22])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            //Byte[] fileBytes = exportPackge.GetAsByteArray();
                            //return File(fileBytes, "application/vnd.ms-excel", "Internal Checklist.xlsx");

                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                            //reportName = ReportNameAdd + DateTime.Today.Date.ToString("ddMMMyyyyhhmmtt") + ".xlsx";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                            ReturnPath = reportName;

                        }
                    }
                    else
                    {
                        ReturnPath = "No Record Found";
                    }
                }

                else if (StatusFlag == 5)//for All
                {
                    #region statutory and Event Based
                    List<SP_Kendo_GetALLStatutoryEBData_Result> objTaskCompDocument1 = new List<SP_Kendo_GetALLStatutoryEBData_Result>();
                    List<SP_Kendo_GetALLStatutoryEBData_Result> objTaskCompDocument2 = new List<SP_Kendo_GetALLStatutoryEBData_Result>();

                    objTaskCompDocument1 = CannedReportManagement.GetCannedReportDatastatutoryEventBased(CustomerID, UserId, FlagIsApp, "Statutory");

                    if (flagDate == 1)
                    {
                        objTaskCompDocument1 = objTaskCompDocument1.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objTaskCompDocument1 = objTaskCompDocument1.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }
                    objTaskCompDocument2 = CannedReportManagement.GetCannedReportDatastatutoryEventBased(CustomerID, UserId, FlagIsApp, "Event Based");

                    if (flagDate == 1)
                    {
                        objTaskCompDocument2 = objTaskCompDocument2.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objTaskCompDocument2 = objTaskCompDocument2.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }

                    objTaskCompDocument = objTaskCompDocument1.Union(objTaskCompDocument2).ToList();

                    if (objTaskCompDocument.Count > 0)
                    {
                        #region filters
                        if (!string.IsNullOrEmpty(SequenceID))
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                        }
                        if (location != null)
                        {
                            System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                            if (Ids.Count != 0)
                            {
                                objTaskCompDocument = objTaskCompDocument.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                            }
                        }
                        if (risk != null)
                        {
                            System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                            if (riskIds.Count != 0)
                            {
                                objTaskCompDocument = objTaskCompDocument.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                            }
                        }
                        if (status != null)
                        {
                            System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                            if (statusIds.Count != 0)
                            {
                                objTaskCompDocument = objTaskCompDocument.Where(x => statusIds.Contains(x.Status)).ToList();
                            }
                        }
                        if (userDetail != null)
                        {
                            System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                            if (usersIds.Count != 0)
                            {
                                objTaskCompDocument = objTaskCompDocument.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                                //objTaskCompDocument = objTaskCompDocument.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                            }
                        }
                        if (actDetail != null)
                        {
                            System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                            if (atcsIds.Count != 0)
                            {
                                objTaskCompDocument = objTaskCompDocument.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                            objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                            objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EventName))
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.EventID == Convert.ToInt64(EventName))).ToList();
                        }
                        if (!string.IsNullOrEmpty(EventNature))
                        {
                            objTaskCompDocument = objTaskCompDocument.Where(entry => (entry.EventScheduleOnID == Convert.ToInt64(EventNature))).ToList();
                        }
                        #endregion
                    }
                    #endregion

                    #region Internal
                    objInternal = InternalCanned_ReportManagement.GetMyReportDataInternal(CustomerID, UserId, FlagIsApp, "Internal");
                    if (flagDate == 1)
                    {
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objInternal = objInternal.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }
                    if (objInternal.Count > 0)
                    {
                        #region filters
                        if (!string.IsNullOrEmpty(SequenceID))
                        {
                            objInternal = objInternal.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                        }
                        if (location != null)
                        {
                            System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                            if (Ids.Count != 0)
                            {
                                objInternal = objInternal.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                            }
                        }
                        if (risk != null)
                        {
                            System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                            if (riskIds.Count != 0)
                            {
                                objInternal = objInternal.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                            }
                        }
                        if (status != null)
                        {
                            System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                            if (statusIds.Count != 0)
                            {
                                objInternal = objInternal.Where(x => statusIds.Contains(x.Status)).ToList();
                            }
                        }
                        if (userDetail != null)
                        {
                            System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                            if (usersIds.Count != 0)
                            {
                                objInternal = objInternal.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                                //objInternal = objInternal.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                            }
                        }
                        if (actDetail != null)
                        {
                            System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                            if (atcsIds.Count != 0)
                            {
                                objInternal = objInternal.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                            objInternal = objInternal.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                            objInternal = objInternal.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                        }
                        #endregion
                    }
                    #endregion

                    #region Internal Checklist
                    ObjInternalChecklist = InternalCanned_ReportManagement.GetMyReportDataInternalChecklist(CustomerID, UserId, FlagIsApp, 3);
                    if (flagDate == 1)
                    {
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }
                    if (ObjInternalChecklist.Count > 0)
                    {
                        #region filters
                        if (!string.IsNullOrEmpty(SequenceID))
                        {
                            ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                        }
                        if (location != null)
                        {
                            System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                            if (Ids.Count != 0)
                            {
                                ObjInternalChecklist = ObjInternalChecklist.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                            }
                        }
                        if (risk != null)
                        {
                            System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                            if (riskIds.Count != 0)
                            {
                                ObjInternalChecklist = ObjInternalChecklist.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                            }
                        }
                        if (status != null)
                        {
                            System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                            if (statusIds.Count != 0)
                            {
                                ObjInternalChecklist = ObjInternalChecklist.Where(x => statusIds.Contains(x.Status)).ToList();
                            }
                        }
                        if (userDetail != null)
                        {
                            System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                            if (usersIds.Count != 0)
                            {
                                ObjInternalChecklist = ObjInternalChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                                //ObjInternalChecklist = ObjInternalChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                            }
                        }
                        if (actDetail != null)
                        {
                            System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                            if (atcsIds.Count != 0)
                            {
                                ObjInternalChecklist = ObjInternalChecklist.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                            ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                            ObjInternalChecklist = ObjInternalChecklist.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                        }
                        #endregion
                    }
                    #endregion

                    #region Statutory Checklist and Event Based Checklist
                    List<SP_kendo_getSChecklist_Result> objStatutoryChecklist1 = new List<SP_kendo_getSChecklist_Result>();
                    List<SP_kendo_getSChecklist_Result> objStatutoryChecklist2 = new List<SP_kendo_getSChecklist_Result>();

                    objStatutoryChecklist1 = CannedReportManagement.GetCannedReportDatastatutoryChecklist(CustomerID, UserId, FlagIsApp, 2);
                    if (flagDate == 1)
                    {
                        objStatutoryChecklist1 = objStatutoryChecklist1.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objStatutoryChecklist1 = objStatutoryChecklist1.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }

                    objStatutoryChecklist2 = CannedReportManagement.GetCannedReportDatastatutoryChecklist(CustomerID, UserId, FlagIsApp, 4);
                    if (flagDate == 1)
                    {
                        objStatutoryChecklist2 = objStatutoryChecklist2.Where(entry => (entry.ScheduledOn >= Fromdate && entry.ScheduledOn <= Todate)).ToList();
                    }
                    if (AflagDate == 1)
                    {
                        objStatutoryChecklist2 = objStatutoryChecklist2.Where(entry => (entry.ScheduledOn >= AFromdate && entry.ScheduledOn <= ATodate)).ToList();
                    }

                    objStatutoryChecklist = objStatutoryChecklist1.Union(objStatutoryChecklist2).ToList();
                    if (objStatutoryChecklist.Count > 0)
                    {
                        #region filters
                        if (!string.IsNullOrEmpty(SequenceID))
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.SequenceID == SequenceID)).ToList();
                        }
                        if (location != null)
                        {
                            System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                            if (Ids.Count != 0)
                            {
                                objStatutoryChecklist = objStatutoryChecklist.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                            }
                        }
                        if (risk != null)
                        {
                            System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                            if (riskIds.Count != 0)
                            {
                                objStatutoryChecklist = objStatutoryChecklist.Where(x => riskIds.Contains(Convert.ToInt32(x.Risk))).ToList();
                            }
                        }
                        if (status != null)
                        {
                            System.Collections.Generic.IList<string> statusIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(status);
                            if (statusIds.Count != 0)
                            {
                                objStatutoryChecklist = objStatutoryChecklist.Where(x => statusIds.Contains(x.Status)).ToList();
                            }
                        }
                        if (userDetail != null)
                        {
                            System.Collections.Generic.IList<int> usersIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(userDetail);
                            if (usersIds.Count != 0)
                            {
                                objStatutoryChecklist = objStatutoryChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.PerformerID)) || usersIds.Contains(Convert.ToInt32(x.ReviewerID)) || usersIds.Contains(Convert.ToInt32(x.ApproverID))).ToList();
                                //objStatutoryChecklist = objStatutoryChecklist.Where(x => usersIds.Contains(Convert.ToInt32(x.UserID))).ToList();
                            }
                        }
                        if (actDetail != null)
                        {
                            System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                            if (atcsIds.Count != 0)
                            {
                                objStatutoryChecklist = objStatutoryChecklist.Where(x => atcsIds.Contains(Convert.ToInt32(x.ActID))).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(StartDateDetail))
                        {
                            DateTime StartDate = Convert.ToDateTime(StartDateDetail);
                            objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn >= StartDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EndDateDetail))
                        {
                            DateTime EnDDate = Convert.ToDateTime(EndDateDetail);
                            objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.ScheduledOn <= EnDDate)).ToList();
                        }
                        if (!string.IsNullOrEmpty(EventName))
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.EventID == Convert.ToInt64(EventName))).ToList();
                        }
                        if (!string.IsNullOrEmpty(EventNature))
                        {
                            objStatutoryChecklist = objStatutoryChecklist.Where(entry => (entry.EventScheduleOnID == Convert.ToInt64(EventNature))).ToList();
                        }
                        #endregion
                    }
                    #endregion

                    DataTable table = new DataTable();
                    table.Columns.Add("ComplianceID", typeof(long));
                    table.Columns.Add("ParentName", typeof(string));
                    table.Columns.Add("Branch", typeof(string));
                    table.Columns.Add("ActName", typeof(string));
                    table.Columns.Add("ComCategoryName", typeof(string));
                    table.Columns.Add("ComSubTypeName", typeof(string));
                    table.Columns.Add("ShortDescription", typeof(string));
                    table.Columns.Add("Description", typeof(string));
                    table.Columns.Add("complianceType", typeof(string));
                    table.Columns.Add("DepartmentName", typeof(string));
                    table.Columns.Add("EventName", typeof(string));
                    table.Columns.Add("EventNature", typeof(string));
                    table.Columns.Add("ForMonth", typeof(string));
                    table.Columns.Add("ScheduledOn", typeof(string));
                    table.Columns.Add("CloseDate", typeof(string));
                    table.Columns.Add("Status", typeof(string));
                    table.Columns.Add("RiskCategory", typeof(string));
                    table.Columns.Add("PerformerName", typeof(string));
                    table.Columns.Add("ReviewerName", typeof(string));
                    table.Columns.Add("ApproverName", typeof(string));
                    table.Columns.Add("PerformerDated", typeof(string));
                    table.Columns.Add("ReviewerDated", typeof(string));
                    table.Columns.Add("OriginalPerformerName", typeof(string));
                    table.Columns.Add("OriginalReviewerName", typeof(string));
                    table.Columns.Add("PenaltyDescription", typeof(string));
                    table.Columns.Add("ReportName", typeof(string));
                    table.Columns.Add("ShortForm", typeof(string));
                    table.Columns.Add("PerformerRemark", typeof(string));
                    table.Columns.Add("ReviewerRemark", typeof(string));
                    table.Columns.Add("SequenceID", typeof(string));
                    table.Columns.Add("ChallanNo", typeof(string));
                    table.Columns.Add("ChallanAmount", typeof(string));
                    table.Columns.Add("BankName", typeof(string));
                    table.Columns.Add("Challanpaiddate", typeof(string));
                    table.Columns.Add("GSTNumber", typeof(string));
                    table.Columns.Add("Penalty", typeof(string));
                    table.Columns.Add("Reviseduedate", typeof(string));
                    table.Columns.Add("ValuesAsPerReturn", typeof(string));

                    if (NewEntityColumn == false)
                    {
                        table.Columns.Remove("ParentName");
                    }
                    if (objTaskCompDocument.Count > 0
                        || objInternal.Count > 0
                        || ObjInternalChecklist.Count > 0
                        || objStatutoryChecklist.Count > 0)
                    {
                        foreach (var item in objTaskCompDocument)
                        {
                            table.Rows.Add(item.ComplianceID, item.ParentName, item.Branch, item.ActName, item.ComCategoryName, item.ComSubTypeName, item.ShortDescription, item.Description,
                              item.ComplianceType, item.DepartmentName, item.EventName, item.EventNature, item.ForMonth, item.ScheduledOn, item.CloseDate, item.Status, item.RiskCategory, item.PerformerName, item.ReviewerName, item.ApproverName
                                , item.PerformerDated, item.ReviewerDated, item.OriginalPerformerName, item.OriginalReviewerName, item.PenaltyDescription, item.ReportName, item.ShortForm, item.PerformerRemark, item.ReviewerRemark, item.SequenceID,
                              item.ChallanNo, item.ChallanAmount, item.BankName, item.Challanpaiddate, item.GSTNumber, item.Penalty, item.Reviseduedate,
                              item.ValuesAsPerReturn
                               );
                        }
                        foreach (var item in objInternal)
                        {
                            table.Rows.Add(item.ComplianceID, item.ParentName, item.Branch, item.ActName, item.ComCategoryName, "", item.ShortDescription, item.DetailedDescription,
                              item.ComplianceType, item.DepartmentName, "", "", item.ForMonth, item.ScheduledOn, item.CloseDate, item.Status, item.RiskCategory, item.PerformerName, item.ReviewerName, item.ApproverName
                                , item.PerformerDated, item.ReviewerDated, item.OriginalPerformerName, item.OriginalReviewerName
                                , item.PenaltyDescription, item.ReportName, item.ShortForm, item.PerformerRemark, item.ReviewerRemark, item.SequenceID,
                              "", "", "", "", "", "", item.Reviseduedate, ""
                              );
                        }
                        foreach (var item in ObjInternalChecklist)
                        {
                            table.Rows.Add(item.ComplianceID, item.ParentName, item.Branch, item.ActName, item.ComCategoryName, "", item.ShortDescription, item.DetailedDescription,
                              item.ComplianceType, item.DepartmentName, "", "", item.ForMonth, item.ScheduledOn, item.CloseDate, item.Status, item.RiskCategory, item.PerformerName, item.ReviewerName, item.ApproverName
                                , item.PerformerDated, item.ReviewerDated, item.OriginalPerformerName, item.OriginalReviewerName
                                , item.PenaltyDescription, item.ReportName, item.ShortForm, item.Remarks, "", item.SequenceID,
                              "", "", "", "", "", "", "", ""
                               );
                        }
                        foreach (var item in objStatutoryChecklist)
                        {
                            table.Rows.Add(item.ComplianceID, item.ParentName, item.Branch, item.ActName, item.ComCategoryName, item.ComSubTypeName, item.ShortDescription, item.Description,
                              item.ComplianceType, item.DepartmentName, item.EventName, item.EventNature, item.ForMonth, item.ScheduledOn, item.CloseDate, item.Status, item.RiskCategory, item.PerformerName, item.ReviewerName, item.ApproverName
                                , item.PerformerDated, item.ReviewerDated, item.OriginalPerformerName, item.OriginalReviewerName
                                , item.PenaltyDescription, item.ReportName, item.ShortForm, item.PerformerRemark, item.ReviewerRemark, item.SequenceID,
                              "", "", "", "", "", "", "", "", ""

                               );
                        }

                        string ReportNameAdd = "Report of All";
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                            System.Data.DataTable ExcelData1 = null;
                            //DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(ObjInternalChecklist);
                            DataView view1 = new System.Data.DataView(table);
                            ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "ActName", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ComplianceType", "DepartmentName", "EventName", "EventNature", "ForMonth", "ScheduledOn", "CloseDate", "Status", "RiskCategory", "PerformerName", "ReviewerName", "ApproverName", "PerformerDated", "ReviewerDated",
                                "OriginalPerformerName", "OriginalReviewerName", "PenaltyDescription", "ShortForm", "ReportName", "PerformerRemark", "ReviewerRemark", "SequenceID", "ChallanNo", "ChallanAmount", "BankName", "Challanpaiddate", "GSTNumber", "Penalty", "Reviseduedate", "ValuesAsPerReturn", "ParentName");

                            exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Value = CustomerName;

                            exWorkSheet1.Cells["A2"].Value = "Report Name:";
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                            exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Cells["B4"].Value = "Entity";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["C4"].Value = "Location";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["D4"].Value = "Act";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["E4"].Value = "Category Name";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["F4"].Value = "Sub Category Name";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["F4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["G4"].Value = "Short Description";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G4"].AutoFitColumns(20);

                            exWorkSheet1.Cells["H4"].Value = "Detail Description";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["I4"].Value = "ComplianceType";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I4"].AutoFitColumns(30);


                            exWorkSheet1.Cells["J4"].Value = "Department Name";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["K4"].Value = "EventName";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["L4"].Value = "EventNature";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["M4"].Value = "Period";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["M4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["N4"].Value = "Due Date";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["N4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["O4"].Value = "Close Date";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["O4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["P4"].Value = "Status";
                            exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["P4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Q4"].Value = "Risk";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["R4"].Value = "Performer";
                            exWorkSheet1.Cells["R4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["S4"].Value = "Reviewer";
                            exWorkSheet1.Cells["S4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S4"].AutoFitColumns(50);
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["T4"].Value = "";
                                exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["T4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["T4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["T4"].Value = "Approver";
                                exWorkSheet1.Cells["T4"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["T4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["T4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["T4"].AutoFitColumns(50);
                            }


                            exWorkSheet1.Cells["U4"].Value = "Actual Close Date Performer";
                            exWorkSheet1.Cells["U4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["U4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["V4"].Value = "Actual Close Date Reviewer";
                            exWorkSheet1.Cells["V4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["V4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["V4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["W4"].Value = " Original Performer Name";
                            exWorkSheet1.Cells["W4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["W4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["W4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["X4"].Value = "Original Reviewer Name";
                            exWorkSheet1.Cells["X4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["X4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["X4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Y4"].Value = "Penalty Description";
                            exWorkSheet1.Cells["Y4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Y4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Y4"].AutoFitColumns(50);

                            exWorkSheet1.Cells["Z4"].Value = "Short Form";
                            exWorkSheet1.Cells["Z4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Z4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Z4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["AA4"].Value = "Report Name";
                            exWorkSheet1.Cells["AA4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AA4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AA4"].AutoFitColumns(40);

                            exWorkSheet1.Cells["AB4"].Value = "PerformerRemark";
                            exWorkSheet1.Cells["AB4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AB4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AB4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["AC4"].Value = "ReviewerRemark";
                            exWorkSheet1.Cells["AC4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AC4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AC4"].AutoFitColumns(30);
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Cells["AD4"].Value = "";
                                exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AD4"].AutoFitColumns(0);
                            }
                            else
                            {
                                exWorkSheet1.Cells["AD4"].Value = "Label";
                                exWorkSheet1.Cells["AD4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AD4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AD4"].AutoFitColumns(30);
                            }



                            if (BankNewFields == true)
                            {
                                exWorkSheet1.Cells["AE4"].Value = "Challan No";
                                exWorkSheet1.Cells["AE4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AE4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AE4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AF4"].Value = "Challan Amount";
                                exWorkSheet1.Cells["AF4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AF4"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                exWorkSheet1.Cells["AF4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AF4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AG4"].Value = "Bank Name";
                                exWorkSheet1.Cells["AG4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AG4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AG4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AH4"].Value = "Challan Paid Date";
                                exWorkSheet1.Cells["AH4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AH4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AH4"].AutoFitColumns(30);

                                exWorkSheet1.Cells["AI4"].Value = "GST No.";
                                exWorkSheet1.Cells["AI4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AI4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AI4"].AutoFitColumns(30);


                            }
                            //if (NewColumnsExtendedDuedate == true)
                            //{
                            exWorkSheet1.Cells["AJ4"].Value = "Penalty";
                            exWorkSheet1.Cells["AJ4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AJ4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AJ4"].AutoFitColumns(30);

                            exWorkSheet1.Cells["AK4"].Value = " Extended Due Date";
                            exWorkSheet1.Cells["AK4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AK4"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AK4"].AutoFitColumns(30);

                            //}
                            //else
                            //{
                            //    exWorkSheet1.Cells["AJ4"].Value = " ";
                            //    exWorkSheet1.Cells["AJ4"].Style.Font.Bold = true;
                            //    exWorkSheet1.Cells["AJ4"].Style.Font.Size = 12;
                            //    exWorkSheet1.Cells["AJ4"].AutoFitColumns(30);
                            //}
                            if (NewColumnsReturn == true)
                            {
                                exWorkSheet1.Cells["AL4"].Value = "Return Amount";
                                exWorkSheet1.Cells["AL4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AL4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AL4"].AutoFitColumns(30);


                            }



                            exWorkSheet1.Column(1).Width = 25;
                            if (NewEntityColumn == true)
                            {
                                exWorkSheet1.Column(2).Width = 25;
                            }
                            else
                            {
                                exWorkSheet1.Column(2).Width = 0;
                            }
                            //exWorkSheet1.Column(2).Width = 25;
                            exWorkSheet1.Column(3).Width = 30;
                            exWorkSheet1.Column(4).Width = 45;
                            exWorkSheet1.Column(5).Width = 25;
                            exWorkSheet1.Column(6).Width = 30;
                            exWorkSheet1.Column(7).Width = 30;
                            exWorkSheet1.Column(8).Width = 45;
                            exWorkSheet1.Column(9).Width = 20;
                            exWorkSheet1.Column(10).Width = 20;
                            exWorkSheet1.Column(11).Width = 20;
                            exWorkSheet1.Column(12).Width = 20;
                            exWorkSheet1.Column(13).Width = 45;
                            exWorkSheet1.Column(14).Width = 30;
                            exWorkSheet1.Column(15).Width = 30;
                            exWorkSheet1.Column(16).Width = 30;
                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 25;
                            exWorkSheet1.Column(19).Width = 25;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(20).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(20).Width = 30;
                            }

                            exWorkSheet1.Column(21).Width = 45;
                            exWorkSheet1.Column(22).Width = 45;
                            exWorkSheet1.Column(23).Width = 45;
                            exWorkSheet1.Column(24).Width = 45;
                            exWorkSheet1.Column(25).Width = 45;
                            exWorkSheet1.Column(26).Width = 45;
                            exWorkSheet1.Column(27).Width = 30;
                            exWorkSheet1.Column(28).Width = 30;
                            exWorkSheet1.Column(29).Width = 30;
                            if (RemoveColumn == true)
                            {
                                exWorkSheet1.Column(30).Width = 0;
                            }
                            else
                            {
                                exWorkSheet1.Column(30).Width = 30;
                            }

                            exWorkSheet1.Column(31).Width = 30;
                            exWorkSheet1.Column(32).Width = 30;
                            exWorkSheet1.Column(33).Width = 30;
                            exWorkSheet1.Column(34).Width = 30;
                            exWorkSheet1.Column(35).Width = 30;
                            exWorkSheet1.Column(36).Width = 30;
                            exWorkSheet1.Column(37).Width = 30;
                            exWorkSheet1.Column(38).Width = 30;

                            for (int i = 4; i <= ExcelData1.Rows.Count + 4; i++)
                            {
                                string DueDateCell = "M" + i;
                                exWorkSheet1.Cells[DueDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ClosedDateCell = "N" + i;
                                exWorkSheet1.Cells[ClosedDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string PerfomrerDateCell = "O" + i;
                                exWorkSheet1.Cells[PerfomrerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ReviewerDateCell = "U" + i;
                                exWorkSheet1.Cells[ReviewerDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";


                                string ReviewerDateCell1 = "V" + i;
                                exWorkSheet1.Cells[ReviewerDateCell1].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ChallanPaidDateCell = "AH" + i;
                                exWorkSheet1.Cells[ChallanPaidDateCell].Style.Numberformat.Format = "dd-MMM-yyyy";

                                string ReviseduedateCell = "AK" + i;
                                exWorkSheet1.Cells[ReviseduedateCell].Style.Numberformat.Format = "dd-MMM-yyyy";
                            }

                            using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 38])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                            //reportName = ReportNameAdd + DateTime.Today.Date.ToString("ddMMMyyyyhhmmtt") + ".xlsx";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                            ReturnPath = reportName;
                        }
                    }
                    else
                    {
                        ReturnPath = "No Record Found";
                    }
                }

                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }
 
        public ActionResult ReportMgmt(int StatusFlag, int Isdept, int isapprover, string CName, int UserId, int CustomerID, string location, string Role, string Function, string actDetail, string FrequencyDetail)
        {
            try
            {
                string ReturnPath = "";
                List<MGMTDashboradResult> objResult = new List<MGMTDashboradResult>();

                List<sp_ComplianceInstanceAssignmentViewDetails_Result> objStatutory = new List<sp_ComplianceInstanceAssignmentViewDetails_Result>();
                List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> objInternal = new List<sp_InternalComplianceInstanceAssignmentViewDetails_Result>();
                List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> objDeptStatutory = new List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result>();
                List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> objDeptInternal = new List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result>();

                if (StatusFlag == -1 || StatusFlag == 1)
                {
                    #region Statutory
                    int actID = -1;
                    if (actDetail != null)
                    {
                        System.Collections.Generic.IList<int> atcsIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(actDetail);
                        if (atcsIds.Count > 0)
                        {
                            actID = atcsIds.FirstOrDefault();
                        }
                    }

                    int RoleID = -1;
                    if (Role != null)
                    {
                        System.Collections.Generic.IList<int> RoleIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Role);
                        if (RoleIds.Count > 0)
                        {
                            RoleID = RoleIds.FirstOrDefault();
                        }
                    }

                    int FunctionID = -1;
                    if (Function != null)
                    {
                        System.Collections.Generic.IList<int> FunctionIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Function);
                        if (FunctionIds.Count > 0)
                        {
                            FunctionID = FunctionIds.FirstOrDefault();
                        }
                    }

                    string FrequencyID = "";
                    if (FrequencyDetail != null)
                    {
                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                        }
                    }
                    System.Collections.Generic.List<long> locationID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.List<long>>(location);

                    if (Isdept == 1)
                    {
                        #region Department Head
                        objDeptStatutory = GradingDisplay.GetComplianceDetailsDashboard_DeptHead(CustomerID, locationID, RoleID, -1, actID, FunctionID, UserId, isapprover);

                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                            objDeptStatutory = objDeptStatutory.Where(x => x.Frequency == FrequencyID).ToList();
                        }

                        if (objDeptStatutory.Count > 0)
                        {
                            string ReportName = string.Empty;
                            ReportName = "Statutory";

                            string ReportNameAdd = "Report of " + ReportName + " Compliances";
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                                System.Data.DataTable ExcelData1 = null;
                                DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objDeptStatutory);
                                DataView view1 = new System.Data.DataView(dataToExport);

                                ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "Section", "ShortDescription", "DepartmentName", "Frequency", "User");

                                exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Value = CName;

                                exWorkSheet1.Cells["A2"].Value = "Report Name:";
                                exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                                exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                                exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                                exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                exWorkSheet1.Cells["B4"].Value = "Location";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["C4"].Value = "Act";
                                exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["D4"].Value = "Category Name";
                                exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D4"].AutoFitColumns(20);

                                exWorkSheet1.Cells["E4"].Value = "Section";
                                exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F4"].Value = "Short Description";
                                exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G4"].Value = "Department";
                                exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["H4"].Value = "Frequency";
                                exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["I4"].Value = "User";
                                exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["I4"].AutoFitColumns(50);

                                exWorkSheet1.Column(1).Width = 21;
                                exWorkSheet1.Column(2).Width = 25;
                                exWorkSheet1.Column(3).Width = 40;
                                exWorkSheet1.Column(4).Width = 20;
                                exWorkSheet1.Column(5).Width = 20;
                                exWorkSheet1.Column(6).Width = 50;
                                exWorkSheet1.Column(7).Width = 30;
                                exWorkSheet1.Column(8).Width = 20;
                                exWorkSheet1.Column(9).Width = 20;

                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 9])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                //Byte[] fileBytes = exportPackge.GetAsByteArray();
                                //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");

                                var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                                string reportName = string.Empty;
                                reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                string p_strPath = string.Empty;
                                string dirpath = string.Empty;
                                p_strPath = @"" + storagedrive + "/" + reportName;
                                dirpath = @"" + storagedrive;
                                if (!Directory.Exists(dirpath))
                                {
                                    Directory.CreateDirectory(dirpath);
                                }
                                FileStream objFileStrm = System.IO.File.Create(p_strPath);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                                ReturnPath = reportName;
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }
                        #endregion
                    }
                    else
                    {
                        #region Statutory
                        objStatutory = GradingDisplay.GetComplianceDetailsDashboard(CustomerID, locationID, 3, RoleID, actID, FunctionID, UserId, isapprover);

                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                            objStatutory = objStatutory.Where(x => x.Frequency == FrequencyID).ToList();
                        }

                        if (objStatutory.Count > 0)
                        {
                            string ReportName = string.Empty;
                            if (StatusFlag == -1)
                            {
                                ReportName = "Statutory";
                            }
                            if (StatusFlag == 0)
                            {
                                ReportName = "Internal";
                            }

                            string ReportNameAdd = "Report of " + ReportName + " Compliances";
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                                System.Data.DataTable ExcelData1 = null;
                                DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objStatutory);
                                DataView view1 = new System.Data.DataView(dataToExport);

                                ExcelData1 = view1.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "Section", "ShortDescription", "Frequency", "User");

                                exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Value = CName;

                                exWorkSheet1.Cells["A2"].Value = "Report Name:";
                                exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                                exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                                exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                                exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                exWorkSheet1.Cells["B4"].Value = "Location";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["C4"].Value = "Act";
                                exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["D4"].Value = "Category Name";
                                exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D4"].AutoFitColumns(20);

                                exWorkSheet1.Cells["E4"].Value = "Section";
                                exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F4"].Value = "Short Description";
                                exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G4"].Value = "Frequency";
                                exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["H4"].Value = "User";
                                exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                                exWorkSheet1.Column(1).Width = 21;
                                exWorkSheet1.Column(2).Width = 25;
                                exWorkSheet1.Column(3).Width = 40;
                                exWorkSheet1.Column(4).Width = 20;
                                exWorkSheet1.Column(5).Width = 20;
                                exWorkSheet1.Column(6).Width = 50;
                                exWorkSheet1.Column(7).Width = 20;
                                exWorkSheet1.Column(8).Width = 20;

                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 8])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                //Byte[] fileBytes = exportPackge.GetAsByteArray();
                                //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");

                                var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                                string reportName = string.Empty;
                                reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                string p_strPath = string.Empty;
                                string dirpath = string.Empty;
                                p_strPath = @"" + storagedrive + "/" + reportName;
                                dirpath = @"" + storagedrive;
                                if (!Directory.Exists(dirpath))
                                {
                                    Directory.CreateDirectory(dirpath);
                                }
                                FileStream objFileStrm = System.IO.File.Create(p_strPath);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                                ReturnPath = reportName;
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }
                        #endregion
                    }
                    #endregion
                }
                else if (StatusFlag == 0)
                {
                    #region Internal
                    int RoleID = -1;
                    if (Role != null)
                    {
                        System.Collections.Generic.IList<int> RoleIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Role);
                        if (RoleIds.Count > 0)
                        {
                            RoleID = RoleIds.FirstOrDefault();
                        }
                    }

                    int FunctionID = -1;
                    if (Function != null)
                    {
                        System.Collections.Generic.IList<int> FunctionIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(Function);
                        if (FunctionIds.Count > 0)
                        {
                            FunctionID = FunctionIds.FirstOrDefault();
                        }
                    }

                    string FrequencyID = "";
                    if (FrequencyDetail != null)
                    {
                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                        }
                    }
                    System.Collections.Generic.List<long> locationID = new JavaScriptSerializer().Deserialize<System.Collections.Generic.List<long>>(location);

                    if (Isdept == 1)
                    {
                        #region Internal Deartment Head
                        objDeptInternal = GradingDisplay.GetInternalComplianceDetailsDashboard_DeptHead(CustomerID, locationID, RoleID, -1, FunctionID, UserId, isapprover);

                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                            objDeptInternal = objDeptInternal.Where(x => x.Frequency == FrequencyID).ToList();
                        }

                        if (objDeptInternal.Count > 0)
                        {
                            string ReportName = string.Empty;
                            ReportName = "Internal";

                            string ReportNameAdd = "Report of " + ReportName + " Compliances";
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                                System.Data.DataTable ExcelData1 = null;
                                DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objDeptInternal);
                                DataView view1 = new System.Data.DataView(dataToExport);

                                ExcelData1 = view1.ToTable("Selected", false, "InternalComplianceID", "Branch", "InternalComplianceCategoryName", "ShortDescription", "DepartmentName", "Frequency", "User", "InternalComplianceTypeName");

                                exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Value = CName;

                                exWorkSheet1.Cells["A2"].Value = "Report Name:";
                                exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                                exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                                exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                                exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                exWorkSheet1.Cells["B4"].Value = "Location";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["C4"].Value = "Category Name";
                                exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C4"].AutoFitColumns(20);

                                exWorkSheet1.Cells["D4"].Value = "Short Description";
                                exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["E4"].Value = "Department";
                                exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F4"].Value = "Frequency";
                                exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G4"].Value = "User";
                                exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["H4"].Value = "InternalCompliance Type";
                                exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["H4"].AutoFitColumns(50);

                                exWorkSheet1.Column(1).Width = 21;
                                exWorkSheet1.Column(2).Width = 25;
                                exWorkSheet1.Column(3).Width = 40;
                                exWorkSheet1.Column(4).Width = 50;
                                exWorkSheet1.Column(5).Width = 30;
                                exWorkSheet1.Column(6).Width = 20;
                                exWorkSheet1.Column(7).Width = 20;
                                exWorkSheet1.Column(7).Width = 30;

                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 8])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }

                                var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                                string reportName = string.Empty;
                                reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                string p_strPath = string.Empty;
                                string dirpath = string.Empty;
                                p_strPath = @"" + storagedrive + "/" + reportName;
                                dirpath = @"" + storagedrive;
                                if (!Directory.Exists(dirpath))
                                {
                                    Directory.CreateDirectory(dirpath);
                                }
                                FileStream objFileStrm = System.IO.File.Create(p_strPath);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                                ReturnPath = reportName;
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }
                        #endregion
                    }
                    else
                    {
                        #region Internal
                        objInternal = GradingDisplay.GetInternalComplianceDetailsDashboard(CustomerID, locationID, RoleID, -1, FunctionID, UserId, 0);

                        System.Collections.Generic.IList<string> FrequencyIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(FrequencyDetail);
                        if (FrequencyIds.Count > 0)
                        {
                            FrequencyID = FrequencyIds.FirstOrDefault();
                            objInternal = objInternal.Where(x => x.Frequency == FrequencyID).ToList();
                        }

                        if (objInternal.Count > 0)
                        {
                            string ReportName = string.Empty;
                            ReportName = "Internal";

                            string ReportNameAdd = "Report of " + ReportName + " Compliances";
                            using (ExcelPackage exportPackge = new ExcelPackage())
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                                System.Data.DataTable ExcelData1 = null;
                                DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objInternal);
                                DataView view1 = new System.Data.DataView(dataToExport);

                                ExcelData1 = view1.ToTable("Selected", false, "InternalComplianceID", "Branch", "InternalComplianceCategoryName", "ShortDescription", "Frequency", "User", "InternalComplianceTypeName");

                                exWorkSheet1.Cells["A1"].Value = "Entity/ Location:";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Value = CName;

                                exWorkSheet1.Cells["A2"].Value = "Report Name:";
                                exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B2"].Value = ReportNameAdd;

                                exWorkSheet1.Cells["A3"].Value = "Report Generated On:";
                                exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B3"].Value = DateTime.Now.ToString("dd MMM yyyy");

                                exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A4"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                exWorkSheet1.Cells["B4"].Value = "Location";
                                exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["C4"].Value = "Category Name";
                                exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C4"].AutoFitColumns(20);

                                exWorkSheet1.Cells["D4"].Value = "Short Description";
                                exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["E4"].Value = "Frequency";
                                exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F4"].Value = "User";
                                exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F4"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G4"].Value = "InternalCompliance Type";
                                exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G4"].AutoFitColumns(50);

                                exWorkSheet1.Column(1).Width = 21;
                                exWorkSheet1.Column(2).Width = 25;
                                exWorkSheet1.Column(3).Width = 40;
                                exWorkSheet1.Column(4).Width = 50;
                                exWorkSheet1.Column(5).Width = 20;
                                exWorkSheet1.Column(6).Width = 20;
                                exWorkSheet1.Column(7).Width = 20;

                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 4 + ExcelData1.Rows.Count, 7])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                //Byte[] fileBytes = exportPackge.GetAsByteArray();
                                //return File(fileBytes, "application/vnd.ms-excel", ReportName + ".xlsx");

                                var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                                string reportName = string.Empty;
                                reportName = ReportNameAdd + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                                string p_strPath = string.Empty;
                                string dirpath = string.Empty;
                                p_strPath = @"" + storagedrive + "/" + reportName;
                                dirpath = @"" + storagedrive;
                                if (!Directory.Exists(dirpath))
                                {
                                    Directory.CreateDirectory(dirpath);
                                }
                                FileStream objFileStrm = System.IO.File.Create(p_strPath);
                                objFileStrm.Close();
                                System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                                ReturnPath = reportName;
                            }
                        }
                        else
                        {
                            ReturnPath = "No Record Found";
                        }
                        #endregion
                    }

                    return Content(ReturnPath);

                    #endregion
                }

                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }
 
        public ActionResult GetFile(string userpath)
        {
            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename);
            Response.BinaryWrite(DocumentManagement.ReadDocFiles(p_strPath)); // create the file
            Response.Flush(); // send it to the client to download
            HttpContext.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            return View();
        }

        #region Fund Document
        public ActionResult ChecklistReport(int UserId, int CustomerID, int SID, int AID)
        {
            try
            {
                string ReturnPath = "";
                List<MGMTDashboradResult> objResult = new List<MGMTDashboradResult>();

                List<CompFund_SP_DetailsCheckListReport_Result> objStatutory = new List<CompFund_SP_DetailsCheckListReport_Result>();

                objStatutory = FundMasterManagement.GetChecklistAudit(SID, AID);

                if (objStatutory.Count > 0)
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        #region Cover sheet
                        int compliedCount = objStatutory.Where(entry => entry.ResultStatusID == 2).ToList().Count;
                        int NotcompliedCount = objStatutory.Where(a => a.ResultStatusID == 3).ToList().Count;
                        int AppliANDNotApplicableCount = objStatutory.Where(b => b.ResultStatusID == 1).ToList().Count;
                        int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;
                        string ReportNameAdd = "Cover sheet";
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ReportNameAdd);
                        System.Data.DataTable ExcelData1 = null;
                        DataTable dataToExport = CannedReportManagement.LINQResultToDataTable(objStatutory);
                        DataView view1 = new System.Data.DataView(dataToExport);

                        ExcelData1 = view1.ToTable("Selected", false, "SNO", "Clause_ref_num", "Comp_Que", "ResultStatus", "Remarks");
                        int cnt = 1;
                        #region Count
                        foreach (DataRow item in ExcelData1.Rows)
                        {
                            item["SNO"] = cnt;
                            cnt++;
                        }
                        #endregion
                        exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData1, true);

                        exWorkSheet1.Cells["A1"].Value = "SNO";
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A1"].AutoFitColumns(5);

                        exWorkSheet1.Cells["B1"].Value = "Relevant Clause Reference";
                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["B1"].AutoFitColumns(50);

                        exWorkSheet1.Cells["C1"].Value = "Compliance Question relating to agreed Term/Conditions";
                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["C1"].AutoFitColumns(50);

                        exWorkSheet1.Cells["D1"].Value = "Status";
                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["D1"].AutoFitColumns(20);

                        exWorkSheet1.Cells["E1"].Value = "Remark";
                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["E1"].AutoFitColumns(50);

                        exWorkSheet1.Column(1).Width = 21;
                        exWorkSheet1.Column(2).Width = 25;
                        exWorkSheet1.Column(3).Width = 40;
                        exWorkSheet1.Column(4).Width = 20;
                        exWorkSheet1.Column(5).Width = 20;

                        #region  chart
                        int chartstart = ExcelData1.Rows.Count + 3;

                        int chartHeader = chartstart + 2;
                        string header = "C" + chartHeader;
                        string header1 = "D" + chartHeader;
                        string header2 = "E" + chartHeader;

                        string headermerge = header + ":" + header2;

                        exWorkSheet1.Cells[header].Value = "Summary of Compliance Status";
                        exWorkSheet1.Cells[headermerge].Merge = true;
                        exWorkSheet1.Cells[headermerge].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells[header].Style.WrapText = true;


                        chartstart = chartstart + 3;
                        int range = (chartstart + 1);
                        int rangenext = (range + 1);
                        int rangenext1 = (rangenext + 1);

                        string x = "C" + chartstart;
                        string y = "C" + range;
                        string z = "C" + rangenext;
                        string t = "C" + rangenext1;

                        string percenatge = "E" + chartstart;
                        string percenatge1 = "E" + range;
                        string percenatge2 = "E" + rangenext;

                        string x1 = "D" + chartstart;
                        string y1 = "D" + range;
                        string z1 = "D" + rangenext;
                        string t1 = "D" + rangenext1;

                        exWorkSheet1.Cells[percenatge].Value = string.Format("{0:0.00}%", ((double)compliedCount / (double)totalPoints) * 100);
                        exWorkSheet1.Cells[percenatge1].Value = string.Format("{0:0.00}%", ((double)NotcompliedCount / (double)totalPoints) * 100);
                        exWorkSheet1.Cells[percenatge2].Value = string.Format("{0:0.00}%", ((double)AppliANDNotApplicableCount / (double)totalPoints) * 100);


                        exWorkSheet1.Cells[x1].Value = compliedCount;
                        exWorkSheet1.Cells[y1].Value = NotcompliedCount;
                        exWorkSheet1.Cells[z1].Value = AppliANDNotApplicableCount;
                        exWorkSheet1.Cells[t1].Value = totalPoints;

                        exWorkSheet1.Cells[x].Value = "Complied";
                        exWorkSheet1.Cells[x].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells[x].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                        exWorkSheet1.Cells[x].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[x].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[x].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[x].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[x].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                        exWorkSheet1.Cells[y].Value = "Not Complied";
                        exWorkSheet1.Cells[y].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells[y].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        exWorkSheet1.Cells[y].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[y].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[y].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[y].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[y].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells[z].Value = "Not Applicable";
                        exWorkSheet1.Cells[z].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells[z].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                        exWorkSheet1.Cells[z].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[z].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[z].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[z].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[z].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells[t].Value = "Total";

                        var myChart = exWorkSheet1.Drawings.AddChart("chart", eChartType.Pie3D);

                        string s = x1 + ":" + z1;
                        string s1 = x + ":" + z;

                        // Define series for the chart
                        var series = myChart.Series.Add(s, s1);


                        var pieSeries = (ExcelPieChartSerie)series;
                        pieSeries.Explosion = 5;

                        pieSeries.DataLabel.ShowPercent = true;
                        pieSeries.DataLabel.ShowLeaderLines = true;
                        pieSeries.DataLabel.Position = eLabelPosition.BestFit;

                        myChart.Border.Fill.Color = System.Drawing.Color.Green;
                        myChart.Title.Text = "Summary of Compliance Status";
                        //myChart.Title.Text = "My Chart";
                        myChart.SetSize(400, 400);
                        // Add to 6th row and to the 6th column
                        myChart.SetPosition(chartstart + 13, 0, 2, 0);

                        #endregion

                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, 1 + ExcelData1.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = exWorkSheet1.Cells[chartHeader, 3, chartHeader + 4, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        #endregion

                        #region Fund Document
                        var objFunddocument = objStatutory.Where(get => get.InvestorID == null).ToList();
                        int compliedCountF = objFunddocument.Where(entry => entry.ResultStatusID == 2).ToList().Count;
                        int NotcompliedCountF = objFunddocument.Where(a => a.ResultStatusID == 3).ToList().Count;
                        int AppliANDNotApplicableCountF = objFunddocument.Where(b => b.ResultStatusID == 1).ToList().Count;
                        int totalPointsF = compliedCountF + NotcompliedCountF + AppliANDNotApplicableCountF;
                        string ReportNameAddF = "Fund Document";

                        ExcelWorksheet exWorkSheet1F = exportPackge.Workbook.Worksheets.Add(ReportNameAddF);
                        System.Data.DataTable ExcelData1F = null;
                        DataTable dataToExportF = CannedReportManagement.LINQResultToDataTable(objFunddocument);
                        DataView view1F = new System.Data.DataView(dataToExportF);

                        ExcelData1F = view1F.ToTable("Selected", false, "SNO", "Clause_ref_num", "Comp_Que", "ResultStatus", "Remarks");
                        int cnt1 = 1;
                        #region Count
                        foreach (DataRow item1 in ExcelData1F.Rows)
                        {
                            item1["SNO"] = cnt1;
                            cnt1++;
                        }
                        #endregion
                        exWorkSheet1F.Cells["A1"].LoadFromDataTable(ExcelData1F, true);

                        exWorkSheet1F.Cells["A1"].Value = "SNO";
                        exWorkSheet1F.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1F.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet1F.Cells["A1"].AutoFitColumns(5);

                        exWorkSheet1F.Cells["B1"].Value = "Relevant Clause Reference";
                        exWorkSheet1F.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1F.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet1F.Cells["B1"].AutoFitColumns(50);

                        exWorkSheet1F.Cells["C1"].Value = "Compliance Question relating to agreed Term/Conditions";
                        exWorkSheet1F.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1F.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet1F.Cells["C1"].AutoFitColumns(50);

                        exWorkSheet1F.Cells["D1"].Value = "Status";
                        exWorkSheet1F.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1F.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet1F.Cells["D1"].AutoFitColumns(20);

                        exWorkSheet1F.Cells["E1"].Value = "Remark";
                        exWorkSheet1F.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1F.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet1F.Cells["E1"].AutoFitColumns(50);

                        exWorkSheet1F.Column(1).Width = 21;
                        exWorkSheet1F.Column(2).Width = 25;
                        exWorkSheet1F.Column(3).Width = 40;
                        exWorkSheet1F.Column(4).Width = 20;
                        exWorkSheet1F.Column(5).Width = 20;

                        #region  chart
                        int chartstartF = ExcelData1F.Rows.Count + 3;

                        int chartHeaderF = chartstartF + 2;
                        string headerF = "C" + chartHeaderF;
                        string header1F = "D" + chartHeaderF;
                        string header2F = "E" + chartHeaderF;

                        string headermergeF = headerF + ":" + header2F;

                        exWorkSheet1F.Cells[headerF].Value = "Summary of Compliance Status";
                        exWorkSheet1F.Cells[headermergeF].Merge = true;
                        exWorkSheet1F.Cells[headermergeF].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1F.Cells[headerF].Style.WrapText = true;


                        chartstartF = chartstartF + 3;
                        int rangeF = (chartstartF + 1);
                        int rangenextF = (rangeF + 1);
                        int rangenext1F = (rangenextF + 1);

                        string xF = "C" + chartstartF;
                        string yF = "C" + rangeF;
                        string zF = "C" + rangenextF;
                        string tF = "C" + rangenext1F;

                        string percenatgeF = "E" + chartstartF;
                        string percenatge1F = "E" + rangeF;
                        string percenatge2F = "E" + rangenextF;

                        string x1F = "D" + chartstartF;
                        string y1F = "D" + rangeF;
                        string z1F = "D" + rangenextF;
                        string t1F = "D" + rangenext1F;

                        exWorkSheet1F.Cells[percenatgeF].Value = string.Format("{0:0.00}%", ((double)compliedCountF / (double)totalPointsF) * 100);
                        exWorkSheet1F.Cells[percenatge1F].Value = string.Format("{0:0.00}%", ((double)NotcompliedCountF / (double)totalPointsF) * 100);
                        exWorkSheet1F.Cells[percenatge2F].Value = string.Format("{0:0.00}%", ((double)AppliANDNotApplicableCountF / (double)totalPointsF) * 100);


                        exWorkSheet1F.Cells[x1F].Value = compliedCountF;
                        exWorkSheet1F.Cells[y1F].Value = NotcompliedCountF;
                        exWorkSheet1F.Cells[z1F].Value = AppliANDNotApplicableCountF;
                        exWorkSheet1F.Cells[t1F].Value = totalPointsF;

                        exWorkSheet1F.Cells[xF].Value = "Complied";
                        exWorkSheet1F.Cells[xF].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1F.Cells[xF].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                        exWorkSheet1F.Cells[xF].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[xF].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[xF].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[xF].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[xF].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                        exWorkSheet1F.Cells[yF].Value = "Not Complied";
                        exWorkSheet1F.Cells[yF].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1F.Cells[yF].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        exWorkSheet1F.Cells[yF].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[yF].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[yF].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[yF].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[yF].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1F.Cells[zF].Value = "Not Applicable";
                        exWorkSheet1F.Cells[zF].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1F.Cells[zF].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                        exWorkSheet1F.Cells[zF].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[zF].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[zF].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[zF].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1F.Cells[zF].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1F.Cells[tF].Value = "Total";

                        var myChartF = exWorkSheet1F.Drawings.AddChart("chart", eChartType.Pie3D);

                        string sF = x1F + ":" + z1F;
                        string s1F = xF + ":" + zF;

                        // Define series for the chart
                        var seriesF = myChartF.Series.Add(sF, s1F);


                        var pieSeriesF = (ExcelPieChartSerie)seriesF;
                        pieSeriesF.Explosion = 5;

                        pieSeriesF.DataLabel.ShowPercent = true;
                        pieSeriesF.DataLabel.ShowLeaderLines = true;
                        pieSeriesF.DataLabel.Position = eLabelPosition.BestFit;

                        myChartF.Border.Fill.Color = System.Drawing.Color.Green;
                        myChartF.Title.Text = "Summary of Compliance Status";
                        //myChart.Title.Text = "My Chart";
                        myChartF.SetSize(400, 400);
                        // Add to 6th row and to the 6th column
                        myChartF.SetPosition(chartstartF + 13, 0, 2, 0);

                        #endregion

                        using (ExcelRange col = exWorkSheet1F.Cells[1, 1, 1 + ExcelData1F.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = exWorkSheet1F.Cells[chartHeaderF, 3, chartHeaderF + 4, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        #endregion

                        #region Side Letter
                        var objSL = objStatutory.Where(get1 => get1.InvestorID != null).ToList();
                        int compliedCountSL = objSL.Where(entry => entry.ResultStatusID == 2).ToList().Count;
                        int NotcompliedCountSL = objSL.Where(a => a.ResultStatusID == 3).ToList().Count;
                        int AppliANDNotApplicableCountSL = objSL.Where(b => b.ResultStatusID == 1).ToList().Count;
                        int totalPointsSL = compliedCountSL + NotcompliedCountSL + AppliANDNotApplicableCountSL;
                        string ReportNameAddSL = "Side Letter";

                        ExcelWorksheet exWorkSheet1SL = exportPackge.Workbook.Worksheets.Add(ReportNameAddSL);
                        System.Data.DataTable ExcelData1SL = null;
                        DataTable dataToExportSL = CannedReportManagement.LINQResultToDataTable(objSL);
                        DataView view1SL = new System.Data.DataView(dataToExportSL);

                        ExcelData1SL = view1SL.ToTable("Selected", false, "SNO", "Clause_ref_num", "Comp_Que", "ResultStatus", "Remarks");
                        int cnt12 = 1;
                        #region Count
                        foreach (DataRow item12 in ExcelData1SL.Rows)
                        {
                            item12["SNO"] = cnt12;
                            cnt12++;
                        }
                        #endregion
                        exWorkSheet1SL.Cells["A1"].LoadFromDataTable(ExcelData1SL, true);

                        exWorkSheet1SL.Cells["A1"].Value = "SNO";
                        exWorkSheet1SL.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1SL.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet1SL.Cells["A1"].AutoFitColumns(5);

                        exWorkSheet1SL.Cells["B1"].Value = "Relevant Clause Reference";
                        exWorkSheet1SL.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1SL.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet1SL.Cells["B1"].AutoFitColumns(50);

                        exWorkSheet1SL.Cells["C1"].Value = "Compliance Question relating to agreed Term/Conditions";
                        exWorkSheet1SL.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1SL.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet1SL.Cells["C1"].AutoFitColumns(50);

                        exWorkSheet1SL.Cells["D1"].Value = "Status";
                        exWorkSheet1SL.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1SL.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet1SL.Cells["D1"].AutoFitColumns(20);

                        exWorkSheet1SL.Cells["E1"].Value = "Remark";
                        exWorkSheet1SL.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1SL.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet1SL.Cells["E1"].AutoFitColumns(50);

                        exWorkSheet1SL.Column(1).Width = 21;
                        exWorkSheet1SL.Column(2).Width = 25;
                        exWorkSheet1SL.Column(3).Width = 40;
                        exWorkSheet1SL.Column(4).Width = 20;
                        exWorkSheet1SL.Column(5).Width = 20;

                        #region  chart
                        int chartstartSL = ExcelData1SL.Rows.Count + 3;

                        int chartHeaderSL = chartstartSL + 2;
                        string headerSL = "C" + chartHeaderSL;
                        string header1SL = "D" + chartHeaderSL;
                        string header2SL = "E" + chartHeaderSL;

                        string headermergeSL = headerSL + ":" + header2SL;

                        exWorkSheet1SL.Cells[headerSL].Value = "Summary of Compliance Status";
                        exWorkSheet1SL.Cells[headermergeSL].Merge = true;
                        exWorkSheet1SL.Cells[headermergeSL].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1SL.Cells[headerSL].Style.WrapText = true;


                        chartstartSL = chartstartSL + 3;
                        int rangeSL = (chartstartSL + 1);
                        int rangenextSL = (rangeSL + 1);
                        int rangenext1SL = (rangenextSL + 1);

                        string xSL = "C" + chartstartSL;
                        string ySL = "C" + rangeSL;
                        string zSL = "C" + rangenextSL;
                        string tSL = "C" + rangenext1SL;

                        string percenatgeSL = "E" + chartstartSL;
                        string percenatge1SL = "E" + rangeSL;
                        string percenatge2SL = "E" + rangenextSL;

                        string x1SL = "D" + chartstartSL;
                        string y1SL = "D" + rangeSL;
                        string z1SL = "D" + rangenextSL;
                        string t1SL = "D" + rangenext1SL;

                        exWorkSheet1SL.Cells[percenatgeSL].Value = string.Format("{0:0.00}%", ((double)compliedCountSL / (double)totalPointsSL) * 100);
                        exWorkSheet1SL.Cells[percenatge1SL].Value = string.Format("{0:0.00}%", ((double)NotcompliedCountSL / (double)totalPointsSL) * 100);
                        exWorkSheet1SL.Cells[percenatge2SL].Value = string.Format("{0:0.00}%", ((double)AppliANDNotApplicableCountSL / (double)totalPointsSL) * 100);

                        exWorkSheet1SL.Cells[x1SL].Value = compliedCountSL;
                        exWorkSheet1SL.Cells[y1SL].Value = NotcompliedCountSL;
                        exWorkSheet1SL.Cells[z1SL].Value = AppliANDNotApplicableCountSL;
                        exWorkSheet1SL.Cells[t1SL].Value = totalPointsSL;

                        exWorkSheet1SL.Cells[xSL].Value = "Complied";
                        exWorkSheet1SL.Cells[xSL].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1SL.Cells[xSL].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                        exWorkSheet1SL.Cells[xSL].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[xSL].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[xSL].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[xSL].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[xSL].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                        exWorkSheet1SL.Cells[ySL].Value = "Not Complied";
                        exWorkSheet1SL.Cells[ySL].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1SL.Cells[ySL].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        exWorkSheet1SL.Cells[ySL].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[ySL].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[ySL].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[ySL].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[ySL].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1SL.Cells[zSL].Value = "Not Applicable";
                        exWorkSheet1SL.Cells[zSL].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1SL.Cells[zSL].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                        exWorkSheet1SL.Cells[zSL].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[zSL].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[zSL].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[zSL].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1SL.Cells[zSL].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1SL.Cells[tSL].Value = "Total";

                        var myChartSL = exWorkSheet1SL.Drawings.AddChart("chart", eChartType.Pie3D);

                        string sSL = x1SL + ":" + z1SL;
                        string s1SL = xSL + ":" + zSL;

                        // Define series for the chart
                        var seriesSL = myChartSL.Series.Add(sSL, s1SL);


                        var pieSeriesSL = (ExcelPieChartSerie)seriesSL;
                        pieSeriesSL.Explosion = 5;

                        pieSeriesSL.DataLabel.ShowPercent = true;
                        pieSeriesSL.DataLabel.ShowLeaderLines = true;
                        pieSeriesSL.DataLabel.Position = eLabelPosition.BestFit;

                        myChartSL.Border.Fill.Color = System.Drawing.Color.Green;
                        myChartSL.Title.Text = "Summary of Compliance Status";
                        //myChart.Title.Text = "My Chart";
                        myChartSL.SetSize(400, 400);
                        // Add to 6th row and to the 6th column
                        myChartSL.SetPosition(chartstartSL + 13, 0, 2, 0);

                        #endregion

                        using (ExcelRange col = exWorkSheet1SL.Cells[1, 1, 1 + ExcelData1SL.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = exWorkSheet1SL.Cells[chartHeaderSL, 3, chartHeaderSL + 4, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        #endregion

                        var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                        string reportName = string.Empty;
                        reportName = "Compliance Report_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".xlsx";
                        string p_strPath = string.Empty;
                        string dirpath = string.Empty;
                        p_strPath = @"" + storagedrive + "/" + reportName;
                        dirpath = @"" + storagedrive;
                        if (!Directory.Exists(dirpath))
                        {
                            Directory.CreateDirectory(dirpath);
                        }
                        FileStream objFileStrm = System.IO.File.Create(p_strPath);
                        objFileStrm.Close();
                        System.IO.File.WriteAllBytes(p_strPath, exportPackge.GetAsByteArray());
                        ReturnPath = reportName;
                    }
                }
                else
                {
                    ReturnPath = "No Record Found";
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Content("Error");
            }
        }

        public ActionResult FundDocumentClosedUpdate(int AID, int SID, int CID, int UID)
        {
            try
            {
                bool flagoutput = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var totalchecklist = (from row in entities.CompFund_tbl_ChecklistMapping
                                          where row.CheckListInstanceId == AID
                                          && row.IsDeleted == false
                                          && row.ScheduleOnId == SID
                                          select row).ToList();

                    totalchecklist = totalchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();

                    var closedchecklist = (from row in entities.CompFund_tbl_ChecklistAuditTransaction
                                           where row.checklistAuditInstanceID == AID
                                           && row.ChecklistAuditScheduleOnID == SID
                                           && row.StatusId == 4
                                           select row).ToList();

                    closedchecklist = closedchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();

                    if (totalchecklist.Count == closedchecklist.Count)
                    {
                        CompFund_tbl_ChecklistAuditScheduleOn Data = (from row in entities.CompFund_tbl_ChecklistAuditScheduleOn
                                                                      where row.CustomerID == CID
                                                                      && row.ChecklistAuditInstanceID == AID
                                                                      && row.ID == SID
                                                                      && row.IsActive == true
                                                                      select row).FirstOrDefault();
                        if (Data != null)
                        {
                            Data.AuditStatusID = 4;
                            Data.ScheduleOnEndDate = DateTime.Now;
                            entities.SaveChanges();
                            flagoutput = true;
                        }
                    }
                }
                if (flagoutput == true)
                {
                    return Json(new { message = "Save Record" });
                }
                else
                {
                    return Json(new { message = "Record Not Save" });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return Json(new { message = "Error" });
            }
        }

        public ActionResult SaveUnAssignCheckList(int UID, string things, int branchId, int PID, int RID, int Year, string FreqId, string Period, string docID, int CID, string InvestID)
        {
            try
            {
                List<int> ChecklistMapIds = new List<int>();
                if (things != null)
                {
                    System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(things);
                    if (Ids.Count != 0)
                    {
                        foreach (var item in Ids)
                        {
                            ChecklistMapIds.Add(item);
                        }
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    foreach (var item in ChecklistMapIds)
                    {
                        CompFund_tbl_ChecklistMapping Data = (from row in entities.CompFund_tbl_ChecklistMapping
                                                              where row.CustomerID == CID
                                                              && row.Id == item
                                                              && row.IsDeleted == false
                                                              select row).FirstOrDefault();
                        if (Data != null)
                        {
                            Data.IsDeleted = true;
                            Data.UpdatedBy = UID;
                            Data.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
                return Json(new { message = "Save Record" });
                //return Content("Save Record");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                //return Content("Error");
                return Json(new { message = "Error" });
            }
        }

        public ActionResult SaveAssignCheckList(int UID, string things, int branchId, int PID, int RID, int Year, string FreqId, string Period, string docID, int CID, string InvestID)
        {
            try
            {
                List<int> ChecklistIds = new List<int>();
                if (things != null)
                {
                    System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(things);
                    if (Ids.Count != 0)
                    {
                        foreach (var item in Ids)
                        {
                            ChecklistIds.Add(item);
                        }
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CompFund_tbl_ChecklistAuditInstance objinstanceID = new CompFund_tbl_ChecklistAuditInstance
                    {
                        CustomerBranchID = branchId,
                        DocumentType = docID,
                        Year = Year,
                        Frequency = FreqId,
                        Period = Period,
                        CreatedBy = UID,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        CustomerID = CID,
                        PerformerID = PID,
                        ReviewerID = RID
                    };
                    bool IsflagInvestor = false;
                    if (docID == "SL")
                    {
                        if (!string.IsNullOrEmpty(InvestID) && InvestID != "null" && InvestID != "-1")
                        {
                            if (Convert.ToInt32(InvestID) > 0)
                            {
                                IsflagInvestor = true;
                                objinstanceID.InvestorID = Convert.ToInt32(InvestID);
                            }
                        }
                    }

                    long ChecklistAuditInstanceID = FundMasterManagement.UpdateChecklistInstance(objinstanceID, IsflagInvestor);
                    if (ChecklistAuditInstanceID > 0)
                    {
                        ChecklistIds = ChecklistIds.Distinct().ToList();

                        foreach (var item in ChecklistIds)
                        {

                            #region Scheduled ON Details
                            long SchdeuledONID = -1;
                            CompFund_tbl_ChecklistAuditScheduleOn objSCList = (from row in entities.CompFund_tbl_ChecklistAuditScheduleOn
                                                                               where row.ID == ChecklistAuditInstanceID
                                                                               select row).FirstOrDefault();
                            if (objSCList == null)
                            {
                                CompFund_tbl_ChecklistAuditScheduleOn complianceScheduleon = new CompFund_tbl_ChecklistAuditScheduleOn();
                                complianceScheduleon.CustomerID = CID;
                                complianceScheduleon.ChecklistAuditInstanceID = ChecklistAuditInstanceID;
                                complianceScheduleon.ScheduleOn = null;
                                complianceScheduleon.ForMonth = Period;
                                complianceScheduleon.ForYear = Year;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.CustomerBranchID = branchId;
                                complianceScheduleon.AuditStatusID = 1;
                                complianceScheduleon.PID = 1;
                                complianceScheduleon.RID = 2;
                                entities.CompFund_tbl_ChecklistAuditScheduleOn.Add(complianceScheduleon);
                                entities.SaveChanges();

                                string UserName = string.Empty;
                                User users = (from row in entities.Users
                                              where row.ID == UID
                                              && row.IsDeleted == false
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                                if (users != null)
                                {
                                    UserName = users.FirstName + " " + users.LastName;
                                }
                                CompFund_tbl_ChecklistAuditTransaction transaction = new CompFund_tbl_ChecklistAuditTransaction()
                                {
                                    checklistAuditInstanceID = ChecklistAuditInstanceID,
                                    ChecklistAuditScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = UID,
                                    CreatedByText = UserName,
                                    CustomerBranchId = branchId,
                                    StatusId = 1,
                                    Dated = DateTime.Now,
                                    Remarks = "New Fund Document checklist assigned.",
                                    CheckListId = item
                                };
                                FundMasterManagement.UpdateChecklistTransaction(transaction);
                                //entities.CompFund_tbl_ChecklistAuditTransaction.Add(transaction);
                                //entities.SaveChanges();
                                SchdeuledONID = complianceScheduleon.ID;
                            }
                            else
                            {
                                string UserName = string.Empty;
                                User users = (from row in entities.Users
                                              where row.ID == UID
                                              && row.IsDeleted == false
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                                if (users != null)
                                {
                                    UserName = users.FirstName + " " + users.LastName;
                                }
                                CompFund_tbl_ChecklistAuditTransaction transaction = new CompFund_tbl_ChecklistAuditTransaction()
                                {
                                    checklistAuditInstanceID = ChecklistAuditInstanceID,
                                    ChecklistAuditScheduleOnID = objSCList.ID,
                                    CreatedBy = UID,
                                    CreatedByText = UserName,
                                    CustomerBranchId = branchId,
                                    StatusId = 1,
                                    Dated = DateTime.Now,
                                    Remarks = "New Fund Document checklist assigned.",
                                    CheckListId = item
                                };
                                FundMasterManagement.UpdateChecklistTransaction(transaction);
                                SchdeuledONID = objSCList.ID;
                                objSCList.IsActive = true;
                                entities.SaveChanges();
                            }
                            #endregion


                            CompFund_tbl_ChecklistAssignment PAss = new CompFund_tbl_ChecklistAssignment
                            {
                                ChechklistInstanceID = Convert.ToInt32(ChecklistAuditInstanceID),
                                RoleID = 3,
                                UserID = PID
                            };
                            FundMasterManagement.UpdateChecklistAssignment(PAss);

                            CompFund_tbl_ChecklistAssignment RAss = new CompFund_tbl_ChecklistAssignment
                            {
                                ChechklistInstanceID = Convert.ToInt32(ChecklistAuditInstanceID),
                                RoleID = 4,
                                UserID = RID
                            };
                            FundMasterManagement.UpdateChecklistAssignment(RAss);

                            CompFund_tbl_ChecklistMapping objMappID = new CompFund_tbl_ChecklistMapping
                            {
                                CustomerID = CID,
                                CheckListInstanceId = Convert.ToInt32(ChecklistAuditInstanceID),
                                CheckListId = item,
                                CreatedBy = UID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false,
                                ScheduleOnId = SchdeuledONID
                            };
                            FundMasterManagement.UpdateChecklistMapping(objMappID);
                        }
                    }
                }
                return Json(new { message = "Save Record" });
                //return Content("Save Record");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return Json(new { message = "Error" });
            }
        }

        #endregion

        public static string designation(long userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var designationdata = (from row in entities.Users
                                       where row.ID == userid
                                       select row.Designation).SingleOrDefault();
                return designationdata;
            }
        }

        public static string GetDeptName(string DeptID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                if (deptIds.Count != 0)
                {
                    var designationdata = (from row in entities.Departments
                                           where deptIds.Contains(row.ID)
                                           select row.Name).FirstOrDefault();
                    return designationdata;
                }
                return "";
            }
        }

        public static string UserName(long userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var designationdata = (from row in entities.Users
                                       where row.ID == userid
                                       select row.FirstName + " "+ row.LastName).FirstOrDefault();
                return designationdata;
            }
        }
        public static string branchName(long userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var designationdata = (from row in entities.Users
                                       join row1 in entities.CustomerBranches 
                                       on row.CustomerBranchID equals row1.ID
                                       where row.ID == userid
                                       select row1.Name).FirstOrDefault();
                return designationdata;
            }
        }

        public ActionResult Compliancecertificate(int UserId, int CustomerID, string MonthId, string location
         , string risk, string StartDateDetail, string EndDateDetail, string DeptID, string CatID, string DdlUserID)
        {
            string ReturnPath = "";

            try
            {
                int catid = -1;
                int deptid = -1;
                int ddluserID = -1;
 
                 string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                DateTime currentdate1 = DateTime.Now;

                DateTime currentdate = currentdate1.Date;


                if (MonthId != "All")
                {
                    if (MonthId != "All")
                    {
                        if (MonthId == "1")
                            dtfrom = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            dtfrom = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            dtfrom = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            dtfrom = DateTime.Now.AddDays(-365);
                    }


                }
                else
                {
                    if (string.IsNullOrEmpty(StartDateDetail))
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(EndDateDetail))
                    {
                        dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                 select row.TemplateContent).FirstOrDefault();
                    if (data1 != null)
                    {

                        var data12 = (from row in entities.SP_ComplianceCertificateNew(CustomerID, catid, deptid, dtfrom, dtTo)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {
                            if (ddluserID != null && ddluserID != -1)
                            {
                                data12 = data12.Where(x => x.UserID == ddluserID).ToList();
                            }

                            if (location != null)
                            {
                                System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (Ids.Count != 0)
                                {
                                    data12 = data12.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                                }
                            }
                            if (DdlUserID != null)
                            {
                                System.Collections.Generic.IList<long> userIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<long>>(DdlUserID);
                                if (userIds.Count != 0)
                                {
                                    data12 = data12.Where(x => userIds.Contains(x.UserID)).ToList();
                                }
                            }
                            if (CatID != null)
                            {
                                System.Collections.Generic.IList<int> catIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(CatID);
                                if (catIds.Count != 0)
                                {
                                    data12 = data12.Where(x => catIds.Contains(x.CategoryID)).ToList();
                                }
                            }
                            if (DeptID != null)
                            {
                                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                                if (deptIds.Count != 0)
                                {
                                    data12 = data12.Where(x => deptIds.Contains(x.DeptID)).ToList();
                                }
                            }
                            if (risk != null)
                            {
                                System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                                if (riskIds.Count != 0)
                                {
                                    data12 = data12.Where(x => riskIds.Contains((int)x.risk)).ToList();
                                }
                            }

                            #region Complied Act List

                            List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.Name).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List

                            List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x => x.Name).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                            strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"ComplianceID");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Act Name");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Location");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Period");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Due Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Reviewer Remarks");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Close Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"</Tr>");
                            Compliedlist.ForEach(eachSection =>
                            {
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ComplianceID);
                                strExportforComplied.Append(@" </Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Name);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Location);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Formonth);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ScheduleOn.ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Remarks);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                            });
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied
                            var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"ComplianceID");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Act Name");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Location");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Period");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Due Date");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                            NonCompliedlist.ForEach(eachSection =>
                            {
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ComplianceID);
                                strExportforNonComplied.Append(@" </Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Name);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Location);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Formonth);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ScheduleOn.ToString("dd-MMM-yyyy"));
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                            });
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);
                            string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());
                            string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                            string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));

                            //string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));

                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");

                            byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                            string strFileName = "ComplianceCertificate" + ".doc";
                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, buffer);
                            ReturnPath = reportName;

                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }
  

        public ActionResult CompliancecertificateNew(int UserId, int CustomerID, string MonthId, string location
, string risk, string StartDateDetail, string EndDateDetail, string DeptID, string CatID, string DdlUserID, string toUser, string fromUser)
        {
            string ReturnPath = "";

            try
            {
                int catid = -1;
                int deptid = -1;
                int ddluserID = -1;

                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                DateTime currentdate1 = DateTime.Now;

                DateTime currentdate = currentdate1.Date;


                if (MonthId != "All")
                {
                    if (MonthId != "All")
                    {
                        if (MonthId == "1")
                            dtfrom = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            dtfrom = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            dtfrom = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            dtfrom = DateTime.Now.AddDays(-365);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(StartDateDetail))
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(EndDateDetail))
                    {
                        dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 360;

                    var ParentBrnachName = "";
                    var FromUser = "";
                    var ToUser = "";
                    var designationFrom = "";
                    var designationTo = "";
                    var deptName = "";

                    bool IsTagVisible = CustomerManagement.CheckForClient(CustomerID, "FromAndToTagCertificate");
                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                 select row.TemplateContent).FirstOrDefault();
                    if (data1 != null)
                    {

                        var data12 = (from row in entities.SP_ComplianceCertificateAll(CustomerID, dtfrom, dtTo, UserId)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {
                            if (IsTagVisible == false)
                            {
                                //if (DdlUserID != null && DdlUserID != "-1")
                                //{
                                //    data12 = data12.Where(x => x.UserID == Convert.ToInt64(DdlUserID)).ToList();
                                //}
                            }
                            else
                            {
                                if (fromUser != "" && fromUser != "-1")
                                {
                                    data12 = data12.Where(x => (x.PerformerID == Convert.ToInt64(fromUser)) || (x.ReviewerID == Convert.ToInt64(fromUser))).ToList();
                                }
                            }

                            if (location != null)
                            {
                                System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);

                                if (IsTagVisible == true)
                                {
                                    if (toUser != "")
                                    {
                                        ToUser = UserName(Convert.ToInt64(toUser));
                                        designationTo = designation(Convert.ToInt64(toUser));
                                    }

                                    if (fromUser != "")
                                    {
                                        FromUser = UserName(Convert.ToInt64(fromUser));
                                        designationFrom = designation(Convert.ToInt64(fromUser));
                                    }

                                    int? ParentBrnachID = 0;
                                    //Get Parent Brnach ID
                                    foreach (var item in Ids)
                                    {

                                        ParentBrnachID = (from row in entities.SP_GetParentBranchesFromChildNode(item, CustomerID)
                                                          where row.ParentID == null
                                                          select row.ID).FirstOrDefault();
                                        break;
                                    }

                                    if (ParentBrnachID != 0)
                                    {
                                        var BranchIDList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                            select row).ToList();

                                        var BranchIDListAssigned = (from row in entities.ComplianceInstances
                                                                    join row1 in entities.ComplianceAssignments
                                                                    on row.ID equals row1.ComplianceInstanceID
                                                                    join row2 in entities.Compliances
                                                                    on row.ComplianceId equals row2.ID
                                                                    where BranchIDList.Contains(row.CustomerBranchID)
                                                                    select row.CustomerBranchID).Distinct().ToList();


                                        if (Ids.Count() >= BranchIDListAssigned.Count())
                                        {
                                            ParentBrnachName = (from row in entities.SP_GetParentBranchesFromChildNode(ParentBrnachID, CustomerID)
                                                                where row.ParentID == null
                                                                select row.Name).FirstOrDefault();

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName + " - " + deptName;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            var ParentBrnachNameList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                                        join row1 in entities.CustomerBranches on row equals row1.ID
                                                                        where Ids.Contains((int)row)
                                                                        select row1.Name).ToList();


                                            foreach (var item in ParentBrnachNameList)
                                            {
                                                if (ParentBrnachName == "")
                                                {
                                                    //ParentBrnachName = item + ",";
                                                    ParentBrnachName = item;
                                                }
                                                else
                                                {
                                                    ParentBrnachName = ParentBrnachName + " , " + item;
                                                }
                                            }
                                            ParentBrnachName = ParentBrnachName.Trim(',');

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName + " - " + deptName;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (DeptID != null)
                                        {
                                            deptName = GetDeptName(DeptID);
                                            if (deptName != "")
                                            {
                                                ParentBrnachName = ParentBrnachName + " - " + deptName;
                                            }
                                        }

                                    }
                                }

                                if (Ids.Count != 0)
                                {
                                    data12 = data12.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                                }
                            }
                            if (DdlUserID != null)
                            {
                                System.Collections.Generic.IList<long?> userIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<long?>>(DdlUserID);
                                if (userIds.Count != 0)
                                {
                                    data12 = data12.Where(x => (x.PerformerID != null && userIds.Contains((long)x.PerformerID)) || (x.ReviewerID != null && userIds.Contains((long)x.ReviewerID)) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();

                                    //data12 = data12.Where(x => userIds.Contains((long)x.PerformerID) || userIds.Contains((long)x.ReviewerID) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();
                                }
                            }
                            if (CatID != null)
                            {
                                System.Collections.Generic.IList<int> catIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(CatID);
                                if (catIds.Count != 0)
                                {
                                    data12 = data12.Where(x => catIds.Contains(x.CategoryID)).ToList();
                                }
                            }
                            if (DeptID != null)
                            {
                                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                                if (deptIds.Count != 0)
                                {
                                    data12 = data12.Where(x => deptIds.Contains((int?)x.DeptID)).ToList();
                                }
                            }
                            if (risk != null)
                            {
                                System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                                if (riskIds.Count != 0)
                                {
                                    data12 = data12.Where(x => riskIds.Contains((int)x.risk)).ToList();
                                }
                            }


                            #region Complied Act List

                            List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.ActName).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List

                            List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x => x.ActName).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();

                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Short Description");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                //strExportforComplied.Append(@"<Td>");
                                //strExportforComplied.Append(@"Period");
                                //strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DivisionVertical);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    //strExportforComplied.Append(@"<Td>");
                                    //strExportforComplied.Append(eachSection.Formonth);
                                    //strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"ComplianceID");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Period");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ComplianceID);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Formonth);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied
                            var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Short Description");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                //strExportforNonComplied.Append(@"<Td>");
                                //strExportforNonComplied.Append(@"Period");
                                //strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DivisionVertical);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    //strExportforNonComplied.Append(@"<Td>");
                                    //strExportforNonComplied.Append(eachSection.Formonth);
                                    //strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"ComplianceID");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Period");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ComplianceID);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Formonth);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion

                            }
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            //#region Complied

                            //var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            //System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                            //strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            //strExportforComplied.Append(@"<Tr>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"ComplianceID");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Act Name");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Location");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Performer");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Reviewer");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Period");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Due Date");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Reviewer Remarks");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"<Td>");
                            //strExportforComplied.Append(@"Close Date");
                            //strExportforComplied.Append(@"</Td>");
                            //strExportforComplied.Append(@"</Tr>");
                            //Compliedlist.ForEach(eachSection =>
                            //{
                            //    strExportforComplied.Append(@"<Tr>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.ComplianceID);
                            //    strExportforComplied.Append(@" </Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.ActName);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.Location);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.PerformerName);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.ReviewerName);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.Formonth);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.Remarks);
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"<Td>");
                            //    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                            //    strExportforComplied.Append(@"</Td>");
                            //    strExportforComplied.Append(@"</Tr>");
                            //});
                            //strExportforComplied.Append(@"</Table>");

                            //#endregion

                            //#region non Complied
                            //var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            //System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            //strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            //strExportforNonComplied.Append(@"<Tr>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"ComplianceID");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Act Name");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Location");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Performer");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Reviewer");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Period");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"<Td>");
                            //strExportforNonComplied.Append(@"Due Date");
                            //strExportforNonComplied.Append(@"</Td>");
                            //strExportforNonComplied.Append(@"</Tr>");
                            //NonCompliedlist.ForEach(eachSection =>
                            //{
                            //    strExportforNonComplied.Append(@"<Tr>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.ComplianceID);
                            //    strExportforNonComplied.Append(@" </Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.ActName);
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.Location);
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.PerformerName);
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.ReviewerName);
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.Formonth);
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"<Td>");
                            //    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            //    strExportforNonComplied.Append(@"</Td>");
                            //    strExportforNonComplied.Append(@"</Tr>");
                            //});
                            //strExportforNonComplied.Append(@"</Table>");
                            //#endregion

                            string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);
                            string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());

                            string replacebranchList = "";
                            string replaceFromUserList = "";
                            string replaceToUserList = "";
                            string replaceToDesig = "";
                            string replaceFromDesig = "";
                            if (IsTagVisible == true)
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", ParentBrnachName);
                                replaceToUserList = replacebranchList.Replace("{{ToUser}}", ToUser);
                                replaceFromUserList = replaceToUserList.Replace("{{FromUser}}", FromUser);
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", designationTo);
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", designationFrom);
                            }
                            else
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", " ");
                                replaceFromUserList = replacebranchList.Replace("{{ToUser}}", " ");
                                replaceToUserList = replaceFromUserList.Replace("{{FromUser}}", " ");
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", " ");
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", " ");
                            }

                            string relacebranch = replaceFromDesig.Replace("{{BranchName}}", BranchName);
                            string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));

                            //string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);

                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");

                            byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                            //string strFileName = "ComplianceCertificate" + ".doc";
                            string strFileName = "ComplianceCertificate";
                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, buffer);
                            ReturnPath = reportName;

                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }

        public ActionResult Compliancecertificatepdf(int UserId, int CustomerID, string MonthId, string location, string risk, 
            string StartDateDetail, string EndDateDetail, string DeptID, string CatID, string DdlUserID)
        {
            string ReturnPath = "";

            try
            {
                int catid = -1;
                int deptid = -1;
                int ddluserID = -1;

                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                DateTime currentdate1 = DateTime.Now;

                DateTime currentdate = currentdate1.Date;


                if (MonthId != "All")
                {
                    if (MonthId != "All")
                    {
                        if (MonthId == "1")
                            dtfrom = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            dtfrom = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            dtfrom = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            dtfrom = DateTime.Now.AddDays(-365);
                    }


                }
                else
                {
                    if (string.IsNullOrEmpty(StartDateDetail))
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(EndDateDetail))
                    {
                        dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                 select row.TemplateContent).FirstOrDefault();
                    if (data1 != null)
                    {

                        var data12 = (from row in entities.SP_ComplianceCertificateAll(CustomerID, dtfrom, dtTo, UserId)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {
                            if (ddluserID != null && ddluserID != -1)
                            {
                                data12 = data12.Where(x => x.UserID == ddluserID).ToList();
                            }

                            if (location != null)
                            {
                                System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);
                                if (Ids.Count != 0)
                                {
                                    data12 = data12.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                                }
                            }
                            if (DdlUserID != null)
                            {
                                System.Collections.Generic.IList<long?> userIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<long?>>(DdlUserID);
                                if (userIds.Count != 0)
                                {
                                    data12 = data12.Where(x => (x.PerformerID != null && userIds.Contains((long)x.PerformerID)) || (x.ReviewerID != null && userIds.Contains((long)x.ReviewerID)) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();

                                    //data12 = data12.Where(x => userIds.Contains((long)x.PerformerID) || userIds.Contains((long)x.ReviewerID) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();
                                }
                            }
                            if (CatID != null)
                            {
                                System.Collections.Generic.IList<int> catIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(CatID);
                                if (catIds.Count != 0)
                                {
                                    data12 = data12.Where(x => catIds.Contains(x.CategoryID)).ToList();
                                }
                            }
                            if (DeptID != null)
                            {
                                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                                if (deptIds.Count != 0)
                                {
                                    data12 = data12.Where(x => deptIds.Contains((int?)x.DeptID)).ToList();
                                }
                            }
                            if (risk != null)
                            {
                                System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                                if (riskIds.Count != 0)
                                {
                                    data12 = data12.Where(x => riskIds.Contains((int)x.risk)).ToList();
                                }
                            }


                            #region Complied Act List

                            List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.ActName).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List

                            List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x => x.ActName).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                            strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"ComplianceID");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Act Name");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Location");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Performer");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Reviewer");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Period");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Due Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Reviewer Remarks");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Close Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"</Tr>");
                            Compliedlist.ForEach(eachSection =>
                            {
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ComplianceID);
                                strExportforComplied.Append(@" </Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ActName);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Location);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.PerformerName);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ReviewerName);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Formonth);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Remarks);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                            });
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied
                            var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"ComplianceID");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Act Name");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Location");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Performer");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Reviewer");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Period");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Due Date");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                            NonCompliedlist.ForEach(eachSection =>
                            {
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ComplianceID);
                                strExportforNonComplied.Append(@" </Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ActName);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Location);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.PerformerName);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ReviewerName);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Formonth);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                            });
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);
                            string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());
                            string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                            string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));
                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");

                            byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                            string strFileName = "ComplianceCertificate";
                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".pdf";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, buffer);
 
                            ReturnPath = reportName;

                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }
 
        public ActionResult CompliancecePreview(int UserId, int CustomerID, string MonthId, string location
      , string risk, string StartDateDetail, string EndDateDetail, string DeptID, string CatID, string DdlUserID, string toUser, string fromUser)
        {
            string ReturnPath = "";

            try
            {
                int catid = -1;
                int deptid = -1;
                int ddluserID = -1;

                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                DateTime currentdate1 = DateTime.Now;

                DateTime currentdate = currentdate1.Date;


                if (MonthId != "All")
                {
                    if (MonthId != "All")
                    {
                        if (MonthId == "1")
                            dtfrom = DateTime.Now.AddDays(-30);

                        if (MonthId == "3")
                            dtfrom = DateTime.Now.AddDays(-90);

                        if (MonthId == "6")
                            dtfrom = DateTime.Now.AddDays(-180);

                        if (MonthId == "12")
                            dtfrom = DateTime.Now.AddDays(-365);
                    }


                }
                else
                {
                    if (string.IsNullOrEmpty(StartDateDetail))
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(EndDateDetail))
                    {
                        dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 360;
                    bool IsTagVisible = CustomerManagement.CheckForClient(CustomerID, "FromAndToTagCertificate");
                    var ParentBrnachName = "";
                    var FromUser = "";
                    var ToUser = "";
                    var designationFrom = "";
                    var designationTo = "";
                    var deptName = "";

                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                    && row.IsActive == true
                                 select row.TemplateContent).FirstOrDefault();
                    if (data1 != null)
                    {

                        var data12 = (from row in entities.SP_ComplianceCertificateAll(CustomerID, dtfrom, dtTo, UserId)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {
                            if (IsTagVisible == false)
                            {
                                if (ddluserID != null && ddluserID != -1)
                                {
                                    data12 = data12.Where(x => x.UserID == ddluserID).ToList();
                                }
                            }
                            else
                            {
                                if (fromUser != "" && fromUser != "-1")
                                {
                                    data12 = data12.Where(x => (x.PerformerID == Convert.ToInt64(fromUser)) || (x.ReviewerID == Convert.ToInt64(fromUser))).ToList();
                                }
                            }


                            if (location != null)
                            {
                                System.Collections.Generic.IList<int> Ids = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(location);

                                if (IsTagVisible == true)
                                {
                                    if (toUser != "")
                                    {
                                        ToUser = UserName(Convert.ToInt64(toUser));
                                        designationTo = designation(Convert.ToInt64(toUser));
                                    }

                                    if (fromUser != "")
                                    {
                                        FromUser = UserName(Convert.ToInt64(fromUser));
                                        designationFrom = designation(Convert.ToInt64(fromUser));
                                    }

                                    int? ParentBrnachID = 0;
                                    //Get Parent Brnach ID
                                    foreach (var item in Ids)
                                    {

                                        ParentBrnachID = (from row in entities.SP_GetParentBranchesFromChildNode(item, CustomerID)
                                                          where row.ParentID == null
                                                          select row.ID).FirstOrDefault();
                                        break;
                                    }

                                    if (ParentBrnachID != 0)
                                    {
                                        var BranchIDList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                            select row).ToList();

                                        var BranchIDListAssigned = (from row in entities.ComplianceInstances
                                                                    join row1 in entities.ComplianceAssignments
                                                                    on row.ID equals row1.ComplianceInstanceID
                                                                    join row2 in entities.Compliances
                                                                    on row.ComplianceId equals row2.ID
                                                                    where BranchIDList.Contains(row.CustomerBranchID)
                                                                    select row.CustomerBranchID).Distinct().ToList();


                                        if (Ids.Count() >= BranchIDListAssigned.Count())
                                        {
                                            ParentBrnachName = (from row in entities.SP_GetParentBranchesFromChildNode(ParentBrnachID, CustomerID)
                                                                where row.ParentID == null
                                                                select row.Name).FirstOrDefault();

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName + " - " + deptName;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            var ParentBrnachNameList = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(ParentBrnachID, CustomerID)
                                                                        join row1 in entities.CustomerBranches on row equals row1.ID
                                                                        where Ids.Contains((int)row)
                                                                        select row1.Name).ToList();


                                            foreach (var item in ParentBrnachNameList)
                                            {
                                                if (ParentBrnachName == "")
                                                { 
                                                    ParentBrnachName = item;
                                                }
                                                else
                                                {
                                                    ParentBrnachName = ParentBrnachName + " , " + item;
                                                }

                                            }
                                            ParentBrnachName = ParentBrnachName.Trim(',');

                                            if (DeptID != null)
                                            {
                                                deptName = GetDeptName(DeptID);
                                                if (deptName != "")
                                                {
                                                    ParentBrnachName = ParentBrnachName + " - " + deptName;
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (DeptID != null)
                                        {
                                            deptName = GetDeptName(DeptID);
                                            if (deptName != "")
                                            {
                                                ParentBrnachName = ParentBrnachName + " - " + deptName;
                                            }
                                        }

                                    }
                                }


                                if (Ids.Count != 0)
                                {
                                    data12 = data12.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                                }
                            }
                            if (DdlUserID != null)
                            {
                                System.Collections.Generic.IList<long?> userIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<long?>>(DdlUserID);
                                if (userIds.Count != 0)
                                {
                                    data12 = data12.Where(x => userIds.Contains((long)x.PerformerID) || userIds.Contains((long)x.ReviewerID) || (x.ApprovalID != null && userIds.Contains((long)x.ApprovalID))).ToList();
                                }
                            }
                            if (CatID != null)
                            {
                                System.Collections.Generic.IList<int> catIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(CatID);
                                if (catIds.Count != 0)
                                {
                                    data12 = data12.Where(x => catIds.Contains(x.CategoryID)).ToList();
                                }
                            }
                            if (DeptID != null)
                            {
                                System.Collections.Generic.IList<int?> deptIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int?>>(DeptID);
                                if (deptIds.Count != 0)
                                {
                                    data12 = data12.Where(x => deptIds.Contains((int?)x.DeptID)).ToList();
                                }
                            }
                            if (risk != null)
                            {
                                System.Collections.Generic.IList<int> riskIds = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<int>>(risk);
                                if (riskIds.Count != 0)
                                {
                                    data12 = data12.Where(x => riskIds.Contains((int)x.risk)).ToList();
                                }
                            }


                            #region Complied Act List

                            List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.ActName).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List

                            List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x => x.ActName).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();

                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Short Description");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                //strExportforComplied.Append(@"<Td>");
                                //strExportforComplied.Append(@"Period");
                                //strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DivisionVertical);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    //strExportforComplied.Append(@"<Td>");
                                    //strExportforComplied.Append(eachSection.Formonth);
                                    //strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"ComplianceID");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Act Name");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Location");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Department");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Performer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Period");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Due Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Reviewer Remarks");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(@"Close Date");
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"</Tr>");
                                Compliedlist.ForEach(eachSection =>
                                {
                                    strExportforComplied.Append(@"<Tr>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ComplianceID);
                                    strExportforComplied.Append(@" </Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ActName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Location);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.DeptName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.PerformerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ReviewerName);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Formonth);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Remarks);
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(eachSection.Closure.ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                    strExportforComplied.Append(@"</Tr>");
                                });
                                #endregion
                            }
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied
                            var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            if (IsTagVisible == true)
                            {
                                #region Sterlite Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Short Description");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                //strExportforNonComplied.Append(@"<Td>");
                                //strExportforNonComplied.Append(@"Period");
                                //strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DivisionVertical);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    //strExportforNonComplied.Append(@"<Td>");
                                    //strExportforNonComplied.Append(eachSection.Formonth);
                                    //strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion
                            }
                            else
                            {
                                #region Standard Template
                                strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"ComplianceID");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Act Name");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Location");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Department");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Performer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Reviewer");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Period");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(@"Due Date");
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                                NonCompliedlist.ForEach(eachSection =>
                                {
                                    strExportforNonComplied.Append(@"<Tr>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ComplianceID);
                                    strExportforNonComplied.Append(@" </Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ActName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Location);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.DeptName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.PerformerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ReviewerName);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.Formonth);
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"<Td>");
                                    strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                    strExportforNonComplied.Append(@"</Td>");
                                    strExportforNonComplied.Append(@"</Tr>");

                                });
                                #endregion

                            }
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = "";
                            if (EndDateDetail != null)
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                            }
                            else
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                            }
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = "";
                            if (EndDateDetail != null)
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                            }
                            else
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                            }
                            string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());


                            //bool IsTagVisible = CustomerManagement.CheckForClient(CustomerID, "FromAndToTagCertificate");
                            string replacebranchList = "";
                            string replaceFromUserList = "";
                            string replaceToUserList = "";
                            string replaceToDesig = "";
                            string replaceFromDesig = "";
                            if (IsTagVisible == true)
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", ParentBrnachName);
                                replaceToUserList = replacebranchList.Replace("{{ToUser}}", ToUser);
                                replaceFromUserList = replaceToUserList.Replace("{{FromUser}}", FromUser);
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", designationTo);
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", designationFrom);
                            }
                            else
                            {
                                replacebranchList = relacedata.Replace("{{BranchList}}", " ");
                                replaceFromUserList = replacebranchList.Replace("{{ToUser}}", " ");
                                replaceToUserList = replaceFromUserList.Replace("{{FromUser}}", " ");
                                replaceToDesig = replaceFromUserList.Replace("{{ToDesignation}}", " ");
                                replaceFromDesig = replaceToDesig.Replace("{{FromDesignation}}", " ");
                            }

                            string relacebranch = replaceFromDesig.Replace("{{BranchName}}", BranchName);
                            string CurrentDateNew = relacebranch.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));
                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");


                            ReturnPath = strHTMLContent.ToString();

                        }
                    }
                }
                return Content(ReturnPath);
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }
 
        private void ShowDocument(string fileName, byte[] fileContent)
        {
            //Split the string by character . to get file extension type  
            string[] stringParts = fileName.Split(new char[] { '.' });
            string strType = stringParts[1];
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //Set the content type as file extension type  
            Response.ContentType = strType;
            //Write the file content  
            this.Response.BinaryWrite(fileContent);
            this.Response.End();
        }
        public static int GetCustomizedCustomerid(int CustId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "TaxReportFields"
                            && row.ClientID == CustId
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }

        public static void DeleteCertificateTransaction(int? UserID, int CerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var TranID = (from row in entities.Certificate_Transaction
                              where row.UserID == UserID
                              && row.Cer_ScheduleOnID == CerID
                              select row.ID).FirstOrDefault();

                entities.Certificate_transactionSchedule.RemoveRange(entities.Certificate_transactionSchedule.Where(x => x.Cer_ScheduleOnID == CerID
                  && x.TransactionID == TranID));

                entities.SaveChanges();

                var query = (from row in entities.Certificate_Transaction
                             where row.UserID == UserID
                             && row.Cer_ScheduleOnID == CerID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    entities.Certificate_Transaction.Remove(query);
                    entities.SaveChanges();
                }
            }
        }

        public ActionResult GetContractCommentedFile(string userpath)
        {
            string p_strPath = string.Empty;
            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["DownloadContractCommentFile"]);
            p_strPath = @"" + storagedrive + "/" + userpath;

            string Filename = Path.GetFileName(p_strPath);
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename);
            Response.BinaryWrite(DocumentManagement.ReadDocFiles(p_strPath)); // create the file
            Response.Flush(); // send it to the client to download
            HttpContext.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            return View();
        }
 
        public ActionResult CertificateSubmit(int UserId, int CustomerID, int SID)
        {
            string result = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DeleteCertificateTransaction(UserId, SID);

                    entities.Certificate_SP_OwnerSachin(UserId, CustomerID);
                    List<Certificate_SP_OwnerFirst_Result> Uobj = (from row in entities.Certificate_SP_OwnerFirst(UserId, CustomerID)
                            select row).ToList();

                    var Uobjadd = Uobj.Where(x => x.CerScheduleonID == SID).FirstOrDefault();
                    if (Uobj != null)
                    {
                        int Timely = 0;
                        int Delay = 0;
                        int Open = 0;
                        int submissionStatusPer = 0;

                        if (Uobjadd.ComplianceStatusClosedTimelyPer != null)
                            Timely = (int)Uobjadd.ComplianceStatusClosedTimelyPer;

                        //if (Uobjadd.ComplianceStatusClosedDelayedPer != null)
                        //    Delay = (int)Uobjadd.ComplianceStatusClosedDelayedPer;

                        if (Uobjadd.SubmissionStatusPer != null)
                            submissionStatusPer = (int)Uobjadd.SubmissionStatusPer;

                        if (Uobjadd.ComplianceStatusOpenPer != null)
                            Open = (int)Uobjadd.ComplianceStatusOpenPer;

                        int TransactionId = 0;
                        Certificate_Transaction output = (from row in entities.Certificate_Transaction
                                                          where row.Cer_ScheduleOnID == SID 
                                                          && row.UserID == UserId
                                                          select row).FirstOrDefault();
                        if (output == null)
                        {
                            Certificate_Transaction obj1 = new Certificate_Transaction();
                            obj1.Cer_ScheduleOnID = SID;
                            obj1.IsSubmitOwner = 1;
                            obj1.SubmitedDate = DateTime.Now;
                            obj1.SubmitedPer = submissionStatusPer;
                            obj1.SubmitedTimely = Timely;
                            obj1.SubmitedDelay = Delay;
                            obj1.SubmitedOpen = Open;
                            obj1.UserID = UserId;
                            obj1.CreatedOn = DateTime.Now;
                            entities.Certificate_Transaction.Add(obj1);
                            entities.SaveChanges();

                            TransactionId = obj1.ID; 

                            List<Certificate_SP_OwnerComplianceTransactionData_Result> USubmit = (from row in entities.Certificate_SP_OwnerComplianceTransactionData(UserId, CustomerID, SID)
                                                                                                  select row).ToList();

                            if (USubmit.Count() > 0)
                            {
                                Certificate_transactionSchedule obj12 = new Certificate_transactionSchedule();

                                foreach (var item in USubmit)
                                {
                                    obj12.TransactionID = TransactionId;
                                    obj12.Cer_ScheduleOnID = SID;
                                    obj12.ComplianceStatusID = item.ComplianceStatusID;
                                    obj12.ScheduleOnID = item.ScheduledOnID;
                                    entities.Certificate_transactionSchedule.Add(obj12);
                                    entities.SaveChanges();
                                }
                            }
                            else
                            {
                                Certificate_transactionSchedule obj12 = new Certificate_transactionSchedule();

                                
                                    obj12.TransactionID = TransactionId;
                                    obj12.Cer_ScheduleOnID = SID;
                                    obj12.ComplianceStatusID = 0;
                                    obj12.ScheduleOnID = 0;
                                    entities.Certificate_transactionSchedule.Add(obj12);
                                    entities.SaveChanges();
                                 
                            }
                        }
                    }
                }
                result = "Success";
                return Content(result);
            }
            catch (Exception ex)
            {
                return Content(result);
            }
        }

        public class CertificateClass
        {
            public string Content { get; set; }
            public string FilenamePDF { get; set; }
            public string FilenameExcel { get; set; }
        }

        public ActionResult ReviewerCertificatePreview(int UserId, int CustomerID, int SID, string Period, string Flag, string StartDateDetail, string EndDateDetail)
        {
            string ReturnPath = "";

            try
            {
                List<CertificateClass> objCer = new List<CertificateClass>();
                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (string.IsNullOrEmpty(StartDateDetail))
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (string.IsNullOrEmpty(EndDateDetail))
                {
                    dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var SchedueleData = (from row in entities.Certificate_ScheduleOn
                                         where row.CustomerID == CustomerID
                                         && row.ID == SID
                                         select row).FirstOrDefault();

                    ComplianceCertificateTransaction data1 = new ComplianceCertificateTransaction();

                    data1 = (from row in entities.ComplianceCertificateTransactions
                             where row.CustomerID == SchedueleData.CustomerID
                             && ((row.FromDate >= SchedueleData.FromDate && row.FromDate <= SchedueleData.EndDate) ||
                            (row.Enddate >= SchedueleData.FromDate && row.Enddate <= SchedueleData.EndDate) ||
                            (SchedueleData.FromDate >= row.FromDate && SchedueleData.FromDate <= row.Enddate) ||
                            (SchedueleData.EndDate >= row.FromDate && SchedueleData.EndDate <= row.Enddate))
                             && row.IsActive == true
                             select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                   && row.IsActive == true
                                 select row).OrderByDescending(entry => entry.Enddate).FirstOrDefault();
                    }


                    var objReveiver = (from row in entities.Certificate_SP_ReviewerPreviewSummery(UserId, CustomerID, SID)
                                       select row).ToList();

                    if (data1 != null)
                    {
                        var data12 = (from row in entities.Certificate_SP_ReviewerPreviewComplianceSummery(UserId, CustomerID, SID)
                                      select row).ToList();

                        if (data12.Count == 0)
                        {

                        }
                        else
                        {

                            #region Complied Act List

                            List<long> lstCompliaedStatus = new List<long>();
                            lstCompliaedStatus.Add(4);
                            lstCompliaedStatus.Add(5);
                            lstCompliaedStatus.Add(7);
                            lstCompliaedStatus.Add(9);
                            lstCompliaedStatus.Add(15);

                            List<long> lstNonCompliaedStatus = new List<long>();
                            lstNonCompliaedStatus.Add(1);
                            lstNonCompliaedStatus.Add(2);
                            lstNonCompliaedStatus.Add(3);
                            lstNonCompliaedStatus.Add(6);
                            lstNonCompliaedStatus.Add(8);
                            lstNonCompliaedStatus.Add(10);
                            lstNonCompliaedStatus.Add(18);

                            List<string> ActList = data12.Where(x => lstCompliaedStatus.Contains(x.ComplianceStatusID)).Select(x => x.ActName).ToList();
                            ActList = ActList.Distinct().ToList();

                            int cnt = 1;
                            System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                            strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Sr.No.");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(@"Act Name");
                            strExportforActList.Append(@"</Td>");
                            strExportforActList.Append(@"</Tr>");
                            ActList.ForEach(eachSection =>
                            {
                                strExportforActList.Append(@"<Tr>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(cnt);
                                strExportforActList.Append(@" </Td>");
                                strExportforActList.Append(@"<Td>");
                                strExportforActList.Append(eachSection);
                                strExportforActList.Append(@"</Td>");
                                cnt++;
                            });
                            strExportforActList.Append(@"</Table>");

                            #endregion

                            #region non Complied Act List
                            List<string> nonCompliedActList = data12.Where(x => lstNonCompliaedStatus.Contains(x.ComplianceStatusID)).Select(x => x.ActName).ToList();

                            //List<string> nonCompliedActList = data12.Where(x => x.NewStatus.Equals("Not Complied")).Select(x => x.ActName).ToList();
                            nonCompliedActList = nonCompliedActList.Distinct().ToList();
                            int cnt1 = 1;
                            System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                            strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Sr.No.");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(@"Act Name");
                            strExportforNonCompliedList.Append(@"</Td>");
                            strExportforNonCompliedList.Append(@"</Tr>");
                            nonCompliedActList.ForEach(eachSection =>
                            {
                                strExportforNonCompliedList.Append(@"<Tr>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(cnt1);
                                strExportforNonCompliedList.Append(@" </Td>");
                                strExportforNonCompliedList.Append(@"<Td>");
                                strExportforNonCompliedList.Append(eachSection);
                                strExportforNonCompliedList.Append(@"</Td>");
                                cnt1++;
                            });
                            strExportforNonCompliedList.Append(@"</Table>");

                            #endregion

                            #region Complied

                            var Compliedlist = data12.Where(x => lstCompliaedStatus.Contains(x.ComplianceStatusID)).ToList();

                            //var Compliedlist = data12.Where(x => x.NewStatus.Equals("Complied")).ToList();

                            System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                            strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Act Name");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"ShortDescription");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Location");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Performer");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Reviewer");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Period");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Due Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Performer Remarks");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Reviewer Remarks");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Perform Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(@"Review Date");
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"</Tr>");
                            Compliedlist.ForEach(eachSection =>
                            {
                                strExportforComplied.Append(@"<Tr>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ActName);
                                strExportforComplied.Append(@" </Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ShortDescription);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.Branch);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.PerformerName);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ReviewerName);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ForMonth);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.PerformerRemark);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(eachSection.ReviewerRemark);
                                strExportforComplied.Append(@"</Td>");
                                strExportforComplied.Append(@"<Td>");
                                if (eachSection.PerformerDated != null)
                                {
                                    strExportforComplied.Append(Convert.ToDateTime(eachSection.PerformerDated).ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                }
                                if (eachSection.ReviewerDated != null)
                                {
                                    strExportforComplied.Append(@"<Td>");
                                    strExportforComplied.Append(Convert.ToDateTime(eachSection.ReviewerDated).ToString("dd-MMM-yyyy"));
                                    strExportforComplied.Append(@"</Td>");
                                }
                                strExportforComplied.Append(@"</Tr>");
                            });
                            strExportforComplied.Append(@"</Table>");

                            #endregion

                            #region non Complied

                            var NonCompliedlist = data12.Where(x => lstNonCompliaedStatus.Contains(x.ComplianceStatusID)).ToList();
                            //var NonCompliedlist = data12.Where(x => x.NewStatus.Equals("Not Complied")).ToList();

                            System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                            strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Act Name");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"ShortDescription");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Location");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Performer");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Reviewer");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Period");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(@"Due Date");
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                            NonCompliedlist.ForEach(eachSection =>
                            {
                                strExportforNonComplied.Append(@"<Tr>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ActName);
                                strExportforNonComplied.Append(@" </Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ShortDescription);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.Branch);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.PerformerName);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ReviewerName);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ForMonth);
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"<Td>");
                                strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                                strExportforNonComplied.Append(@"</Td>");
                                strExportforNonComplied.Append(@"</Tr>");
                            });
                            strExportforNonComplied.Append(@"</Table>");
                            #endregion

                            #region Reviewer Details
                            var Reveiewelist = objReveiver.ToList();

                            System.Text.StringBuilder strExportforReveiwer = new System.Text.StringBuilder();
                            strExportforReveiwer.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                            strExportforReveiwer.Append(@"<Tr>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(@"Performer Name");
                            strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(@"Completed");
                            strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(@"Overdue");
                            strExportforReveiwer.Append(@"</Td>");
                            //strExportforReveiwer.Append(@"<Td>");
                            //strExportforReveiwer.Append(@"Open");
                            //strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"</Tr>");
                            Reveiewelist.ForEach(eachSection =>
                            {
                                strExportforReveiwer.Append(@"<Tr>");
                                strExportforReveiwer.Append(@"<Td>");
                                strExportforReveiwer.Append(eachSection.PerformerName);
                                strExportforReveiwer.Append(@" </Td>");
                                strExportforReveiwer.Append(@"<Td>");
                                strExportforReveiwer.Append(eachSection.ComplianceStatusClosedTimelyPer + "%");
                                strExportforReveiwer.Append(@"</Td>");
                                strExportforReveiwer.Append(@"<Td>");
                                strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                                strExportforReveiwer.Append(@"</Td>");
                                //strExportforReveiwer.Append(@"<Td>");
                                //strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                                //strExportforReveiwer.Append(@"</Td>");
                                strExportforReveiwer.Append(@"</Tr>");
                            });
                            strExportforReveiwer.Append(@"</Table>");
                            #endregion

                            string relacedataactnondemo = data1.TemplateContent.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                            string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                            string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                            string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                            string relacedataenddate = "";
                            if (EndDateDetail != null)
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                            }
                            else
                            {
                                relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                            }
                            string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                            string replacecurrentdate = "";
                            if (EndDateDetail != null)
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                            }
                            else
                            {
                                replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                            }

                            string BlankNote = "";
                            if (data12.Count == 0)
                            {
                                BlankNote = "*Note- This is Nil Compliance Certificate, no compliance(s) were assigned for selected period. ";
                            }
                            else
                            {
                                BlankNote = "";
                            }

                            string BlankNotedata = replacecurrentdate.Replace("{{BlankCertificateNote}}", BlankNote);
                            string relacedata = BlankNotedata.Replace("{{UserName}}", username.ToString());
                            string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                            string relaceSummary = relacebranch.Replace("{{Summary}}", strExportforReveiwer.ToString());
                            string relaceMonth = relaceSummary.Replace("{{ForMonth}}", Period);
                            string CurrentDateNew = relaceMonth.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));

                            StringBuilder strHTMLContent = new StringBuilder();
                            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                            strHTMLContent.Append(CurrentDateNew);
                            strHTMLContent.Append("</body></html>");

                            byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                            string strFileName = "ComplianceCertificate";
                            var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                            string reportName = string.Empty;
                            reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                            string p_strPath = string.Empty;
                            string dirpath = string.Empty;
                            p_strPath = @"" + storagedrive + "/" + reportName;
                            dirpath = @"" + storagedrive;
                            if (!Directory.Exists(dirpath))
                            {
                                Directory.CreateDirectory(dirpath);
                            }
                            FileStream objFileStrm = System.IO.File.Create(p_strPath);
                            objFileStrm.Close();
                            System.IO.File.WriteAllBytes(p_strPath, buffer);

                            objCer.Add(new CertificateClass { Content = strHTMLContent.ToString(), FilenamePDF = "", FilenameExcel = reportName });

                        }
                    }
                }

                return Json(new { result = objCer });
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }
        public ActionResult CertificatePreview(int UserId, int CustomerID, int SID, string Period, string Flag, string StartDateDetail, string EndDateDetail)
        {
            string ReturnPath = "";
            try
            {
                List<CertificateClass> objCer = new List<CertificateClass>();
                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (string.IsNullOrEmpty(StartDateDetail))
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (string.IsNullOrEmpty(EndDateDetail))
                {
                    dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var SchedueleData = (from row in entities.Certificate_ScheduleOn
                                         where row.CustomerID == CustomerID
                                         && row.ID == SID
                                         select row).FirstOrDefault();

                    ComplianceCertificateTransaction data1 = new ComplianceCertificateTransaction();

                    data1 = (from row in entities.ComplianceCertificateTransactions
                             where row.CustomerID == SchedueleData.CustomerID
                             && ((row.FromDate >= SchedueleData.FromDate && row.FromDate <= SchedueleData.EndDate) ||
                            (row.Enddate >= SchedueleData.FromDate && row.Enddate <= SchedueleData.EndDate) ||
                            (SchedueleData.FromDate >= row.FromDate && SchedueleData.FromDate <= row.Enddate) ||
                            (SchedueleData.EndDate >= row.FromDate && SchedueleData.EndDate <= row.Enddate))
                             && row.IsActive == true
                             select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                   && row.IsActive == true
                                 select row).OrderByDescending(entry => entry.Enddate).FirstOrDefault();
                    }

                    entities.Certificate_SP_OwnerWithCer_scheduleonIDSachin(UserId, CustomerID, 4, SID);

                    var objReveiver = (from row in entities.Certificate_SP_ReviewerDataSummery(UserId, CustomerID, SID)
                                       select row).ToList();

                    if (data1 != null)
                    {
                        var data12 = (from row in entities.Certificate_SP_OwnerComplianceDataSummery(UserId, CustomerID, SID)
                                      select row).ToList();


                        #region Complied Act List

                        List<long> lstCompliaedStatus = new List<long>();
                        lstCompliaedStatus.Add(4);
                        lstCompliaedStatus.Add(5);
                        lstCompliaedStatus.Add(7);
                        lstCompliaedStatus.Add(9);
                        lstCompliaedStatus.Add(15);

                        List<long> lstNonCompliaedStatus = new List<long>();
                        lstNonCompliaedStatus.Add(1);
                        lstNonCompliaedStatus.Add(2);
                        lstNonCompliaedStatus.Add(3);
                        lstNonCompliaedStatus.Add(6);
                        lstNonCompliaedStatus.Add(8);
                        lstNonCompliaedStatus.Add(10);
                        lstNonCompliaedStatus.Add(18);

                        List<string> ActList = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();
                        ActList = ActList.Distinct().ToList();

                        int cnt = 1;
                        System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                        strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforActList.Append(@"<Tr>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Sr.No.");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Act Name");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"</Tr>");
                        ActList.ForEach(eachSection =>
                        {
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(cnt);
                            strExportforActList.Append(@" </Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(eachSection);
                            strExportforActList.Append(@"</Td>");
                            cnt++;
                        });
                        strExportforActList.Append(@"</Table>");

                        #endregion

                        #region non Complied Act List
                        List<string> nonCompliedActList = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();

                        //List<string> nonCompliedActList = data12.Where(x => x.NewStatus.Equals("Not Complied")).Select(x => x.ActName).ToList();
                        nonCompliedActList = nonCompliedActList.Distinct().ToList();
                        int cnt1 = 1;
                        System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                        strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonCompliedList.Append(@"<Tr>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Sr.No.");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Act Name");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"</Tr>");
                        nonCompliedActList.ForEach(eachSection =>
                        {
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(cnt1);
                            strExportforNonCompliedList.Append(@" </Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(eachSection);
                            strExportforNonCompliedList.Append(@"</Td>");
                            cnt1++;
                        });
                        strExportforNonCompliedList.Append(@"</Table>");

                        #endregion

                        #region Complied

                        var Compliedlist = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).ToList();

                        //var Compliedlist = data12.Where(x => x.NewStatus.Equals("Complied")).ToList();

                        System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                        strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforComplied.Append(@"<Tr>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Act Name");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"ShortDescription");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Location");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Period");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Due Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Perform Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Review Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"</Tr>");
                        Compliedlist.ForEach(eachSection =>
                        {
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ActName);
                            strExportforComplied.Append(@" </Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ShortDescription);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.Branch);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ForMonth);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            if (eachSection.PerformerDated != null)
                            {
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.PerformerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            if (eachSection.ReviewerDated != null)
                            {
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.ReviewerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            strExportforComplied.Append(@"</Tr>");
                        });
                        strExportforComplied.Append(@"</Table>");

                        #endregion

                        #region non Complied

                        var NonCompliedlist = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).ToList();
                        //var NonCompliedlist = data12.Where(x => x.NewStatus.Equals("Not Complied")).ToList();

                        System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                        strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonComplied.Append(@"<Tr>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Act Name");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"ShortDescription");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Location");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Performer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Reviewer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Period");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Due Date");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"</Tr>");
                        NonCompliedlist.ForEach(eachSection =>
                        {
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ActName);
                            strExportforNonComplied.Append(@" </Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ShortDescription);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Branch);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.PerformerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ReviewerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ForMonth);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                        });
                        strExportforNonComplied.Append(@"</Table>");
                        #endregion

                        #region Reviewer Details
                        var Reveiewelist = objReveiver.ToList();

                        System.Text.StringBuilder strExportforReveiwer = new System.Text.StringBuilder();
                        strExportforReveiwer.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforReveiwer.Append(@"<Tr>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Reveiwer Name");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Completed");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Overdue");
                        strExportforReveiwer.Append(@"</Td>");
                        //strExportforReveiwer.Append(@"<Td>");
                        //strExportforReveiwer.Append(@"Open");
                        //strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"</Tr>");
                        Reveiewelist.ForEach(eachSection =>
                        {
                            strExportforReveiwer.Append(@"<Tr>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ReviewerName);
                            strExportforReveiwer.Append(@" </Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusClosedTimelyPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            //strExportforReveiwer.Append(@"<Td>");
                            //strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            //strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"</Tr>");
                        });
                        strExportforReveiwer.Append(@"</Table>");
                        #endregion

                        string relacedataactnondemo = data1.TemplateContent.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                        string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                        string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                        string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                        string relacedataenddate = "";
                        if (EndDateDetail != null)
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                        }
                        else
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                        }
                        string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                        string replacecurrentdate = "";
                        if (EndDateDetail != null)
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                        }
                        else
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                        }




                        string BlankNote = "";
                        if (data12.Count == 0)
                        {
                            BlankNote = "*Note- This is Nil Compliance Certificate, no compliance(s) were assigned for selected period. ";
                        }
                        else
                        {
                            BlankNote = "";
                        }

                        string BlankNotedata = replacecurrentdate.Replace("{{BlankCertificateNote}}", BlankNote);
                        string relacedata = BlankNotedata.Replace("{{UserName}}", username.ToString());
                        string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                        string relaceSummary = relacebranch.Replace("{{Summary}}", strExportforReveiwer.ToString());
                        string relaceMonth = relaceSummary.Replace("{{ForMonth}}", Period);
                        string CurrentDateNew = relaceMonth.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));
                        StringBuilder strHTMLContent = new StringBuilder();
                        strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                        strHTMLContent.Append(CurrentDateNew);
                        strHTMLContent.Append("</body></html>");

                        byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                        string strFileName = "ComplianceCertificate";
                        var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                        string reportName = string.Empty;
                        reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                        string p_strPath = string.Empty;
                        string dirpath = string.Empty;
                        p_strPath = @"" + storagedrive + "/" + reportName;
                        dirpath = @"" + storagedrive;
                        if (!Directory.Exists(dirpath))
                        {
                            Directory.CreateDirectory(dirpath);
                        }
                        FileStream objFileStrm = System.IO.File.Create(p_strPath);
                        objFileStrm.Close();
                        System.IO.File.WriteAllBytes(p_strPath, buffer);

                        objCer.Add(new CertificateClass { Content = strHTMLContent.ToString(), FilenamePDF = "", FilenameExcel = reportName });

                        //}
                    }
                }

                return Json(new { result = objCer });
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }

        public ActionResult OfficerCertificatePreview(int UserId, int CustomerID, int ceID, string Period, string Flag, string StartDateDetail, string EndDateDetail)
        {
            string ReturnPath = "";

            try
            {
                List<CertificateClass> objCer = new List<CertificateClass>();
                string designationname = designation(UserId);
                string username = UserName(UserId);
                string BranchName = branchName(UserId);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (string.IsNullOrEmpty(StartDateDetail))
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (string.IsNullOrEmpty(EndDateDetail))
                {
                    dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var SchedueleData = (from row in entities.Certificate_ScheduleOn
                                         where row.CustomerID == CustomerID
                                         && row.ID == ceID
                                         select row).FirstOrDefault();

                    ComplianceCertificateTransaction data1 = new ComplianceCertificateTransaction();

                    data1 = (from row in entities.ComplianceCertificateTransactions
                             where row.CustomerID == SchedueleData.CustomerID
                             && ((row.FromDate >= SchedueleData.FromDate && row.FromDate <= SchedueleData.EndDate) ||
                            (row.Enddate >= SchedueleData.FromDate && row.Enddate <= SchedueleData.EndDate) ||
                            (SchedueleData.FromDate >= row.FromDate && SchedueleData.FromDate <= row.Enddate) ||
                            (SchedueleData.EndDate >= row.FromDate && SchedueleData.EndDate <= row.Enddate))
                             && row.IsActive == true
                             select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                   && row.IsActive == true
                                 select row).OrderByDescending(entry => entry.Enddate).FirstOrDefault();
                    }


                    entities.Certificate_SP_OfficerWithCer_scheduleonID(UserId, CustomerID, 3, ceID);

                    var objReveiver = (from row in entities.Certificate_SP_OfficerOwnerDataSummery(UserId, CustomerID, ceID)
                                       select row).ToList();


                    if (data1 != null)
                    {
                        var data12 = (from row in entities.Certificate_SP_OfficerComplianceDataSummery(UserId, CustomerID, ceID)
                                      select row).ToList();


                        List<long> lstCompliaedStatus = new List<long>();
                        lstCompliaedStatus.Add(4);
                        lstCompliaedStatus.Add(5);
                        lstCompliaedStatus.Add(7);
                        lstCompliaedStatus.Add(9);
                        lstCompliaedStatus.Add(15);

                        List<long> lstNonCompliaedStatus = new List<long>();
                        lstNonCompliaedStatus.Add(1);
                        lstNonCompliaedStatus.Add(2);
                        lstNonCompliaedStatus.Add(3);
                        lstNonCompliaedStatus.Add(6);
                        lstNonCompliaedStatus.Add(8);
                        lstNonCompliaedStatus.Add(10);
                        lstNonCompliaedStatus.Add(18);
                        #region Complied Act List

                        List<string> ActList = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();
                        ActList = ActList.Distinct().ToList();

                        int cnt = 1;
                        System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                        strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforActList.Append(@"<Tr>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Sr.No.");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Act Name");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"</Tr>");
                        ActList.ForEach(eachSection =>
                        {
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(cnt);
                            strExportforActList.Append(@" </Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(eachSection);
                            strExportforActList.Append(@"</Td>");
                            cnt++;
                        });
                        strExportforActList.Append(@"</Table>");

                        #endregion

                        #region non Complied Act List

                        List<string> nonCompliedActList = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();
                        nonCompliedActList = nonCompliedActList.Distinct().ToList();
                        int cnt1 = 1;
                        System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                        strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonCompliedList.Append(@"<Tr>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Sr.No.");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Act Name");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"</Tr>");
                        nonCompliedActList.ForEach(eachSection =>
                        {
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(cnt1);
                            strExportforNonCompliedList.Append(@" </Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(eachSection);
                            strExportforNonCompliedList.Append(@"</Td>");
                            cnt1++;
                        });
                        strExportforNonCompliedList.Append(@"</Table>");

                        #endregion

                        #region Complied

                        var Compliedlist = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).ToList();
                        Compliedlist = Compliedlist.Distinct().ToList();
                        System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                        strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforComplied.Append(@"<Tr>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Act Name");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"ShortDescription");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Location");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Period");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Due Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Perform Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Review Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"</Tr>");
                        Compliedlist.ForEach(eachSection =>
                        {
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ActName);
                            strExportforComplied.Append(@" </Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ShortDescription);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.Branch);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ForMonth);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            if (eachSection.PerformerDated != null)
                            {
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.PerformerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            if (eachSection.ReviewerDated != null)
                            {
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.ReviewerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            strExportforComplied.Append(@"</Tr>");
                        });
                        strExportforComplied.Append(@"</Table>");

                        #endregion

                        #region non Complied
                        var NonCompliedlist = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).ToList();
                        NonCompliedlist = NonCompliedlist.Distinct().ToList();
                        System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                        strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonComplied.Append(@"<Tr>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Act Name");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"ShortDescription");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Location");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Performer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Reviewer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Period");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Due Date");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"</Tr>");
                        NonCompliedlist.ForEach(eachSection =>
                        {
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ActName);
                            strExportforNonComplied.Append(@" </Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ShortDescription);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Branch);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.PerformerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ReviewerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ForMonth);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                        });
                        strExportforNonComplied.Append(@"</Table>");
                        #endregion

                        #region Reviewer Details
                        var Reveiewelist = objReveiver.ToList();

                        System.Text.StringBuilder strExportforReveiwer = new System.Text.StringBuilder();
                        strExportforReveiwer.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforReveiwer.Append(@"<Tr>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Owner Name");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Completed");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Overdue");
                        strExportforReveiwer.Append(@"</Td>");
                        //strExportforReveiwer.Append(@"<Td>");
                        //strExportforReveiwer.Append(@"Open");
                        //strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"</Tr>");
                        Reveiewelist.ForEach(eachSection =>
                        {
                            strExportforReveiwer.Append(@"<Tr>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.OwnerName);
                            strExportforReveiwer.Append(@" </Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusClosedTimelyPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            //strExportforReveiwer.Append(@"<Td>");
                            //strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            //strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"</Tr>");
                        });
                        strExportforReveiwer.Append(@"</Table>");
                        #endregion

                        string relacedataactnondemo = data1.TemplateContent.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                        string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                        string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                        string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                        string relacedataenddate = "";
                        if (EndDateDetail != null)
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                        }
                        else
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                        }
                        string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                        string replacecurrentdate = "";
                        if (EndDateDetail != null)
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                        }
                        else
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                        }


                        string BlankNote = "";
                        if (objReveiver.Count == 0)
                        {
                            BlankNote = "*Note- This is Nil Compliance Certificate, no compliance(s) were assigned for selected period. ";
                        }
                        else
                        {
                            BlankNote = "";
                        }

                        string BlankNotedata = replacecurrentdate.Replace("{{BlankCertificateNote}}", BlankNote);
                        string relacedata = BlankNotedata.Replace("{{UserName}}", username.ToString());
                        string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                        string relaceSummary = relacebranch.Replace("{{Summary}}", strExportforReveiwer.ToString());
                        string relaceMonth = relaceSummary.Replace("{{ForMonth}}", Period);
                        string CurrentDateNew = relaceMonth.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));

                        StringBuilder strHTMLContent = new StringBuilder();
                        strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                        strHTMLContent.Append(CurrentDateNew);
                        strHTMLContent.Append("</body></html>");

                        byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());


                        string strFileName = "ComplianceCertificate";
                        var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                        string reportName = string.Empty;
                        reportName = strFileName + "_" + UserId.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                        string p_strPath = string.Empty;
                        string dirpath = string.Empty;
                        p_strPath = @"" + storagedrive + "/" + reportName;
                        dirpath = @"" + storagedrive;
                        if (!Directory.Exists(dirpath))
                        {
                            Directory.CreateDirectory(dirpath);
                        }
                        FileStream objFileStrm = System.IO.File.Create(p_strPath);
                        objFileStrm.Close();
                        System.IO.File.WriteAllBytes(p_strPath, buffer);

                        objCer.Add(new CertificateClass { Content = strHTMLContent.ToString(), FilenamePDF = "", FilenameExcel = reportName });

                        //}
                    }
                }

                return Json(new { result = objCer });
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }

        public ActionResult ManagemementCertificatePreview(int Mgmtid, int CustomerID, int ceID, string Period, string Flag, string StartDateDetail, string EndDateDetail)
        {
            string ReturnPath = "";

            try
            {
                List<CertificateClass> objCer = new List<CertificateClass>();
                string designationname = designation(Mgmtid);
                string username = UserName(Mgmtid);
                string BranchName = branchName(Mgmtid);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (string.IsNullOrEmpty(StartDateDetail))
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(StartDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (string.IsNullOrEmpty(EndDateDetail))
                {
                    dtTo = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(EndDateDetail.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var SchedueleData = (from row in entities.Certificate_ScheduleOn
                                         where row.CustomerID == CustomerID
                                         && row.ID == ceID
                                         select row).FirstOrDefault();

                    ComplianceCertificateTransaction data1 = new ComplianceCertificateTransaction();

                    data1 = (from row in entities.ComplianceCertificateTransactions
                             where row.CustomerID == SchedueleData.CustomerID
                             && ((row.FromDate >= SchedueleData.FromDate && row.FromDate <= SchedueleData.EndDate) ||
                            (row.Enddate >= SchedueleData.FromDate && row.Enddate <= SchedueleData.EndDate) ||
                            (SchedueleData.FromDate >= row.FromDate && SchedueleData.FromDate <= row.Enddate) ||
                            (SchedueleData.EndDate >= row.FromDate && SchedueleData.EndDate <= row.Enddate))
                             && row.IsActive == true
                             select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == CustomerID
                                  && row.IsActive == true
                                 select row).OrderByDescending(entry => entry.Enddate).FirstOrDefault();
                    }


                    entities.Certificate_SP_MgmtWithCer_scheduleonID(Mgmtid, CustomerID, 3, ceID);

                    var objReveiver = (from row in entities.Certificate_SP_MgmtOfficerDataSummery(Mgmtid, CustomerID, ceID)
                                       select row).ToList();

                    if (data1 != null)
                    {
                        var data12 = (from row in entities.Certificate_SP_MgmtComplianceDataSummery(Mgmtid, CustomerID, ceID)
                                      select row).ToList();


                        List<long> lstCompliaedStatus = new List<long>();
                        lstCompliaedStatus.Add(4);
                        lstCompliaedStatus.Add(5);
                        lstCompliaedStatus.Add(7);
                        lstCompliaedStatus.Add(9);
                        lstCompliaedStatus.Add(15);

                        List<long> lstNonCompliaedStatus = new List<long>();
                        lstNonCompliaedStatus.Add(1);
                        lstNonCompliaedStatus.Add(2);
                        lstNonCompliaedStatus.Add(3);
                        lstNonCompliaedStatus.Add(6);
                        lstNonCompliaedStatus.Add(8);
                        lstNonCompliaedStatus.Add(10);
                        lstNonCompliaedStatus.Add(18);
                        #region Complied Act List

                        List<string> ActList = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();

                        ActList = ActList.Distinct().ToList();

                        int cnt = 1;
                        System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                        strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforActList.Append(@"<Tr>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Sr.No.");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Act Name");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"</Tr>");
                        ActList.ForEach(eachSection =>
                        {
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(cnt);
                            strExportforActList.Append(@" </Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(eachSection);
                            strExportforActList.Append(@"</Td>");
                            cnt++;
                        });
                        strExportforActList.Append(@"</Table>");

                        #endregion

                        #region non Complied Act List

                        List<string> nonCompliedActList = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).Select(x => x.ActName).ToList();
                        nonCompliedActList = nonCompliedActList.Distinct().ToList();
                        int cnt1 = 1;
                        System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                        strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonCompliedList.Append(@"<Tr>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Sr.No.");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Act Name");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"</Tr>");
                        nonCompliedActList.ForEach(eachSection =>
                        {
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(cnt1);
                            strExportforNonCompliedList.Append(@" </Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(eachSection);
                            strExportforNonCompliedList.Append(@"</Td>");
                            cnt1++;
                        });
                        strExportforNonCompliedList.Append(@"</Table>");

                        #endregion

                        #region Complied

                        var Compliedlist = data12.Where(x => lstCompliaedStatus.Contains(x.OldStatusID)).ToList();
                        Compliedlist = Compliedlist.Distinct().ToList();
                        System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                        strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforComplied.Append(@"<Tr>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Act Name");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"ShortDescription");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Location");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Period");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Due Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Performer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reviewer Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Perform Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Review Date");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"</Tr>");
                        Compliedlist.ForEach(eachSection =>
                        {
                            strExportforComplied.Append(@"<Tr>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ActName);
                            strExportforComplied.Append(@" </Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ShortDescription);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.Branch);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerName);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ForMonth);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.PerformerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            strExportforComplied.Append(eachSection.ReviewerRemark);
                            strExportforComplied.Append(@"</Td>");
                            strExportforComplied.Append(@"<Td>");
                            if (eachSection.PerformerDated != null)
                            {
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.PerformerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            if (eachSection.ReviewerDated != null)
                            {
                                strExportforComplied.Append(@"<Td>");
                                strExportforComplied.Append(Convert.ToDateTime(eachSection.ReviewerDated).ToString("dd-MMM-yyyy"));
                                strExportforComplied.Append(@"</Td>");
                            }
                            strExportforComplied.Append(@"</Tr>");
                        });
                        strExportforComplied.Append(@"</Table>");

                        #endregion

                        #region non Complied
                        var NonCompliedlist = data12.Where(x => lstNonCompliaedStatus.Contains(x.OldStatusID)).ToList();
                        NonCompliedlist = NonCompliedlist.Distinct().ToList();
                        System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                        strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonComplied.Append(@"<Tr>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Act Name");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"ShortDescription");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Location");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Performer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Reviewer");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Period");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"<Td>");
                        strExportforNonComplied.Append(@"Due Date");
                        strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"</Tr>");
                        NonCompliedlist.ForEach(eachSection =>
                        {
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ActName);
                            strExportforNonComplied.Append(@" </Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ShortDescription);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Branch);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.PerformerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ReviewerName);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ForMonth);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ScheduledOn.ToString("dd-MMM-yyyy"));
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");
                        });
                        strExportforNonComplied.Append(@"</Table>");
                        #endregion

                        #region Reviewer Details
                        var Reveiewelist = objReveiver.ToList();

                        System.Text.StringBuilder strExportforReveiwer = new System.Text.StringBuilder();
                        strExportforReveiwer.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforReveiwer.Append(@"<Tr>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Officer Name");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Completed");
                        strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"<Td>");
                        strExportforReveiwer.Append(@"Overdue");
                        strExportforReveiwer.Append(@"</Td>");
                        //strExportforReveiwer.Append(@"<Td>");
                        //strExportforReveiwer.Append(@"Open");
                        //strExportforReveiwer.Append(@"</Td>");
                        strExportforReveiwer.Append(@"</Tr>");
                        Reveiewelist.ForEach(eachSection =>
                        {
                            strExportforReveiwer.Append(@"<Tr>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.OfficerName);
                            strExportforReveiwer.Append(@" </Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusClosedTimelyPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"<Td>");
                            strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            strExportforReveiwer.Append(@"</Td>");
                            //strExportforReveiwer.Append(@"<Td>");
                            //strExportforReveiwer.Append(eachSection.ComplianceStatusOpenPer + "%");
                            //strExportforReveiwer.Append(@"</Td>");
                            strExportforReveiwer.Append(@"</Tr>");
                        });
                        strExportforReveiwer.Append(@"</Table>");
                        #endregion

                        string relacedataactnondemo = data1.TemplateContent.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                        string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                        string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                        string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                        string relacedataenddate = "";
                        if (EndDateDetail != null)
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", EndDateDetail.ToString());

                        }
                        else
                        {
                            relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", " ");
                        }
                        string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                        string replacecurrentdate = "";
                        if (EndDateDetail != null)
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", StartDateDetail);

                        }
                        else
                        {
                            replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", " ");
                        }

                        string BlankNote = "";
                        if (objReveiver.Count == 0)
                        {
                            BlankNote = "*Note- This is Nil Compliance Certificate, no compliance(s) were assigned for selected period. ";
                        }
                        else
                        {
                            BlankNote = "";
                        }
                        string BlankNotedata = replacecurrentdate.Replace("{{BlankCertificateNote}}", BlankNote);
                        string relacedata = BlankNotedata.Replace("{{UserName}}", username.ToString());
                        string relacebranch = relacedata.Replace("{{BranchName}}", BranchName);
                        string relaceSummary = relacebranch.Replace("{{Summary}}", strExportforReveiwer.ToString());
                        string relaceMonth = relaceSummary.Replace("{{ForMonth}}", Period);
                        string CurrentDateNew = relaceMonth.Replace("{{CurrentDate}}", DateTime.Now.ToString("dd-MMM-yyyy"));
                        StringBuilder strHTMLContent = new StringBuilder();
                        strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                        strHTMLContent.Append(CurrentDateNew);
                        strHTMLContent.Append("</body></html>");

                        byte[] buffer = Encoding.ASCII.GetBytes(strHTMLContent.ToString().ToCharArray());

                        string strFileName = "ComplianceCertificate";
                        var storagedrive = Convert.ToString(ConfigurationManager.AppSettings["MGMTReportDrive"]);
                        string reportName = string.Empty;
                        reportName = strFileName + "_" + Mgmtid.ToString() + "_" + DateTime.Today.Date.ToString("ddMMMyyyy") + ".doc";
                        string p_strPath = string.Empty;
                        string dirpath = string.Empty;
                        p_strPath = @"" + storagedrive + "/" + reportName;
                        dirpath = @"" + storagedrive;
                        if (!Directory.Exists(dirpath))
                        {
                            Directory.CreateDirectory(dirpath);
                        }
                        FileStream objFileStrm = System.IO.File.Create(p_strPath);
                        objFileStrm.Close();
                        System.IO.File.WriteAllBytes(p_strPath, buffer);

                        objCer.Add(new CertificateClass { Content = strHTMLContent.ToString(), FilenamePDF = "", FilenameExcel = reportName });

                        //}
                    }
                }

                return Json(new { result = objCer });
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Content(ReturnPath); ;
            }
        }

    }
}