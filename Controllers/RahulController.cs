﻿using AppWebApplication.Data;
using AppWebApplication.Models;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;

namespace AppWebApplication.Controllers
{
    public class RahulController : ApiController
    {
        public static string key = "avantis";

//        {
//        "query": {
//        "multi_match": {
//        "query": "Resource",
//        "fields": [
//        "actID",
//        "actname",
//        "compliancecategoryname"
//        ]
//    }
//},
//        "size": 1000,
//        "from": 0
//        }     
        [Route("Rahul/GetLegalUpdateAllActsNew")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLegalUpdateAllActsNew([FromBody]UserDetail UserDetailObj)
        {
            try
            {
               

                int page = 2 * 10;
                int from = page-10;
                ISearchResponse<ComplianceManagement.ActListNew> actsearchresults = null;
                MainRootObject objmainrootobject = new MainRootObject();
                SearchObject searchobj = new SearchObject();
                int userID = -1;
                string Email = string.Empty;
                if (!(string.IsNullOrEmpty(UserDetailObj.Email)))
                {
                    Email = UserDetailObj.Email.ToString();
                }
                Email = Email.Replace(" ", "+");
                var encryptedBytes = Convert.FromBase64String(Email);
                try
                {
                    Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                }
                catch { }
                string EmailIOS = string.Empty;
                try
                {
                    EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                }
                catch { }
                if (!string.IsNullOrEmpty(Email))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        userID = Convert.ToInt32((from row in entities.Users
                                                  where (row.Email == Email)
                                                    && row.IsActive == true
                                                    && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());


                        List<int> actids = (from row in entities.LegalUpdates_MyFavorites
                                            where row.UserID == userID && row.CType == "Act"
                                            && row.IsMyFavourite == false
                                            select (int)row.ItemID).ToList();

                        string elasticsearchconnectionstring = ConfigurationManager.AppSettings["ElasticSearchConnectionsring"].ToString();
                        var node = new Uri(elasticsearchconnectionstring);
                        var actsettings = new ConnectionSettings(node);
                        actsettings.DefaultIndex("actdataindex");
                        actsettings.DisableDirectStreaming();
                        actsettings.PrettyJson().DisableDirectStreaming();
                        var actclient = new ElasticClient(actsettings);

                        actsearchresults = actclient.Search<ComplianceManagement.ActListNew>(s => s
                                        .Query(q => q // define query
                                        .MultiMatch(mp => mp // of type MultiMatch
                                        .Query(UserDetailObj.Filter) // pass text
                                        .Fields(f => f // define fields to search against
                                        .Fields(f1 => f1.actname, f2 => f2.compliancecategoryname, f3 => f3.compliancetypename,f4=>f4.state))))
                                        .From(from) // apply paging
                                        .Size(page)); // limit to page size
                        using (MemoryStream mStream = new MemoryStream())
                        {
                            actclient.Serializer.Serialize(actsearchresults, mStream);
                            var rawQueryText = Encoding.ASCII.GetString(mStream.ToArray());
                            search.RootObject item = JsonConvert.DeserializeObject<search.RootObject>(rawQueryText);
                            objmainrootobject.rootobject1 = item;
                            searchobj.actlist = objmainrootobject.rootobject1.hits.hits;
                            foreach (var lstitem in objmainrootobject.rootobject1.hits.hits.Where(w => actids.Contains(w._source.actid)))
                            {
                                lstitem._source.IsMyFavourite = 0;
                            }

                            searchobj.Compliacelist = new List<SearchCompliance.Hit>();
                            searchobj.DailyUpdateActlist = new List<DailyUpdateSearch.Hit>();
                            searchobj.NewsLetterlist = new List<NewsLetterSearch.Hit>();
                        }                       
                    }
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JObject.FromObject(searchobj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog(ex.Message);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(null).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
