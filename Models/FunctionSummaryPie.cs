﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AvacomTokenDetails
    {
        public string UserEmail { get; set; }
        public string tokendetail { get; set; }
    }
    public class FunctionSummaryPie
    {
        public long NotCompleted { get; set; }
        public long AfterDueDated { get; set; }
        public long InTime { get; set; }

        public long InTimeCritical { get; set; }
        public long InTimeHigh { get; set; }
        public long InTimeMedium { get; set; }
        public long InTimeLow { get; set; }

        public long AfterDueDatedCritical { get; set; }
        public long AfterDueDatedHigh { get; set; }
        public long AfterDueDatedMedium { get; set; }
        public long AfterDueDatedLow { get; set; }

        public long NotCompletedCritical { get; set; }
        public long NotCompletedHigh { get; set; }
        public long NotCompletedMedium { get; set; }
        public long NotCompletedLow { get; set; }



    }
}

