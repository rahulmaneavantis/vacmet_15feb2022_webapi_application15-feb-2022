﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
namespace AppWebApplication.Models
{
    public class ComplianceCategoryManagement
    {
        public static List<sp_ComplianceAssignedDepartment_Result> GetNewAll(int UserID, int CustomerBranchId, List<long> Branchlist, string isstatutoryinternal, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedDepartment_Result> complianceCategorys = new List<sp_ComplianceAssignedDepartment_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<sp_ComplianceAssignedCategory_Result> GetNewAllFunction(int UserID, int CustomerBranchId, List<long> Branchlist, string role, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (role.Equals("APPR"))
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else if (role.Equals("PRA"))
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "PRA")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "PRA")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                else if (role.Equals("MGMT"))
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                else if (role.Equals("DEPT"))
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "DEPT")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {

                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "DEPT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<SP_ComplianceCertificateCategoryWise_Result> GetCertificateAllFunction(int CustomerID,int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_ComplianceCertificateCategoryWise_Result> complianceCategorys = new List<SP_ComplianceCertificateCategoryWise_Result>();
               
                    
                        complianceCategorys = (from row in entities.SP_ComplianceCertificateCategoryWise(CustomerID,UserID)
                                                select row).ToList();
                    
                
                return complianceCategorys.OrderBy(entry => entry.CategoryName).ToList();
            }
        }

        public static List<SP_ComplianceCertificateDeptWise_Result> GetCertificateAllDept(int CustomerID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_ComplianceCertificateDeptWise_Result> complianceCategorys = new List<SP_ComplianceCertificateDeptWise_Result>();


                complianceCategorys = (from row in entities.SP_ComplianceCertificateDeptWise(CustomerID, UserID)
                                       select row).ToList();


                return complianceCategorys.OrderBy(entry => entry.DeptName).ToList();
            }
        }

        public static List<SP_ComplianceCertificateUserWise_Result> GetCertificateAllUser(int CustomerID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_ComplianceCertificateUserWise_Result> complianceuser = new List<SP_ComplianceCertificateUserWise_Result>();


                complianceuser = (from row in entities.SP_ComplianceCertificateUserWise(CustomerID, UserID)
                                       select row).ToList();


                return complianceuser.OrderBy(entry => entry.UserName).ToList();
            }
        }
        public static List<sp_ComplianceAssignedCategory_Result> GetNewAllNew(int Userid, int CustomerBranchId, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (CustomerBranchId != -1)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(Userid, "APPR")
                                               where row.CustomerBranchID == CustomerBranchId
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                        //complianceCategorys = (from row in entities.ComplianceCategories
                        //                       join row1 in entities.ComplianceAssignedInstancesViews
                        //                       on row.ID equals row1.ComplianceCategoryId
                        //                       where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
                        //                       && row1.RoleID == 6
                        //                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(Userid, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                        //complianceCategorys = (from row in entities.ComplianceCategories
                        //                       join row1 in entities.ComplianceAssignedInstancesViews
                        //                       on row.ID equals row1.ComplianceCategoryId
                        //                       where row1.UserID == Userid && row1.RoleID == 6
                        //                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    //List<ComplianceAssignedInstancesView> ComplianceCount = new List<ComplianceAssignedInstancesView>();
                    if (CustomerBranchId != -1)
                    {

                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(Userid, "MGMT")
                                               where row.CustomerBranchID == CustomerBranchId
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                        //ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                        //                   join row1 in entities.EntitiesAssignments
                        //                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                        //                   where row.CustomerBranchID == row1.BranchID
                        //                   && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                        //                   select row).Distinct().ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(Userid, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                        //ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                        //                   join row1 in entities.EntitiesAssignments
                        //                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                        //                   where row.CustomerBranchID == row1.BranchID
                        //                   && row1.UserID == Userid
                        //                   select row).Distinct().ToList();
                    }
                    //ComplianceCount = ComplianceCount.GroupBy(a => (int?)a.ComplianceCategoryId).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();
                    //complianceCategorys = (from row in ComplianceCount
                    //                       join row1 in entities.ComplianceCategories
                    //                       on row.ComplianceCategoryId equals row1.ID
                    //                       select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<InternalCompliancesCategory> GetAllInternalCompliancesCategories(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.InternalCompliancesCategories
                                           where row.IsDeleted == false
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<sp_InternalComplianceAssignedCategory_Result> GetFunctionDetailsInternal(int UserID, List<long> Branchlist, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceAssignedCategory_Result> complianceCategorys = new List<sp_InternalComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {

                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, int CustomerBranchId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (CustomerBranchId != -1)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
                    if (CustomerBranchId != -1)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid

                                                   select row).Distinct().ToList();

                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           where row1.IsDeleted == false
                                           select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        //public static List<sp_InternalComplianceAssignedCategory_Result> GetFunctionDetailsInternal(int UserID, List<long> Branchlist, string role)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<sp_InternalComplianceAssignedCategory_Result> complianceCategorys = new List<sp_InternalComplianceAssignedCategory_Result>();
        //        if (role.Equals("APPR"))
        //        {
        //            if (Branchlist.Count > 0)
        //            {
        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
        //                                       where Branchlist.Contains((long)row.CustomerBranchID)
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
        //            }
        //            else
        //            {
        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
        //            }
        //        }
        //        else if (role.Equals("MGMT"))
        //        {
        //            if (Branchlist.Count > 0)
        //            {
        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
        //                                       where Branchlist.Contains((long)row.CustomerBranchID)
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

        //            }
        //            else
        //            {

        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
        //            }
        //            complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();
        //        }
        //        else if (role.Equals("DEPT"))
        //        {
        //            if (Branchlist.Count > 0)
        //            {
        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "DEPT")
        //                                       where Branchlist.Contains((long)row.CustomerBranchID)
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

        //            }
        //            else
        //            {

        //                complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "DEPT")
        //                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

        //            }
        //            complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

        //        }
        //        return complianceCategorys.OrderBy(entry => entry.Name).ToList();
        //    }
        //}
        //public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, int CustomerBranchId, bool approver = false)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
        //        if (approver == true)
        //        {
        //            if (CustomerBranchId != -1)
        //            {
        //                complianceCategorys = (from row in entities.InternalCompliancesCategories
        //                                       join row1 in entities.InternalComplianceAssignedInstancesViews
        //                                       on row.ID equals row1.InternalComplianceCategoryID
        //                                       where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
        //                                       && row1.RoleID == 6
        //                                       && row.IsDeleted == false
        //                                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
        //            }
        //            else
        //            {
        //                complianceCategorys = (from row in entities.InternalCompliancesCategories
        //                                       join row1 in entities.InternalComplianceAssignedInstancesViews
        //                                       on row.ID equals row1.InternalComplianceCategoryID
        //                                       where row1.UserID == Userid
        //                                       && row1.RoleID == 6
        //                                       && row.IsDeleted == false
        //                                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
        //            }
        //        }
        //        else
        //        {
        //            List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
        //            if (CustomerBranchId != -1)
        //            {
        //                InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
        //                                           join row1 in entities.EntitiesAssignmentInternals
        //                                           on (long) row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
        //                                           where row.CustomerBranchID == row1.BranchID
        //                                           && row1.UserID == Userid && row1.BranchID == CustomerBranchId
        //                                           select row).Distinct().ToList();
        //            }
        //            else
        //            {
        //                InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
        //                                           join row1 in entities.EntitiesAssignmentInternals
        //                                           on (long) row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
        //                                           where row.CustomerBranchID == row1.BranchID
        //                                           && row1.UserID == Userid

        //                                           select row).Distinct().ToList();

        //            }
        //            InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?) a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
        //            complianceCategorys = (from row in InternalComplianceCount
        //                                   join row1 in entities.InternalCompliancesCategories
        //                                   on row.InternalComplianceCategoryID equals row1.ID
        //                                   where row1.IsDeleted == false
        //                                   select row1).ToList();
        //        }
        //        return complianceCategorys.OrderBy(entry => entry.Name).ToList();
        //    }
        //}
        public static List<ComplianceCategory> GetNewAll(int Userid, int CustomerBranchId, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceCategory> complianceCategorys = new List<ComplianceCategory>();
                if (approver == true)
                {
                    if (CustomerBranchId != -1)
                    {
                        complianceCategorys = (from row in entities.ComplianceCategories
                                               join row1 in entities.ComplianceAssignedInstancesViews
                                               on row.ID equals row1.ComplianceCategoryId
                                               where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.ComplianceCategories
                                               join row1 in entities.ComplianceAssignedInstancesViews
                                               on row.ID equals row1.ComplianceCategoryId
                                               where row1.UserID == Userid && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<ComplianceAssignedInstancesView> ComplianceCount = new List<ComplianceAssignedInstancesView>();
                    if (CustomerBranchId != -1)
                    {
                        ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                                           join row1 in entities.EntitiesAssignments
                                            on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                           where row.CustomerBranchID == row1.BranchID
                                           && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                           select row).Distinct().ToList();

                    }
                    else
                    {
                        ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                                           join row1 in entities.EntitiesAssignments
                                            on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                           where row.CustomerBranchID == row1.BranchID
                                           && row1.UserID == Userid
                                           select row).Distinct().ToList();
                    }
                    ComplianceCount = ComplianceCount.GroupBy(a => (int?) a.ComplianceCategoryId).Select(a => a.FirstOrDefault()).ToList();

                    complianceCategorys = (from row in ComplianceCount
                                           join row1 in entities.ComplianceCategories
                                           on row.ComplianceCategoryId equals row1.ID
                                           select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

    }
}