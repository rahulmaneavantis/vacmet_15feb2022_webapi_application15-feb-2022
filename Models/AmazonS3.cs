﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amazon;
using Amazon.S3;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    //private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
    //private static IAmazonS3 s3Client;
    public class AmazonS3
    {
        public static Client_StorageDetail GetAWSStorageDetail(long CustId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.Client_StorageDetail
                            where row.CustomerID == CustId
                            select row).FirstOrDefault();

                return Data;
            }
        }
    }
}