﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AssignLocationList
    {
        public int AssignID { get; set; }
        public string AssignName { get; set; }
    }
}