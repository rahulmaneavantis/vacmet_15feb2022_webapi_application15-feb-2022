﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class RiskChartData
    {
        public long NotCompletedCritical { get; set; }
        public long NotCompletedHigh { get; set; }
        public long NotCompletedMedium { get; set; }
        public long NotCompletedLow { get; set; }

        public long AfterDueDatedCritical { get; set; }
        public long AfterDueDatedHigh { get; set; }
        public long AfterDueDateMedium { get; set; }
        public long AfterDueDateLow { get; set; }

        public long InTimedCritical { get; set; }
        public long InTimedHigh { get; set; }
        public long InTimeMedium { get; set; }
        public long InTimeLow { get; set; }
    }
}