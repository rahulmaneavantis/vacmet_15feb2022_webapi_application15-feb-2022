﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ComplianceStatusSummary
    {
        public string Email { get; set; }
        public int filterType { get; set; }
        public int CustomerbranchID { get; set; }
        //public string SDate { get; set; }
        //public string ToDate { get; set; }
       // public int CId { get; set; }
        public List<string> CID { get; set; }
        public string Period { get; set; }
    }
}

