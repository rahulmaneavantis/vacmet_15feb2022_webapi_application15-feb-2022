﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class NewsLetterSearch
    {
        public class Shards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int skipped { get; set; }
            public int failed { get; set; }
        }

        public class Source
        {
            public int ID { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public DateTime NewsDate { get; set; }
            public int CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public int UpdatedBy { get; set; }
            public DateTime UpdatedDate { get; set; }
            public bool IsDeleted { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string DocFileName { get; set; }
            public string DocFilePath { get; set; }
            public long intnewsletterdate { get; set; }
        }

        public class Hit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public double _score { get; set; }
            public Source _source { get; set; }
        }

        public class Hits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class RootObject
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public Shards _shards { get; set; }
            public Hits hits { get; set; }
        }
    }
}