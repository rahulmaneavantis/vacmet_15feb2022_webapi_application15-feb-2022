﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class SearchCompliance
    {
        public class Shards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int failed { get; set; }
            public List<object> failures { get; set; }
        }

        public class Aggregations
        {
        }

        public class Suggest
        {
        }

        public class Fields
        {
        }

        public class Source
        {
            public int complianceid { get; set; }
            public string description { get; set; }
            public int actid { get; set; }
            public string sections { get; set; }
            public string shortdescription { get; set; }
            public string penaltydescription { get; set; }
            public string referencematerialtext { get; set; }
            public string sampleformlink { get; set; }
            public string compliancesampleform { get; set; }            
            public string compliancesampleformpath { get; set; }
            public int ismyfavourite { get; set; }
        }

        public class InnerHits
        {
        }

        public class Highlights
        {
        }

        public class Hit
        {
            public Fields fields { get; set; }
            public Source _source { get; set; }
            public string _index { get; set; }
            public InnerHits inner_hits { get; set; }
            public double _score { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public List<object> sort { get; set; }
            public Highlights highlights { get; set; }
            public List<object> matched_queries { get; set; }
        }

        public class Hits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class Field
        {
        }

        public class RootObject
        {
            public Shards _shards { get; set; }
            public Aggregations aggregations { get; set; }
            public Suggest suggest { get; set; }
            public int took { get; set; }
            public bool timed_out { get; set; }
            public bool terminated_early { get; set; }
            public Hits hits { get; set; }
            public int num_reduce_phases { get; set; }
            public List<Field> fields { get; set; }
        }
    }
}