﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ComplianceStatusSummaryDetail
    {
        public string Email { get; set; }
        //public int CategoryID { get; set; }
        public List<string> CategoryID { get; set; }
        public int locationId { get; set; }
        public List<string> RiskId { get; set; }
        //public int RiskId { get; set; }
        public string Flag { get; set; }
        public int IsFlag { get; set; }
        public int page { get; set; }
        public string Period { get; set; }
    }
}