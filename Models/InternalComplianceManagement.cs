﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
using System.Globalization;

namespace AppWebApplication.Models
{
   public class InternalComplianceManagement
    {
        public static void BindInternalStatutoryCheckListDatainRedis(long InternalComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (true)
                    {
                        var statusList = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_PRA_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_PRA_3";
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_REV_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_REV_3";
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_APPR_3";
                                }
                                else
                                {
                                    CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_APPR_3";
                                }
                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.InternalComplianceInstances
                                        join row1 in entities.InternalCompliances
                                        on row.InternalComplianceID equals row1.ID
                                        join row3 in entities.EntitiesAssignmentInternals
                                        on (long)row1.IComplianceCategoryID equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == InternalComplianceInstanceID && row1.IsDeleted == false
                                        && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetInternalCheckListData_" + item + "_" + Customerid + "_MGMT_3";
                            }
                            else
                            {
                                CacheName = "CacheGetInternalCheckListData_" + item + "_" + Customerid + "_MGMT_3";
                            }
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static long? GetInternalComplianceInstanceID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstanceID = (from row in entities.InternalComplianceScheduledOns
                                            where row.ID == ScheduleOnID
                                            select row.InternalComplianceInstanceID).FirstOrDefault();
                return ComplianceInstanceID;
            }
        }
        public static void CreateTransaction(List<InternalComplianceTransaction> transaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalComplianceTransactions.Add(entry);
                });
                entities.SaveChanges();
            }
        }

        #region Mrunal
        public static List<InternalComplianceReminderListView> GetComplianceReminderNotificationsByUserID(int userID, string filter = null)
        {
            DateTime date = DateTime.Now.Date;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<InternalComplianceReminderListView> complianceReminders = (from row in entities.InternalComplianceReminderListViews
                                                                                where row.UserID == userID && row.RemindOn >= date && row.Status == 0
                                                                                select row).OrderBy(entry => entry.RemindOn).ToList();


                if (!string.IsNullOrEmpty(filter))
                {
                    complianceReminders = complianceReminders.Where(entry => entry.CustomerBranchName.ToUpper().Contains(filter.ToUpper()) || entry.IShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Description.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceReminders;
            }

        }
        #endregion


        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPGetMappedComplianceCheckListInternal(long UserID,
List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MaterInternalCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterInternalCheckListTransactionsQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
                                         && row.InternalComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();
                }
                return transactionsQuery;
            }
        }

        //public static List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> SPGetMappedComplianceCheckListInternal(long UserID,
        //  List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> MaterInternalCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> transactionsQuery = new List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result>();
        //        if (IsPerformer == true)
        //        {
        //            int performerRoleID = (from row in entities.Roles
        //                                   where row.Code == "PERF"
        //                                   select row.ID).Single();


        //            DateTime now = DateTime.UtcNow.Date;
        //            transactionsQuery = (from row in MaterInternalCheckListTransactionsQuery //entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 where row.UserID == UserID && row.RoleID == performerRoleID
        //                                 && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
        //                                 && row.InternalComplianceStatusID == 1
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList();
        //        }
        //        return transactionsQuery;
        //    }
        //}

        public static InternalCompliance GetByID(long InternalcomplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  where row.ID == InternalcomplianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }


        public static List<InternalComplianceSchedule> GetScheduleByComplianceID(long internalcomplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.InternalComplianceSchedules
                                    where row.IComplianceID == internalcomplianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }


        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:

                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    //string yearq = date.ToString("yy");
                    //if (forMonth == 10)
                    //{
                    //    //yearq = (date.AddYears(-1)).ToString("yy");
                    //    if (date.ToString("MM") == "12")
                    //    {
                    //        yearq = (date).ToString("yy");
                    //    }
                    //    else
                    //    {
                    //        yearq = (date.AddYears(-1)).ToString("yy");
                    //    }
                    //}
                    //forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }

                        //year1 = (date.AddYears(-1)).ToString("yy");

                    }
                    if (forMonth == 10)
                    {
                        // year1 = (date.AddYears(-1)).ToString("yy");
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }

                    //string year1 = date.ToString("yy");
                    //if (forMonth == 7)
                    //{
                    //    year1 = (date.AddYears(-1)).ToString("yy");

                    //}

                    //if (forMonth == 10)
                    //{
                    //    year1 = (date.AddYears(-1)).ToString("yy");
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(4).Substring(0, 3) + " " + date.ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                    //               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    //}

                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    }

                    if (forMonth == 1)
                    {
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        year2 = (date.AddYears(-1)).ToString("yy");

                    }

                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }

                    break;
                case 5:
                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    //string startFinancial2Year = date.ToString("yy");
                    //string endFinancial2Year = date.ToString("yy");
                    //if (date.Month >= 4)
                    //{
                    //    startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                    //    endFinancial2Year = date.ToString("yy");
                    //}
                    //else
                    //{
                    //    startFinancial2Year = (date.AddYears(-3)).ToString("yy");
                    //    endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                    //}

                    //forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
        public static List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> GetInternalComplianceDetailsDashboard(int customerid, List<long> Branchlist, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_InternalComplianceInstanceAssignmentViewDetails_Result>();


                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "APPR")
                                         select row).ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();

                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                //if (CategoryID != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.InternalComplianceCategoryID == CategoryID).Distinct().ToList();
                //}

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }


        public static InternalCompliance GetInternalComplianceByInstanceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.InternalComplianceTransactions
                                             where row.InternalComplianceScheduledOnID == ScheduledOnID
                                             select row.InternalComplianceInstanceID).FirstOrDefault();

                long complianceID = (from row in entities.InternalComplianceInstances
                                     where row.ID == complianceInstanceID
                                     select row.InternalComplianceID).FirstOrDefault();

                var compliance = (from row in entities.InternalCompliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        public static List<InternalComplianceView> GetInternalCompliance(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.InternalComplianceTransactions
                                             where row.InternalComplianceScheduledOnID == ScheduledOnID
                                             select row.InternalComplianceInstanceID).FirstOrDefault();

                long complianceID = (from row in entities.InternalComplianceInstances
                                     where row.ID == complianceInstanceID
                                     select row.InternalComplianceID).FirstOrDefault();

                var compliance = (from row in entities.InternalComplianceViews
                                  where row.ID == complianceID
                                  select row).ToList();

                return compliance;
            }
        }
        public static RecentInternalComplianceTransactionView GetCurrentStatusByInternalComplianceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.RecentInternalComplianceTransactionViews
                                       where row.InternalComplianceScheduledOnID == ScheduledOnID
                                       select row).First();

                return currentStatusID;
            }
        }
        public static List<InternalComplianceInstanceTransactionView> GetPeriodBranchLocationPerformerInternal(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalComplianceInstanceTransactionViews
                             where row.InternalScheduledOnID == scheduledOnID
                             && row.InternalComplianceInstanceID == complianceInstanceID
                             select row).Distinct().ToList();

                return query;
            }
        }
        public static InternalComplianceInstanceTransactionView GetInstanceTransactionInternalCompliance(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.InternalScheduledOnID == ScheduledOnID && row.RoleID == 3
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).FirstOrDefault();

                return ReviceCompliances;
            }
        }
        public static bool CreateTransaction(InternalComplianceTransaction transaction, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //entities.Connection.Open();
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {
                                DocumentManagement.SaveDocFiles(filesList);
                                //transaction.Dated = DateTime.UtcNow; Comment on 19 JAN 2017 Sachin Nikam
                                transaction.Dated = DateTime.Now;
                                //entities.InternalComplianceTransactions.Attach(transaction);
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();

                            }
                            catch (Exception ex)
                            {

                                var aa = ex.Message.ToString();
                            }

                            long transactionId = Convert.ToInt64(transaction.ID);

                            if (files != null)
                            {

                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = transaction.InternalComplianceScheduledOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);

                                }

                            }
                            entities.SaveChanges();

                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            //entities.Connection.Close();

                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }

                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("gaurav@tlregtech.in");
                TO.Add("rahul@tlregtech.in");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO),null,
                    null, "Error Occured as Internal CreateTransaction Function Andriod Service", "Internal CreateTransaction");

                return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static List<InternalComplianceInstanceCheckListTransactionView> GetMappedComplianceCheckListInternal(long UserID, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
                                         && row.InternalComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                return transactionsQuery;
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForPerformer_ChecklistInternal(int userID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery1 = new List<InternalComplianceInstanceCheckListTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                     where row.UserID == userID && row.RoleID == performerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                      && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now) && row.InternalComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                transactionsQuery1 = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                      where row.UserID == userID && row.RoleID == performerRoleID
                                     && (row.InternalScheduledOn == now || row.InternalScheduledOn <= now)
                                     && row.InternalComplianceStatusID == 1
                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                      select row).ToList();


                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForReviewer_ChecklistInternal(int userID, int risk, int location, int type, int category, DateTime FromDate, DateTime ToDate, string StringType)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                int ReviewerRoleID = (from row in entities.Roles
                                      where row.Code == "RVW1"
                                      select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                     where row.UserID == userID && row.RoleID == ReviewerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                     //&& (row.InternalScheduledOn == now || row.InternalScheduledOn <= now) && row.InternalComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }      
        public static List<long> GetInstanceIDfromReviewer(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var InstanceIDs = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == UserID && row.RoleID == 4
                                   select row.InternalComplianceInstanceID).ToList();

                return InstanceIDs;
            }
        }

        public static List<long> GetPerformerListUsingReviewer(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var InstanceIDs = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == UserID && row.RoleID == 4
                                   select row.InternalComplianceInstanceID).ToList();

                var performerList = (from row in entities.InternalComplianceAssignments
                                     where InstanceIDs.Contains(row.InternalComplianceInstanceID) && row.RoleID == 3
                                     select row.UserID).ToList();

                return performerList;
            }
        }
     
    }
}