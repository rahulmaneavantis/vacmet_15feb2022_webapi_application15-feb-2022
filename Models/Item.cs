﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class Item
    {

         public string ItemName { get; set; }
              
         public long highcount { get; set; }
         public long CompletedCountHigh { get; set; }
         public long AfterDueDatecountHigh { get; set; }
         public long NotCompletedcountHigh { get; set; }

         public long mediumCount { get; set; }
         public long CompletedCountMedium { get; set; }
         public long AfterDueDatecountMedium { get; set; }
         public long NotCompletedcountMedium { get; set; }

         public long lowcount { get; set; }
         public long CompletedCountLow { get; set; }
         public long AfterDueDatecountLow { get; set; }
         public long NotCompletedcountLow { get; set; }
     
    }
}