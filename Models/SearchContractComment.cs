﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class SearchContractComment
    {
        public class Shards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int failed { get; set; }
            public List<object> failures { get; set; }
        }

        public class Aggregations
        {
        }

        public class Suggest
        {
        }

        public class Fields
        {
        }

        public class Source
        {
            public string _id { get; set; }
            public int contractID { get; set; }
            public int taskID { get; set; }
            public int user_id { get; set; }
            public int customer_id { get; set; }
            public string comment { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public DateTime deleted_at { get; set; }
            public string message_id { get; set; }
            public string user_name { get; set; }
        }

        public class InnerHits
        {
        }

        public class Highlights
        {
        }

        public class Hit
        {
            public Fields fields { get; set; }
            public Source _source { get; set; }
            public string _index { get; set; }
            public InnerHits inner_hits { get; set; }
            public double _score { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public List<object> sort { get; set; }
            public Highlights highlights { get; set; }
            public List<object> matched_queries { get; set; }
        }

    
        public class Hits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class Field
        {
        }

        public class RootObject
        {
            public Shards _shards { get; set; }
            public Aggregations aggregations { get; set; }
            public Suggest suggest { get; set; }
            public int took { get; set; }
            public bool timed_out { get; set; }
            public bool terminated_early { get; set; }
            public Hits hits { get; set; }
            public int num_reduce_phases { get; set; }
            public List<Field> fields { get; set; }
        }
    }
}