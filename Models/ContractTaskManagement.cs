﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AppWebApplication.Data;
using AppWebApplication.Models.Litigation;

namespace AppWebApplication.Models
{
    public class ContractTaskManagement
    {
        public static int GetCountMyReviews(long customerID, int UserID, string Role)
        {
            int Count = 0;
            try
            {
                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), UserID, Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), UserID, Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedPrimaryApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), UserID, Role,
                   29, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContracts.Count + lstAssignedApproverContracts.Count + lstAssignedPrimaryApproverContracts.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static List<Cont_SP_AuditLogs_All_Result> GetContractAuditLogs_All(int customerID, long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAuditLogs = (from row in entities.Cont_SP_AuditLogs_All(customerID, contractID)
                                    select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                return lstAuditLogs.ToList();
            }
        }
        public static List<Cont_SP_GetReviwedContracts_All_Result> GetAssignedContractsTemplateList(int customerID, int loggedInUserID, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetReviwedContracts_All(customerID, loggedInUserID, roleID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();
                    
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ID, //ContractID
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.ContractTypeID,
                                 g.TypeName,
                                 g.ContractSubTypeID,
                                 g.SubTypeName,
                                 g.ProposalDate,
                                 g.AgreementDate,
                                 g.EffectiveDate,
                                 g.ReviewDate,
                                 g.ExpirationDate,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.StatusName,
                                 g.ContractAmt,
                                 g.ContactPersonOfDepartment,
                                 g.PaymentType,
                                 g.AddNewClause,
                                 g.Owner
                             } into GCS
                             select new Cont_SP_GetReviwedContracts_All_Result()
                             {
                                 ID = GCS.Key.ID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 TypeName = GCS.Key.TypeName,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 SubTypeName = GCS.Key.SubTypeName,
                                 ProposalDate = GCS.Key.ProposalDate,
                                 AgreementDate = GCS.Key.AgreementDate,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ReviewDate = GCS.Key.ReviewDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 StatusName = GCS.Key.StatusName,
                                 ContractAmt = GCS.Key.ContractAmt,
                                 ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                 PaymentType = GCS.Key.PaymentType,
                                 AddNewClause = GCS.Key.AddNewClause,
                                 Owner = GCS.Key.Owner
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }


        public static List<Cont_SP_AssignContractMilestone_Result> GetContractListformilestone(long cutomerID, long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractMilestoneList = entities.Cont_SP_AssignContractMilestone(Convert.ToInt32(cutomerID), Convert.ToInt32(userID)).ToList();
                return lstContractMilestoneList;
            }
        }
        public static List<Cont_SP_AssignMilestone_Result> GetContractMilestoneList(long cutomerID, long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractMilestoneList = entities.Cont_SP_AssignMilestone(Convert.ToInt32(cutomerID), Convert.ToInt32(userID)).ToList();
                return lstContractMilestoneList;
            }
        }
        public static List<Cont_SP_AssignMilestoneDetailNew_Result> GetContractMilestoneDetail(long cutomerID, long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractMilestoneType = entities.Cont_SP_AssignMilestoneDetailNew(Convert.ToInt32(cutomerID), Convert.ToInt32(userID)).ToList();
                return lstContractMilestoneType;
            }
        }
        public static List<Cont_SP_GetTaskDetails_UserRoleWise_Result> GetTaskDetailsByUserRoleWise(long contractID, long taskID, int userID, int roleID) //, int customerID
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_SP_GetTaskDetails_UserRoleWise(contractID, taskID, userID, roleID)
                                   select row).ToList();
                return queryResult;
            }
        }
        public static List<Cont_tbl_TaskUserAssignment> GetContractTaskUserAssignment(int customerID, long contractID, long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskUserAssignment
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }
        public static List<Cont_SP_GetAssignedTasks_All_Result> GetAssignedTaskList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, string taskStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetAssignedTasks_All(customerID)
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.AssignToUserID == loggedInUserID).ToList(); //|| entry.OwnerID == loggedInUserID

                if (priorityID != 0)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                if (!string.IsNullOrEmpty(taskStatus))
                {
                    if (!taskStatus.ToUpper().Equals("ALL"))
                    {
                        query = ContractTaskManagement.GetTasksStatusWise(query, taskStatus);
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID,
                                 g.ContractType,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.TaskID,
                                 g.TaskType,
                                 g.TaskTitle,
                                 g.TaskDesc,
                                 g.AssignOn,
                                 g.DueDate,
                                 g.PriorityID,
                                 g.Priority,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.RoleID,
                                 g.StatusName,
                                 g.TaskTypeName
                             } into GCS
                             select new Cont_SP_GetAssignedTasks_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 ContractType = GCS.Key.ContractType,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 TaskID = GCS.Key.TaskID,
                                 TaskType = GCS.Key.TaskType,
                                 TaskTitle = GCS.Key.TaskTitle,
                                 TaskDesc = GCS.Key.TaskDesc,
                                 AssignOn = GCS.Key.AssignOn,
                                 DueDate = GCS.Key.DueDate,
                                 PriorityID = GCS.Key.PriorityID,
                                 Priority = GCS.Key.Priority,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 RoleID = GCS.Key.RoleID,
                                 StatusName = GCS.Key.StatusName,
                                 TaskTypeName = GCS.Key.TaskTypeName
                             }).ToList();
                }



                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.DueDate)
                        .ThenBy(entry => entry.PriorityID)
                        .ThenBy(entry => entry.TaskType).ToList();
                }

                return query.ToList();
            }
        }


        public static List<Cont_SP_GetAssignedTasks_All_Result> GetTasksStatusWise(List<Cont_SP_GetAssignedTasks_All_Result> lstTaskRecords, string statusName)
        {
            try
            {
                if (statusName.Equals("Upcoming"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                                && (row.AssignOn > DateTime.Now && row.AssignOn <= DateTime.Now.AddDays(30))).ToList();
                    //&& (row.DueDate > DateTime.Now)

                }
                else if (statusName.Equals("Submitted for Review"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                        && row.AssignOn <= DateTime.Now
                                        && row.TaskType == 4
                                        && (row.DueDate >= DateTime.Now)).ToList();
                }
                else if (statusName.Equals("Submitted for Approval"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                        && row.AssignOn <= DateTime.Now
                                        && row.TaskType == 6
                                        && row.DueDate >= DateTime.Now).ToList();
                }
                else if (statusName.Equals("Reviewed/Approved"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => row.StatusName.Contains("Submitted")).ToList();
                    //&& row.AssignOn >= DateTime.Now                                                                                        
                    //&& !(row.DueDate > DateTime.Now)
                }
                else if (statusName.Equals("Overdue"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress") && row.DueDate < DateTime.Now).ToList();
                }
                else if (statusName.Equals("Closed"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => row.StatusName == "Closed").ToList();
                }

                return lstTaskRecords;
            }
            catch (Exception ex)
            {
                //ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<Cont_SP_GetAssignedTasks_All_Result>();
            }
        }

    }
}