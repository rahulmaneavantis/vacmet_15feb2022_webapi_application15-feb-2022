﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class LoggerMessage
    {
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                //com.VirtuosoITech.Logger.Logger.Instance.Insert(msg); 
                if (ex != null)
                {
                    ReminderUserList.Add(new LogMessage() { LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    ReminderUserList.Add(new LogMessage() { LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
               ComplianceManagement.InsertLogToDatabase(ReminderUserList);
            }
            catch (Exception)
            {
                throw;

            }
        }
    }
}