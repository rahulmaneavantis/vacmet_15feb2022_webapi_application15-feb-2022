﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class search
    {

        public class Shards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int skipped { get; set; }
            public int failed { get; set; }
        }

        public class Source
        {
            public int actid { get; set; }
            public int compliancetypeid { get; set; }
            public int compliancecategoryid { get; set; }
            public string actname { get; set; }
            public string compliancetypename { get; set; }
            public string compliancecategoryname { get; set; }
            public int? stateid { get; set; }
            public string state { get; set; }
            public int IsMyFavourite { get; set; }            
            public long compliancecount { get; set; }            
            public long dailyupdatecount { get; set; }
        }

        public class Hit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public double _score { get; set; }
            public Source _source { get; set; }
        }

        public class Hits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class RootObject
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public Shards _shards { get; set; }
            public Hits hits { get; set; }
        }
        
        //public class Shards
        //{
        //    public int total { get; set; }
        //    public int successful { get; set; }
        //    public int skipped { get; set; }
        //    public int failed { get; set; }
        //}

        //public class Child
        //{
        //    public int ComplianceID { get; set; }
        //    public string Description { get; set; }
        //    public int ActID { get; set; }
        //    public string Sections { get; set; }
        //    public string ShortDescription { get; set; }
        //    public string PenaltyDescription { get; set; }
        //    public string ReferenceMaterialText { get; set; }
        //    public string SampleFormLink { get; set; }
        //}

        //public class Source
        //{
        //    public int ActID { get; set; }
        //    public int ComplianceTypeId { get; set; }
        //    public int ComplianceCategoryId { get; set; }
        //    public string ActName { get; set; }
        //    public string ComplianceTypeName { get; set; }
        //    public string ComplianceCategoryName { get; set; }
        //    //public List<Child> Children { get; set; }
        //    public string State { get; set; }

       
        //}

        //public class Hit
        //{
        //    public string _index { get; set; }
        //    public string _type { get; set; }
        //    public string _id { get; set; }
        //    public double _score { get; set; }
        //    public Source _source { get; set; }
        //}

        //public class Hits
        //{
        //    public int total { get; set; }
        //    public double max_score { get; set; }
        //    public List<Hit> hits { get; set; }
        //}

        //public class RootObject
        //{
        //    public int took { get; set; }
        //    public bool timed_out { get; set; }
        //    public Shards _shards { get; set; }
        //    public Hits hits { get; set; }
        //}
    }
}