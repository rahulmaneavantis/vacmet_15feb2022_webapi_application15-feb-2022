﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class UpdateSubscription
    {
        public string Email { get; set; }
        public string IsTrial { get; set; }
        public string IsSubscription { get; set; }          
        public string SubscriptionStartDate { get; set; }
        public string SubscriptionEndDate { get; set; }
    }
}