﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class MyReportDetails
    {
        public List<ComplianceInstanceTransactionView> Statustory { get; set; }
        public List<ComplianceInstanceTransactionView> EventBased { get; set; }
        public List<InternalComplianceInstanceTransactionView> Internal { get; set; }
        public List<AssignLocationList> AssignLocationListObj { get; set; }

    }
}