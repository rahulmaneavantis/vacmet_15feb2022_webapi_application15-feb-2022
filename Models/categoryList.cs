﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AssignedTask
    {
        public string TaskType { get; set; }
        public string TaskTitle { get; set; }
        public System.DateTime ScheduleOnDate { get; set; }
        public string Priority { get; set; }
        public string AssignToName { get; set; }

    }
    public class count
    {
        public int NoticeOpen { get; set; }

        public int NoticeClosed { get; set; }

        public int CaseOpen { get; set; }

        public int CaseClosed { get; set; }

        public int TaskOpen { get; set; }

        public int TaskClosed { get; set; }
    }
    public class mydocumenttypeAndriod
    {
        public string RefNo { get; set; }
        public string Title { get; set; }
        public string PartyName { get; set; }
        public string PartyID { get; set; }
        public string DepartmentID { get; set; }
        public string CustomerBranchID { get; set; }
        public string Priority { get; set; }
        public string Task { get; set; }
        public string Task_Description { get; set; }
        public string Status { get; set; }
        public string Assigned_To { get; set; }
        public DateTime Due_date { get; set; }
        public string TypeName { get; set; }
        public string InstanceID { get; set; }
    }
    public class StandardReport
    {
        public long id { get; set; }
        public Nullable<int> user_id { get; set; }
        public string report_name { get; set; }
        public string pdf_name { get; set; }
        public string location { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public bool IsSend { get; set; }
        public string ReportType { get; set; }
    }
    public class OutputDetail
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class categoryList
    {
        public int HR { get; set; }
        public int Finance { get; set; }
        public int Commercial { get; set; }
        public int Corporate { get; set; }
        public int Customs { get; set; }
        public int EHS { get; set; }
        public int Local { get; set; }
        public int Regulatory { get; set; }
        public int InternalCompliance { get; set; }
        public int FEMA { get; set; }
        public int Secretarial { get; set; }
        public int ClientSpecific { get; set; }
    }
}