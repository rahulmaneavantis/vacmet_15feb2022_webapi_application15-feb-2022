﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace AppWebApplication.Models
{
    public class BranchClass
    {
        public static List<NameValueHierarchy> GetAllHierarchyVendor(long customerID, List<int> LocationList)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, LocationList, customerID);
                }
            }

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAllAssignedHierarchySatutory(long customerID, List<int> LocationList, long userID)
        {

            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             join row1 in entities.LitigationEntitiesAssignments
                             on row.ID equals row1.BranchID
                             select Cust).Distinct();
                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();


                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, LocationList, customerID);
                }
            }

            return hierarchy;
        }
        //public static List<NameValueHierarchy> GetAllAssignedHierarchySatutory(long customerID, List<int> LocationList, long userID)
        //{

        //    List<NameValueHierarchy> hierarchy = null;
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        var query = (from row in entities.CustomerBranches
        //                     join Cust in entities.Customers
        //                     on row.CustomerID equals Cust.ID
        //                     join row1 in entities.LitigationEntitiesAssignments
        //                     on row.ID equals row1.BranchID
        //                     select Cust).Distinct();

        //        if (customerID != -1)
        //        {
        //            query = query.Where(entry => entry.ID == customerID);
        //        }

        //        hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

        //        foreach (var item in hierarchy)
        //        {
        //            LoadSubEntities(item, true, entities, LocationList, customerID);
        //        }
        //    }

        //    return hierarchy;
        //}

        public static bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, List<int> LocationList, long customerID)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
                var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in subEntities)
                {
                    LoadSubEntities(item, false, entities, LocationList, customerID);
                    if (FindNodeExists(item, LocationList))
                    {                                                
                        nvp.Children.Add(item);
                    }                   
                }
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
                var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in subEntities)
                {
                    LoadSubEntities(item, false, entities, LocationList, customerID);
                    if (FindNodeExists(item, LocationList))
                    {                        
                        nvp.Children.Add(item);
                    }                    
                }
            }           
        }

        public static List<long> GetAllHierarchy(int customerID, int customerbranchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {              
                var list = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch(customerbranchid, customerID) select (long)row).ToList();
                return list.ToList();
            }            
        }
       
        public static List<NameValueHierarchy> GetAllHierarchySatutory(long customerID,List<int> LocationList)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, LocationList, customerID);
                }
            }

            return hierarchy;
        }
    }
}