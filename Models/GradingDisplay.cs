﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models
{
    public class CalendarCompliances
    {
        public DataTable PList { get; set; }
        public DataTable RList { get; set; }
    }

    public class GradingDisplay
    {
        public static List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> GetComplianceDetailsDashboard_DeptHead(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int ActId, int CategoryID, int Userid, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> ComplianceDetails = new List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result>();
                ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "MGMT")
                                     select row).ToList();

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }
                //ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<sp_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetailsDashboard(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int ActId, int CategoryID, int Userid, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_ComplianceInstanceAssignmentViewDetails_Result>();                
                if (IsApprover == 1)
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "APPR")
                                        select row).ToList();                  
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();                  
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                //ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> GetInternalComplianceDetailsDashboard_DeptHead(int customerid, List<long> branchlist, int RoleID, int CustomerBranchID, int CategoryID, int Userid, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> ComplianceDetails = new List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result>();

                ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "MGMT")
                                     select row).ToList();

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();

                if (branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }
                //ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> GetInternalComplianceDetailsDashboard(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int CategoryID, int Userid, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_InternalComplianceInstanceAssignmentViewDetails_Result>();


                if (IsApprover == 1)
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "APPR")
                                         select row).ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();

                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.InternalComplianceCategoryID == CategoryID).Distinct().ToList();
                }

                //ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        #region Calendar
        public static DateTime GetDateCalender(string date)
        {
            string date1 = "";
            if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(0, 4) + "-" + date.Substring(8, 2) + "-" + date.Substring(5, 2);
            }

            return Convert.ToDateTime(date1);
        }
        public static bool CanChangeStatus(long userID,long actualuserid, int RoleID, int statusID, string Type)
        {
            try
            {
                bool result = false;
                if (RoleID == 3 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangePerformerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 4 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangeReviewerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Statutory CheckList")
                {
                    result = CanChangePerformerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 4 && (Type == "Statutory CheckList"))
                {
                    result = CanChangeReviewerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal")
                {
                    result = CanChangeInternalPerformerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 4 && Type == "Internal")
                {
                    result = CanChangeInternalReviewerStatus(userID, actualuserid, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal Checklist")
                {
                    result = CanChangeInternalPerformerStatus(userID, actualuserid, RoleID, statusID);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        public static bool CanChangeReviewerStatus(long userID, long actualuserid, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == actualuserid)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12 || statusID == 16 || statusID == 18;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        public static bool CanChangeInternalPerformerStatus(long userID, long actualuserid, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == actualuserid)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 8 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }

        public static bool CanChangeInternalReviewerStatus(long userID, long actualuserid, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == actualuserid)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 18;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }

        //public static bool CanChangeInternalReviewerStatus(long userID, long actualuserid, int roleID, int statusID)
        //{
        //    try
        //    {
        //        bool result = false;

        //        if (userID == actualuserid)
        //        {
        //            if (roleID == 3)
        //            {
        //                result = statusID == 1;
        //            }
        //            else if (roleID == 4)
        //            {
        //                result = statusID == 2 || statusID == 3 || statusID == 11;
        //            }
        //            else if (roleID == 5)
        //            {
        //                result = statusID == 2 || statusID == 3;
        //            }
        //            else if (roleID == 6)
        //            {
        //                result = statusID == 4 || statusID == 5 || statusID == 6;
        //            }
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //    return false;
        //}


        public static bool CanChangePerformerStatus(long userID,long actualuserid, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == actualuserid)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12 || statusID == 13 || statusID == 14;
                        //result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12;
                        //result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        public static DataTable fetchDataPerformer(int customerid, int userid,string isallorsi, string datev)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                DateTime dt =GetDateCalender(datev.ToString());
                
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 3
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 3 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                                   SLType = row.SLType,
                                                   flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based"),
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                                  SLType = row.SLType,
                                                  flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.InternalComplianceStatusID, row.ComplianceType == 1 ? "Internal Checklist" : "Internal"),
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                table.Columns.Add("SLType", typeof(string));
                table.Columns.Add("flag", typeof(bool));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["SLType"] = item.SLType;
                        dr["flag"] = item.flag;                        
                        table.Rows.Add(dr);
                    }
                }


                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["SLType"] = item.SLType;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }
        public static DataTable fetchDataApprover(int customerid, int userid, string isallorsi, string datev)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(datev.ToString());
               
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 6
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 6 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                                   flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based"),
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                                  flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.InternalComplianceStatusID, row.ComplianceType == 1 ? "Internal Checklist" : "Internal"),
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                table.Columns.Add("flag", typeof(bool));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }


                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }
        public static DataTable fetchDataManagement_DeptHead(int customerid, int userid, string isallorsi, string datev)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(datev.ToString());               
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var mgmtQueryStatutory = (from row in entities.SP_ComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                          select row).ToList();
                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var mgmtQueryInternal = (from row in entities.SP_InternalComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                         select row).ToList();

                if (mgmtQueryInternal.Count > 0)
                {
                    mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion



                var QueryStatutoryPerformer = (from row in mgmtQueryStatutory
                                               where row.PerformerScheduledOn == dt
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                                   flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based"),
                                               }).ToList();


                var QueryInternalPerformer = (from row in mgmtQueryInternal
                                              where row.InternalScheduledOn.Year == dt.Year
                                             && row.InternalScheduledOn.Month == dt.Month
                                             && row.InternalScheduledOn.Day == dt.Day
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                                  flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.InternalComplianceStatusID, row.ComplianceType == 1 ? "Internal Checklist" : "Internal"),
                                              }).ToList();


                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                table.Columns.Add("flag", typeof(bool));

                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }
                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        public static DataTable fetchDataManagement(int customerid, int userid, string isallorsi, string datev)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(datev.ToString());
               
                #region Statutory
                List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();               
                string cashstatutoryval = "MSPSD" + userid;
                try
                {
                    if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                    {
                        try
                        {
                            MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                        }
                        catch (Exception)
                        {
                            if (isallorsi == "SI")
                            {
                                var customizedChecklist = CustomerManagement.CheckForClient((int)customerid, "MGMT_Checklist");

                                if (customizedChecklist)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                                }
                            }
                            else
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            }
                        }                        
                    }
                    else
                    {
                        if (isallorsi == "SI")
                        {

                            var customizedChecklist = CustomerManagement.CheckForClient((int)customerid, "MGMT_Checklist");

                            if (customizedChecklist)
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            }
                            else
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                            }
                        }
                        else
                        {
                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                        }
                        try
                        {
                            StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                        }
                        catch (Exception)
                        {                            
                        }
                        
                    }
                }
                catch (Exception)
                {
                    if (isallorsi == "SI")
                    {
                        var customizedChecklist = CustomerManagement.CheckForClient((int)customerid, "MGMT_Checklist");

                        if (customizedChecklist)
                        {
                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                    }
                }
               

                entities.Database.CommandTimeout = 180;               
                var mgmtQueryStatutory = MasterManagementCompliancesSummaryQuery.ToList();
                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;              
                string cashinternalval = "MIPSD" + userid;
                List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                try
                {
                    if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                    {
                        try
                        {
                            MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                        }
                        catch (Exception)
                        {
                            if (isallorsi == "SI")
                            {
                                entities.Database.CommandTimeout = 180;
                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                            }
                            else
                            {
                                entities.Database.CommandTimeout = 180;
                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            }
                        }
                        
                    }
                    else
                    {
                        if (isallorsi == "SI")
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                        }
                        else
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                        }
                        try
                        {
                            StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                        }
                        catch (Exception)
                        {                            
                        }                        
                    }
                }
                catch (Exception)
                {
                    if (isallorsi == "SI")
                    {
                        entities.Database.CommandTimeout = 180;
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 180;
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                    }
                }
            
           
                var mgmtQueryInternal = MasterManagementInternalCompliancesSummaryQuery.ToList();

                if (mgmtQueryInternal.Count > 0)
                {
                    mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion



                var QueryStatutoryPerformer = (from row in mgmtQueryStatutory
                                               where row.PerformerScheduledOn == dt
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                                   flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based"),
                                               }).ToList();


                var QueryInternalPerformer = (from row in mgmtQueryInternal
                                              where row.ScheduledOn.Year == dt.Year
                                             && row.ScheduledOn.Month == dt.Month
                                             && row.ScheduledOn.Day == dt.Day
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.ComplianceInstanceID,
                                                  ScheduledOn = row.ScheduledOn,
                                                  ScheduledOnID = row.ScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.ComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                                  flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Internal Checklist" : "Internal"),
                                              }).ToList();


                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                table.Columns.Add("flag", typeof(bool));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }
                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        public static DataTable fetchDataReviewer(int customerid, int userid, string isallorsi, string datev)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(datev.ToString());
                
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {                    
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 4
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();
                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 4 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                                   SLType = row.SLType,
                                                   flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.ComplianceStatusID, row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based"),
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                                  SLType = row.SLType,
                                                  flag = CanChangeStatus(row.UserID, userid, row.RoleID, row.InternalComplianceStatusID, row.ComplianceType == 1 ? "Internal Checklist" : "Internal"),
                                              }).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                table.Columns.Add("SLType", typeof(string));
                table.Columns.Add("flag", typeof(bool));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["SLType"] = item.SLType;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }

                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        dr["SLType"] = item.SLType;
                        dr["flag"] = item.flag;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        
        #endregion      
    }
}