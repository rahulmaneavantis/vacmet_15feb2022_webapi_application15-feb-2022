﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
namespace AppWebApplication.Models
{
    public class StackExchangeRedisExtensions
    {

        private static readonly IDatabase _cache;
        private static ConnectionMultiplexer _connectionMultiplexer;
        static StackExchangeRedisExtensions()
        {
            var connection = ConfigurationManager.AppSettings["RedisConnectionsring"];
            _connectionMultiplexer = ConnectionMultiplexer.Connect(connection);
            _cache = _connectionMultiplexer.GetDatabase();
        }
        public static bool KeyExists(string key)
        {
            return _cache.KeyExists(key);
        }
        public static void Remove(string key)
        {
            _cache.KeyDelete(key);
        }
        public static T Get<T>(string key)
        {
            var r = _cache.StringGet(key);
            return Deserialize<T>(r);
        }
        public void Clear()
        {
            var endpoints = _connectionMultiplexer.GetEndPoints(true);
            foreach (var endpoint in endpoints)
            {
                var server = _connectionMultiplexer.GetServer(endpoint);
                server.FlushAllDatabases();
            }
        }
        public static List<T> GetList<T>(string key)
        {
            return (List<T>)Get(key);
        }

        public static void SetList<T>(string key, List<T> list)
        {
            Set(key, list);
        }

        public static object Get(string key)
        {
            return Deserialize<object>(_cache.StringGet(key));
        }

        public static void Set(string key, object value)
        {
            _cache.StringSet(key, Serialize(value));
        }

        static byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, o);
                byte[] objectDataAsStream = memoryStream.ToArray();
                return objectDataAsStream;
            }
        }

        static T Deserialize<T>(byte[] stream)
        {
            if (stream == null)
            {
                return default(T);
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream(stream))
            {
                T result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }
    }
}