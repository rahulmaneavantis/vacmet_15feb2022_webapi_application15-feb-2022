﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class NotificationDetails
    {
        public long? ActID { get; set; }
        public long? ComplianceID { get; set; }
        public long ID { get; set; }
        public bool? IsRead { get; set; }
        public long? NotificationID { get; set; }
        public string Remark { get; set; }
        public string ShortDetail { get; set; }
        public string Type { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UserID { get; set; }

    }
}