﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AnsLeaglDetailsOTPVerify
    {
        public string Msg { get; set; }
        public string IsTrial { get; set; }
        public string TrialStartDdate { get; set; }
        public string IsSubscription { get; set; }
        public string SubscriptionStartDate { get; set; }
        public string SubscriptionEndDate { get; set; }
        public List<int> RoleID { get; set; }
    }
}