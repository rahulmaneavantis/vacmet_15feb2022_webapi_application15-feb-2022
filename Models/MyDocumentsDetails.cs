﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class MyDocumentsDetails
    {
        public List<SP_GetComplianceDocument_Result> Statustory { get; set; }
        public List<SP_GetEventComplianceDocument_Result> EventBased { get; set; }
        public List<SP_GetInternalComplianceDocument_Result> Internal { get; set; }
        public List<SP_GetCheckListDocument_Result> Checklist { get; set; }
        public List<SP_GetInternalCheckListComplianceDocument_Result> ChecklistInternal { get; set; }
        public List<AssignLocationList> AssignLocationListObj { get; set; }

        public List<SP_GetCheckListReviewerDocument_Result> GetCheckListReviewerDocument { get; set; }
        
    }
}