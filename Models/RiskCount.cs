﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class RiskCount
    {
        public long Critical { get; set; }
        public long High { get; set; }
        public long Medium { get; set; }
        public long Low { get; set; }
    }
}