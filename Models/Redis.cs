﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public  class RedisList<T> : IList<T>
    {
        public static ConnectionMultiplexer _con;
        private string key;
       
        public  RedisList(string key)
        {
            string NewsletterolPath = ConfigurationManager.AppSettings["RedisConnectionsring"].ToString();
            this.key = key;
            _con = ConnectionMultiplexer.Connect(NewsletterolPath);

        }

        
        private  IDatabase GetRedisDb()
        {
            try
            {
                if (!_con.IsConnected)
                {
                    string NewsletterolPath = ConfigurationManager.AppSettings["RedisConnectionsring"].ToString();
                    _con = ConnectionMultiplexer.Connect(NewsletterolPath);
                }

                return _con.GetDatabase();
            }
            catch (Exception ex)
            {
                return null;                
            }                      
        }
        private  string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        private  List<T> Deserialize<T>(string serialized)
        {
            return JsonConvert.DeserializeObject<List<T>>(serialized);
        }
        public  List<T> Get(string key)
        {            
            var Database = GetRedisDb();
            if (Database != null)
            {
                var valueBytes = Database.StringGet(key);
                if (valueBytes.HasValue)
                {
                    return Deserialize<T>(valueBytes);
                     
                }
                else
                {
                    return null;
                     
                }
                
            }
            else
            {
                return null;
            }
              
        }

        public void CloseConnection()
        {
            if (_con.IsConnected)
            {
                _con.Close(true);
            }
        }
        public  bool Add<T>(string key, T value, DateTimeOffset expiresAt)
        {
            var entryBytes = Serialize(value);
            var expiration = expiresAt.Subtract(DateTimeOffset.Now);

            var Database = GetRedisDb();
            if (Database != null)
            {
                
                return Database.StringSet(key, entryBytes, expiration);
            }
            else
            {
                return false;
            }
            
        }
        public void Insert(int index, T item, string key)
        {
            var db = GetRedisDb();
            var before = db.ListGetByIndex(key, index);
            db.ListInsertBefore(key, before, Serialize(item));
        }
        public void RemoveAt(int index,string key)
        {
            var db = GetRedisDb();
            var value = db.ListGetByIndex(key, index);
            if (!value.IsNull)
            {
                db.ListRemove(key, value);
            }
        }              
        public void Clear(string key)
        {
            try
            {
                GetRedisDb().KeyDelete(key);
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        public  bool KeyExist(string key)
        {
            try
            {
               
                if (GetRedisDb()!=null)
                {
                    if (GetRedisDb().KeyExists(key))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }               
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }                             
        public bool Remove(List<T> item, string key)
        {           
            return GetRedisDb().ListRemove(key, Serialize(item)) > 0;
        }
        
    }
}