﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
using AppWebApplication.DataRisk;

namespace AppWebApplication.Models
{
    public class CustomerManagement
    {
        public static bool CheckForClient(int customerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == customerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public static URL_Customization GetURLCustomization(int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    URL_Customization exists = (from row in entities.URL_Customization
                                                where row.CustID == CustomerID
                                                select row).FirstOrDefault();

                    return exists;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static int Create(SubscriptionDetail Subscription)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int addid = 0;
                entities.SubscriptionDetails.Add(Subscription);
                entities.SaveChanges();
                addid = Convert.ToInt32(Subscription.Id);
                return addid;
            }
        }

        public static bool Exists(SubscriptionDetail obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SubscriptionDetails
                             where row.UId == obj.UId && row.CId == obj.CId
                             select row);

                if (obj.Id > 0)
                {
                    query = query.Where(entry => entry.Id != obj.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool Exists(Customer customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                                  && row.Name.Equals(customer.Name)
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static int Create(Customer customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int addid = 0;
                customer.IsDeleted = false;
                customer.CreatedOn = DateTime.UtcNow;
                entities.Customers.Add(customer);
                entities.SaveChanges();
                addid = Convert.ToInt32(customer.ID);
                return addid;
            }
        }
        public static bool Create(mst_Customer customer)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    customer.IsDeleted = false;
                    customer.CreatedOn = DateTime.UtcNow;
                    //customer.ServiceProviderID = 95;
                    //customer.IsServiceProvider = false;
                    entities.mst_Customer.Add(customer);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static void deleteCustReset(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.Customers.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.Customers.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIdCustomer(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }

        public static void deleteMstCustReset(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    var tet = entities.mst_Customer.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.mst_Customer.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIdMstCustomer(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }


        public static string CustomerGetByIDName(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row.Name).FirstOrDefault();

                return customer;
            }
        }
        public static Customer GetByID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerID
                                select row).SingleOrDefault();

                return customer;
            }
        }
    }
}