﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppWebApplication.Data;
using System.Data;
using AppWebApplication.Models;
using System.Configuration;
using System.Web;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CannedReportManagement
    {
        public static DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();


            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                    (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static List<SP_Kendo_GetALLStatutoryEBData_Result> GetCannedReportDatastatutoryEventBased(int Customerid, int userID, string FlagUser, string status)
        {

            #region New 
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_Kendo_GetALLStatutoryEBData_Result> transactionsQuery = new List<SP_Kendo_GetALLStatutoryEBData_Result>();

                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string CacheName = string.Empty;
                if (objlocal == "Local")
                {
                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
                }
                else
                {
                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
                }

                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                {
                    try
                    {
                        transactionsQuery = StackExchangeRedisExtensions.GetList<SP_Kendo_GetALLStatutoryEBData_Result>(CacheName);
                    }
                    catch (Exception ex)
                    {
                        LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (entities.SP_Kendo_GetALLStatutoryEBData(userID, Customerid, FlagUser, status)).ToList();
                    }
                }
                else
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (entities.SP_Kendo_GetALLStatutoryEBData(userID, Customerid, FlagUser, status)).ToList();
                    try
                    {
                        StackExchangeRedisExtensions.SetList<SP_Kendo_GetALLStatutoryEBData_Result>(CacheName, transactionsQuery);
                    }
                    catch (Exception ex)
                    {
                        LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                    }
                }

                transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

                if (status.Equals("Event Based"))
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
            #endregion

            //#region old
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    List<SP_Kendo_GetALLStatutoryEBData_Result> transactionsQuery = new List<SP_Kendo_GetALLStatutoryEBData_Result>();

            //    int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //    string CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
            //    transactionsQuery = (List<SP_Kendo_GetALLStatutoryEBData_Result>)HttpContext.Current.Cache[CacheName];
            //    if (transactionsQuery == null)
            //    {
            //        entities.Database.CommandTimeout = 360;
            //        transactionsQuery = (entities.SP_Kendo_GetALLStatutoryEBData(userID, Customerid, FlagUser, status)).ToList();
            //        HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //    }
            //    //if (FlagUser.Equals("MGMT"))
            //    //{
            //    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
            //    //}
            //    if (status.Equals("Event Based"))
            //    {
            //        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
            //    }
            //    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            //}
            //#endregion
        }


        public static List<SP_kendo_getSChecklist_Result> GetCannedReportDatastatutoryChecklist(int Customerid, int userID, string FlagIsApp, int StatusFlag)
        {
            
            #region New
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_kendo_getSChecklist_Result> transactionsQuery = new List<SP_kendo_getSChecklist_Result>();

                if (StatusFlag == 2)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string CacheName = string.Empty;
                    if (objlocal == "Local")
                    {
                        CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    else
                    {
                        CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                    {
                        try
                        {
                            transactionsQuery = StackExchangeRedisExtensions.GetList<SP_kendo_getSChecklist_Result>(CacheName);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            entities.Database.CommandTimeout = 360;
                            transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                                                 where row.EventFlag == null
                                                 select row).ToList();
                        }
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                                             where row.EventFlag == null
                                             select row).ToList();
                        try
                        {
                            StackExchangeRedisExtensions.SetList<SP_kendo_getSChecklist_Result>(CacheName, transactionsQuery);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        }
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else if (StatusFlag == 4)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string CacheName = string.Empty;
                    if (objlocal == "Local")
                    {
                        CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    else
                    {
                        CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                    {
                        try
                        {
                            transactionsQuery = StackExchangeRedisExtensions.GetList<SP_kendo_getSChecklist_Result>(CacheName);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            entities.Database.CommandTimeout = 360;
                            transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                                                 where row.EventFlag == true
                                                 select row).ToList();
                        }
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                                             where row.EventFlag == true
                                             select row).ToList();
                        try
                        {
                            StackExchangeRedisExtensions.SetList<SP_kendo_getSChecklist_Result>(CacheName, transactionsQuery);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        }
                    }

                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else
                {
                    return transactionsQuery;
                }

                #endregion
                
            #region OLD
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    List<SP_kendo_getSChecklist_Result> transactionsQuery = new List<SP_kendo_getSChecklist_Result>();

                //    if (StatusFlag == 2)
                //    {
                //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
                //        string CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                //        transactionsQuery = (List<SP_kendo_getSChecklist_Result>)HttpContext.Current.Cache[CacheName];
                //        if (transactionsQuery == null)
                //        {
                //            entities.Database.CommandTimeout = 180;
                //            transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                //                             where row.EventFlag == null
                //                             select row).ToList();
                //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                //        }
                //    }
                //    else if (StatusFlag == 4)
                //    {
                //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
                //        string CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                //        transactionsQuery = (List<SP_kendo_getSChecklist_Result>)HttpContext.Current.Cache[CacheName];
                //        if (transactionsQuery == null)
                //        {
                //            entities.Database.CommandTimeout = 180;
                //            transactionsQuery = (from row in entities.SP_kendo_getSChecklist(userID, Customerid, FlagIsApp)
                //                                 where row.EventFlag == true
                //                                 select row).ToList();
                //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                //        }
                //    }

                //    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

                //    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                #endregion
            }
        }
        public static List<CheckListInstanceTransactionReviewerView> GetCheckListReportDataForReviewerrNew(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformer status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RevRoleID = (from row in entities.Roles
                                 where row.Code == "RVW1"
                                 select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (Flag == "N")//call If Company Admin
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.CustomerID == Customerid && row.RoleID == RevRoleID && row.UserID == userID

                                             select row).ToList();

                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }

                    switch (status)
                    {
                        //case CheckListCannedReportPerformer.NotCompleted:                            
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        //    break;
                        case CheckListCannedReportPerformer.Status:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

                }
                else
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.UserID == userID && row.RoleID == RevRoleID
                                               && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).ToList();

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }
                    switch (status)
                    {
                        //case CheckListCannedReportPerformer.NotCompleted:                           
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        //    break;
                        case CheckListCannedReportPerformer.Status:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }


        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int ComplianceType, int risk, CannedReportFilterNewStatus status, int location, int EventID, int EventSchudeOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //int performerRoleID = (from row in entities.Roles
                //                       where row.Code == "PERF"
                //                       select row.ID).Single();

                //long RoleID = (from row in entities.Users
                //               where row.ID == userID
                //               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                //var transactionsQuery = GetCannedReportDataCompliance(Customerid);

                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();

                //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID != 1).ToList();

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                //var GetApprover = (from row in entities.ComplianceAssignments
                //                   where row.UserID == userID
                //                   select row)
                //                   .GroupBy(a => a.RoleID)
                //                   .Select(a => a.FirstOrDefault())
                //                   .Select(entry => entry.RoleID).ToList();

                //if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //    RoleID = 6;

                //if (RoleID == 8)
                //{
                transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                     join row1 in entities.EntitiesAssignments
                                      on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                     where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                     && row1.UserID == userID //&& row.RoleID == 3                                     
                                     select row).Distinct().ToList();


                if (ComplianceType == 1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                }
                else
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                }


                transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();

                transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                //}
                //else if (RoleID == 6)
                //{
                //    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();

                //    if (ComplianceType == 1)
                //    {
                //        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                //    }

                //    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
                //        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                //}
                //else
                //{
                //    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();
                //    if (ComplianceType == 1)
                //    {
                //        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                //    }

                //    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                //        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                //}


                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //if (RoleID == 8)
                //{
                //    transactionsQuery = transactionsQuery.
                //    Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).GroupBy(g=>g.ScheduledOnID).Select(g=>g.FirstOrDefault()).ToList();
                //}
                //else
                //{
                //    transactionsQuery = transactionsQuery.
                //    Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                //    && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                //    ).ToList();
                //}

                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.UserID == userID && row.RoleID == performerRoleID
                //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select row).ToList();

                //Type Filter
                //if (type != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                //}
                //category Filter
                //if (category != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                //}

                //category Filter
                //if (ActID != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                //}

                //Datr Filter
                //if (FromDate.ToString() != "01-01-1900 00:00:00" && ToDate.ToString() != "01-01-1900 00:00:00")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                //}
                //if (RoleID == 8)
                //{
                //    if (ToDate.ToString() != "01-01-1900 00:00:00")
                //    {
                //        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                //    }
                //}

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }


                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;

                    case CannedReportFilterNewStatus.NotComplied:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 17).ToList();
                        break;
                }

                //Type SubType
                //if (SubTypeID != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                //}
                // Find data through String contained in Description
                //if (StringType != "")
                //{

                //        transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, CannedReportFilterNewStatus filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                transactionsQuery = transactionsQuery.
                Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                ).ToList();


                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.UserID == userID && row.RoleID == performerRoleID
                //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select row).ToList();

                switch (filter)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && entry.ComplianceStatusID == 1).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 7).ToList();
                        break;
                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                        break;
                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5).ToList();
                        break;
                    //Added by Sachin 30 Aug 2016
                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                //var transactionsQuery = GetCannedReportDataCompliance(Customerid);

                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();

                //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID != 1).ToList();

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignments
                                          on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();

                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }


                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //if (RoleID == 8)
                //{
                //    transactionsQuery = transactionsQuery.
                //    Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).GroupBy(g=>g.ScheduledOnID).Select(g=>g.FirstOrDefault()).ToList();
                //}
                //else
                //{
                //    transactionsQuery = transactionsQuery.
                //    Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                //    && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                //    ).ToList();
                //}

                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.UserID == userID && row.RoleID == performerRoleID
                //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select row).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (RoleID == 8)
                {
                    if (ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                    }
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }
                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                //var transactionsQuery = GetCannedReportDataCompliance(Customerid);
                //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID != 1).ToList(); 

                //transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)&& (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();

                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignments
                                          on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int) entry.RoleID)
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }



                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //actid Filter
                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(actid))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }


        public static List<CheckListInstanceTransactionView> GetCheckListReportDataForPerformer(int Customerid, int userID, string Flag, CheckListReportFilterForPerformerCompletedNotCompleted status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (Flag == "N")//call If Company Admin
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                             where row.CustomerID == Customerid && row.RoleID == performerRoleID && row.UserID == userID

                                             select row).ToList();

                    ////Type Filter
                    //if (type != -1)
                    //{
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    //}
                    ////category Filter
                    //if (category != -1)
                    //{
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    //}

                    switch (status)
                    {
                        case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

                }
                else
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).ToList();
                    switch (status)
                    {
                        case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }

        public static List<CheckListInstanceTransactionView> GetCheckListReportDataForPerformerNew(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformer status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (Flag == "N")//call If Company Admin
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                             where row.CustomerID == Customerid && row.RoleID == performerRoleID && row.UserID == userID

                                             select row).ToList();

                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }

                    switch (status)
                    {
                        //case CheckListCannedReportPerformer.NotCompleted:                            
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        //    break;
                        case CheckListCannedReportPerformer.Status:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

                }
                else
                {
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).ToList();

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }
                    switch (status)
                    {
                        //case CheckListCannedReportPerformer.NotCompleted:                           
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        //    break;
                        case CheckListCannedReportPerformer.Status:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }
        //public static List<CheckListInstanceTransactionView> GetCheckListReportDataForPerformerNew(int Customerid, int userID, string Flag, int risk, CheckListReportFilterForPerformerCompletedNotCompleted status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();


        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (Flag == "N")//call If Company Admin
        //        {                    
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.CustomerID == Customerid && row.RoleID == performerRoleID  && row.UserID == userID 

        //               select row).ToList();

        //            //Type Filter
        //            if (type != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //            }
        //            //category Filter
        //            if (category != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //            }

        //            //category Filter
        //            if (ActID != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
        //            }

        //            //Datr Filter
        //            if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //            }

        //            //Risk Filter
        //            if (risk != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //            }

        //            //Location
        //            if (location != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //            }

        //            switch (status)
        //            {
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:                            
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;                       
        //            }

        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

        //        }
        //        else
        //        {
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                       && (row.IsActive != false || row.IsUpcomingNotDeleted != false)                                          
        //                                     select row).ToList();
        //            switch (status)
        //            {
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:                           
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;                       
        //            }
        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //        }              
        //    }
        //}

     
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForApprover(int Customerid, int userID, CannedReportFilterForApprover filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;


                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.UserID == userID && row.RoleID == approverRoleID
                //                          && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select row).ToList();

                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == approverRoleID
                                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();



                switch (filter)
                {
                    case CannedReportFilterForApprover.ForFinalApproval:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5).ToList();
                        break;
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

       }
}
