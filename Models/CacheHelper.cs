﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace AppWebApplication.Models
{
    public class CacheHelper
    {

        const double DefaultExpirationMins = 1440; //1 Day
        //const double DefaultExpirationMins = 10; //1 Day


        /// <summary>Adds the specified value at the specified key in the cache, with no sliding expiration and default expiry (1 day)</summary>

        /// <typeparam name="T">the type of value</typeparam>

        /// <param name="key">the cache key</param>

        /// <param name="value">the object to cache</param>

        public static void Set<T>(string key, T value)
        {

            if (string.IsNullOrWhiteSpace(key) || value == null)

                return;


            HttpContext.Current.Cache.Insert(key, value, null,

                DateTime.Now.AddMinutes(DefaultExpirationMins), Cache.NoSlidingExpiration);

        }


        ///<summary>Adds the specified value at the specified key in the cache, with no sliding expiration and default expiry (1 day) </summary>

        /// <typeparam name="T">the type of value</typeparam>

        /// <param name="key">the cache key</param>

        /// <param name="value">the object to cache</param>

        /// <param name="expirationInMinutes">the cache expiration, in minutes</param>

        public static void Set<T>(string key, T value, double expirationInMinutes)
        {

            HttpContext.Current.Cache.Insert(key, value, null,

                DateTime.Now.AddMinutes(expirationInMinutes),

                Cache.NoSlidingExpiration);

        }


        /// <summary>Gets whether the specified key exists in the cache</summary>

        /// <param name="key">the cache key</param>

        /// <returns>True if the key exists, otherwise false</returns>

        public static bool Exists(string key)
        {

            return HttpContext.Current.Cache[key] != null;

        }


        /// <summary>Removes the specified key and associated object from the cache</summary>

        /// <param name="key">the cache key to remove</param>

        public static void Remove(string key)
        {

            HttpContext.Current.Cache.Remove(key);

        }


        /// <summary>Gets the cached object associated with the provided key</summary>

        /// <typeparam name="T">Type of cached item</typeparam>

        /// <param name="key">Name of item</param>

        /// <param name="value">the cached item</param>

        /// <returns>True if the cached item is retrieved, otherwise False</returns>

        public static bool Get<T>(string key, out T value)
        {

            try
            {

                if (Exists(key))
                {

                    value = (T)HttpContext.Current.Cache[key];

                    return true;

                }

            }
            catch (InvalidCastException)
            { //TODO: Handle exceptions accordingly

            }


            value =

                default(T);

            return false;

        }


        /// <summary> Gets the cached object with the provided key if it exists

        ///  or executes the getter func, caches the result and returns it.

        /// </summary>

        /// <typeparam name="T">Type of cached item</typeparam>

        /// <param name="key">Name of item</param>

        /// <param name="getter">the func to execute if key does not exist in the Cache</param>

        /// <returns>the cached item associated with the key</returns>

        public static T GetOrSet<T>(string key, Func<T> getter)
        {

            T value;

            if (!Exists(key) || !Get(key, out value))
            {

                value = getter();

                Set(key, value);

            }

            return value;

        }

    }
}