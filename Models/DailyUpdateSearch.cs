﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class InternalComplianceTypeList
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class MyGraphData
    {
        public List<SP_InternalLicenseInstanceTransactionCount_Result> Internal { get; set; }
        public List<SP_LicenseInstanceTransactionCount_Result> Statutory { get; set; }

    }
    public class DailyUpdateDetailwithcountOutput
    {
        public List<RootObjectDailyUpdate> dailyUpdatedata { get; set; }
        public int Countdetail { get; set; }
    }
    public class RootObjectDailyUpdate
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string CreatedDate { get; set; }
        public List<string> Acts { get; set; }
        public List<string> Categories { get; set; }
        public List<string> States { get; set; }
        public List<string> Types { get; set; }
        public string DailyUpdateStateName { get; set; }
        public string DailyUpdateCategoryName { get; set; }
    }

    public class DailyUpdateDetailNewOutput
    {
        public List<DailyUpdateElastic_Result> dailyUpdatedata { get; set; }
        public int Countdetail { get; set; }
    }

    public class DailyUpdateDetailNewFiler
    {
        public string ActName { get; set; }
        public string CategoryName { get; set; }
        public string StateName { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Filter { get; set; }
        public int page { get; set; }
    }

    public class DailyUpdateDetailNew
    {
        public List<string> ActId { get; set; }
        public List<string> CategoryID { get; set; }
        public List<string> StateId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int page { get; set; }
    }

    public class DailyUpdateSearch
    {
        public class Shards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int skipped { get; set; }
            public int failed { get; set; }
        }

        public class Source
        {
            public int id { get; set; }
            public int dailyupdateid { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public int actid { get; set; }
            public int compliancetypeid { get; set; }
            public int compliancecategoryid { get; set; }
            public int stateid { get; set; }
            public string actname { get; set; }
            public string compliancetypename { get; set; }
            public string compliancecategoryname { get; set; }
            public string state { get; set; }            
            public DateTime createddate { get; set; }
            public int ismyfavourite { get; set; }
            public long intdaliyupdatedate { get; set; }
        }

        public class Hit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public double _score { get; set; }
            public Source _source { get; set; }
        }

        public class Hits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class RootObject
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public Shards _shards { get; set; }
            public Hits hits { get; set; }
        }
    }
}