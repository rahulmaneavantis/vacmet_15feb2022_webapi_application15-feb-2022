﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class MyReportTaskResult
    {
        public List<SP_TaskInstanceTransactionStatutoryViewNew_Result> Statustory { get; set; }
        public List<SP_TaskInstanceTransactionInternalViewNew_Result> Internal { get; set; }
      
    }
}