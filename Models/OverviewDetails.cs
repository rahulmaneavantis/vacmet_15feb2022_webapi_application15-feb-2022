﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class DailyUpdateDetailwithcountOutputNew
    {
        public List<RootObjectDailyUpdateNew> dailyUpdatedata { get; set; }
        public int Countdetail { get; set; }
    }
    public class RootObjectDailyUpdateNew
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string CreatedDate { get; set; }
        public List<string> Acts { get; set; }
        public List<string> Categories { get; set; }
        public List<string> States { get; set; }
        public List<string> Types { get; set; }
        public string DailyUpdateStateName { get; set; }
        public string DailyUpdateCategoryName { get; set; }
    }
    public class DocumentDetailNew
    {
        public List<GetComplianceDocumentsViewNew1> complianceDetail { get; set; }
        public List<GetInternalComplianceDocumentsViewNew1> InternalcomplianceDetail { get; set; }
    }
    public class GetInternalComplianceDocumentsViewNew1
    {
        public Nullable<int> ID { get; set; }
        public int FileID { get; set; }
        public Nullable<long> TransactionID { get; set; }
        public Nullable<long> InternalComplianceScheduledOnID { get; set; }
        public string FileName { get; set; }
        public Nullable<int> FileType { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string UploadedName { get; set; }
    }
    public class GetComplianceDocumentsViewNew1
    {
        public Nullable<int> ID { get; set; }
        public int FileID { get; set; }
        public Nullable<long> TransactionID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public string FileName { get; set; }
        public Nullable<int> FileType { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string UploadedName { get; set; }
    }

    public class AuditLogDetailNew
    {
        public List<SP_GetAuditLogDetailandroid_Result> complianceDetail { get; set; }
        public List<SP_GetAuditLogDetailInternalandroid_Result> InternalcomplianceDetail { get; set; }
    }

    public class ComplianceOverviewDetailNew
    {
        public List<SP_GetComplianceDetails_Result> complianceDetail { get; set; }
        public List<InternalComplianceInstanceTransactionViewApp> InternalcomplianceDetail { get; set; }
    }

    public class OverviewDetails
    {
        public List<ComplianceInstanceTransactionView> complianceInstance { get; set; }
        public List<GetComplianceDocumentsView> complianceDocument { get; set; }
        public List<ComplianceView> compliance { get; set; }
        public List<SP_GetAuditLogDetailandroid_Result > auditLog { get; set; }
        public List<InternalComplianceInstanceTransactionView> complianceInstanceInternal { get; set; }
        public List<GetInternalComplianceDocumentsView> complianceDocumentInternal { get; set; }
        public List<InternalComplianceView> complianceInternal { get; set; }
        public List<SP_GetAuditLogDetailInternalandroid_Result> auditLogInternal { get; set; }
    }


    public partial class ComplianceDocument
    {
        public Nullable<int> ID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public int FileID { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
    }
}