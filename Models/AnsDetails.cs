﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class MyCheckListResult
    {
        public List<Sp_ChecklistTransaction_Result> Statutory { get; set; }
        public List<Sp_InternalChecklistTransaction_Result> Internal { get; set; }
        public List<Sp_ChecklistTransaction_Result> Eventbased { get; set; }
    }
    public class AnsDetails
    {
        public string StatusCheck { get; set; }
        public string Msg { get; set; }
        public List<int> RoleID { get; set; }

        public int LitigationRoleID { get; set; }
        public List<int> ContractRoleID { get; set; }
        
            public int SecretarialRoleID { get; set; }

        public List<int> OtherRoleID { get; set; }
    }

    public class GetEmail
    {
        public string Email { get; set; }
        public string DeviceIDIMEI { get; set; }
    }
}