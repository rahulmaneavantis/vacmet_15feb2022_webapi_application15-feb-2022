﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CloseDelay
    {
        
        public string CloseDelayVisible { get; set; }
        public string chkPenaltySaveReview { get; set; }
        public string txtInterestReview { get; set; }
        public string txtPenaltyReview { get; set; }

    }
}