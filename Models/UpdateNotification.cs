﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
  public class MyLicenseDetails
     {
        public string CustomerBrach { get; set; }
        public string LicensetypeName { get; set; }
        public string LicenseNo { get; set; }
        public string Licensetitle { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
        public string DeptName { get; set; }
        public DateTime EndDate { get; set; }
        public string MGRStatus { get; set; }
        public bool StatusResult { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceScheduleOnID { get; set; }
        public int CustomerBranchID { get; set; }
        public Nullable<long> DepartmentID { get; set; }
        public long LicenseTypeID { get; set; }
        public int StatusID { get; set; }
        public long LicenseID { get; set; }
        public Nullable<long> ComplianceID { get; set; }
        public string Status { get; set; }



    }

    public class MyFunctionCategoryData
    {
        public List<classCategoryFunctions> Data { get; set; }
    }

    public class ComplianceDetail
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public long ScheduledOnID { get; set; }
        public int CustomerID { get; set; }
        public string remark { get; set; }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public int ComplianceTypeID { get; set; }
    }
    public class updateComplianceDetail
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public long ComplianceStatusID { get; set; }
        public long ScheduledOnID { get; set; }
        public int CustomerID { get; set; }
        public string remark { get; set; }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public long ComplianceTypeID { get; set; }
        public Nullable<DateTime> ScheduledOn { get; set; }
        public string ReportType { get; set; }

    }

    public class ChecklistupdateComplianceDetail
    {
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public long ScheduledOnID { get; set; }
        public int CustomerID { get; set; }
        public string remark { get; set; }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public int ComplianceTypeID { get; set; }
        public int IsApplicable { get; set; }
        public string remark1 { get; set; }
    }

    public class classCategoryFunctions
    {
        public string Name { get; set; }
        public Nullable<int> Id { get; set; }
        public int CustomerBranchID { get; set; }
        public int ComplianceId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public int ComplianceCount { get; set; }
        public int UserCount { get; set; }
        public int LocationCount { get; set; }

    }


    //public partial class sp_ComplianceAssignedCategory_Result
    //{
    //    public string Name { get; set; }
    //    public Nullable<int> Id { get; set; }
    //    public int CustomerBranchID { get; set; }
    //    public int ComplianceId { get; set; }
    //    public int UserId { get; set; }
    //    public int ComplianceCategoryId { get; set; }
    //}

    public class AddReminder
    {
        public string Email { get; set; }
        public string type { get; set; }
        public string InstanceID { get; set; }
        public string Reminder { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string RemindDate { get; set; }
        public int ReminderID { get; set; } //0
        public string Flag { get; set; }//Delete
    }
    public class Assignmentdatails
    {
        public List<ComplianceCategorydetail> Category { get; set; }
        public List<USP_GetStatutoryInternalAssignment_New_Result> Assignmentdata { get; set; }
    }
    public class ComplianceCategorydetail
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class updateFundDocumentDetail
    {
        public long ID { get; set; }
        public int CustomerID { get; set; }
        public int UserID { get; set; }
        public string StartDate { get; set; }

    }
    public class UpdateNotification
    {
        public string Message { get; set; }
        public string MessageType { get; set; }

    }
    public class UpdateNotificationGoogle
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<int> RoleID { get; set; }

        public int LitigationRoleID { get; set; }
        public int SeretarialRoleID { get; set; }
        
        public List<int> ContractRoleID { get; set; }
        public string Token { get; set; }
        public string CurrentEmailName { get; set; }
        public string UserContact { get; set; }
        public string UserEmail { get; set; }
        
    }
    public class CustomerEntities
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    public class classMangementFunctions
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public int ComplianceCount { get; set; }
    }
    public class CustomerLocations
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }

    public class classMangementUsers
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }        
        public string Email { get; set; }
        public string ContactNo { get; set; }

    }
}