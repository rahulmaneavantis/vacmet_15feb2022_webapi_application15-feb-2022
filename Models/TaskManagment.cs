﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class TaskManagment
    {
        public static List<SP_TaskInstanceTransactionStatutoryViewNew_Result> GetCannedReportStatutoryTask(int Customerid, int userID, string FlagIsApp)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_TaskInstanceTransactionStatutoryViewNew_Result> transactionsQuery = new List<SP_TaskInstanceTransactionStatutoryViewNew_Result>();

                transactionsQuery = (from row in entities.SP_TaskInstanceTransactionStatutoryViewNew(Customerid)
                                     select row).ToList();

                if (FlagIsApp != "" && FlagIsApp != "MGMT")
                {
                    List<int> RoleIDs = new List<int>();
                    RoleIDs.Add(3);
                    RoleIDs.Add(4);
                    //RoleIDs.Add(5);

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && RoleIDs.Contains((int)entry.RoleID)
                        && (entry.IsUpcomingNotDeleted != false)).ToList();
                    //transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == RoleID
                    //        && (entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    List<int> RoleIDs = new List<int>();
                    RoleIDs.Add(3);
                    //RoleIDs.Add(4);

                    transactionsQuery = transactionsQuery.Where(entry => RoleIDs.Contains((int)entry.RoleID) && (entry.IsUpcomingNotDeleted != false)).ToList();

                    transactionsQuery = (from row in transactionsQuery
                                         join row1 in entities.EntitiesAssignments
                                         on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid
                                         && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }



        public static List<SP_TaskInstanceTransactionInternalViewNew_Result> GetCannedReportInternalTask(int Customerid, int userID, string FlagIsApp)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<SP_TaskInstanceTransactionInternalViewNew_Result> transactionsQuery = new List<SP_TaskInstanceTransactionInternalViewNew_Result>();

                transactionsQuery = (from row in entities.SP_TaskInstanceTransactionInternalViewNew(Customerid)
                                     select row).ToList();

                if (FlagIsApp != "" && FlagIsApp != "MGMT")
                {
                    List<int> RoleIDs = new List<int>();
                    RoleIDs.Add(3);
                    RoleIDs.Add(4);

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && RoleIDs.Contains((int)entry.RoleID)
                       && (entry.IsUpcomingNotDeleted != false)).ToList();

                    //transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == RoleID
                    //    && (entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    List<int> RoleIDs = new List<int>();
                    RoleIDs.Add(3);

                    transactionsQuery = transactionsQuery.Where(entry => RoleIDs.Contains((int)entry.RoleID) && (entry.IsUpcomingNotDeleted != false)).ToList();

                    //transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == RoleID && (entry.IsUpcomingNotDeleted != false)).ToList();

                    transactionsQuery = (from row in transactionsQuery
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid
                                         && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }


                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<SP_Kendo_GetTaskComplianceDocument_Result> GetFilteredTaskDocuments(int customerID, int userID, string Role, int statustask, string MonthId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_Kendo_GetTaskComplianceDocument(customerID, MonthId, Convert.ToString(statustask))
                                    select row).ToList();

                if (RoleID != 8)
                {
                    var GetApprover = (from row in entities.TaskAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList(); 

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    {
                        if (!Role.Equals("CADMN"))
                        {
                            documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                        }
                    }
                    else
                    {
                        if (!Role.Equals("CADMN"))
                        {
                            documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                        }
                    } 
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long)row.CustomerBranchID equals row1.BranchID
                                    where row.CustomerID == customerID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                return documentData.GroupBy(entry => entry.TaskScheduleOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

    }
}