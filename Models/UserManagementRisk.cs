﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AppWebApplication.DataRisk;
namespace AppWebApplication.Models
{
    public class UserManagementRisk
    {
        public static bool Create(mst_User user, List<UserParameterValue_Risk> parameters, string SenderEmailAddress, string message, bool WhetherSendMail)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = GetByID(Convert.ToInt32(user.CustomerID)).Status;

                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        //parameters.ForEach(entry => user.UserParameterValue_Risk.Add(entry));
                        entities.mst_User.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        if (WhetherSendMail)
                        {
                            //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                            //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
                        }
                        return true;

                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDMstUser(Addid);
                        }
                        return false;
                    }
                }
            }
        }
        #region Amol 5 AUG 2021 Vendor Audit
        public static void WrongAttemptCountUpdate(long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from Row in entities.mst_User
                                         where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                         select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt = 0;

                    entities.SaveChanges();
                }
            }
        }
    
        public static void ContactExists(mst_User user, out bool ContactExist)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var emailQuery = (from row in entities.mst_User
                                  where row.IsDeleted == false
                                       && row.ContactNumber == user.ContactNumber
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                ContactExist = emailQuery.Select(entry => true).FirstOrDefault();

            }
        }

        public static void Exists(mst_User user, out bool emailExists)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var emailQuery = (from row in entities.mst_User
                                  where row.IsDeleted == false
                                       && row.Email.Equals(user.Email)
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                emailExists = emailQuery.Select(entry => true).FirstOrDefault();

            }
        }
      
        public static bool Update(mst_User user, List<UserParameterValue_Risk> parameters)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == user.ID
                                         select row).FirstOrDefault();

                userToUpdate.UserName = user.UserName;
                userToUpdate.IsHead = user.IsHead;
                userToUpdate.FirstName = user.FirstName;
                userToUpdate.LastName = user.LastName;
                userToUpdate.Designation = user.Designation;
                userToUpdate.Email = user.Email;
                userToUpdate.ContactNumber = user.ContactNumber;
                userToUpdate.Address = user.Address;
                //userToUpdate.CustomerID = user.CustomerID;
                userToUpdate.CustomerBranchID = user.CustomerBranchID;
                //userToUpdate.RoleID = user.RoleID;
                userToUpdate.DepartmentID = user.DepartmentID;
                userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;
                userToUpdate.AuditorID = user.AuditorID;
                userToUpdate.VendorAuditRoleID = user.VendorAuditRoleID;
                userToUpdate.AuditEndPeriod = user.AuditEndPeriod;
                userToUpdate.AuditStartPeriod = user.AuditStartPeriod;
                userToUpdate.Startdate = user.Startdate;
                userToUpdate.Enddate = user.Enddate;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.IMEIChecked = user.IMEIChecked;
                userToUpdate.IMEINumber = user.IMEINumber;
                userToUpdate.DesktopRestrict = user.DesktopRestrict;
                userToUpdate.MobileAccess = user.MobileAccess;
                userToUpdate.HRRoleID = user.HRRoleID;
                userToUpdate.SecretarialRoleID = user.SecretarialRoleID;

                userToUpdate.SSOAccess = user.SSOAccess;

                List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                var existingParameters = (from parameterRow in entities.UserParameterValue_Risk
                                          where parameterRow.UserId == user.ID
                                          select parameterRow).ToList();

                existingParameters.ForEach(entry =>
                {
                    if (parameterIDs.Contains(entry.ID))
                    {
                        UserParameterValue_Risk parameter = parameters.Find(param1 => param1.ID == entry.ID);
                        entry.Value = parameter.Value;
                    }
                    else
                    {
                        entities.UserParameterValue_Risk.Remove(entry);
                    }
                });

                // parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValue_Risk.Add(entry));

                entities.SaveChanges();
                return true;
            }
        }
        //public static bool Create(mst_User user, List<UserParameterValue_Risk> parameters, string SenderEmailAddress, string message)
        //{
        //    int Addid = 0;
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                var customerStatus = GetByID(Convert.ToInt32(user.CustomerID)).Status;

        //                user.IsDeleted = false;
        //                user.CreatedOn = DateTime.Now;
        //                if (customerStatus != null && customerStatus == 1)
        //                {
        //                    user.IsActive = true;
        //                }
        //                else
        //                {
        //                    if (user.RoleID == 2)
        //                        user.IsActive = true;
        //                    else
        //                        user.IsActive = false;
        //                }
        //                user.Password = user.Password;
        //                user.ChangPasswordDate = DateTime.Now;
        //                user.ChangePasswordFlag = true;
        //                parameters.ForEach(entry => user.UserParameterValue_Risk.Add(entry));
        //                entities.mst_User.Add(user);
        //                entities.SaveChanges();
        //                Addid = Convert.ToInt32(user.ID);
        //                dbtransaction.Commit();

        //                return true;

        //            }
        //            catch (Exception)
        //            {
        //                dbtransaction.Rollback();
        //                Addid--;
        //                if (Addid > 0)
        //                {
        //                    entities.SP_ResetIDMstUser(Addid);
        //                }
        //                return false;
        //            }
        //        }
        //    }
        //}
        public static bool Create(mst_User user, List<UserParameterValue_Risk> parameters, string SenderEmailAddress, string message)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = GetByID(Convert.ToInt32(user.CustomerID)).Status;

                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;                    
                        entities.mst_User.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();                       
                        return true;

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDMstUser(Addid);
                        }
                        return false;
                    }
                }
            }
        }
      

        #endregion
        public static mst_Customer GetByID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_Customer
                                where row.ID == customerID
                                select row).SingleOrDefault();

                return customer;
            }
        }

        public static bool Exists(mst_Customer customer)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Customer
                             where row.IsDeleted == false
                                  && row.Name.Equals(customer.Name)
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        
     
        public static void deleteMstUser(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    var tet = entities.mst_User.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.mst_User.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDMstUser(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }


        public static mst_User GetByID_OnlyEditOption(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var user = (from row in entities.mst_User
                            where row.ID == userID// && row.IsDeleted == false && row.IsActive == true
                            select row).SingleOrDefault();

                return user;
            }
        }
        public static bool ChangePassword(mst_User user, string SenderEmailAddress, string message)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.EnType = user.EnType;
                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;
                        userToUpdate.ChangePasswordFlag = true;
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

       
    }
}