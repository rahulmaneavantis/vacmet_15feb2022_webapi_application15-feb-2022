﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
using System.Reflection;
using System.Configuration;

namespace AppWebApplication.Models
{    
    public class CustomerBranchManagement
    {

        public static string GetBranchNameById(int branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.CustomerBranches
                            where row.ID == branchid
                            select row.Name).FirstOrDefault();

                return data;
            }
        }
        public static string GetActNameById(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Acts
                            where row.ID == actid
                            select row.Name).FirstOrDefault();

                return data;
            }
        }

        public static string GetComplianceById(int complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Compliances
                            where row.ID == complianceid
                            select row.ShortDescription).FirstOrDefault();

                return data;
            }
        }

        public static string GetInternalComplianceById(int complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.InternalCompliances
                            where row.ID == complianceid
                            select row.IShortDescription).FirstOrDefault();

                return data;
            }
        }


        public static List<int> GetAssignedLocationListVendorAuditRahul(int custID = -1)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == custID
                             select row);

                if (query != null)
                    LocationList = query.Select(a => a.ID).ToList();

                return LocationList;
            }
        }
        public static List<int> GetAssignedEntitiesLocationListLitigationRahulbyuserNew(int custID, int UserID, string FlagIsApp)
        {
            List<int> LocationList = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var BranchList = (from row in entities.LitigationEntitiesAssignments
                                  where row.UserID == UserID
                                  select (int)row.BranchID).ToList();


                List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                 join row1 in entities.tbl_LegalCaseInstance
                                                 on row.CaseInstanceID equals row1.ID
                                                 where row1.CustomerID == (int)custID
                                                 && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || BranchList.Contains(row1.CustomerBranchID))
                                                 select (int)row1.CustomerBranchID).ToList();

                List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                   join row1 in entities.tbl_LegalNoticeInstance
                                                   on row.NoticeInstanceID equals row1.ID
                                                   where row1.CustomerID == (int)custID
                                                   && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || BranchList.Contains(row1.CustomerBranchID))
                                                   select (int)row1.CustomerBranchID).ToList();

                List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                LocationList = LegalCasbranchlist.Select(a => a).ToList();

                return LocationList;
            }
        }

        public static List<int> GetAssignedEntitiesLocationListLitigationRahulbyuser(int custID, int UserID, string FlagIsApp)
        {
            List<int> LocationList = new List<int>();
            //string IsEntityassignmentCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsEntityLocationAssignment"]);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //if (IsEntityassignmentCustomer == Convert.ToString(custID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(custID));
                if (isexist != null)
                {
                   
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == UserID
                                          select (int)row.BranchID).ToList();

                    if (FlagIsApp == "CADMN" || FlagIsApp == "MGMT")
                    {
                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)custID
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)custID
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        LocationList = LegalCasbranchlist.Select(a => a).ToList();
                    }
                    else
                    {
                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)custID
                                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || BranchList.Contains(row1.CustomerBranchID))
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)custID
                                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || BranchList.Contains(row1.CustomerBranchID))
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        LocationList = LegalCasbranchlist.Select(a => a).ToList();
                    }
                  
                }
                else
                {
                    if (FlagIsApp == "CADMN" || FlagIsApp == "MGMT")
                    {
                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)custID
                                                         select row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)custID
                                                           select row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

                        if (LegalCasbranchlist.Count > 0)
                        {
                            var query = (from row in entities.CustomerBranches
                                         where row.IsDeleted == false
                                         && row.CustomerID == custID
                                         && LegalCasbranchlist.Contains(row.ID)
                                         select row);

                            if (query != null)
                                LocationList = query.Select(a => a.ID).ToList();
                        }
                    }
                    else
                    {
                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)custID
                                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                         select row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)custID
                                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                           select row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

                        if (LegalCasbranchlist.Count > 0)
                        {
                            var query = (from row in entities.CustomerBranches
                                         where row.IsDeleted == false
                                         && row.CustomerID == custID
                                         && LegalCasbranchlist.Contains(row.ID)
                                         select row);

                            if (query != null)
                                LocationList = query.Select(a => a.ID).ToList();
                        }
                    }
                }



                return LocationList;
            }
        }
        public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "", int statusflag = -1)
        {
            List<int> LocationListnew = new List<int>();

            //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LocationList = (from row in entities.LitigationEntitiesAssignments
                                    where row.UserID == UserID
                                    select (int)row.BranchID).ToList();


                List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                 join row1 in entities.tbl_LegalCaseInstance
                                                 on row.CaseInstanceID equals row1.ID
                                                 where row1.CustomerID == (int)custID
                                                 && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
                                                 select (int)row1.ID).ToList();


                List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                   join row1 in entities.tbl_LegalNoticeInstance
                                                   on row.NoticeInstanceID equals row1.ID
                                                   where row1.CustomerID == (int)custID
                                                   && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
                                                   select (int)row1.ID).ToList();


                //List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                //LocationListnew = LegalCasbranchlist.Select(a => a).ToList();
                if (statusflag == 1)
                {
                    LocationListnew = LegalNoticebranchlist.Select(a => a).ToList();
                }
                else if (statusflag == 2)
                {
                    LocationListnew = LegalCasebranchlist.Select(a => a).ToList();
                }
                else
                {
                    List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                    LocationListnew = LegalCasbranchlist.Select(a => a).ToList();
                }

            }


            return LocationListnew;
        }

        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationListnew = new List<int>();

        //    string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var LocationList = (from row in entities.LitigationEntitiesAssignments
        //                            where row.UserID == UserID
        //                            select (int)row.BranchID).ToList();


        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                         join row1 in entities.tbl_LegalCaseInstance
        //                                         on row.CaseInstanceID equals row1.ID
        //                                         where row1.CustomerID == (int)custID
        //                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
        //                                         select (int)row1.ID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                           on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID || LocationList.Contains(row1.CustomerBranchID))
        //                                           select (int)row1.ID).ToList();

        //        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
        //        LocationListnew = LegalCasbranchlist.Select(a => a).ToList();
                
        //    }


        //    return LocationListnew;
        //}

        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationList = new List<int>();

        //    string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();

        //    if (IsEntityassignmentCustomer == Convert.ToString(custID))
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                             join row1 in entities.tbl_LegalCaseInstance
        //                                             on row.CaseInstanceID equals row1.ID
        //                                             where row1.CustomerID == (int)custID
        //                                             && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                             select row1.CustomerBranchID).ToList();

        //            List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                               join row1 in entities.tbl_LegalNoticeInstance
        //                                            on row.NoticeInstanceID equals row1.ID
        //                                               where row1.CustomerID == (int)custID
        //                                               && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                               select row1.CustomerBranchID).ToList();

        //            List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //            if (LegalCasbranchlist.Count > 0)
        //            {
        //                var query = (from row in entities.CustomerBranches
        //                             where row.IsDeleted == false
        //                             && row.CustomerID == custID
        //                             && LegalCasbranchlist.Contains(row.ID)
        //                             select row);

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).ToList();
        //            }
        //            if (role == "MGMT" && IsEntityassignmentCustomer == Convert.ToString(custID))
        //            {
        //                var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                                where row.UserID == UserID
        //                                                select row).ToList();
        //                if (getAssignedEntitiyByUSer.Count > 0)
        //                {

        //                    var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
        //                                 select row.ID).ToList();

        //                    if (query != null)
        //                        LocationList = query.Select(a => a).ToList();
        //                }
        //                else
        //                {
        //                    var query = (from row in entities.CustomerBranches
        //                                 where row.IsDeleted == false
        //                                 && row.CustomerID == custID
        //                                 && LegalCasbranchlist.Contains(row.ID)
        //                                 select row);

        //                    if (query != null)
        //                        LocationList = query.Select(a => a.ID).ToList();
        //                }
        //            }
        //            else
        //            {
        //                var query = (from row in entities.CustomerBranches
        //                             where row.IsDeleted == false
        //                             && row.CustomerID == custID
        //                             && LegalCasbranchlist.Contains(row.ID)
        //                             select row);

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).ToList();
        //            }                  
        //        }
        //    }
        //    return LocationList;
        //}

        public static List<int> GetAssignedCaseNoticeLitigation(int custID = -1, int UserID = -1, string role = "")
        {
            List<int> caseList = new List<int>();
           // string IsEntityassignmentCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsEntityLocationAssignment"]);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //if (IsEntityassignmentCustomer == Convert.ToString(custID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(custID));
                if (isexist != null)
                {
                    //if (role == "MGMT")
                    //{
                        var query = (from row in entities.SP_GetLitigationAssignedLocationByUser(UserID, custID)
                                     select row.caseid).ToList();

                        if (query != null)
                            caseList = query.Select(a => Convert.ToInt32(a)).ToList();
                    //}
                }
                return caseList;
            }
        }
       
        //public static List<int> GetAssignedCaseNoticeLitigation(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> caseList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        if (role == "MGMT")
        //        {
        //            //var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //            //                                where row.UserID == UserID
        //            //                                select row).Distinct().ToList();
        //            //if (getAssignedEntitiyByUSer.Count > 0)
        //            //{

        //                var query = (from row in entities.SP_GetLitigationAssignedLocationByUser(UserID, custID)
        //                             select row.caseid).ToList();

        //                if (query != null)
        //                    caseList = query.Select(a => Convert.ToInt32(a)).ToList();
        //            //}
        //        }

        //        return caseList;
        //    }
        //}

        //public static List<int> GetAssignedEntityLitigation(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        if (role == "MGMT")
        //        {
        //            var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                            where row.UserID == UserID
        //                                            select row).ToList();
        //            if (getAssignedEntitiyByUSer.Count > 0)
        //            {

        //                var query = (from row in entities.SP_GetLitigationAssignedLocationByUser(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //        }
        //        return LocationList;
        //    }
        //}

        //public static List<int> GetAssignedEntityLitigation(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                         join row1 in entities.tbl_LegalCaseInstance
        //                                         on row.CaseInstanceID equals row1.ID
        //                                         where row1.CustomerID == (int)custID
        //                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                         select row1.CustomerBranchID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                        on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                           select row1.CustomerBranchID).ToList();

        //        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //        if (LegalCasbranchlist.Count > 0)
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }


        //        if (role == "MGMT")
        //        {
        //            var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                            where row.UserID == UserID
        //                                            select row).ToList();
        //            if (getAssignedEntitiyByUSer.Count > 0)
        //            {

        //                var query = (from row in entities.SP_GetLitigationAssignedLocationByUser(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.SP_GetLitigationAssignedLocationByUser(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //        }
        //        else
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }



        //        return LocationList;
        //    }
        //}

        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                         join row1 in entities.tbl_LegalCaseInstance
        //                                         on row.CaseInstanceID equals row1.ID
        //                                         where row1.CustomerID == (int)custID
        //                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                         select row1.CustomerBranchID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                        on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                           select row1.CustomerBranchID).ToList();

        //        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //        if (LegalCasbranchlist.Count > 0)
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }


        //        if (role == "MGMT")
        //        {
        //            var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                            where row.UserID == UserID
        //                                            select row).ToList();
        //            if (getAssignedEntitiyByUSer.Count > 0)
        //            {

        //                var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.CustomerBranches
        //                             where row.IsDeleted == false
        //                             && row.CustomerID == custID
        //                             && LegalCasbranchlist.Contains(row.ID)
        //                             select row);

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).ToList();
        //            }
        //        }
        //        else
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }



        //        return LocationList;
        //    }
        //}

        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1, string role = "")
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                         join row1 in entities.tbl_LegalCaseInstance
        //                                         on row.CaseInstanceID equals row1.ID
        //                                         where row1.CustomerID == (int)custID
        //                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                         select row1.CustomerBranchID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                        on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                           select row1.CustomerBranchID).ToList();

        //        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //        if (LegalCasbranchlist.Count > 0)
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }


        //        if (role == "MGMT")
        //        {
        //            var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                            where row.UserID == UserID
        //                                            select row).ToList();
        //            if (getAssignedEntitiyByUSer.Count > 0)
        //            {

        //                var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //        }
        //        else
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }

        //        return LocationList;
        //    }
        //}

        public static List<NameValueHierarchy> GetAllHierarchy(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);



                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static List<int> GetAssignedEntitiesLocationListLitigationRahulbyuser(int custID, int UserID)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
                                                where row.UserID == UserID
                                                select row).ToList();
                if (getAssignedEntitiyByUSer.Count > 0)
                {
                    var query = (from row in entities.CustomerBranches
                                 join row1 in entities.LitigationEntitiesAssignments
                                 on row.ID equals row1.BranchID
                                 where row.IsDeleted == false
                                 && row.CustomerID == custID
                                && row1.UserID == UserID
                                 select row);
                    LocationList = query.Select(a => a.ID).ToList();
                }



                return LocationList;
            }
        }
        //public static List<int> GetAssignedEntitiesLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1)
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.CustomerBranches
        //                     join row1 in entities.LitigationEntitiesAssignments
        //                     on row.ID equals row1.BranchID
        //                     where row.IsDeleted == false
        //                     && row.CustomerID == custID
        //                    && row1.UserID == UserID
        //                     select row);

        //        if (query != null)
        //            LocationList = query.Select(a => a.ID).ToList();


        //        return LocationList;
        //    }
        //}

        public static List<int> GetAssignedLocationListLitigationRahulbyuserNew(int custID = -1, int UserID = -1,int statusID=-1)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statusID == 1)
                {
                    List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                       join row1 in entities.tbl_LegalNoticeInstance
                                                    on row.NoticeInstanceID equals row1.ID
                                                       where row1.CustomerID == (int)custID
                                                       && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                       select row1.CustomerBranchID).ToList();
                    
                    if (LegalNoticebranchlist.Count > 0)
                    {
                        var query = (from row in entities.CustomerBranches
                                     where row.IsDeleted == false
                                     && row.CustomerID == custID
                                     && LegalNoticebranchlist.Contains(row.ID)
                                     select row);

                        if (query != null)
                            LocationList = query.Select(a => a.ID).ToList();
                    }

                }
                if (statusID == 2)
                {

                    List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                     join row1 in entities.tbl_LegalCaseInstance
                                                     on row.CaseInstanceID equals row1.ID
                                                     where row1.CustomerID == (int)custID
                                                     && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                     select row1.CustomerBranchID).ToList();
                    
                    if (LegalCasebranchlist.Count > 0)
                    {
                        var query = (from row in entities.CustomerBranches
                                     where row.IsDeleted == false
                                     && row.CustomerID == custID
                                     && LegalCasebranchlist.Contains(row.ID)
                                     select row);

                        if (query != null)
                            LocationList = query.Select(a => a.ID).ToList();
                    }

                }
                return LocationList;
            }
        }

        public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                 join row1 in entities.tbl_LegalCaseInstance
                                                 on row.CaseInstanceID equals row1.ID
                                                 where row1.CustomerID == (int)custID
                                                 && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                 select row1.CustomerBranchID).ToList();

                List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                   join row1 in entities.tbl_LegalNoticeInstance
                                                on row.NoticeInstanceID equals row1.ID
                                                   where row1.CustomerID == (int)custID
                                                   && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
                                                   select row1.CustomerBranchID).ToList();

                List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

                if (LegalCasbranchlist.Count > 0)
                {
                    var query = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false
                                 && row.CustomerID == custID
                                 && LegalCasbranchlist.Contains(row.ID)
                                 select row);

                    if (query != null)
                        LocationList = query.Select(a => a.ID).ToList();
                }



                var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
                                                where row.UserID == UserID
                                                select row).ToList();
                if (getAssignedEntitiyByUSer.Count > 0)
                {

                    var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
                                 select row.ID).ToList();

                    if (query != null)
                        LocationList = query.Select(a => a).ToList();
                }
                else
                {
                    var query = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false
                                 && row.CustomerID == custID
                                 && LegalCasbranchlist.Contains(row.ID)
                                 select row);

                    if (query != null)
                        LocationList = query.Select(a => a.ID).ToList();
                }

                return LocationList;
            }
        }

        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1)
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                         join row1 in entities.tbl_LegalCaseInstance
        //                                         on row.CaseInstanceID equals row1.ID
        //                                         where row1.CustomerID == (int)custID
        //                                         && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                         select row1.CustomerBranchID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                        on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                           select row1.CustomerBranchID).ToList();

        //        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //        if (LegalCasbranchlist.Count > 0)
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }

        //        if (custID == 5)
        //        {

        //            var getAssignedEntitiyByUSer = (from row in entities.LitigationEntitiesAssignments
        //                                            where row.UserID == UserID
        //                                            select row).ToList();
        //            if (getAssignedEntitiyByUSer.Count > 0)
        //            {

        //                var query = (from row in entities.SP_GetLitigationAssignedLocation(UserID, custID)
        //                             select row.ID).ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => a).ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.CustomerBranches
        //                             where row.IsDeleted == false
        //                             && row.CustomerID == custID
        //                             && LegalCasbranchlist.Contains(row.ID)
        //                             select row);

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).ToList();
        //            }
        //        }
        //        return LocationList;
        //    }
        //}
        //public static List<int> GetAssignedLocationListLitigationRahulbyuser(int custID = -1, int UserID = -1)
        //{
        //    List<int> LocationList = new List<int>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
        //                                        join row1 in entities.tbl_LegalCaseInstance
        //                                        on row.CaseInstanceID equals row1.ID
        //                                        where row1.CustomerID == (int)custID
        //                                        && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                        select row1.CustomerBranchID).ToList();

        //        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
        //                                           join row1 in entities.tbl_LegalNoticeInstance
        //                                        on row.NoticeInstanceID equals row1.ID
        //                                           where row1.CustomerID == (int)custID
        //                                           && (row.UserID == UserID || row1.OwnerID == UserID || row1.CreatedBy == UserID)
        //                                           select row1.CustomerBranchID).ToList();

        //        List<int>  LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();

        //        if (LegalCasbranchlist.Count > 0)
        //        {
        //            var query = (from row in entities.CustomerBranches
        //                         where row.IsDeleted == false
        //                         && row.CustomerID == custID
        //                         && LegalCasbranchlist.Contains(row.ID)
        //                         select row);

        //            if (query != null)
        //                LocationList = query.Select(a => a.ID).ToList();
        //        }

        //        return LocationList;
        //    }
        //}

        public static List<int> GetAssignedLocationListLitigationRahul(int custID = -1)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == custID
                             select row);

                if (query != null)
                    LocationList = query.Select(a => a.ID).ToList();

                return LocationList;
            }
        }
        public static List<int> GetAssignedLocationLitigationList(int custID)
        {
            List<int> LocationList = new List<int>();                      
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == custID
                             select row.ID).ToList();
                        if (query != null)
                            LocationList = query.Distinct().ToList();
                  
                return LocationList;
            }
        }
        public static List<int> GetAssignedCalenderRoleId(int Userid, DateTime dt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.Sp_GetAllAssignedCalenderRoles(Userid, dt)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                return roles;
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchySatutoryRahul(long customerID, List<int> LocationList)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesRahul(item, true, entities, LocationList, customerID);
                }

            }

            return hierarchy;
        }
        public static void LoadSubEntitiesRahul(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, List<int> LocationList, long customerID)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
                var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                //foreach (var item in subEntities)
                //{
                //    nvp.Children.Add(item);
                //    LoadSubEntitiesRahul(item, false, entities, LocationList, customerID);                 
                //}

                List<int?> AllAssignlocation = new List<int?>();
                foreach (var item in LocationList)
                {
                    var getData = entities.SP_GetParentBranchesFromChildNode(item, Convert.ToInt32(customerID)).Select(x => x.ID).ToList();
                    foreach (var item1 in getData)
                    {
                        AllAssignlocation.Add(item1);
                    }
                }
                AllAssignlocation = AllAssignlocation.Distinct().ToList();
                foreach (var item in subEntities)
                {
                    if (AllAssignlocation.Contains(item.ID))
                    {
                        nvp.Children.Add(item);
                        LoadSubEntitiesRahul(item, false, entities, LocationList, customerID);
                    }
                }
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
                var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in subEntities)
                {
                    if (FindNodeExists(item, LocationList))
                    {
                        nvp.Children.Add(item);
                        LoadSubEntitiesRahul(item, false, entities, LocationList, customerID);
                    }
                }
            }
        }
        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<int> LocationList = new List<int>();
            var UserDetails = UserManagement.GetByID(UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    Role = "DEPT";
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT")
                    {
                        var query = (from row in entities.EntitiesAssignments
                                     where row.UserID == UserID
                                     select (int)row.BranchID).ToList();
                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                    else if (Role == "AUD")
                    {
                        var query = (from row in entities.EntitiesAssignments
                                     where row.UserID == UserID
                                     select (int)row.BranchID).ToList();
                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal == "S"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {
                        var query = (from CA in entities.ComplianceAssignments
                                     join CI in entities.ComplianceInstances
                                     on CA.ComplianceInstanceID equals CI.ID
                                     where CA.UserID == UserID
                                     //  && (CI.CustomerBranchID ==7 || CI.CustomerBranchID == 8)
                                     select CI.CustomerBranchID).ToList();

                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                }
                else if (statutoryInternal == "I")
                {
                    if (Role == "MGMT")
                    {
                        var query = (from row in entities.EntitiesAssignmentInternals
                                     where row.UserID == UserID
                                     select (int)row.BranchID).ToList();
                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                    else if (Role == "AUD")
                    {
                        var query = (from row in entities.EntitiesAssignmentInternals
                                     where row.UserID == UserID
                                     select (int)row.BranchID).ToList();
                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                    else if (Role == "DEPT")
                    {
                        var query = (from row in entities.EntitiesAssignment_IsDept
                                     where row.UserID == UserID && row.IsStatutoryInternal == "I"
                                     select row).Distinct().ToList();
                        if (query != null)
                            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                    }
                    else
                    {

                        var query = (from ICA in entities.InternalComplianceAssignments
                                     join ICI in entities.InternalComplianceInstances
                                     on ICA.InternalComplianceInstanceID equals ICI.ID
                                     where ICA.UserID == UserID
                                     select ICI.CustomerBranchID).ToList();

                        if (query != null)
                            LocationList = query.Distinct().ToList();
                    }
                }
                return LocationList;
            }
        }
      
        public static List<int> GetAssignedLocationListRahul(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<int> LocationList = new List<int>();
            var UserDetails = UserManagement.GetByID(UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    Role = "DEPT";
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_Kendo_GetALLLocation(UserID, custID, Role, statutoryInternal)
                             select row).Distinct().ToList();

                if (query != null)
                    LocationList = query.Select(a => (int)a.ID).Distinct().ToList();

                //if (statutoryInternal == "S")
                //{
                //    if (Role == "MGMT" || Role == "AUD" || Role == "HRMGR")
                //    {
                //        var query = (from row in entities.EntitiesAssignments
                //                     where row.UserID == UserID
                //                     select row).Distinct().ToList();

                //        if (query != null)
                //            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();

                //    }
                //    else if (Role == "DEPT")
                //    {
                //        var query = (from row in entities.EntitiesAssignment_IsDept
                //                     where row.UserID == UserID && row.IsStatutoryInternal == "S"
                //                     select row).Distinct().ToList();
                //        if (query != null)
                //            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                //    }
                //    else
                //    {
                //        var query = (from row in entities.ComplianceAssignedInstancesViews
                //                     join row1 in entities.CustomerBranches
                //                     on row.CustomerBranchID equals row1.ID
                //                     where row.UserID == UserID
                //                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                //        if (query != null)
                //            LocationList = query.Select(a => a.ID).Distinct().ToList();
                //    }
                //}
                //else if (statutoryInternal == "I")
                //{
                //    if (Role == "MGMT" || Role == "AUD" || Role == "HRMGR")
                //    {
                //        var query = (from row in entities.EntitiesAssignmentInternals
                //                     where row.UserID == UserID
                //                     select row).Distinct().ToList();

                //        if (query != null)
                //            LocationList = query.Select(a => (int)a.BranchID).ToList();

                //    }
                //    else if (Role == "DEPT")
                //    {
                //        var query = (from row in entities.EntitiesAssignment_IsDept
                //                     where row.UserID == UserID && row.IsStatutoryInternal == "I"
                //                     select row).Distinct().ToList();
                //        if (query != null)
                //            LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
                //    }
                //    else
                //    {
                //        var query = (from row in entities.InternalComplianceAssignedInstancesViews
                //                     join row1 in entities.CustomerBranches
                //                     on row.CustomerBranchID equals row1.ID
                //                     where row.UserID == UserID
                //                     select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                //        if (query != null)
                //            LocationList = query.Select(a => a.ID).ToList();
                //    }
                //}
                return LocationList;
            }
        }

        //public static List<int> GetAssignedLocationListRahul(int UserID, int custID, String Role, string statutoryInternal)
        //{
        //    List<int> LocationList = new List<int>();
        //    var UserDetails = UserManagement.GetByID(UserID);
        //    if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
        //    {
        //        if ((bool)UserDetails.IsHead)
        //        {
        //            Role = "DEPT";
        //        }
        //    }
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        if (statutoryInternal == "S")
        //        {
        //            if (Role == "MGMT" || Role == "AUD" || Role == "HRMGR")
        //            {
        //                var query = (from row in entities.EntitiesAssignments
        //                             where row.UserID == UserID
        //                             select row).Distinct().ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();

        //            }
        //            else if (Role == "DEPT")
        //            {
        //                var query = (from row in entities.EntitiesAssignment_IsDept
        //                             where row.UserID == UserID && row.IsStatutoryInternal == "S"
        //                             select row).Distinct().ToList();
        //                if (query != null)
        //                    LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.ComplianceAssignedInstancesViews
        //                             join row1 in entities.CustomerBranches
        //                             on row.CustomerBranchID equals row1.ID
        //                             where row.UserID == UserID
        //                             select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).Distinct().ToList();
        //            }
        //        }
        //        else if (statutoryInternal == "I")
        //        {
        //            if (Role == "MGMT" || Role == "AUD" || Role == "HRMGR")
        //            {
        //                var query = (from row in entities.EntitiesAssignmentInternals
        //                             where row.UserID == UserID
        //                             select row).Distinct().ToList();

        //                if (query != null)
        //                    LocationList = query.Select(a => (int)a.BranchID).ToList();

        //            }
        //            else if (Role == "DEPT")
        //            {
        //                var query = (from row in entities.EntitiesAssignment_IsDept
        //                             where row.UserID == UserID && row.IsStatutoryInternal == "I"
        //                             select row).Distinct().ToList();
        //                if (query != null)
        //                    LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();
        //            }
        //            else
        //            {
        //                var query = (from row in entities.InternalComplianceAssignedInstancesViews
        //                             join row1 in entities.CustomerBranches
        //                             on row.CustomerBranchID equals row1.ID
        //                             where row.UserID == UserID
        //                             select row1).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

        //                if (query != null)
        //                    LocationList = query.Select(a => a.ID).ToList();
        //            }
        //        }
        //        return LocationList;
        //    }
        //}

        public static List<NameValueHierarchy> GetAllHierarchySatutory(long customerID, List<int> LocationList)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, LocationList);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, List<int> LocationList)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                if (FindNodeExists(item, LocationList))
                {
                    nvp.Children.Add(item);
                    LoadSubEntities(item, false, entities, LocationList);
                }
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutory(int customerID, List<int> LocationList)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();
              
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, LocationList);
                }
            }

            return hierarchy;
        }


        public static List<NameValueHierarchy> GetAllHierarchySatutory(long customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }      
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities);
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutoryWebApp(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();
                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }       
        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutory(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             join Cust in entities.Customers
                             on row.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static List<CustomerEntities> GetAssignedManagementLocationList(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<CustomerEntities> LocationList = new List<CustomerEntities>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT")
                    {
                        var query = (from row in entities.EntitiesAssignments
                                     join row1 in entities.CustomerBranches
                                     on row.BranchID equals row1.ID
                                     where row1.IsDeleted == false && row1.Status == 1
                                     && row.UserID == UserID
                                     select new CustomerEntities
                                     {
                                         ID = row1.ID,
                                         Name = row1.Name,
                                     }).Distinct().ToList();


                        if (query != null)
                            LocationList = query.ToList();

                    }
                    else
                    {
                        List<SP_ManagementDashboardApproverComplianceCount_Result> ApproverComplianceCount = new List<SP_ManagementDashboardApproverComplianceCount_Result>();
                        var ApproverCompliancedetails = (entities.SP_ManagementDashboardApproverComplianceCount(Convert.ToInt32(UserID), Convert.ToInt32(custID))).ToList();

                        var query = (from row in ApproverCompliancedetails
                                     join Cbranch in entities.CustomerBranches
                                     on row.CustomerBranchID equals Cbranch.ID
                                     where Cbranch.CustomerID == custID
                                     select new CustomerEntities
                                     {
                                         ID = Cbranch.ID,
                                         Name = Cbranch.Name,
                                     }).Distinct().ToList();

                        //select Cbranch).Distinct().ToList(); 
                        if (query != null)
                        {
                            query = query.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                            LocationList = query.ToList();
                        }
                    }
                }
                else if (statutoryInternal == "I")
                {
                    if (Role == "MGMT")
                    {

                        var query = (from row in entities.EntitiesAssignmentInternals
                                     join row1 in entities.CustomerBranches
                                     on row.BranchID equals row1.ID
                                     where row1.IsDeleted == false && row1.Status == 1
                                     && row.UserID == UserID
                                     select new CustomerEntities
                                     {
                                         ID = row1.ID,
                                         Name = row1.Name,
                                     }).Distinct().ToList();
                        
                        if (query != null)                            
                            LocationList = query.ToList();

                    }                   
                }


                return LocationList;
            }
        }
       
        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                         
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.Sp_GetAllAssignedRoles(Userid)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();                              
                return roles;
            }
        }
        public static bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

    
        
       
    }
}