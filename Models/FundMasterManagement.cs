﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class FundMasterManagement
    {
        public static string getinvestorName(int InvestorID)
        {
            string investorName = string.Empty;
            if (InvestorID != null && InvestorID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    investorName = (from row in entities.CompFund_tbl_InvestorMaster
                                    where row.IsDeleted == false
                                    select row.InvestorName).FirstOrDefault();
                }

            }
            return investorName;
        }
        public static void UpdateChecklistTransaction(CompFund_tbl_ChecklistAuditTransaction obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_ChecklistAuditTransaction Data = (from row in entities.CompFund_tbl_ChecklistAuditTransaction
                                                               where row.checklistAuditInstanceID == obj.checklistAuditInstanceID
                                                         && row.ChecklistAuditScheduleOnID == obj.ChecklistAuditScheduleOnID
                                                         && row.StatusId == obj.StatusId && row.CheckListId == obj.CheckListId
                                                               select row).FirstOrDefault();
                if (Data == null)
                {
                    entities.CompFund_tbl_ChecklistAuditTransaction.Add(obj);
                    entities.SaveChanges();
                }
            }
        }
        public static List<int> GetAssignedLocationList(int UserID, int custID)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CompFund_SP_GetLLLocation(UserID, custID)
                             select row).Distinct().ToList();

                if (query != null)
                    LocationList = query.Select(a => (int)a.Value).Distinct().ToList();

                return LocationList;
            }
        }
        public static List<CompFund_SP_GetAuditChecklist_Result> GeAuditChecklist(int CID, int UID, string DocType, int RoleID, int AuditchecklistID, int AuditChecklistScheduleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetAuditChecklist(DocType, CID, UID, RoleID, AuditchecklistID, AuditChecklistScheduleID)
                            select row).ToList();

                return Data;
            }
        }
        public static List<CompFund_SP_GetOpenAndClosedAuditCountDetails_Result> GetOpenClosedAudit(int CID, int UID, string DocType, string Role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetOpenAndClosedAuditCountDetails(CID, UID, DocType, Role)
                            select row).ToList();

                return Data;
            }
        }
        public static List<CompFund_SP_DetailsCheckListReport_Result> GetChecklistAudit(int SID, int AID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_DetailsCheckListReport(SID, AID)
                            select row).ToList();

                return Data;
            }
        }
        public static List<CompFund_SP_GetOpenAuditchecklistListDetails_Result> GetScheduledChecklist(int CID, int CBID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetOpenAuditchecklistListDetails(CID, CBID)
                            select row).ToList();

                return Data;
            }
        }
        public static List<CompFund_SP_GetALLChecklist_Result> GetChecklist(int DocTypeID, int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetALLChecklist(DocTypeID, CID)
                            select row).ToList();
                return Data;
            }
        }

        public static long UpdateChecklistInstance(CompFund_tbl_ChecklistAuditInstance obj, bool IsFlagInvest)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IsFlagInvest == true)
                {
                    CompFund_tbl_ChecklistAuditInstance Data = (from row in entities.CompFund_tbl_ChecklistAuditInstance
                                                                where row.CustomerID == obj.CustomerID
                                                                && row.CustomerBranchID == obj.CustomerBranchID
                                                                && row.Year == obj.Year
                                                                && row.Frequency == obj.Frequency
                                                                && row.Period == obj.Period
                                                                && row.PerformerID == obj.PerformerID
                                                                && row.ReviewerID == obj.ReviewerID
                                                                //&& row.DocumentType == obj.DocumentType
                                                                // && row.InvestorID == obj.InvestorID
                                                                select row).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.IsActive = true;
                        Data.UpdatedBy = obj.CreatedBy;
                        Data.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        long ChecklistInstanceID = Data.ID;
                        return ChecklistInstanceID;
                    }
                    else
                    {
                        entities.CompFund_tbl_ChecklistAuditInstance.Add(obj);
                        entities.SaveChanges();
                        long ChecklistInstanceID = obj.ID;
                        return ChecklistInstanceID;
                    }
                }
                else
                {
                    CompFund_tbl_ChecklistAuditInstance Data = (from row in entities.CompFund_tbl_ChecklistAuditInstance
                                                                where row.CustomerID == obj.CustomerID
                                                                && row.CustomerBranchID == obj.CustomerBranchID
                                                                && row.Year == obj.Year
                                                                && row.Frequency == obj.Frequency
                                                                && row.Period == obj.Period
                                                                && row.PerformerID == obj.PerformerID
                                                                && row.ReviewerID == obj.ReviewerID
                                                                && row.DocumentType == obj.DocumentType
                                                                select row).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.IsActive = true;
                        Data.UpdatedBy = obj.CreatedBy;
                        Data.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        long ChecklistInstanceID = Data.ID;
                        return ChecklistInstanceID;
                    }
                    else
                    {
                        entities.CompFund_tbl_ChecklistAuditInstance.Add(obj);
                        entities.SaveChanges();
                        long ChecklistInstanceID = obj.ID;
                        return ChecklistInstanceID;
                    }
                }
            }
        }
        public static void UpdateChecklistAssignment(CompFund_tbl_ChecklistAssignment obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_ChecklistAssignment Data = (from row in entities.CompFund_tbl_ChecklistAssignment
                                                         where row.RoleID == obj.RoleID
                                                         && row.UserID == obj.UserID
                                                         && row.ChechklistInstanceID == obj.ChechklistInstanceID
                                                         select row).FirstOrDefault();
                if (Data == null)
                {

                    entities.CompFund_tbl_ChecklistAssignment.Add(obj);
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateChecklistMapping(CompFund_tbl_ChecklistMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_ChecklistMapping Data = (from row in entities.CompFund_tbl_ChecklistMapping
                                                      where row.CustomerID == obj.CustomerID
                                                      && row.CheckListInstanceId == obj.CheckListInstanceId
                                                      && row.CheckListId == obj.CheckListId
                                                      && row.ScheduleOnId == obj.ScheduleOnId
                                                      select row).FirstOrDefault();
                if (Data != null)
                {
                    Data.IsDeleted = false;
                    Data.UpdatedBy = obj.CreatedBy;
                    Data.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    entities.CompFund_tbl_ChecklistMapping.Add(obj);
                    entities.SaveChanges();
                }
            }
        }

        public static int GetRoleID(int UID, int CID, int AuditChecklistID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RoleID = (from row in entities.CompFund_tbl_ChecklistAssignment
                          where row.UserID == UID
                          && row.ChechklistInstanceID == AuditChecklistID
                          select row.RoleID).FirstOrDefault();
            }
            return RoleID;
        }
    }
}