﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using AppWebApplication.Data;
using AppWebApplication.DataRisk;
using AppWebApplication.VendorAudit.Model;

namespace AppWebApplication.Models
{
    public class UserManagement
    {

        public static Role GetRoleByID(int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var role = (from row in entities.Roles
                            where row.ID == roleID
                            select row).SingleOrDefault();

                return role;
            }
        }

        public static string GetUserNameByEmail(string Email, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserData = (from row in entities.Users
                                where row.Email == Email.Trim()
                                && row.CustomerID == customerID
                                && row.IsDeleted == false
                                && row.IsActive == true
                                select row).FirstOrDefault();
                if (UserData != null)
                {
                    return UserData.FirstName + ' ' + UserData.LastName;
                }
                return "0";
            }
        }

        public static string GetUserNameByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserName = (from row in entities.Users
                                where row.ID == userID
                                select row.FirstName + " " + row.LastName).FirstOrDefault();

                return UserName;
            }
        }

        #region Amol 5 AUG 2021 Vendor Audit

        public static List<UserDetails> GetUserDetails(int CID)
        {
            
            List<UserDetails> userFinalList = new List<UserDetails>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userList = (from row in entities.Users
                                where row.RoleID == 39 || row.RoleID == 40 || row.RoleID == 41 || row.RoleID == 42
                                || row.RoleID == 43
                                select row).OrderBy(x => x.UserName).ToList();
                foreach (var item in userList)
                {
                    UserDetails finalData = new UserDetails();
                    finalData.ID = item.ID;
                    finalData.UserName = item.UserName;
                    finalData.RoleID = item.RoleID;
                    finalData.ContactNumber = item.ContactNumber;
                    finalData.RoleName = UserManagement.GetRoleNameByRoleID(item.RoleID);
                    finalData.Email = item.Email;
                    userFinalList.Add(finalData);
                }
            }
            return userFinalList;
        }
        public static string GetRoleNameByRoleID(int RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getRole = (from row in entities.Roles
                               where row.ID == RoleID
                               select row.Name).FirstOrDefault();
                if (getRole != null)
                {
                    return getRole;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<RoleDTO> GetRoleDetails(int CID)
        {
            List<RoleDTO> userFinalList = new List<RoleDTO>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                userFinalList = (from row in entities.Roles
                                 where ( row.ID == 39 || row.ID == 40 || row.ID == 41)

                              
                                 select new RoleDTO
                                 {
                                     ID = row.ID,
                                     Name = row.Name
                                 }).Distinct().ToList();
            }
            return userFinalList;
        }

        public static User GetByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userID
                            select row).SingleOrDefault();

                return user;
            }
        }
      
        public static List<User> Getalluserlistbyids(List<long> lstUserIdEmails)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<User> userlst = (from row in entities.Users
                                      where row.IsActive == true
                                        && row.IsDeleted == false && lstUserIdEmails.Contains(row.ID)
                                      select row).ToList();

                return userlst;
            }
        }
        public static void WrongAttemptCountUpdate(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User userToUpdate = (from Row in entities.Users
                                     where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                     select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt = 0;

                    entities.SaveChanges();
                }
            }
        }
       

        public static string GetByUsername(int UID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == UID
                            && row.IsDeleted == false
                            select row.FirstName).FirstOrDefault();

                return user;
            }
        }

        public static void ContactExists(User user, out bool ContactExist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from row in entities.Users
                                  where row.IsDeleted == false
                                       && row.ContactNumber == user.ContactNumber
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                ContactExist = emailQuery.Select(entry => true).FirstOrDefault();

            }
        }
      
        public static void Exists(User user, out bool emailExists)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from row in entities.Users
                                  where row.IsDeleted == false
                                       && row.Email.Equals(user.Email)
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                emailExists = emailQuery.Select(entry => true).FirstOrDefault();

            }
        }
    
        public static bool Update(User user, List<UserParameterValue> parameters)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from row in entities.Users
                                     where row.ID == user.ID
                                     select row).FirstOrDefault();

                userToUpdate.UserName = user.UserName;
                userToUpdate.IsHead = user.IsHead;
                userToUpdate.FirstName = user.FirstName;
                userToUpdate.LastName = user.LastName;
                userToUpdate.Designation = user.Designation;
                userToUpdate.Email = user.Email;
                userToUpdate.ContactNumber = user.ContactNumber;
                userToUpdate.Address = user.Address;
                userToUpdate.CustomerBranchID = user.CustomerBranchID;
                userToUpdate.ReportingToID = user.ReportingToID;            
                userToUpdate.DepartmentID = user.DepartmentID;
                userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;
                userToUpdate.AuditorID = user.AuditorID;
                userToUpdate.AuditEndPeriod = user.AuditEndPeriod;
                userToUpdate.AuditStartPeriod = user.AuditStartPeriod;
                userToUpdate.Startdate = user.Startdate;
                userToUpdate.Enddate = user.Enddate;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.IMEIChecked = user.IMEIChecked;
                userToUpdate.IMEINumber = user.IMEINumber;
                userToUpdate.DesktopRestrict = user.DesktopRestrict;
                userToUpdate.MobileAccess = user.MobileAccess;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.VendorAuditRoleID = user.VendorAuditRoleID;
                userToUpdate.HRRoleID = user.HRRoleID;
                userToUpdate.SecretarialRoleID = user.SecretarialRoleID;
                userToUpdate.Cer_OwnerRoleID = user.Cer_OwnerRoleID;
                userToUpdate.Cer_OfficerRoleID = user.Cer_OfficerRoleID;
                userToUpdate.IsCertificateVisible = user.IsCertificateVisible;
                userToUpdate.SSOAccess = user.SSOAccess;

                List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                var existingParameters = (from parameterRow in entities.UserParameterValues
                                          where parameterRow.UserId == user.ID
                                          select parameterRow).ToList();

                existingParameters.ForEach(entry =>
                {
                    if (parameterIDs.Contains(entry.ID))
                    {
                        UserParameterValue parameter = parameters.Find(param1 => param1.ID == entry.ID);
                        entry.Value = parameter.Value;
                    }
                    else
                    {
                        entities.UserParameterValues.Remove(entry);
                    }
                });

                parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValues.Add(entry));

                entities.SaveChanges();
                return true;
            }
        }

        public static int CreateNewVendor(User user, List<UserParameterValue> parameters, string SenderEmailAddress, string returnMessage, string UserPasswordText)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
          
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Customer customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus.Status == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                       
                        dbtransaction.Commit();

                        if (Addid > 0)
                        {
                            #region EmailTrigger
                            string message = Properties.Settings.Default.EmailTemplate_UserCreationAlert
                                .Replace("@User", user.FirstName)
                                .Replace("@loginName", user.Email)
                                .Replace("@Customer", customerStatus.Name)
                                .Replace("@password", UserPasswordText)
                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                .Replace("@From", ConfigurationManager.AppSettings["SenderEmailAddress"].ToString());
                            EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { user.Email }), null, null, "Temporary Password", message);

                            #endregion
                        }

                        return Addid;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }

        public static void deleteUser(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.Users.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.Users.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDUser(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }
    
        public static bool checkContactExists(string ContactNumber, string Email)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var contQuery = (from row in entities.Users
                                 where row.IsDeleted == false
                                      && row.ContactNumber == ContactNumber
                                      && row.Email != Email
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool checkContactAduitExists(string ContactNumber, string Email)
        {
            bool output = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var contQuery = (from row in entities.mst_User
                                 where row.IsDeleted == false
                                      && row.ContactNumber == ContactNumber
                                      && row.Email != Email
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool checkEmailExists(string Email, int ID)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var contQuery = (from row in entities.Users
                                 where row.IsDeleted == false
                                      && row.Email.Equals(Email)
                                      && row.ID != ID
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool checkauditEmailExists(string Email, int ID)
        {
            bool output = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var contQuery = (from row in entities.mst_User
                                 where row.IsDeleted == false
                                      && row.Email.Equals(Email)
                                      && row.ID != ID
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool UpdateUserDetail(int UserID, string Name, int CID, string EmailID, string cont)
        {
            bool output = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contQuery = (from row in entities.Users
                                     where row.IsDeleted == false
                                          && row.ID == UserID
                                          && row.CustomerID == CID
                                     select row).FirstOrDefault();
                    if (contQuery != null)
                    {
                        contQuery.FirstName = Name;
                        contQuery.Email = EmailID;
                        contQuery.ContactNumber = cont;
                        contQuery.UserName = Name;
                        entities.SaveChanges();
                        output = true;
                    }
                }
                if (output)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var contQuery = (from row in entities.mst_User
                                         where row.IsDeleted == false
                                              && row.ID == UserID
                                              && row.CustomerID == CID
                                         select row).FirstOrDefault();

                        if (contQuery != null)
                        {
                            contQuery.FirstName = Name;
                            contQuery.Email = EmailID;
                            contQuery.ContactNumber = cont;
                            contQuery.UserName = Name;
                            entities.SaveChanges();
                            output = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return output;
        }


        public static bool checkContactExists1(string ContactNumber)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var contQuery = (from row in entities.Users
                                 where row.IsDeleted == false
                                      && row.ContactNumber == ContactNumber
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }


        public static bool checkContactAduitExists1(string ContactNumber)
        {
            bool output = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var contQuery = (from row in entities.mst_User
                                 where row.IsDeleted == false
                                      && row.ContactNumber == ContactNumber
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool checkEmailExists1(string Email)
        {
            bool output = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var contQuery = (from row in entities.Users
                                 where row.IsDeleted == false
                                      && row.Email.Equals(Email)
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool checkauditEmailExists1(string Email)
        {
            bool output = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var contQuery = (from row in entities.mst_User
                                 where row.IsDeleted == false
                                      && row.Email.Equals(Email)
                                 select row).FirstOrDefault();
                if (contQuery != null)
                {
                    output = true;
                }
            }
            return output;
        }

        public static int GetRoleIdByCode(string RoleCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getRole = (from row in entities.Roles
                               where row.Code == RoleCode
                               select row.ID).FirstOrDefault();
                if (getRole != null)
                {
                    return getRole;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static int CreateNewcontractor(User user, List<UserParameterValue> parameters, string SenderEmailAddress, string message)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }
        public static int CreateNewcontractor(User user, List<UserParameterValue> parameters, string SenderEmailAddress, string message, bool WhetherSendMail)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        if (WhetherSendMail)
                        {
                            //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                            //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                        }
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }
        public static int UpdateUserDetail(string Email, string Name, string ContNo, int CID, int RoleID, int UID, bool WhetherSendMail)
        {
            int resultValue = 0;
            try
            {

                #region Update User
                List<UserParameterValue> parameters = new List<UserParameterValue>();
                List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

                User user = new User()
                {
                    FirstName = Name.ToString(),
                    LastName = "",
                    Designation = "",
                    Email = Email.ToString(),
                    ContactNumber = ContNo,
                    Address = "",
                    RoleID = RoleID,
                    IsHead = false,
                    CustomerID = CID,
                    PAN = "",
                    VendorAuditRoleID = RoleID,
                    EnType = "M",
                    UserName = Name.ToString()
                };

                mst_User mstUser = new mst_User()
                {
                    FirstName = Name.ToString(),
                    LastName = "",
                    Designation = "",
                    Email = Email.ToString(),
                    ContactNumber = ContNo,
                    Address = "",
                    RoleID = RoleID,
                    IsExternal = false,
                    IsHead = false,
                    CustomerID = CID,
                    PAN = "",
                    VendorAuditRoleID = RoleID,
                    EnType = "M",
                    UserName = Name.ToString()
                };

                bool result = false;
                string UserName = UserManagement.GetByUsername(UID);

                user.CreatedBy = UID;
                user.CreatedByText = UserName;

                string passwordText = Util.CreateRandomPassword(10);
                user.Password = Util.CalculateMD5Hash(passwordText);
                string message = "true";
                mstUser.CreatedBy = UID;
                mstUser.CreatedByText = UserName;

                mstUser.Password = Util.CalculateMD5Hash(passwordText);

                resultValue = UserManagement.CreateNewcontractor(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message, WhetherSendMail);
                if (resultValue > 0)
                {
                    result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message, WhetherSendMail);
                    if (result == false)
                    {
                        UserManagement.deleteUser(resultValue);
                        resultValue = 0;
                    }
                    if (result)
                    {
                        int Addid = Convert.ToInt32(resultValue);
                        if (Addid > 0)
                        {
                            try
                            {
                                #region EmailTrigger
                                Customer customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));

                                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                string message1 = Properties.Settings.Default.EmailTemplate_UserCreationAlert
                                    .Replace("@User", user.FirstName)
                                    .Replace("@loginName", user.Email)
                                    .Replace("@Customer", customerStatus.Name)
                                    .Replace("@password", passwordText)
                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    .Replace("@From", ConfigurationManager.AppSettings["SenderEmailAddress"].ToString());
                                EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { user.Email }), null, null, "Temporary Password", message1);

                                #endregion
                            }
                            catch (Exception e)
                            { }
                        }
                        resultValue = resultValue;
                    }
                }
                #endregion
                return resultValue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return resultValue;
            }
        }

        //public static int UpdateUserDetail(string Email, string Name, string ContNo, int CID, int RoleID, int UID)
        //{
        //    int resultValue = 0;
        //    try
        //    {

        //        #region Update User
        //        List<UserParameterValue> parameters = new List<UserParameterValue>();
        //        List<UserParameterValue_Risk> parametersRisk = new List<UserParameterValue_Risk>();

        //        User user = new User()
        //        {
        //            FirstName = Name.ToString(),
        //            LastName = "",
        //            Designation = "",
        //            Email = Email.ToString(),
        //            ContactNumber = ContNo,
        //            Address = "",
        //            RoleID = RoleID,
        //            IsHead = false,
        //            CustomerID = CID,
        //            PAN = "",
        //            VendorAuditRoleID = RoleID,
        //            EnType="M",
        //            UserName = Name.ToString()
        //        };

        //        mst_User mstUser = new mst_User()
        //        {
        //            FirstName = Name.ToString(),
        //            LastName = "",
        //            Designation = "",
        //            Email = Email.ToString(),
        //            ContactNumber = ContNo,
        //            Address = "",
        //            RoleID = RoleID,
        //            IsExternal = false,
        //            IsHead = false,
        //            CustomerID = CID,
        //            PAN = "",
        //            VendorAuditRoleID = RoleID,
        //            EnType = "M",
        //            UserName = Name.ToString()
        //        };

        //        bool result = false;
        //        string UserName = UserManagement.GetByUsername(UID);

        //        user.CreatedBy = UID;
        //        user.CreatedByText = UserName;

        //        string passwordText = Util.CreateRandomPassword(10);
        //        user.Password = Util.CalculateMD5Hash(passwordText);
        //        string message = "true";
        //        mstUser.CreatedBy = UID;
        //        mstUser.CreatedByText = UserName;

        //        mstUser.Password = Util.CalculateMD5Hash(passwordText);

        //        resultValue = UserManagement.CreateNewcontractor(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //        if (resultValue > 0)
        //        {
        //            result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
        //            if (result == false)
        //            {
        //                UserManagement.deleteUser(resultValue);
        //                resultValue = 0;
        //            }
        //            if (result)
        //            {
        //                int Addid = Convert.ToInt32(resultValue);
        //                if (Addid > 0)
        //                {
        //                    try
        //                    {
        //                        #region EmailTrigger
        //                        Customer customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));

        //                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
        //                        string message1 = Properties.Settings.Default.EmailTemplate_UserCreationAlert
        //                            .Replace("@User", user.FirstName)
        //                            .Replace("@loginName", user.Email)
        //                            .Replace("@Customer", customerStatus.Name)
        //                            .Replace("@password", passwordText)
        //                            .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                            .Replace("@From", ConfigurationManager.AppSettings["SenderEmailAddress"].ToString());
        //                        EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { user.Email }), null, null, "Temporary Password", message1);

        //                        #endregion
        //                    }
        //                    catch (Exception e)
        //                    { }
        //                }
        //                resultValue = resultValue;
        //            }
        //        }
        //        #endregion
        //        return resultValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return resultValue;
        //    }
        //}




        #endregion
        public static void CreateMaintainLoginDetail(MaintainLoginDetail obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.MaintainLoginDetails.Add(obj);
                entities.SaveChanges();
            }
        }
        public static ClientCustomization GetAssignedEntity(string customizationname, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientCustomization CustomerToUpdate = (from Row in entities.ClientCustomizations
                                                        where Row.ClientID == customerid
                                                        && Row.CustomizationName == customizationname
                                                        select Row).FirstOrDefault();

                return CustomerToUpdate;
            }
        }
        public static int ExistBranchAssignment(int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.BranchAssignments
                                 where row.IsActive == true
                                     && row.CustomerID == CustomerID
                                 select row).FirstOrDefault();

                    if (query != null)
                        return 1;
                    else
                        return 0;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public static string GetuserID(string UserName)
        {
            string output = string.Empty;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_User_Mapping emailQuery = (from RUM in entities.RLCS_User_Mapping
                                                where RUM.Email.Trim().ToUpper() == UserName.Trim().ToUpper()
                                                select RUM).FirstOrDefault();
                if (emailQuery != null)
                {
                    if (emailQuery.UserID != null)
                    {
                        output = emailQuery.UserID;
                    }
                }
                return output;
            }
        }
        public static bool EmailIDExists(string UserName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from RUM in entities.RLCS_User_Mapping
                                  join U in entities.Users on RUM.AVACOM_UserID equals U.ID
                                  join C in entities.Customers on U.CustomerID equals C.ID
                                  where RUM.Email.Trim().ToUpper() == UserName.Trim().ToUpper()
                                  && U.IsDeleted == false
                                  && C.IsDeleted == false
                                  //&& (C.ComplianceProductType == 1 || C.ComplianceProductType == 3)
                                  && (RUM.UserType == "T" || RUM.UserType == "R")
                                  select RUM).ToList();
                if (emailQuery.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool RLCSChangePasswordNew(RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        var userToUpdate = (from entry in entities.RLCS_User_Mapping
                                            where entry.UserID == user.UserID
                                            select entry).ToList();
                        userToUpdate.ForEach(entry =>
                        {
                            entry.Password = user.Password;
                            entry.EnType = "A";
                        });
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static RLCS_User_Mapping GetByRLCSUsername(string username)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from RMU in entities.RLCS_User_Mapping
                            join U in entities.Users
                            on RMU.AVACOM_UserID equals U.ID
                            join C in entities.Customers on U.CustomerID equals C.ID
                            where RMU.UserID.Trim().ToUpper() == username.Trim().ToUpper()
                            && U.IsDeleted == false
                            && C.IsDeleted == false
                            select RMU).FirstOrDefault();

                return user;
            }
        }
        public static bool IsValidUserID(string username, string password, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = null;
                string encpassword = string.Empty;
                var userdetails = (from userRow in entities.Users
                                   join RUM in entities.RLCS_User_Mapping
                                   on userRow.ID equals RUM.AVACOM_UserID
                                   where userRow.IsDeleted == false
                                   && (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                   //&& RUM.Password == password
                                   select RUM).ToList();

                if (userdetails != null)
                {
                    if (userdetails.Count > 0)
                    {
                        if (userdetails.FirstOrDefault().EnType == "M")
                        {
                            encpassword = Util.CalculateMD5Hash(password);
                        }
                        else if (userdetails.FirstOrDefault().EnType == "A")
                        {
                            encpassword = Util.CalculateAESHash(password);
                        }
                        //encpassword = password;
                        user = (from RUM in userdetails
                                where (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                && RUM.Password == encpassword
                                select RUM).FirstOrDefault();
                    }
                }
            }
            return user != null;
        }


        public static List<long> GetByProductIDList(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long)row.ProductID).ToList();

                return productmapping;
            }
        }
        public static bool GetUserTypeInternalExternalByUserID(int userID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = (from row in entities.Users
                                   where row.CustomerID == customerID
                                   && row.ID == userID
                                   select row.IsExternal).FirstOrDefault();

                if (UserDetails != null)
                    return Convert.ToBoolean(UserDetails);
                else
                    return false;
            }
        }
        public static bool RLCSChangePassword(RLCS_User_Mapping user, string SenderEmailAddress, string message)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        var userToUpdate = (from entry in entities.RLCS_User_Mapping
                                            where entry.UserID == user.UserID
                                            select entry).ToList();
                        userToUpdate.ForEach(entry =>
                        {
                            entry.Password = user.Password;
                        });
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static RLCS_User_Mapping GetRLCSByID(string UserName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var emailQuery = (from RMU in entities.RLCS_User_Mapping
                                  join U in entities.Users
                                  on RMU.AVACOM_UserID equals U.ID
                                  join C in entities.Customers on U.CustomerID equals C.ID
                                  where RMU.UserID.Trim().ToUpper() == UserName.Trim().ToUpper()
                                  && C.ServiceProviderID == 94
                                  && U.IsDeleted == false
                                  && C.IsDeleted == false
                                  select RMU).FirstOrDefault();
                return emailQuery;

            }
        }
        public static void LoginCreateDetail(MaintainLoginDetail obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.MaintainLoginDetails.Add(obj);
                entities.SaveChanges();
            }
        }
        public static bool UpdateUserPassword(int UserId, string password)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = (from row in entities.Users
                                where row.ID == UserId
                                && row.IsActive == true && row.IsDeleted == false
                                select row).FirstOrDefault();

                    if (data != null)
                    {
                        data.Password = password.ToString();
                        entities.SaveChanges();
                    }
                }

                using (AuditControlEntities auditentities = new AuditControlEntities())
                {
                    var Auditdata = (from row in auditentities.mst_User
                                     where row.ID == UserId
                                     && row.IsActive == true && row.IsDeleted == false
                                     select row).FirstOrDefault();

                    if (Auditdata != null)
                    {
                        Auditdata.Password = password.ToString();
                        auditentities.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

       

        public static SubscriptionDetail GetSubscriptionDetailByID(int UId, int CId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SubscriptionDetails
                            where row.UId == UId && row.CId == CId
                            select row).FirstOrDefault();
                return data;
            }
        }



      


        public static int CreateNew(User user, List<UserParameterValue> parameters, string SenderEmailAddress, string message)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        parameters.ForEach(entry => user.UserParameterValues.Add(entry));
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }

     
        public static bool Create(widget wid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                try
                {
                    entities.widgets.Add(wid);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        public static string getPassword(string password, string Type)
        {
            string output = string.Empty;

            if (Type == "M")
            {
                output = Util.CalculateMD5Hash(password);
            }
            else if (Type == "A")
            {
                output = Util.CalculateAESHash(password);
            }

            return output;
        }

        public static bool IsValidUser(string username, string password, out User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string encpassword = string.Empty;
                var userdetails = (from userRow in entities.Users
                                   where userRow.IsDeleted == false
                                         && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                                   select userRow).ToList();
                if (userdetails.Count > 0)
                {
                    if (userdetails != null)
                    {
                        if (userdetails.FirstOrDefault().EnType == "M")
                        {
                            encpassword = Util.CalculateMD5Hash(password);
                        }
                        else if (userdetails.FirstOrDefault().EnType == "A")
                        {
                            encpassword = Util.CalculateAESHash(password);
                        }
                    }
                }

                user = (from userRow in userdetails
                        where userRow.IsDeleted == false
                              && userRow.Email.ToUpper().Trim() == username.ToUpper().Trim()
                              && userRow.Password == encpassword
                        select userRow).FirstOrDefault();    //.SingleOrDefault();


               
            }
            return user != null;

           
        }
     
        public static bool HasUserSecurityQuestion(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var isActive = (from row in entities.SecurityQuestionAnswars
                                where row.UserID == userID
                                select true).FirstOrDefault();

                return isActive;
            }
        }

        public static void WrongUpdate(string emailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User userToUpdate = (from Row in entities.Users
                                     where Row.Email == emailID && Row.IsDeleted == false && Row.IsActive == true
                                     select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt += 1;

                    entities.SaveChanges();
                }
            }
        }
        public static void LastLoginUpdate(string emailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                User userToUpdate = (from Row in entities.Users
                                     where Row.Email == emailID && Row.IsDeleted == false && Row.IsActive == true
                                     select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.LastLoginTime = DateTime.Now;

                    entities.SaveChanges();
                }
            }
        }
        public static int WrongAttemptCount(string emailID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int count = (from Row in entities.Users
                             where Row.Email == emailID && Row.IsDeleted == false && Row.IsActive == true
                             select Row.WrongAttempt).FirstOrDefault();

                return count;
            }
        }
        public static bool ChangePassword(User user, string SenderEmailAddress, string message)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                                           
                        User userToUpdate = new User();
                        userToUpdate = (from entry in entities.Users
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.EnType = user.EnType;
                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;                        
                        userToUpdate.ChangePasswordFlag = true;

                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();
                        return false;
                    }
                }
            }
        }

      
        public static List<SecurityQuestion> GetSecurityQuestions()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SecurityQuestions
                            select row).ToList();

                return data;
            }
        }
        public static List<SecurityQuestion> GetRandomQuestions(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> ids = (from row in entities.SecurityQuestionAnswars
                                 where row.UserID == userID
                                 select (int) row.SQID).ToList();

                Random rndNumber = new Random();
                IEnumerable<int> randomids = ids.OrderBy(x => rndNumber.Next()).Take(2);


                List<SecurityQuestion> randomQuestions = (from row in entities.SecurityQuestions
                                                          where randomids.Contains(row.ID)
                                                          select row).ToList();


                return randomQuestions;
            }
        }
        public static User GetByUsername(string username)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.Email == username && row.IsDeleted == false
                            select row).FirstOrDefault();

                return user;
            }
        }
        public static int ValidateQuestions(List<SecurityQuestionAnswar> securityQuestionAns)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long userID = Convert.ToInt64(securityQuestionAns[0].UserID);
                List<SecurityQuestionAnswar> data = (from row in entities.SecurityQuestionAnswars
                                                     where row.UserID == userID
                                                     select row).ToList();

                if (securityQuestionAns[0] != null)
                {
                    var AnserceData = data.Where(entry => entry.SQID == securityQuestionAns[0].SQID && entry.UserID == securityQuestionAns[0].UserID).FirstOrDefault();
                    if (!AnserceData.Answar.Equals(securityQuestionAns[0].Answar))
                        return 1;

                }

                if (securityQuestionAns[1] != null)
                {
                    var AnserceData = data.Where(entry => entry.SQID == securityQuestionAns[1].SQID && entry.UserID == securityQuestionAns[1].UserID).FirstOrDefault();
                    if (!AnserceData.Answar.Equals(securityQuestionAns[1].Answar))
                        return 2;
                }

                return 0;
            }
        }
    }
}