﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class NatureOfComp
    {

        public string lblNatureofcomplianceID { get; set; }

        public string typenatureofcompliance { get; set; }
        public string lblValueAsPerSystem { get; set; }
        public string lblValueAsPerReturn { get; set; }
        public string liabilityPaid { get; set; }

        public string txtValueAsPerSystem { get; set; }
        public string txtValueAsPerReturn { get; set; }
        public string txtLiabilityPaid { get; set; }
        
    }
}