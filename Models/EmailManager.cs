﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Net.Mime;
using System.IO;

namespace AppWebApplication.Models
{
    public class EmailManager
    {     
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();

                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];                

                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                {
                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));
                }
                email.Send(mailMessage);
            }
        }      
    }
}