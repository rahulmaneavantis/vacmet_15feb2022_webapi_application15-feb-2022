﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CFODashboardGraph
    {
        public List<FunctionSummaryPie> FunctionSummaryPieChart { get; set; }
        public List<RiskChartData> RiskChartData { get; set; }
        public List<Item> FunctionSummaryBarChart { get; set; }
        public List<PenaltySummaryChart> PenaltySummaryChart { get; set; }
        public List<CountCFO> CountCFO { get; set; }
        public List<NameValueHierarchy> locationList { get; set; }
        public List<AssignLocationList> AssignLocationList { get; set; }
        
        
    }
}