﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
namespace AppWebApplication.Models
{
    public class InternalDashboardManagement
    {
        public static List<sp_InternalComplianceInstanceTransactionNEW_Result> DashboardDataForPerformerAllNew(int userID, int customerid, string status, DateTime CalenderDate, List<sp_InternalComplianceInstanceTransactionNEW_Result> msttransactionsQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_InternalComplianceInstanceTransactionNEW_Result>();
                //int performerRoleID = (from row in entities.Roles
                //                       where row.Code == "PERF"
                //                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in msttransactionsQuery// entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                             where row.RoleID == 3 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6
                                             || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 18 || row.ComplianceStatusID == 19)
                                              && row.ScheduledOn < nextOneMonth
                                               && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                             where row.RoleID == 3 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 ||
                                               row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 18 || row.ComplianceStatusID == 19)
                                              && row.ScheduledOn == CalenderDate
                                               && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in msttransactionsQuery // entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                         where row.RoleID == 3 && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 19)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in msttransactionsQuery // entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                         where row.RoleID == 3 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 19)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in msttransactionsQuery // entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                         where row.RoleID == 3 && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in msttransactionsQuery // entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
                                         where row.RoleID == 3 && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<sp_InternalComplianceInstanceTransactionNEW_Result> DashboardDataForReviewerAllNew(int userID, int customerID, string status, DateTime CalenderDate, List<sp_InternalComplianceInstanceTransactionNEW_Result> msttransactionsQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_InternalComplianceInstanceTransactionNEW_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in msttransactionsQuery  //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                             where reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.ScheduledOn <= now
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                             where reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.ScheduledOn == CalenderDate
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
                else if (status == "PendingForApproval")
                {

                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                         where reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                         where reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 19 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Completed")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                         where reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 15) //&& row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                         where reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
                                         where reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<sp_ComplianceInstanceTransactionNEW_Result> DashboardDataForReviewerAllNew(int userID, string status, DateTime CalenderDate, List<sp_ComplianceInstanceTransactionNEW_Result> msttransactionsQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceInstanceTransactionNEW_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in msttransactionsQuery // entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
                                             row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 ||
                                             row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
                                             row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 ||
                                             row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 19) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Completed")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID
                                         && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.ScheduledOn <= now
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 15)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 ||
                                          row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                //if (queryStringFlag == "E")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                //}
                //else if (queryStringFlag == "C")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                //}
                return transactionsQuery.ToList();
            }
        }

        public static List<Sp_InternalChecklistTransaction_Result> InternalChecklistDataReviewer(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_InternalChecklistTransaction_Result> transactionsQuery = new List<Sp_InternalChecklistTransaction_Result>();

                DateTime now = DateTime.UtcNow.Date;

                entities.Database.CommandTimeout = 180;
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                if (status == "Overdue")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else if (status == "Closed-Timely")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 4
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else if (status == "Not Applicable")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 15
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else if (status == "Not Complied")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 17
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<Sp_InternalChecklistTransaction_Result> InternalChecklistDataPerformer(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_InternalChecklistTransaction_Result> transactionsQuery = new List<Sp_InternalChecklistTransaction_Result>();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                entities.Database.CommandTimeout = 180;

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                if (status == "Overdue")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == nextOneMonth || row.ScheduledOn <= nextOneMonth) && row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                if (status == "Closed-Timely")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 4
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Not Applicable")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 15
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Not Complied")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_InternalChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 17
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<sp_InternalComplianceInstanceTransactionNEW_Result> DashboardDataForReviewerNew(int userID, int customerID, string status, DateTime CalenderDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<sp_InternalComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_InternalComplianceInstanceTransactionNEW_Result>();
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (status == "Status")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                     where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                     && row.ScheduledOn <= now
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 18)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //            }
        //            else
        //            {
        //                transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                     where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                     && row.ScheduledOn == CalenderDate
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //            }

        //        }
        //        else if (status == "PendingForApproval")
        //        {

        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "DueButNotSubmitted")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 19 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Completed")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 15) && row.ScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        else if (status == "PendingForReview")
        //        {

        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Rejected")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 4, customerID)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}


        //public static List<sp_InternalComplianceInstanceTransactionNEW_Result> DashboardDataForPerformerNew(int userID, int customerid, string status, DateTime CalenderDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<sp_InternalComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_InternalComplianceInstanceTransactionNEW_Result>();
        //        //int performerRoleID = (from row in entities.Roles
        //        //                       where row.Code == "PERF"
        //        //                       select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        if (status == "Status")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                     where
        //                                     //row.UserID == userID && row.RoleID == performerRoleID &&
        //                                     (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6
        //                                     || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 18 || row.ComplianceStatusID == 19)
        //                                      && row.ScheduledOn < nextOneMonth
        //                                       && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //            }
        //            else
        //            {
        //                transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                     where
        //                                       //--row.UserID == userID && row.RoleID == performerRoleID &&
        //                                       (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 ||
        //                                       row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 18 || row.ComplianceStatusID == 19)
        //                                      && row.ScheduledOn == CalenderDate
        //                                       && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //            }
        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                 where
        //                                 //row.UserID == userID && row.RoleID == performerRoleID &&
        //                                  (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 19)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        else if (status == "Overdue")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                 where
        //                                 //row.UserID == userID && row.RoleID == performerRoleID &&
        //                                 (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 19)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && row.ScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                 where
        //                                 //row.UserID == userID && row.RoleID == performerRoleID &&
        //                                  (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Rejected")
        //        {
        //            transactionsQuery = (from row in entities.sp_InternalComplianceInstanceTransactionNEW(userID, 3, customerid)
        //                                 where
        //                                 //row.UserID == userID && row.RoleID == performerRoleID &&
        //                                 (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForReviewerInternalChecklistDisplayCount(int userID,
     List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternalTransactionQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         // && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> SPDashboardDataForReviewerInternalChecklistDisplayCount(int userID,
        // List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> MasterInternalTransactionQuery, CannedReportFilterForPerformer filter, bool pending = false)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        //List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
        //        List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result> transactionsQuery = new List<Sp_InternalComplianceInstanceCheckListTransactionCount_Result>();
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        if (filter == CannedReportFilterForPerformer.PendingForReview)
        //        {
        //            transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.InternalComplianceStatusID == 4)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (pending)
        //        {
        //            //Due But Not Submitted
        //            transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.InternalScheduledOn <= nextOneMonth
        //                                 // && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForReviewerDisplayCount(int userID,
List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        //      public static List<SP_InternalComplianceInstanceTransactionCount_Result> SPDashboardDataForReviewerDisplayCount(int userID,
        //List<SP_InternalComplianceInstanceTransactionCount_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        //      {


        //          using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //          {
        //              List<SP_InternalComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_Result>();
        //              var reviewerRoleIDs = (from row in entities.Roles
        //                                     where row.Code.StartsWith("RVW")
        //                                     select row.ID).ToList();
        //              DateTime now = DateTime.UtcNow.Date;
        //              DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //              if (filter == CannedReportFilterForPerformer.PendingForReview)
        //              {
        //                  transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                       where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                       && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //                                       select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //              }
        //              else if (filter == CannedReportFilterForPerformer.Rejected)
        //              {
        //                  transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                       where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                       && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
        //                                       select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //              }
        //              else if (pending)
        //              {
        //                  //Due But Not Submitted
        //                  transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                       where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                       && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
        //                                       select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //              }
        //              else
        //              {
        //                  transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                       where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                       && row.InternalScheduledOn <= nextOneMonth
        //                                       && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //                                       select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //              }
        //              return transactionsQuery.ToList();
        //          }
        //      }

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForPerformerDisplayCount(int userID,
           List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                             && row.InternalScheduledOn < now
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<SP_InternalComplianceInstanceTransactionCount_Result> SPDashboardDataForPerformerDisplayCount(int userID,
        //       List<SP_InternalComplianceInstanceTransactionCount_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter)
        //{


        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<SP_InternalComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_Result>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        switch (filter)
        //        {
        //            case CannedReportFilterForPerformer.Upcoming:
        //                transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
        //                                     && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
        //                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.Overdue:
        //                transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
        //                                     && row.InternalScheduledOn < now
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.PendingForReview:
        //                transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;

        //            case CannedReportFilterForPerformer.Rejected:

        //                transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
        //                                     select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}


        public static List<InternalComplianceDashboardSummaryView> GetComplianceDashboardSummaryInternal(int Customerid, int Userid = -1, bool ISApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceDashboardSummaryView> transactionsQuery = new List<InternalComplianceDashboardSummaryView>();
                if (ISApprover == true)
                {
                    transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         where row.CustomerID == Customerid
                                         && row.UserID == Userid && row.RoleID == 6
                                         select row).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         join row2 in entities.EntitiesAssignmentInternals
                                         on (long)row.CustomerBranchID equals row2.BranchID
                                         where row.CustomerID == Customerid
                                         && row2.UserID == Userid
                                         select row).ToList();
                }

                return transactionsQuery;
            }
        }


        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {

                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                          && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                          && row.InternalScheduledOn < nextOneMonth
                                           && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         // && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                         //&& (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 10)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.InternalScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                    
                }
                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Date Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }
                //string through search
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
        
        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                             && row.InternalScheduledOn < now
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        
        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int PerformerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= now
                                          && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForApproval")
                {

                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                //if (status == CannedReportFilterForReviewer.Status)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         //&& (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                //else if (status == CannedReportFilterForReviewer.PendingForApproval)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                ////else if (pending)
                ////{
                ////    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                ////                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                ////                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                ////                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                ////}
                //else
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && row.InternalScheduledOn <= nextOneMonth
                //                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}

                //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                //List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
                //foreach (var loc in locations)
                //{
                //    InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                //    BranchRow.ShortDescription = loc.Branch;
                //    BranchRow.InternalComplianceInstanceID = -1;
                //    BranchRow.UserID = -1;
                //    BranchRow.RoleID = -1;
                //    BranchRow.InternalComplianceStatusID = -1;
                //    newList.Add(BranchRow);
                //    List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.InternalScheduledOn).ToList();
                //    foreach (var item1 in temptransaction)
                //    {
                //        newList.Add(item1);
                //    }
                //}

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //performer ID AdvanceSearch
                if (PerformerID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCount(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForReviewerInternalChecklistDisplayCount(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         // && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        //Added by Rahul on 14 Dec 2015 For Internal Dashboard
        public static List<InternalComplianceDashboardSummaryView> GetComplianceDashboardSummaryInternal()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         select row).ToList();

                return transactionsQuery;
            }
        }
        public static DataTable GetPerformanceSummaryRiskWise(IEnumerable<dynamic> transactionsQuery, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            // for Heigh risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 0).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 0).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 0).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 0).Count();
            }
            table.Rows.Add(0, "High", delayedCount, Intime, pendingcount);

            // for Medium risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 1).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 1).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 1).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 1).Count();
            }

            table.Rows.Add(1, "Medium", delayedCount, Intime, pendingcount);

            // for Low risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 2).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 2).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 2).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 2).Count();
            }

            table.Rows.Add(2, "Low", delayedCount, Intime, pendingcount);

            return table;

        }

        public static DataTable GetPerformanceSummaryCatagoryWise(IEnumerable<dynamic> transactionsQueryforCatagory, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories();

            foreach (InternalCompliancesCategory cc in CatagoryList)
            {
                delayedCount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.IComplianceCategoryId == cc.ID).Count();
                Intime = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.IComplianceCategoryId == cc.ID).Count();
                if (role.Equals("PERF"))
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.IComplianceCategoryId == cc.ID).Count();
                }
                else
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.IComplianceCategoryId == cc.ID).Count();
                }

                if (delayedCount != 0 || Intime != 0 || pendingcount != 0)
                    table.Rows.Add(cc.ID, cc.Name, delayedCount, Intime, pendingcount);

            }

            return table;
        }
      
        public static DataTable GetPerformanceSummaryofReporty(int userID, DateTime fromdate, DateTime todate, int branchId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                List<long> reporteeIds = InternalComplianceManagement.GetPerformerListUsingReviewer(userID);
                List<long> MappedinstanceId = InternalComplianceManagement.GetInstanceIDfromReviewer(userID);

                long delayedCount;
                long Intime;
                long pendingcount;
                DateTime now = DateTime.UtcNow.Date;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(string));
                table.Columns.Add("RiskCatagory", typeof(string));
                table.Columns.Add("Delayed", typeof(long));
                table.Columns.Add("InTime", typeof(long));
                table.Columns.Add("Pending", typeof(long));

                var ReportyList = (from row in entities.Users
                                   where reporteeIds.Contains(row.ID) && row.IsDeleted != true
                                   select row).ToList();

                List<InternalComplianceInstanceTransactionView> transactionQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                                                    where row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate && row.RoleID == performerRoleID
                                                                                    && MappedinstanceId.Contains(row.InternalComplianceInstanceID)
                                                                                    select row).ToList();

                if (branchId != -1)
                {
                    transactionQuery = transactionQuery.Where(entry => entry.CustomerBranchID == branchId).ToList();
                }

                foreach (User reporty in ReportyList)
                {
                    pendingcount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6)).Select(entity => entity.InternalScheduledOnID).Distinct().Count();
                    delayedCount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    Intime = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    table.Rows.Add(reporty.ID, reporty.FirstName + " " + reporty.LastName, delayedCount, Intime, pendingcount);
                }
                return table;
            }

        }
      
    }
}