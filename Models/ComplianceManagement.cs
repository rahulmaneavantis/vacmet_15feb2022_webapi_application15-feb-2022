﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using AppWebApplication.Data;
using Nest;

namespace AppWebApplication.Models
{

    public class GetReportDetail
    {
        public int ID { get; set; }
        public long UserID { get; set; }
        public Nullable<long> CustID { get; set; }
        public string ComplianceType { get; set; }
        public string RoleFlag { get; set; }
        public string risk { get; set; }
        public string status { get; set; }
        public string userDetail { get; set; }
        public string actDetail { get; set; }
        public int ExportRequestID { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string Location { get; set; }
        public System.DateTime filecreatedon { get; set; }
        public string complianceType { get; set; }
    }
    public class GradingDisplayPopup
    {
        public List<ComplianceInstanceTransactionView> Statutory { get; set; }
        public List<Sp_DeptHead_ComplianceInstanceTransactionView_Result> StatutoryDept { get; set; }

        public List<InternalComplianceInstanceTransactionView> Internal { get; set; }
        public List<Sp_DeptHead_InternalComplianceInstanceTransactionView_Result> InternalDept { get; set; }

        public string perGradingRiskChart { get; set; }
    }
    public class MyData
    {
        public List<SP_InternalLicenseInstanceTransactionCount_Result> Internal { get; set; }
        public List<SP_LicenseInstanceTransactionCount_Result> Statutory { get; set; }

    }
    public class ComplianceManagement
    {

        #region Sachin Changes 17 JAN 2022

        public static bool DownloadFileAndSaveDocuments(string downloadFilePath, string fileName, string file, out string downloadedFilePath)
        {
            bool downloadSuccess = false;
            downloadedFilePath = string.Empty;

            try
            {
                string newfile = file.Substring(0, 8);

                string AppLocation = ConfigurationManager.AppSettings["ApprovalActDocSavePath"];
                AvantisManagementMailService.Library.WriteErrorLog("AppLocation - " + AppLocation);
                string directoryPath = string.Empty;
                string DocAPIURlwithPATH = string.Empty;

                directoryPath = AppLocation + "/" + newfile;
                AvantisManagementMailService.Library.WriteErrorLog("directoryPath - " + directoryPath);
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                directoryPath = directoryPath + @"\" + fileName;
                AvantisManagementMailService.Library.WriteErrorLog("directoryPath - " + directoryPath);
                DocAPIURlwithPATH = downloadFilePath;

                AvantisManagementMailService.Library.WriteErrorLog("DocAPIURlwithPATH - " + DocAPIURlwithPATH);

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    downloadSuccess = DownloadFile(DocAPIURlwithPATH, directoryPath);

                    if (downloadSuccess)
                        downloadedFilePath = directoryPath;
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        //public static bool DownloadFileAndSaveDocuments(string downloadFilePath, string fileName, string file, out string downloadedFilePath)
        //{
        //    bool downloadSuccess = false;
        //    downloadedFilePath = string.Empty;

        //    try
        //    {
        //        string newfile = file.Substring(0, 8);

        //        string AppLocation = ConfigurationManager.AppSettings["ApprovalActDocSavePath"];

        //        string directoryPath = string.Empty;
        //        string DocAPIURlwithPATH = string.Empty;

        //        directoryPath = AppLocation + "/" + newfile;

        //        if (!Directory.Exists(directoryPath))
        //            Directory.CreateDirectory(directoryPath);

        //        directoryPath = directoryPath + @"\" + fileName;

        //        DocAPIURlwithPATH = downloadFilePath;

        //        if (!string.IsNullOrEmpty(directoryPath))
        //        {
        //            downloadSuccess = DownloadFile(DocAPIURlwithPATH, directoryPath);

        //            if (downloadSuccess)
        //                downloadedFilePath = directoryPath;
        //        }

        //        return downloadSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return downloadSuccess;
        //    }
        //}

        public static bool DownloadFile(string urlAddress, string fileSavePath)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(urlAddress, fileSavePath);
                    client.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Logger.InsertException_DBLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion


        #region new Report 9 SEP 2021
        public static int GetDetailReportThreshold()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.DetailReportThresholds
                            select row.ThresholdNo).FirstOrDefault();
                return form;
            }
        }
        public static ExportDetailReportRequest CheckExportReportRequest(int UserID, int CustomerID, string FlagIsApp)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.ExportDetailReportRequests
                            where row.UserID == UserID
                            && row.CustID == CustomerID
                             && row.CreatedOn.Value.Year == DateTime.Today.Year
                            && row.CreatedOn.Value.Month == DateTime.Today.Month
                            && row.CreatedOn.Value.Day == DateTime.Today.Day
                            && row.IsActive == true
                            && row.RoleFlag == FlagIsApp
                            select row).FirstOrDefault();

                return file;
            }
        }

        public static void UpdateExportDetailReportRequests(ExportDetailReportRequest UpdateExportDetailReportRequest)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var updateExportDetailReportRequest = (from row in entities.ExportDetailReportRequests
                                                       where row.ID == UpdateExportDetailReportRequest.ID
                                                       select row).FirstOrDefault();

                if (updateExportDetailReportRequest != null)
                {
                    updateExportDetailReportRequest.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }

        #endregion
        public static long CreateEmailLog(Email email)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {

                    entities.Emails.Add(email);
                    entities.SaveChanges();

                    return email.Id;
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }
        public static int GetCountDept(int DepartmentID, String CountType, string satutoryinternal, bool IsApprover, int userID, int customerid, int CustBranchID)
        {
            try
            {
                int Count = 0;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (satutoryinternal == "Statutory")
                    {
                        List<sp_ComplianceInstanceAssignmentViewCount_DeptHead_Result> TotalList = new List<sp_ComplianceInstanceAssignmentViewCount_DeptHead_Result>();
                        if (IsApprover == true)
                        {
                            TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount_DeptHead(userID, DepartmentID, "APPR")
                                         select row).ToList();
                        }
                        else
                        {
                            TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount_DeptHead(userID, DepartmentID, "DEPT")
                                         select row).ToList();
                        }

                        if (CustBranchID != -1)
                            TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                        //if (Category != -1)
                        TotalList = TotalList.Where(Entry => Entry.DepartmentID == DepartmentID).ToList();

                        if (CountType == "U")
                        {
                            //TotalList = TotalList.GroupBy(entry => entry.CustomerBranchID).Select(q => q.FirstOrDefault()).ToList();
                            Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();
                        }

                        if (CountType == "C")
                            Count = TotalList.Select(Entry => Entry.ComplianceId).Distinct().Count();

                        if (CountType == "L")
                            Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();

                    }
                    else if (satutoryinternal == "Internal")
                    {
                        List<sp_InternalComplianceInstanceAssignmentViewCount_DeptHead_Result> TotalList = new List<sp_InternalComplianceInstanceAssignmentViewCount_DeptHead_Result>();
                        if (IsApprover == true)
                        {
                            TotalList = (from row in entities.sp_InternalComplianceInstanceAssignmentViewCount_DeptHead(userID, DepartmentID, "APPR")
                                         select row).ToList();
                        }
                        else
                        {
                            TotalList = (from row in entities.sp_InternalComplianceInstanceAssignmentViewCount_DeptHead(userID, DepartmentID, "DEPT")
                                         select row).ToList();
                        }

                        if (CustBranchID != -1)
                            TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                        //if (Category != -1)
                        TotalList = TotalList.Where(Entry => Entry.DepartmentID == DepartmentID).ToList();

                        if (CountType == "U")
                        {
                            //TotalList = TotalList.GroupBy(entry => entry.CustomerBranchID).Select(q => q.FirstOrDefault()).ToList();
                            Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();
                        }
                        if (CountType == "C")
                            Count = TotalList.Select(Entry => Entry.InternalComplianceID).Distinct().Count();

                        if (CountType == "L")
                            Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();

                    }
                    return Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static tbl_ComplianceRemark GetCompliancelistRemark(int SOID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.tbl_ComplianceRemark
                            where row.ScheduleOnID == SOID
                            select row).FirstOrDefault();
                return form;
            }
        }
        public static tbl_ComplianceRemark UpdateRemark(string remark, int scheduleOnID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.tbl_ComplianceRemark
                            where row.ScheduleOnID == scheduleOnID
                            select row).FirstOrDefault();
                if (form != null)
                {
                    form.Remark = remark;
                    form.UpdatedBy = UserID;
                    form.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                return form;
            }
        }
        public static void BindStatutoryCheckListDatainRedis(long ComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int StatusFlag = -1;
                    var getComplianceStatus = (from row in entities.ComplianceInstances
                                               join row1 in entities.Compliances
                                               on row.ComplianceId equals row1.ID
                                               where row.ID == ComplianceInstanceID
                                               select row1).FirstOrDefault();

                    if (getComplianceStatus != null)
                    {
                        if (getComplianceStatus.EventFlag != null)
                            StatusFlag = 4;
                        else
                            StatusFlag = 2;
                    }
                    if (StatusFlag != -1)
                    {
                        var statusList = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == ComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_PRA_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_PRA_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_REV_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_REV_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_APPR_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_APPR_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.ComplianceInstances
                                        join row1 in entities.Compliances
                                        on row.ComplianceId equals row1.ID
                                        join row2 in entities.Acts
                                        on row1.ActID equals row2.ID
                                        join row3 in entities.EntitiesAssignments
                                        on (long)row2.ComplianceCategoryId equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == ComplianceInstanceID && row1.IsDeleted == false
                                        && row2.IsDeleted == false && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetChecklistEventData_" + item + "_" + Customerid + "_MGMT_" + StatusFlag;
                            }
                            else
                            {
                                CacheName = "CacheGetChecklistEventData_" + item + "_" + Customerid + "_MGMT_" + StatusFlag;
                            }

                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static void UpdatePenaltyTransactionByReviewer1(ComplianceTransaction complianceTransaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceInstanceId == complianceTransaction.ComplianceInstanceId
                                                     && row.ComplianceScheduleOnID == complianceTransaction.ComplianceScheduleOnID
                                                     && (row.StatusId == 2 || row.StatusId == 3)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();

                Transaction.Interest = complianceTransaction.Interest;
                Transaction.Penalty = complianceTransaction.Penalty;
                Transaction.IsPenaltySave = complianceTransaction.IsPenaltySave;
                Transaction.PenaltySubmit = complianceTransaction.PenaltySubmit;
                entities.SaveChanges();
            }
        }
        public static SP_Check_UserLeavePeriodExists_Result GetUserLeavePeriodExists(long userid, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskReminders = (from row in entities.SP_Check_UserLeavePeriodExists(userid, flag)
                                     select row).FirstOrDefault();
                return taskReminders;
            }
        }
        public static bool CreateTransaction(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.SaveDocFiles(filesList);
                            //transaction.Dated = DateTime.UtcNow; Comment on 19 JAN 2017 Sachin Nikam
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("gaurav@tlregtech.in");
                TO.Add("rahul@tlregtech.in");
                //SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), "Teamlease Regtech", new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function Andriod Service", "CreateTransaction");

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), null,
                  null, "Error Occured as  CreateTransaction Function Andriod Service", " CreateTransaction");

                return false;
            }
        }
        public static void BindDatainRedis(long ComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string type = string.Empty;

                    var getComplianceStatus = (from row in entities.ComplianceInstances
                                               join row1 in entities.Compliances
                                               on row.ComplianceId equals row1.ID
                                               where row.ID == ComplianceInstanceID
                                               select row1).FirstOrDefault();

                    if (getComplianceStatus != null)
                    {
                        if (getComplianceStatus.EventFlag != null)
                            type = "Event Based";
                        else
                            type = "Statutory";
                    }

                    if (!string.IsNullOrEmpty(type))
                    {
                        var statusList = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == ComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_REV_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_REV_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.ComplianceInstances
                                        join row1 in entities.Compliances
                                        on row.ComplianceId equals row1.ID
                                        join row2 in entities.Acts
                                        on row1.ActID equals row2.ID
                                        join row3 in entities.EntitiesAssignments
                                        on (long)row2.ComplianceCategoryId equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == ComplianceInstanceID && row1.IsDeleted == false
                                        && row2.IsDeleted == false && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetStatutoryEventData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }
                            else
                            {
                                CacheName = "CacheGetStatutoryEventData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }

                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static ComplianceScheduleOn GetComplianceScheduleOnDetails(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstanceID = (from row in entities.ComplianceScheduleOns
                                            where row.ID == ScheduleOnID
                                            select row).FirstOrDefault();
                return ComplianceInstanceID;
            }
        }
        public static long? GetComplianceInstanceID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstanceID = (from row in entities.ComplianceScheduleOns
                                            where row.ID == ScheduleOnID
                                            select row.ComplianceInstanceID).FirstOrDefault();
                return ComplianceInstanceID;
            }
        }
        public static ComplianceTransaction GetPenaltyTransactionReviewer(long ComplianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                     && (row.StatusId == 5)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();

                return Transaction;
            }
        }
        public static void CreateTransaction(List<ComplianceTransaction> transaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    //entry.Dated = DateTime.UtcNow; Comment on 19 JAN 2017 Sachin Nikam
                    entities.ComplianceTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }
        public static void UpdateApproverReminders(Managment_Reminder UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Managment_Reminder
                                                  where row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.UserId == UpdateEscalation.UserId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.RemindFlag = UpdateEscalation.RemindFlag;
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateManagmentReminders(Managment_Reminder UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Managment_Reminder
                                                  where row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.UserId == UpdateEscalation.UserId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.RemindFlag = UpdateEscalation.RemindFlag;
                    entities.SaveChanges();
                }
            }
        }
        public static void CreateManagmentReminders(Managment_Reminder cmpcustomescalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Managment_Reminder.Add(cmpcustomescalation);
                entities.SaveChanges();
            }
        }
        public static List<ManagmentReminddemo> GetComplianceReminderApprover(int UserID, int CustomerID, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var a = "Y";
                var b = "A";
                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row2 in entities.Managment_Reminder
                                         on new { a = (int)row.ComplianceID, b = (int)row.CustomerBranchID }
                                         equals new { a = row2.ComplianceId, b = (int)row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID //&& rt.UserFlag == "A"
                                         && row.UserID == UserID && row.RoleID == 6
                                         select new ManagmentReminddemo()
                                         {
                                             complianceID = (int)row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long)row.ActID,
                                             CustomerBranchID = (int)row.CustomerBranchID,
                                             ActName = row.Name,
                                             RemindFlag = rt.RemindFlag,
                                             UserFlag = rt.UserFlag,
                                             EventFlag = (Boolean)row.EventFlag,
                                             RiskType = (int)row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?)row.DueDate,
                                             IsChecktrue = (rt.RemindFlag == a && rt.UserFlag == b ? "True" : "False"),
                                             PerformerName = row.User,
                                             ReviewerName = row.ReviewerName,
                                         }).Distinct().ToList();

                if (Type == -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                if (Type == 1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                //if (Risk != -1)
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                //if (location != "Entity/Sub-Entity/Location")
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }
        //public static List<ManagmentReminddemo> GetComplianceReminderApprover(int UserID, int CustomerID,int Type)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var a = "Y";
        //        var b = "A";
        //        var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
        //                                 join row2 in entities.Managment_Reminder
        //                                 on new { a = (int)row.ComplianceID, b = (int)row.CustomerBranchID }
        //                                 equals new { a = row2.ComplianceId, b = (int)row2.CustomerBranchID }
        //                                 into t
        //                                 from rt in t.DefaultIfEmpty()
        //                                 where row.CustomerID == CustomerID //&& rt.UserFlag == "A"
        //                                 && row.UserID == UserID && row.RoleID == 6
        //                                 select new ManagmentReminddemo()
        //                                 {
        //                                     complianceID = (int)row.ComplianceID,
        //                                     ShortDescription = row.ShortDescription,
        //                                     ActID = (long)row.ActID,
        //                                     CustomerBranchID = (int)row.CustomerBranchID,
        //                                     ActName = row.Name,
        //                                     RemindFlag = rt.RemindFlag,
        //                                     UserFlag = rt.UserFlag,
        //                                     EventFlag = (Boolean)row.EventFlag,
        //                                     RiskType = (int)row.RiskType,
        //                                     Branch = row.Branch,
        //                                     Frequency = row.Frequency == 0 ? "Monthly" :
        //                                     row.Frequency == 1 ? "Quarterly" :
        //                                     row.Frequency == 2 ? "HalfYearly" :
        //                                     row.Frequency == 3 ? "Annual" :
        //                                     row.Frequency == 4 ? "FourMonthly" :
        //                                     row.Frequency == 5 ? "TwoYearly" :
        //                                     row.Frequency == 6 ? "SevenYearly" : "",
        //                                     DueDate = (int?)row.DueDate,
        //                                     IsChecktrue = (rt.RemindFlag == a && rt.UserFlag == b ? "True" : "False")
        //                                 }).Distinct().ToList();

        //        if (Type == -1)
        //            ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

        //        if (Type == 1)
        //            ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

        //        //if (Risk != -1)
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

        //        //if (location != "Entity/Sub-Entity/Location")
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
        //        return ComplianceDetails;
        //    }
        //}


        public static List<ManagmentReminddemo> GetComplianceReminderManagment(int UserID, int CustomerID,int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var a = "Y";
                var b = "M";
                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join acte in entities.EntitiesAssignments
                                         on (long?)row.CustomerBranchID equals acte.BranchID
                                         join row2 in entities.Managment_Reminder
                                         on new { a = (int)row.ComplianceID, b = (int)row.CustomerBranchID }
                                         equals new { a = (int)row2.ComplianceId, b = (int)row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID //&& rt.UserFlag == "M"
                                         //&& row.UserID == UserID && row.RoleID == 4
                                         select new ManagmentReminddemo()
                                         {
                                             complianceID = (int)row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long)row.ActID,
                                             CustomerBranchID = (int)row.CustomerBranchID,
                                             ActName = row.Name,
                                             RemindFlag = rt.RemindFlag,
                                             UserFlag = rt.UserFlag,
                                             EventFlag = (Boolean)row.EventFlag,
                                             RiskType = (int)row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?)row.DueDate,
                                             IsChecktrue = (rt.RemindFlag == a && rt.UserFlag == b ? "True" : "False")
                                         }).Distinct().ToList();
                if (Type == -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                if (Type == 1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                //if (Risk != -1)
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                //if (location != "Entity/Sub-Entity/Location")
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }


        #region Mrunal
        public static void CreateEscalationReminders(ComplianceCustomEscalation cmpcustomescalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceCustomEscalations.Add(cmpcustomescalation);
                entities.SaveChanges();
            }
        }




        public static void UpdateEscalation(ComplianceCustomEscalation UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.ComplianceCustomEscalations
                                                  where row.UserIdReviewer == UpdateEscalation.UserIdReviewer
                                                  && row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.Interimdays = UpdateEscalation.Interimdays;
                    NotificationRecordToUpdate.days = UpdateEscalation.days;
                    entities.SaveChanges();
                }
            }
        }



        public static List<EscalationClass> GetComplianceEscalation(int UserID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row2 in entities.ComplianceCustomEscalations
                                         on new { a = (int)row.ComplianceID, b = (int)row.CustomerBranchID }
                                         equals new { a = (int)row2.ComplianceId, b = (int)row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID
                                         && row.UserID == UserID && row.RoleID == 4
                                         && row.Frequency != 7 && row.Frequency != 8
                                         && row.ComplianceType !=1
                                         select new EscalationClass()
                                         {
                                             complianceID = (int)row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long)row.ActID,
                                             CustomerBranchID = (int)row.CustomerBranchID,
                                             ActName = row.Name,
                                             User = row.User,
                                             IntreimDays = rt.Interimdays,
                                             EscDays = rt.days,
                                             EventFlag = (Boolean)row.EventFlag,
                                             RiskType = (int)row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?)row.DueDate,
                                             PerformerName = row.PerformerName,
                                             ReviewerName = row.ReviewerName,
                                             ReportType = row.ReportType,
                                         }).Distinct().ToList();

                //if (Type == -1)
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                //if (Type == 1)
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                //if (Risk != -1)
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                //if (location != "Entity/Sub-Entity/Location")
                //    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }

        //public static List<EscalationClass> GetComplianceEscalation(int UserID, int CustomerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
        //                                 join row2 in entities.ComplianceCustomEscalations
        //                                 on new { a = (int)row.ComplianceID, b = (int)row.CustomerBranchID }
        //                                 equals new { a = (int)row2.ComplianceId, b = (int)row2.CustomerBranchID }
        //                                 into t
        //                                 from rt in t.DefaultIfEmpty()
        //                                 where row.CustomerID == CustomerID
        //                                 && row.UserID == UserID && row.RoleID == 4
        //                                 && row.Frequency != 7 && row.Frequency != 8
        //                                 select new EscalationClass()
        //                                 {
        //                                     complianceID = (int)row.ComplianceID,
        //                                     ShortDescription = row.ShortDescription,
        //                                     ActID = (long)row.ActID,
        //                                     CustomerBranchID = (int)row.CustomerBranchID,
        //                                     ActName = row.Name,
        //                                     User = row.User,
        //                                     IntreimDays = rt.Interimdays,
        //                                     EscDays = rt.days,
        //                                     EventFlag = (Boolean)row.EventFlag,
        //                                     RiskType = (int)row.RiskType,
        //                                     Branch = row.Branch,
        //                                     Frequency = row.Frequency == 0 ? "Monthly" :
        //                                     row.Frequency == 1 ? "Quarterly" :
        //                                     row.Frequency == 2 ? "HalfYearly" :
        //                                     row.Frequency == 3 ? "Annual" :
        //                                     row.Frequency == 4 ? "FourMonthly" :
        //                                     row.Frequency == 5 ? "TwoYearly" :
        //                                     row.Frequency == 6 ? "SevenYearly" : "",
        //                                     DueDate = (int?)row.DueDate,
        //                                 }).Distinct().ToList();

        //        //if (Type == -1)
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

        //        //if (Type == 1)
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

        //        //if (Risk != -1)
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

        //        //if (location != "Entity/Sub-Entity/Location")
        //        //    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
        //        return ComplianceDetails;
        //    }
        //}




        public static List<ComplianceReminderListView> GetComplianceReminderNotificationsByUserID(int userID, string filter = null)
        {
            DateTime date = DateTime.Now.Date;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceReminderListView> complianceReminders = (from row in entities.ComplianceReminderListViews
                                                                        where row.UserID == userID && row.RemindOn >= date && row.Status == 0
                                                                        select row).OrderBy(entry => entry.RemindOn).ToList();


                if (!string.IsNullOrEmpty(filter))
                {
                    complianceReminders = complianceReminders.Where(entry => entry.CustomerBranchName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Description.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceReminders;
            }

        }

        #endregion


        public static List<ComplianceForm> GetComplianceForm(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.ComplianceForms
                                  where row.ComplianceID == complianceID
                                  select row).ToList();

                return compliance;
            }
        }
        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> SPGetMappedComplianceCheckListReviewer(long UserID, List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MasterStatutoryReviewerCheckList, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in MasterStatutoryReviewerCheckList //entities.CheckListInstanceTransactionReviewerViews
                                     where row.UserID == UserID && row.RoleID == performerRoleID
                                     && (row.ScheduledOn == now || row.ScheduledOn <= now)
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).ToList();
                return transactionsQuery;
            }
        }

        //public static List<Sp_CheckListInstanceTransactionReviewerCount_Result> SPGetMappedComplianceCheckListReviewer(long UserID, List<Sp_CheckListInstanceTransactionReviewerCount_Result> MasterStatutoryReviewerCheckList, bool IsPerformer, string filter = null)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<Sp_CheckListInstanceTransactionReviewerCount_Result> transactionsQuery = new List<Sp_CheckListInstanceTransactionReviewerCount_Result>();

        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "RVW1"
        //                               select row.ID).Single();


        //        DateTime now = DateTime.UtcNow.Date;
        //        transactionsQuery = (from row in MasterStatutoryReviewerCheckList //entities.CheckListInstanceTransactionReviewerViews
        //                             where row.UserID == UserID && row.RoleID == performerRoleID
        //                             && (row.ScheduledOn == now || row.ScheduledOn <= now)
        //                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                             select row).ToList();
        //        return transactionsQuery;
        //    }
        //}

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> GetMappedComplianceCheckListRahul1(long UserID,
  List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MaterCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterCheckListTransactionsQuery //entities.CheckListInstanceTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.ScheduledOn == now || row.ScheduledOn <= now)
                                         && row.ComplianceStatusID == 1
                                         select row).ToList();

                }
                return transactionsQuery;
            }
        }

        // public static List<SP_CheckListInstanceTransactionCount_Result> GetMappedComplianceCheckListRahul1(long UserID,
        //List<SP_CheckListInstanceTransactionCount_Result> MaterCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        // {
        //     using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //     {
        //         List<SP_CheckListInstanceTransactionCount_Result> transactionsQuery = new List<SP_CheckListInstanceTransactionCount_Result>();
        //         if (IsPerformer == true)
        //         {
        //             int performerRoleID = (from row in entities.Roles
        //                                    where row.Code == "PERF"
        //                                    select row.ID).Single();


        //             DateTime now = DateTime.UtcNow.Date;
        //             transactionsQuery = (from row in MaterCheckListTransactionsQuery //entities.CheckListInstanceTransactionViews
        //                                  where row.UserID == UserID && row.RoleID == performerRoleID
        //                                  && (row.ScheduledOn == now || row.ScheduledOn <= now)
        //                                  && row.ComplianceStatusID == 1
        //                                  select row).ToList();

        //         }
        //         return transactionsQuery;
        //     }
        // }
        public static InternalCompliance GetInternalComplianceByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        //public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var scheduleList = (from row in entities.ComplianceSchedules
        //                            where row.ComplianceID == complianceID
        //                            orderby row.ForMonth
        //                            select row).ToList();

        //        return scheduleList;
        //    }
        //}

        //public static Compliance GetByID(long complianceID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
        //                          where row.ID == complianceID
        //                          select row).SingleOrDefault();

        //        return compliance;
        //    }
        //}

        //public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        //{
        //    string forMonthName = string.Empty;
        //    switch (Frequency)
        //    {
        //        case 0:
        //            string year = date.ToString("yy");
        //            if (date > DateTime.Today.Date)
        //            {
        //                if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
        //                {
        //                    if (forMonth == 11)
        //                    {
        //                        year = (date.AddYears(-1)).ToString("yy");
        //                    }
        //                    else if (forMonth == 12)
        //                    {
        //                        year = (date.AddYears(-1)).ToString("yy");
        //                    }
        //                    else
        //                    {
        //                        year = (date).ToString("yy");
        //                    }
        //                }
        //                else if (forMonth == 12)
        //                {
        //                    year = (date.AddYears(-1)).ToString("yy");
        //                }//added by rahul on 18 OCT 2016   
        //            }
        //            else
        //            {
        //                if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
        //                {
        //                    year = (date.AddYears(-1)).ToString("yy");
        //                }
        //                else if (forMonth == 12)
        //                {
        //                    year = (date.AddYears(-1)).ToString("yy");
        //                }//added by rahul on 18 OCT 2016   
        //            }
        //            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
        //            break;
        //        case 1:
        //            string yearq = date.ToString("yy");
        //            if (forMonth == 10)
        //            {
        //                if (date.ToString("MM") == "10")
        //                {
        //                    yearq = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "11")
        //                {
        //                    yearq = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "12")
        //                {
        //                    yearq = (date).ToString("yy");
        //                }
        //                else
        //                {
        //                    yearq = (date.AddYears(-1)).ToString("yy");
        //                }
        //            }
        //            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
        //            break;
        //        case 2:
        //            string year1 = date.ToString("yy");
        //            if (forMonth == 7)
        //            {
        //                if (date.ToString("MM") == "10")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "11")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "12")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }

        //                else
        //                {
        //                    year1 = (date.AddYears(-1)).ToString("yy");
        //                }

        //                //year1 = (date.AddYears(-1)).ToString("yy");

        //            }
        //            if (forMonth == 10)
        //            {
        //                // year1 = (date.AddYears(-1)).ToString("yy");
        //                if (date.ToString("MM") == "10")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "11")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }
        //                else if (date.ToString("MM") == "12")
        //                {
        //                    year1 = (date).ToString("yy");
        //                }
        //                else
        //                {
        //                    year1 = (date.AddYears(-1)).ToString("yy");
        //                }
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
        //            }
        //            else
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
        //                           " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
        //            }

        //            break;

        //        case 3:

        //            string startFinancial1Year = date.ToString("yy");
        //            string endFinancial1Year = date.ToString("yy");
        //            if (date.Month >= 4)
        //            {
        //                startFinancial1Year = (date.AddYears(-1)).ToString("yy");
        //                endFinancial1Year = date.ToString("yy");
        //            }
        //            else
        //            {
        //                startFinancial1Year = (date.AddYears(-2)).ToString("yy");
        //                endFinancial1Year = (date.AddYears(-1)).ToString("yy");
        //            }

        //            if (forMonth == 1)
        //            {
        //                forMonthName = "CY - " + endFinancial1Year;
        //            }
        //            else
        //            {
        //                forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
        //            }
        //            break;

        //        case 4:

        //            string year2 = date.ToString("yy");
        //            if (forMonth == 9)
        //            {
        //                if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
        //                {
        //                    year2 = (date.AddYears(-1)).ToString("yy");
        //                }
        //                else
        //                {
        //                    year2 = (date).ToString("yy");
        //                }

        //            }
        //            if (forMonth == 12)
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
        //                          " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
        //            }
        //            else
        //            {
        //                forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
        //                               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
        //            }
        //            break;
        //        case 5:

        //            string startFinancial2Year = date.ToString("yy");
        //            string endFinancial2Year = date.ToString("yy");
        //            if (forMonth == 1)
        //            {
        //                startFinancial2Year = (date.AddYears(-2)).ToString("yy");
        //                endFinancial2Year = (date.AddYears(-1)).ToString("yy");
        //                forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
        //            }
        //            else if (forMonth == 4)
        //            {
        //                startFinancial2Year = (date.AddYears(-2)).ToString("yy");
        //                endFinancial2Year = date.ToString("yy");
        //                forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
        //            }
        //            break;
        //        case 6:

        //            string startFinancial7Year = date.ToString("yy");
        //            string endFinancial7Year = date.ToString("yy");
        //            if (date.Month >= 4)
        //            {
        //                startFinancial7Year = (date.AddYears(-7)).ToString("yy");
        //                endFinancial7Year = date.ToString("yy");
        //            }
        //            else
        //            {
        //                startFinancial7Year = (date.AddYears(-8)).ToString("yy");
        //                endFinancial7Year = (date.AddYears(-1)).ToString("yy");
        //            }

        //            forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
        //            break;

        //        default:
        //            forMonthName = string.Empty;
        //            break;
        //    }

        //    return forMonthName;
        //}
        public static List<sp_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetailsDashboard(int customerid, List<long> Branchlist, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_ComplianceInstanceAssignmentViewDetails_Result>();

                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "APPR")
                                         select row).ToList();

                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();

                }

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                //if (ActId != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                //}
                //if (CategoryID != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                //}

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }


        public static List<Sp_GetLegalUpdateMyFavouriteDetails_Result> GetMyFavouriteDetails(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Sp_GetLegalUpdateMyFavouriteDetails(UserID)                            
                             select row).Distinct().ToList();

                return query;
            }
        }

        public static List<ComplianceInstanceTransactionView> GetPeriodBranchLocation(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID && row.RoleID == 3
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).Distinct().ToList();

                return query;
            }
        }
        public class ActListNew
        {
            [Text(Fielddata = true)]
            public int actid { get; set; }
            [Text(Fielddata = true)]
            public Nullable<int> compliancetypeid { get; set; }
            [Text(Fielddata = true)]
            public Nullable<int> compliancecategoryid { get; set; }
            public Nullable<int> stateid { get; set; }
            [Text(Fielddata = true)]
            public string actname { get; set; }
            [Text(Fielddata = true)]
            public string compliancetypename { get; set; }
            [Text(Fielddata = true)]
            public string compliancecategoryname { get; set; }
            [Text(Fielddata = true)]
            public string state { get; set; }            
            public int IsMyFavourite { get; set; }
            [Text(Fielddata = true)]
            public long compliancecount { get; set; }
            [Text(Fielddata = true)]
            public long dailyupdatecount { get; set; }
        }

        public class ComplianceListNew
        {
            [Text(Fielddata = true)]
            public long complianceid { get; set; }
            [Text(Fielddata = true)]
            public string description { get; set; }
            [Text(Fielddata = true)]
            public string actid { get; set; }
            [Text(Fielddata = true)]
            public string sections { get; set; }
            [Text(Fielddata = true)]
            public string shortdescription { get; set; }
            [Text(Fielddata = true)]
            public string penaltydescription { get; set; }
            [Text(Fielddata = true)]
            public string referencematerialtext { get; set; }
            [Text(Fielddata = true)]
            public string sampleformlink { get; set; }
            [Text(Fielddata = true)]
            public string compliancesampleform { get; set; }
            [Text(Fielddata = true)]
            public string compliancesampleformpath { get; set; }
            public int ismyfavourite { get; set; }
        }

        public class ComplianceListNew1
        {       
            public long complianceid { get; set; }
     
            public string description { get; set; }
   
            public int actid { get; set; }
     
            public string sections { get; set; }
    
            public string shortdescription { get; set; }
  
            public string penaltydescription { get; set; }
    
            public string referencematerialtext { get; set; }
          
            public string sampleformlink { get; set; }
  
            public string compliancesampleform { get; set; }
     
            public string compliancesampleformpath { get; set; }
            public int ismyfavourite { get; set; }
        }

        public class DailyUpdatesListParameter
        {
           
            [Text(Fielddata = true)]
            public int id { get; set; }

            [Text(Fielddata = true)]
            public int dailyupdateid { get; set; }

            [Text(Fielddata = true)]
            public string title { get; set; }
            [Text(Fielddata = true)]
            public string description { get; set; }

            [Text(Fielddata = true)]
            public int actid { get; set; }
            [Text(Fielddata = true)]
            public Nullable<int> compliancetypeid { get; set; }
            [Text(Fielddata = true)]
            public Nullable<int> compliancecategoryid { get; set; }
            public Nullable<int> stateid { get; set; }
            [Text(Fielddata = true)]
            public string actname { get; set; }
            [Text(Fielddata = true)]
            public string compliancetypename { get; set; }
            [Text(Fielddata = true)]
            public string compliancecategoryname { get; set; }
            [Text(Fielddata = true)]
            public string state { get; set; }
            [Text(Fielddata = true)]          
            public Nullable<System.DateTime> createddate { get; set; }
            public int ismyfavourite { get; set; }
            public long intdaliyupdatedate { get; set; }
        }

        public class NewsLetterListNew
        {
            public int ID { get; set; }
            [Text(Fielddata = true)]
            public string Title { get; set; }
            [Text(Fielddata = true)]
            public string Description { get; set; }
            public Nullable<System.DateTime> NewsDate { get; set; }
            public Nullable<int> CreatedBy { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public Nullable<int> UpdatedBy { get; set; }
            public Nullable<System.DateTime> UpdatedDate { get; set; }
            public Nullable<int> ApproveBy { get; set; }
            public Nullable<System.DateTime> ApproveDate { get; set; }
            public Nullable<bool> IsApproved { get; set; }
            public Nullable<bool> IsDeleted { get; set; }
            [Text(Fielddata = true)]
            public string FileName { get; set; }
            [Text(Fielddata = true)]
            public string FilePath { get; set; }
            [Text(Fielddata = true)]
            public string DocFileName { get; set; }
            [Text(Fielddata = true)]
            public string DocFilePath { get; set; }
            public long intnewsletterdate { get; set; }
        }
        public static List<classMangementUsers> GetUserDetailsStatutory(int Userid,int Customerid, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<classMangementUsers> UserDetails = new List<classMangementUsers>();

                if (approver == true)
                {
                    UserDetails = (from row in entities.ComplianceAssignedInstancesViews
                                   join row1 in entities.CustomerBranches
                                    on row.CustomerBranchID equals row1.ID
                                   join row2 in entities.Users
                                    on row.UserID equals row2.ID
                                   where row.CustomerID == Customerid
                                   && row.UserID == Userid
                                   && row.RoleID == 6
                                   && row2.IsDeleted == false
                                   select new classMangementUsers
                                   {
                                       ID = row2.ID,
                                       Name = row2.FirstName + " " + row2.LastName,
                                       Location = row1.Name,
                                       Email = row2.Email,
                                       ContactNo = row2.ContactNumber
                                   }).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();
                    
                }
                else
                {
                    UserDetails = (from row in entities.ComplianceAssignedInstancesViews
                                   join row1 in entities.EntitiesAssignments
                                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                   join row2 in entities.Users
                                    on row.UserID equals row2.ID
                                   join row3 in entities.CustomerBranches
                                   on row.CustomerBranchID equals row3.ID
                                   where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                   && row1.UserID == Userid
                                  && row2.IsDeleted == false
                                   select new classMangementUsers
                                   {
                                       ID = row2.ID,
                                       Name = row2.FirstName + " " + row2.LastName,
                                       Location = row3.Name,
                                       Email = row2.Email,
                                       ContactNo = row2.ContactNumber
                                   }).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();                   
                }
                return UserDetails.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<classMangementUsers> GetUserDetailsInternal(int Userid, int Customerid, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<classMangementUsers> UserDetails = new List<classMangementUsers>();

                if (approver == true)
                {
                    UserDetails = (from row in entities.InternalComplianceAssignedInstancesViews
                                   join row1 in entities.CustomerBranches
                                    on row.CustomerBranchID equals row1.ID
                                   join row2 in entities.Users
                                   on row.UserID equals row2.ID
                                   where row.CustomerID == Customerid
                                   && row.UserID == Userid
                                   && row.RoleID == 6
                                   && row2.IsDeleted == false
                                   select new classMangementUsers
                                   {
                                       ID = row2.ID,
                                       Name = row2.FirstName + " " + row2.LastName,
                                       Location = row1.Name,
                                       Email = row2.Email,
                                       ContactNo = row2.ContactNumber
                                   }).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();                    
                }
                else
                {
                    
                    UserDetails = (from row in entities.InternalComplianceAssignedInstancesViews
                                   join row1 in entities.EntitiesAssignmentInternals
                                    on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                   join row2 in entities.Users
                                    on row.UserID equals row2.ID
                                   join row3 in entities.CustomerBranches
                                 on row.CustomerBranchID equals row3.ID
                                   where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                   && row1.UserID == Userid
                                  && row2.IsDeleted == false
                                   select new classMangementUsers
                                   {
                                       ID = row2.ID,
                                       Name = row2.FirstName + " " + row2.LastName,
                                       Location = row3.Name,
                                       Email = row2.Email,
                                       ContactNo = row2.ContactNumber
                                   }).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();                    
                }
                return UserDetails.OrderBy(entry => entry.Name).ToList();
            }
        }
      
        public static List<classMangementFunctions> GetFunctionDetailsInternal(int Userid, int CustomerBranchId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<classMangementFunctions> complianceCategorys = new List<classMangementFunctions>();
                if (approver == true)
                {
                    if (CustomerBranchId != -1)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
                                               && row1.RoleID == 6
                                               //select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                                               select new classMangementFunctions
                                               {
                                                   ID = row.ID,
                                                   Name = row.Name,
                                               }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               && row1.RoleID == 6
                                               //select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                                               select new classMangementFunctions
                                               {
                                                   ID = row.ID,
                                                   Name = row.Name,
                                               }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
                    if (CustomerBranchId != -1)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid
                                                   select row).Distinct().ToList();
                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           //select row1).ToList();
                                           select new classMangementFunctions
                                           {
                                               ID = row1.ID,
                                               Name = row1.Name,
                                           }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static int GetCount(int CategoryID, String CountType, string satutoryinternal, bool IsApprover, int userID, int customerid,int CustBranchID)
        {
            try
            {               
                int Count = 0;
               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (satutoryinternal == "Statutory")
                    {
                        List<sp_ComplianceInstanceAssignmentViewCount_Result> TotalList = new List<sp_ComplianceInstanceAssignmentViewCount_Result>();
                        if (IsApprover == true)
                        {
                            TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount(userID, CategoryID, "APPR")
                                         select row).ToList();
                        }
                        else
                        {
                            TotalList = (from row in entities.sp_ComplianceInstanceAssignmentViewCount(userID, CategoryID, "MGMT")
                                         select row).ToList();
                        }
                        
                        if (CustBranchID != -1)
                            TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                        //if (Category != -1)
                        TotalList = TotalList.Where(Entry => Entry.ComplianceCategoryId == CategoryID).ToList();

                        if (CountType == "U")
                        {
                            //TotalList = TotalList.GroupBy(entry => entry.CustomerBranchID).Select(q => q.FirstOrDefault()).ToList();
                            Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();
                        }

                        if (CountType == "C")
                            Count = TotalList.Select(Entry => Entry.ComplianceId).Distinct().Count();

                        if (CountType == "L")
                            Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();

                    }
                    else if (satutoryinternal == "Internal")
                    {
                        List<InternalComplianceAssignedInstancesView> TotalList = new List<InternalComplianceAssignedInstancesView>();
                        if (IsApprover == true)
                        {
                            TotalList = (from row in entities.InternalComplianceAssignedInstancesViews
                                         join row1 in entities.CustomerBranches
                                          on row.CustomerBranchID equals row1.ID
                                         where row.CustomerID == customerid && row.RoleID == 6
                                         && row.UserID == userID
                                         select row).Distinct().ToList();
                        }
                        else
                        {
                            TotalList = (from row in entities.InternalComplianceAssignedInstancesViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                        }

                        if (CustBranchID != -1)
                            TotalList = TotalList.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                        //if (Category != -1)
                        TotalList = TotalList.Where(Entry => Entry.InternalComplianceCategoryID == CategoryID).ToList();

                        if (CountType == "U")
                        {
                            //TotalList = TotalList.GroupBy(entry => entry.CustomerBranchID).Select(q => q.FirstOrDefault()).ToList();
                            Count = TotalList.Select(Entry => Entry.UserID).Distinct().Count();
                        }
                        if (CountType == "C")
                            Count = TotalList.Select(Entry => Entry.InternalComplianceID).Distinct().Count();

                        if (CountType == "L")
                            Count = TotalList.Select(Entry => Entry.CustomerBranchID).Distinct().Count();

                    }
                    return Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
      
        public static List<classMangementFunctions> GetFunctionDetailsStatutory(int Userid, int CustomerBranchId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<classMangementFunctions> complianceCategorys = new List<classMangementFunctions>();
                if (approver == true)
                {
                    if (CustomerBranchId != -1)
                    {
                        complianceCategorys = (from row in entities.ComplianceCategories
                                               join row1 in entities.ComplianceAssignedInstancesViews
                                               on row.ID equals row1.ComplianceCategoryId
                                               where row1.UserID == Userid && row1.CustomerBranchID == CustomerBranchId
                                               && row1.RoleID == 6
                                               // select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                                               select new classMangementFunctions
                                               {
                                                   ID = row.ID,
                                                   Name = row.Name,
                                               }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.ComplianceCategories
                                               join row1 in entities.ComplianceAssignedInstancesViews
                                               on row.ID equals row1.ComplianceCategoryId
                                               where row1.UserID == Userid && row1.RoleID == 6
                                               //select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                                               select new classMangementFunctions
                                               {
                                                   ID = row.ID,
                                                   Name = row.Name,
                                               }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<ComplianceAssignedInstancesView> ComplianceCount = new List<ComplianceAssignedInstancesView>();
                    if (CustomerBranchId != -1)
                    {
                        ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                                           join row1 in entities.EntitiesAssignments
                                            on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                           where row.CustomerBranchID == row1.BranchID
                                           && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                           select row).Distinct().ToList();
                    }
                    else
                    {
                        ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                                           join row1 in entities.EntitiesAssignments
                                            on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                           where row.CustomerBranchID == row1.BranchID
                                           && row1.UserID == Userid
                                           select row).Distinct().ToList();
                    }
                    ComplianceCount = ComplianceCount.GroupBy(a => (int?)a.ComplianceCategoryId).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in ComplianceCount
                                           join row1 in entities.ComplianceCategories
                                           on row.ComplianceCategoryId equals row1.ID
                                           select new classMangementFunctions
                                           {
                                               ID = row1.ID,
                                               Name = row1.Name,
                                           }).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    //select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }

                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }

                        //year1 = (date.AddYears(-1)).ToString("yy");

                    }
                    if (forMonth == 10)
                    {
                        // year1 = (date.AddYears(-1)).ToString("yy");
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }

                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    }

                    if (forMonth == 1)
                    {
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year2 = (date.AddYears(-1)).ToString("yy");
                        }
                        else
                        {
                            year2 = (date).ToString("yy");
                        }

                    }
                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
        public static void SetReadToNotification(List<long> Notifications, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var eachNotification in Notifications)
                {
                    var NotificationDetails = (from row in entities.UserNotifications
                                               where row.NotificationID == eachNotification
                                               && row.UserID == userid
                                               select row).FirstOrDefault();

                    if (NotificationDetails != null)
                    {
                        NotificationDetails.IsRead = true;
                    }
                }

                entities.SaveChanges();
            }
        }
        public static List<SP_GetUserNotificationsByUserID_Result> GetAllUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.SP_GetUserNotificationsByUserID(Convert.ToInt32(UserID))
                                         select row).ToList();
                if (UserNotifications.Count>0)
                {
                    UserNotifications = UserNotifications.GroupBy(entity => entity.UpdatedOn).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return UserNotifications;
            }
        }

        public static List<SP_GetUserNotificationsByUserID_Result> GetAllUserNotificationNew(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.SP_GetUserNotificationsByUserID(Convert.ToInt32(UserID))
                                         select row).OrderByDescending(x => x.ID).ToList();

                return UserNotifications;
            }
        }
        public static ComplianceInstanceTransactionView GetInstanceTransactionCompliance(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.ScheduledOnID == ScheduledOnID && row.RoleID == 3
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).FirstOrDefault();

                return ReviceCompliances;
            }
        }
        public static ComplianceForm GetComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceForms
                            where row.ComplianceID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static List<CheckListInstanceTransactionView> GetMappedComplianceCheckListRahul(long UserID, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<CheckListInstanceTransactionView> transactionsQuery = new List<CheckListInstanceTransactionView>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();


                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.ScheduledOn == now || row.ScheduledOn <= now)
                                         && row.ComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                return transactionsQuery;
            }
        }
        public static List<CheckListInstanceTransactionReviewerView> GetMappedComplianceCheckListReviewer(long UserID, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<CheckListInstanceTransactionReviewerView> transactionsQuery = new List<CheckListInstanceTransactionReviewerView>();
               
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                     where row.UserID == UserID && row.RoleID == performerRoleID                                      
                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                }
               
                return transactionsQuery;
            }
        }
        public static Compliance CheckMonetary(int complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.Compliances
                            where row.ID == complianceid
                            && row.NonComplianceType != null
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static RecentComplianceTransactionView GetCurrentStatusByComplianceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.RecentComplianceTransactionViews
                                       where row.ComplianceScheduleOnID == ScheduledOnID
                                       select row).First();

                return currentStatusID;
            }
        }
        public static Compliance GetComplianceByInstanceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ScheduledOnID
                                             select row.ComplianceInstanceId).FirstOrDefault();

                long complianceID = (from row in entities.ComplianceInstances
                                     where row.ID == complianceInstanceID && row.IsDeleted == false
                                     select row.ComplianceId).FirstOrDefault();

                var compliance = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        public static List<ComplianceView> GetCompliance(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ScheduledOnID
                                             select row.ComplianceInstanceId).FirstOrDefault();

                long complianceID = (from row in entities.ComplianceInstances
                                     where row.ID == complianceInstanceID && row.IsDeleted == false
                                     select row.ComplianceId).FirstOrDefault();

                var complianceInfo = (from row in entities.ComplianceViews
                                      where row.ID == complianceID
                                  select row).ToList();

                return complianceInfo;
            }
        }
        public static void UpdatePenaltyTransactionByReviewer(ComplianceTransaction complianceTransaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceInstanceId == complianceTransaction.ComplianceInstanceId
                                                     && row.ComplianceScheduleOnID == complianceTransaction.ComplianceScheduleOnID
                                                     && (row.StatusId == 2 || row.StatusId == 3)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();
                if (Transaction != null)
                {
                    Transaction.IsPenaltySave = complianceTransaction.IsPenaltySave;
                    Transaction.PenaltySubmit = complianceTransaction.PenaltySubmit;
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdatePenaltyTransactionByReviewerExist(ComplianceTransaction complianceTransaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceInstanceId == complianceTransaction.ComplianceInstanceId
                                                     && row.ComplianceScheduleOnID == complianceTransaction.ComplianceScheduleOnID
                                                     && row.StatusId == complianceTransaction.StatusId
                                                     select row).FirstOrDefault();
                if (Transaction != null)
                {
                    Transaction.StatusChangedOn = complianceTransaction.StatusChangedOn;
                    Transaction.IsPenaltySave = complianceTransaction.IsPenaltySave;
                    Transaction.PenaltySubmit = complianceTransaction.PenaltySubmit;
                    Transaction.Remarks = complianceTransaction.Remarks;
                    Transaction.Interest = complianceTransaction.Interest;
                    Transaction.Penalty = complianceTransaction.Penalty;
                    Transaction.ComplianceSubTypeID = complianceTransaction.ComplianceSubTypeID;
                    Transaction.ValuesAsPerSystem = complianceTransaction.ValuesAsPerSystem;
                    Transaction.ValuesAsPerReturn = complianceTransaction.ValuesAsPerReturn;
                    Transaction.LiabilityPaid = complianceTransaction.LiabilityPaid;
                    entities.SaveChanges();           
                }
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }


        public static List<InternalComplianceAssignedInstancesView> GetInternalComplianceDetailsDashboard(int customerid, int RoleID, int CustomerBranchID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceAssignedInstancesView> ComplianceDetails = new List<InternalComplianceAssignedInstancesView>();
                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.InternalComplianceAssignedInstancesViews
                                         join row1 in entities.CustomerBranches
                                          on row.CustomerBranchID equals row1.ID
                                         where row.CustomerID == customerid && row.RoleID == 6
                                         && row.UserID == Userid
                                         select row).Distinct().ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.InternalComplianceAssignedInstancesViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == Userid && row.RoleID == RoleID
                                         select row).Distinct().ToList();
                }
               
                if (CustomerBranchID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
                }

                //if (CategoryID != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.InternalComplianceCategoryID == CategoryID).Distinct().ToList();
                //}
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<ComplianceAssignedInstancesView> GetComplianceDetailsDashboard(int customerid, int RoleID, int CustomerBranchID, int CategoryID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceAssignedInstancesView> ComplianceDetails = new List<ComplianceAssignedInstancesView>();
                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row1 in entities.CustomerBranches
                                          on row.CustomerBranchID equals row1.ID
                                         where row.CustomerID == customerid && row.RoleID == 6
                                         && row.UserID == Userid
                                         select row).Distinct().ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row1 in entities.EntitiesAssignments
                                          on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == Userid && row.RoleID == RoleID
                                         select row).Distinct().ToList();
                }
               
                if (CustomerBranchID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
                }
                //if (ActId != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                //}
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }


        public static List<DashboardDataDisplayClass> GetPenaltyComplianceDetailsDashboard(int customerid, int CustomerBranchID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceInstanceTransactionViews
                                         join acte in entities.Acts
                                         on row.ActID equals acte.ID
                                         join row2 in entities.EntitiesAssignments
                                         on row.CustomerBranchID equals row2.BranchID
                                         join user in entities.Users
                                         on row.UserID equals user.ID
                                         where row.CustomerID == customerid
                                         && row.ComplianceCategoryId == row2.ComplianceCatagoryID
                                         //&& row.ScheduledOn >= startDate
                                         //&& row.ScheduledOn <= EndDate 
                                         && row.Penalty != null && row.Interest != null
                                         && row.PenaltySubmit == "S" && row.IsPenaltySave == false && row.RoleID == 4
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         //&& row.NonComplianceType !=null
                                         && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         select new DashboardDataDisplayClass()
                                         {
                                             ActID = row.ActID,
                                             ActName = acte.Name,
                                             ShortDescription = row.ShortDescription,
                                             CustomerBranchID = row.CustomerBranchID,
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             ComplianceID = row.ComplianceID,
                                             ComplianceCategoryId = acte.ComplianceCategoryId,
                                             // ComplianceStatusID = row.ComplianceStatusID,
                                             ScheduledOn = row.ScheduledOn,
                                             Risk = row.Risk,
                                             ScheduledOnID = row.ScheduledOnID,
                                             RoleID = row.RoleID,
                                             User = user.FirstName + " " + user.LastName,
                                             Penalty = row.Penalty,
                                             Interest = row.Interest,
                                             PenaltyStatus = row.PenaltySubmit,
                                         }).ToList();

                ComplianceDetails = ComplianceDetails.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                //ComplianceDetails= ComplianceDetails.Where(row=>row.NonComplianceType != 1).ToList();

                //if (risk == "High")
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                //}
                //if (risk == "Medium")
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                //}
                //if (risk == "Low")
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                //}
                if (CustomerBranchID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
                }
                if (CustomerBranchID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
                }
                if (CustomerBranchID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
                }
                //if (ActId != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                //}
                //if (CategoryID != -1)
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                //}

                //if (startdate.ToString() != "" && enddate.ToString() != "" && startdate.ToString() != "1/1/0001 12:00:00 AM" && enddate.ToString() != "1/1/0001 12:00:00 AM")
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                //}


                //if (PenaltyStatus == 0)
                //{
                    ComplianceDetails = ComplianceDetails.Where(row => row.PenaltyStatus == "S").Distinct().ToList();
                //}
                //else
                //{
                //    ComplianceDetails = ComplianceDetails.Where(row => row.PenaltyStatus == "P").Distinct().ToList();
                //}
                //ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<RecentApproverTransactionView> GetApproverAssignedCompliances(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var transactionList = (from row in entities.RecentApproverTransactionViews
                                       where row.UserID == userID  //&& row.ScheduledOn <= curruntDate
                                       select row).ToList();

                return transactionList;
            }
        }

        public static List<RecentApproverInternalTransactionView> GetApproverAssignedInternalCompliances(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var transactionList = (from row in entities.RecentApproverInternalTransactionViews
                                       where row.UserID == userID  //&& row.ScheduledOn <= curruntDate
                                       select row).ToList();

                return transactionList;
            }
        }

        public static tbl_ChecklistRemark GetChecklistRemark(long ChecklistRemarkID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.tbl_ChecklistRemark
                            where row.ID == ChecklistRemarkID
                            select row).FirstOrDefault();

                return form;
            }
        }

        public static bool deleteChecklistRemark(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean deleteSuccess = false;
                var data = (from row in entities.tbl_ChecklistRemark
                            where row.ID == ID
                            select row).FirstOrDefault();

                if (data != null)
                {
                    data.IsActive = false;
                    entities.SaveChanges();
                    deleteSuccess = true;
                }
                else
                {
                    deleteSuccess = false;
                }



                return deleteSuccess;
            }
        }

    }
}