﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class LicenseMgmt
    {
        public static List<Lic_SP_MyWorkspaceDetail_Result> GetAllLicenseDetails(int customerID, int UserID, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string IsPERMGMTCA, string Is_StatutoryInternal, string tbxtypeTofilter = null)
        {
            List<int> statusId = new List<int>();
            statusId.Add(4);
            statusId.Add(5);
            statusId.Add(7);
            statusId.Add(9);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_SP_MyWorkspaceDetail(UserID, customerID, IsPERMGMTCA, Is_StatutoryInternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (IsPERMGMTCA == "MGMT" || IsPERMGMTCA == "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (IsPERMGMTCA == "MGMT" || IsPERMGMTCA == "CADMN")
                    {
                        //List<int> cstatusId = new List<int>();
                        //cstatusId.Add(1);
                        //cstatusId.Add(2);
                        //cstatusId.Add(3);
                        //cstatusId.Add(6);
                        //cstatusId.Add(8);
                        //cstatusId.Add(10);

                        if (!string.IsNullOrEmpty(licenseStatus))
                        {
                            if (licenseStatus == "Applied")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus
                                && statusId.Contains((int)row.ComplianceStatusID)).ToList();

                            }
                            else if (licenseStatus == "Active")
                            {
                                //query = query.Where(row => row.MGRStatus == licenseStatus                                
                                //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "Expiring")
                            {
                                //query = query.Where(row => row.MGRStatus == licenseStatus
                                //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "Expired")
                            {
                                //query = query.Where(row => row.MGRStatus.Trim() == licenseStatus.Trim()
                                //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "Rejected")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "Terminate")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "PR")
                            {
                                //query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                                query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                                //query = query.Where(row => !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Registered")
                            {
                                query = query.Where(row => row.StatusID == 9).ToList();
                            }
                            else if (licenseStatus == "RRF")
                            {
                                query = query.Where(row => row.StatusID == 10).ToList();
                            }
                            else if (licenseStatus == "VE")
                            {
                                query = query.Where(row => row.StatusID == 11).ToList();
                            }
                            else
                            {
                               // query = query.Where(entry => entry.MGRStatus == licenseStatus).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(licenseStatus))
                        {
                            if (licenseStatus == "Applied")
                            {
                                query = query.Where(row => row.Status == licenseStatus && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Active")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expiring")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expired")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Rejected")
                            {
                                query = query.Where(row => row.Status == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "Terminate")
                            {
                                query = query.Where(row => row.Status == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "PR")
                            {
                                query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                                //statusId.Add(7);
                                //statusId.Add(9);
                                //query = query.Where(row => !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Registered")
                            {
                                query = query.Where(row => row.StatusID == 9).ToList();
                            }
                            else if (licenseStatus == "RRF")
                            {
                                query = query.Where(row => row.StatusID == 10).ToList();
                            }
                            else if (licenseStatus == "VE")
                            {
                                query = query.Where(row => row.StatusID == 11).ToList();
                            }
                            else
                            {
                               // query = query.Where(entry => entry.Status == licenseStatus).ToList();
                            }
                        }
                    }
                    //comment by rahul on 24 OCT 2020
                    //if (licenseStatus == "" || licenseStatus == "-1")
                    //{
                    //    query = query.Where(row => (row.Status == "Applied" || !statusId.Contains((int)row.ComplianceStatusID))).ToList();
                    //}

                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                   
                }
                return query.ToList();
            }
        }

        public static List<long> GetLicenseScheduleonIdList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_LicenseInstance
                             on row.LicenseID equals row1.ID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }
        public static List<SP_LicenseMyReport_Result> GetAllReportData(int customerID, int loggedInUserID, string loggedInUserRole, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string isstatutoryinternal) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                statusId.Add(7);
                statusId.Add(9);
                statusId.Add(12);
                var query = (from row in entities.SP_LicenseMyReport(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (loggedInUserRole == "MGMT" || loggedInUserRole == "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (!string.IsNullOrEmpty(licenseStatus))
                    {

                        if (licenseStatus == "Applied")
                        {
                            query = query.Where(row => row.Status == licenseStatus
                                && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                        }
                        else if (licenseStatus == "Active")
                        {
                            query = query.Where(row => row.Status == licenseStatus).ToList();
                        }
                        else if (licenseStatus == "Expiring")
                        {
                            query = query.Where(row => row.Status == licenseStatus).ToList();
                        }
                        else if (licenseStatus == "Expired")
                        {
                            query = query.Where(row => row.Status == licenseStatus).ToList();
                        }
                        else if (licenseStatus == "Rejected")
                        {
                            query = query.Where(row => row.Status == licenseStatus).ToList();
                        }
                        else if (licenseStatus == "PR")
                        {
                            query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                            query = query.Where(row => !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                        }
                        else if (licenseStatus == "Terminate")
                        {
                            query = query.Where(row => row.Status == licenseStatus).ToList();
                        }
                        else
                        {
                            query = query.Where(entry => entry.Status == licenseStatus).ToList();
                        }
                    }
                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                }

                return query;
            }
        }
        //public static List<SP_LicenseMyReport_Result> GetAllReportData(int customerID, int loggedInUserID, string loggedInUserRole, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string isstatutoryinternal) /*int branchID,*/
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<int> statusId = new List<int>();
        //        statusId.Add(4);
        //        statusId.Add(5);
        //        statusId.Add(7);
        //        statusId.Add(9);
        //        var query = (from row in entities.SP_LicenseMyReport(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
        //                     select row).ToList();

        //        if (query.Count > 0)
        //        {
        //            if (loggedInUserRole == "MGMT" || loggedInUserRole == "CADMN")
        //            {
        //                query = query.Where(entry => entry.RoleID == 3).ToList();
        //            }
        //            if (branchList.Count > 0)
        //                query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //            if (deptID != -1)
        //                query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //            if (!string.IsNullOrEmpty(licenseStatus))
        //            {

        //                if (licenseStatus == "Applied")
        //                {
        //                    query = query.Where(row => row.Status == licenseStatus
        //                        && statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                }
        //                else if (licenseStatus == "Active")
        //                {
        //                    query = query.Where(row => row.Status == licenseStatus).ToList();
        //                }
        //                else if (licenseStatus == "Expiring")
        //                {
        //                    query = query.Where(row => row.Status == licenseStatus).ToList();
        //                }
        //                else if (licenseStatus == "Expired")
        //                {
        //                    query = query.Where(row => row.Status == licenseStatus).ToList();
        //                }
        //                else if (licenseStatus == "Rejected")
        //                {
        //                    query = query.Where(row => row.Status == licenseStatus).ToList();
        //                }
        //                else if (licenseStatus == "PR")
        //                {
        //                    query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
        //                    query = query.Where(row => !statusId.Contains((int)row.ComplianceStatusID)).ToList();
        //                }
        //                else
        //                {
        //                    query = query.Where(entry => entry.Status == licenseStatus).ToList();
        //                }
        //            }
        //            if (licenseTypeID != -1 && licenseTypeID != 0)
        //                query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
        //        }

        //        return query;
        //    }
        //}

        //10/12/2021
        public static List<SP_License_UserList_Result> GetAllLicenseUserList(int customerID, int loggedInUserID, string loggedInUserRole, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string isstatutoryinternal) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //List<int> statusId = new List<int>();
                //statusId.Add(4);
                //statusId.Add(5);
                //statusId.Add(7);
                //statusId.Add(9);
                //statusId.Add(12);
                var query = (from row in entities.SP_License_UserList(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    //if (loggedInUserRole == "MGMT" || loggedInUserRole == "CADMN")
                    //{
                    //    query = query.Where(entry => entry.RoleID == 3).ToList();
                    //}
                    //if (branchList.Count > 0)
                    //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    //if (deptID != -1)
                    //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    //if (!string.IsNullOrEmpty(licenseStatus))
                    //{

                    //    if (licenseStatus == "Applied")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus
                    //            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                    //    }
                    //    else if (licenseStatus == "Active")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus).ToList();
                    //    }
                    //    else if (licenseStatus == "Expiring")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus).ToList();
                    //    }
                    //    else if (licenseStatus == "Expired")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus).ToList();
                    //    }
                    //    else if (licenseStatus == "Rejected")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus).ToList();
                    //    }
                    //    else if (licenseStatus == "PR")
                    //    {
                    //        query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                    //        query = query.Where(row => !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                    //    }
                    //    else if (licenseStatus == "Terminate")
                    //    {
                    //        query = query.Where(row => row.Status == licenseStatus).ToList();
                    //    }
                    //    else
                    //    {
                    //        query = query.Where(entry => entry.Status == licenseStatus).ToList();
                    //    }
                    //}
                    //if (licenseTypeID != -1 && licenseTypeID != 0)
                    //    query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                }

                return query;
            }
        }

        public static List<SP_License_DeptList_Result> GetAllLicenseDeptList(int customerID, int loggedInUserID, string loggedInUserRole, int deptID, string licenseStatus, long licenseTypeID, string isstatutoryinternal) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_License_DeptList(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
                             select row).ToList();
                return query;
            }
        }
        //10/12/2021
        public static List<long> GetLicenseScheduleonIdList(long customerID,long UserID,long RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_LicenseInstance
                             on row.LicenseID equals row1.ID
                             join row2 in entities.ComplianceAssignments
                             on row.ComplianceInstanceID equals row2.ComplianceInstanceID
                             where row1.CustomerID == customerID
                             && row2.UserID == UserID
                             && row2.RoleID == RoleID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }
       
    }
}