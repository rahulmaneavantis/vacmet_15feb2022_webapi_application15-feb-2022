﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CheckListDetails
    {
        public List<CheckListInstanceTransactionView> PerformerStatustory { get; set; }
        public List<CheckListInstanceTransactionReviewerView> ReviwerStatustory { get; set; }
        public List<AssignLocationList> AssignLocationListObj { get; set; }
        public List<InternalComplianceInstanceCheckListTransactionView> PerformerInternal { get; set; }
        public List<InternalComplianceInstanceCheckListTransactionView> ReviwerInternal { get; set; }

    }
}