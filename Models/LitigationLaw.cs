﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models
{
    public class LitigationLaw
    {
        #region Amol
        public static bool DeleteLCDetailData(int ID, long CustomerID)
        {
            bool output = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_PartyDetail Query = (from row in entities.tbl_PartyDetail
                                             where row.ID == ID && row.CustomerID == CustomerID
                                             select row).FirstOrDefault();
                    if (Query != null)
                    {
                        entities.tbl_PartyDetail.Remove(Query);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                output = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return output;
        }

        public static List<tbl_PartyDetail> GetLCPartyDetails(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_PartyDetail
                             where row.CustomerID == CustomerID
                             select row).OrderBy(entry => entry.Name);
                return Query.ToList();
            }
        }
        #endregion
    }
}