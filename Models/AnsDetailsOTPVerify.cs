﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AnsDetailsOTPVerify
    {
        public string StatusCheck { get; set; }
        public string Msg { get; set; }
        public List<int> RoleID { get; set; }
        public string PageName { get; set; }
        public int LitigationRoleID { get; set; }
        public int SecretarialRoleID { get; set; }
        
        public List<int> ContractRoleID { get; set; }
        public List<int> OtherRoleID { get; set; }
    }
}