﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class ContractManagement
    {
        public static List<Cont_SP_GetReviwedContracts_All_Result> GetAssignedContractsTemplateList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetReviwedContracts_All(customerID, loggedInUserID, roleID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.CreatedBy == loggedInUserID)).ToList();
                    //else // In case of MGMT or CADMN 
                    //{
                    //    query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    //}
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ID, //ContractID
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.ContractTypeID,
                                 g.TypeName,
                                 g.ContractSubTypeID,
                                 g.SubTypeName,
                                 g.ProposalDate,
                                 g.AgreementDate,
                                 g.EffectiveDate,
                                 g.ReviewDate,
                                 g.ExpirationDate,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.StatusName,
                                 g.ContractAmt,
                                 g.ContactPersonOfDepartment,
                                 g.PaymentType,
                                 g.AddNewClause,
                                 g.Owner
                             } into GCS
                             select new Cont_SP_GetReviwedContracts_All_Result()
                             {
                                 ID = GCS.Key.ID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 TypeName = GCS.Key.TypeName,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 SubTypeName = GCS.Key.SubTypeName,
                                 ProposalDate = GCS.Key.ProposalDate,
                                 AgreementDate = GCS.Key.AgreementDate,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ReviewDate = GCS.Key.ReviewDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 StatusName = GCS.Key.StatusName,
                                 ContractAmt = GCS.Key.ContractAmt,
                                 ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                 PaymentType = GCS.Key.PaymentType,
                                 AddNewClause = GCS.Key.AddNewClause,
                                 Owner = GCS.Key.Owner
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }

        public static string getBranchName(long custbranchid)
        {
            string queryResult = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    queryResult = (from row in entities.CustomerBranches
                                   where row.IsDeleted == false
                                   && row.ID == custbranchid
                                   select row.Name).FirstOrDefault();

                    return queryResult;
                }
            }
            catch (Exception ex)
            {
                return queryResult;
            }
        }
        public static List<Cont_tbl_TypeMaster> GetContractTypes_All(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractType = (from row in entities.Cont_tbl_TypeMaster
                                       where row.TypeName != null
                                       && row.IsDeleted == false
                                       && row.CustomerID == customerID
                                       select row).ToList();
                return lstContractType;
            }
        }
        public static string GetContractUserAssignment_All(long contractID, int CustID)
        {
            string queryResult = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Result = (from row in entities.Cont_tbl_UserAssignment
                                  join row1 in entities.Users
                                  on row.UserID equals row1.ID
                                  where row.ContractID == contractID
                                      && row.IsActive == true
                                      && row1.IsDeleted == false
                                      && row1.CustomerID == CustID
                                      && row1.ContractRoleID != null
                                  select row1).FirstOrDefault();

                    if (Result != null)
                    {
                        queryResult = Result.FirstName + " " + Result.LastName;
                    }

                    return queryResult;
                }
            }
            catch (Exception ex)
            {
                return queryResult;
            }
        }
        public static List<Cont_SP_GetAssignedContracts_All_Result> GetAssignedContractsList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetAssignedContracts_All(customerID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.CreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ID, //ContractID
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.ContractTypeID,
                                 g.TypeName,
                                 g.ContractSubTypeID,
                                 g.SubTypeName,
                                 g.ProposalDate,
                                 g.AgreementDate,
                                 g.EffectiveDate,
                                 g.ReviewDate,
                                 g.ExpirationDate,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.StatusName,
                                 g.ContractAmt,
                                 g.ContactPersonOfDepartment,
                                 g.PaymentType,
                                 g.AddNewClause,
                                 g.Owner,
                                 //g.ProductItems,
                                 //g.Product_Specification,
                                 //g.Rate_per_product,
                                 //g.GST,
                                 //g.PaymentTerm,
                                 //g.Delivery_Installation_Period,
                                 //g.Penalty,
                                 //g.Product_Warranty
                             } into GCS
                             select new Cont_SP_GetAssignedContracts_All_Result()
                             {
                                 ID = GCS.Key.ID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 TypeName = GCS.Key.TypeName,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 SubTypeName = GCS.Key.SubTypeName,
                                 ProposalDate = GCS.Key.ProposalDate,
                                 AgreementDate = GCS.Key.AgreementDate,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ReviewDate = GCS.Key.ReviewDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 StatusName = GCS.Key.StatusName,
                                 ContractAmt = GCS.Key.ContractAmt,
                                 ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                 PaymentType = GCS.Key.PaymentType,
                                 AddNewClause = GCS.Key.AddNewClause,
                                 Owner = GCS.Key.Owner,
                                 //ProductItems = GCS.Key.ProductItems,
                                 //Product_Specification = GCS.Key.Product_Specification,
                                 //Rate_per_product = GCS.Key.Rate_per_product,
                                 //GST = GCS.Key.GST,
                                 //PaymentTerm = GCS.Key.PaymentTerm,
                                 //Delivery_Installation_Period = GCS.Key.Delivery_Installation_Period,
                                 //Penalty = GCS.Key.Penalty,
                                 //Product_Warranty = GCS.Key.Product_Warranty
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }

    }
}