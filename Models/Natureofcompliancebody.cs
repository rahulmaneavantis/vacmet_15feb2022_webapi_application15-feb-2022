﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class RiskDetail
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    public class Natureofcompliancebody
    {
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("ScheduledOnID")]
        public string ScheduledOnID { get; set; }
    }
}