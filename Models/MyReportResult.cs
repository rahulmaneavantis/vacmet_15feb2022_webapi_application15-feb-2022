﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class OverDueList
    {
        public List<SP_GetOverdueDeptHead_Result> StatutoryList { get; set; }
        public List<SP_GetInternalOverdueDeptHead_Result> InternalList { get; set; }
    }
    public class MyReportResult
    {
        public List<SP_Kendo_GetALLStatutoryEBData_Result> StatustoryEB { get; set; }
        //public List<InternalComplianceInstanceTransactionViewNew> Internal { get; set; }
        public List<SP_Kendo_GetALLInternalEBData_Result> Internal { get; set; }
        
        //public List<InternalComplianceInstanceCheckListTransactionViewNew> InternalChecklist { get; set; }
        public List<SP_kendo_getInternalChecklist_Result> InternalChecklist { get; set; }        
        public List<SP_kendo_getSChecklist_Result> StatChecklist { get; set; }
        public int TotalCount { get; set; }
    }

    public class MyReportResultNew
    {
        public List<SP_Kendo_GetALLStatutoryEBData_Result> StatustoryEB { get; set; }
        //public List<SP_GetMyReportData_Result> StatustoryEB { get; set; }
        public List<InternalComplianceInstanceTransactionViewNew> Internal { get; set; }
        public List<InternalComplianceInstanceCheckListTransactionViewNew> InternalChecklist { get; set; }
        public List<SP_kendo_getSChecklist_Result> StatChecklist { get; set; }
    }
}