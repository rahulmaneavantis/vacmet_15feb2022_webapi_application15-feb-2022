﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    //public class getAllData
    //{
    //    public Nullable<long> ComplianceId { get; set; }
    //    public int ComplianceReminderID { get; set; }
    //    public int ComplianceAssignmentID { get; set; }
    //    public Nullable<int> RoleID { get; set; }
    //    public Nullable<long> ComplianceInstanceID { get; set; }
    //    public System.DateTime ScheduledOn { get; set; }
    //    public System.DateTime RemindOn { get; set; }
    //    public Nullable<byte> Status { get; set; }
    //    public string Description { get; set; }
    //    public string ShortDescription { get; set; }
    //    public string CustomerBranchName { get; set; }
    //    public Nullable<int> CustomerBranchID { get; set; }
    //    public string Role { get; set; }
    //    public Nullable<long> UserID { get; set; }
    //    public string UserName { get; set; }
    //    public Nullable<byte> RiskType { get; set; }
    //    public string Type { get; set; }
    //}
    public class EscalationClass
    {

        public string Frequency { get; set; }
        public long complianceID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public int? DueDate { get; set; }
        public int? IntreimDays { get; set; }
        public int? EscDays { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Boolean? EventFlag { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string User { get; set; }
        public int RiskType { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
        public string ReportType { get; set; }
    }

    public class getAllData
    {
        public Nullable<long> ComplianceId { get; set; }
        public int ComplianceReminderID { get; set; }
        public int ComplianceAssignmentID { get; set; }
        public Nullable<int> RoleID { get; set; }
        public Nullable<long> ComplianceInstanceID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public System.DateTime RemindOn { get; set; }
        public Nullable<byte> Status { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerBranchName { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public string Role { get; set; }
        public Nullable<long> UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<byte> RiskType { get; set; }
        public string Type { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
    }
    //public class EscalationClass
    //{

    //    public string Frequency { get; set; }
    //    public long complianceID { get; set; }
    //    public string Branch { get; set; }
    //    public string ShortDescription { get; set; }
    //    public int? DueDate { get; set; }
    //    public int? IntreimDays { get; set; }
    //    public int? EscDays { get; set; }
    //    public Nullable<int> CustomerBranchID { get; set; }
    //    public Boolean? EventFlag { get; set; }
    //    public long ActID { get; set; }
    //    public string ActName { get; set; }
    //    public string User { get; set; }
    //    public int RiskType { get; set; }
    //}
    public class ActCountData
    {
        public int CId { get; set; }
        public string CategoryName { get; set; }
        public int State { get; set; }
        public int Central { get; set; }
    }
}