﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CertificateUser
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
    public class CertificatePeriod
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class GetAllNoticeDocuments
    {
        public string Email { get; set; }
        public long NoticeInstanceID { get; set; }

        public string Flag { get; set; }
    }
    public class ErrorDetails
    {
        public string ErrorMessage { get; set; }
        public string ErrorDetailMessage { get; set; }
    }

    public class DetailLockingDay
    {
        public long ID { get; set; }
        public long ScheduleOnID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public System.DateTime DueDate { get; set; }
        public int DueDay { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
    }
}