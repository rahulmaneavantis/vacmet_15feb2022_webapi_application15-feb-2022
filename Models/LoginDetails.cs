﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ResendOTP
    {
        public string status { get; set; }
    }

    public class LoginDetails
    {
        public string status { get; set; }
        public string msg { get; set; }
        public string tokendetail { get; set; }
        public string CurrentEmailName { get; set; }
        public string Notificationstatus { get; set; }
        public string UserEmail { get; set; }
        public string UserContact { get; set; }

    }
}