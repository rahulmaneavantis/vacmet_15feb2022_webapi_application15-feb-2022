﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ManagmentReminddemo
    {
        public string IsChecktrue { get; set; }
        public string Frequency { get; set; }
        public long complianceID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public int? DueDate { get; set; }
        public string RemindFlag { get; set; }
        public string UserFlag { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Boolean? EventFlag { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string User { get; set; }
        public int RiskType { get; set; }
        public string PerformerName { get; set; }
        public string ReviewerName { get; set; }
    }
    //public class ManagmentReminddemo
    //{
    //    public string IsChecktrue { get; set; }
    //    public string Frequency { get; set; }
    //    public long complianceID { get; set; }
    //    public string Branch { get; set; }
    //    public string ShortDescription { get; set; }
    //    public int? DueDate { get; set; }
    //    public string RemindFlag { get; set; }
    //    public string UserFlag { get; set; }
    //    public Nullable<int> CustomerBranchID { get; set; }
    //    public Boolean? EventFlag { get; set; }
    //    public long ActID { get; set; }
    //    public string ActName { get; set; }
    //    public string User { get; set; }
    //    public int RiskType { get; set; }
    //}

    public class ManagmentRemindClass
    {
        public string Frequency { get; set; }
        public long complianceID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public int? DueDate { get; set; }
        public string RemindFlag { get; set; }
        public string UserFlag { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Boolean? EventFlag { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string User { get; set; }
        public int RiskType { get; set; }
    }
    public class Comments3
    {
        public string _id { get; set; }
        public int contractID { get; set; }
        public int taskID { get; set; }
        public int user_id { get; set; }
        public int customer_id { get; set; }
        public string comment { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime deleted_at { get; set; }
        public string message_id { get; set; }
        public string user_name { get; set; }
    }

    public class Check
    {
        public int ID { get; set; }
        public string Comment { get; set; }
        public int UserID { get; set; }
        public string time { get; set; }
        public int msgId { get; set; }
        //public string Flag { get; set; }
    }
    public class Comments
    {
        public string _id { get; set; }
        public int compliance_id { get; set; }

        public int user_id { get; set; }
        public int customer_id { get; set; }

        public string comment { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime deleted_at { get; set; }
        public string message_id { get; set; }
        public string recipient { get; set; }

        public string user_name { get; set; }
        public string compliance_type { get; set; }
        public int CustomerBranchID { get; set; }
    }

    public class Comments1
    {
        public string _id { get; set; }
        public int compliance_id { get; set; }

        public int user_id { get; set; }
        public int customer_id { get; set; }

        public string comment { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime deleted_at { get; set; }
        public string message_id { get; set; }
        public string recipient { get; set; }

        public string user_name { get; set; }
        public string compliance_type { get; set; }
        public int CustomerBranchID { get; set; }
    }

    public class Comments2
    {
        public string _id { get; set; }
        public int compliance_id { get; set; }

        public int user_id { get; set; }
        public int customer_id { get; set; }

        public string comment { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string deleted_at { get; set; }
        public string message_id { get; set; }
        public string recipient { get; set; }

        public string user_name { get; set; }
        public string compliance_type { get; set; }
        public int CustomerBranchID { get; set; }
    }
    public class ComplianceInfo
    {

    }

    public class DashboardDataDisplayClass
    {
        public string ShortDescription { get; set; }
        public string ActName { get; set; }
        public string PenaltyStatus { get; set; }
        public long ActID { get; set; }
        public int CustomerBranchID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public Nullable<int> ComplianceStatusID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public Nullable<byte> Risk { get; set; }
        public long ScheduledOnID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string User { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public Nullable<decimal> Penalty { get; set; }
        public Nullable<decimal> Interest { get; set; }
        public Nullable<decimal> totalvalue { get; set; }
    }

}