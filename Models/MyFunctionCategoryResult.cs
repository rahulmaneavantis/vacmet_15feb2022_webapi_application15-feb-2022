﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class MyFunctionCategoryResult
    {
        public List<sp_ComplianceAssignedCategory_Result> Statustory { get; set; }
        public List<InternalCompliancesCategory> Internal { get; set; }
    }
}