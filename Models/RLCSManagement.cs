﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Compression;
using System.Linq;
using System.Web;
using Ionic.Zip;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AppWebApplication.Models
{
    public class PushNotofication
    {
        public string ProfileID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
    


    public class API_AuthKey_Response
    {
        public string ProfileId { get; set; }
        public string AuthKey { get; set; }
    }

    public class SetPinUserName
    {
        public string UserName { get; set; }
    }

    public class SetRLCSPinToken
    {
        public string status { get; set; }
        public string setAccessToken { get; set; }
    }

    public class GetCompliancesSummary
    {
        public int NotCompleted { get; set; }
        public int AfterDueDate { get; set; }
        public int InTime { get; set; }

        public int NotCompletedHigh { get; set; }
        public int AfterDueDateHigh { get; set; }
        public int InTimeHigh { get; set; }

        public int NotCompletedMedium { get; set; }
        public int AfterDueDateMedium { get; set; }
        public int InTimeMedium { get; set; }

        public int NotCompletedLow { get; set; }
        public int AfterDueDateLow { get; set; }
        public int InTimeLow { get; set; }
    }

    public class GetCompliancesMonthWiseSummary
    {
        public int NotComplied { get; set; }
        public int Complied { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    public class GetCompliancesMonthlySummary
    {
        public int NotComplied { get; set; }
        public int Complied { get; set; }
        public string Branch { get; set; }
    }
    public class GetClientCompliancesMonthlySummary
    {
        public int NotComplied { get; set; }
        public int Complied { get; set; }
    }

    public class GetResultOnboardingComplianceSummary
    {
        public List<GetClientCompliancesMonthlySummary> Clientdata { get; set; }
        public List<GetCompliancesMonthlySummary> locationdata { get; set; }
    }

    public class RLCSDashboradCount
    {
        public int Compliance { get; set; }
        public int DueToday { get; set; }
        public int Upcoming { get; set; }
        public int Overdue { get; set; }
    }
    public class ActDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class RLCSLoginDetails
    {
        public string status { get; set; }
        public string msg { get; set; }
        public string tokendetail { get; set; }
        public string CurrentEmailName { get; set; }
        public string Notificationstatus { get; set; }
        public string UserEmail { get; set; }
        public string UserContact { get; set; }
        public string UserRole { get; set; }
        public string ProfileID { get; set; }
        public string authKey { get; set; }
        public string ProfileID_Encrypted { get; set; }
    }
    public class AuthKeyDetails
    {
        public string status { get; set; }
        public string tokendetail { get; set; }
        public string MessageDetail { get; set; }
    }


    public class RLCSManagement
    {
        public class GetEntityInput
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }         
        }

        public class GetUserDetail
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string DeviceIDIMEI { get; set; }
        }
        
        public class GetInvoiceInput
        {
            public string UserName { get; set; }
            public string clientID { get; set; }
            public string type { get; set; }
            public int PageNumber { get; set; }           //-1            
        }

        public class GetInvoiceDocumentInput
        {
            public string UserName { get; set; }
            public string clientID { get; set; }
            public string MonthID { get; set; }         
            public string YearID { get; set; }         
            public string InvoiceNumber { get; set; }         
        }
        public class DashboradCountInput
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }           //-1            
        }

        public class RegistrationRepositoryInput
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int LocationID { get; set; }
            public string ActivityID { get; set; }
            public int page { get; set; }
        }

        public class GetUpdateUserTrak
        {
            public string UserName { get; set; }
            public string RecordID { get; set; }
            public string ProfileID { get; set; }
            public string PageName { get; set; }           
            public string Activity { get; set; }             
        }

        public class AssignmentComplianceByActId
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }           //-1  
            public int ActId { get; set; }           //-1
            public int Page { get; set; }           //1
        }

        public class DashboradComplianceSummaryCount
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }           //-1 
            public int FYyear { get; set; }           //-1 

        }
        public class ReportMonthlyComplianceSummary
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }           //-1 
            public int year { get; set; }
            public int Month { get; set; }  //-1 

        }
        public class RLCSMyNoticeInput
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }
            public string StatusType { get; set; }
            public string NoticeType { get; set; }
            public int Page { get; set; }
        }

        

        public class RLCSSummaryDetailInput
        {
            public string UserName { get; set; }
            public string Risk { get; set; }
            public string Flag { get; set; }
            public string Year { get; set; }
            public int LocationId { get; set; }
            public int CategoryId { get; set; }
            public int Page { get; set; }
        }
        public class RLCSSummaryOutputDetails
        {
            public List<SP_GetCannedReportCompliancesSummary_Result> ComplianceSummaryDetail { get; set; }

            public int HighCount { get; set; }
            public int MediumCount { get; set; }
            public int LowCount { get; set; }

            public int hrcount { get; set; }
            public int Financecount { get; set; }

        }

        public class RLCSMyDocumentNotice
        {
            public string UserName { get; set; }
            public int noticeInstanceID { get; set; }
            public int Page { get; set; }            
        }
        public class RLCSMyDocumentNoticeOutput
        {
            public string Type { get; set; }
            public string Receipt_Date { get; set; }
            public string Reference_No { get; set; }
            public string Close_Date { get; set; }
            public string Act { get; set; }
            public int ActId { get; set; }
            public string Location { get; set; }
            public int LocationId { get; set; }
            public string Notice_Title { get; set; }
        }
        public class ReturnsNew
        {
            public List<SP_RLCS_GetBranchList_ReturnChallan_HistoricalNew_Result> HistoricalData { get; set; }
            public List<SP_RLCS_GetBranchList_Returns_Documents_Result> NewData { get; set; }
        }
        public class RLCSDeviceDetails
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public string DeviceIDIMEI { get; set; }
            public string DeviceToken { get; set; }
            public string flag { get; set; }
        }
        public class MydocumentHistorical
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int LocationID { get; set; }
            public string MonthId { get; set; }           //-1 
            public string YearID { get; set; }           //-1 
            public string scopeID { get; set; }           //-1 
            public string challanType { get; set; }           //-1 
            public int page { get; set; }
        }

        public class AllComplianceDocumentList
        {
            public string UserName { get; set; }
            public string ScheduledOnID { get; set; }
        }

        public class AllComplianceDocumentPath
        {
            public string UserName { get; set; }
            public string ScheduledOnID { get; set; }
            public string FileID { get; set; }
        }

        public class AllDocumentList
        {
            public string UserName { get; set; }
            public string scopeID { get; set; }
            public string RecordID { get; set; }
        }
        public class DocumentPathInput
        {
            public string UserName { get; set; }
            public string FileID { get; set; }
            public string FileType { get; set; }  //H or C
        }
       


        public class ComplianceDetailInput
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public int locationId { get; set; }           //-1    
            public int page { get; set; }      //1  
            //public string CustomerId { get; set; }
            public int ActId { get; set; }           //-1
        }

        public class DetailInputduetodayUpcomingOverdue
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
            public string filterType { get; set; }      //(Upcoming,Overdue,DueToday)
            public int locationId { get; set; }      //-1
            public int ActId { get; set; }      //-1 
            public int page { get; set; }      //1 

        }
        public static List<SP_RLCS_NoticeDocuments_Result> GetNoticeDocuments(int noticeInstanceID, int customerID, string docType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SP_RLCS_NoticeDocuments(noticeInstanceID, customerID)
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    if (docType != "")
                        queryResult = queryResult.Where(row => row.DocType.Trim() == docType).ToList();
                    else
                        queryResult = queryResult.Where(row => (row.DocType.Trim() == "N") || (row.DocType.Trim() == "NR") || (row.DocType.Trim() == "NT") || (row.DocType.Trim() == "CD")).ToList();
                }

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }

        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Result> MastertransactionsQuery,
                 string filter)
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && row.RoleID == 3
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        //NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.RoleID == 3
                                              && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                        //                    where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)                                             
                        //                     && row.RoleID == 3
                        //                     && row.PerformerScheduledOn < now
                        //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.RoleID == 3
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.RoleID == 3
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }

        public static List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetails(int customerid, List<long> Branchlist, string role, int CustomerBranchID, int ActId, int CategoryID, int Userid, string Profileid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result>();

                ComplianceDetails = (from row in entities.SP_RLCS_ComplianceInstanceAssignmentViewDetails(Userid, customerid, true, Profileid)
                                     select row).ToList();

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();

                return ComplianceDetails;
            }
        }


        public static List<ActDetails> GetAllAssignedActs_RLCS(int Customerid, int userID, int CategoryId, string ProfileID, List<long> blist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var details = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID, true)
                               select row).ToList();

                if (blist.Count > 0)
                {
                    details = details.Where(entry => blist.Contains(entry.BranchID)).ToList();
                }

                details = details.GroupBy(entity => entity.ACTID).Select(entity => entity.FirstOrDefault()).ToList();
                List<ActDetails> acts = new List<ActDetails>();
                if (CategoryId != -1)
                {
                    acts = (from row in details
                            where row.ComplianceCategoryId == CategoryId
                            select new ActDetails { Id = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<ActDetails>();

                    //acts = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID)
                    //        where row.ComplianceCategoryId == CategoryId
                    //        select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();                
                }
                else
                {
                    acts = (from row in details
                            select new ActDetails { Id = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<ActDetails>();

                    //acts = (from row in entities.SP_RLCS_GetApplicableActs(Customerid, userID, ProfileID)
                    //        select new { ID = row.ACTID, Name = row.ACTNAME }).OrderBy(entry => entry.Name).ToList<object>();                   
                }

                acts = acts.ToList();
                return acts;
            }
        }

        public static List<sp_ComplianceAssignedCategory_Result> GetNewAll(int UserID, int CustomerBranchId, List<long> Branchlist, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();

                if (Branchlist.Count > 0)
                {
                    complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "RLCS")
                                           where Branchlist.Contains((long)row.CustomerBranchID)
                                           select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                }
                else
                {
                    complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "RLCS")
                                           select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                }


                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static string GetMonthName(int monthNumber)
        {
            string monthName = string.Empty;

            if (monthNumber != 0)
            {
                if (monthNumber == 1)
                    monthName = "Jan";
                else if (monthNumber == 2)
                    monthName = "Feb";
                else if (monthNumber == 3)
                    monthName = "Mar";
                else if (monthNumber == 4)
                    monthName = "Apr";
                else if (monthNumber == 5)
                    monthName = "May";
                else if (monthNumber == 6)
                    monthName = "Jun";
                else if (monthNumber == 7)
                    monthName = "Jul";
                else if (monthNumber == 8)
                    monthName = "Aug";
                else if (monthNumber == 9)
                    monthName = "Sep";
                else if (monthNumber == 10)
                    monthName = "Oct";
                else if (monthNumber == 11)
                    monthName = "Nov";
                else if (monthNumber == 12)
                    monthName = "Dec";
            }

            return monthName;
        }

        public static List<SP_RLCS_NoticeAssignedInstance_Result> GetInspectionNoticeList(int customerID, int loggedInUserID, string loggedInUserProfileID, List<long> branchList, string noticeStatus, string noticeType) /*int branchID*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_RLCS_NoticeAssignedInstance(customerID, loggedInUserID, loggedInUserProfileID)
                             select row).ToList();

                if (!string.IsNullOrEmpty(noticeStatus))
                {
                    if (noticeStatus == "P") //3--Closed otherwise Open
                        query = query.Where(entry => entry.IN_Status == noticeStatus).ToList();
                    else if (noticeStatus == "C")
                        query = query.Where(entry => entry.IN_Status == noticeStatus).ToList();
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime EndDate = DateTime.Today.Date;

                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["RLCSCacheClearTime"]);

                string CacheName = "CacheGetCannedReportCompliancesSummary_" + userId + "_" + customerid;
                detailView = (List<SP_GetCannedReportCompliancesSummary_Result>)HttpContext.Current.Cache[CacheName];
                if (detailView == null)
                {
                    entities.Database.CommandTimeout = 180;
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();
                    HttpContext.Current.Cache.Insert(CacheName, detailView, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }

                //entities.Database.CommandTimeout = 180;
                //detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();

                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                //if (statusIDs.Count != 0 && CategoryID.Count != 0)
                //{
                //    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                //}
                if (statusIDs.Count != 0)
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                //if (!String.IsNullOrEmpty(filter))
                //{

                //    if (filter.Equals("Function"))
                //    {
                //        if (Risk != -1)
                //        {
                //            detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                //        }
                //    }
                //}
                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime EndDate = DateTime.Today.Date;

                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["RLCSCacheClearTime"]);
                string CacheName = "CacheGetCannedReportCompliancesSummary_" + userId + "_" + customerid;
                detailView = (List<SP_GetCannedReportCompliancesSummary_Result>)HttpContext.Current.Cache[CacheName];
                if (detailView == null)
                {
                    entities.Database.CommandTimeout = 180;
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();
                    HttpContext.Current.Cache.Insert(CacheName, detailView, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }

                //entities.Database.CommandTimeout = 180;
                //detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                //if (statusIDs.Count != 0 && CategoryID.Count != 0)
                //{
                //    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                //}
                if (statusIDs.Count != 0)
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                //if (filter.Equals("Function"))
                //{
                //    if (Risk != -1)
                //    {
                //        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                //    }
                //}

                return detailView;
            }
        }

        #region Notice and ACT

        public static tbl_LegalNoticeInstance GetNoticeByID(int noticeInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_LegalNoticeInstance
                             where row.ID == noticeInstanceID
                             select row).FirstOrDefault();
                return Query;
            }
        }
        public static View_NoticeAssignedInstance GetNoticeStatusDetail(int noticeInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_NoticeAssignedInstance
                                   where row.NoticeInstanceID == noticeInstanceID
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }
        public static List<tbl_ActMapping> GetListOfAct(long noticeOrCaseInstanceID, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_ActMapping
                                   where row.CaseNoticeInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   select row);

                return queryResult.ToList();
            }
        }

        #endregion

        public static string GetAuthKeyByProfileID(string requestUrl, string ProfileID, string key)
        {
            string authKey = string.Empty;
            string authEncryptedKey = string.Empty;

            requestUrl += "UserMaster/GetAuthKeyBasedOnProfileId?ProfileId=" + ProfileID;

            //string responseData = RLCSAPIClasses.Invoke_Live("GET", requestUrl, "");
            string responseData = Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var _objAuthKeyResponse = JsonConvert.DeserializeObject<API_AuthKey_Response>(responseData);

                if (_objAuthKeyResponse != null)
                {
                    if (_objAuthKeyResponse.ProfileId != null && _objAuthKeyResponse.AuthKey != null)
                    {
                        if (_objAuthKeyResponse.ProfileId.Trim().ToUpper().Equals(ProfileID))
                        {
                            authEncryptedKey = _objAuthKeyResponse.AuthKey;

                            if (!string.IsNullOrEmpty(authEncryptedKey))
                            {
                                authKey = CryptographyHandler.decrypt(authEncryptedKey, key);
                            }
                        }
                    }
                }
            }

            return authKey;
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        public static string InvokeWithAuthKey(string Method, string Uri, string authKey, string encryptedProfileID, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

            var authValue = authKey;
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));

            var _UserAgent = encryptedProfileID;
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);

            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }
        public static string InvokeWithoutAuthKey(string Method, string Uri, string authKey, string encryptedProfileID, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

            //var authValue = authKey;
            //cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));

            var _UserAgent = encryptedProfileID;
            // You can actually also set the User-Agent via a built-in property
            cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);

            // You get the following exception when trying to set the "Content-Type" header like this:
            // cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            // "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }
        public class Profile_BasicInfo
        {
            public string ClientCount { get; set; }
            public string ProfileName { get; set; }
            public string Designation { get; set; }
            public List<ClientInfo> lstBasicInfrmation { get; set; }
        }
        public class ClientInfo
        {
            public string ClientID { get; set; }
            public string ClientName { get; set; }
            public string Branches { get; set; }
            public string Employees { get; set; }
            public string Contractors { get; set; }
            public string Joinees { get; set; }
            public string Resignees { get; set; }
        }



        public static bool CreateUpdateTrack_User_Activities(RLCS_Track_User_Activities _objRecord)
        {
            bool saveSuccess = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (_objRecord != null)
                {
                    _objRecord.CreatedDate = DateTime.Now;
                    entities.RLCS_Track_User_Activities.Add(_objRecord);
                    saveSuccess = true;
                    entities.SaveChanges();
                }

                return saveSuccess;
            }
        }

        public class MyCriticalDocuments
        {
            public string UserName { get; set; }
            public string ProfileID { get; set; }
        }
    }
}