﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Reflection;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class EmailNotations
    {
        public static string SendPasswordResetNotificationEmail(User user, string passwordText, string url, string customerName)
        {
            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
            string message = Properties.Settings.Default.EmailTemplate_UserPasswordReset
                                    .Replace("@Username", user.Email)
                                    .Replace("@User", username)
                                    .Replace("@PortalURL", url)
                                    .Replace("@Password", passwordText)
                                    .Replace("@From", customerName)
                                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

            return message;           
        }
    }
}