﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CountCFO
    {
        public string divPenalty { get; set; }
        public string divLocationCount { get; set; }
        public string divEntitesCount { get; set; }
        public string divUsersCount { get; set; }
        public string divCompliancesCount { get; set; }
        public string divFunctionCount { get; set; }
    }
    public class CommentsContract
    {
        public string _id { get; set; }
        public int contractID { get; set; }
        public int user_id { get; set; }
        public int customer_id { get; set; }
        public string comment { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string message_id { get; set; }
        public string user_name { get; set; }
        public int isfile { get; set; }
        public string filepath { get; set; }
        public int touser_id { get; set; }
    }
}