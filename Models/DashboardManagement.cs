﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
namespace AppWebApplication.Models
{
    public class DashboardManagement
    {
        public static List<sp_ComplianceInstanceTransactionNEW_Result> DashboardDataForPerformerAllNEW(int userID, string status, DateTime CalenderDate, List<sp_ComplianceInstanceTransactionNEW_Result> msttransactionsQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceInstanceTransactionNEW_Result>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                entities.Database.CommandTimeout = 180;

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        //PerformerScheduledOn change coz name change in sp for condition
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14 || row.ComplianceStatusID == 19)
                                              && row.ScheduledOn < nextOneMonth
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14 || row.ComplianceStatusID == 19)
                                              && row.ScheduledOn == CalenderDate
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19) && row.PerformerScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                //if (queryStringFlag == "E")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true && entry.ReportType != "StatutoryLicense").ToList();
                //}
                //else if (queryStringFlag == "C")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null && entry.ReportType != "StatutoryLicense").ToList();
                //}
                //else if (queryStringFlag == "S")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.ReportType != "StatutoryLicense").ToList();
                //}
                //else if (queryStringFlag == "SL")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.ReportType == "StatutoryLicense").ToList();
                //}
                return transactionsQuery.ToList();
            }
        }
        public static List<sp_ComplianceInstanceTransactionNEW_Result> DashboardDataForReviewerAllNew(int userID, string status, DateTime CalenderDate, List<sp_ComplianceInstanceTransactionNEW_Result> msttransactionsQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceInstanceTransactionNEW_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in msttransactionsQuery // entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
                                             row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 ||
                                             row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
                                             row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 ||
                                             row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 19) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Completed")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID
                                         && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.ScheduledOn <= now
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 15)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in msttransactionsQuery //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 ||
                                          row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in msttransactionsQuery  //entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                //if (queryStringFlag == "E")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                //}
                //else if (queryStringFlag == "C")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                //}
                return transactionsQuery.ToList();
            }
        }


        public static List<sp_StatutoryMGMTData_Result> DashboardDataMGMtReviewerAll(int userID, int custID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_StatutoryMGMTData_Result> transactionsQuery = new List<sp_StatutoryMGMTData_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                //userID = 37;

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "01/01/1900 00:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                             where
                                              row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                                 // where
                                                 // row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
                                                 // && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             where row.Status != "Overdue"
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                         where
                                         //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                         where
                                          // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)&&
                                          row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                         where
                                         // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                         where
                                         //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)   &&
                                          row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (from row in entities.sp_StatutoryMGMTData(userID, custID)
                                         where
                                        // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && 
                                        row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        public static List<sp_ComplianceMgmtTransactionNEW_Result> DashboardDataMGMtReviewerAll(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceMgmtTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceMgmtTransactionNEW_Result>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "01/01/1900 00:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 4)
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }
                return transactionsQuery.ToList();
            }


        }

        public static List<sp_ComplianceMgmtTransactionNEW_Result> DashboardDataMGMTForPerformerAll(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceMgmtTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceMgmtTransactionNEW_Result>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                entities.Database.CommandTimeout = 180;

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "01/01/1900 00:00:00 AM")
                    {
                        //PerformerScheduledOn change coz name change in sp for condition
                        transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.ScheduledOn < nextOneMonth
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                              && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                              && row.ScheduledOn == CalenderDate
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.sp_ComplianceMgmtTransactionNEW(userID, 3)
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<Sp_ChecklistTransaction_Result> ChecklistDataReviewer(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_ChecklistTransaction_Result> transactionsQuery = new List<Sp_ChecklistTransaction_Result>();

                DateTime now = DateTime.UtcNow.Date;

                entities.Database.CommandTimeout = 180;
                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1 
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                if (status == "Overdue")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1 
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                else if (status == "Closed-Timely")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 4
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                else if (status == "Not Applicable")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 15
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                else if (status == "Not Complied")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID //&& row.RoleID == 4
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 17
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<Sp_ChecklistTransaction_Result> ChecklistDataReviewer(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<Sp_ChecklistTransaction_Result> transactionsQuery = new List<Sp_ChecklistTransaction_Result>();

        //        DateTime now = DateTime.UtcNow.Date;

        //        entities.Database.CommandTimeout = 180;
        //        if (status == "Status" || status == "Overdue")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
        //                                     where row.UserID == userID && row.RoleID == 4
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     && (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1 
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                if (queryStringFlag == "E")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //                }
        //                else if (queryStringFlag == "C")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //                }
        //            }
        //        }
        //        else if (status == "Closed-Timely")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
        //                                     where row.UserID == userID //&& row.RoleID == 4
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 4
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                if (queryStringFlag == "E")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //                }
        //                else if (queryStringFlag == "C")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //                }
        //            }
        //        }
        //        else if (status == "Not Applicable")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
        //                                     where row.UserID == userID //&& row.RoleID == 4
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     && row.ComplianceStatusID == 15
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                if (queryStringFlag == "E")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //                }
        //                else if (queryStringFlag == "C")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //                }
        //            }
        //        }
        //        else if (status == "Not Complied")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
        //                                     where row.UserID == userID //&& row.RoleID == 4
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                     && row.ComplianceStatusID == 17
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                if (queryStringFlag == "E")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //                }
        //                else if (queryStringFlag == "C")
        //                {
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //                }
        //            }
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<Sp_ChecklistTransaction_Result> ChecklistDataPerformer(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_ChecklistTransaction_Result> transactionsQuery = new List<Sp_ChecklistTransaction_Result>();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                entities.Database.CommandTimeout = 180;

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {

                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == now || row.ScheduledOn <= now) //&& row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                if (status == "Overdue")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {

                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ScheduledOn == nextOneMonth || row.ScheduledOn <= nextOneMonth) && row.ComplianceStatusID == 1
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                if (status == "Closed-Timely")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {

                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 4
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                else if (status == "Not Applicable")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {

                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 15
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                else if (status == "Not Complied")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {

                        transactionsQuery = (from row in entities.Sp_ChecklistTransaction(userID)
                                             where row.UserID == userID && row.RoleID == 3
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && row.ComplianceStatusID == 17
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (queryStringFlag == "E")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                        }
                        else if (queryStringFlag == "C")
                        {
                            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                        }
                    }
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<sp_ComplianceInstanceTransactionNEW_Result> DashboardDataForReviewerNewSp(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<sp_ComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceInstanceTransactionNEW_Result>();

        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        if (status == "Status")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                     where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn <= nextOneMonth
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
        //                                     row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 ||
        //                                     row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //            }
        //            else
        //            {
        //                transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                     where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true && row.ScheduledOn == CalenderDate
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 ||
        //                                     row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || 
        //                                     row.ComplianceStatusID == 12 || row.ComplianceStatusID == 18)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //            }
        //        }
        //        else if (status == "DueButNotSubmitted")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 19) && row.ScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Completed")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID
        //                                 && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.ScheduledOn <= now
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 15) 
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                  
        //        }
        //        else if (status == "Rejected")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                  && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || 
        //                                  row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 4)
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && row.ScheduledOn <= now
        //                                  && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        if (queryStringFlag == "E")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //        }
        //        else if (queryStringFlag == "C")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}


        //public static List<sp_ComplianceInstanceTransactionNEW_Result> DashboardDataForPerformerAll(int userID, string queryStringFlag, string status, DateTime CalenderDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<sp_ComplianceInstanceTransactionNEW_Result> transactionsQuery = new List<sp_ComplianceInstanceTransactionNEW_Result>();

        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        entities.Database.CommandTimeout = 180;

        //        if (status == "Status")
        //        {
        //            if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
        //            {
        //                //PerformerScheduledOn change coz name change in sp for condition
        //                transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14 || row.ComplianceStatusID == 19)
        //                                      && row.ScheduledOn < nextOneMonth
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //            }
        //            else
        //            {
        //                transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14 || row.ComplianceStatusID == 19)
        //                                      && row.ScheduledOn == CalenderDate
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //            }
        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Overdue")
        //        {
        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 19) && row.ScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "Rejected")
        //        {

        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {

        //            transactionsQuery = (from row in entities.sp_ComplianceInstanceTransactionNEW(userID, 3)
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 16 || row.ComplianceStatusID == 18)
        //                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        if (queryStringFlag == "E")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
        //        }
        //        else if (queryStringFlag == "C")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}


        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForReviewerDisplayCount(int userID,
   List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.EventFlag == null
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due but Not Submitted
                    //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                    //                     where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null 
                    //                   //  && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                    //                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
                    //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                    transactionsQuery = (from row in MastertransactionsQuery
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.EventFlag == null
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.ScheduledOn <= nextOneMonth
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                return transactionsQuery.ToList();
            }
        }
        // public static List<SP_ComplianceInstanceTransactionCount_Result> DashboardDataForReviewerDisplayCount(int userID,
        //List<SP_ComplianceInstanceTransactionCount_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        // {

        //     using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //     {
        //         List<SP_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Result>();
        //         var reviewerRoleIDs = (from row in entities.Roles
        //                                where row.Code.StartsWith("RVW")
        //                                select row.ID).ToList();

        //         DateTime now = DateTime.UtcNow.Date;
        //         DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //         if (filter == CannedReportFilterForPerformer.PendingForReview)
        //         {
        //             transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                  where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.EventFlag == null
        //                                                                                                          //  && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                   && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
        //                                  select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //         }
        //         else if (pending)
        //         {
        //             //Due but Not Submitted
        //             transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                  where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.EventFlag == null 
        //                                                                                                          //  && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
        //                                  select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //         }
        //         else if (filter == CannedReportFilterForPerformer.Rejected)
        //         {
        //             transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                  where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //                                  // && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                  select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //         }
        //         else
        //         {
        //             transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                  where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) //&& row.EventFlag == null
        //                                  //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                  && row.ScheduledOn <= nextOneMonth
        //                                   && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
        //                                  select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //         }
        //         return transactionsQuery.ToList();
        //     }
        // }

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerDisplayCount(int userID,
       List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> mastertransactionsQuery, CannedReportFilterForPerformer filter)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID // && row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<SP_ComplianceInstanceTransactionCount_Result> DashboardDataForPerformerDisplayCount(int userID,
        // List<SP_ComplianceInstanceTransactionCount_Result> mastertransactionsQuery, CannedReportFilterForPerformer filter)
        //{

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        List<SP_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Result>();
        //        switch (filter)
        //        {
        //            case CannedReportFilterForPerformer.Upcoming:
        //                transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
        //                                     && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.Overdue:
        //                transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
        //                                     && row.ScheduledOn < now
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.PendingForReview:
        //                transactionsQuery = (from row in mastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID // && row.EventFlag == null                                              
        //                                     && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                break;
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerOverDueDisplayCount(int userID,
List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                        //                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                        //                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                        //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        transactionsQuery = (from row in MastertransactionsQuery
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        //public static List<SP_ComplianceInstanceTransactionCount_Result> DashboardDataForPerformerOverDueDisplayCount(int userID,
        //    List<SP_ComplianceInstanceTransactionCount_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<SP_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Result>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        switch (filter)
        //        {
        //            case CannedReportFilterForPerformer.Upcoming:
        //                transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
        //                                     && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.Overdue:
        //                transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
        //                                     && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForPerformer.PendingForReview:
        //                transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
        //                                     && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
        //                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardDataForPerformerUpcomingDisplayCount(int userID,
 List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
                                             && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID// && row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        // public static List<SP_ComplianceInstanceTransactionCount_Result> DashboardDataForPerformerUpcomingDisplayCount(int userID,
        //List<SP_ComplianceInstanceTransactionCount_Result> MastertransactionsQuery, CannedReportFilterForPerformer filter)
        // {
        //     using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //     {

        //         List<SP_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Result>();
        //         int performerRoleID = (from row in entities.Roles
        //                                where row.Code == "PERF"
        //                                select row.ID).Single();
        //         DateTime now = DateTime.UtcNow.Date;
        //         DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //         switch (filter)
        //         {
        //             case CannedReportFilterForPerformer.Upcoming:

        //                 transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                      where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                             
        //                                      && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
        //                                      select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                 break;
        //             case CannedReportFilterForPerformer.Overdue:

        //                 transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                      where row.UserID == userID && row.RoleID == performerRoleID// && row.EventFlag == null                                              
        //                                      && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.PerformerScheduledOn < now
        //                                      select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                 break;
        //             case CannedReportFilterForPerformer.PendingForReview:

        //                 transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                      where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null                                              
        //                                      && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
        //                                      select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                 break;

        //             case CannedReportFilterForPerformer.Rejected:

        //                 transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
        //                                      where row.UserID == userID && row.RoleID == performerRoleID
        //                                      && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
        //                                      select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                 break;
        //         }
        //         return transactionsQuery.ToList();
        //     }
        // }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, string StringType, int UserIDPerformer)//15
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                          && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 || row.ComplianceStatusID == 14)
                                          && row.PerformerScheduledOn < nextOneMonth
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.PerformerScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (queryStringFlag == "E")
                {
                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                //PerformerID
                if (UserIDPerformer != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == UserIDPerformer)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
           
        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerUpcomingDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                List<ComplianceInstanceTransactionView> display = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        //transactionsQuery = display.Where(entry =>  (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID// && row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<SP_GetAllActivatedEvent_Result> DashboardDataActiveEventCount(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                int eventRoleID = 10;
              
                var transactionsQuery = entities.SP_GetAllActivatedEvent(eventRoleID, userID, Convert.ToInt32(UserManagement.GetByID(userID).CustomerID));

                return transactionsQuery.ToList();
            }
        }

        public static List<Event_Assigned_View> DashboardDataAssignedEventCount(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
               
                int eventRoleID = 10;
                var transactionsQuery = EventManagement.GetAllAssignedInstancesCount(eventRoleID, -1, userID, Convert.ToInt32(UserManagement.GetByID(userID).CustomerID));

                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerOverDueDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForPerformerDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID //&& row.EventFlag == null 
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 13)
                                             && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID // && row.EventFlag == null 
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<Sp_RLCSGetStatutoryInternalAssignment_Result> StatutoryInternalAssignment(int userID, string ProfileID,int CustID, int distID,int spID, string roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_RLCSGetStatutoryInternalAssignment_Result> transactionsQuery = new List<Sp_RLCSGetStatutoryInternalAssignment_Result>();
                transactionsQuery = (from row in entities.Sp_RLCSGetStatutoryInternalAssignment(userID, ProfileID, CustID, distID, spID, roleID)
                                     select row).ToList();                          
                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, string queryStringFlag, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int EventID, int EventSchudeOnID, int SubTypeID, int PerformerID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (status == "Status")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12) && row.ScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.ScheduledOn >= now && row.ScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= now
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (queryStringFlag == "E")
                {

                    if (EventID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                    if (EventSchudeOnID != -1)
                        transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == true && entry.ComplinceVisible == true).ToList();
                }
                else if (queryStringFlag == "C")
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();

                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                if (PerformerID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
                }
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
       
        public static List<ComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCount(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due but Not Submitted
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null 
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID) //&& row.EventFlag == null
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.ScheduledOn <= nextOneMonth
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                return transactionsQuery.ToList();
            }
        }

        public static List<ComplianceDashboardSummaryView> GetComplianceDashboardSummary(int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                         where row.CustomerID == Customerid && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();
                return transactionsQuery;
            }
        }

        public static List<CheckListInstanceTransactionReviewerView> DashboardDataForReviewer_ChecklistNew(int userID, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<CheckListInstanceTransactionReviewerView> transactionsQuery = new List<CheckListInstanceTransactionReviewerView>();
                int ReviewerRoleID = (from row in entities.Roles
                                      where row.Code == "RVW1"
                                      select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                     where row.UserID == userID && row.RoleID == ReviewerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                     //&& (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Description.Contains(StringType))).ToList();
                }


                return transactionsQuery.ToList();
            }
        }

        public static List<CheckListInstanceTransactionView> DashboardDataForPerformer_ChecklistNew(int userID, int risk, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<CheckListInstanceTransactionView> transactionsQuery = new List<CheckListInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                                     where row.UserID == userID && row.RoleID == performerRoleID
                                      && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                      && (row.ScheduledOn == now || row.ScheduledOn <= now) && row.ComplianceStatusID == 1
                                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //Type Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return transactionsQuery.ToList();
            }
        }

    }
}