﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class LawyerManagement
    {
        #region Suvek
        public static bool DeleteLawyerData(int ID, long customerID)
        {
            bool output = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_Lawyer Query = (from row in entities.tbl_Lawyer
                                        where row.ID == ID && row.CustomerID == customerID
                                        select row).FirstOrDefault();
                    if (Query != null)
                    {
                        Query.IsActive = false;
                        //entities.Mst_Laywer.Remove(Query);
                        entities.SaveChanges();
                        output = true;
                    }
                }
                return output;
            }
            catch (Exception ex)
            {
                return output;
            }
        }
        #endregion

        #region Gaurav

        public static List<tbl_Lawyer> GetLawyerListAll(long CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.tbl_Lawyer
                                  where row.IsActive == true
                                  && row.CustomerID == CustomerId
                                  select row);
                return ObjCountry.ToList();
            }
        }
        public static bool DeleteCrtteriaByID(long customerID, int ID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objdelcriiaID = (from row in entities.tbl_CriteriaRatingMaster
                                          where row.CustomerID == customerID
                                          && row.ID == ID
                                          && row.IsActive == true
                                          select row).FirstOrDefault();
                    if (_objdelcriiaID != null)
                    {
                        _objdelcriiaID.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}