﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    #region Amol
    public class CreateTask
    {
        public string Email { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public string DueDate { get; set; }
        public string Priority { get; set; }
        public string ExpectedOutcome { get; set; }
        public int InternalUser { get; set; }
        public int ExternalUser { get; set; }
        public string Remark { get; set; }
    }
    public class mylawyerlist
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string Specilisation { get; set; }
        public string Islawyer { get; set; }
    }
    public class HearingDetailReport
    {
        public long CaseInstanceID { get; set; }
        public Nullable<System.DateTime> ResponseDate { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CreatedByText { get; set; }

        public Nullable<System.DateTime> NextHearingDate { get; set; }
    }

    public class ResponseDetailReport
    {
        public long CaseInstanceID { get; set; }
        public Nullable<System.DateTime> ResponseDate { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CreatedByText { get; set; }

        public Nullable<System.DateTime> NextHearingDate { get; set; }
    }
    //public class UpdateNotificationNew
    //{
    //    public string Message { get; set; }
    //}
    public class MyReportLitigation
    {
        public List<sp_LitigationNoticeReportWithCustomParameter_Result> Notice { get; set; }
        public List<SP_LitigationCaseReportWithCustomParameter_Result> Case { get; set; }
    }
    public class DepartmentList
    {
        public string DepartmentName { get; set; }
        public int DepartmentID { get; set; }
    }
    public class OutputList
    {
        public string Name { get; set; }
        public long ID { get; set; }
    }
    public class LitigationModel
    {
    }
    #endregion

    #region Gaurav
    public class myfiletag
    {
        public string filetag { get; set; }
    }
    
    public class mydocdetails
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ID1 { get; set; }
        public string Name1 { get; set; }
        public int ID2 { get; set; }
        public string Name2 { get; set; }
    }
    public class mydocumenttype
    {
        public string RefNo { get; set; }
        public string Title { get; set; }
        public string PartyName { get; set; }
        public string PartyID { get; set; }
        public string DepartmentID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public string Priority { get; set; }
        public string Task { get; set; }
        public string Task_Description { get; set; }
        public string Status { get; set; }
        public string Assigned_To { get; set; }
        public DateTime Due_date { get; set; }
        public string TypeName { get; set; }
        public string InstanceID { get; set; }
        public string BranchName { get; set; }
        public string FYName { get; set; }

        public string TaskType { get; set; }
        public Nullable<long> NoticeCaseInstanceID { get; set; }
        public string CBU { get; set; }
        public string Zone { get; set; }
        public string Region { get; set; }
        public string Territory { get; set; }
        public string CaseCreator { get; set; }
        public string EmailId { get; set; }
        public string ContactNumber { get; set; }
    }
    internal class OutputDetil
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CustFieldAndType
    {
        public long ID { get; set; }
        public string Label { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int TypeID { get; set; }
        public string CaseType { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class GetALLUserDetail
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Designation { get; set; }
        public string Address { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> LawyerFirmID { get; set; }
        public Nullable<bool> IsExternal { get; set; }
        public Nullable<int> LitigationRoleID { get; set; }
        public Nullable<long> LawyerID { get; set; }
        public string Rating { get; set; }
        public Nullable<bool> RatingIsActive { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public string ISUnlock { get; set; }
    }
    #endregion

    public class MGMTWidgetCompliances
    {
        public List<SP_GetWidgetCompliancesSummary_Result> SList { get; set; }
    }
    public class myreminderclass
    {
        public string Email { get; set; }

        public int ddltype { get; set; }

        public int InstanceID  { get; set; }

        public int PageID { get; set; }
    }
    public class myreminderFilterclass
    {
        public string Email { get; set; }

        public int ddltype { get; set; }

    }

    public class MyDetailLockingDetail
    {
        public string Email { get; set; }
        public string Type { get; set; }
        //public int IDofDetailLocking { get; set; }
        public List<int> IDofDetailLocking { get; set; }
        public string txtUpdateDate { get; set; }
        public string remark { get; set; }

    }

    public class updateLockingDetail
    {
        //public string ActName { get; set; }
        //public int ActID { get; set; }
        //public string ShortDescription { get; set; }
        //public string Description { get; set; }
        public long ID { get; set; }
        //public long ScheduleOnID { get; set; }
        //public System.DateTime DueDate { get; set; }
        //public int DueDay { get; set; }
        //public bool IsLock { get; set; }
        //public Nullable<long> CreatedBy { get; set; }
        //public Nullable<System.DateTime> CreatedOn { get; set; }
        //public Nullable<long> UpdatedBy { get; set; }
        //public Nullable<System.DateTime> UpdateOn { get; set; }
        public string ComplianceType { get; set; }
        //public bool IsActive { get; set; }
        public int CustomerID { get; set; }
        //public string PerformerName { get; set; }
        //public long ComplianceID { get; set; }
        //public string BranchName { get; set; }
        public int UserID { get; set; }
        public string remark { get; set; }
        public string ActivateDate { get; set; }
        
    }

    public class updateMGMTReminder
    {
        public int UId { get; set; }
        public int CustId { get; set; }
        public int RoleId { get; set; }
        public int ComplianceId { get; set; }
        public int CustomerBranchID { get; set; }        
        public string setvalue { get; set; }        
        public string RemindFlag { get; set; }
        public string UserFlag { get; set; }        
    }

    public class mydocumentclass
    {
        public string Email { get; set; }

        public int ddltype { get; set; }

        public int ddlstatus { get; set; }

        public string dept{ get; set; }

        public string location { get; set; }

        public int IOType { get; set; }

        public int partyid { get; set; }

        public int PageID { get; set; }

    }

    public class mynoticewcclass
    {
        public string Email { get; set; }

        public int ddlstatus { get; set; }

        public int IOType { get; set; }

        public string FY { get; set; }

        public int dept { get; set; }

        public int location { get; set; }

        public string PartyID { get; set; }

        public int CaseTypeID { get; set; }

        public int OwnerID { get; set; }

        public int PageID { get; set; }
        public string InstanceID { get; set; }
    }

    public class mycasewcclass
    {
        public string Email { get; set; }

        public int ddlstatus { get; set; }

        public int IOType { get; set; }

        public string FY { get; set; }

        public int dept { get; set; }

        public int location { get; set; }

        public string PartyID { get; set; }
        
        public int CaseTypeID { get; set; }

        public int OwnerID { get; set; }

        public int PageID { get; set; }

        public string InstanceID { get; set; }

    }

    public class mytaskwcclass
    {
        public string Email { get; set; }

        public int ddlstatus { get; set; }

        public int priorityID { get; set; }

        public string StatusID { get; set; }

        public string FY { get; set; }

        public int PageID { get; set; }

        public string TaskID { get; set; }

    }
    public class mydashboardclass
    {
        public string Email { get; set; }

        public int PageID { get; set; }

    }
    public class mydashboardtaskclass
    {
        public string Email { get; set; }

        public string TaskType { get; set; }

        public int InstanceID { get; set; }

    }
    public class myreportclass
    {
        public string Email { get; set; }

        public int StatusFlag { get; set; }

        public int StatusType { get; set; }

        public int dept { get; set; }

        public int location { get; set; }

        public int IOType { get; set; }

        public string FY { get; set; }

        public int PageID { get; set; }


    }
    public class UpcomingdashBoardHearings
    {
        public string Email { get; set; }

        public string Dt { get; set; }

        public int PageID { get; set; }

    }
    public class LawyerList
    {
        public string LawyerName { get; set; }
        public long LawyertID { get; set; }
    }
    public class myDocumentclass
    {
        public string Email { get; set; }

        public int InstanceID { get; set; }

        public int TaskID { get; set; }

        public string Type { get; set; }

        public int PageID { get; set; }
        
        public string Flag { get; set; }
    }
    public class HearingClass
    {
        public string Email { get; set; }

        public int InstanceID { get; set; }

        public string Date { get; set; }

    }

    public class mytaskreportclass
    {
        public string Email { get; set; }

        public int taskStatus { get; set; }

        public int priorityID { get; set; }

        public string StatusID { get; set; }

        public string FY { get; set; }

        public int PageID { get; set; }
    }
    public class mydeptclass
    {
        public string Email { get; set; }

    }
    public class AddHearingDate
    {
        public string Email { get; set; }

        public string Dt { get; set; }

        public int CaseInstanceID { get; set; }

    }

    public class GetAllHearingDate
    {
        public string Email { get; set; }
        public int CaseInstanceID { get; set; }

    }
    public class AllHearingDate
    {
        public string HearingRefNo { get; set; }
        public long ID { get; set; }

    }
    public class AddHearing
    {
        public string Email { get; set; }

        public int CaseInstanceID { get; set; }

        public int HearingRefNoID { get; set; }

        public string Description { get; set; }

        public string NextHearingDate { get; set; }

        public string RemindMeOn { get; set; }

        public string Remarks { get; set; }

    }
    public class GetAllTask
    {
        public string Email { get; set; }
        public int CaseInstanceID { get; set; }

    }
    public class GetAllHearing
    {
        public string Email { get; set; }
        public int CaseInstanceID { get; set; }

    }
    public class NoticeDocument
    {
        public long NoticeCaseInstanceID { get; set; }
        public string TypeCaseNotice { get; set; }
        public string DocumentName { get; set; }
        public int FileID { get; set; }
        public string TypeName { get; set; }
        public string RefNo { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string BranchName { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public string Owner { get; set; }
        public int AssignedUser { get; set; }
        public int AssignedUserRoleID { get; set; }
        public string AssignedUserName { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public int CustomerBranchID { get; set; }
        public bool IsDeleted { get; set; }
        public string Filetag { get; set; }
        public string FYName { get; set; }
        public Nullable<int> DocumentCount { get; set; }
        public Nullable<int> Status { get; set; }
        public string PartyID { get; set; }
        public string PartyName { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }

    }
    public class CaseDocumentDetail
    {
        public long NoticeCaseInstanceID { get; set; }
        public string TypeCaseNotice { get; set; }
        public string FileName { get; set; }
        public int FileID { get; set; }
        public string TypeName { get; set; }
        public string RefNo { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string BranchName { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public string Owner { get; set; }
        public int AssignedUser { get; set; }
        public int AssignedUserRoleID { get; set; }
        public string AssignedUserName { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public int CustomerBranchID { get; set; }
        public Nullable<int> Status { get; set; }
        public bool IsDeleted { get; set; }
        public string Filetag { get; set; }
        public string FYName { get; set; }
        public Nullable<int> DocumentCount { get; set; }
        public string PartyID { get; set; }
        public string PartyName { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
    }
    public class GetAllDocuments
    {
        public string Email { get; set; }
        //public int CaseInstanceID { get; set; }
        public long CaseInstanceID { get; set; }

        public string Flag { get; set; }
    }
    public class CaseLitDetails
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class getCaseList
    {
            public string Email { get; set; }
    }

    public class myreminderTitleclass
    {
        public string Email { get; set; }

        public string Type { get; set; }

    }
    public class mylocationclass
    {
        public string Email { get; set; }

    }
    public class myopponentclass
    {
        public string Email { get; set; }

    }
    public class myownerlistclass
    {
        public string Email { get; set; }

        public int Status { get; set; }

    }
    public class myLegalclass
    {
        public string Email { get; set; }

    }
    public class MyDocumentclass
    {
        public string Email { get; set; }

        public string CheckType { get; set; }

        public string Type { get; set; }

        public int InstanceID { get; set; }

        public int TaskID { get; set; }
        public int FileId { get; set; }


        internal class OutputDetil
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }


        public class MyReportLitigation
        {
            public List<sp_LitigationNoticeReportWithCustomParameter_Result> Notice { get; set; }
            public List<SP_LitigationCaseReportWithCustomParameter_Result> Case { get; set; }
        }


        public class DepartmentList
        {
            public string DepartmentName { get; set; }
            public int DepartmentID { get; set; }
        }

      

        public class mydocdetails
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int ID1 { get; set; }
            public string Name1 { get; set; }
            public int ID2 { get; set; }
            public string Name2 { get; set; }
        }


        //internal class mydocumenttype
        //{
        //    public string RefNo { get; set; }
        //    public string Title { get; set; }
        //    public string PartyName { get; set; }
        //    public string PartyID { get; set; }
        //    public string DepartmentID { get; set; }
        //    public Nullable<int> CustomerBranchID { get; set; }
        //    public string Priority { get; set; }
        //    public string Task { get; set; }
        //    public string Task_Description { get; set; }
        //    public string Status { get; set; }
        //    public string Assigned_To { get; set; }
        //    public DateTime Due_date { get; set; }
        //    public string TypeName { get; set; }
        //    public string InstanceID { get; set; }
        //    public string BranchName { get; set; }
        //    public string FYName { get; set; }

        //    public string TaskType { get; set; }
        //}


      

    }

}