﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class NoticeManagement
    {
        public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int noticeStatus, string noticeType) /*int branchID*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    //if (noticeStatus == 3) //3--Closed otherwise Open
                    //    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    //else
                    //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();//required by nikhil
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.FYName

                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 FYName = GCS.Key.FYName
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }


        #region Amol
        public static bool DeleteNoticeByID(int noticeInstanceID, int deletedByUserID)
        {
            try
            {
                bool deleteSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<tbl_LitigationFileData> noticeDocToDelete = entities.tbl_LitigationFileData
                                                                .Where(x => x.NoticeCaseInstanceID == noticeInstanceID
                                                                && (x.DocType.Trim() == "N" || x.DocType.Trim() == "NT" || x.DocType.Trim() == "NR")).ToList();

                    if (noticeDocToDelete.Count > 0)
                    {
                        noticeDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        noticeDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        noticeDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_LegalNoticeResponse> ResponsesToDelete = entities.tbl_LegalNoticeResponse.Where(x => x.NoticeInstanceID == noticeInstanceID).ToList();

                    if (ResponsesToDelete.Count > 0)
                    {
                        ResponsesToDelete.ForEach(entry => entry.IsActive = false);
                        ResponsesToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponsesToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_TaskScheduleOn> tasksToDelete = entities.tbl_TaskScheduleOn
                                                            .Where(x => x.NoticeCaseInstanceID == noticeInstanceID
                                                            && x.TaskType == "N").ToList();

                    if (tasksToDelete.Count > 0)
                    {
                        tasksToDelete.ForEach(entry => entry.IsActive = false);
                        tasksToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        tasksToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    var queryRating = (from row in entities.tbl_LawyerListRating
                                       where row.CaseNoticeID == noticeInstanceID
                                       && row.Type == 2
                                       select row).ToList();

                    if (queryRating.Count > 0)
                    {
                        queryRating.ForEach(entry => entry.IsActive = false);
                        queryRating.ForEach(entry => entry.UpdatedBy = deletedByUserID);
                        queryRating.ForEach(entry => entry.UpdatedOn = DateTime.Now);
                        entities.SaveChanges();

                        queryRating.ForEach(eachRatingRecord =>
                        {
                            var LawyerRating = (from row in entities.sp_LiGetLawyerRatingAVG(Convert.ToInt32(eachRatingRecord.LawyerID))
                                                select row).FirstOrDefault();

                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                var checkUser = (from row in entities.tbl_LawyerFinalRating
                                                 where row.LawyerID == objNewRate.LawyerID
                                                 && row.IsActive == true
                                                 select row).FirstOrDefault();
                                if (checkUser == null)
                                {
                                    objNewRate.CreatedBy = deletedByUserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    entities.tbl_LawyerFinalRating.Add(objNewRate);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkUser.Rating = objNewRate.Rating;
                                    checkUser.UpdatedBy = objNewRate.UpdatedBy;
                                    checkUser.UpdatedOn = objNewRate.UpdatedOn;
                                    entities.SaveChanges();
                                }
                            }
                        });
                    }

                    var queryResult = (from row in entities.tbl_LegalNoticeInstance
                                       where row.ID == noticeInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                    else
                        deleteSuccess = false;

                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<View_NoticeAssignedInstanceNew> GetAssignedNoticeListNew(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID, int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeAssignedInstanceNew
                             where row.CustomerID == CustomerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    if (noticeStatus == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.NoticeRiskID,
                                 g.NoticeDate,
                                 g.Status,
                                 g.ActName,
                                 g.OwnerName                                
                             } into GCS
                             select new View_NoticeAssignedInstanceNew()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 NoticeRiskID = GCS.Key.NoticeRiskID,
                                 NoticeDate = GCS.Key.NoticeDate,
                                 Status = GCS.Key.Status,
                                 ActName = GCS.Key.ActName,
                                 OwnerName = GCS.Key.OwnerName
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }

        public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID,int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             where row.CustomerID == CustomerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    if (noticeStatus == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.NoticeRiskID
                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 NoticeRiskID = GCS.Key.NoticeRiskID
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }
        #endregion

        #region Gaurav

        public static int GetAssignedNoticeCount(List<View_NoticeAssignedInstance> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             select row).ToList();

                if (noticeStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                else
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeTerm,
                                 g.NoticeDetailDesc,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName
                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeTerm = GCS.Key.NoticeTerm,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                             }).ToList();
                }

                return query.Count();
            }
        }
        public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, string noticeType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.TxnStatusID
                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 TxnStatusID = GCS.Key.TxnStatusID
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }
        #endregion
    }
}