﻿using AppWebApplication.DataLitigation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class TaxationMethods
    {

        public static bool DeleteCCStatusDetails(int ID, string DeletedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var queryResult = (from row in entities.tax_tbl_CommercialCaseStatus
                                       where row.ID == ID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(queryResult.CCaseInstanceID), Convert.ToInt32(DeletedBy), "Case Status deleted", DateTime.Now.ToString("dd-MM-yyyy"));
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddCCaseResult(string CaseStatus, string CloseDate, string Remarks, int CustomerID, int CCaseID, string IViewState, int Id, int CreatedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {

                    var query = (from row in entities.tax_tbl_CommercialCaseStatus
                                 where row.CCaseInstanceID == CCaseID
                                 && row.StatusID == CaseStatus
                                 && row.CustomerID == CustomerID
                                 && row.IsDeleted == false
                                 select row);

                    if (!string.IsNullOrEmpty(CloseDate) && CloseDate != "day-month-year")
                    {
                        DateTime Od = DateTime.ParseExact(CloseDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                        query = query.Where(x => x.CloseDate == Od);
                    }

                    if (IViewState == "1" && Id != 0)
                    {
                        if (Id > 0)
                        {
                            query = query.Where(entry => entry.ID != Id);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_CommercialCaseStatus
                                                where row.ID == Id
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.StatusID = CaseStatus;
                                recordtoEdit.ClosureRemark = Remarks;
                                recordtoEdit.CustomerID = CustomerID;
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;
                                if (CaseStatus == "Pending/Open")
                                {
                                    recordtoEdit.CloseDate = null;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(CloseDate) && CloseDate != "day-month-year")
                                    {
                                        recordtoEdit.CloseDate = DateTime.ParseExact(CloseDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    }
                                }
                                entities.SaveChanges();
                                TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Case Status Updated", DateTime.Now.ToString("dd-MM-yyyy"));
                                return "Status Details Updated Successfully";
                            }
                            else
                            {
                                return "Error at Updating Status Details";
                            }
                        }
                        else
                        {
                            return "Status Details with same name already exists";
                        }

                    }
                    else if (!query.Any() && Id == 0)
                    {
                        tax_tbl_CommercialCaseStatus objResult = new tax_tbl_CommercialCaseStatus()
                        {
                            CCaseInstanceID = CCaseID,
                            StatusID = CaseStatus,
                            ClosureRemark = Remarks,
                            IsActive = true,
                            CustomerID = CustomerID,
                            CreatedBy = CreatedBy,
                            IsDeleted = false
                        };

                        if (CaseStatus == "Disposed/Closed")
                        {
                            if (!string.IsNullOrEmpty(CloseDate) && CloseDate != "day-month-year")
                            {
                                objResult.CloseDate = DateTime.ParseExact(CloseDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                        }

                        bool saveSuccess = CreateCCStatusDetails(objResult);
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Case Status Created", DateTime.Now.ToString("dd-MM-yyyy"));

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                    }
                    else
                    {
                        return "Status with same Details already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Commercial Case Status.";
            }
        }

        public static bool CreateCCStatusDetails(tax_tbl_CommercialCaseStatus newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_CommercialCaseStatus.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }



        public static List<tax_tbl_CommercialCaseFileData> GetCCDocuments(long CCaseID, string DocType, long ID)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var AllDocument = (from row in entities.tax_tbl_CommercialCaseFileData
                                   where row.CommercialCaseInstanceID == CCaseID
                                   && row.DocType == DocType
                                   && row.ID == ID
                                   && row.IsDeleted == false
                                   select row).ToList();

                return AllDocument;
            }
        }

        public static List<tax_tbl_CommercialCaseFileData> GetCCDocumentsByDocType(long CCaseID, string Doctype)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var AllDocument = (from row in entities.tax_tbl_CommercialCaseFileData
                                   where row.CommercialCaseInstanceID == CCaseID
                                   && row.IsDeleted == false
                                   select row).ToList();

                if (Doctype == "A")
                {
                    return AllDocument;
                }
                else if (Doctype == "C")
                {
                    AllDocument = AllDocument.Where(x => x.DocType == "CD").ToList();
                    return AllDocument;
                }
                else if (Doctype == "AT")
                {
                    AllDocument = AllDocument.Where(x => x.DocType == "Action").ToList();
                    return AllDocument;
                }
                else if (Doctype == "ADO")
                {
                    AllDocument = AllDocument.Where(x => x.DocType == "AuditObser").ToList();
                    return AllDocument;
                }
                else if (Doctype == "ADR")
                {
                    AllDocument = AllDocument.Where(x => x.DocType == "AuditReply").ToList();
                    return AllDocument;
                }
                else
                {
                    return AllDocument;
                }
            }
        }


        public static bool DeleteCCaseDocuments(int FileID, string DeletedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var queryResult = (from row in entities.tax_tbl_CommercialCaseFileData
                                       where row.ID == FileID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(queryResult.CommercialCaseInstanceID), Convert.ToInt32(DeletedBy), "Case Document deleted", DateTime.Now.ToString("dd-MM-yyyy"));
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteActionTakenDetails(int ID, string DeletedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var queryResult = (from row in entities.tax_tbl_ActionTakenDetails
                                       where row.ID == ID
                                       select row).FirstOrDefault();

                    var queryResultFile = (from row in entities.tax_tbl_CommercialCaseFileData
                                           where row.ResponseID == ID
                                           select row).ToList();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(queryResult.CCaseID), Convert.ToInt32(DeletedBy), "Entry is deleted in Action Taken Details", DateTime.Now.ToString("dd-MM-yyyy"));
                        entities.SaveChanges();
                    }
                    if (queryResultFile.Count > 0)
                    {
                        foreach (var item in queryResultFile)
                        {
                            item.IsDeleted = true;
                            entities.SaveChanges();
                        }
                        return true;
                    }
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteAuditObservationDetails(int ID, string DeletedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var queryResult = (from row in entities.tax_tbl_AuditObservationDetails
                                       where row.ID == ID
                                       select row).FirstOrDefault();

                    var queryResultFile = (from row in entities.tax_tbl_CommercialCaseFileData
                                           where row.ResponseID == ID
                                           select row).ToList();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(queryResult.CCaseID), Convert.ToInt32(DeletedBy), "Entry is deleted in Audit Observation Details", DateTime.Now.ToString("dd-MM-yyyy"));
                        entities.SaveChanges();
                    }
                    if (queryResultFile.Count > 0)
                    {
                        foreach (var item in queryResultFile)
                        {
                            item.IsDeleted = true;
                            entities.SaveChanges();
                        }
                        return true;
                    }
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tax_tbl_CommercialCaseFileData> GetCCDocumentsByIds(long CCaseID, string DocTypes, long ResponseID)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var AllDocument = (from row in entities.tax_tbl_CommercialCaseFileData
                                   where row.CommercialCaseInstanceID == CCaseID
                                   && row.ResponseID == ResponseID
                                   && row.IsDeleted == false
                                   select row).ToList();

                AllDocument = AllDocument.Where(item => DocTypes.Contains(item.DocType)).ToList();

                return AllDocument;
            }
        }

        public static List<tax_tbl_CommercialCaseFileData> GetCCDocumentsById(long CCaseID, string DocType, long ResponseID)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var AllDocument = (from row in entities.tax_tbl_CommercialCaseFileData
                                   where row.CommercialCaseInstanceID == CCaseID
                                   && row.DocType == DocType
                                   && row.ResponseID == ResponseID
                                   && row.IsDeleted == false
                                   select row).ToList();

                return AllDocument;
            }
        }

        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

        public static string AddICaseDetails(int ForumID, string OpenDate, string TaxCategory, string CategoryNature, string CustomerBranchID, string CaseType, string DemandPeriod,
         string Assessee, string AssesseeName, string ReasonCl, string WhetherPassthrough, string CustomerSCN, string CaseBrief, string DealingOfficerName, string DealingOfficerCPF,
         string HeadFinance, string ContingentAmount, string SAPNo, string ConsultantName, string ConsultantAddress, int CustomerID, int ICaseID, string IViewState, int UserID, string MergeCaseNos, string CreatedByCommercial, string CreatedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_ILegalCaseInstance
                                 where row.ID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    var ForumName = (from row in entities.tax_tbl_ForumPendingAt
                                     where row.ID == ForumID
                                     && row.CustomerID == CustomerID && row.IsDeleted == false
                                     select row.ForumLocation).FirstOrDefault();
                    if (IViewState == "1")
                    {
                        if (ICaseID != 0)
                        {
                            query = query.Where(entry => entry.ID != ICaseID);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_ILegalCaseInstance
                                                where row.ID == ICaseID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.TaxCategory = Convert.ToInt32(TaxCategory);
                                recordtoEdit.CategoryNature = CategoryNature;
                                recordtoEdit.CaseType = CaseType;
                                recordtoEdit.CustomerBranchID = CustomerBranchID;
                                // recordtoEdit.ForumID = Convert.ToInt32(ForumID);
                                recordtoEdit.Assessee = Convert.ToInt32(Assessee);
                                recordtoEdit.AssesseeName = AssesseeName;
                                recordtoEdit.ReasonCl = ReasonCl;
                                recordtoEdit.DemandPeriod = DemandPeriod;
                                recordtoEdit.WhetherpassThrough = WhetherPassthrough;
                                recordtoEdit.CustomerName = CustomerSCN;
                                recordtoEdit.CaseBrief = CaseBrief;
                                recordtoEdit.DealingOfficerName = DealingOfficerName;
                                recordtoEdit.DealingOfficerCPF = DealingOfficerCPF;
                                recordtoEdit.HeadFinance = HeadFinance;
                                recordtoEdit.ContingentAmount = ContingentAmount;
                                recordtoEdit.ConsultantName = ConsultantName;
                                recordtoEdit.SAPDocNo = SAPNo;
                                recordtoEdit.ConsultantAddress = ConsultantAddress;
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                                return "Indirect Tax Case Details Updated Successfully";
                            }
                        }
                        else
                        {
                            return "Indirect Tax Case with same details already exists";
                        }

                    }
                    else if (!query.Any() && IViewState == "0")
                    {
                        tax_tbl_ILegalCaseInstance objCase = new tax_tbl_ILegalCaseInstance()
                        {
                            OpenDate = DateTime.ParseExact(OpenDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            CategoryNature = CategoryNature,
                            CaseType = CaseType,
                            CustomerBranchID = CustomerBranchID,
                            ForumID = Convert.ToInt32(ForumID),
                            AssesseeName = AssesseeName,
                            ReasonCl = ReasonCl,
                            DemandPeriod = DemandPeriod,
                            WhetherpassThrough = WhetherPassthrough,
                            CustomerName = CustomerSCN,
                            CaseBrief = CaseBrief,
                            DealingOfficerName = DealingOfficerName,
                            DealingOfficerCPF = DealingOfficerCPF,
                            HeadFinance = HeadFinance,
                            ContingentAmount = ContingentAmount,
                            ConsultantName = ConsultantName,
                            SAPDocNo = SAPNo,
                            ConsultantAddress = ConsultantAddress,
                            IsDeleted = false,
                            CustomerID = CustomerID,
                            UserID = UserID,
                            MergeCaseNO = MergeCaseNos,
                            CreatedByCommercial = CreatedByCommercial,
                            CreatedBy = CreatedBy
                        };

                        if (Assessee != null)
                        {
                            objCase.Assessee = Convert.ToInt32(Assessee);
                        }
                        if (TaxCategory != null)
                        {
                            objCase.TaxCategory = Convert.ToInt32(TaxCategory);
                        }

                        long saveSuccess = CreateICaseDetails(objCase);

                        if (saveSuccess > 0)
                        {

                            tax_tbl_IndirectCaseStatus objStatus = new tax_tbl_IndirectCaseStatus()
                            {
                                ICaseInstanceID = saveSuccess,
                                ForumID = Convert.ToInt32(ForumID),
                                Forum = Convert.ToString(ForumName),
                                StatusID = "1",
                                statusName = "Open",
                                CloseDate = DateTime.Now,
                                CustomerID = CustomerID,
                                CreatedBy = UserID,
                                CreatedOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false
                            };

                            entities.tax_tbl_IndirectCaseStatus.Add(objStatus);
                            entities.SaveChanges();

                            if (!string.IsNullOrEmpty(MergeCaseNos))
                            {
                                List<SP_IndirectCaseWorkspaceReport_Result> objIC = new List<SP_IndirectCaseWorkspaceReport_Result>();

                                objIC = (from row in entities.SP_IndirectCaseWorkspaceReport(CustomerID)
                                         where row.ICaseNo != null
                                         select row).OrderByDescending(entry => entry.ICaseInstanceID).ToList();

                                MergeCaseNos = MergeCaseNos + ',';
                                if (MergeCaseNos != ",")
                                {
                                    objIC = objIC.Where(item => MergeCaseNos.Contains(item.ICaseNo + ',')).ToList();
                                }

                                if (objIC.Count != 0)
                                {
                                    foreach (var item in objIC)
                                    {
                                        tax_tbl_IndirectCaseStatus objMergeCasesStatus = new tax_tbl_IndirectCaseStatus()
                                        {
                                            ICaseInstanceID = item.ICaseInstanceID,
                                            ForumID = Convert.ToInt32(item.ForumID),
                                            Forum = item.Forum,
                                            StatusID = "3",
                                            statusName = "Merged",
                                            CloseDate = DateTime.Now,
                                            CustomerID = CustomerID,
                                            CreatedBy = UserID,
                                            CreatedOn = DateTime.Now,
                                            IsActive = true,
                                            IsDeleted = false
                                        };
                                        entities.tax_tbl_IndirectCaseStatus.Add(objMergeCasesStatus);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            return Convert.ToString(saveSuccess);
                        }
                        else
                        {
                            return "0";
                        }
                    }
                    else
                    {
                        return "Indirect Tax with same Case No already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Indirect Tax Case Details";
            }
        }

        public static long CreateICaseDetails(tax_tbl_ILegalCaseInstance newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_ILegalCaseInstance.Add(newRecord);
                    entities.SaveChanges();

                    return newRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool AddProtestMoney(int ForumID, string PreDepositAmount, string DatePreDepositAmount, string PreDepositRemarks, string UnderProtestAmount, string DateUnderProtestAmount, string AmtPaidProtestRemarks, string ICaseID, int CustId, int ID, string ViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {

                    if (ViewState == "1" && ID != 0)
                    {

                        if (ID > 0)
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_ICDeptProtestmoney
                                                where row.ID == ID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.PreDepositAmount = PreDepositAmount;
                                recordtoEdit.UnderProtestAmount = UnderProtestAmount;
                                recordtoEdit.PreDepositRemarks = PreDepositRemarks;
                                recordtoEdit.AmtPaidProtestRemarks = AmtPaidProtestRemarks;
                                if (!string.IsNullOrEmpty(DatePreDepositAmount) && DatePreDepositAmount != "day-month-year")
                                {
                                    recordtoEdit.DatePreDepositAmount = DateTime.ParseExact(DatePreDepositAmount, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                if (!string.IsNullOrEmpty(DateUnderProtestAmount) && DateUnderProtestAmount != "day-month-year")
                                {
                                    recordtoEdit.DateUnderProtestAmount = DateTime.ParseExact(DateUnderProtestAmount, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                recordtoEdit.IsDeleted = false;
                                entities.SaveChanges();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }

                    }
                    if (ViewState == "0")
                    {
                        tax_tbl_ICDeptProtestmoney objcategory = new tax_tbl_ICDeptProtestmoney()
                        {
                            ForumID = ForumID,
                            PreDepositAmount = PreDepositAmount,
                            UnderProtestAmount = UnderProtestAmount,
                            PreDepositRemarks = PreDepositRemarks,
                            AmtPaidProtestRemarks = AmtPaidProtestRemarks,
                            IsDeleted = false,
                            ICaseID = ICaseID,
                            CustomerID = CustId
                        };
                        if (!string.IsNullOrEmpty(DatePreDepositAmount) && DatePreDepositAmount != "day-month-year")
                        {
                            objcategory.DatePreDepositAmount = DateTime.ParseExact(DatePreDepositAmount, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(DateUnderProtestAmount) && DateUnderProtestAmount != "day-month-year")
                        {
                            objcategory.DateUnderProtestAmount = DateTime.ParseExact(DateUnderProtestAmount, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        bool saveSuccess = CreateProtestmoney(objcategory);

                        if (saveSuccess)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool CreateProtestmoney(tax_tbl_ICDeptProtestmoney newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    entities.tax_tbl_ICDeptProtestmoney.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddDetailsInDeptForum(string DIN_No, string DIN_Date, string IssuingAuthority, string DateRecieptSCN, string GroundsSCN,
            string Disputed_Tax_Amount, string Disputed_Penalty, string Disputed_Interest, string Disputed_Fine, string Total_Disputed_Amount, string Remarks,
            string OIONoBySCN, string OIODateBySCN, string txtOIOIssuingSCN, string OIOReceiptDate, string GroundforpassingOIOBydeptSCN, string drdSCNAppealFiledYN, string drpappealfilledwith,
            string ApealFilledDate, string RepliedfiledByDate, string RepliedfiledOnDate, string OngcDefence, string RepliedfiledWith, string OIOTaxAmount, string OIOPenalty, string OIOInterest,
            string OIOFine, string OIOTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_IndirectCaseDeptForum
                                 where row.ICaseID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (IViewState == 1 && ICaseID != 0)
                    {
                        if (ICaseID != 0)
                        {
                            query = query.Where(entry => entry.ICaseID != ICaseID);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_IndirectCaseDeptForum
                                                where row.ICaseID == ICaseID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.SCNDINNo = DIN_No;
                                if (!string.IsNullOrEmpty(DIN_Date) && DIN_Date != "day-month-year")
                                {
                                    recordtoEdit.SCNDINDate = DateTime.ParseExact(DIN_Date, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.SCNDINDate = null;
                                }
                                recordtoEdit.IssuingAuthority = IssuingAuthority;
                                if (!string.IsNullOrEmpty(DateRecieptSCN) && DateRecieptSCN != "day-month-year")
                                {
                                    recordtoEdit.DateRecieptSCN = DateTime.ParseExact(DateRecieptSCN, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.DateRecieptSCN = null;
                                }
                                recordtoEdit.GroundsSCN = GroundsSCN;
                                recordtoEdit.DisputedTax_Amount = Disputed_Tax_Amount;
                                recordtoEdit.DisputedPenalty = Disputed_Penalty;
                                recordtoEdit.DisputedInterest = Disputed_Interest;
                                recordtoEdit.DisputedFine = Disputed_Fine;
                                recordtoEdit.TotalDisputedAmount = Total_Disputed_Amount;
                                recordtoEdit.Remarks = Remarks;
                                recordtoEdit.OIONoBySCN = OIONoBySCN;
                                if (!string.IsNullOrEmpty(OIODateBySCN) && OIODateBySCN != "day-month-year")
                                {
                                    recordtoEdit.OIODateBySCN = DateTime.ParseExact(OIODateBySCN, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.OIODateBySCN = null;
                                }
                                recordtoEdit.txtOIOIssuingSCN = txtOIOIssuingSCN;

                                if (!string.IsNullOrEmpty(OIOReceiptDate) && OIOReceiptDate != "day-month-year")
                                {
                                    recordtoEdit.OIOReceiptDate = DateTime.ParseExact(OIOReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.OIOReceiptDate = null;
                                }
                                recordtoEdit.GroundforpassingOIOBydeptSCN = GroundforpassingOIOBydeptSCN;
                                recordtoEdit.drdSCNAppealFiledYN = drdSCNAppealFiledYN;

                                if (recordtoEdit.drdSCNAppealFiledYN == "Y")
                                {
                                    recordtoEdit.ApealFilledDate = DateTime.ParseExact(ApealFilledDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.drpappealfilledwith = null;
                                    recordtoEdit.ApealFilledDate = null;
                                }

                                recordtoEdit.OIOTaxAmount = OIOTaxAmount;
                                recordtoEdit.OIOPenalty = OIOPenalty;
                                recordtoEdit.OIOInterest = OIOInterest;
                                recordtoEdit.OIOFine = OIOFine;
                                recordtoEdit.OIOTotalAmount = OIOTotalAmount;

                                if (!string.IsNullOrEmpty(RepliedfiledByDate) && RepliedfiledByDate != "day-month-year")
                                {
                                    recordtoEdit.RepliedfiledByDate = DateTime.ParseExact(RepliedfiledByDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                if (!string.IsNullOrEmpty(RepliedfiledOnDate) && RepliedfiledOnDate != "day-month-year")
                                {
                                    recordtoEdit.RepliedfiledOnDate = DateTime.ParseExact(RepliedfiledOnDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.RepliedfiledOnDate = null;
                                }
                                recordtoEdit.OngcDefence = OngcDefence;
                                recordtoEdit.RepliedfiledWith = RepliedfiledWith;

                                recordtoEdit.CustomerID = CustomerID;
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                                return "Updated Successfully";
                            }
                            else
                            {
                                return "";
                            }

                        }
                        else
                        {
                            return "Details with same name already exists";
                        }

                    }
                    else if (!query.Any())
                    {
                        tax_tbl_IndirectCaseDeptForum objcategory = new tax_tbl_IndirectCaseDeptForum()
                        {
                            ICaseID = ICaseID,
                            SCNDINNo = DIN_No,
                            IssuingAuthority = IssuingAuthority,
                            GroundsSCN = GroundsSCN,
                            DisputedTax_Amount = Disputed_Tax_Amount,
                            DisputedPenalty = Disputed_Penalty,
                            DisputedInterest = Disputed_Interest,
                            DisputedFine = Disputed_Fine,
                            TotalDisputedAmount = Total_Disputed_Amount,
                            Remarks = Remarks,
                            OIONoBySCN = OIONoBySCN,
                            txtOIOIssuingSCN = txtOIOIssuingSCN,
                            GroundforpassingOIOBydeptSCN = GroundforpassingOIOBydeptSCN,
                            drdSCNAppealFiledYN = drdSCNAppealFiledYN,
                        };

                        if (!string.IsNullOrEmpty(DIN_Date) && DIN_Date != "day-month-year")
                        {
                            objcategory.SCNDINDate = DateTime.ParseExact(DIN_Date, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.SCNDINDate = null;
                        }

                        if (!string.IsNullOrEmpty(DateRecieptSCN) && DateRecieptSCN != "day-month-year")
                        {
                            objcategory.DateRecieptSCN = DateTime.ParseExact(DateRecieptSCN, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.DateRecieptSCN = null;
                        }

                        if (!string.IsNullOrEmpty(OIODateBySCN) && OIODateBySCN != "day-month-year")
                        {
                            objcategory.OIODateBySCN = DateTime.ParseExact(OIODateBySCN, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIODateBySCN = null;
                        }

                        if (!string.IsNullOrEmpty(OIOReceiptDate) && OIOReceiptDate != "day-month-year")
                        {
                            objcategory.OIOReceiptDate = DateTime.ParseExact(OIOReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIOReceiptDate = null;
                        }

                        if (objcategory.drdSCNAppealFiledYN == "Y")
                        {
                            objcategory.drpappealfilledwith = Convert.ToInt32(drpappealfilledwith);
                            objcategory.ApealFilledDate = DateTime.ParseExact(ApealFilledDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        objcategory.OIOTaxAmount = OIOTaxAmount;
                        objcategory.OIOPenalty = OIOPenalty;
                        objcategory.OIOInterest = OIOInterest;
                        objcategory.OIOFine = OIOFine;
                        objcategory.OIOTotalAmount = OIOTotalAmount;

                        if (!string.IsNullOrEmpty(RepliedfiledByDate) && RepliedfiledByDate != "day-month-year")
                        {
                            objcategory.RepliedfiledByDate = DateTime.ParseExact(RepliedfiledByDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(RepliedfiledOnDate) && RepliedfiledOnDate != "day-month-year")
                        {
                            objcategory.RepliedfiledOnDate = DateTime.ParseExact(RepliedfiledOnDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                        }
                        else
                        {
                            objcategory.RepliedfiledOnDate = null;
                        }
                        objcategory.OngcDefence = OngcDefence;
                        objcategory.RepliedfiledWith = RepliedfiledWith;

                        objcategory.CustomerID = CustomerID;
                        objcategory.IsDeleted = false;
                        objcategory.UpdatedOn = DateTime.Now;

                        bool saveSuccess = CreateDeptFormforIC(objcategory);

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                        else
                        {
                            return "Error at adding details at Department Forum";
                        }
                    }
                    else
                    {
                        return "Details already exists";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Department Forum Details";
            }
        }


        public static bool CreateDeptFormforIC(tax_tbl_IndirectCaseDeptForum newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_IndirectCaseDeptForum.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddDetailsInAppellateForum(string OIODINNo, string OIODINDate, string OIOIssuingAuthority, string OIOOrderReceiptDate,
        string OIOAppealFiledBy, string OIOAppealNo, string OIOAppealDate, string OIOGrounds, string OIADinNoByOIO, string OIADinDateByOIO, string OIAIssuingAuthByOIO,
        string OIADinReceiptDateByOIO, string drdAppealFiledYNByOIOYN, string OIOappealfiledwith, string OIOAppealfiledbydate,
        string OIATaxAmount, string OIAPenalty, string OIAInterest, string OIAFine, string OIATotalAmount,
         string OIOAppealTaxAmount, string OIOAppealInterest, string OIOAppealTaxPenalty, string OIOAppealFine, string OIOAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_IndirectCaseIstAppealForum
                                 where row.ICaseID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (IViewState == 1 && ICaseID != 0)
                    {
                        var recordtoEdit = (from row in entities.tax_tbl_IndirectCaseIstAppealForum
                                            where row.ICaseID == ICaseID
                                            select row).FirstOrDefault();

                        if (recordtoEdit != null)
                        {
                            recordtoEdit.OIODINNo = OIODINNo;
                            if (!string.IsNullOrEmpty(OIODINDate) && OIODINDate != "day-month-year")
                            {
                                recordtoEdit.OIODINDate = DateTime.ParseExact(OIODINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIODINDate = null;
                            }
                            recordtoEdit.OIOIssuingAuthority = OIOIssuingAuthority;

                            if (!string.IsNullOrEmpty(OIOOrderReceiptDate) && OIOOrderReceiptDate != "day-month-year")
                            {
                                recordtoEdit.OIOOrderReceiptDate = DateTime.ParseExact(OIOOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIOOrderReceiptDate = null;
                            }
                            recordtoEdit.OIOAppealFiledBy = OIOAppealFiledBy;
                            recordtoEdit.OIOAppealNo = OIOAppealNo;

                            if (!string.IsNullOrEmpty(OIOAppealDate) && OIOAppealDate != "day-month-year")
                            {
                                recordtoEdit.OIOAppealDate = DateTime.ParseExact(OIOAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIOAppealDate = null;
                            }
                            recordtoEdit.OIOGrounds = OIOGrounds;
                            recordtoEdit.OIADinNoByOIO = OIADinNoByOIO;
                            if (!string.IsNullOrEmpty(OIADinDateByOIO) && OIADinDateByOIO != "day-month-year")
                            {
                                recordtoEdit.OIADinDateByOIO = DateTime.ParseExact(OIADinDateByOIO, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIADinDateByOIO = null;
                            }
                            recordtoEdit.OIAIssuingAuthByOIO = OIAIssuingAuthByOIO;

                            if (!string.IsNullOrEmpty(OIADinReceiptDateByOIO) && OIADinReceiptDateByOIO != "day-month-year")
                            {
                                recordtoEdit.OIADinReceiptDateByOIO = DateTime.ParseExact(OIADinReceiptDateByOIO, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIADinReceiptDateByOIO = null;
                            }
                            recordtoEdit.drdAppealFiledYNByOIOYN = drdAppealFiledYNByOIOYN;
                            if (recordtoEdit.drdAppealFiledYNByOIOYN == "Y")
                            {
                                //   recordtoEdit.OIOappealfiledwith_ = OIOappealfiledwith;
                                recordtoEdit.OIOAppealfiledbydate = DateTime.ParseExact(OIOAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            recordtoEdit.OIATaxAmount = OIATaxAmount;
                            recordtoEdit.OIAPenalty = OIAPenalty;
                            recordtoEdit.OIAInterest = OIAInterest;
                            recordtoEdit.OIAFine = OIAFine;
                            recordtoEdit.OIATotalAmount = OIATotalAmount;

                            recordtoEdit.OIOAppealTaxAmount = OIOAppealTaxAmount;
                            recordtoEdit.OIOAppealInterest = OIOAppealInterest;
                            recordtoEdit.OIOAppealTaxPenalty = OIOAppealTaxPenalty;
                            recordtoEdit.OIOAppealFine = OIOAppealFine;
                            recordtoEdit.OIOAppealTotalAmount = OIOAppealTotalAmount;

                            recordtoEdit.CustomerID = CustomerID;
                            recordtoEdit.IsDeleted = false;
                            recordtoEdit.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                            return "Updated Successfully";
                        }

                        else
                        {
                            return "Details with same name already exists";
                        }
                    }


                    else if (!query.Any())
                    {
                        tax_tbl_IndirectCaseIstAppealForum objcategory = new tax_tbl_IndirectCaseIstAppealForum()
                        {
                            ICaseID = ICaseID,
                            OIODINNo = OIODINNo,
                            OIOIssuingAuthority = OIOIssuingAuthority,
                            OIOAppealFiledBy = OIOAppealFiledBy,
                            OIOAppealNo = OIOAppealNo,
                            OIOGrounds = OIOGrounds,
                            OIADinNoByOIO = OIADinNoByOIO,
                            OIAIssuingAuthByOIO = OIAIssuingAuthByOIO,
                            drdAppealFiledYNByOIOYN = drdAppealFiledYNByOIOYN,
                            CustomerID = CustomerID,
                            IsDeleted = false
                        };
                        if (!string.IsNullOrEmpty(OIODINDate) && OIODINDate != "day-month-year")
                        {
                            objcategory.OIODINDate = DateTime.ParseExact(OIODINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIODINDate = null;
                        }

                        if (!string.IsNullOrEmpty(OIOOrderReceiptDate) && OIOOrderReceiptDate != "day-month-year")
                        {
                            objcategory.OIOOrderReceiptDate = DateTime.ParseExact(OIOOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIOOrderReceiptDate = null;
                        }
                        if (!string.IsNullOrEmpty(OIOAppealDate) && OIOAppealDate != "day-month-year")
                        {
                            objcategory.OIOAppealDate = DateTime.ParseExact(OIOAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIOAppealDate = null;
                        }

                        if (!string.IsNullOrEmpty(OIADinDateByOIO) && OIADinDateByOIO != "day-month-year")
                        {
                            objcategory.OIADinDateByOIO = DateTime.ParseExact(OIADinDateByOIO, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIADinDateByOIO = null;
                        }

                        if (!string.IsNullOrEmpty(OIADinReceiptDateByOIO) && OIADinReceiptDateByOIO != "day-month-year")
                        {
                            objcategory.OIADinReceiptDateByOIO = DateTime.ParseExact(OIADinReceiptDateByOIO, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIADinReceiptDateByOIO = null;
                        }

                        if (objcategory.drdAppealFiledYNByOIOYN == "Y")
                        {
                            objcategory.OIOappealfiledwith = OIOappealfiledwith;
                            objcategory.OIOAppealfiledbydate = DateTime.ParseExact(OIOAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        objcategory.OIATaxAmount = OIATaxAmount;
                        objcategory.OIAPenalty = OIAPenalty;
                        objcategory.OIAInterest = OIAInterest;
                        objcategory.OIAFine = OIAFine;
                        objcategory.OIATotalAmount = OIATotalAmount;

                        objcategory.OIOAppealTaxAmount = OIOAppealTaxAmount;
                        objcategory.OIOAppealInterest = OIOAppealInterest;
                        objcategory.OIOAppealTaxPenalty = OIOAppealTaxPenalty;
                        objcategory.OIOAppealFine = OIOAppealFine;
                        objcategory.OIOAppealTotalAmount = OIOAppealTotalAmount;

                        bool saveSuccess = CreateAppellateFormforIC(objcategory);

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                    }
                    else
                    {
                        return "Details already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Ist Appellate Forum Details";
            }
        }

        public static bool CreateAppellateFormforIC(tax_tbl_IndirectCaseIstAppealForum newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_IndirectCaseIstAppealForum.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddDetailsInTribunalForum(string OIADINNo, string OIADINDate, string OIAIssuingAuthority, string OIAOrderReceiptDate,
               string OIAAppealFiledBy, string OIAAppealNo, string OIAAppealDate, string OIAGrounds, string HCDinNoByOIA, string HCDinDateByOIA, string HCIssuingAuthByOIA, string HCDinReceiptDateByOIA,
              string drdAppealFiledYNByOIAYN, string OIAappealfiledwith, string OIAAppealfiledbydate, string TribunalTaxAmount, string TribunalPenalty, string TribunalInterest, string TribunalFine, string TribunalTotalAmount,
              string TribunalAppealTaxAmount, string TribunalAppealInterest, string TribunalAppealTaxPenalty, string TribunalAppealFine, string TribunalAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_IndirectCaseTribunalForum
                                 where row.ICaseID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (IViewState == 1 && ICaseID != 0)
                    {
                        var recordtoEdit = (from row in entities.tax_tbl_IndirectCaseTribunalForum
                                            where row.ICaseID == ICaseID
                                            select row).FirstOrDefault();

                        if (recordtoEdit != null)
                        {
                            recordtoEdit.OIADINNo = OIADINNo;
                            if (!string.IsNullOrEmpty(OIADINDate) && OIADINDate != "day-month-year")
                            {
                                recordtoEdit.OIADINDate = DateTime.ParseExact(OIADINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIADINDate = null;
                            }
                            recordtoEdit.OIAIssuingAuthority = OIAIssuingAuthority;

                            if (!string.IsNullOrEmpty(OIAOrderReceiptDate) && OIAOrderReceiptDate != "day-month-year")
                            {
                                recordtoEdit.OIAOrderReceiptDate = DateTime.ParseExact(OIAOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIAOrderReceiptDate = null;
                            }

                            recordtoEdit.OIAAppealFiledBy = OIAAppealFiledBy;
                            recordtoEdit.OIAAppealNo = OIAAppealNo;

                            if (!string.IsNullOrEmpty(OIAAppealDate) && OIAAppealDate != "day-month-year")
                            {
                                recordtoEdit.OIAAppealDate = DateTime.ParseExact(OIAAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.OIAAppealDate = null;
                            }
                            recordtoEdit.OIAGrounds = OIAGrounds;
                            recordtoEdit.HCDinNoByOIA = HCDinNoByOIA;

                            if (!string.IsNullOrEmpty(HCDinDateByOIA) && HCDinDateByOIA != "day-month-year")
                            {
                                recordtoEdit.HCDinDateByOIA = DateTime.ParseExact(HCDinDateByOIA, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.HCDinDateByOIA = null;
                            }
                            recordtoEdit.HCIssuingAuthByOIA = HCIssuingAuthByOIA;

                            if (!string.IsNullOrEmpty(HCDinReceiptDateByOIA) && HCDinReceiptDateByOIA != "day-month-year")
                            {
                                recordtoEdit.HCDinReceiptDateByOIA = DateTime.ParseExact(HCDinReceiptDateByOIA, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.HCDinReceiptDateByOIA = null;
                            }
                            recordtoEdit.drdAppealFiledYNByOIAYN = drdAppealFiledYNByOIAYN;
                            if (recordtoEdit.drdAppealFiledYNByOIAYN == "Y")
                            {
                                //  recordtoEdit.OIAappealfiledwith = OIAappealfiledwith;
                                recordtoEdit.OIAAppealfiledbydate = DateTime.ParseExact(OIAAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            recordtoEdit.TribunalTaxAmount = TribunalTaxAmount;
                            recordtoEdit.TribunalPenalty = TribunalPenalty;
                            recordtoEdit.TribunalInterest = TribunalInterest;
                            recordtoEdit.TribunalFine = TribunalFine;
                            recordtoEdit.TribunalTotalAmount = TribunalTotalAmount;

                            recordtoEdit.TribunalAppealTaxAmount = TribunalAppealTaxAmount;
                            recordtoEdit.TribunalAppealInterest = TribunalAppealInterest;
                            recordtoEdit.TribunalAppealTaxPenalty = TribunalAppealTaxPenalty;
                            recordtoEdit.TribunalAppealFine = TribunalAppealFine;
                            recordtoEdit.TribunalAppealTotalAmount = TribunalAppealTotalAmount;

                            recordtoEdit.CustomerID = CustomerID;
                            recordtoEdit.IsDeleted = false;
                            recordtoEdit.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                            return "Updated Successfully";
                        }

                        else
                        {
                            return "Details with same name already exists";
                        }
                    }


                    else if (!query.Any())
                    {
                        tax_tbl_IndirectCaseTribunalForum objcategory = new tax_tbl_IndirectCaseTribunalForum()
                        {
                            ICaseID = ICaseID,
                            OIADINNo = OIADINNo,
                            OIAIssuingAuthority = OIAIssuingAuthority,
                            OIAAppealFiledBy = OIAAppealFiledBy,
                            OIAAppealNo = OIAAppealNo,
                            OIAGrounds = OIAGrounds,
                            HCDinNoByOIA = HCDinNoByOIA,
                            HCIssuingAuthByOIA = HCIssuingAuthByOIA,
                            drdAppealFiledYNByOIAYN = drdAppealFiledYNByOIAYN,
                            CustomerID = CustomerID,
                            IsDeleted = false
                        };
                        if (!string.IsNullOrEmpty(OIADINDate) && OIADINDate != "day-month-year")
                        {
                            objcategory.OIADINDate = DateTime.ParseExact(OIADINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIADINDate = null;
                        }
                        if (!string.IsNullOrEmpty(OIAOrderReceiptDate) && OIAOrderReceiptDate != "day-month-year")
                        {
                            objcategory.OIAOrderReceiptDate = DateTime.ParseExact(OIAOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIAOrderReceiptDate = null;
                        }

                        if (!string.IsNullOrEmpty(OIAAppealDate) && OIAAppealDate != "day-month-year")
                        {
                            objcategory.OIAAppealDate = DateTime.ParseExact(OIAAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.OIAAppealDate = null;
                        }
                        if (!string.IsNullOrEmpty(HCDinDateByOIA) && HCDinDateByOIA != "day-month-year")
                        {
                            objcategory.HCDinDateByOIA = DateTime.ParseExact(HCDinDateByOIA, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.HCDinDateByOIA = null;
                        }

                        if (!string.IsNullOrEmpty(HCDinReceiptDateByOIA) && HCDinReceiptDateByOIA != "day-month-year")
                        {
                            objcategory.HCDinReceiptDateByOIA = DateTime.ParseExact(HCDinReceiptDateByOIA, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.HCDinReceiptDateByOIA = null;
                        }

                        if (objcategory.drdAppealFiledYNByOIAYN == "Y")
                        {
                            objcategory.OIAappealfiledwith = OIAappealfiledwith;
                            objcategory.OIAAppealfiledbydate = DateTime.ParseExact(OIAAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        objcategory.TribunalTaxAmount = TribunalTaxAmount;
                        objcategory.TribunalPenalty = TribunalPenalty;
                        objcategory.TribunalInterest = TribunalInterest;
                        objcategory.TribunalFine = TribunalFine;
                        objcategory.TribunalTotalAmount = TribunalTotalAmount;

                        objcategory.TribunalAppealTaxAmount = TribunalAppealTaxAmount;
                        objcategory.TribunalAppealInterest = TribunalAppealInterest;
                        objcategory.TribunalAppealTaxPenalty = TribunalAppealTaxPenalty;
                        objcategory.TribunalAppealFine = TribunalAppealFine;
                        objcategory.TribunalAppealTotalAmount = TribunalAppealTotalAmount;

                        bool saveSuccess = CreateTribunalFormforIC(objcategory);

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                    }
                    else
                    {
                        return "Details already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Tribunal Forum Details";
            }
        }
        public static bool CreateTribunalFormforIC(tax_tbl_IndirectCaseTribunalForum newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_IndirectCaseTribunalForum.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddDetailsInHCForum(string HCDINNo, string HCDINDate, string HCIssuingAuthority, string HCOrderReceiptDate,
           string HCAppealFiledBy, string HCAppealNo, string HCAppealDate, string HCGrounds, string SCDinNoByHC, string SCDinDateByHC, string SCIssuingAuthByHC, string SCDinReceiptDateByHC,
          string drdAppealFiledByHCYN, string HCappealfiledwith, string HCAppealfiledbydate, string HCTaxAmount, string HCPenalty, string HCInterest, string HCFine, string HCTotalAmount,
           string HCAppealTaxAmount, string HCAppealInterest, string HCAppealTaxPenalty, string HCAppealFine, string HCAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_IndirectCaseHCForum
                                 where row.ICaseID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (IViewState == 1 && ICaseID != 0)
                    {
                        var recordtoEdit = (from row in entities.tax_tbl_IndirectCaseHCForum
                                            where row.ICaseID == ICaseID
                                            select row).FirstOrDefault();

                        if (recordtoEdit != null)
                        {
                            recordtoEdit.HCDINNo = HCDINNo;
                            recordtoEdit.HCIssuingAuthority = HCIssuingAuthority;
                            recordtoEdit.HCAppealFiledBy = HCAppealFiledBy;
                            recordtoEdit.HCAppealNo = HCAppealNo;
                            if (!string.IsNullOrEmpty(HCAppealDate) && HCAppealDate != "day-month-year")
                            {
                                recordtoEdit.HCAppealDate = DateTime.ParseExact(HCAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.HCAppealDate = null;
                            }
                            recordtoEdit.HCGrounds = HCGrounds;
                            recordtoEdit.SCDinNoByHC = SCDinNoByHC;
                            if (!string.IsNullOrEmpty(SCDinDateByHC) && SCDinDateByHC != "day-month-year")
                            {
                                recordtoEdit.SCDinDateByHC = DateTime.ParseExact(SCDinDateByHC, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.SCDinDateByHC = null;
                            }
                            recordtoEdit.SCIssuingAuthByHC = SCIssuingAuthByHC;
                            if (!string.IsNullOrEmpty(SCDinReceiptDateByHC) && SCDinReceiptDateByHC != "day-month-year")
                            {
                                recordtoEdit.SCDinReceiptDateByHC = DateTime.ParseExact(SCDinReceiptDateByHC, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            else
                            {
                                recordtoEdit.SCDinReceiptDateByHC = null;
                            }

                            recordtoEdit.drdAppealFiledByHCYN = drdAppealFiledByHCYN;
                            if (recordtoEdit.drdAppealFiledByHCYN == "Y")
                            {
                                // recordtoEdit.HCappealfiledwith = HCappealfiledwith;
                                recordtoEdit.HCAppealfiledbydate = DateTime.ParseExact(HCAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            if (!string.IsNullOrEmpty(HCDINDate) && HCDINDate != "day-month-year")
                            {
                                recordtoEdit.HCDINDate = DateTime.ParseExact(HCDINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            if (!string.IsNullOrEmpty(HCOrderReceiptDate) && HCOrderReceiptDate != "day-month-year")
                            {
                                recordtoEdit.HCOrderReceiptDate = DateTime.ParseExact(HCOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            recordtoEdit.HCTaxAmount = HCTaxAmount;
                            recordtoEdit.HCPenalty = HCPenalty;
                            recordtoEdit.HCInterest = HCInterest;
                            recordtoEdit.HCFine = HCFine;
                            recordtoEdit.HCTotalAmount = HCTotalAmount;

                            recordtoEdit.HCAppealTaxAmount = HCAppealTaxAmount;
                            recordtoEdit.HCAppealInterest = HCAppealInterest;
                            recordtoEdit.HCAppealTaxPenalty = HCAppealTaxPenalty;
                            recordtoEdit.HCAppealFine = HCAppealFine;
                            recordtoEdit.HCAppealTotalAmount = HCAppealTotalAmount;

                            recordtoEdit.CustomerID = CustomerID;
                            recordtoEdit.IsDeleted = false;
                            recordtoEdit.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                            return "Updated Successfully";
                        }

                        else
                        {
                            return "Details with same name already exists";
                        }
                    }


                    else if (!query.Any())
                    {
                        tax_tbl_IndirectCaseHCForum objcategory = new tax_tbl_IndirectCaseHCForum()
                        {
                            ICaseID = ICaseID,
                            HCDINNo = HCDINNo,
                            HCIssuingAuthority = HCIssuingAuthority,
                            HCAppealFiledBy = HCAppealFiledBy,
                            HCAppealNo = HCAppealNo,
                            HCGrounds = HCGrounds,
                            SCDinNoByHC = SCDinNoByHC,
                            SCIssuingAuthByHC = SCIssuingAuthByHC,
                            drdAppealFiledByHCYN = drdAppealFiledByHCYN,
                            CustomerID = CustomerID,
                            IsDeleted = false
                        };
                        if (!string.IsNullOrEmpty(HCAppealDate) && HCAppealDate != "day-month-year")
                        {
                            objcategory.HCAppealDate = DateTime.ParseExact(HCAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.HCAppealDate = null;
                        }
                        if (!string.IsNullOrEmpty(SCDinDateByHC) && SCDinDateByHC != "day-month-year")
                        {
                            objcategory.SCDinDateByHC = DateTime.ParseExact(SCDinDateByHC, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.SCDinDateByHC = null;
                        }

                        if (!string.IsNullOrEmpty(SCDinReceiptDateByHC) && SCDinReceiptDateByHC != "day-month-year")
                        {
                            objcategory.SCDinReceiptDateByHC = DateTime.ParseExact(SCDinReceiptDateByHC, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            objcategory.SCDinReceiptDateByHC = null;
                        }
                        if (objcategory.drdAppealFiledByHCYN == "Y")
                        {
                            objcategory.HCappealfiledwith = HCappealfiledwith;
                            objcategory.HCAppealfiledbydate = DateTime.ParseExact(HCAppealfiledbydate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(HCDINDate) && HCDINDate != "day-month-year")
                        {
                            objcategory.HCDINDate = DateTime.ParseExact(HCDINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(HCOrderReceiptDate) && HCOrderReceiptDate != "day-month-year")
                        {
                            objcategory.HCOrderReceiptDate = DateTime.ParseExact(HCOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        objcategory.HCTaxAmount = HCTaxAmount;
                        objcategory.HCPenalty = HCPenalty;
                        objcategory.HCInterest = HCInterest;
                        objcategory.HCFine = HCFine;
                        objcategory.HCTotalAmount = HCTotalAmount;

                        objcategory.HCAppealTaxAmount = HCAppealTaxAmount;
                        objcategory.HCAppealInterest = HCAppealInterest;
                        objcategory.HCAppealTaxPenalty = HCAppealTaxPenalty;
                        objcategory.HCAppealFine = HCAppealFine;
                        objcategory.HCAppealTotalAmount = HCAppealTotalAmount;

                        bool saveSuccess = CreateHCFormforIC(objcategory);

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                    }
                    else
                    {
                        return "Details already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding High Court Forum Details";
            }
        }

        public static bool CreateHCFormforIC(tax_tbl_IndirectCaseHCForum newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_IndirectCaseHCForum.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string AddDetailsInSCForum(string SCDINNo, string SCDINDate, string SCIssuingAuthority, string SCOrderReceiptDate,
            string SCAppealFiledBy, string SCAppealNo, string SCAppealDate, string SCGrounds, string SCOrderNo, string dateSCOrder,
            string SCTaxAmount, string SCPenalty, string SCInterest, string SCFine, string SCTotalAmount,
            string SCAppealTaxAmount, string SCAppealInterest, string SCAppealTaxPenalty, string SCAppealFine, string SCAppealTotalAmount, int CustomerID, long ICaseID, int IViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_IndirectCaseSCForum
                                 where row.ICaseID == ICaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (IViewState == 1 && ICaseID != 0)
                    {
                        var recordtoEdit = (from row in entities.tax_tbl_IndirectCaseSCForum
                                            where row.ICaseID == ICaseID
                                            select row).FirstOrDefault();

                        if (recordtoEdit != null)
                        {
                            recordtoEdit.SCDINNo = SCDINNo;
                            recordtoEdit.SCIssuingAuthority = SCIssuingAuthority;
                            recordtoEdit.SCAppealFiledBy = SCAppealFiledBy;
                            recordtoEdit.SCAppealNo = SCAppealNo;
                            if (!string.IsNullOrEmpty(SCAppealDate) && SCAppealDate != "day-month-year")
                            {
                                recordtoEdit.SCAppealDate = DateTime.ParseExact(SCAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            recordtoEdit.SCGrounds = SCGrounds;
                            recordtoEdit.SCTaxAmount = SCTaxAmount;
                            recordtoEdit.SCPenalty = SCPenalty;
                            recordtoEdit.SCInterest = SCInterest;
                            recordtoEdit.SCFine = SCFine;
                            recordtoEdit.SCTotalAmount = SCTotalAmount;

                            recordtoEdit.SCAppealTaxAmount = SCAppealTaxAmount;
                            recordtoEdit.SCAppealInterest = SCAppealInterest;
                            recordtoEdit.SCAppealTaxPenalty = SCAppealTaxPenalty;
                            recordtoEdit.SCAppealFine = SCAppealFine;
                            recordtoEdit.SCAppealTotalAmount = SCAppealTotalAmount;

                            recordtoEdit.CustomerID = CustomerID;
                            recordtoEdit.IsDeleted = false;
                            recordtoEdit.UpdatedOn = DateTime.Now;
                            recordtoEdit.SCOrderNo = SCOrderNo;

                            if (!string.IsNullOrEmpty(dateSCOrder) && dateSCOrder != "day-month-year")
                            {
                                recordtoEdit.dateSCOrder = DateTime.ParseExact(dateSCOrder, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            if (!string.IsNullOrEmpty(SCDINDate) && SCDINDate != "day-month-year")
                            {
                                recordtoEdit.SCDINDate = DateTime.ParseExact(SCDINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            if (!string.IsNullOrEmpty(SCOrderReceiptDate) && SCOrderReceiptDate != "day-month-year")
                            {
                                recordtoEdit.SCOrderReceiptDate = DateTime.ParseExact(SCOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            entities.SaveChanges();
                            return "Updated Successfully";
                        }

                        else
                        {
                            return "Details with same name already exists";
                        }
                    }


                    else if (!query.Any())
                    {
                        tax_tbl_IndirectCaseSCForum objcategory = new tax_tbl_IndirectCaseSCForum()
                        {
                            ICaseID = ICaseID,
                            SCDINNo = SCDINNo,
                            SCIssuingAuthority = SCIssuingAuthority,
                            SCAppealFiledBy = SCAppealFiledBy,
                            SCAppealNo = SCAppealNo,
                            SCGrounds = SCGrounds,
                            SCTaxAmount = SCTaxAmount,
                            SCPenalty = SCPenalty,
                            SCInterest = SCInterest,
                            SCFine = SCFine,
                            SCTotalAmount = SCTotalAmount,
                            CustomerID = CustomerID,
                            IsDeleted = false,
                            SCOrderNo = SCOrderNo
                        };

                        if (!string.IsNullOrEmpty(SCAppealDate) && SCAppealDate != "day-month-year")
                        {
                            objcategory.SCAppealDate = DateTime.ParseExact(SCAppealDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(dateSCOrder) && dateSCOrder != "day-month-year")
                        {
                            objcategory.dateSCOrder = DateTime.ParseExact(dateSCOrder, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        if (!string.IsNullOrEmpty(SCDINDate) && SCDINDate != "day-month-year")
                        {
                            objcategory.SCDINDate = DateTime.ParseExact(SCDINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        if (!string.IsNullOrEmpty(SCOrderReceiptDate) && SCOrderReceiptDate != "day-month-year")
                        {
                            objcategory.SCOrderReceiptDate = DateTime.ParseExact(SCOrderReceiptDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }


                        objcategory.SCAppealTaxAmount = SCAppealTaxAmount;
                        objcategory.SCAppealInterest = SCAppealInterest;
                        objcategory.SCAppealTaxPenalty = SCAppealTaxPenalty;
                        objcategory.SCAppealFine = SCAppealFine;
                        objcategory.SCAppealTotalAmount = SCAppealTotalAmount;

                        bool saveSuccess = CreateSCFormforIC(objcategory);

                        if (saveSuccess)
                        {
                            return "Sucessfully Added";
                        }
                    }
                    else
                    {
                        return "Details already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Supreme Court Forum Details";
            }
        }

        public static bool CreateSCFormforIC(tax_tbl_IndirectCaseSCForum newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_IndirectCaseSCForum.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static int ExistsICaseDocumentReturnVersion(tax_tbl_TaxFileData newDocumentRecord)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var query = (from row in entities.tax_tbl_TaxFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.IndirectCaseInstanceID == newDocumentRecord.IndirectCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        public static int CreateICaseDocumentMappingGetID(tax_tbl_TaxFileData objNoticeCaseDoc)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    objNoticeCaseDoc.EnType = "A";
                    entities.tax_tbl_TaxFileData.Add(objNoticeCaseDoc);
                    entities.SaveChanges();
                    return objNoticeCaseDoc.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static string AddPHDetails(string ICaseID, string HearingForum, string hearingDate, string HearingDescr, string NextHearingDate, string ReminderDate, int CustomerID, int UserID, int PHID, string ViewState)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    DateTime Hd = DateTime.ParseExact(hearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                    DateTime RHd = DateTime.ParseExact(ReminderDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                    var query = (from row in entities.tax_tbl_ICHearingDetails
                                 where row.hearingDate == Hd
                                 && row.CustomerID == CustomerID
                                 && row.IsDeleted == false
                                 && row.HearingForum == "-1"
                                 && row.IndirectCaseInstanceID == ICaseID
                                 select row);
                    if (ViewState == "1" && PHID != 0)
                    {
                        if (PHID > 0)
                        {
                            query = query.Where(entry => entry.ID != PHID);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_ICHearingDetails
                                                where row.ID == PHID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.HearingForum = HearingForum;
                                recordtoEdit.hearingDate = Hd;
                                recordtoEdit.HearingDescr = HearingDescr;
                                if (!string.IsNullOrEmpty(NextHearingDate) && NextHearingDate != "day-month-year")
                                {
                                    DateTime NextHd = DateTime.ParseExact(NextHearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    recordtoEdit.NextHearingDate = NextHd;
                                }

                                recordtoEdit.ReminderDate = RHd;
                                recordtoEdit.CustomerID = CustomerID;
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;
                                recordtoEdit.UpdatedBy = UserID;

                                entities.SaveChanges();
                                return "1";
                            }
                            else
                            {
                                return "0";
                            }

                        }
                        else
                        {
                            return "0";
                        }

                    }
                    else if (!query.Any() && PHID == 0)
                    {
                        tax_tbl_ICHearingDetails objcategory = new tax_tbl_ICHearingDetails()
                        {
                            IndirectCaseInstanceID = ICaseID,
                            HearingForum = HearingForum,
                            hearingDate = Hd,
                            HearingDescr = HearingDescr,
                            ReminderDate = RHd,
                            CustomerID = CustomerID,
                            UserID = UserID,
                            IsDeleted = false
                        };
                        if (!string.IsNullOrEmpty(NextHearingDate) && NextHearingDate != "day-month-year")
                        {
                            DateTime NextHd = DateTime.ParseExact(NextHearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            objcategory.NextHearingDate = NextHd;
                        }

                        long saveSuccess = CreatePHDetails(objcategory);

                        if (saveSuccess > 0)
                        {
                            var recordtoEdit = (from row in entities.SP_ICHearingDetails(CustomerID)
                                                where row.ID == saveSuccess
                                                select row).FirstOrDefault();

                            //List<string> CaseOwnerandIUser = CaseManagement.getCaseOwnerAndInternalUser(Convert.ToInt32(caseInstanceID), AuthenticationHelper.CustomerID);

                            //List<string> UniqueMail = new List<string>();
                            //if (CaseOwnerandIUser.Count > 0)
                            //{
                            //    foreach (var item in CaseOwnerandIUser)
                            //    {
                            //        if (!UniqueMail.Contains(item))
                            //        {
                            //            UniqueMail.Add(item);
                            //        }
                            //    }
                            //}

                            //string message = AppWebApplication.Properties.Settings.Default.EmailTemplate_IndirectTaxCase_HearingCreation
                            //                             .Replace("@ICaseNo", recordtoEdit.ICaseNo)
                            //                             .Replace("@Forum", recordtoEdit.HearingForum)
                            //                             .Replace("@Location", recordtoEdit.Location)
                            //                             .Replace("@HearingDate",Convert.ToString(recordtoEdit.hearingDate))
                            //                             .Replace("@NextHearingDate", Convert.ToString(recordtoEdit.NextHearingDate))
                            //                             .Replace("@HearingDescription", recordtoEdit.HearingDescr)
                            //                             .Replace("@AccessURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                             .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["IndirectTaxTeam"]))
                            //                             .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                            //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), null, null, "Indirect Tax Case Hearing Created", message);

                            return Convert.ToString(saveSuccess);
                        }
                    }
                    else
                    {
                        return "0";
                    }
                    return "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "-1";
                //return "Server Error Occured at adding Hearing.";
            }
        }

        public static long CreatePHDetails(tax_tbl_ICHearingDetails newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_ICHearingDetails.Add(newRecord);
                    entities.SaveChanges();

                    return newRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static int ExistsCCaseDocumentReturnVersion(tax_tbl_CommercialCaseFileData newDocumentRecord)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                var query = (from row in entities.tax_tbl_CommercialCaseFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.CommercialCaseInstanceID == newDocumentRecord.CommercialCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        public static int CreateCCaseDocumentMappingGetID(tax_tbl_CommercialCaseFileData objDCDoc)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    entities.tax_tbl_CommercialCaseFileData.Add(objDCDoc);
                    entities.SaveChanges();
                    return objDCDoc.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static void CreateAuditLog(string CaseType, string CaseInstanceID, int Createdby, string Remark, string CreatedOn)
        {
            using (LitigationDataContainer entities = new LitigationDataContainer())
            {
                string UserName = UserManagement.GetUserNameByID(Convert.ToInt32(Createdby));
                try
                {
                    tax_tbl_AuditDetails TBL_LAL = new tax_tbl_AuditDetails()
                    {
                        CaseType = CaseType,
                        CaseInstanceID = CaseInstanceID,
                        CreatedBy = UserName,
                        CreatedOn = DateTime.Now,
                        Remarks = Remark,
                    };

                    entities.tax_tbl_AuditDetails.Add(TBL_LAL);
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public static string AddActionTakenDetails(string dateCaseUpdated, string Discussion, string Suggession, string CCaseID, int CustomerID, string ViewState, int ID, string CCaseNo, int CreatedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {

                    if (ViewState == "1" && ID != 0)
                    {
                        if (ID > 0)
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_ActionTakenDetails
                                                where row.ID == ID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.Discussion = Discussion;
                                recordtoEdit.Suggession = Suggession;

                                if (dateCaseUpdated != null && dateCaseUpdated != "day-month-year")
                                {
                                    recordtoEdit.dateCaseUpdated = DateTime.ParseExact(dateCaseUpdated, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }

                                recordtoEdit.IsDeleted = false;
                                TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Entry Updated in Action Taken Details", DateTime.Now.ToString("dd-MM-yyyy"));
                                entities.SaveChanges();
                                return Convert.ToString(ID);
                            }
                            else
                            {
                                return "0";
                            }
                        }
                        else
                        {
                            return "0";
                        }

                    }
                    else if (ViewState == "0")
                    {
                        tax_tbl_ActionTakenDetails objcategory = new tax_tbl_ActionTakenDetails()
                        {
                            CCaseID = CCaseID,
                            CCaseNo = CCaseNo,
                            Suggession = Suggession,
                            Discussion = Discussion,
                            IsDeleted = false,
                            CustomerID = CustomerID,
                            CreatedBy = CreatedBy
                        };
                        if (dateCaseUpdated != null && dateCaseUpdated != "day-month-year")
                        {
                            objcategory.dateCaseUpdated = DateTime.ParseExact(dateCaseUpdated, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        long saveSuccess = CreateActionTakenDetails(objcategory);
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Entry is created in Action Taken Details", DateTime.Now.ToString("dd-MM-yyyy"));
                        if (saveSuccess > 0)
                        {
                            return Convert.ToString(saveSuccess);
                        }
                        else
                        {
                            return "0";
                        }

                    }
                    else
                    {
                        return "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "0";
            }
        }

        public static long CreateActionTakenDetails(tax_tbl_ActionTakenDetails newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    entities.tax_tbl_ActionTakenDetails.Add(newRecord);
                    entities.SaveChanges();

                    return newRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }



        public static string AddAuditObservationDetails(string AuditObserveYN, string ObservationRecordDate, string AuditType, string AuditTypeOthers, string StatusAuditObservartion, string CCaseID, int CustomerID, string ViewState, int ID, string CCaseNo, int CreatedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {

                    if (ViewState == "1" && ID != 0)
                    {
                        if (ID > 0)
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_AuditObservationDetails
                                                where row.ID == ID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.AuditObserveYN = AuditObserveYN;
                                recordtoEdit.AuditType = AuditType;
                                recordtoEdit.AuditTypeOthers = AuditTypeOthers;
                                recordtoEdit.StatusAuditObservartion = StatusAuditObservartion;

                                if (ObservationRecordDate != null && ObservationRecordDate != "day-month-year")
                                {
                                    recordtoEdit.ObservationRecordDate = DateTime.ParseExact(ObservationRecordDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }

                                recordtoEdit.IsDeleted = false;
                                TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Entry is updated in Audit Observation Details", DateTime.Now.ToString("dd-MM-yyyy"));
                                entities.SaveChanges();
                                return Convert.ToString(ID);
                            }
                            else
                            {
                                return "0";
                            }
                        }
                        else
                        {
                            return "0";
                        }

                    }
                    else if (ViewState == "0")
                    {
                        tax_tbl_AuditObservationDetails objcategory = new tax_tbl_AuditObservationDetails()
                        {
                            CCaseID = CCaseID,
                            CCaseNo = CCaseNo,
                            AuditObserveYN = AuditObserveYN,
                            AuditType = AuditType,
                            AuditTypeOthers = AuditTypeOthers,
                            StatusAuditObservartion = StatusAuditObservartion,
                            IsDeleted = false,
                            CustomerID = CustomerID,
                            CreatedBy = CreatedBy
                        };

                        if (ObservationRecordDate != null && ObservationRecordDate != "day-month-year")
                        {
                            objcategory.ObservationRecordDate = DateTime.ParseExact(ObservationRecordDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        long saveSuccess = CreateAuditObserDetails(objcategory);
                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), CreatedBy, "Entry is created in Audit Observation Details", DateTime.Now.ToString("dd-MM-yyyy"));

                        if (saveSuccess > 0)
                        {
                            return Convert.ToString(saveSuccess);
                        }
                        else
                        {
                            return "0";
                        }

                    }
                    else
                    {
                        return "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "0";
            }
        }
        public static long CreateAuditObserDetails(tax_tbl_AuditObservationDetails newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    entities.tax_tbl_AuditObservationDetails.Add(newRecord);
                    entities.SaveChanges();

                    return newRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static string AddCCaseDetails(string ForumID, string Forum, string ForumOthers, string Location, string FPR, string FPRFollow, string BusinessPartner, string CustomerCode, string BPCategory, string Product, string CaseYear,
      string PeriodFrom, string PeriodTo, string CaseBrief, string PersonIncharge, string DealingOfficer, string ConcernedDirectorDD, string DisputeDetails, string PresentStatus, string Refrences,
      string AccountedForCurrent, string NotAccountedForCurrent, string TotalCurrent, string InterestNature, string MGONature, string Othersnature,
      string TotalNature, string ProvisionCreated, string ProvisionAmount, string SAPDocNos, string AmountRecovered, string InitialDisputedAmt,
      string DisputePendingAgainstForum, string CaseLasthearingDate, string CaseFilingDate, string CaseNexthearingDate, string PresentStatusCase,
      int CustomerID, int CCaseID, string IViewState, int UserID, string UpdatedBy)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    var query = (from row in entities.tax_tbl_CLegalCaseInstance
                                 where row.ID == CCaseID
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);

                    if (IViewState == "1")
                    {
                        if (CCaseID != 0)
                        {
                            query = query.Where(entry => entry.ID != CCaseID);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tax_tbl_CLegalCaseInstance
                                                where row.ID == CCaseID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.ForumID = ForumID;
                                recordtoEdit.Forum = Forum;
                                recordtoEdit.ForumOthers = ForumOthers;
                                recordtoEdit.CustomerBranchID = Location;
                                recordtoEdit.FPR = FPR;
                                recordtoEdit.FPRFollow = FPRFollow;
                                recordtoEdit.BusinessPartner = BusinessPartner;
                                recordtoEdit.CustomerCode = CustomerCode;
                                recordtoEdit.BPCategory = BPCategory;
                                recordtoEdit.Product = Product;
                                recordtoEdit.CaseYear = CaseYear;
                                recordtoEdit.PeriodFrom = DateTime.ParseExact(PeriodFrom, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                recordtoEdit.CaseBrief = CaseBrief;
                                recordtoEdit.PersonIncharge = PersonIncharge;
                                recordtoEdit.DealingOfficer = DealingOfficer;
                                recordtoEdit.ConcernedDirectorDD = ConcernedDirectorDD;
                                recordtoEdit.DisputeDetails = DisputeDetails;
                                recordtoEdit.PresentStatus = PresentStatus;
                                recordtoEdit.Refrences = Refrences;

                                recordtoEdit.AccountedForCurrent = AccountedForCurrent;
                                recordtoEdit.NotAccountedForCurrent = NotAccountedForCurrent;
                                recordtoEdit.TotalCurrent = TotalCurrent;
                                recordtoEdit.InterestNature = InterestNature;
                                recordtoEdit.MGONature = MGONature;
                                recordtoEdit.Othersnature = Othersnature;

                                recordtoEdit.TotalNature = TotalNature;
                                recordtoEdit.ProvisionCreated = ProvisionCreated;
                                recordtoEdit.ProvisionAmount = ProvisionAmount;
                                recordtoEdit.SAPDocNos = SAPDocNos;
                                recordtoEdit.AmountRecovered = AmountRecovered;
                                recordtoEdit.InitialDisputedAmt = InitialDisputedAmt;

                                if (ForumID == "12" || Forum == "Others")
                                {
                                    recordtoEdit.PresentStatusCase = PresentStatusCase;
                                    if (!string.IsNullOrEmpty(CaseFilingDate) && CaseFilingDate != "day-month-year")
                                    {
                                        recordtoEdit.CaseFilingDate = DateTime.ParseExact(CaseFilingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    }

                                    if (!string.IsNullOrEmpty(CaseLasthearingDate) && CaseLasthearingDate != "day-month-year")
                                    {
                                        recordtoEdit.CaseLasthearingDate = DateTime.ParseExact(CaseLasthearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    }

                                    if (!string.IsNullOrEmpty(CaseNexthearingDate) && CaseNexthearingDate != "day-month-year")
                                    {
                                        recordtoEdit.CaseNexthearingDate = DateTime.ParseExact(CaseNexthearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    }
                                }
                                else
                                {
                                    recordtoEdit.PresentStatusCase = null;
                                    recordtoEdit.CaseFilingDate = null;
                                    recordtoEdit.CaseLasthearingDate = null;
                                    recordtoEdit.CaseNexthearingDate = null;
                                }

                                if (!string.IsNullOrEmpty(PeriodTo) && PeriodTo != "day-month-year")
                                {
                                    recordtoEdit.PeriodTo = DateTime.ParseExact(PeriodTo, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                else
                                {
                                    recordtoEdit.PeriodTo = null;
                                }
                                if (string.IsNullOrEmpty(DisputePendingAgainstForum))
                                {
                                    recordtoEdit.DisputePendingAgainstForum = "2";
                                }
                                else
                                {
                                    recordtoEdit.DisputePendingAgainstForum = DisputePendingAgainstForum;
                                }
                                if (DisputePendingAgainstForum == "2" && DisputePendingAgainstForum != null)
                                {
                                    recordtoEdit.LitiCaseNo = null;
                                }
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;
                                recordtoEdit.UpdatedBy = UpdatedBy;

                                entities.SaveChanges();
                                TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(CCaseID), UserID, "Case Updated", DateTime.Now.ToString("dd-MM-yyyy"));
                                return "Commercial Case Details Updated Successfully";
                            }

                        }
                        else
                        {
                            return "Commercial Case with same details already exists";
                        }

                    }
                    else if (!query.Any() && IViewState == "0")
                    {
                        tax_tbl_CLegalCaseInstance objCase = new tax_tbl_CLegalCaseInstance()
                        {
                            ForumID = ForumID,
                            Forum = Forum,
                            ForumOthers = ForumOthers,
                            CustomerBranchID = Location,
                            FPR = FPR,
                            FPRFollow = FPRFollow,
                            BusinessPartner = BusinessPartner,
                            CustomerCode = CustomerCode,
                            BPCategory = BPCategory,
                            Product = Product,
                            CaseYear = CaseYear,
                            PeriodFrom = DateTime.ParseExact(PeriodFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            CaseBrief = CaseBrief,
                            DisputeDetails = DisputeDetails,
                            PresentStatus = PresentStatus,
                            Refrences = Refrences,

                            PersonIncharge = PersonIncharge,
                            DealingOfficer = DealingOfficer,
                            ConcernedDirectorDD = ConcernedDirectorDD,

                            AccountedForCurrent = AccountedForCurrent,
                            NotAccountedForCurrent = NotAccountedForCurrent,
                            TotalCurrent = TotalCurrent,
                            InterestNature = InterestNature,
                            MGONature = MGONature,
                            Othersnature = Othersnature,

                            TotalNature = TotalNature,
                            ProvisionCreated = ProvisionCreated,
                            ProvisionAmount = ProvisionAmount,
                            SAPDocNos = SAPDocNos,
                            AmountRecovered = AmountRecovered,
                            InitialDisputedAmt = InitialDisputedAmt,
                            IsDeleted = false,
                            CustomerID = CustomerID,
                            UserID = UserID,
                            UpdatedBy = UpdatedBy
                        };
                        if (ForumID == "8" || Forum == "Arbitration")
                        {
                            objCase.PresentStatusCase = PresentStatusCase;

                            if (!string.IsNullOrEmpty(CaseFilingDate) && CaseFilingDate != "day-month-year")
                            {
                                objCase.CaseFilingDate = DateTime.ParseExact(CaseFilingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            if (!string.IsNullOrEmpty(CaseLasthearingDate) && CaseLasthearingDate != "day-month-year")
                            {
                                objCase.CaseLasthearingDate = DateTime.ParseExact(CaseLasthearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            if (!string.IsNullOrEmpty(CaseNexthearingDate) && CaseNexthearingDate != "day-month-year")
                            {
                                objCase.CaseNexthearingDate = DateTime.ParseExact(CaseNexthearingDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                        }
                        else
                        {
                            objCase.PresentStatusCase = null;
                            objCase.CaseFilingDate = null;
                            objCase.CaseLasthearingDate = null;
                            objCase.CaseNexthearingDate = null;
                        }

                        if (!string.IsNullOrEmpty(PeriodTo) && PeriodTo != "day-month-year")
                        {
                            objCase.PeriodTo = DateTime.ParseExact(PeriodTo, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }

                        if (DisputePendingAgainstForum == null)
                        {
                            objCase.DisputePendingAgainstForum = "2";
                        }
                        else
                        {
                            objCase.DisputePendingAgainstForum = DisputePendingAgainstForum;
                        }

                        long saveSuccess = CreateCCaseDetails(objCase);

                        TaxationMethods.CreateAuditLog("Commercial", Convert.ToString(saveSuccess), UserID, "Case Created", DateTime.Now.ToString("dd-MM-yyyy"));

                        if (saveSuccess > 0)
                        {
                            return Convert.ToString(saveSuccess);
                        }
                        else
                        {
                            return "0";
                        }
                    }
                    else
                    {
                        return "Commercial Case with same Case No already exists";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Server Error Occured at adding Commercial Case Details";
            }
        }

        public static long CreateCCaseDetails(tax_tbl_CLegalCaseInstance newRecord)
        {
            try
            {
                using (LitigationDataContainer entities = new LitigationDataContainer())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tax_tbl_CLegalCaseInstance.Add(newRecord);
                    entities.SaveChanges();

                    return newRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


    }
}