﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class CompDeptManagement
    {
        public static List<Department> GetAllDepartmentMasterList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentMasterList = (from row in entities.Departments
                                            where row.IsDeleted == false
                                            //&& row.CustomerID == customerID
                                            select row);

                return DepartmentMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}