﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class LitigationPaymentType
    {

        public static bool DeletePaymentMaster(int paymentID)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_PaymentType ObjPay = (from row in entities.tbl_PaymentType
                                              where row.ID == paymentID
                                              && row.IsDeleted == false
                                              select row).FirstOrDefault();

                    ObjPay.IsDeleted = true;
                    entities.SaveChanges();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
    }
}