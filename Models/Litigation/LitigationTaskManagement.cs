﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class LitigationTaskManagement
    {
        public static List<View_NoticeCaseTaskDetail> GetTaskDetails(int noticeCaseInstanceID, string tasktype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_NoticeCaseTaskDetail
                                   where row.NoticeCaseInstanceID == noticeCaseInstanceID
                                   && row.TaskType == tasktype
                                   && row.IsActive == true
                                   select row).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderByDescending(entry => entry.Status)
                        .OrderBy(entry => entry.RefID)
                        .ThenByDescending(entry => entry.TaskUpdatedOn)
                        .ThenByDescending(entry => entry.ScheduleOnDate).ToList();


                return queryResult;
            }
        }
        public static string GetTaskTypebyID(int TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in entities.tbl_TaskScheduleOn
                                   where row.ID == TaskID
                                   select row.TaskType).FirstOrDefault();
                return QueryResult;
            }
        }
        public static List<View_NoticeCaseTaskDetail> GetAssignedTaskList(int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, int partyID, int deptID, int taskStatus, string taskType, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();

                if (taskStatus != -1)
                {
                    query = query.Where(entry => entry.StatusID == taskStatus).ToList();

                    //if (taskStatus != 3)//3--Closed otherwise Open
                    //    query = query.Where(entry => entry.StatusID < 3).ToList();
                    //else
                    //    query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.StatusID < 3).ToList();
                }
                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (priorityID != -1)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID == partyID).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }
                //if (lstSelectedFileTags.Count > 0)
                //    //query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();


                return query.ToList();
            }
        }
        public static bool UpdateTaskAccessURL(long taskID, tbl_TaskScheduleOn taskRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.AccessURL = taskRecord.AccessURL;
                        taskToUpdate.UpdatedBy = taskRecord.UpdatedBy;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateTask(tbl_TaskScheduleOn objTaskRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objTaskRecord.CreatedOn = DateTime.Now;

                    entities.tbl_TaskScheduleOn.Add(objTaskRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistNoticeCaseTaskTitle(string taskTitle, string tasktType, long noticeCaseInstanceID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskDetail = entities.tbl_TaskScheduleOn
                        .Where(x => x.TaskType == tasktType
                        && x.NoticeCaseInstanceID == noticeCaseInstanceID
                        && x.IsActive == true && x.CustomerID == CustomerID
                        && x.TaskTitle.Trim().ToUpper() == taskTitle.Trim().ToUpper()).FirstOrDefault();

                    if (taskDetail != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LitigationFileData> GetTaskDocuments(long taskID, long instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == instanceID
                                   && row.DocTypeInstanceID == taskID
                                   && (row.DocType.Trim() == "CT" || row.DocType.Trim() == "NT" || row.DocType.Trim() == "T")
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #region Amol
        public static bool DeleteTask(int taskID, int deletedByUserID, long CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn ActionLogToDelete = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID && x.CustomerID == CustomerID).FirstOrDefault();

                    if (ActionLogToDelete != null)
                    {
                        ActionLogToDelete.IsActive = false;
                        ActionLogToDelete.DeletedBy = deletedByUserID;
                        ActionLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static List<View_NoticeCaseTaskDetail> GetAssignedTaskListAmol1(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID,int taskStatus)//, int priorityID, int partyID, int deptID, int taskStatus, string taskType
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();

                if (taskStatus != -1)
                {
                    if (taskStatus == 12)
                    {
                        query = query.Where(entry => entry.StatusID == 1 || entry.StatusID == 2).ToList();
                    }
                    else
                    {
                        query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                    }
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")             
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
             
                //if (priorityID != -1)
                //    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                //    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }

                return query.ToList();
            }
        }

        public static List<View_NoticeCaseTaskDetail> GetAssignedTaskListAmol(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID, int taskStatus)//, int priorityID, int partyID, int deptID, int taskStatus, string taskType
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();

                if (taskStatus != -1)
                {
                    query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }

                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(CustomerID));
                if (isexist != null)
                {
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(CustomerID), loggedInUserID, "", -1);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();

                }
                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();

                //if (priorityID != -1)
                //    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                //    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }

                return query.ToList();
            }
        }
        #endregion

        #region Gaurav

        public static List<View_NoticeCaseTaskDetail> GetDashboardTaskList(List<View_NoticeCaseTaskDetail> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID) /*, int taskStatus*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             where row.IsActive == true
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.StatusID)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).Take(5).ToList();

                return query;
            }
        }

        public static int GetAssignedTaskCount(List<View_NoticeCaseTaskDetail> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int taskStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             where row.IsActive == true
                             select row).ToList();

                if (taskStatus != 0)
                {
                    if (taskStatus != 3)//3--Closed otherwise Open
                        query = query.Where(entry => entry.StatusID < 3).ToList();
                    else
                        query = query.Where(entry => entry.StatusID == taskStatus).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                return query.Count();
            }
        }

        public static List<SP_Litigation_NoticeCaseTaskDetail_Result> GetAssignedTaskListforTag(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, string FileName, int status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_Litigation_NoticeCaseTaskDetail(customerID)
                             select row).ToList();


                if (status != -1)
                {
                    if (status == 1)
                    {
                        query = query.Where(entry => entry.StatusID == 1).ToList();//Opne
                    }
                    else if (status == 2)
                    {
                        query = query.Where(entry => entry.StatusID == 2).ToList();//Submitted
                    }
                    else if (status == 3)
                    {
                        query = query.Where(entry => entry.StatusID == 3).ToList();//Closed
                    }
                    else
                    {
                        query = query.Where(entry => entry.StatusID == status).ToList();
                    }

                }

                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerID, loggedInUserID, "", -1);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                        query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();

                }

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                //    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
              

                if (query != null)
                  
                    if (query.Count > 0)
                    {
                        query = query.OrderBy(entry => entry.TaskType)
                            .ThenBy(entry => entry.ScheduleOnDate)
                            .ThenBy(entry => entry.PriorityID).ToList();
                    }

                if (!string.IsNullOrEmpty(FileName))
                {
                    query = (from row in query where row.DocumentName != null select row).ToList();
                    query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();

                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefID,
                                 g.Priority,
                                 g.StatusID,
                                 g.AssignTo,
                                 //g.DocumentName,
                                 g.TaskType,
                                 g.TaskTitle,
                                 g.TaskDesc,
                                 g.ScheduleOnDate,
                                 g.PriorityID,
                                 g.AssignToName,
                                 g.TaskID,
                                 g.FinancialYear,
                                 g.CustomerBranchID,
                                 g.Status
                                 //g.Filetag
                             } into GCS
                             select new SP_Litigation_NoticeCaseTaskDetail_Result()
                             {
                                 //FileName=GCS.Key.FileName,
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefID = GCS.Key.RefID,
                                 Priority = GCS.Key.Priority,
                                 StatusID = GCS.Key.StatusID,
                                 AssignTo = GCS.Key.AssignTo,
                                 //DocumentName=GCS.Key.DocumentName,
                                 TaskType = GCS.Key.TaskType,
                                 TaskTitle = GCS.Key.TaskTitle,
                                 TaskDesc = GCS.Key.TaskDesc,
                                 ScheduleOnDate = GCS.Key.ScheduleOnDate,
                                 PriorityID = GCS.Key.PriorityID,
                                 AssignToName = GCS.Key.AssignToName,
                                 TaskID = GCS.Key.TaskID,
                                 FinancialYear = GCS.Key.FinancialYear,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 Status = GCS.Key.Status
                                 //Filetag = GCS.Key.Filetag
                             }).ToList();
                }
                ////return query;


                return query;
            }
        }


        //public static List<SP_Litigation_NoticeCaseTaskDetail_Result> GetAssignedTaskListforTag(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, string FileName, int status)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.SP_Litigation_NoticeCaseTaskDetail(customerID)
        //                     select row).ToList();


        //        if (status != -1)
        //        {
        //            if (status == 0)
        //            {
        //                query = query.Where(entry => entry.StatusID == 1).ToList();
        //            }
        //            else if (status == 1)
        //            {
        //                query = query.Where(entry => entry.StatusID == 2).ToList();
        //            }
        //            else
        //            {
        //                query = query.Where(entry => entry.StatusID == status).ToList();
        //            }
        //            //if (taskStatus != 3)//3--Closed otherwise Open
        //            //    query = query.Where(entry => entry.StatusID < 3).ToList();
        //            //else
        //            //    query = query.Where(entry => entry.StatusID == taskStatus).ToList();
        //        }
        //        //if (status != -1)
        //        //{
        //        //    query = query.Where(entry => entry.StatusID == status).ToList();

        //        //if (status == 3)//3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.StatusID < 3).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.StatusID == status).ToList();


        //        //if (status == 3) //3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.StatusID == status).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.StatusID < 3).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
        //            query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

        //        //if (priorityID != -1)
        //        //    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID == partyID).ToList();

        //        if (query != null)
        //            //query = query.Where(entry => entry.DepartmentID == deptID).ToList();
        //         //commented by gaurav   query = query.Where(entry => entry.NoticeCaseInstanceID == 0).ToList();

        //        //if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
        //        //    query = query.Where(entry => entry.TaskType == taskType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TaskType)
        //                .ThenBy(entry => entry.ScheduleOnDate)
        //                .ThenBy(entry => entry.PriorityID).ToList();
        //        }

        //        if (!string.IsNullOrEmpty(FileName))
        //        {
        //            query = (from row in query where row.DocumentName != null select row).ToList();
        //            query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();

        //        }
        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TaskType)
        //                .ThenBy(entry => entry.ScheduleOnDate)
        //                .ThenBy(entry => entry.PriorityID).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.RefID,
        //                         g.Priority,
        //                         g.Status,
        //                         g.AssignTo,
        //                         //g.DocumentName,
        //                         g.TaskType,
        //                         g.TaskTitle,
        //                         g.TaskDesc,
        //                         g.ScheduleOnDate,
        //                         g.PriorityID,
        //                         g.AssignToName,
        //                         g.TaskID
        //                         //g.Filetag
        //                     } into GCS
        //                     select new SP_Litigation_NoticeCaseTaskDetail_Result()
        //                     {
        //                         //FileName=GCS.Key.FileName,
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         RefID = GCS.Key.RefID,
        //                         Priority = GCS.Key.Priority,
        //                         Status = GCS.Key.Status,
        //                         AssignTo = GCS.Key.AssignTo,
        //                         //DocumentName=GCS.Key.DocumentName,
        //                         TaskType = GCS.Key.TaskType,
        //                         TaskTitle = GCS.Key.TaskTitle,
        //                         TaskDesc = GCS.Key.TaskDesc,
        //                         ScheduleOnDate = GCS.Key.ScheduleOnDate,
        //                         PriorityID = GCS.Key.PriorityID,
        //                         AssignToName = GCS.Key.AssignToName,
        //                         TaskID = GCS.Key.TaskID
        //                         //Filetag = GCS.Key.Filetag
        //                     }).ToList();
        //        }
        //        ////return query;


        //        return query;
        //    }
        //}

        public static List<View_NoticeCaseTaskDetail> GetAssignedTaskList(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_NoticeCaseTaskDetail
                             where row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();

                //i have               if (taskStatus != -1)
                //              {
                //                  query = query.Where(entry => entry.StatusID == taskStatus).ToList();

                //               }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //query = query.Where(entry => entry.AssignTo == loggedInUserID || entry.TaskCreatedBy == loggedInUserID).ToList();
                    query = query.Where(entry => (entry.AssignTo == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.TaskCreatedBy == loggedInUserID)).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //i have                if (priorityID != -1)
                //                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID == partyID).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                //i have                if (taskType != "" && taskType != "B") //B--Both --Inward(I) and Outward(O)
                //                    query = query.Where(entry => entry.TaskType == taskType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TaskType)
                        .ThenBy(entry => entry.ScheduleOnDate)
                        .ThenBy(entry => entry.PriorityID).ToList();
                }
                //if (lstSelectedFileTags.Count > 0)
                //    //query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();


                return query.ToList();
            }
        }
        #endregion
    }
}