﻿using System;
using AppWebApplication.Data;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Ionic.Zip;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;

namespace AppWebApplication.Models.Litigation
{
    public class PublicClassZip1
    {
        public static string FetchPathforContract(int ContractID, int FileID, string version, int userID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;

            List<Cont_tbl_FileData> ComplianceFileData = new List<Cont_tbl_FileData>();
            List<Cont_tbl_FileData> ComplianceDocument = new List<Cont_tbl_FileData>();
            if (FileID != -1)
            {
                ComplianceDocument = DocumentManagement.GetContractData(Convert.ToInt32(ContractID), FileID).ToList();
            }
            if (version.Equals("1.0"))
            {
                ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                if (ComplianceFileData.Count <= 0)
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                }
            }
            else
            {
                ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
            }

            ComplianceDocument = DocumentManagement.GetContractData(Convert.ToInt32(ContractID), FileID).ToList();

            using (ZipFile ComplianceZip = new ZipFile())
            {
                if (ComplianceFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in ComplianceFileData)
                    {
                        string inputfilepath = (file.FilePath).ToString();
                        inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                        inputfilepath = inputfilepath.Replace(@"/", @"\");

                        string ApplicationPathSource = string.Empty;

                        ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                        string filepathserver = ApplicationPathSource + inputfilepath;

                        string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));


                        filePath = filePath.Replace(@"\\", @"\");

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string destinationPath = string.Empty;

                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();

                            string Folder = "~/TempFiles/Contract";

                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);

                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = userID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);

                            //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //bw.Close();
                            //i++;

                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            i++;

                            CompDocReviewPath = finalfilepathforuploadfile;
                        }
                    }
                }
            }

            return CompDocReviewPath;
        }

        public static string fetchpathforLitigationAndriodNew(int CaseNoticeInstanceID, List<string> doctypes, string version, int userID, int TaskID,int FileID,int CustomerID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;
            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region AWS
                List<tbl_LitigationFileData> ComplianceFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> ComplianceDocument = new List<tbl_LitigationFileData>();
                if (FileID != -1)
                {
                    ComplianceDocument = DocumentManagement.GetLitigationFile(Convert.ToInt32(FileID)).ToList();
                }
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetLitigationFileDataDetails(Convert.ToInt32(FileID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;
                            string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;
                            Directory.CreateDirectory(finalDateFolder);

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(finalDateFolder, file.FileName);
                            CompDocReviewPath = filePath;                            
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Normal
                List<tbl_LitigationFileData> ComplianceFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> ComplianceDocument = new List<tbl_LitigationFileData>();
                if (FileID != -1)
                {
                    ComplianceDocument = DocumentManagement.GetLitigationFile(Convert.ToInt32(FileID)).ToList();
                }
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetLitigationFileDataDetails(Convert.ToInt32(FileID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));


                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = userID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }
                #endregion
            }
            return CompDocReviewPath;
        }
        public static string fetchpathforLitigationAndriod(int CaseNoticeInstanceID, List<string> doctypes, string version, int userID,int TaskID,int CustomerID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;
            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region AWS
                List<tbl_LitigationFileData> ComplianceFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> ComplianceDocument = new List<tbl_LitigationFileData>();
                if (TaskID != -1)
                {
                    ComplianceDocument = DocumentManagement.GetLitigationData1(Convert.ToInt32(CaseNoticeInstanceID), doctypes, TaskID).ToList();
                }
                else
                {
                    ComplianceDocument = DocumentManagement.GetLitigationData(Convert.ToInt32(CaseNoticeInstanceID), doctypes).ToList();
                }
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetLitigationData(Convert.ToInt32(CaseNoticeInstanceID), doctypes).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;
                            string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;
                            Directory.CreateDirectory(finalDateFolder);

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(finalDateFolder, file.FileName);
                            CompDocReviewPath = filePath;                            
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Normal
                List<tbl_LitigationFileData> ComplianceFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> ComplianceDocument = new List<tbl_LitigationFileData>();
                if (TaskID != -1)
                {
                    ComplianceDocument = DocumentManagement.GetLitigationData1(Convert.ToInt32(CaseNoticeInstanceID), doctypes, TaskID).ToList();
                }
                else
                {
                    ComplianceDocument = DocumentManagement.GetLitigationData(Convert.ToInt32(CaseNoticeInstanceID), doctypes).ToList();
                }
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetLitigationData(Convert.ToInt32(CaseNoticeInstanceID), doctypes).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));


                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = userID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }
                #endregion
            }
            return CompDocReviewPath;
        }

    }
}