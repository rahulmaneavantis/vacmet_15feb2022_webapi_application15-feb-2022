﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class LitigationManagement
    {
        public static bool EditAdvbillApproverEmail(string ApproverEmail, int CreatedBy, int CustomerID, int BranchID, string ViewState)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    if (ViewState == "1" && BranchID != 0)
                    {
                        var recordtoEdit = (from row in entities.tbl_Advcatebillapprovers
                                            where row.ID == BranchID
                                            select row).FirstOrDefault();

                        if (recordtoEdit != null)
                        {
                            recordtoEdit.ApproverEmail = ApproverEmail;
                            recordtoEdit.CustomerID = CustomerID;
                            recordtoEdit.IsDeleted = false;
                            recordtoEdit.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool AddAdvbillApproverEmail(string ApproverEmail,string ApproverLevel, int CreatedBy, int CustomerID, int BranchID, string ViewState)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.tbl_Advcatebillapprovers
                                 where row.ApproverEmail.Trim().ToLower() == ApproverEmail.Trim().ToLower()
                                 && row.ApproverLevel.Trim().ToLower() == ApproverLevel.Trim().ToLower()
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);

                    if (!query.Any() && ViewState == "0")
                    {
                        tbl_Advcatebillapprovers objcategory = new tbl_Advcatebillapprovers()
                        {
                            ApproverEmail = ApproverEmail,
                            ApproverLevel= ApproverLevel,
                            IsDeleted = false,
                            CreatedBy = CreatedBy,
                            CustomerID = CustomerID,
                        };

                        bool saveSuccess = CreateAdvBillApprovers(objcategory);

                        if (saveSuccess)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateAdvBillApprovers(tbl_Advcatebillapprovers newRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tbl_Advcatebillapprovers.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteAdvbillApproverEmail(int ID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.tbl_Advcatebillapprovers
                                       where row.ID == ID
                                       select row).FirstOrDefault();

                    var objcases = (from row in entities.SP_LitigationCaseAdvocateBill(CustomerID)
                                    where (row.Approver1 == ID || row.Approver2 ==ID || row.Approver3 == ID)
                                    select row).ToList();

                    if (objcases.Count == 0)
                    {
                        if (queryResult != null)
                        {
                            queryResult.IsDeleted = true;
                            entities.SaveChanges();
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool AddNoticeStages(string NoticeStage, int CreatedBy, int CustomerID, int BranchID, string ViewState)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.tbl_NoticeStage
                                 where row.NoticeStage.Trim().ToLower() == NoticeStage.Trim().ToLower()
                                 && row.CustomerID == CustomerID && row.IsDeleted == false
                                 select row);
                    if (ViewState == "1" && BranchID != 0)
                    {
                        if (BranchID > 0)
                        {
                            query = query.Where(entry => entry.ID != BranchID);
                        }
                        if (!query.Any())
                        {
                            var recordtoEdit = (from row in entities.tbl_NoticeStage
                                                where row.ID == BranchID
                                                select row).FirstOrDefault();

                            if (recordtoEdit != null)
                            {
                                recordtoEdit.NoticeStage = NoticeStage;
                                recordtoEdit.CustomerID = CustomerID;
                                recordtoEdit.IsDeleted = false;
                                recordtoEdit.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                            }
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else if (!query.Any() && ViewState == "0")
                    {
                        tbl_NoticeStage objcategory = new tbl_NoticeStage()
                        {
                            NoticeStage = NoticeStage,
                            IsDeleted = false,
                            CreatedBy = CreatedBy,
                            CustomerID = CustomerID
                        };

                        bool saveSuccess = CreateNoticeStage(objcategory);

                        if (saveSuccess)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool CreateNoticeStage(tbl_NoticeStage newRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tbl_NoticeStage.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteNoticeStagesByID(int CaseStageID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.tbl_NoticeStage
                                       where row.ID == CaseStageID
                                       select row).FirstOrDefault();

                    //var objcases = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(CustomerID)
                    //                where row.NoticeStage == CaseStageID
                    //                select row).ToList();

                    //if (objcases.Count == 0)
                    //{
                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                    //}
                    //else
                    //    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool AddRPACaseStatus(string CourtType, string State, string StateName, string CaseTypeID, string CaseType, string CaseNo, string CaseYear, string CourtName, string Bench, string BenchID, string District, string CreatedBy, string RPAStatus, int UserID, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_RPACaseStatus objcategory = new tbl_RPACaseStatus()
                    {
                        CourtType = CourtType,
                        StateName = StateName,
                        CaseTypeID = Convert.ToInt32(CaseTypeID),
                        CaseType = CaseType,
                        CaseNo = CaseNo,
                        CaseYear = CaseYear,
                        CourtName = CourtName,
                        Bench = Bench,
                        BenchID = BenchID,
                        District = District,
                        CreatedBy = CreatedBy,
                        CreatedOn = DateTime.Now,
                        RPAStatus = RPAStatus,
                        IsDeleted = false,
                        UserID = UserID,
                        CustomerID = CustomerID
                    };

                    if (!string.IsNullOrEmpty(State) && State != "0")
                    {
                        objcategory.State = Convert.ToInt32(State);
                    }
                    bool saveSuccess = CreateRPAStatus(objcategory);

                    if (saveSuccess)
                    {
                        return true;
                    }

                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateRPAStatus(tbl_RPACaseStatus newRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tbl_RPACaseStatus.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool UpdateLitigationReminder(tbl_LitigationCustomReminder recordDetail)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var recordtoEdit = (from row in entities.tbl_LitigationCustomReminder
                                        where row.ID == recordDetail.ID
                                        select row).FirstOrDefault();

                    if (recordtoEdit != null)
                    {
                        recordtoEdit.Type = recordDetail.Type;
                        recordtoEdit.InstanceID = recordDetail.InstanceID;
                        recordtoEdit.ReminderTitle = recordDetail.ReminderTitle;
                        recordtoEdit.Description = recordDetail.Description;
                        recordtoEdit.Remark = recordDetail.Remark;
                        recordtoEdit.RemindOn = recordDetail.RemindOn;
                        recordtoEdit.Status = recordDetail.Status;

                        recordtoEdit.IsDeleted = false;
                        recordtoEdit.UpdatedBy = recordDetail.UpdatedBy;
                        recordtoEdit.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }


        public static bool CreateLitigationReminder(tbl_LitigationCustomReminder newRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LitigationCustomReminder.Add(newRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistsLitigationReminder(tbl_LitigationCustomReminder objReminder, long reminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_LitigationCustomReminder
                             where row.ReminderTitle.ToLower().Equals(objReminder.ReminderTitle.Trim().ToLower())
                             && row.RemindOn == objReminder.RemindOn
                             && row.UserID == objReminder.UserID
                             && row.InstanceID == objReminder.InstanceID
                             select row);

                if (query != null)
                {
                    if (reminderID > 0)
                    {
                        query = query.Where(entry => entry.ID != reminderID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static long CreateAuditLog(string noticeOrCase, long noticeCaseInstanceID, string TableName, string Action, int customerID, int Createdby, string Remark, bool IsVisibleToUser)
        {
            //noticeOrCase 1-Case 2-Notice
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    tbl_LitigationAuditLog TBL_LAL = new tbl_LitigationAuditLog()
                    {
                        Type = noticeOrCase,
                        NoticeCaseInstanceID = noticeCaseInstanceID,
                        TableName = TableName,
                        Action = Action,
                        CustomerId = customerID,
                        CreatedBy = Createdby,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        Remark = Remark,
                        SentMail = false,
                        IsVisibleToUser = IsVisibleToUser,
                    };

                    entities.tbl_LitigationAuditLog.Add(TBL_LAL);
                    entities.SaveChanges();

                    return TBL_LAL.Id;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static List<int> GetAllHierarchy(int customerID, int customerBranchID)
        {
            List<NameValueHierarchy> hierarchy = null;
            List<int> branchHierarchyList = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {              
                var masterListCustomerBranches = (from row in entities.CustomerBranches
                                                  where row.IsDeleted == false
                                                  && row.CustomerID == customerID
                                                  && row.ID == customerBranchID
                                                  select row);

                var query = masterListCustomerBranches.Where(row => row.ID == customerBranchID);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    branchHierarchyList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities, branchHierarchyList, masterListCustomerBranches);
                }
            }

            return branchHierarchyList;
        }
        public static void LoadSubEntities(int customerID, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, List<int> branchHierarchyList, IQueryable<CustomerBranch> masterListCustomerBranches)
        {          
            IQueryable<CustomerBranch> query = masterListCustomerBranches.Where(row => row.ParentID == nvp.ID);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                branchHierarchyList.Add(item.ID);
                LoadSubEntities(customerID, item, false, entities, branchHierarchyList, masterListCustomerBranches);
            }
        }

        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> lst_LogMessages = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = ClassName;
                //msg.FunctionName = FunctionName;
                //msg.CreatedOn = DateTime.Now;
                //if (ex != null)
                //{
                //    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //    msg.StackTrace = ex.StackTrace;
                //}
                //else
                //{
                //    msg.Message = ClassName;
                //    msg.StackTrace = ClassName;
                //}
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;

                //if (ex != null)
                //{
                //    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                //}
                //else
                //{
                //    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                //}
                //Business.ComplianceManagement.InsertLogToDatabase(lst_LogMessages);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Gaurav
        public static List<View_LitigationReminderView> GetAssignedReminderList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID) /*, int taskStatus*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_LitigationReminderView
                             where row.CustomerID == customerID
                             //&& row.ReminderStatus == 0
                             select row).ToList(); //0-Pending 1-Sent

                //if (instanceID != -1)
                //    query = query.Where(entry => entry.InstanceID == instanceID).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.UserID == loggedInUserID).ToList();
                //else
                //    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (reminderType != 0)
                //{
                //    if (reminderType == 1) //Notice
                //        query = query.Where(entry => entry.Type == "N").ToList();
                //    else if (reminderType == 2) //Case
                //        query = query.Where(entry => entry.Type == "C").ToList();
                //    else if (reminderType == 3) //Task
                //        query = query.Where(entry => entry.Type == "T").ToList();
                //}

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.RemindOn)
                        .ThenBy(entry => entry.Type).ToList();

                return query;
            }
        }

        public static bool DeleteLitigationReminderByID(int reminderID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.tbl_LitigationCustomReminder
                                       where row.ID == reminderID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion
    }
}