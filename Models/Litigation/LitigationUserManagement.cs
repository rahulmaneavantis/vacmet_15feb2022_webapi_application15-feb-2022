﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class LitigationUserManagement
    {
        public static List<OutputList> GetRequiredUsers(List<User> userList, int userType) //UserType 1=Internal,2-Lawyer,3-External&Lawyer,
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in userList
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID == null).ToList();
                    else if (userType == 2)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                    else if (userType == 3)
                        query = query.Where(entry => entry.IsExternal == true || entry.LawyerFirmID != null).ToList();
                }

                var lstUsers = (from row in query
                                select new OutputList { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<OutputList>();

                return lstUsers.ToList();
            }
        }
        public static List<User> GetLitigationAllUsers(int customerID) //UserType 1=Internal,2-External,3-Both
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.LitigationRoleID != null
                             select row).ToList();

                return query.ToList();
            }
        }

        public static List<View_DisplayUserWithRating> GetAllUser(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.View_DisplayUserWithRating
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.ToLower().Contains(((filter).ToLower()).Trim()) || entry.LastName.ToLower().Contains(((filter).ToLower()).Trim()) || entry.Email.ToLower().Contains(((filter).ToLower()).Trim()) || entry.Address.ToLower().Contains(((filter).ToLower()).Trim()));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static string GetLitigationUsersByLawyerID(int customerID, int lawyerFirmID)
        {
            string ans = "False";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.LawyerFirmID == lawyerFirmID
                             select row).ToList();

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();
                if (lstUsers.Count > 0)
                {
                    ans = "True";
                }
                return ans;
            }
        }

        public static List<Sp_LitigationGetOwnerOfNotice_Result> GetOwnerListforNotice(int customerID, int NoticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Sp_LitigationGetOwnerOfNotice(customerID, NoticeStatus)
                             select row).ToList();

                return query;
            }
        }
    }
}