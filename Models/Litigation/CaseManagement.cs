﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models.Litigation
{
    public class CaseManagement
    {

        public static long CreateNewRefNo(tbl_CaseHearingRef newObj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    newObj.CreatedOn = DateTime.Now;

                    entities.tbl_CaseHearingRef.Add(newObj);
                    entities.SaveChanges();

                    return newObj.ID;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static int GetExistsRefNo(tbl_CaseHearingRef obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCourt = (from row in entities.tbl_CaseHearingRef
                                where row.CaseNoticeInstanceID == obj.CaseNoticeInstanceID
                                && row.CustomerID == obj.CustomerID
                                && row.IsDeleted == false
                                select row);

                if (objCourt != null)
                {
                    if (objCourt.Where(entry => entry.HearingDate == obj.HearingDate).Count() > 0)
                        return -1;
                    else
                        return objCourt.Count();
                }
                else
                    return 0;
            }
        }


        public static List<tbl_CaseHearingRef> GetAllRefNo(long customerID, long caseInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRefNo = (from row in entities.tbl_CaseHearingRef
                                where row.HearingRefNo != null
                                && row.IsDeleted == false
                                && row.CustomerID == customerID
                                && row.CaseNoticeInstanceID == caseInstanceID
                                select row);

                return lstRefNo.ToList();
            }
        }


        public static tbl_CaseHearingRef GetRefNoDetail(long refID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRefDetail = (from row in entities.tbl_CaseHearingRef
                                    where row.ID == refID
                                    select row).FirstOrDefault();

                return objRefDetail;
            }
        }


        public static long CreateCaseResponseLog(tbl_LegalCaseResponse objNextActionRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNextActionRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LegalCaseResponse.Add(objNextActionRecord);
                    entities.SaveChanges();

                    return objNextActionRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


        public static List<View_LegalCaseResponse> GetCaseResponseDetails(int CaseInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.View_LegalCaseResponse
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }


        public static List<View_CaseAssignedInstance> GetAssignedCaseList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_CaseAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (caseStatus != -1)
                {
                    //if (caseStatus == 3) //3--Closed otherwise Open
                    //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                    //else
                    //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }
                if (caseType != "" && caseType != "B")//--Both(B) --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.CaseType == caseType).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseCategoryID,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CaseStageID,
                                 g.CaseStage,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.FYName

                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseType = GCS.Key.CaseType,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CaseStageID = GCS.Key.CaseStageID,
                                 CaseStage = GCS.Key.CaseStage,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 FYName = GCS.Key.FYName
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.CaseType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }

        public static bool CreateCaseDocumentMapping(tbl_LitigationFileData newNoticeDoc)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newNoticeDoc.EnType = "A";
                    entities.tbl_LitigationFileData.Add(newNoticeDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static int ExistsCaseDocumentReturnVersion(tbl_LitigationFileData newDocumentRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.NoticeCaseInstanceID == newDocumentRecord.NoticeCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }
        public static List<View_CaseAssignedInstanceNew> GetAssignedCaseListNew(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID, int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_CaseAssignedInstanceNew
                             where row.CustomerID == CustomerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    if (noticeStatus == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseCategoryID,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CaseStageID,
                                 g.CaseStage,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 //g.CNature,
                                 //g.GlGbNcdNo,
                                 //g.GlWeight,
                                 //g.Amount,
                                 //g.PersonAppearingInCourt,
                                 //g.Payment_Terms,
                                 //g.CostAnalysis,
                                 g.CourtID,
                                 g.CourtName,
                                 g.OpenDate,
                                 g.CaseRiskID,
                                 g.Status,
                                 g.OwnerName,
                                 g.ActName,
                                 g.FinancialYear
                             } into GCS
                             select new View_CaseAssignedInstanceNew()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseType = GCS.Key.CaseType,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CaseStageID = GCS.Key.CaseStageID,
                                 CaseStage = GCS.Key.CaseStage,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 //CNature = GCS.Key.CNature,
                                 //GlWeight = GCS.Key.GlWeight,
                                 //GlGbNcdNo = GCS.Key.GlGbNcdNo,
                                 //Amount = GCS.Key.Amount,
                                 //PersonAppearingInCourt = GCS.Key.PersonAppearingInCourt,
                                 //Payment_Terms = GCS.Key.Payment_Terms,
                                 //CostAnalysis = GCS.Key.CostAnalysis,
                                 CourtID = GCS.Key.CourtID,
                                 CourtName = GCS.Key.CourtName,
                                 OpenDate = GCS.Key.OpenDate,
                                 CaseRiskID = GCS.Key.CaseRiskID,
                                 Status = GCS.Key.Status,
                                 OwnerName = GCS.Key.OwnerName,
                                 ActName = GCS.Key.ActName,
                                 FinancialYear = GCS.Key.FinancialYear
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }

        public static List<Sp_Litigation_CaseDocument_Result> GetCaseDocumentMapping(int caseInstanceID, string docType, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Sp_Litigation_CaseDocument(caseInstanceID, CustomerID)
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    if (docType != "")
                        queryResult = queryResult.Where(row => row.DocType.Trim() == docType).ToList();
                    else
                        queryResult = queryResult.Where(row => (row.DocType.Trim() == "C") || (row.DocType.Trim() == "CH") || (row.DocType.Trim() == "CT") || (row.DocType.Trim() == "CO") || (row.DocType.Trim() == "CD")).ToList();

                    //C-Case, CH-Case Hearing, CO-CaseOrder, CD-Compliance Document
                }

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }

        #region Amol
        public static bool DeleteCaseByID(int caseInstanceID, int deletedByUserID)
        {
            try
            {
                bool deleteSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<tbl_LitigationFileData> CaseDocToDelete = entities.tbl_LitigationFileData
                                                                .Where(x => x.NoticeCaseInstanceID == caseInstanceID
                                                                && (x.DocType.Trim() == "C" || x.DocType.Trim() == "CT" || x.DocType.Trim() == "CH")).ToList();

                    if (CaseDocToDelete.Count > 0)
                    {
                        CaseDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        CaseDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        CaseDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_LegalCaseResponse> ResponsesToDelete = entities.tbl_LegalCaseResponse.Where(x => x.CaseInstanceID == caseInstanceID).ToList();

                    if (ResponsesToDelete.Count > 0)
                    {
                        ResponsesToDelete.ForEach(entry => entry.IsActive = false);
                        ResponsesToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponsesToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_TaskScheduleOn> tasksToDelete = entities.tbl_TaskScheduleOn
                                                            .Where(x => x.NoticeCaseInstanceID == caseInstanceID
                                                            && x.TaskType == "C").ToList();

                    if (tasksToDelete.Count > 0)
                    {
                        tasksToDelete.ForEach(entry => entry.IsActive = false);
                        tasksToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        tasksToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    var queryRating = (from row in entities.tbl_LawyerListRating
                                       where row.CaseNoticeID == caseInstanceID
                                       && row.Type == 1
                                       select row).ToList();

                    if (queryRating.Count > 0)
                    {
                        queryRating.ForEach(entry => entry.IsActive = false);
                        queryRating.ForEach(entry => entry.UpdatedBy = deletedByUserID);
                        queryRating.ForEach(entry => entry.UpdatedOn = DateTime.Now);
                        entities.SaveChanges();

                        queryRating.ForEach(eachRatingRecord =>
                        {
                            var LawyerRating = (from row in entities.sp_LiGetLawyerRatingAVG(Convert.ToInt32(eachRatingRecord.LawyerID))
                                                select row).FirstOrDefault();

                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                var checkUser = (from row in entities.tbl_LawyerFinalRating
                                                 where row.LawyerID == objNewRate.LawyerID
                                                 && row.IsActive == true
                                                 select row).FirstOrDefault();
                                if (checkUser == null)
                                {
                                    objNewRate.CreatedBy = deletedByUserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    entities.tbl_LawyerFinalRating.Add(objNewRate);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkUser.Rating = objNewRate.Rating;
                                    checkUser.UpdatedBy = objNewRate.UpdatedBy;
                                    checkUser.UpdatedOn = objNewRate.UpdatedOn;
                                    entities.SaveChanges();
                                }
                            }
                        });
                    }



                    var queryResult = (from row in entities.tbl_LegalCaseInstance
                                       where row.ID == caseInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                    else
                        deleteSuccess = false;

                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static List<View_CaseAssignedInstance> GetAssignedCaseList(int loggedInUserID, string loggedInUserRole, int roleID, long CustomerID, int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_CaseAssignedInstance
                             where row.CustomerID == CustomerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    if (noticeStatus == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseCategoryID,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CaseStageID,
                                 g.CaseStage,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn
                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseType = GCS.Key.CaseType,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CaseStageID = GCS.Key.CaseStageID,
                                 CaseStage = GCS.Key.CaseStage,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }

        public static List<HearingDetailReport> GetHearingDetailCaseWise(long noticeCaseInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_LegalCaseResponse
                             where row.CaseInstanceID == noticeCaseInstanceID
                             && row.IsActive == true
                             select new HearingDetailReport()
                             {
                                 CaseInstanceID = row.CaseInstanceID,
                                 ResponseDate = row.ResponseDate,
                                 Description = row.Description,
                                 Remark = row.Remark,
                                 NextHearingDate = row.ReminderDate,
                                 CreatedByText = row.CreatedByText
                             }).ToList();

                return Query;
            }
        }
        public static List<ResponseDetailReport> GetResponseDetailNoticeWise(long noticeCaseInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.tbl_LegalNoticeResponse
                             where row.NoticeInstanceID == noticeCaseInstanceID
                             && row.IsActive == true
                             select new ResponseDetailReport()
                             {
                                 CaseInstanceID = row.NoticeInstanceID,
                                 ResponseDate = row.ResponseDate,
                                 Description = row.Description,
                                 Remark = row.Remark,
                                 NextHearingDate = row.ResponseDate,
                                 CreatedByText = row.CreatedByText
                             }).ToList();

                return Query;
            }
        }
        #endregion

        #region Suvek
        public static bool DeleteCaseStageDetailByID(int iD, long customerID)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var dtlcasestagebyID = (from row in Entities.tbl_TypeMaster
                                        where row.ID == iD && row.IsActive == true
                                        && row.customerID == customerID
                                        select row).FirstOrDefault();

                if (dtlcasestagebyID != null)
                {
                    dtlcasestagebyID.DeletedOn = DateTime.Now;
                    dtlcasestagebyID.DeletedBy = Convert.ToInt16(customerID);
                    dtlcasestagebyID.IsActive = false;
                    Entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        #endregion

        #region Gaurav

        public static int GetAssignedCaseCount(List<View_CaseAssignedInstance> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int caseStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             select row).ToList();

                if (caseStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                else
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName
                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                             }).ToList();
                }

                return query.Count();
            }
        }
        public static bool DeleteCustomFieldDyID(int iD, int typeID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var _objdelByID = (from row in entities.tbl_CustomField
                                       where row.IsActive == true
                                       && row.CaseNoticeCategory == typeID
                                       && row.ID == iD
                                       && row.CustomerID == CustomerID
                                       select row).FirstOrDefault();
                    if (_objdelByID != null)
                    {
                        _objdelByID.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool DeleteDocType(long docTypeID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == customerID
                                          && row.ID == docTypeID
                                          && row.IsDeleted == false
                                          select row).FirstOrDefault();
                if (noticecaseTypeList != null)
                {
                    noticecaseTypeList.IsDeleted = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<View_CaseAssignedInstance> GetAssignedCaseList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.View_CaseAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();


                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                //if (caseStatus != -1)
                //{
                //    if (caseStatus == 3) //3--Closed otherwise Open
                //        query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                //    else
                //        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                //}

                //if (caseType != "" && caseType != "B")//--Both(B) --Inward(I) and Outward(O)
                //    query = query.Where(entry => entry.CaseType == caseType).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseCategoryID,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CaseStageID,
                                 g.CaseStage,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.TxnStatusID
                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseType = GCS.Key.CaseType,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CaseStageID = GCS.Key.CaseStageID,
                                 CaseStage = GCS.Key.CaseStage,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 TxnStatusID = GCS.Key.TxnStatusID
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.CaseType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }
        #endregion
    }
}