﻿

using AppWebApplication.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;

namespace AppWebApplication.Models
{
    public class PublicClassZip
    {
        #region File get using fileId

        public static string FetchPathforInternalCompliance(int ScheduleOnID, string version, int userID, int FileId,int CustomerID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;

            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region Aws
                List<GetInternalComplianceDocumentsView> InternalComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> InternalComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                InternalComplianceDocument = DocumentManagement.GetFileDataInternalbyId(Convert.ToInt32(ScheduleOnID), Convert.ToInt32(FileId)).ToList();
                InternalComplianceDocument = InternalComplianceDocument.Where(x => x.ISLink == false).ToList();
                if (version.Equals("1.0"))
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (InternalComplianceFileData.Count <= 0)
                    {
                        InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                using (ZipFile ComplianceZip = new ZipFile())
                {

                    //var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    //if (compliancetype == 1)
                    //{
                    //    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}
                    //else
                    //{
                    //    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}

                    if (InternalComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in InternalComplianceFileData)
                        {

                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;
                            string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;
                            Directory.CreateDirectory(finalDateFolder);

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(finalDateFolder, file.FileName);
                            CompDocReviewPath = filePath;

                            //string inputfilepath = (file.FilePath).ToString();
                            //inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            //inputfilepath = inputfilepath.Replace(@"/", @"\");

                            //string ApplicationPathSource = string.Empty;

                            //ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            //string filepathserver = ApplicationPathSource + inputfilepath;

                            //string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            //filePath = filePath.Replace(@"\\", @"\");

                            //if (file.FilePath != null && File.Exists(filePath))
                            //{
                            //    string destinationPath = string.Empty;

                            //    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                            //    string Folder = "~/TempFiles";
                            //    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            //    string DateFolder = Folder + "/" + FileData;

                            //    string extension = System.IO.Path.GetExtension(filePath);

                            //    string DateFolderforconvert = (DateFolder).ToString();
                            //    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            //    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            //    string finalDateFolder = destinationPath + DateFolderforconvert;

                            //    Directory.CreateDirectory(finalDateFolder);

                            //    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                            //    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            //    string User = userID + "" + customerID + "" + FileDate;

                            //    string FileName = DateFolder + "/" + User + "" + extension;

                            //    //for server change
                            //    string filenameConvertforfilepath = FileName;
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            //    string destinationpathplaceoutput = destinationPath + @"\";

                            //    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            //    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            //    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            //    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            //    BinaryWriter bw = new BinaryWriter(fs);
                            //    //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    if (file.EnType == "M")
                            //    {
                            //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    }
                            //    else
                            //    {
                            //        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    }
                            //    bw.Close();
                            //    i++;

                              //  CompDocReviewPath = finalfilepathforuploadfile;
                            //}
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Normal
                List<GetInternalComplianceDocumentsView> InternalComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> InternalComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                InternalComplianceDocument = DocumentManagement.GetFileDataInternalbyId(Convert.ToInt32(ScheduleOnID), Convert.ToInt32(FileId)).ToList();
                InternalComplianceDocument = InternalComplianceDocument.Where(x => x.ISLink == false).ToList();
                if (version.Equals("1.0"))
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (InternalComplianceFileData.Count <= 0)
                    {
                        InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                using (ZipFile ComplianceZip = new ZipFile())
                {

                    var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    if (compliancetype == 1)
                    {
                        var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }
                    else
                    {
                        var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }

                    if (InternalComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in InternalComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = userID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }
                #endregion
            }
            return CompDocReviewPath;
        }

        public static string FetchPathforCompliance(int ScheduleOnID, string version, int userID, int FileId,int CustomerID)
        {
            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region AWS
                try
                {
                    string CompDocReviewPath = string.Empty;
                    string fetchhhOutput = string.Empty;
                    string FileNameCheck = string.Empty;

                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                    ComplianceDocument = DocumentManagement.GetFileDatabyId(Convert.ToInt32(ScheduleOnID), Convert.ToInt32(FileId)).ToList();
                    ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == false).ToList();
                    if (version.Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    }

                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        //var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                        //if (compliancetype == 1)
                        //{
                        //    var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                        //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                        //}
                        //else
                        //{
                        //    var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                        //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                        //}
                        
                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;
                                string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;
                                Directory.CreateDirectory(finalDateFolder);

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(finalDateFolder, file.FileName);
                                CompDocReviewPath = filePath;
                            }
                        }
                    }
                    return CompDocReviewPath;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                #endregion
            }
            else
            {
                #region Normal
                try
                {
                    string CompDocReviewPath = string.Empty;
                    string fetchhhOutput = string.Empty;
                    string FileNameCheck = string.Empty;

                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                    ComplianceDocument = DocumentManagement.GetFileDatabyId(Convert.ToInt32(ScheduleOnID), Convert.ToInt32(FileId)).ToList();
                    ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == false).ToList();
                    if (version.Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    }

                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                        if (compliancetype == 1)
                        {
                            var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                        }
                        else
                        {
                            var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                        }



                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string inputfilepath = (file.FilePath).ToString();
                                inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                                inputfilepath = inputfilepath.Replace(@"/", @"\");

                                string ApplicationPathSource = string.Empty;

                                ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                                string filepathserver = ApplicationPathSource + inputfilepath;

                                string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                                filePath = filePath.Replace(@"\\", @"\");

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string destinationPath = string.Empty;

                                    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                    string Folder = "~/TempFiles";
                                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + FileData;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    string DateFolderforconvert = (DateFolder).ToString();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                    string finalDateFolder = destinationPath + DateFolderforconvert;

                                    Directory.CreateDirectory(finalDateFolder);

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = userID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    //for server change
                                    string filenameConvertforfilepath = FileName;
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                    string destinationpathplaceoutput = destinationPath + @"\";

                                    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;

                                    CompDocReviewPath = finalfilepathforuploadfile;
                                }
                            }
                        }
                    }
                    return CompDocReviewPath;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                #endregion
            }
        }

        #endregion

        public static string fetchpathforandriodInternalDocument(int ScheduleOnID, string version, int userID,int CustomerID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;

            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region AWS
                List<GetInternalComplianceDocumentsView> InternalComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> InternalComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                InternalComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(ScheduleOnID)).ToList();

                if (version.Equals("1.0"))
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (InternalComplianceFileData.Count <= 0)
                    {
                        InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                InternalComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(ScheduleOnID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {

                    //var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    //if (compliancetype == 1)
                    //{
                    //    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}
                    //else
                    //{
                    //    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}
                    
                    InternalComplianceFileData = InternalComplianceFileData.Where(x => x.ISLink == false).ToList();
                    if (InternalComplianceFileData.Count > 0)
                    {
                        foreach (var file in InternalComplianceFileData)
                        {
                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;
                            string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;
                            Directory.CreateDirectory(finalDateFolder);

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(finalDateFolder, file.FileName);
                            CompDocReviewPath = filePath;                            
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Normal
                List<GetInternalComplianceDocumentsView> InternalComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> InternalComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                InternalComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(ScheduleOnID)).ToList();

                if (version.Equals("1.0"))
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (InternalComplianceFileData.Count <= 0)
                    {
                        InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    InternalComplianceFileData = InternalComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                InternalComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(ScheduleOnID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {

                    var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    if (compliancetype == 1)
                    {
                        var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }
                    else
                    {
                        var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }



                    InternalComplianceFileData = InternalComplianceFileData.Where(x => x.ISLink == false).ToList();
                    if (InternalComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in InternalComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = userID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }
                #endregion
            }
            return CompDocReviewPath;
        }

        public static string fetchpathforandriod(int ScheduleOnID, string version, int userID,int CustomerID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;
            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
            if (AWSData != null)
            {
                #region AWS
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();
                ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == false).ToList();
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    //var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    //if (compliancetype == 1)
                    //{
                    //    var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}
                    //else
                    //{
                    //    var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                    //    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    //}
                    
                    if (ComplianceFileData.Count > 0)
                    {
                        foreach (var file in ComplianceFileData)
                        {
                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;
                            string destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;
                            Directory.CreateDirectory(finalDateFolder);

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(finalDateFolder + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(finalDateFolder, file.FileName);
                            CompDocReviewPath = filePath;

                            //string inputfilepath = (file.FilePath).ToString();
                            //inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            //inputfilepath = inputfilepath.Replace(@"/", @"\");

                            //string ApplicationPathSource = string.Empty;

                            //ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            //string filepathserver = ApplicationPathSource + inputfilepath;

                            //string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            //filePath = filePath.Replace(@"\\", @"\");

                            //if (file.FilePath != null && File.Exists(filePath))
                            //{
                            //    string destinationPath = string.Empty;

                            //    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                            //    string Folder = "~/TempFiles";
                            //    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            //    string DateFolder = Folder + "/" + FileData;

                            //    string extension = System.IO.Path.GetExtension(filePath);

                            //    string DateFolderforconvert = (DateFolder).ToString();
                            //    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            //    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            //    string finalDateFolder = destinationPath + DateFolderforconvert;

                            //    Directory.CreateDirectory(finalDateFolder);

                            //    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                            //    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            //    string User = userID + "" + customerID + "" + FileDate;

                            //    string FileName = DateFolder + "/" + User + "" + extension;

                            //    //for server change
                            //    string filenameConvertforfilepath = FileName;
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            //    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            //    string destinationpathplaceoutput = destinationPath + @"\";

                            //    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            //    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            //    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            //    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            //    BinaryWriter bw = new BinaryWriter(fs);
                            //    //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    if (file.EnType == "M")
                            //    {
                            //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    }
                            //    else
                            //    {
                            //        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //    }
                            //    bw.Close();
                            //    i++;

                            //    CompDocReviewPath = finalfilepathforuploadfile;
                            //}
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Normal
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();
                ComplianceDocument = ComplianceDocument.Where(x => x.ISLink == false).ToList();
                if (version.Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
                }

                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();

                using (ZipFile ComplianceZip = new ZipFile())
                {

                    var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                    if (compliancetype == 1)
                    {
                        var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }
                    else
                    {
                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);
                    }



                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string inputfilepath = (file.FilePath).ToString();
                            inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                            inputfilepath = inputfilepath.Replace(@"/", @"\");

                            string ApplicationPathSource = string.Empty;

                            ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                            string filepathserver = ApplicationPathSource + inputfilepath;

                            string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                            filePath = filePath.Replace(@"\\", @"\");

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string destinationPath = string.Empty;

                                destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


                                string Folder = "~/TempFiles";
                                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + FileData;

                                string extension = System.IO.Path.GetExtension(filePath);

                                string DateFolderforconvert = (DateFolder).ToString();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                                DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                                string finalDateFolder = destinationPath + DateFolderforconvert;

                                Directory.CreateDirectory(finalDateFolder);

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = userID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                //for server change
                                string filenameConvertforfilepath = FileName;
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                                filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                                string destinationpathplaceoutput = destinationPath + @"\";

                                destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                                string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                                finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                                FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = finalfilepathforuploadfile;
                            }
                        }
                    }
                }
                #endregion
            }
            return CompDocReviewPath;
        }

        //public static string fetchpathforandriod(int ScheduleOnID, string version, int userID)
        //{
        //    string CompDocReviewPath = string.Empty;
        //    string fetchhhOutput = string.Empty;
        //    string FileNameCheck = string.Empty;

        //    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
        //    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

        //    ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();

        //    if (version.Equals("1.0"))
        //    {
        //        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
        //        if (ComplianceFileData.Count <= 0)
        //        {
        //            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
        //        }
        //    }
        //    else
        //    {
        //        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == version).ToList();
        //    }

        //    ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduleOnID)).ToList();

        //    using (ZipFile ComplianceZip = new ZipFile())
        //    {
        //        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
        //        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + version);

        //        if (ComplianceFileData.Count > 0)
        //        {
        //            int i = 0;
        //            foreach (var file in ComplianceFileData)
        //            {
        //                string inputfilepath = (file.FilePath).ToString();
        //                inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
        //                inputfilepath = inputfilepath.Replace(@"/", @"\");

        //                string ApplicationPathSource = string.Empty;

        //                ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

        //                string filepathserver = ApplicationPathSource + inputfilepath;

        //                string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

        //                filePath = filePath.Replace(@"\\", @"\");

        //                if (file.FilePath != null && File.Exists(filePath))
        //                {
        //                    string destinationPath = string.Empty;

        //                    destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();


        //                    string Folder = "~/TempFiles";
        //                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
        //                    string DateFolder = Folder + "/" + FileData;

        //                    string extension = System.IO.Path.GetExtension(filePath);

        //                    string DateFolderforconvert = (DateFolder).ToString();
        //                    DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
        //                    DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
        //                    string finalDateFolder = destinationPath + DateFolderforconvert;

        //                    Directory.CreateDirectory(finalDateFolder);

        //                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0);

        //                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //                    string User = userID + "" + customerID + "" + FileDate;

        //                    string FileName = DateFolder + "/" + User + "" + extension;

        //                    //for server change
        //                    string filenameConvertforfilepath = FileName;
        //                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
        //                    filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
        //                    filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

        //                    string destinationpathplaceoutput = destinationPath + @"\";

        //                    destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

        //                    string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
        //                    finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

        //                    FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
        //                    BinaryWriter bw = new BinaryWriter(fs);
        //                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));

        //                    bw.Close();
        //                    i++;

        //                    CompDocReviewPath = finalfilepathforuploadfile;
        //                }
        //            }
        //        }
        //    }
        //    return CompDocReviewPath;
        //}


        #region RLCS Document
        public class FileDataDetail
        {
            public int Id { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string FileKey { get; set; }
            public string Version { get; set; }
            public string EnType { get; set; }
        }

        private static List<FileDataDetail> GetComplianceDocuments(int FileId, long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.GetComplianceDocumentsViews
                            where row.FileID == FileId
                            && row.ScheduledOnID == ScheduledOnID
                            && row.IsDeleted == false
                            select new FileDataDetail
                            {
                                Id = row.FileID,
                                FileName = row.FileName,
                                FileKey = row.FileKey,
                                FilePath = row.FilePath,
                                Version = row.Version,
                                EnType = row.EnType
                            }).ToList();
                return file;
            }
        }

        private static List<FileDataDetail> GetNewDocuments(int Id, string FileType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (FileType.Equals("H"))
                {
                    var file = (from row in entities.RLCS_HistoricalDocument_FileData
                                where row.ID == Id
                                && row.IsDeleted == false
                                select new FileDataDetail
                                {
                                    Id = row.ID,
                                    FileName = row.FileName,
                                    FileKey = row.FileKey,
                                    FilePath = row.FilePath,
                                    Version = row.Version,
                                    EnType = row.EnType
                                }).ToList();
                    return file;
                }
                else if (FileType.Equals("C"))
                {
                    var file = (from row in entities.FileDatas
                                where row.ID == Id
                                && row.IsDeleted == false
                                select new FileDataDetail
                                {
                                    Id = row.ID,
                                    FileName = row.Name,
                                    FileKey = row.FileKey,
                                    FilePath = row.FilePath,
                                    Version = row.Version,
                                    EnType = row.EnType
                                }).ToList();
                    return file;
                }
                else if (FileType.Equals("N"))
                {
                    var file = (from row in entities.tbl_LitigationFileData
                                where row.ID == Id
                                && row.IsDeleted == false
                                select new FileDataDetail
                                {
                                    Id = row.ID,
                                    FileName = row.FileName,
                                    FileKey = row.FileKey,
                                    FilePath = row.FilePath,
                                    Version = row.Version,
                                    EnType = row.EnType
                                }).ToList();
                    return file;
                }
                else
                {
                    List<FileDataDetail> file = new List<FileDataDetail>();
                    return file;
                }
            }
        }

        public static string FetchRLCSDocumentPath(int FileID, int userID, int customerID, string FileType,string UserType)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;

            List<FileDataDetail> ComplianceFileData = new List<FileDataDetail>();

            ComplianceFileData = GetNewDocuments(FileID, FileType).ToList();

            using (ZipFile ComplianceZip = new ZipFile())
            {

                if (ComplianceFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in ComplianceFileData)
                    {
                        string inputfilepath = (file.FilePath).ToString();
                        inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                        inputfilepath = inputfilepath.Replace(@"/", @"\");

                        string ApplicationPathSource = string.Empty;

                        ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                        string filepathserver = ApplicationPathSource + inputfilepath;

                        string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                        filePath = filePath.Replace(@"\\", @"\");

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string destinationPath = string.Empty;

                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();

                            //string Folder = "~/TempFiles";
                            string Folder = "~/TempFiles/RLCS";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension.Equals(".csv") && UserType.Equals("Android"))
                            {
                                extension = ".xls";
                                //extension = ".xlsx";
                            }
                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = Convert.ToString(userID) + "" + Convert.ToString(customerID) + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            i++;

                            CompDocReviewPath = finalfilepathforuploadfile;
                        }
                    }
                }
            }
            return CompDocReviewPath;
        }

        public static string FetchRLCSComplianceDocumentPath(int FileID, int userID, int customerID, long ScheduledOnID)
        {
            string CompDocReviewPath = string.Empty;
            string fetchhhOutput = string.Empty;
            string FileNameCheck = string.Empty;

            List<FileDataDetail> ComplianceFileData = new List<FileDataDetail>();

            ComplianceFileData = GetComplianceDocuments(FileID, ScheduledOnID).ToList();

            using (ZipFile ComplianceZip = new ZipFile())
            {

                if (ComplianceFileData.Count > 0)
                {
                    int i = 0;
                    foreach (var file in ComplianceFileData)
                    {
                        string inputfilepath = (file.FilePath).ToString();
                        inputfilepath = inputfilepath.Replace(@"~", string.Empty).Trim();
                        inputfilepath = inputfilepath.Replace(@"/", @"\");

                        string ApplicationPathSource = string.Empty;

                        ApplicationPathSource = ConfigurationManager.AppSettings["SourcePath"].ToString();

                        string filepathserver = ApplicationPathSource + inputfilepath;

                        string filePath = Path.Combine(filepathserver, file.FileKey + Path.GetExtension(file.FileName));

                        filePath = filePath.Replace(@"\\", @"\");

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string destinationPath = string.Empty;

                            destinationPath = ConfigurationManager.AppSettings["destinationPath"].ToString();

                            //string Folder = "~/TempFiles";
                            string Folder = "~/TempFiles/RLCS";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);

                            string DateFolderforconvert = (DateFolder).ToString();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"~", string.Empty).Trim();
                            DateFolderforconvert = DateFolderforconvert.Replace(@"/", @"\");
                            string finalDateFolder = destinationPath + DateFolderforconvert;

                            Directory.CreateDirectory(finalDateFolder);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = Convert.ToString(userID) + "" + Convert.ToString(customerID) + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            //for server change
                            string filenameConvertforfilepath = FileName;
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(filenameConvertforfilepath.IndexOf('~') + 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Substring(1, filenameConvertforfilepath.Length - 1);
                            filenameConvertforfilepath = filenameConvertforfilepath.Replace(@"/", @"\");

                            string destinationpathplaceoutput = destinationPath + @"\";

                            destinationpathplaceoutput = destinationpathplaceoutput.Replace(@"\\", @"");

                            string finalfilepathforuploadfile = destinationpathplaceoutput + filenameConvertforfilepath;
                            finalfilepathforuploadfile = finalfilepathforuploadfile.Replace(@"\\", @"\");

                            FileStream fs = new FileStream(finalfilepathforuploadfile, FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            //bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            i++;

                            CompDocReviewPath = finalfilepathforuploadfile;
                        }
                    }
                }
            }
            return CompDocReviewPath;
        }

        #endregion

    }
}