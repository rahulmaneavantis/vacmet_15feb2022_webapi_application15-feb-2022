﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class NameValueHierarchyNew
    {
        public string text { get; set; }

        public int value { get; set; }

        public List<NameValueHierarchyNew> items { get; set; }

        public NameValueHierarchyNew()
        {
            items = new List<NameValueHierarchyNew>();
        }
    }
}