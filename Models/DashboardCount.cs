﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{


    public class CalenderDetailOutputDetails
    {
        public string IsPerformer { get; set; }
        public string IsReviwer { get; set; }
        public List<CalenderdataClass> PerformerData { get; set; }
        public List<CalenderdataClass> ReviwerData { get; set; }
    }

    public class DashboardCount
    {
        public List<int> RoleID { get; set; }
        public List<DashboardRoleCount> DashboardCounts { get; set; }
    }

    public class CalenderOutputDetails
    {
        public System.DateTime ScheduledOn { get; set; }
        public int Count { get; set; }
        public string Status { get; set; }
        //public string ScheduleOnID { get; set; }
    }

    public class CalenderdataClass
    {        
        public string Interimdays { get; set; }        
        public int UserID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public string ShortDescription { get; set; }
        public int ComplianceStatusID { get; set; }
        public int RoleID { get; set; }
        public long ScheduleOnID { get; set; }
        public int CustomerBranchID { get; set; }
        public string CType { get; set; }
        public int Risk { get; set; }
        public string Type { get; set; }
        public int ComplianceType { get; set; }
        public int ComplianceID { get; set; }
    }
}