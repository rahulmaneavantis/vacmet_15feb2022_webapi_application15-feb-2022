﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Web;
using AppWebApplication.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseTypeMasterManagement
    {
        public static List<Sp_BindLicenseType_Result> GetLicenseTypeWiseAssinedUser(int userID, string role, string Is_StatutoryInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var users = (from LTLCIM in entities.Sp_BindLicenseType(userID, role, Is_StatutoryInternal)
                             select LTLCIM);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
