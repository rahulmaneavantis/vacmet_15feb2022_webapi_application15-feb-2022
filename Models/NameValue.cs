﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class NameValue
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}