﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class c_json
    {
        public DataTable fetchDataApprover(int customerid, int userid, string Type, string date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(date);                
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 6
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 6 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }


                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }

        public DataTable fetchDataManagement(int customerid, int userid, string Type, string date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(date);
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }

                #region Statutory
                entities.Database.CommandTimeout = 180;

                List<SP_GetManagementCompliancesSummary_Result> mgmtQueryStatutory = new List<SP_GetManagementCompliancesSummary_Result>();
              
                try
                {
                  
                    if (isallorsi == "SI")
                    {                    
                        var customizedChecklist = CustomerManagement.CheckForClient((int)customerid, "MGMT_Checklist");

                        if (customizedChecklist)
                        {
                            mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                    }
                    
                }
                catch (Exception)
                {
                   
                }

                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
               
                List<SP_GetManagementInternalCompliancesSummary_Result> mgmtQueryInternal = new List<SP_GetManagementInternalCompliancesSummary_Result>();


                try
                {
                    
                    if (isallorsi == "SI")
                    {
                        entities.Database.CommandTimeout = 180;
                        mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 180;
                        mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                    }
                   
                }
                catch (Exception)
                {
                    
                }

                if (mgmtQueryInternal.Count > 0)
                {
                    mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion



                var QueryStatutoryPerformer = (from row in mgmtQueryStatutory
                                               where row.PerformerScheduledOn == dt
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();


                var QueryInternalPerformer = (from row in mgmtQueryInternal
                                              where row.ScheduledOn.Year == dt.Year
                                             && row.ScheduledOn.Month == dt.Month
                                             && row.ScheduledOn.Day == dt.Day
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.ComplianceInstanceID,
                                                  ScheduledOn = row.ScheduledOn,
                                                  ScheduledOnID = row.ScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.ComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();


                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));

                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }

        

        public DataTable fetchDataReviewer(int customerid, int userid, string Type, string date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(date);
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    //QueryStatutory= QueryStatutory.Where(entry=>entry.RoleID == 4 && entry.ScheduledOn == dt).ToList();
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 4
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 4 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }

        public static DateTime GetDateCalender(string date)
        {
            string date1 = "";
            if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(0, 4) + "-" + date.Substring(8, 2) + "-" + date.Substring(5, 2);
            }

            return Convert.ToDateTime(date1);
        }

        public DataTable fetchDataPerformer(int customerid, int userid, string Type, string date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = GetDateCalender(date);
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 3
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 3 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }


                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }

        public DataTable fetchData(int customerid, int userid, string Type, string roleid, string Rolecode, string month, string year)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DataTable statutorydatatable = new DataTable();
                statutorydatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                statutorydatatable.Columns.Add("ComplianceStatusID", typeof(int));
                statutorydatatable.Columns.Add("RoleID", typeof(int));
                statutorydatatable.Columns.Add("ScheduledOnID", typeof(long));
                statutorydatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable Internaldatatable = new DataTable();
                Internaldatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                Internaldatatable.Columns.Add("ComplianceStatusID", typeof(int));
                Internaldatatable.Columns.Add("RoleID", typeof(int));
                Internaldatatable.Columns.Add("ScheduledOnID", typeof(long));
                Internaldatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable mgmtstatutorydatatable = new DataTable();
                mgmtstatutorydatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                mgmtstatutorydatatable.Columns.Add("ComplianceStatusID", typeof(int));
                mgmtstatutorydatatable.Columns.Add("RoleID", typeof(int));
                mgmtstatutorydatatable.Columns.Add("ScheduledOnID", typeof(long));
                mgmtstatutorydatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable mgmtInternaldatatable = new DataTable();
                mgmtInternaldatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                mgmtInternaldatatable.Columns.Add("ComplianceStatusID", typeof(int));
                mgmtInternaldatatable.Columns.Add("RoleID", typeof(int));
                mgmtInternaldatatable.Columns.Add("ScheduledOnID", typeof(long));
                mgmtInternaldatatable.Columns.Add("CustomerBranchID", typeof(int));

                //DataTable statutorydatatable = new DataTable();
                //DataTable Internaldatatable = new DataTable();
                //DataTable mgmtstatutorydatatable = new DataTable();
                //DataTable mgmtInternaldatatable = new DataTable();

                bool IsApprover = false;

                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                if (!string.IsNullOrEmpty(roleid) && roleid == "8")
                {

                    if (Rolecode.Equals("MGMT"))
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        var GetApprover = (entities.Sp_GetApproverUsers(userid)).ToList();
                        if (GetApprover.Count > 0)
                        {
                            IsApprover = true;
                        }
                    }

                    if (IsApprover == false)
                    {
                        #region Statutory
                        entities.Database.CommandTimeout = 180;
                        List<SP_GetManagementCompliancesSummary_Result> mgmtQueryStatutory = new List<SP_GetManagementCompliancesSummary_Result>();
                       

                        try
                        {
                            
                            if (isallorsi == "SI")
                            {
                       
                                var customizedChecklist = CustomerManagement.CheckForClient((int)customerid, "MGMT_Checklist");

                                if (customizedChecklist)
                                {
                                    mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                                }
                                else
                                {
                                    mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                                }

                            }
                            else
                            {
                                mgmtQueryStatutory = (entities.SP_GetManagementCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            }

                        }
                        catch (Exception)
                        {

                        }
                        if (mgmtQueryStatutory.Count > 0)
                        {
                            mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }

                        if (mgmtQueryStatutory.Count > 0)
                        {

                            DateTime now = DateTime.Now;
                            DateTime startDate;
                            DateTime endDate;
                            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                            {
                                startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                                endDate = startDate.AddMonths(1).AddDays(-1);

                                mgmtQueryStatutory = mgmtQueryStatutory.Where(entry => entry.PerformerScheduledOn.Value.Date >= startDate.Date
                                                                              && entry.PerformerScheduledOn.Value.Date <= endDate.Date).ToList();
                            }

                            var Statutory = (from row in mgmtQueryStatutory
                                             select new
                                             {
                                                 ScheduledOn = row.PerformerScheduledOn,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.ScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();
                            if (Statutory.Count > 0)
                            {
                                foreach (var item in Statutory)
                                {
                                    mgmtstatutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                        }
                        #endregion

                        #region Internal
                        entities.Database.CommandTimeout = 180;
                        List<SP_GetManagementInternalCompliancesSummary_Result> mgmtQueryInternal = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                        //var mgmtQueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, isallorsi)
                        //                         select row).ToList();


                        try
                        {
                            //if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                            //{
                            //try
                            //{
                            //    mgmtQueryInternal = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                            //}
                            //catch (Exception)
                            //{
                            //LogLibrary.WriteErrorLog("GET fetchData MGMT Internal APP Error exception :" + cashinternalval);
                            //if (isallorsi == "SI")
                            //{
                            //    entities.Database.CommandTimeout = 180;
                            //    mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                            //}
                            //else
                            //{
                            //    entities.Database.CommandTimeout = 180;
                            //    mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            //}
                            //}
                            //}
                            //else
                            //{
                            if (isallorsi == "SI")
                            {
                                entities.Database.CommandTimeout = 180;
                                mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                            }
                            else
                            {
                                entities.Database.CommandTimeout = 180;
                                mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            }
                            //try
                            //{
                            //    StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, mgmtQueryInternal);
                            //    if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                            //    {
                            //        StackExchangeRedisExtensions.Remove(cashTimeval);
                            //        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                            //    }
                            //    else
                            //    {
                            //        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                            //    }
                            //}
                            //catch (Exception)
                            //{
                            //    LogLibrary.WriteErrorLog("SET fetchData MGMT Internal APP  Error exception :" + cashinternalval);
                            //}
                            //}
                        }
                        catch (Exception)
                        {
                            //LogLibrary.WriteErrorLog("KeyExists fetchData MGMT Internal APP Error exception :" + cashinternalval);
                            //if (isallorsi == "SI")
                            //{
                            //    entities.Database.CommandTimeout = 180;
                            //    mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMT")).ToList();
                            //}
                            //else
                            //{
                            //    entities.Database.CommandTimeout = 180;
                            //    mgmtQueryInternal = (entities.SP_GetManagementInternalCompliancesSummary(userid, (int)customerid, "MGMTALL")).ToList();
                            //}
                        }

                        if (mgmtQueryInternal.Count > 0)
                        {
                            mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        if (mgmtQueryInternal.Count > 0)
                        {

                            DateTime now = DateTime.Now;
                            DateTime startDate;
                            DateTime endDate;
                            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                            {
                                startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                                endDate = startDate.AddMonths(1).AddDays(-1);

                                mgmtQueryInternal = mgmtQueryInternal.Where(entry => entry.ScheduledOn.Date >= startDate.Date
                                                                              && entry.ScheduledOn.Date <= endDate.Date).ToList();
                            }
                            var Internal = (from row in mgmtQueryInternal
                                            select new
                                            {
                                                ScheduledOn = row.ScheduledOn,
                                                ComplianceStatusID = row.ComplianceStatusID,
                                                RoleID = row.RoleID,
                                                ScheduledOnID = row.ScheduledOnID,
                                                CustomerBranchID = row.CustomerBranchID,
                                            }).Distinct().ToList();
                            if (Internal.Count > 0)
                            {
                                foreach (var item in Internal)
                                {
                                    mgmtInternaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //mgmtInternaldatatable = Internal.ToDataTable();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Statutory
                        entities.Database.CommandTimeout = 180;
                        var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                              select row).ToList();

                        if (QueryStatutory.Count > 0)
                        {
                            QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }

                        if (QueryStatutory.Count > 0)
                        {
                            DateTime now = DateTime.Now;
                            DateTime startDate;
                            DateTime endDate;
                            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                            {
                                startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                                endDate = startDate.AddMonths(1).AddDays(-1);
                                QueryStatutory = QueryStatutory.Where(entry => entry.PerformerScheduledOn.Value.Date >= startDate.Date
                                                                    && entry.PerformerScheduledOn.Value.Date <= endDate.Date).ToList();
                            }

                            var Statutory = (from row in QueryStatutory
                                             select new
                                             {
                                                 ScheduledOn = row.PerformerScheduledOn,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.ScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();

                            if (Statutory.Count > 0)
                            {
                                foreach (var item in Statutory)
                                {
                                    statutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //statutorydatatable = Statutory.ToDataTable();
                        }
                        #endregion

                        #region Internal
                        entities.Database.CommandTimeout = 180;
                        var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                             select row).ToList();
                        if (QueryInternal.Count > 0)
                        {
                            QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        if (QueryInternal.Count > 0)
                        {
                            DateTime now = DateTime.Now;
                            DateTime startDate;
                            DateTime endDate;
                            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                            {
                                startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                                endDate = startDate.AddMonths(1).AddDays(-1);
                                QueryInternal = QueryInternal.Where(entry => entry.InternalScheduledOn.Date >= startDate.Date
                                                                  && entry.InternalScheduledOn.Date <= endDate.Date).ToList();
                            }

                            var Internal = (from row in QueryInternal
                                            select new
                                            {
                                                ScheduledOn = row.InternalScheduledOn,
                                                ComplianceStatusID = row.InternalComplianceStatusID,
                                                RoleID = row.RoleID,
                                                ScheduledOnID = row.InternalScheduledOnID,
                                                CustomerBranchID = row.CustomerBranchID,
                                            }).Distinct().ToList();

                            if (Internal.Count > 0)
                            {
                                foreach (var item in Internal)
                                {
                                    Internaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //Internaldatatable = Internal.ToDataTable();
                        }
                        #endregion
                    }
                }
                else if (!string.IsNullOrEmpty(roleid) && roleid == "3")
                {
                    //performer as well as reviewer
                    //DateTime date = DateTime.Today.AddDays(7);
                    #region Statutory
                    entities.Database.CommandTimeout = 180;
                    var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                          select row).ToList();

                    if (QueryStatutory.Count > 0)
                    {
                        QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (QueryStatutory.Count > 0)
                    {

                        DateTime now = DateTime.Now;
                        DateTime startDate;
                        DateTime endDate;
                        if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                        {
                            startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                            endDate = startDate.AddMonths(1).AddDays(-1);
                            QueryStatutory = QueryStatutory.Where(entry => entry.PerformerScheduledOn.Value.Date >= startDate.Date
                                                              && entry.PerformerScheduledOn.Value.Date <= endDate.Date).ToList();
                        }

                        var Statutory = (from row in QueryStatutory
                                         select new
                                         {
                                             ScheduledOn = row.PerformerScheduledOn,
                                             ComplianceStatusID = row.ComplianceStatusID,
                                             RoleID = row.RoleID,
                                             ScheduledOnID = row.ScheduledOnID,
                                             CustomerBranchID = row.CustomerBranchID,
                                         }).Distinct().ToList();


                        if (Statutory.Count > 0)
                        {
                            foreach (var item in Statutory)
                            {
                                statutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                            }
                        }
                        //statutorydatatable = Statutory.ToDataTable();
                    }
                    #endregion

                    #region Internal
                    entities.Database.CommandTimeout = 180;
                    var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                         select row).ToList();
                    if (QueryInternal.Count > 0)
                    {
                        QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryInternal.Count > 0)
                    {
                        DateTime now = DateTime.Now;
                        DateTime startDate;
                        DateTime endDate;
                        if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
                        {
                            startDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                            endDate = startDate.AddMonths(1).AddDays(-1);
                            QueryInternal = QueryInternal.Where(entry => entry.InternalScheduledOn.Date >= startDate.Date
                                                              && entry.InternalScheduledOn.Date <= endDate.Date).ToList();
                        }
                        var Internal = (from row in QueryInternal
                                        select new
                                        {
                                            ScheduledOn = row.InternalScheduledOn,
                                            ComplianceStatusID = row.InternalComplianceStatusID,
                                            RoleID = row.RoleID,
                                            ScheduledOnID = row.InternalScheduledOnID,
                                            CustomerBranchID = row.CustomerBranchID,
                                        }).Distinct().ToList();

                        if (Internal.Count > 0)
                        {
                            foreach (var item in Internal)
                            {
                                Internaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                            }
                        }
                        //Internaldatatable = Internal.ToDataTable();
                    }
                    #endregion                  
                }
                DataTable newtable = new DataTable();
                // Performer or reviewer
                if (statutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutorydatatable);
                }
                if (Internaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(Internaldatatable);
                }

                //management
                if (mgmtstatutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtstatutorydatatable);
                }
                if (mgmtInternaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtInternaldatatable);
                }
                return newtable;
            }
        }

        public DataTable fetchData(int customerid, int userid, string Type, string month, string Role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DataTable statutorydatatable = new DataTable();
                statutorydatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                statutorydatatable.Columns.Add("ComplianceStatusID", typeof(int));
                statutorydatatable.Columns.Add("RoleID", typeof(int));
                statutorydatatable.Columns.Add("ScheduledOnID", typeof(long));
                statutorydatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable Internaldatatable = new DataTable();
                Internaldatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                Internaldatatable.Columns.Add("ComplianceStatusID", typeof(int));
                Internaldatatable.Columns.Add("RoleID", typeof(int));
                Internaldatatable.Columns.Add("ScheduledOnID", typeof(long));
                Internaldatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable mgmtstatutorydatatable = new DataTable();
                mgmtstatutorydatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                mgmtstatutorydatatable.Columns.Add("ComplianceStatusID", typeof(int));
                mgmtstatutorydatatable.Columns.Add("RoleID", typeof(int));
                mgmtstatutorydatatable.Columns.Add("ScheduledOnID", typeof(long));
                mgmtstatutorydatatable.Columns.Add("CustomerBranchID", typeof(int));

                DataTable mgmtInternaldatatable = new DataTable();
                mgmtInternaldatatable.Columns.Add("ScheduledOn", typeof(DateTime));
                mgmtInternaldatatable.Columns.Add("ComplianceStatusID", typeof(int));
                mgmtInternaldatatable.Columns.Add("RoleID", typeof(int));
                mgmtInternaldatatable.Columns.Add("ScheduledOnID", typeof(long));
                mgmtInternaldatatable.Columns.Add("CustomerBranchID", typeof(int));

                //DataTable statutorydatatable = new DataTable();
                //DataTable Internaldatatable = new DataTable();
                //DataTable mgmtstatutorydatatable = new DataTable();
                //DataTable mgmtInternaldatatable = new DataTable();

                bool IsApprover = false;

                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                if (!string.IsNullOrEmpty(month) && month == "8")
                {

                    if (Role.Equals("MGMT"))
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        var GetApprover = (entities.Sp_GetApproverUsers(userid)).ToList();
                        if (GetApprover.Count > 0)
                        {
                            IsApprover = true;
                        }
                    }

                    if (IsApprover == false)
                    {
                        #region Statutory
                        entities.Database.CommandTimeout = 180;
                        var mgmtQueryStatutory = (from row in entities.ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, isallorsi)
                                                  select row).ToList();
                        if (mgmtQueryStatutory.Count > 0)
                        {
                            mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        if (mgmtQueryStatutory.Count > 0)
                        {
                            var Statutory = (from row in mgmtQueryStatutory
                                             select new
                                             {
                                                 ScheduledOn = row.PerformerScheduledOn,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.ScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();
                            if (Statutory.Count > 0)
                            {
                                foreach (var item in Statutory)
                                {
                                    mgmtstatutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                        }
                        #endregion

                        #region Internal
                        entities.Database.CommandTimeout = 180;
                        var mgmtQueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, isallorsi)
                                                 select row).ToList();

                        if (mgmtQueryInternal.Count > 0)
                        {
                            mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }

                        if (mgmtQueryInternal.Count > 0)
                        {
                            var Internal = (from row in mgmtQueryInternal
                                            select new
                                            {
                                                ScheduledOn = row.InternalScheduledOn,
                                                ComplianceStatusID = row.InternalComplianceStatusID,
                                                RoleID = row.RoleID,
                                                ScheduledOnID = row.InternalScheduledOnID,
                                                CustomerBranchID = row.CustomerBranchID,
                                            }).Distinct().ToList();
                            if (Internal.Count > 0)
                            {
                                foreach (var item in Internal)
                                {
                                    mgmtInternaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //mgmtInternaldatatable = Internal.ToDataTable();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Statutory
                        entities.Database.CommandTimeout = 180;
                        var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                              select row).ToList();

                        if (QueryStatutory.Count > 0)
                        {
                            QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }

                        if (QueryStatutory.Count > 0)
                        {
                            var Statutory = (from row in QueryStatutory
                                             select new
                                             {
                                                 ScheduledOn = row.PerformerScheduledOn,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.ScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();

                            if (Statutory.Count > 0)
                            {
                                foreach (var item in Statutory)
                                {
                                    statutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //statutorydatatable = Statutory.ToDataTable();
                        }
                        #endregion

                        #region Internal
                        entities.Database.CommandTimeout = 180;
                        var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                             select row).ToList();
                        if (QueryInternal.Count > 0)
                        {
                            QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        if (QueryInternal.Count > 0)
                        {

                            var Internal = (from row in QueryInternal
                                            select new
                                            {
                                                ScheduledOn = row.InternalScheduledOn,
                                                ComplianceStatusID = row.InternalComplianceStatusID,
                                                RoleID = row.RoleID,
                                                ScheduledOnID = row.InternalScheduledOnID,
                                                CustomerBranchID = row.CustomerBranchID,
                                            }).Distinct().ToList();

                            if (Internal.Count > 0)
                            {
                                foreach (var item in Internal)
                                {
                                    Internaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                                }
                            }
                            //Internaldatatable = Internal.ToDataTable();
                        }
                        #endregion
                    }
                }
                else if (!string.IsNullOrEmpty(month) && month == "3")
                {
                    //performer as well as reviewer
                    //DateTime date = DateTime.Today.AddDays(7);
                    #region Statutory
                    entities.Database.CommandTimeout = 180;
                    var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                          select row).ToList();

                    if (QueryStatutory.Count > 0)
                    {
                        QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (QueryStatutory.Count > 0)
                    {
                        var Statutory = (from row in QueryStatutory
                                         select new
                                         {
                                             ScheduledOn = row.PerformerScheduledOn,
                                             ComplianceStatusID = row.ComplianceStatusID,
                                             RoleID = row.RoleID,
                                             ScheduledOnID = row.ScheduledOnID,
                                             CustomerBranchID = row.CustomerBranchID,
                                         }).Distinct().ToList();


                        if (Statutory.Count > 0)
                        {
                            foreach (var item in Statutory)
                            {
                                statutorydatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                            }
                        }
                        //statutorydatatable = Statutory.ToDataTable();
                    }
                    #endregion

                    #region Internal
                    entities.Database.CommandTimeout = 180;
                    var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                         select row).ToList();
                    if (QueryInternal.Count > 0)
                    {
                        QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryInternal.Count > 0)
                    {

                        var Internal = (from row in QueryInternal
                                        select new
                                        {
                                            ScheduledOn = row.InternalScheduledOn,
                                            ComplianceStatusID = row.InternalComplianceStatusID,
                                            RoleID = row.RoleID,
                                            ScheduledOnID = row.InternalScheduledOnID,
                                            CustomerBranchID = row.CustomerBranchID,
                                        }).Distinct().ToList();

                        if (Internal.Count > 0)
                        {
                            foreach (var item in Internal)
                            {
                                Internaldatatable.Rows.Add(item.ScheduledOn, item.ComplianceStatusID, item.RoleID, item.ScheduledOnID, item.CustomerBranchID);
                            }
                        }
                        //Internaldatatable = Internal.ToDataTable();
                    }
                    #endregion                  
                }
                DataTable newtable = new DataTable();
                // Performer or reviewer
                if (statutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutorydatatable);
                }
                if (Internaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(Internaldatatable);
                }

                //management
                if (mgmtstatutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtstatutorydatatable);
                }
                if (mgmtInternaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtInternaldatatable);
                }
                return newtable;
            }
        }

        public DataTable fetch(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.CustomerID == customerid && row.UserID == userid
                             select new
                             {
                                 ScheduledOn = row.ScheduledOn,
                             }).Distinct().ToList();

                var Query2 = (from row in entities.InternalComplianceInstanceTransactionViews
                              where row.CustomerID == customerid && row.UserID == userid
                              select new
                              {
                                  ScheduledOn = row.InternalScheduledOn,
                              }).Distinct().ToList();

                DataTable table = new DataTable();
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                if (Query.Count > 0)
                {
                    foreach (var item in Query)
                    {
                        table.Rows.Add(item.ScheduledOn);
                    }
                }
                if (Query2.Count > 0)
                {
                    foreach (var item in Query2)
                    {
                        table.Rows.Add(item.ScheduledOn);
                    }
                }
                return table;
            }
        }
    }
}