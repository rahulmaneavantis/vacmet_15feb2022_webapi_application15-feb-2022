﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebApplication.Models
{
    public class SendSms
    {

        public static bool sendsmsto(string mobileNumber, string message, string DLTTEID)
        {
            //Your authentication key          
            //Multiple mobiles numbers separated by comma           
            //Sender ID,While using route4 sender id should be 6 characters long.        
            //Prepare you post parameters

            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["authkey"]);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&DLT_TE_ID={0}", DLTTEID);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", ConfigurationManager.AppSettings["senderId"]);
            sbPostData.AppendFormat("&route={0}", 5);

            try
            {
                //Call Send SMS API
                //  string sendSMSUri = "http://www.vngsms.com/api/sendhttp.php";
                string sendSMSUri = "http://sms.vngsms.in/api/sendhttp.php"; //New Api URL
                //Create HTTPWebrequest
                System.Net.HttpWebRequest httpWReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
                return true;
            }
            catch (SystemException ex)
            {
                //throw ex;
                return false;
            }
        }


        //public static bool sendsmsto(string mobileNumber, string message)
        //{
        //    //Your authentication key          
        //    //Multiple mobiles numbers separated by comma           
        //    //Sender ID,While using route4 sender id should be 6 characters long.        
        //    //Prepare you post parameters

        //    StringBuilder sbPostData = new StringBuilder();
        //    sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["authkey"]);
        //    sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        //    sbPostData.AppendFormat("&message={0}", message);
        //    sbPostData.AppendFormat("&sender={0}", ConfigurationManager.AppSettings["senderId"]);
        //    sbPostData.AppendFormat("&route={0}", 5);

        //    try
        //    {
        //        //Call Send SMS API
        //        //  string sendSMSUri = "http://www.vngsms.com/api/sendhttp.php";
        //        //string sendSMSUri = "http://vngsms.in/api/sendhttp.php"; //New Api URL
        //        string sendSMSUri = "http://sms.vngsms.in/api/sendhttp.php"; //New Api URL
        //                                                                     //Create HTTPWebrequest
        //        System.Net.HttpWebRequest httpWReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(sendSMSUri);
        //        //Prepare and Add URL Encoded data
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        byte[] data = encoding.GetBytes(sbPostData.ToString());
        //        //Specify post method
        //        httpWReq.Method = "POST";
        //        httpWReq.ContentType = "application/x-www-form-urlencoded";
        //        httpWReq.ContentLength = data.Length;
        //        using (Stream stream = httpWReq.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }
        //        //Get the response
        //        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)httpWReq.GetResponse();
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        string responseString = reader.ReadToEnd();

        //        //Close the response
        //        reader.Close();
        //        response.Close();
        //        return true;
        //    }
        //    catch (SystemException ex)
        //    {
        //        //throw ex;
        //        return false;
        //    }
        //}

        //public static bool sendsmsto(string mobileNumber, string message)
        //{
        //    //Your authentication key          
        //    //Multiple mobiles numbers separated by comma           
        //    //Sender ID,While using route4 sender id should be 6 characters long.        
        //    //Prepare you post parameters

        //    StringBuilder sbPostData = new StringBuilder();
        //    sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["authkey"]);
        //    sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        //    sbPostData.AppendFormat("&message={0}", message);
        //    sbPostData.AppendFormat("&sender={0}", ConfigurationManager.AppSettings["senderId"]);
        //    sbPostData.AppendFormat("&route={0}", 4);

        //    try
        //    {
        //        //Call Send SMS API
        //        string sendSMSUri = "http://www.vngsms.com/api/sendhttp.php";
        //        //Create HTTPWebrequest
        //        System.Net.HttpWebRequest httpWReq = (System.Net.HttpWebRequest) System.Net.WebRequest.Create(sendSMSUri);
        //        //Prepare and Add URL Encoded data
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        byte[] data = encoding.GetBytes(sbPostData.ToString());
        //        //Specify post method
        //        httpWReq.Method = "POST";
        //        httpWReq.ContentType = "application/x-www-form-urlencoded";
        //        httpWReq.ContentLength = data.Length;
        //        using (Stream stream = httpWReq.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }
        //        //Get the response
        //        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse) httpWReq.GetResponse();
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        string responseString = reader.ReadToEnd();

        //        //Close the response
        //        reader.Close();
        //        response.Close();
        //        return true;
        //    }
        //    catch (SystemException ex)
        //    {
        //        //throw ex;
        //        return false;
        //    }
        //}
    }
}