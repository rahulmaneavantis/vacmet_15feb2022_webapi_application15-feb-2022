﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class OverdueComplianceData
    {
        public string Email { get; set; }
        public int location { get; set; }
        public int category { get; set; }
        public int ActID { get; set; }
        public int RiskId { get; set; }
        public int IsFlag { get; set; }
        public int page { get; set; }
    }
}