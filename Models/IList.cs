﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppWebApplication.Models
{
    public interface IList<T>
    {
        void Insert(int index, T item, string key);        
        void RemoveAt(int index,string keys);
        bool Remove(List<T> item, string key);
        List<T> Get(string key);
        void Clear(string key);
        bool KeyExist(string key);
        void CloseConnection();

    }
}
