﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AppWebApplication.Data;
using AppWebApplication.Models;
using System.Configuration;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalCanned_ReportManagement
    {
        public static List<SP_Kendo_GetALLInternalEBData_Result> GetMyReportDataInternal(int Customerid, int userID, string FlagUser, string status)
        {
            #region New
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_Kendo_GetALLInternalEBData_Result> transactionsQuery = new List<SP_Kendo_GetALLInternalEBData_Result>();

                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string CacheName = string.Empty;
                if (objlocal == "Local")
                {
                    CacheName = "Cache_GetInternalData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
                }
                else
                {
                    CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
                }
                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                {
                    try
                    {
                        transactionsQuery = StackExchangeRedisExtensions.GetList<SP_Kendo_GetALLInternalEBData_Result>(CacheName);
                    }
                    catch (Exception ex)
                    {
                        LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (entities.SP_Kendo_GetALLInternalEBData(userID, Customerid, FlagUser, status)).ToList();
                    }
                }
                else
                {
                    entities.Database.CommandTimeout = 360;
                    transactionsQuery = (entities.SP_Kendo_GetALLInternalEBData(userID, Customerid, FlagUser, status)).ToList();
                    try
                    {
                        StackExchangeRedisExtensions.SetList<SP_Kendo_GetALLInternalEBData_Result>(CacheName, transactionsQuery);
                    }
                    catch (Exception ex)
                    {
                        LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                    }
                }
                transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
            #endregion

            #region OLD
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    List<SP_Kendo_GetALLInternalEBData_Result> transactionsQuery = new List<SP_Kendo_GetALLInternalEBData_Result>();

            //    int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //    string CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + FlagUser + "_" + status;
            //    transactionsQuery = (List<SP_Kendo_GetALLInternalEBData_Result>)HttpContext.Current.Cache[CacheName];
            //    if (transactionsQuery == null)
            //    {
            //        entities.Database.CommandTimeout = 180;
            //        transactionsQuery = (entities.SP_Kendo_GetALLInternalEBData(userID, Customerid, FlagUser, status)).ToList();
            //        HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //    }
            //    //if (FlagUser.Equals("MGMT"))
            //    //{
            //    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
            //    //}
            //    //if (status.Equals("Event Based"))
            //    //{
            //    //    transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
            //    //}
            //    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            //}
            #endregion
        }

        public static List<InternalComplianceInstanceTransactionViewNew> GetMyReportDataInternal(int Customerid, int userID, string Statusflag)
        {
            #region New
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionViewNew> transactionsQuery = new List<InternalComplianceInstanceTransactionViewNew>();

                if (Statusflag.Equals("MGMT") || Statusflag.Equals("AUD"))//roleID-8/9
                {
                    entities.Database.CommandTimeout = 180;
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }
                else if (Statusflag.Equals("DEPT"))
                {
                    entities.Database.CommandTimeout = 180;
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
                                         join row1 in entities.EntitiesAssignment_IsDept on row.CustomerBranchID equals row1.BranchID
                                         where row.CustomerID == Customerid
                                         && row1.UserID == userID
                                         select row).Distinct().ToList();
                }
                else if (Statusflag.Equals("APPR"))//RoleID == 6
                {
                    entities.Database.CommandTimeout = 180;
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    List<int> RoleIDs = new List<int>();
                    //RoleIDs.Add(3);
                    //RoleIDs.Add(4);
                    //RoleIDs.Add(6);
                    if (Statusflag.Equals("PRA"))
                        RoleIDs.Add(3);

                    if (Statusflag.Equals("REV"))
                        RoleIDs.Add(4);

                    entities.Database.CommandTimeout = 180;
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && RoleIDs.Contains((int)entry.RoleID)).ToList();
                }

                transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

                return transactionsQuery.Distinct().OrderBy(entry => entry.ScheduledOn).ToList();
            }
            #endregion
          
            #region old
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    List<InternalComplianceInstanceTransactionViewNew> transactionsQuery = new List<InternalComplianceInstanceTransactionViewNew>();

            //    if (Statusflag.Equals("MGMT") || Statusflag.Equals("AUD"))//roleID-8/9
            //    {
            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + Statusflag;
            //        transactionsQuery = (List<InternalComplianceInstanceTransactionViewNew>)HttpContext.Current.Cache[CacheName];

            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
            //                                 join row1 in entities.EntitiesAssignmentInternals
            //                                  on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
            //                                 where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
            //                                 && row1.UserID == userID
            //                                 //&& row.RoleID == 3
            //                                 select row).Distinct().ToList();

            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //    }
            //    else if (Statusflag.Equals("DEPT"))
            //    {
            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + Statusflag;
            //        transactionsQuery = (List<InternalComplianceInstanceTransactionViewNew>)HttpContext.Current.Cache[CacheName];

            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
            //                                 join row1 in entities.EntitiesAssignment_IsDept on row.CustomerBranchID equals row1.BranchID
            //                                 where row.CustomerID == Customerid
            //                                 && row1.UserID == userID
            //                                 select row).Distinct().ToList();

            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //    }
            //    else if (Statusflag.Equals("APPR"))//RoleID == 6
            //    {
            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + Statusflag;
            //        transactionsQuery = (List<InternalComplianceInstanceTransactionViewNew>)HttpContext.Current.Cache[CacheName];

            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
            //                                 where row.CustomerID == Customerid
            //                                 select row).ToList();
            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //        transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
            //    }
            //    else
            //    {
            //        List<int> RoleIDs = new List<int>();
            //        //RoleIDs.Add(3);
            //        //RoleIDs.Add(4);
            //        //RoleIDs.Add(6);
            //        if (Statusflag.Equals("PRA"))
            //            RoleIDs.Add(3);

            //        if (Statusflag.Equals("REV"))
            //            RoleIDs.Add(4);

            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalData_" + userID + "_" + Customerid + "_" + Statusflag;
            //        transactionsQuery = (List<InternalComplianceInstanceTransactionViewNew>)HttpContext.Current.Cache[CacheName];

            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViewNews
            //                                 where row.CustomerID == Customerid
            //                                 select row).ToList();
            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //        transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && RoleIDs.Contains((int)entry.RoleID)).ToList();
            //        //transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
            //    }

            //    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

            //    return transactionsQuery.Distinct().OrderBy(entry => entry.ScheduledOn).ToList();
            //}
            #endregion
        }

        public static List<SP_kendo_getInternalChecklist_Result> GetMyReportDataInternalChecklist(int Customerid, int userID, string FlagIsApp, int StatusFlag)
        {
            #region New
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_kendo_getInternalChecklist_Result> transactionsQuery = new List<SP_kendo_getInternalChecklist_Result>();
                if (StatusFlag == 3 || StatusFlag == 4)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string CacheName = string.Empty;
                    if (objlocal == "Local")
                    {
                        CacheName = "Cache_GetInternalCheckListData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    else
                    {
                        CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
                    }
                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                    {
                        try
                        {
                            transactionsQuery = StackExchangeRedisExtensions.GetList<SP_kendo_getInternalChecklist_Result>(CacheName);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            entities.Database.CommandTimeout = 360;
                            transactionsQuery = (from row in entities.SP_kendo_getInternalChecklist(userID, Customerid, FlagIsApp)
                                                 select row).ToList();
                        }
                    }
                    else
                    {
                        entities.Database.CommandTimeout = 360;
                        transactionsQuery = (from row in entities.SP_kendo_getInternalChecklist(userID, Customerid, FlagIsApp)
                                             select row).ToList();
                        try
                        {
                            StackExchangeRedisExtensions.SetList<SP_kendo_getInternalChecklist_Result>(CacheName, transactionsQuery);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                        }
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else
                {
                    return transactionsQuery;
                }
            }
            #endregion

            #region OLD
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    List<SP_kendo_getInternalChecklist_Result> transactionsQuery = new List<SP_kendo_getInternalChecklist_Result>();

            //    if (StatusFlag == 3)
            //    {
            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
            //        transactionsQuery = (List<SP_kendo_getInternalChecklist_Result>)HttpContext.Current.Cache[CacheName];
            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.SP_kendo_getInternalChecklist(userID, Customerid, FlagIsApp)                                             
            //                                 select row).ToList();
            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //    }
            //    else if (StatusFlag == 4)
            //    {
            //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
            //        string CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_" + FlagIsApp + "_" + StatusFlag;
            //        transactionsQuery = (List<SP_kendo_getInternalChecklist_Result>)HttpContext.Current.Cache[CacheName];
            //        if (transactionsQuery == null)
            //        {
            //            entities.Database.CommandTimeout = 180;
            //            transactionsQuery = (from row in entities.SP_kendo_getInternalChecklist(userID, Customerid, FlagIsApp)                                             
            //                                 select row).ToList();
            //            HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            //        }
            //    }

            //    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

            //    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            //}
            #endregion
        }

        public static List<InternalComplianceInstanceCheckListTransactionViewNew> GetMyReportDataInternalChecklist(int Customerid, int userID, string Statusflag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {              
                List<InternalComplianceInstanceCheckListTransactionViewNew> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionViewNew>();

                if (Statusflag.Equals("MGMT") || Statusflag.Equals("AUD"))//roleID-8/9
                {
                    int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
                    string CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_" + Statusflag;
                    transactionsQuery = (List<InternalComplianceInstanceCheckListTransactionViewNew>)HttpContext.Current.Cache[CacheName];
                    if (transactionsQuery == null)
                    {
                        entities.Database.CommandTimeout = 180;
                        transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViewNews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID 
                                         //&& row.RoleID == 3
                                         select row).Distinct().ToList();
                        HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                    }
                }
                else
                {
                    List<int> RoleIDs = new List<int>();
                    if (Statusflag.Equals("PRA"))
                        RoleIDs.Add(3);

                    if (Statusflag.Equals("REV"))
                        RoleIDs.Add(4);

                    if (Statusflag.Equals("APPR"))
                        RoleIDs.Add(6);

                    //RoleIDs.Add(3);
                    //RoleIDs.Add(4);
                    //RoleIDs.Add(6);
                    int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeReport"]);
                    string CacheName = "CacheGetInternalCheckListData_" + userID + "_" + Customerid + "_" + Statusflag;
                    transactionsQuery = (List<InternalComplianceInstanceCheckListTransactionViewNew>)HttpContext.Current.Cache[CacheName];
                    if (transactionsQuery == null)
                    {
                        transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViewNews
                                             where row.CustomerID == Customerid
                                             select row).ToList();

                        HttpContext.Current.Cache.Insert(CacheName, transactionsQuery, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && RoleIDs.Contains((int)entry.RoleID)).ToList();
                }
                transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();

                return transactionsQuery.Distinct().OrderBy(entry => entry.ScheduledOn).ToList();
            }

        }
        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForPerformer(int Customerid, int userID, int risk, CheckListCannedReportPerformer status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformer.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CheckListCannedReportPerformer.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    //case CheckListCannedReportPerformer.ClosedDelayed:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                    //    break;

                    //case CheckListCannedReportPerformer.PendingForReview:
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                    //    break;

                    //case CheckListCannedReportPerformer.Rejected:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                    //    break;
                }
                // Find data through String contained in Description
                //if (StringType != "")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForReviewer(int Customerid, int userID, int risk, CheckListCannedReportPerformer status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 4
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformer.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CheckListCannedReportPerformer.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    //case CheckListCannedReportPerformer.ClosedDelayed:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                    //    break;

                    //case CheckListCannedReportPerformer.PendingForReview:
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                    //    break;

                    //case CheckListCannedReportPerformer.Rejected:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                    //    break;
                }
                // Find data through String contained in Description
                //if (StringType != "")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int ComplianceType, int risk, CannedReportFilterNewStatus status, int location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();

                //var GetApprover = (from row in entities.InternalComplianceAssignments
                //                   where row.UserID == userID
                //                   select row)
                //                   .GroupBy(a => a.RoleID)
                //                   .Select(a => a.FirstOrDefault())
                //                   .Select(entry => entry.RoleID).ToList();

                //if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //    RoleID = 6;

                //if (RoleID == 8)
                //{
                transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                     join row1 in entities.EntitiesAssignmentInternals
                                      on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                     where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                     && row1.UserID == userID && row.RoleID == 3
                                     select row).Distinct().ToList();
                //}
                //else if (RoleID == 6)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();

                //    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                //}
                //else
                //{
                //transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                     where row.CustomerID == Customerid
                //                     select row).ToList();

                //transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                //}

                //Type Filter
                //if (type != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                //}
                //category Filter
                //if (category != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                //}

                //Datr Filter
                //if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                //}

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;

                        //case CannedReportFilterNewStatus.Upcoming:
                        //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && entry.InternalComplianceStatusID == 1).ToList();
                        //    break;
                        //case CannedReportFilterNewStatus.Completed:
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5).ToList();
                        //    break;
                        //case CannedReportFilterNewStatus.Overdue:
                        //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        //    break;
                        //case CannedReportFilterNewStatus.PendingForReview:
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11).ToList();
                        //    break;
                }
                // Find data through String contained in Description
                //if (StringType != "")
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();

                //var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                        where row.CustomerID == userID && row.RoleID == performerRoleID
                //                        select row);

                //var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.CustomerID == Customerid
                //                         select row).ToList();

                //transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID != 1).ToList();

                //if (RoleID != 8)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                //}

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a=>a.FirstOrDefault())
                                   .Select(entry=>entry.RoleID).ToList();

                if (GetApprover.Count==1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();                    
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }                

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;

                    //case CannedReportFilterNewStatus.Upcoming:
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && entry.InternalComplianceStatusID == 1).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.Completed:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.Overdue:
                    //    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.PendingForReview:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11).ToList();
                    //    break;
                }

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();

                //var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         select row);

                //var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.CustomerID == custimerid
                //                         select row).ToList();

                //transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID != 1).ToList();

                //if (RoleID != 8)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
                //}

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
                }                

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;

                    //case CannedReportFilterNewStatus.PendingForReview:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.Delayed:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 5).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.Open:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 1).ToList();
                    //    break;
                    //case CannedReportFilterNewStatus.Rejected:
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                    //    break;
                    //default:
                    //    break;
                }

                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }

        //public static List<ComplianceInstanceTransactionView> GetCannedReportDataForApprover(int userID, CannedReportFilterForApprover filter)
        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForApprover(int userID, CannedReportFilterForApprover filter)
        {
            //InternalComplianceInstanceTransactionView
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;

                //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                //                         where row.UserID == userID && row.RoleID == approverRoleID
                //                         select row);

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == approverRoleID
                                         select row);

                switch (filter)
                {
                    case CannedReportFilterForApprover.ForFinalApproval:
                        //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5);
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5);
                        break;
                }

                //return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }
    }
}
