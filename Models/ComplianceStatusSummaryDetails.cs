﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ComplianceStatusSummaryDetails
    {
        public List<SP_GetCannedReportCompliancesSummary_Result> StatutoryData { get; set; }
        public List<InternalComplianceDashboardSummaryView> InternalData { get; set; }
                
        public List<CategoryListCount> CategoryLists { get; set; }

        public List<RiskCount> RiskCount { get; set; }
               
    }
}