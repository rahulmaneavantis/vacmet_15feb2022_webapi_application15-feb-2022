﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class AdvocateBillData
    {
        public int CaseInstanceID { get; set; }
        public string CaseTitle { get; set; }
        public string InvoiceNo { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string Remark { get; set; }
        public string Lawyer { get; set; }
        public string AssignTo { get; set; }
        public string Hearing { get; set; }
        public string status { get; set; }
        public string CaseNo { get; set; }
        public int AdvocateBillId { get; set; }
        public string CaseDescription { get; set; }
        public string CustomerBranch { get; set; }
        public string Department { get; set; }
        public string Lawfirm { get; set; }

        public Nullable<DateTime> InvoiceDate { get; set; }
        public string Currency { get; set; }


    }
    public class NotificationUSerList
    {
        public string UserName { get; set; }
        public string TypeOfMail { get; set; }
        public DateTime SentOn { get; set; }
        public int UserID { get; set; }
    }
    public class UserList
    {
        public string FullName { get; set; }

        public int UID { get; set; }
    }
    public class GetComplianceData
    {
        public string IDs { get; set; }
        public string ComplianceID { get; set; }
        public string Comment { get; set; }
        public string Email { get; set; }
        public string BranchID { get; set; }

    }
    public class ChecklistRemark
    {
        public int ID;
        public string remark;
        public string CreatedUser;
        public DateTime Createon;
    }

}