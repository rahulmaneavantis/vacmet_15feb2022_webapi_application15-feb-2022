﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class Mst_StateCountry
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string CountryName { get; set; }
    }
    public class ActManagement
    {

        public static List<Act_Document> getFileNamebyID(long actID)
        {
            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var getfileName = (from row in dbcontext.Act_Document
                                       where row.Act_ID == actID
                                       orderby row.Id ascending
                                       select row).ToList();

                    return getfileName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Mst_StateCountry> GetAllState()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateList = (from row in entities.States
                                 join row1 in entities.Countries
                                 on row.CountryID equals row1.ID
                                 where row.Name != null
                                 select new Mst_StateCountry
                                 {
                                     ID = row.ID,
                                     Name = row.Name,
                                     CountryID = row.CountryID,
                                     CountryName = row1.Name,
                                 }).ToList();

                return StateList.ToList();
            }
        }
        public static List<SP_ComplianceWiseActMaster_Result> GetAllAct(int CId, string flag, int Uid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_ComplianceWiseActMaster(CId, flag, Uid)
                            select row);
                return acts.ToList();
            }
        }
        public static List<SP_GetAssignedCategorynew_Result> GetAllAssignedCategoryAllNew(int Customerid, string Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetAssignedCategorynew_Result> objCat = new List<SP_GetAssignedCategorynew_Result>();

                objCat = (from row in entities.SP_GetAssignedCategorynew(Customerid, Type)
                          select row).ToList();

                return objCat;
            }
        }

        public static List<SP_GetAssignedActs_Result> GetActsByCID(int CId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetAssignedActs(CId)
                            select row);
                return acts.ToList();
            }
        }
        public static List<SP_GetComplianceSubTypeID_Result> GetSubTypeByUserID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceSubtype = (from row in entities.SP_GetComplianceSubTypeID(UserID)
                            select row);
                return complianceSubtype.ToList();
            }
        }

        public static List<SP_GetAssignedTypes_Result> GetAllAssignedTypeAll(int UserId, int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetAssignedTypes_Result> objType = new List<SP_GetAssignedTypes_Result>();

                objType = (from row in entities.SP_GetAssignedTypes(Customerid, UserId)
                           select row).ToList();

                return objType;
            }
        }

        public static List<SP_GetAssignedCategory_Result> GetAllAssignedCategoryAll(int UserId, int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetAssignedCategory_Result> objCat = new List<SP_GetAssignedCategory_Result>();

                objCat = (from row in entities.SP_GetAssignedCategory(Customerid, UserId)
                          select row).ToList();

                return objCat;
            }
        }

        public static List<SP_GetActsByUserID_Result> GetActsByUserID(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_GetActsByUserID(UserID)
                            select row);
                return acts.ToList();
            }
        }

        public static List<SP_GetActiveEventDropDown_Result> GetActiveEvents(int eventRoleID, int Customerid, int UserID, int userRoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Event = (from row in entities.SP_GetActiveEventDropDown(UserID, Customerid, userRoleID)
                             select row).ToList();
                return Event;
            }
        }
        public static List<SP_GetActiveEventNatureDropDown_Result> GetEventNature(int eventRoleID, int Customerid, int UserID, int EventID, int userRoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Event = (from row in entities.SP_GetActiveEventNatureDropDown(UserID, Customerid, EventID, userRoleID)
                             select row).ToList();
                return Event;
            }
        }

       
    }
}