﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models
{
   
   
    public class RLCSUserManagement
    {
        public static bool IsValidProfileID(string ProfileID, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.ProfileID.ToUpper().Trim() == ProfileID.ToUpper().Trim()
                        select row1).FirstOrDefault();
            }
            return user != null;
        }

        public static bool IsValidUserName(string username, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = (from userRow in entities.Users
                        join row1 in entities.RLCS_User_Mapping
                        on userRow.ID equals row1.AVACOM_UserID
                        where userRow.IsDeleted == false
                        && row1.UserID.ToUpper().Trim() == username.ToUpper().Trim()
                        select row1).FirstOrDefault();
            }
            return user != null;
        }

        public static int GetCustomerIDRLCS(string CorporateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from row in entities.RLCS_Customer_Corporate_Mapping
                                      where row.CO_CorporateID.Trim().ToUpper() == CorporateID.Trim().ToUpper()
                                      select row.AVACOM_CustomerID).FirstOrDefault();
                if (productmapping != null)
                {
                    return Convert.ToInt32(productmapping);
                }
                else
                {
                    return -1;
                }
            }
        }

        public static bool IsValidUserID(string username, string password, out RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                user = null;
                string encpassword = string.Empty;
                var userdetails = (from userRow in entities.Users
                                   join RUM in entities.RLCS_User_Mapping
                                   on userRow.ID equals RUM.AVACOM_UserID
                                   where userRow.IsDeleted == false
                                   && (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                   //&& RUM.Password == password
                                   select RUM).ToList();

                if (userdetails != null)
                {
                    if (userdetails.Count > 0)
                    {
                        if (userdetails.FirstOrDefault().EnType == "M")
                        {
                            encpassword = Util.CalculateMD5Hash(password);
                        }
                        else if (userdetails.FirstOrDefault().EnType == "A")
                        {
                            encpassword = Util.CalculateAESHash(password);
                        }
                        //encpassword = password;
                        user = (from RUM in userdetails
                                where (RUM.UserID.ToUpper().Trim() == username.ToUpper().Trim() || RUM.Email.ToUpper().Trim().Equals(username.ToUpper().Trim()))
                                && RUM.Password == encpassword
                                select RUM).FirstOrDefault();
                    }
                }
            }
            return user != null;
        }
        //public static bool IsValidUserID(string username, string password, out RLCS_User_Mapping user)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        user = (from userRow in entities.Users
        //                join row1 in entities.RLCS_User_Mapping
        //                on userRow.ID equals row1.AVACOM_UserID
        //                where userRow.IsDeleted == false
        //                && row1.UserID.ToUpper().Trim() == username.ToUpper().Trim()
        //                && row1.Password == password
        //                select row1).FirstOrDefault();
        //    }
        //    return user != null;
        //}

        public static void Create(MaintainLoginDetail obj)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.MaintainLoginDetails.Add(obj);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void LastLoginUpdate(int userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    User userToUpdate = (from Row in entities.Users
                                         where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                         select Row).FirstOrDefault();

                    if (userToUpdate != null)
                    {
                        userToUpdate.LastLoginTime = DateTime.Now;

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }

 
}