﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class UserActList
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int? StateID { get; set; }

        public int? CategoryID { get; set; }

    }
    public class SetTrialDaysModel
    {
        public long no_of_days { get; set; }

        public DateTime start_date { get; set; }

        public DateTime end_date { get; set; }
    }

    public class UserConfiguredList
    {
        public string Email { get; set; }
        public List<int?> Acts { get; set; }

    }

    public class UpdateSelfConfigurationModel
    {
        public string Email { get; set; }

        public List<int?> state { get; set; }

        public List<int?> industry { get; set; }

        public List<int?> category { get; set; }

    }

    public class Count
    {
        public int CountValue { get; set; }
    }
    public class StateInputDetail
    {
        public int ID { get; set; }
    }
    public class LegalUpdateOTP
    {
        public string Email { get; set; }
        public int OTP { get; set; }
    }
    public class IndustryDetail
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class StateDetail
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string CountryId { get; set; }
    }
    public class ActList
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class ComplianceList
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}