﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class LocationDetails
    {
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
}