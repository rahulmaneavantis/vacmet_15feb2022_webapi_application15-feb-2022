﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ChangePasswordDetails
    {
        public string ChangePassStatus { get; set; }
        public string ChangePassMsg { get; set; }
    }
}