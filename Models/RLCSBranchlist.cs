﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class RLCSBranchlist
    {
       public static List<long> Branchlist = new List<long>();

        public static List<long> GetAllHierarchy(int customerID, int customerbranchid)
        {
            Branchlist.Clear();
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return Branchlist;
        }

        //public static List<NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        //{
        //    List<NameValueHierarchy> hierarchy = null;
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.CustomerBranches
        //                     where row.IsDeleted == false && row.CustomerID == customerID
        //                     && row.ID == customerbranchid
        //                     select row);
        //        hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
        //        foreach (var item in hierarchy)
        //        {
        //            Branchlist.Add(item.ID);
        //            LoadSubEntities(customerID, item, true, entities);
        //        }
        //    }
        //    return hierarchy;
        //}

        public static void LoadSubEntities(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

    }
}