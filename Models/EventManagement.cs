﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;
namespace AppWebApplication.Models
{
    public class EventManagement
    {
        public static long? GetEventClassification(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long? EventClassificationID = (from row in entities.Events
                                               where row.ID == eventID
                                               select row.EventClassificationID).FirstOrDefault();
                return EventClassificationID;
            }
        }

        public static List<SP_ClosedEvent_Result> GetClosedEvents(long Userid, int EventClassificationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_ClosedEvent()
                            join row1 in entities.ClosedEventDetails
                            on row.EventScheduledOnId equals row1.EventSchuduleID
                            where row.UserID == Userid
                            select row).ToList();
                if (EventClassificationID != -1)
                {
                    data = data.Where(entry => entry.EventClassificationID == EventClassificationID).ToList();
                }
                return data;
            }
        }

        public static List<SP_ClosedEvent_Result> GetClosedEvents(long Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_ClosedEvent()
                            join row1 in entities.ClosedEventDetails
                            on row.EventScheduledOnId equals row1.EventSchuduleID
                            where row.UserID == Userid
                            select row).ToList();
                return data;
            }
        }
        public static List<Event_Assigned_View> GetAllAssignedInstancesCount(int eventRoleID, int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var complianceInstancesQuery = (from row in entities.Event_Assigned_View
                                                where row.RoleID == eventRoleID
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.ToList(); //complianceInstancesQuery.OrderByDescending(entry => entry.EventAssignmentID).ToList();
            }
        }

    }
}