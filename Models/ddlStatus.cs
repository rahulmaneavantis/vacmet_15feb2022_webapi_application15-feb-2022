﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class ddlStatus
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}