﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppWebApplication.Models
{
    public interface ISearchService<T>
    {
        SearchResult<T> Search(string query, int page, int pageSize);
        SearchResult<ActListNew> SearchByCategory(string query, IEnumerable<string> tags, int page, int pageSize);
        IEnumerable<string> Autocomplete(string query, int count);
    }
    public class SearchResult<T>
    {
        public int Total { get; set; }
        public int Page { get; set; }
        public IEnumerable<T> Results { get; set; }
        public int ElapsedMilliseconds { get; set; }
    }
    public class ActListNew
    {

        public int ActID { get; set; }
        public int ComplianceTypeId { get; set; }
        public int ComplianceCategoryId { get; set; }
        public string ActName { get; set; }
        public string ComplianceTypeName { get; set; }
        public string ComplianceCategoryName { get; set; }
        public int? StateID { get; set; }
        public string State { get; set; }
        public int IsMyFavourite { get; set; }
      
    }
}
