﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class PenaltySummaryChart
    {
        public int CriticalApr_Jun { get; set; }
        public int CriticalJul_Sep { get; set; }
        public int CriticalOct_Dec { get; set; }
        public int CriticalJan_Mar { get; set; }

        public int HighApr_Jun { get; set; }
        public int HighJul_Sep { get; set; }
        public int HighOct_Dec { get; set; }
        public int HighJan_Mar { get; set; }

        public int MediumApr_Jun { get; set; }
        public int MediumJul_Sep { get; set; }
        public int MediumOct_Dec { get; set; }
        public int MediumJan_Mar { get; set; }

        public int LowApr_Jun { get; set; }
        public int LowJul_Sep { get; set; }
        public int LowOct_Dec { get; set; }
        public int LowJan_Mar { get; set; }

    }
}


