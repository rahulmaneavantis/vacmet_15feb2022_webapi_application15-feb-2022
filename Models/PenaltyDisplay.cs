﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class PenaltyDisplay
    {
        public static List<SP_GetPenaltyQuarterWise_DeptHead_Result> GetPenaltyComplianceDetailsDashboard_DeptHead(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise_DeptHead(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                        where row.PenaltySubmit == "S" && row.IsPenaltySave == false && row.RoleID == 4 
                                        && (row.NonComplianceType == 2 || row.NonComplianceType == 0)                                        
                                        && (row.Penalty != null || row.Interest != null)
                                        select row).ToList();

              

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != ""
                     && startdate.ToString() != "1/1/0001 12:00:00 AM"
                     && enddate.ToString() != "1/1/0001 12:00:00 AM"
                     && startdate.ToString() != "1/1/1900 12:00:00 AM"
                     && enddate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<SP_GetPenaltyQuarterWise_DeptHead_Result> GetPenaltyComplianceDetailsDashboardPending_DeptHead(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise_DeptHead(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == true && (row.RoleID == 3 || row.RoleID == 4)
                                         select row).ToList();

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<SP_GetPenaltyQuarterWise_Result> GetPenaltyComplianceDetailsDashboard(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "S" && row.IsPenaltySave == false && row.RoleID == 4
                                        && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                        && (row.Penalty != null || row.Interest != null)
                                         select row).ToList();



                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != ""
                    && startdate.ToString() != "1/1/0001 12:00:00 AM"
                    && enddate.ToString() != "1/1/0001 12:00:00 AM"
                    && startdate.ToString() != "1/1/1900 12:00:00 AM"
                    && enddate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<SP_GetPenaltyQuarterWiseApprover_Result> GetPenaltyComplianceDetailsDashboardApprover(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWiseApprover(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "S" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == false && row.RoleID == 6
                                         select row).ToList();

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != ""
                   && startdate.ToString() != "1/1/0001 12:00:00 AM"
                   && enddate.ToString() != "1/1/0001 12:00:00 AM"
                   && startdate.ToString() != "1/1/1900 12:00:00 AM"
                   && enddate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }
        public static List<SP_GetPenaltyQuarterWise_Result> GetPenaltyComplianceDetailsDashboardPending(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && (row.RoleID == 3)
                                         select row).ToList();
                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }
        public static List<SP_GetPenaltyQuarterWiseApprover_Result> GetPenaltyComplianceDetailsDashboardPendingApprover(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, int IsApprover)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWiseApprover(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && (row.RoleID == 3)
                                         select row).ToList();
                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }

    }
}