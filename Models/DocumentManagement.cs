﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AppWebApplication.Data;
namespace AppWebApplication.Models
{
    public class DocumentManagement
    {
        public static long GetCreatorId(long ContractIniID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.ID == ContractIniID
                            select row.CreatedBy).FirstOrDefault();
                return Convert.ToInt32(data);
            }
        }
        public static long GetRequestorId(long ContractIniID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.ID == ContractIniID
                            select row.RequestTo).FirstOrDefault();
                return Convert.ToInt32(data);
            }
        }
        public static long CreateContractTermSheetDocument(ContractTermSheetDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ContractTermSheetDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static long CreateContractDocument(ContractDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ContractDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static List<Cont_tbl_FileData> GetContractData(int ContractID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.Cont_tbl_FileData
                                where row.ContractID == ContractID
                                && row.IsDeleted == false
                                && row.ID == FileID
                                select row).ToList();

                return fileData;
            }
        }
        public static void Litigation_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static List<tbl_LitigationFileData> GetLitigationData(int CaseNoticeInstanceId, List<string> doctypes)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.tbl_LitigationFileData
                                where row.NoticeCaseInstanceID == CaseNoticeInstanceId && doctypes.Contains(row.DocType.Trim())
                                && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        public static List<tbl_LitigationFileData> GetLitigationFile(int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.tbl_LitigationFileData
                                where row.ID == FileID
                                && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }

        public static List<tbl_LitigationFileData> GetLitigationData1(int CaseNoticeInstanceId, List<string> doctypes, int taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.tbl_LitigationFileData
                                where row.NoticeCaseInstanceID == CaseNoticeInstanceId
                                && row.DocTypeInstanceID == taskID
                                && (row.DocType.Trim() == "CT" || row.DocType.Trim() == "NT" || row.DocType.Trim() == "T")
                                && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        public static List<tbl_LitigationFileData> GetLitigationFileDataDetails(int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.tbl_LitigationFileData
                                where row.ID == FileID
                                && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        public static List<SP_GetAssignedActsDocument_Departmenthead_Result> GetAssignedActsDocumentDepartmenthead_(int UserId, string Role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var documentData = (from row in entities.SP_GetAssignedActsDocument_Departmenthead(UserId, Role)
                                    select row).ToList();

                return documentData;
            }
        }
        public static List<SP_GetAssignedActsDocument_Result> GetAssignedActsDocument(int UserId, int RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var documentData = (from row in entities.SP_GetAssignedActsDocument(UserId, RoleID)
                                    select row).ToList();

                return documentData;
            }
        }
        public static List<GetComplianceDocumentsView> GetFileDatabyId(int ScheduledOnID, int FileId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                && row.FileID == FileId
                                select row).ToList();

                return fileData;
            }
        }

        public static List<GetInternalComplianceDocumentsView> GetFileDataInternalbyId(int ScheduledOnID, int FileId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                && row.FileID == FileId
                                select row).ToList();

                return fileData;
            }
        }

        public static int GetInternalComplianceTypeID(int ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.InternalComplianceInstances
                                      join row1 in entities.InternalComplianceScheduledOns
                                      on row.ID equals row1.InternalComplianceInstanceID
                                      join row3 in entities.InternalCompliances
                                      on row.InternalComplianceID equals row3.ID
                                      where row1.ID == ScheduleOnID
                                      select (int)row3.IComplianceType).Distinct().FirstOrDefault();
                if (ScheduleOnData != null)
                {
                    return ScheduleOnData;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static InternalComplianceInstanceTransactionView GetForMonthInternal(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceInstanceTransactionView ScheduleOnData = (from row in entities.InternalComplianceInstanceTransactionViews
                                                                            where row.InternalScheduledOnID == ScheduledOnID
                                                                            select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {

                    var complianceSchedule = InternalComplianceManagement.GetScheduleByComplianceID((long)ScheduleOnData.InternalComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = InternalComplianceManagement.GetByID((long)ScheduleOnData.InternalComplianceID);

                        string spacialDate = ScheduleOnData.InternalScheduledOn.Day.ToString("00") + ScheduleOnData.InternalScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;

                        if (ForMonthSchedule != null && objCompliance.IFrequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = InternalComplianceManagement.GetForMonth(ScheduleOnData.InternalScheduledOn, ForMonthSchedule.ForMonth, objCompliance.IFrequency);

                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }
        public static int GetComplianceTypeID(int ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.ComplianceInstances
                                      join row1 in entities.ComplianceScheduleOns
                                      on row.ID equals row1.ComplianceInstanceID
                                      join row3 in entities.Compliances
                                      on row.ComplianceId equals row3.ID
                                      where row1.ID == ScheduleOnID
                                      select (int)row3.ComplianceType).Distinct().FirstOrDefault();
                if (ScheduleOnData != null)
                {
                    return ScheduleOnData;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static CheckListInstanceTransactionView GetForMonthChecklist(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.CheckListInstanceTransactionViews
                                      where row.ScheduledOnID == ScheduledOnID
                                      select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                        string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;
                        if (ForMonthSchedule != null && objCompliance.Frequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = ComplianceManagement.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }



        public static InternalComplianceInstanceCheckListTransactionView InternalComplianceCheckListGetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceInstanceCheckListTransactionView ScheduleOnData = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                                                                     where row.InternalScheduledOnID == ScheduledOnID
                                                                                     select row).FirstOrDefault();
                if (ScheduleOnData != null)
                {
                    if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                    {

                        var complianceSchedule = InternalComplianceManagement.GetScheduleByComplianceID((long)ScheduleOnData.InternalComplianceID);

                        if (complianceSchedule.Count > 0)//added by Manisha for One time condition
                        {

                            var objCompliance = ComplianceManagement.GetInternalComplianceByID((long)ScheduleOnData.InternalComplianceID);

                            string spacialDate = ScheduleOnData.InternalScheduledOn.Day.ToString("00") + ScheduleOnData.InternalScheduledOn.Month.ToString("00");
                            var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                            string forMonth = string.Empty;

                            forMonth = InternalComplianceManagement.GetForMonth(ScheduleOnData.InternalScheduledOn, ForMonthSchedule.ForMonth, objCompliance.IFrequency);
                            ScheduleOnData.ForMonth = forMonth;
                        }

                    }
                }
                return ScheduleOnData;
            }
        }

        public static List<SP_GetDocumentCompliancesSummary_DepartmentHead_Result> GetFilteredComplianceDocumentsDepartmenthead(int UserId, int CustomerID, string FlagIsApp, string MonthId, string isStatutoryInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var documentData = (from row in entities.SP_GetDocumentCompliancesSummary_DepartmentHead(UserId, CustomerID, FlagIsApp, MonthId, isStatutoryInternal)
                                    select row).ToList();

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static List<SP_GetDocumentCompliancesSummary_Result> GetFilteredComplianceDocumentsNew(int UserId,int  CustomerID,string FlagIsApp,string MonthId,string isStatutoryInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var documentData = (from row in entities.SP_GetDocumentCompliancesSummary(UserId, CustomerID, FlagIsApp, MonthId, isStatutoryInternal)
                                    select row).ToList();
                               
                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }    
        public static List<SP_GetEventComplianceDocument_Result> GetFilteredEventComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, int EventID, int EventSchedueID, int SubTypeID, string StringType,int LastDataCheck)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetEventComplianceDocument(Customerid)
                                    select row).ToList();

                if (LastDataCheck != null && LastDataCheck > 0)
                {
                    if (LastDataCheck == 2)//Past Month
                    {
                        DateTime FromBindData = DateTime.Now.AddDays(-30);
                        documentData = documentData.Where(x => x.ScheduledOn >= FromBindData).ToList();
                    }
                    else if (LastDataCheck == 1)//Past Year
                    {
                        DateTime FromBindData = DateTime.Now.AddDays(-365);
                        documentData = documentData.Where(x => x.ScheduledOn >= FromBindData).ToList();
                    }
                }
                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    //if (Roleid != 0)
                    //    documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (!string.IsNullOrEmpty(location))
                {
                    if (location != "Entity/Sub-Entity/Location")
                        documentData = documentData.Where(entry => entry.Branch == location).ToList();
                }

                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (EventID != -1)
                    documentData = documentData.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchedueID != -1)
                    documentData = documentData.Where(entry => entry.EventScheduleOnID == EventSchedueID).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(StringType))
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }        
        public static List<SP_GetComplianceDocument_Result> GetFilteredComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk,
            DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, int SubTypeID, string StringType,int LastDataCheck)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetComplianceDocument(Customerid)
                                    select row).ToList();
                              
                if (LastDataCheck != null && LastDataCheck > 0)
                {
                    if (LastDataCheck == 2)//Past Month
                    {
                        DateTime FromBindData = DateTime.Now.AddDays(-30);
                        documentData = documentData.Where(x => x.ScheduledOn >= FromBindData).ToList();
                    }
                    else if (LastDataCheck == 1)//Past Month
                    {
                        DateTime FromBindData = DateTime.Now.AddDays(-365);
                        documentData = documentData.Where(x => x.ScheduledOn >= FromBindData).ToList();
                    }
                }

                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    //if (Roleid != 0)
                    //    documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();
                if (!string.IsNullOrEmpty(location))
                {
                    if (location != "Entity/Sub-Entity/Location")
                        documentData = documentData.Where(entry => entry.Branch == location).ToList();
                }
               
                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(StringType))
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<SP_GetComplianceDocument_Result> GetFilteredComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetComplianceDocument(Customerid)
                                    select row).ToList();

                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();
                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }


        public static List<GetInternalComplianceDocumentsView> GetFileDataInternal(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }
        public static List<SP_GetInternalCheckListComplianceDocument_Result> GetFilteredInternalCheckListComplianceDocuments(int Customerid, int userID, int Risk, DocumentFilterNewStatus status, String location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //long RoleID = (from row in entities.Users
                //               where row.ID == userID
                //               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetInternalCheckListComplianceDocument(Customerid)
                                    select row).ToList();


                //if (RoleID != 8)
                //{
                //    var GetApprover = (from row in entities.InternalComplianceAssignments
                //                       where row.UserID == userID
                //                       select row)
                //                       .GroupBy(a => a.RoleID)
                //                       .Select(a => a.FirstOrDefault())
                //                       .Select(entry => entry.RoleID).ToList();

                //    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //        Roleid = 6;

                //    if (!Role.Equals("CADMN"))
                //    {
                //        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                //    }

                //    if (Roleid != 0)
                //        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                //}
                //else
                //{
                documentData = (from row in documentData
                                join row1 in entities.EntitiesAssignmentInternals
                                on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                && row1.UserID == userID
                                select row).Distinct().ToList();
                //}

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();

                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;
                }

                //if (ComType != -1)
                //    documentData = documentData.Where(entry => entry.IComplianceTypeID == ComType).ToList();

                //if (ComCategory != -1)
                //    documentData = documentData.Where(entry => entry.IComplianceCategoryID == ComCategory).ToList();

                //if (StartDate != null && EndDate != null)
                //    documentData = documentData.Where(entry => (entry.InternalScheduledOn >= StartDate && entry.InternalScheduledOn <= EndDate)).ToList();

                //// Find data through String contained in Description
                //if (StringType != "")
                //{
                //    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}
                return documentData.GroupBy(entry => entry.InternalComplianceScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<SP_GetEventComplianceDocument_Result> GetFilteredEventComplianceDocuments(int Customerid, int userID, int Risk, DocumentFilterNewStatus status, String location, int EventID, int EventSchedueID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //long RoleID = (from row in entities.Users
                //               where row.ID == userID
                //               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetEventComplianceDocument(Customerid)
                                    select row).ToList();

                //if (RoleID != 8)
                //{
                //    var GetApprover = (from row in entities.ComplianceAssignments
                //                       where row.UserID == userID
                //                       select row)
                //                       .GroupBy(a => a.RoleID)
                //                       .Select(a => a.FirstOrDefault())
                //                       .Select(entry => entry.RoleID).ToList();

                //    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //        Roleid = 6;

                //    if (!Role.Equals("CADMN"))
                //    {
                //        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                //    }

                //    if (Roleid != 0)
                //        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                //}
                //else
                //{
                documentData = (from row in documentData
                                join row1 in entities.EntitiesAssignments
                                on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                && row1.UserID == userID
                                select row).Distinct().ToList();
                //}

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //if (ComType != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                //if (ComCategory != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                //if (Act != -1)
                //    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                //if (StartDate != null && EndDate != null)
                //    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (EventID != -1)
                    documentData = documentData.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchedueID != -1)
                    documentData = documentData.Where(entry => entry.EventScheduleOnID == EventSchedueID).ToList();

                //if (SubTypeID != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                //// Find data through String contained in Description
                //if (StringType != "")
                //{
                //    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<SP_GetInternalComplianceDocument_Result> GetFilteredInternalComplianceDocuments(int Customerid, int userID, int Risk, DocumentFilterNewStatus status, String location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetInternalComplianceDocument(Customerid)
                                    select row).ToList();


                //if (RoleID != 8)
                //{
                //    var GetApprover = (from row in entities.InternalComplianceAssignments
                //                       where row.UserID == userID
                //                       select row)
                //                       .GroupBy(a => a.RoleID)
                //                       .Select(a => a.FirstOrDefault())
                //                       .Select(entry => entry.RoleID).ToList();

                //    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //        Roleid = 6;

                //    if (!Role.Equals("CADMN"))
                //    {
                //        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                //    }

                //    if (Roleid != 0)
                //        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                //}
                //else
                //{
                documentData = (from row in documentData
                                join row1 in entities.EntitiesAssignmentInternals
                                on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                && row1.UserID == userID
                                select row).Distinct().ToList();
                //}


                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();

                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;
                }

                //if (ComType != -1)
                //    documentData = documentData.Where(entry => entry.IComplianceTypeID == ComType).ToList();

                //if (ComCategory != -1)
                //    documentData = documentData.Where(entry => entry.IComplianceCategoryID == ComCategory).ToList();

                //if (StartDate != null && EndDate != null)
                //    documentData = documentData.Where(entry => (entry.InternalScheduledOn >= StartDate && entry.InternalScheduledOn <= EndDate)).ToList();

                //// Find data through String contained in Description
                //if (StringType != "")
                //{
                //    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}


                return documentData.GroupBy(entry => entry.InternalComplianceScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<SP_GetCheckListDocument_Result> GetFilteredCheckListDocuments(int Customerid, int userID, int Risk, DocumentFilterNewStatus status, String location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //long RoleID = (from row in entities.Users
                //               where row.ID == userID
                //               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetCheckListDocument(Customerid)
                                    select row).ToList();

                //if (RoleID != 8)
                //{
                //    var GetApprover = (from row in entities.ComplianceAssignments
                //                       where row.UserID == userID
                //                       select row)
                //                       .GroupBy(a => a.RoleID)
                //                       .Select(a => a.FirstOrDefault())
                //                       .Select(entry => entry.RoleID).ToList();

                //    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //        Roleid = 6;

                //    if (!Role.Equals("CADMN"))
                //    {
                //        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                //    }

                //    if (Roleid != 0)
                //        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                //}
                //else
                //{
                documentData = (from row in documentData
                                join row1 in entities.EntitiesAssignments
                                on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                && row1.UserID == userID
                                select row).Distinct().ToList();
                //}

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //if (ComType != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                //if (ComCategory != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                //if (Act != -1)
                //    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                //if (StartDate != null && EndDate != null)
                //    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                ////if (SubTypeID != -1)
                ////    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                //// Find data through String contained in Description
                //if (StringType != "")
                //{
                //    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<SP_GetComplianceDocument_Result> GetFilteredComplianceDocuments(int Customerid, int userID, int Risk, DocumentFilterNewStatus status, String location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetComplianceDocument(Customerid)
                                    select row).ToList();

                //if (RoleID != 8)
                //{
                //    var GetApprover = (from row in entities.ComplianceAssignments
                //                       where row.UserID == userID
                //                       select row)
                //                       .GroupBy(a => a.RoleID)
                //                       .Select(a => a.FirstOrDefault())
                //                       .Select(entry => entry.RoleID).ToList();

                //    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                //        Roleid = 6;

                //    if (!Role.Equals("CADMN"))
                //    {
                //        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                //    }

                //    if (Roleid != 0)
                //        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                //}
                //else
                //{
                documentData = (from row in documentData
                                join row1 in entities.EntitiesAssignments
                                on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                && row1.UserID == userID
                                select row).Distinct().ToList();
                //}

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //if (ComType != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                //if (ComCategory != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                //if (Act != -1)
                //    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                //if (StartDate != null && EndDate != null)
                //    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                //if (SubTypeID != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                //// Find data through String contained in Description
                //if (StringType != "")
                //{
                //    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                //}


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static List<GetComplianceDocumentsView> GetFileData1(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }
        public static ComplianceInstanceTransactionView GetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceInstanceTransactionView ScheduleOnData = (from row in entities.ComplianceInstanceTransactionViews
                                                                    where row.ScheduledOnID == ScheduledOnID
                                                                    select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                        string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;
                        if (ForMonthSchedule != null && objCompliance.Frequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = ComplianceManagement.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }
        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int) fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.Encrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }

        public static List<SP_GetDocumentCompliancesSummary_Result> GetComplianceMyDocumentsNew(int UserId, int CustomerID, string FlagIsApp, string MonthId, string isStatutoryInternal, int risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var documentData = (from row in entities.SP_GetDocumentCompliancesSummary(UserId, CustomerID, FlagIsApp, MonthId, isStatutoryInternal)
                                    select row).ToList();

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

    }
}