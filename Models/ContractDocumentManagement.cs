﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
   
    public class ContractDocumentManagement
    {
        public static string GetContractTypeName(List<Cont_tbl_TypeMaster> Type, long TypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string Name = string.Empty;

                Name = (from row in Type
                        where row.IsDeleted == false
                        && row.ID == TypeID
                        select row.TypeName).FirstOrDefault();

                return Name;
            }
        }
        public static string GetDeptName(List<Department> Department, long DeptID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string Name = string.Empty;

                Name = (from row in Department
                        where row.IsDeleted == false
                        && row.ID == DeptID
                        select row.Name).FirstOrDefault();

                return Name;
            }
        }
        public static string GetVendorName(List<Cont_tbl_VendorMapping> VendorMapping, List<Cont_tbl_VendorMaster> VendorMaster, long CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string Name = string.Empty;

                Name = (from row in VendorMapping
                        join row1 in VendorMaster
                         on row.VendorID equals row1.ID
                        where row1.IsDeleted == false
                        && row.ContractID == CID && row.IsActive == true
                        select row1.VendorName).FirstOrDefault();

                return Name;
            }
        }

        public static List<Department> GetAllDepartmentMasterList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentMasterList = (from row in entities.Departments
                                            where row.IsDeleted == false
                                            //&& row.CustomerID == customerID
                                            select row);

                return DepartmentMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Cont_tbl_VendorMapping> GetVendorsMapping_All(int ContID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Cont_tbl_VendorMapping
                             where row.IsActive == true
                              && row.ContractID == ContID
                             select row).ToList();
                return Query.ToList();
            }
        }
        public static List<Cont_tbl_VendorMaster> GetVendors_All(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Cont_tbl_VendorMaster
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).OrderBy(entry => entry.VendorName);
                return Query.ToList();
            }
        }
        public static List<Cont_tbl_FileData> GetTaskDocumentsByDocTypeID(List<Cont_tbl_FileData> ContractsDetail, long TaskID)
        {
            var AllDocument = (from row in ContractsDetail
                               where row.TaskID == TaskID
                               && row.IsDeleted == false
                               select row).ToList();

            return AllDocument;
        }
        public static string getPriorityName(long ID)
        {
            string PriorityName = string.Empty;
            if (ID == 1)
            {
                PriorityName = "High";
            }
            else if (ID == 2)
            {
                PriorityName = "Medium";
            }
            else if (ID == 3)
            {
                PriorityName = "Low";
            }
            return PriorityName;
        }
        public static List<Cont_tbl_FileData> GetContractDocumentsByDocTypeID(long contractID, long docTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.Cont_tbl_FileData
                                   where row.ContractID == contractID
                                   && row.IsDeleted == false
                                   select row).ToList();

                if (docTypeID != 0)
                {
                    AllDocument = AllDocument.Where(row => row.DocTypeID == docTypeID).ToList();
                }

                return AllDocument;
            }
        }

        public static List<Cont_SP_MyDocuments_All_Result> GetAssigned_MyDocuments(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID, List<string> lstSelectedFileTags)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_MyDocuments_All(customerID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    if (lstSelectedFileTags.Count > 0)
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID,
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.DepartmentID,
                                 g.ContractTypeID,
                                 g.ContractSubTypeID,
                                 g.EffectiveDate,
                                 g.ExpirationDate,
                                 g.StatusName
                             } into GCS
                             select new Cont_SP_MyDocuments_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 StatusName = GCS.Key.StatusName
                             }).ToList();
                }

                return query.ToList();
            }
        }

    }
}