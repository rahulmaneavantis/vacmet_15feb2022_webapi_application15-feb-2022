﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class MyComplianceDashboradResult
    {
        public List<sp_ComplianceInstanceAssignmentViewDetails_Result> Statustory { get; set; }
        public List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> Internal { get; set; }
    }
    public class MGMTPenaltyDetails
    {
        public List<SP_GetPenaltyQuarterWise_Result> penalty { get; set; }
        public List<SP_GetPenaltyQuarterWiseApprover_Result> penaltyapprover { get; set; }
        public List<SP_GetPenaltyQuarterWise_DeptHead_Result> Deptpenalty { get; set; }
    }
    //public class MyWorkSpaceMgmtResult
    //{
    //    public List<sp_ComplianceMgmtTransactionNEW_Result> Statutory { get; set; }
    //    public List<sp_ComplianceMgmtTransactionNEW_Result> EventBased { get; set; }
    //}
    public class MyWorkSpaceMgmtResult
    {
        public List<sp_StatutoryMGMTData_Result> Statutory { get; set; }
        public List<sp_StatutoryMGMTData_Result> EventBased { get; set; }
    }
    public class MGMTDashboradResult
    {
        public List<sp_ComplianceInstanceAssignmentViewDetails_Result> Statustory { get; set; }
        public List<sp_InternalComplianceInstanceAssignmentViewDetails_Result> Internal { get; set; }

        public List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> DeptStatustory { get; set; }
        public List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> DeptInternal { get; set; }
    }
}