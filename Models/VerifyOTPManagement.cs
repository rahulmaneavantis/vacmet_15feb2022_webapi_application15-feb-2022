﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using AppWebApplication.Data;
using AppWebApplication.DataRisk;

namespace AppWebApplication.Models
{
    public class VerifyOTPManagement
    {
       
        public static bool ChangePassword(mst_User user, string SenderEmailAddress, string message)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.EnType = "A";
                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.ChangePasswordFlag = false;
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }


        public static bool ChangePassword(User user, string SenderEmailAddress, string message)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        User userToUpdate = new User();
                        userToUpdate = (from entry in entities.Users
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.EnType = "A";
                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.ChangePasswordFlag = false;
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }


        public static void Create(VerifyOTP OTP)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                entities.VerifyOTPs.Add(OTP);
                entities.SaveChanges();
            }
        }
        public static bool Exists(long OTP, int Uid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var data = (from row in entities.VerifyOTPs
                            where row.OTP == OTP && row.UserId == Uid
                            orderby row.VId descending
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
              
            }
        }


        public static VerifyOTP GetByID(int UserId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                            where row.UserId == UserId
                            orderby row.VId descending
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static void UpdateVerifiedFlag(int UserId, long OTP)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.VerifyOTPs
                            where row.UserId == UserId
                            && row.OTP == OTP
                            select row).FirstOrDefault();

                if (data != null)
                {
                    data.IsVerified = true;
                    entities.SaveChanges();
                }
            }
        }
    }
}