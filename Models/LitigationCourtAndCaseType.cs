﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AppWebApplication.Models
{
    public class LitigationCourtAndCaseType
    {
        public static tbl_CaseType GetRPALegalCaseTypeDetailByID(int ID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.tbl_CaseType
                               where row.RPACaseTypeID == ID
                               && row.CustomerID == CustomerID
                               select row).SingleOrDefault();
                return Objcase;
            }
        }
        public static bool DeleteLegalCaseTypeDetail(int ID, long CustomerID)
        {
            bool output = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_CaseType objCase = (from row in entities.tbl_CaseType
                                            where row.ID == ID && row.CustomerID == CustomerID
                                            && row.IsDeleted == false
                                            select row).FirstOrDefault();

                    objCase.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                output = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return output;
        }

        public static bool DeleteCourtMasterData(int ID, long CustomerID)
        {
            bool output = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_CourtMaster objCase = (from row in entities.tbl_CourtMaster
                                               where row.ID == ID
                                               && row.CustomerID == CustomerID
                                               && row.IsDeleted == false
                                               select row).FirstOrDefault();

                    objCase.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                output = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return output;
        }

        public static List<View_CourtMaster> BindAllCourtMasterData(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCourt = (from row in entities.View_CourtMaster
                                where row.CourtName != null
                                && row.CustomerID == CustomerID
                                select row).OrderBy(entry => entry.CourtName);
                return objCourt.ToList();
            }
        }
        public static List<tbl_CaseType> GetAllLegalCaseTypeData(long CutomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objcases = (from row in entities.tbl_CaseType
                                where row.CaseType != null && row.IsDeleted == false
                                && row.CustomerID == CutomerID
                                select row).OrderBy(entry => entry.CaseType);
                return objcases.ToList();
            }
        }
    }
}