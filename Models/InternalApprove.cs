﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class InternalApprove
    {
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("ScheduledOnID")]
        public string ScheduledOnID { get; set; }

        [JsonProperty("ComplianceInstanceID")]
        public string ComplianceInstanceID { get; set; }

        [JsonProperty("Remark")]
        public string Remark { get; set; }

        [JsonProperty("StatusChangedOnDate")]
        public string StatusChangedOnDate { get; set; }

        [JsonProperty("StatusId")]
        public string StatusId { get; set; }


        [JsonProperty("conditionAR")]
        public string conditionAR { get; set; }


    }
}