﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class SearchDailyUpdateTLObject
    {
        public List<DailyUpdateSearch.Hit> HRData { get; set; }
    }
    public class MainRootObject
    {        
        public search.RootObject rootobject1 { get; set; }
        public SearchCompliance.RootObject rootobject2 { get; set; }
        public DailyUpdateSearch.RootObject rootobject3 { get; set; }
        public NewsLetterSearch.RootObject rootobject4 { get; set; }
    }

    public class SearchObject
    {
        public List<search.Hit> actlist { get; set; }
        public List<SearchCompliance.Hit> Compliacelist { get; set; }
        public List<DailyUpdateSearch.Hit> DailyUpdateActlist { get; set; }
        public List<NewsLetterSearch.Hit> NewsLetterlist { get; set; }
    }

    public class SearchDailyUpdateObject
    {
        public List<DailyUpdateSearch.Hit> Alllist { get; set; }
        public List<DailyUpdateSearch.Hit> Todaylist { get; set; }
        public List<DailyUpdateSearch.Hit> Weeklylist { get; set; }
        public List<DailyUpdateSearch.Hit> Monthlylist { get; set; }
    }
}