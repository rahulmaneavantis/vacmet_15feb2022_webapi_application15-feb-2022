﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class LegalUpdateRegister
    {
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }     
        public string ContactNumber { get; set; }
        public string GSTNumber { get; set; }
        public string CRegNumber { get; set; }
        public string UserPAN { get; set; }
        public int Flag { get; set; }
    }
}