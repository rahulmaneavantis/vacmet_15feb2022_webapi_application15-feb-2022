﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class OverdueCompliances
    {
        public List<SP_GetOverDueHighInternalCompliance_Result> IList { get; set; }
        public List<SP_GetOverDueHighCompliance_Result> SList { get; set; }
    }

    public class MGMTOverdueCompliances
    {
        public List<SP_GetOverDueHighCompliance_Result> SList { get; set; }
        public List<SP_GetOverDueHighCompliance_DeptHead_Result> DeptSList { get; set; }
        public List<SP_GetOverDueHighInternalCompliance_Result> IList { get; set; }
        public List<SP_GetOverDueHighInternalCompliance_DeptHead_Result> DeptIList { get; set; }
    }
    public class StatutoryMGMTDashboards
    {
        public List<SP_GetCannedReportCompliancesSummary_Result> FunctionPieChart { get; set; }
        public List<SP_GetCannedReportCompliancesSummary_Result> FunctionBarChart { get; set; }
        public List<SP_GetCannedReportCompliancesSummary_Result> RiskBarChart { get; set; }
        public List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> DeptFunctionPieChart { get; set; }
        public List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> DeptFunctionBarChart { get; set; }
        public List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> DeptRiskBarChart { get; set; }
    }
    public class InternalMGMTDashboards
    {
        public List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionPieChart { get; set; }
        public List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionBarChart { get; set; }
        public List<SP_GetCannedReportInternalCompliancesSummary_Result> RiskBarChart { get; set; }
        public List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> DeptFunctionPieChart { get; set; }
        public List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> DeptFunctionBarChart { get; set; }
        public List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> DeptRiskBarChart { get; set; }
    }
}