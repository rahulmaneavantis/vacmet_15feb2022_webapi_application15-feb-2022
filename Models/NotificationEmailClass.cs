﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;


namespace AppWebApplication.Models
{
    public class NotificationEmailClass
    {
        public static string SendNotificationEmail(User user, string passwordText)
        {
            try//NorificationemailClass.cs
            {
                if (user != null)
                {
                    int customerID = -1;
                    string ReplyEmailAddressName = "";
                    if (user.RoleID == 1 || user.RoleID == 12)
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else
                    {
                        customerID = Convert.ToInt32(user.CustomerID);
                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                    }
                    string username = string.Format("{0} {1}", user.FirstName, user.LastName);

                    string message = Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                                         .Replace("@Username", user.Email)
                                         .Replace("@User", username)
                                         .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                         .Replace("@Password", passwordText)
                                         .Replace("@From", ReplyEmailAddressName)
                                         .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    return message;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;
        }
    }
}