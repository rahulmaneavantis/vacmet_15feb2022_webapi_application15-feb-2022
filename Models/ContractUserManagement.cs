﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class ContractUserManagement
    {
        public static string GetUserNameByUserID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = (from row in entities.Users
                                   where row.ID == userID
                                   select row.FirstName + " " + row.LastName).FirstOrDefault();

                return UserDetails;
            }
        }
    }
}