﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using System.Data;
using AppWebApplication.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DepartmentHeadManagement
    {

        public static List<sp_ComplianceAssignedDepartment_Result> GetFunctionDetailsStatutory_DeptHead(int UserID, int CustomerBranchId, List<long> Branchlist, string isstatutoryinternal, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedDepartment_Result> complianceCategorys = new List<sp_ComplianceAssignedDepartment_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
     
        public static List<USP_InternalCompliancesCategory_Result> GetFunctionDetailsInternal1(int Userid, int CustomerID, List<long> branchlist, int CustomerBranchId, string IsDeptHead, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<USP_InternalCompliancesCategory_Result> complianceCategorys = new List<USP_InternalCompliancesCategory_Result>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.USP_InternalCompliancesCategory(CustomerID)
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& row1.CustomerBranchID == CustomerBranchId
                                               && branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.USP_InternalCompliancesCategory(CustomerID)
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    //List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();

                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.USP_InternalCompliancesCategory(CustomerID)
                                               join row1 in entities.EntitiesAssignmentInternals
                                               on (long)row.ID equals row1.ComplianceCatagoryID
                                               where branchlist.Contains((long)row.CustomerBranchID) //&& row.CustomerBranchID == row1.BranchID
                                            && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                               select row).Distinct().ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.USP_InternalCompliancesCategory(CustomerID)
                                               join row1 in entities.EntitiesAssignmentInternals
                                               on (long)row.ID equals row1.ComplianceCatagoryID
                                               where row.CustomerBranchID == row1.BranchID
                                               // && branchlist.Contains((long)row.CustomerBranchID)
                                               && row1.UserID == Userid
                                               select row).Distinct().ToList();
                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int?)a.ID).Select(a => a.FirstOrDefault()).ToList();
                    //complianceCategorys = (from row in InternalComplianceCount
                    //                       join row1 in entities.USP_InternalCompliancesCategory(CustomerID)
                    //                       on row.InternalComplianceCategoryID equals row1.ID
                    //                       select row1).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, List<long> branchlist, int CustomerBranchId, string IsDeptHead, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& row1.CustomerBranchID == CustomerBranchId
                                               && branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();

                    if (branchlist.Count > 0)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where branchlist.Contains((long)row.CustomerBranchID) //&& row.CustomerBranchID == row1.BranchID
                                                && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   // && branchlist.Contains((long)row.CustomerBranchID)
                                                   && row1.UserID == Userid
                                                   select row).Distinct().ToList();
                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           where row1.IsDeleted == false
                                           select row1).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
                
    }
}
