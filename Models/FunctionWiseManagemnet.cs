﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class FunctionWiseManagemnet
    {
        public static List<sp_ComplianceAssignedCategory_Result> GetFunctionDetailsStatutoryNew(int UserID, List<long> branchlist, int CustomerBranchId, bool approver)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    //List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                    if (branchlist != null && branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }

                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<sp_InternalComplianceAssignedCategory_Result> GetFunctionDetailsInternalnew(int UserID, List<long> branchlist, int CustomerBranchId, bool approver)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceAssignedCategory_Result> complianceCategorys = new List<sp_InternalComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_InternalComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }

                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<sp_ComplianceAssignedCategory_Result> GetFunctionDetailsStatutory(int UserID, List<long> branchlist, int CustomerBranchId, bool approver)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }

                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }    
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, List<long> branchlist, int CustomerBranchId, bool approver)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& row1.CustomerBranchID == CustomerBranchId
                                               && branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               && row.IsDeleted == false
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
                    if (branchlist.Count > 0)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where branchlist.Contains((long)row.CustomerBranchID) //&& row.CustomerBranchID == row1.BranchID
                                                && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   // && branchlist.Contains((long)row.CustomerBranchID)
                                                   && row1.UserID == Userid
                                                   select row).Distinct().ToList();
                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           where row1.IsDeleted == false
                                           select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }


        #region  add by rahul on 24 JUNE 2019
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, int barnchIda, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, string isstaorinternal, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool customizedChecklist;
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (customizedChecklist)
                    {
                        if (isstaorinternal == "StatutoryAll")
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    }
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername,string isstaorinternal, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool customizedChecklist;
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (customizedChecklist)
                    {
                        if (isstaorinternal == "StatutoryAll")
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    }
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //if (statusIDs.Count > 0)               
                //    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                
                //if (CategoryID.Count > 0)
                //    detailView = detailView.Where(entry => CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                                

                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, string isstaorinternal, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool customizedChecklist;
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (customizedChecklist)
                    {
                        if (isstaorinternal == "StatutoryAll")
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    }
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, string isstaorinternal, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool customizedChecklist;
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (customizedChecklist)
                    {
                        if (isstaorinternal == "StatutoryAll")
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                        }
                        else
                        {
                            detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                        }
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    }
                }
                if (Branchlist != null)
                {
                    if (Branchlist.Count > 0)
                    {
                        detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion

        #region Statutory Department Head added by rahul on 24 JUNE 2019
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionBARGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
      
                var role = (from row in entities.Users
                            where row.ID== userId
                           select row.RoleID).FirstOrDefault();                          

                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (role==8)
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                    }                    
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionBARGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var role = (from row in entities.Users
                            where row.ID == userId
                            select row.RoleID).FirstOrDefault();
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (role == 8)
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                    }
                        
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionPIEGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionPIEGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                
                //if (statusIDs.Count > 0)
                //    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();

                //if (CategoryID.Count > 0)
                //    detailView = detailView.Where(entry => CategoryID.Contains(entry.DepartmentID)).ToList();
                

                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion

        #region Internal added by rahul on 25 JUNE 2019         
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionBARInternalGetManagementDetailView(int customerid, List<long> branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //bool customizedChecklist;
                //customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                entities.Database.CommandTimeout = 180;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    //if (customizedChecklist)
                    //{
                    //    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMTALL")).ToList();                        
                    //}
                    //else
                    //{
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    //}
                }


                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    // }
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                if (branchlist != null)
                {
                    if (branchlist.Count > 0)
                    {
                        detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                }

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionBARInternalGetManagementDetailView(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //bool customizedChecklist;
                //customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                entities.Database.CommandTimeout = 180;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    //if (customizedChecklist)
                    //{
                    //    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                    //}
                    //else
                    //{
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    //}
                }

                if (branchlist != null)
                {
                    if (branchlist.Count > 0)
                    {
                        detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                }                
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    // }
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }

                return detailView;
            }
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionPIEInternalGetManagementDetailView(int customerid, List<long> branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //bool customizedChecklist;
                //customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                entities.Database.CommandTimeout = 180;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    //if (customizedChecklist)
                    //{
                    //    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                    //}
                    //else
                    //{
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    //}
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                   // }
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionPIEInternalGetManagementDetailView(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
         
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //bool customizedChecklist;
                //customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(customerid), "MGMT_Checklist");
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                entities.Database.CommandTimeout = 180;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    //if (customizedChecklist)
                    //{
                    //    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMTALL")).ToList();
                    //}
                    //else
                    //{
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                    //}
                }
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion

        #region Internal Department Head added by rahul on 25 JUNE 2019        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionBARInternalGetManagementDetailView_DeptHead(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;

                var role = (from row in entities.Users
                            where row.ID == userId
                            select row.RoleID).FirstOrDefault();

                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (role == 8)
                    {
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                    }
                }
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionBARInternalGetManagementDetailView_DeptHead(int customerid, List<long> branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;

                var role = (from row in entities.Users
                            where row.ID == userId
                            select row.RoleID).FirstOrDefault();

                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    if (role == 8)
                    {
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                    }
                    else
                    {
                        detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                    }
                }
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
                //detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                }

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID).ToList();
                    }
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionPIEInternalGetManagementDetailView_DeptHead(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;

                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
               // detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionPIEInternalGetManagementDetailView_DeptHead(int customerid, List<long> branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;

                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    //}
                }
                else
                {
                    //if (pointername == "Not completed")
                    //{
                    detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    //}
                }
               // detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                
                if (branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion
    }
}