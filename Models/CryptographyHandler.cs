﻿using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using AppWebApplication.Data;
using System;

namespace AppWebApplication.Models
{
    public class CryptographyHandler
    {
        // salt value - 16 characters long
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("wcdwbluudgr88ukb");

        private static readonly string passPhrase = "sepzq7f3";

        // keysize of the encryption algorithm.
        private const int keysize = 256;

        public static byte[] Encrypt(byte[] plainText)
        {
            //byte[] plainTextBytes = new byte[plainText.Length];
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            using (RijndaelManaged symmetricKey = new RijndaelManaged())
            {
                symmetricKey.Mode = CipherMode.CBC;
                using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(password.GetBytes(keysize / 8), initVectorBytes))
                using (MemoryStream memoryStream = new MemoryStream())
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainText, 0, plainText.Length);
                    cryptoStream.FlushFinalBlock();
                    byte[] cipherTextBytes = memoryStream.ToArray();
                    return cipherTextBytes;
                }
            }
        }

        public static byte[] Decrypt(byte[] cipherText)
        {
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            using (RijndaelManaged symmetricKey = new RijndaelManaged())
            {
                symmetricKey.Mode = CipherMode.CBC;
                using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(password.GetBytes(keysize / 8), initVectorBytes))
                using (MemoryStream memoryStream = new MemoryStream(cipherText))
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    byte[] plainTextBytes = new byte[cipherText.Length];
                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                    // byte[] data = new byte[decryptedByteCount];
                    return plainTextBytes.Take(decryptedByteCount).ToArray();
                }
            }
        }

        public static byte[] keyArray = { };
        public static byte[] IV = {
             0x12,
             0x34,
             0x56,
             0x78,
             0x90,
             0xab,
             0xcd,
             0xef};

        public static string encrypt(string stringToEncrypt, string SEncryptionKey)
        {
            try
            {
                keyArray = System.Text.Encoding.UTF8.GetBytes((SEncryptionKey.Substring(0, 8)));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(keyArray, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string decrypt(string stringtodecrypt, string sencryptionkey)
        {
            byte[] inputbytearray = new byte[stringtodecrypt.Length + 1];
            try
            {
                keyArray = System.Text.Encoding.UTF8.GetBytes((sencryptionkey.Substring(0, 8)));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputbytearray = Convert.FromBase64String(stringtodecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(keyArray, IV), CryptoStreamMode.Write);
                cs.Write(inputbytearray, 0, inputbytearray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return "Error";
            }
        }

        #region AES
        private static string secretIV = "3BD28F6C-BAF7-4E";
        private static string secretKey = "3BD28F6C-BAF7-4E97-91A9-0577F728";
        private static int AESkeysize = 256;
        private static string GenerateKey(int iKeySize)
        {
            RijndaelManaged aesEncryption = new RijndaelManaged();
            aesEncryption.KeySize = iKeySize;
            aesEncryption.BlockSize = 128;
            aesEncryption.Mode = CipherMode.CBC;
            aesEncryption.Padding = PaddingMode.PKCS7;
            aesEncryption.GenerateIV();
            string ivStr = Convert.ToBase64String(aesEncryption.IV);
            aesEncryption.GenerateKey();
            string keyStr = Convert.ToBase64String(aesEncryption.Key);
            string completeKey = ivStr + "," + keyStr;
            return Convert.ToBase64String(ASCIIEncoding.UTF8.GetBytes(completeKey));
        }
        public static string EncryptPass(string plainStr)
        {
            RijndaelManaged aesEncryption = GetRijndaelManaged();

            byte[] plainText = ASCIIEncoding.UTF8.GetBytes(plainStr);
            ICryptoTransform crypto = aesEncryption.CreateEncryptor();

            byte[] cipherText = crypto.TransformFinalBlock(plainText, 0, plainText.Length);
            return Convert.ToBase64String(cipherText);
        }
        public static RijndaelManaged GetRijndaelManaged()
        {
            var secretIVBytes = Encoding.UTF8.GetBytes(secretIV);
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            RijndaelManaged aesEncryption = new RijndaelManaged();
            aesEncryption.KeySize = AESkeysize;
            aesEncryption.BlockSize = 128;
            aesEncryption.Mode = CipherMode.CBC;
            aesEncryption.Padding = PaddingMode.PKCS7;
            aesEncryption.IV = secretIVBytes;
            aesEncryption.Key = secretKeyBytes;
            return aesEncryption;
        }
        public static byte[] AESEncrypt(byte[] plainBytes)
        {
            RijndaelManaged rijndaelManaged = GetRijndaelManaged();
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateEncryptor()
             .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            }
            catch
            {
            }
            return decode;

        }
        public static byte[] AESDecrypt(byte[] encryptedData)
        {
            RijndaelManaged rijndaelManaged = GetRijndaelManaged();
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch
            {
            }
            return decode;
        }
        #endregion
    }
}