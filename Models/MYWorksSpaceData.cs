﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{

    public class MyWorkSpaceResult
    {
        public List<sp_ComplianceInstanceTransactionNEW_Result> Statutory { get; set; }
        public List<sp_InternalComplianceInstanceTransactionNEW_Result> Internal { get; set; }
        public List<sp_ComplianceInstanceTransactionNEW_Result> EventBased { get; set; }
        public List<sp_ComplianceInstanceTransactionNEW_Result> Statutorychecklist { get; set; }
        public List<sp_ComplianceInstanceTransactionNEW_Result> Statutorylicense { get; set; }
        public List<sp_InternalComplianceInstanceTransactionNEW_Result> Internallicense { get; set; }
    }
    public class MYWorksSpaceData
    {
        public List<ddlStatus> ddlStatus { get; set; }

        public List<ComplianceInstanceTransactionView> bindDataStatutory { get; set; }
        public List<InternalComplianceInstanceTransactionView> bindDataInternal { get; set; }

        public List<NameValueHierarchy> locationList { get; set; }

        public List<AssignLocationList> assignLocationList { get; set; }

        public List<SP_GetActiveEventDropDown_Result> GetActiveEvent { get; set; }
        
    }
}