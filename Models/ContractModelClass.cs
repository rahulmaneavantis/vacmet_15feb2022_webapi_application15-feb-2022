﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppWebApplication.Data;

namespace AppWebApplication.Models
{
    public class ContractModelClass
    {
    }
    public class myVendorContractclass
    {
        public string Email { get; set; }

    }
    public class VendorListContract
    {
        public string VendorName { get; set; }
        public long VendorID { get; set; }
    }
    public class DepartmentListContract
    {
        public string DepartmentName { get; set; }
        public int DepartmentID { get; set; }
    }
    public class mydeptContractclass
    {
        public string Email { get; set; }

    }
    public class GetContractDetailsOutput
    {
        public long ID { get; set; }
        public int CustomerID { get; set; }
        public bool IsDeleted { get; set; }
        public int ContractType { get; set; }
        public string ContractNo { get; set; }
        public string ContractTitle { get; set; }
        public string ContractDetailDesc { get; set; }
        public int CustomerBranchID { get; set; }
        public int DepartmentID { get; set; }
        public long ContractTypeID { get; set; }
        public Nullable<long> ContractSubTypeID { get; set; }
        public Nullable<System.DateTime> ProposalDate { get; set; }
        public Nullable<System.DateTime> AgreementDate { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> ReviewDate { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public Nullable<int> NoticeTermNumber { get; set; }
        public Nullable<int> NoticeTermType { get; set; }
        public Nullable<int> PaymentTermID { get; set; }
        public Nullable<decimal> ContractAmt { get; set; }
        public string ProductItems { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<int> ContactPersonOfDepartment { get; set; }
        public Nullable<int> PaymentType { get; set; }
        public string AddNewClause { get; set; }
        //public string Product_Specification { get; set; }
        //public Nullable<decimal> Rate_per_product { get; set; }
        //public Nullable<decimal> GST { get; set; }
        //public Nullable<int> PaymentTerm { get; set; }
        //public string Delivery_Installation_Period { get; set; }
        //public string Penalty { get; set; }
        //public string Product_Warranty { get; set; }
        public long ContractStatusID { get; set; }
        public string StatusName { get; set; }
        public System.DateTime StatusChangeOn { get; set; }
        public string VendorName { get; set; }
        public string DeptName { get; set; }

        public string OwnerName { get; set; }
        public string ContractTypeName { get; set; }
        public string BrancName { get; set; }

    }
    public class ContractTaskDetailOutput
    {
        public List<Cont_SP_GetTaskDetails_UserRoleWise_Result> TaskDetail { get; set; }

        public List<Cont_SP_GetContractDetails_Result> ContractsDetail { get; set; }
        public string TaskAssignName { get; set; }

    }

    public class ContractReviewsDetailclass
    {
        public string Email { get; set; }
        public int RoleID { get; set; }
    }
    public class ContractAuditDetailclass
    {
        public string Email { get; set; }
        public int ContractID { get; set; }
    }

    public class ContractUserRole
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    public class ContractMilestoneDetailclass
    {
        public string Email { get; set; }

    }
    public class ContractMilestoneDetailclass1
    {
        public string Email { get; set; }
        public int MilestoneID { get; set; }
        public int ContractID { get; set; }
        public int StatusID { get; set; }
    }
    public class ContractTaskDetailclass
    {
        public string Email { get; set; }

        public int contractID { get; set; }

        public int TaskID { get; set; }

        public int RoleID { get; set; }

    }
    public class getContractDetailclass
    {
        public string Email { get; set; }
        public int FileID { get; set; }
        public int ContractID { get; set; }

    }
    public class ContractDetailclass
    {
        public string Email { get; set; }

        public int contractID { get; set; }

    }
    public class getmyTaskDocumentPathContractclass
    {
        public string Email { get; set; }

        public int ContractID { get; set; }

        public int FileID { get; set; }

        public int TaskID { get; set; }

        public int PageID { get; set; }

    }
    public class getTaskInstanceDetail
    {

        public long ID { get; set; }
        public Nullable<long> ParentTaskID { get; set; }
        public int CustomerID { get; set; }
        public long ContractID { get; set; }
        public bool IsActive { get; set; }
        public int TaskType { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDesc { get; set; }
        public System.DateTime AssignOn { get; set; }
        public System.DateTime DueDate { get; set; }
        public int PriorityID { get; set; }
        public string ExpectedOutcome { get; set; }
        public string Remark { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<System.DateTime> DeletedOn { get; set; }
        public string Priority { get; set; }
        public List<Cont_tbl_FileData> FileDetail { get; set; }
    }
    public class ContractAllTaskclass
    {
        public string Email { get; set; }

        public int contractID { get; set; }

    }
    public class getmyDocumentPathContractclass
    {
        public string Email { get; set; }

        public int ContractID { get; set; }

        public int FileID { get; set; }

        public int PageID { get; set; }

    }
    public class StatusType
    {
        public string StatusName { get; set; }
        public int StatusID { get; set; }
    }
    public class PriorityType
    {
        public string PriorityName { get; set; }
        public int PriorityID { get; set; }
    }
    public class mylocationContractclass
    {
        public string Email { get; set; }
    }
    public class StatusListContract
    {
        public string StatusName { get; set; }
        public long StatusID { get; set; }
    }
    public class StatusContractclass
    {
        public string Email { get; set; }

    }
    public class myDocumentContractclass
    {
        public string Email { get; set; }

        public int contractStatusID { get; set; }

        public int vendorID { get; set; }

        public int deptID { get; set; }

        public int branchID { get; set; }

        public int PageID { get; set; }

    }
    public class mycaseReportContractclass
    {
        public string Email { get; set; }

        public int contractStatusID { get; set; }

        public int vendorID { get; set; }

        public int deptID { get; set; }

        public int branchID { get; set; }

        public int PageID { get; set; }

    }
    public class myCaseWorkspaceContractclass
    {
        public string Email { get; set; }

        public int contractStatusID { get; set; }

        public int vendorID { get; set; }

        public int deptID { get; set; }

        public int branchID { get; set; }

        public int ContractID { get; set; }

        public int PageID { get; set; }

    }
    public class myCaseWorkspaceContractTaskclass
    {
        public string Email { get; set; }

        public string taskStatus { get; set; }//null

        public int priorityID { get; set; }//0

        public int PageID { get; set; }

    }
    public class ContractExpiringclass
    {
        public string Email { get; set; }

        public int upcomingDays { get; set; }

        public int contractStatusID { get; set; }

        public int vendorID { get; set; }

        public int deptID { get; set; }

        public int branchID { get; set; }

        public int PageID { get; set; }

    }
    public class ContractTypecount
    {
        public int DraftCount { get; set; }
        public int PendingReviewCount { get; set; }
        public int ReviewedCount { get; set; }
        public int PendingApprovalCount { get; set; }
        public int ApprovedCount { get; set; }
        public int ActiveCount { get; set; }
        public int ExpiredCount { get; set; }
        public int ReviewesCount { get; set; }
    }
    public class mydashboardclassContract
    {
        public string Email { get; set; }

    }
}