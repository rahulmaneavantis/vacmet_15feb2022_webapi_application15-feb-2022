﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class DashboardRoleCount
    {
        public int PerformerUpcomingStatutory { get; set; }
        public int PerformerUpcomingInternal { get; set; }
        public int PerformerOverdueStatutory { get; set; }
        public int PerformerOverdueInternal { get; set; }
        public int PerformerChecklistStatutory { get; set; }
        public int PerformerChecklistInternal { get; set; }
        public int PerformerRejectedStatutory { get; set; }
        public int PerformerRejectedInternal { get; set; }
        public int PerformerPendingforReviewStatutory { get; set; }
        public int PerformerPendingforReviewInternal { get; set; }
                
        public int ReviewerDueButNotSubmitedStatutory { get; set; }
        public int ReviewerDueButNotSubmitedInternal { get; set; }
        public int ReviewerPendingforReviewStatutory { get; set; }
        public int ReviewerPendingforReviewInternal { get; set; }
        public int ReviewerChecklistStatutory { get; set; }
        public int ReviewerChecklistInternal { get; set; }
        public int ReviewerRejectedStatutory { get; set; }
        public int ReviewerRejectedInternal { get; set; }
        
        public int AssignedEventCount { get; set; }
        public int ActivatedEventcount { get; set; }
        public int ClosedEventCount { get; set; }
    }
}

                