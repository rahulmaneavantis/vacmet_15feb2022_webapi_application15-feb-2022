﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class InternalLicenseMgmt
    {
        public static List<long> GetLicenseScheduleonIdList(long customerID,long UserID,long RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_InternalLicenseInstance
                             on row.LicenseID equals row1.ID
                             join row2 in entities.InternalComplianceAssignments
                           on row.ComplianceInstanceID equals row2.InternalComplianceInstanceID
                             where row1.CustomerID == customerID
                             && row2.UserID == UserID
                             && row2.RoleID == RoleID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }

        //public static List<long> GetLicenseScheduleonIdList(long customerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
        //                     join row1 in entities.Lic_tbl_InternalLicenseInstance
        //                     on row.LicenseID equals row1.ID
        //                     where row1.CustomerID == customerID
        //                     select row.ComplianceScheduleOnID).ToList();

        //        return query.ToList();
        //    }
        //}
    }
}