﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class QuestionDetails
    {
        public string QuestionOneID { get; set; }
        public string QuestionOne { get; set; }
        public string QuestionTwoID { get; set; }
        public string QuestionTwo { get; set; }
    }
}