﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWebApplication.Models
{
    public class CriticalDocumentList
    {
        public int ID { get; set; }
        public long? ParentID { get; set; }
        public int CustomerID { get; set; }
        public bool isFile { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public List<CriticalDocumentList> _subData { get; set; }
    }
    public static class GroupEnumerable
    {
        public static List<CriticalDocumentList> BuildTree(this IEnumerable<CriticalDocumentList> source)
        {
            var groups = source.GroupBy(i => i.ParentID);

            var roots = groups.FirstOrDefault(g => g.Key.HasValue == false).ToList();

            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddChildren(roots[i], dict);
            }

            return roots;
        }

        private static void AddChildren(CriticalDocumentList node, Dictionary<long, List<CriticalDocumentList>> source)
        {
            if (source.ContainsKey(node.ID))
            {
                node._subData = source[node.ID];
                for (int i = 0; i < node._subData.Count; i++)
                    AddChildren(node._subData[i], source);
            }
            else
            {
                node._subData = new List<CriticalDocumentList>();
            }
        }
    }

}