﻿using AppWebApplication.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace AppWebApplication.Models
{
    public class LitigationDocumentManagement
    {
        #region Gaurav
        public static List<sp_LitigationNoticeFileTag_Result> GetDistinctFileTagsNoticeMyDocument(int customerID, long litigationInstanceID, long UserID, string Role, string statusflag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.sp_LitigationNoticeFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN")
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }
                int statusid = -1;
                if (statusflag == "N")
                {
                    statusid = 1;
                }
                //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                //if ( IsEntityassignmentCustomer == Convert.ToString(customerID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerID, (int)UserID, Role, statusid);
                    if (caseList.Count > 0)
                    {
                        Query = Query.Where(entry => caseList.Contains(Convert.ToInt32(entry.noticecaseinstanceID))).ToList();
                    }
                    else
                    {
                        Query = Query.Where(entry => caseList.Contains(Convert.ToInt32(entry.noticecaseinstanceID))).ToList();
                    }
                }
                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationNoticeFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }
        public static List<sp_LitigationCaseFileTag_Result> GetDistinctFileTagsCaseMyDocument(int customerID, long litigationInstanceID, long UserID, string Role, string statusflag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.sp_LitigationCaseFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN")
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }
                int statusflagid = -1;
                if (statusflag == "C")
                {
                    statusflagid = 2;
                }
                //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                //if ( IsEntityassignmentCustomer == Convert.ToString(customerID))
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(customerID, (int)UserID, Role, statusflagid);
                    if (caseList.Count > 0)
                    {
                        Query = Query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    }
                    else
                    {
                        Query = Query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                    }
                }

                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationCaseFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }
        //public static List<sp_LitigationNoticeFileTag_Result> GetDistinctFileTagsNoticeMyDocument(int customerID, long litigationInstanceID, long UserID, string Role)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var Query = (from row in entities.sp_LitigationNoticeFileTag(customerID)
        //                     select row).Distinct().ToList();

        //        if (Role != "MGMT" && Role != "CADMN")
        //            Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            Query = Query.Where(entry => entry.RoleID == 3).ToList();
        //        }

        //        if (Query.Count > 0)
        //        {
        //            Query = (from g in Query
        //                     group g by new
        //                     {
        //                         g.FileTag
        //                     } into GCS
        //                     select new sp_LitigationNoticeFileTag_Result()
        //                     {
        //                         FileTag = GCS.Key.FileTag
        //                     }).ToList();
        //        }
        //        return Query;
        //    }
        //}
        public static List<sp_LitigationCaseFileTag_Result> GetDistinctFileTagsCaseMyDocument(int customerID, long litigationInstanceID, long UserID, string Role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.sp_LitigationCaseFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN")
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationCaseFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }
        public static List<SP_LitigationNoticeDocumentfor_Tag_Result> GetAllDocumentListofNotice(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, string FileName, int status, string TagFilter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerID)
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 1);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN
                    {
                        query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    }
                }

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                //else // In case of MGMT or CADMN
                //{
                //    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                //}
                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                if (status != -1)
                {
                    if (status == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.Status == status).ToList();
                    else
                        query = query.Where(entry => entry.Status < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
                }
                if (caseType != "" && caseType != "All")//B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }


                if (TagFilter != null && TagFilter != "")
                {
                    System.Collections.Generic.IList<string> lstSelectedFileTags = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(TagFilter);
                    if (lstSelectedFileTags.Count != 0)
                    {
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                    }
                }
                //if (lstSelectedFileTags.Count > 0)
                //{
                //    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                //}
                if (FileName != null)
                {
                    query = (from row in query where row.DocumentName != null select row).ToList();
                    query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName,
                                 g.CBU,
                                 g.Zone,
                                 g.Region,
                                 g.Territory,
                                 g.CaseCreator,
                                 g.EmailId,
                                 g.ContactNumber
                                 //g.Filetag
                             } into GCS
                             select new SP_LitigationNoticeDocumentfor_Tag_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName,
                                 CBU=GCS.Key.CBU,
                                 Zone=GCS.Key.Zone,
                                 Region=GCS.Key.Region,
                                 Territory=GCS.Key.Territory,
                                 CaseCreator=GCS.Key.CaseCreator,
                                 EmailId=GCS.Key.EmailId,
                                 ContactNumber=GCS.Key.ContactNumber
                             }).ToList();
                }
                return query;
            }
        }

        //public static List<SP_LitigationNoticeDocumentfor_Tag_Result> GetAllDocumentListofNotice(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, string FileName, int status, string TagFilter)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        var query = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerID)
        //                     select row).ToList();
        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN
        //        {
        //            query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
        //        }
        //        //if (branchList.Count > 0)
        //        //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        //if (deptID != -1)
        //        //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
        //        if (status != -1)
        //        {
        //            if (status == 3) //3--Closed otherwise Open
        //                query = query.Where(entry => entry.Status == status).ToList();
        //            else
        //                query = query.Where(entry => entry.Status < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
        //        }
        //        if (caseType != "" && caseType != "All")//B--All --Inward(I) and Outward(O)
        //            query = query.Where(entry => entry.TypeName == caseType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TypeName)
        //                .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }


        //        //if (TagFilter != null)
        //        //{
        //        //    System.Collections.Generic.IList<string> lstSelectedFileTags = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(TagFilter);
        //        //    if (lstSelectedFileTags.Count != 0)
        //        //    {
        //        //        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
        //        //        //objTaskCompDocument = objTaskCompDocument.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
        //        //    }
        //        //}
        //        //if (lstSelectedFileTags.Count > 0)
        //        //{
        //        //    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
        //        //}
        //        if (FileName != null)
        //        {
        //            query = (from row in query where row.DocumentName != null select row).ToList();
        //            query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();
        //        }
        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.RefNo,
        //                         g.TypeCaseNotice,
        //                         g.TypeName,
        //                         g.Title,
        //                         g.Description,
        //                         g.CustomerID,
        //                         g.CustomerBranchID,
        //                         g.BranchName,
        //                         g.DepartmentID,
        //                         g.DeptName,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.Status,
        //                         g.DocumentCount,
        //                         g.FYName
        //                         //g.Filetag
        //                     } into GCS
        //                     select new SP_LitigationNoticeDocumentfor_Tag_Result()
        //                     {
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         RefNo = GCS.Key.RefNo,
        //                         TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                         TypeName = GCS.Key.TypeName,
        //                         Title = GCS.Key.Title,
        //                         Description = GCS.Key.Description,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         BranchName = GCS.Key.BranchName,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         DeptName = GCS.Key.DeptName,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         Status = GCS.Key.Status,
        //                         DocumentCount = GCS.Key.DocumentCount,
        //                         FYName = GCS.Key.FYName
        //                     }).ToList();
        //        }
        //        return query;
        //    }
        //}
        public static bool CheckForClientNew(int CustomerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == CustomerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }


        public static List<SP_LitigationCaseDocumentfor_tag_Result> GetAllDocumentListofCase(long customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, /*List<string> lstSelectedFileTags,*/ string FileName, int status, string TagFilter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LitigationCaseDocumentfor_tag(customerID)
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 2);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN
                    {
                        query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    }
                }
                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                //else // In case of MGMT or CADMN
                //{
                //    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                //}
                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                if (status != -1)
                {
                    if (status == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.Status == status).ToList();
                    else
                        query = query.Where(entry => entry.Status < 3).ToList();
                }
                if (caseType != "" && caseType != "All") //B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }
                if (TagFilter != null && TagFilter != "")
                {
                    System.Collections.Generic.IList<string> lstSelectedFileTags = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(TagFilter);
                    if (lstSelectedFileTags.Count != 0)
                    {
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                        //objTaskCompDocument = objTaskCompDocument.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                    }
                }
                //if (lstSelectedFileTags.Count > 0)
                //{
                //    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                //}
                //if (!(string.IsNullOrEmpty(FileName)))
                {
                    query = query.Where(Entry => Entry.FileName.ToLower().Contains(FileName.ToLower())).ToList();

                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 //g.FileName,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName,
                                 g.CBU,
                                 g.Zone,
                                 g.Region,
                                 g.Territory,
                                 g.CaseCreator,
                                 g.EmailId,
                                 g.ContactNumber,
                             } into GCS
                             select new SP_LitigationCaseDocumentfor_tag_Result()
                             {
                                 //FileName=GCS.Key.FileName,
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName,
                                 CBU=GCS.Key.CBU,
                                 Zone=GCS.Key.Zone,
                                 Region=GCS.Key.Region,
                                 Territory=GCS.Key.Territory,
                                 CaseCreator=GCS.Key.CaseCreator,
                                 EmailId=GCS.Key.EmailId,
                                 ContactNumber=GCS.Key.ContactNumber,
                             }).ToList();
                }
                return query;
            }

        }
        #endregion

        #region Amol
        public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportDataReport(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                return query;
            }
        }


        public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportDataReport(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                return query;
            }
        }

        public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.CaseCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.TxnStatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.CaseType,
                                 g.NoticeCaseDesc,
                                 g.CaseRefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.Opposition,
                                 g.WinningProspect,
                                 g.NoticeDate
                             } into GCS
                             select new SP_LitigationCaseReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 TxnStatusID = GCS.Key.TxnStatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 CaseType = GCS.Key.CaseType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 Opposition = GCS.Key.Opposition,
                                 WinningProspect = GCS.Key.WinningProspect,
                                 NoticeDate = GCS.Key.NoticeDate
                             }).ToList();
                }
                if (StatusType != -1)
                {
                    if (StatusType == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == StatusType).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                //if (caseStatus == 3) //3--Closed otherwise Open
                //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                //else
                //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                //if (caseType != "" && caseType != "B") //B--All --Inward(I) and Outward(O)
                //    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }

        public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.NoticeCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.StatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.NoticeType,
                                 g.NoticeCaseDesc,
                                 g.RefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.Opposition                                 
                             } into GCS
                             select new sp_LitigationNoticeReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 StatusID = GCS.Key.StatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 RefNo = GCS.Key.RefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 Opposition=GCS.Key.Opposition
                             }).ToList();
                }
                if (StatusType != -1)
                {
                    if (StatusType == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.StatusID == StatusType).ToList();
                    else
                        query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
                }
                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                //if (caseStatus == 3) //3--Closed otherwise Open
                //    query = query.Where(entry => entry.StatusID == caseStatus).ToList();
                //else
                //    query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

                //if (caseType != "" && caseType != "B")//B--All --Inward(I) and Outward(O)
                //    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }
        #endregion

        public static List<tbl_LitigationFileData> GetDocuments(long v, List<string> doctypes)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == v && doctypes.Contains(row.DocType.Trim())
                                   && row.IsDeleted == false
                                   select row).ToList();

                return AllDocument;
            }
        }

        public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 1);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                }
                if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
                {

                    char[] MyChar = { '[', ']' };
                    AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

                    AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

                    List<int?> details = new List<int?>();
                    string[] values = AssingnedLawyer.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        details.Add(Convert.ToInt32(values[i]));
                    }
                    query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.NoticeCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.StatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.NoticeType,
                                 g.NoticeCaseDesc,
                                 g.RefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.WLResult,
                                 g.Jurisdiction,
                                 g.Provisionalamt,
                                 g.BankGurantee,
                                 g.ProtestMoney,
                                 g.FYName,
                                 g.ClaimAmt,
                                 g.ProbableAmt,
                                 g.CNResult,
                                 g.StateName,
                                 g.Opposition,
                                 g.CaseCreator,
                                 g.EmailId,
                                 g.ContactNumber,
                                 g.CBU,
                                 g.Zone,
                                 g.Region,
                                 g.Territory,
                                 g.NoticeStageName,
                                 g.Owner,
                                 g.OwnerName
                             } into GCS
                             select new sp_LitigationNoticeReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 StatusID = GCS.Key.StatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 RefNo = GCS.Key.RefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 WLResult = GCS.Key.WLResult,
                                 Jurisdiction = GCS.Key.Jurisdiction,
                                 Provisionalamt = GCS.Key.Provisionalamt,
                                 BankGurantee = GCS.Key.BankGurantee,
                                 ProtestMoney = GCS.Key.ProtestMoney,
                                 FYName = GCS.Key.FYName,
                                 ClaimAmt = GCS.Key.ClaimAmt,
                                 ProbableAmt = GCS.Key.ProbableAmt,
                                 CNResult = GCS.Key.CNResult,
                                 StateName = GCS.Key.StateName,
                                 Opposition = GCS.Key.Opposition,
                                 CaseCreator=GCS.Key.CaseCreator,
                                 EmailId= GCS.Key.EmailId,
                                 ContactNumber=GCS.Key.ContactNumber,
                                 CBU=GCS.Key.CBU,
                                 Zone=GCS.Key.Zone,
                                 Region=GCS.Key.Region,
                                 Territory=GCS.Key.Territory,
                                 NoticeStageName = GCS.Key.NoticeStageName,
                                 Owner = GCS.Key.Owner,
                                 OwnerName = GCS.Key.OwnerName
                             }).ToList();
                }
                if (StatusType != -1)
                {
                    if (StatusType == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.StatusID == StatusType).ToList();
                    else
                        query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }


        //public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
        //                     select row).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //        }
        //        if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
        //        {

        //            char[] MyChar = { '[', ']' };
        //            AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

        //            AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

        //            List<int?> details = new List<int?>();
        //            string[] values = AssingnedLawyer.Split(',');
        //            for (int i = 0; i < values.Length; i++)
        //            {
        //                details.Add(Convert.ToInt32(values[i]));
        //            }
        //            query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.Title,
        //                         g.OpenDate,
        //                         g.CloseDate,
        //                         g.Status,
        //                         g.CustomerID,
        //                         g.BranchName,
        //                         g.TypeCaseNotice,
        //                         g.NoticeCategoryID,
        //                         g.Category,
        //                         g.Department,
        //                         g.StatusID,
        //                         g.CustBrachID,
        //                         g.DepartmentID,
        //                         g.CustomerBranchID,
        //                         g.NoticeType,
        //                         g.NoticeCaseDesc,
        //                         g.RefNo,
        //                         g.TypeName,
        //                         g.Act,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CreatedBy,
        //                         g.WLResult,
        //                         g.Jurisdiction,
        //                         g.Provisionalamt,
        //                         g.BankGurantee,
        //                         g.ProtestMoney,
        //                         g.FYName,
        //                         g.ClaimAmt,
        //                         g.ProbableAmt,
        //                         g.CNResult,
        //                         g.StateName,
        //                         g.Opposition
        //                     } into GCS
        //                     select new sp_LitigationNoticeReportWithCustomParameter_Result()
        //                     {
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         Title = GCS.Key.Title,
        //                         OpenDate = GCS.Key.OpenDate,
        //                         CloseDate = GCS.Key.CloseDate,
        //                         Status = GCS.Key.Status,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         BranchName = GCS.Key.BranchName,
        //                         TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                         NoticeCategoryID = GCS.Key.NoticeCategoryID,
        //                         Category = GCS.Key.Category,
        //                         Department = GCS.Key.Department,
        //                         StatusID = GCS.Key.StatusID,
        //                         CustBrachID = GCS.Key.CustBrachID,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         NoticeType = GCS.Key.NoticeType,
        //                         NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
        //                         RefNo = GCS.Key.RefNo,
        //                         TypeName = GCS.Key.TypeName,
        //                         Act = GCS.Key.Act,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CreatedBy = GCS.Key.CreatedBy,
        //                         WLResult = GCS.Key.WLResult,
        //                         Jurisdiction = GCS.Key.Jurisdiction,
        //                         Provisionalamt = GCS.Key.Provisionalamt,
        //                         BankGurantee = GCS.Key.BankGurantee,
        //                         ProtestMoney = GCS.Key.ProtestMoney,
        //                         FYName = GCS.Key.FYName,
        //                         ClaimAmt = GCS.Key.ClaimAmt,
        //                         ProbableAmt = GCS.Key.ProbableAmt,
        //                         CNResult = GCS.Key.CNResult,
        //                         StateName = GCS.Key.StateName,
        //                         Opposition = GCS.Key.Opposition
        //                     }).ToList();
        //        }
        //        if (StatusType != -1)
        //        {
        //            if (StatusType == 3) //3--Closed otherwise Open
        //                query = query.Where(entry => entry.StatusID == StatusType).ToList();
        //            else
        //                query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
        //        }
        //        //if (branchList.Count > 0)
        //        //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        //if (deptID != -1)
        //        //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        //if (caseStatus == 3) //3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.StatusID == caseStatus).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

        //        //if (caseType != "" && caseType != "B")//B--All --Inward(I) and Outward(O)
        //        //    query = query.Where(entry => entry.TypeName == caseType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TypeName)
        //                .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query;
        //    }
        //}


        //public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
        //                     select row).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //        }
        //        if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
        //        {

        //            char[] MyChar = { '[', ']' };
        //            AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

        //            AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

        //            List<int?> details = new List<int?>();
        //            string[] values = AssingnedLawyer.Split(',');
        //            for (int i = 0; i < values.Length; i++)
        //            {
        //                details.Add(Convert.ToInt32(values[i]));
        //            }
        //            query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.Title,
        //                         g.OpenDate,
        //                         g.CloseDate,
        //                         g.Status,
        //                         g.CustomerID,
        //                         g.BranchName,
        //                         g.TypeCaseNotice,
        //                         g.NoticeCategoryID,
        //                         g.Category,
        //                         g.Department,
        //                         g.StatusID,
        //                         g.CustBrachID,
        //                         g.DepartmentID,
        //                         g.CustomerBranchID,
        //                         g.NoticeType,
        //                         g.NoticeCaseDesc,
        //                         g.RefNo,
        //                         g.TypeName,
        //                         g.Act,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CreatedBy,
        //                         g.WLResult,
        //                         g.Jurisdiction,
        //                         g.Provisionalamt,
        //                         g.BankGurantee,
        //                         g.ProtestMoney,
        //                         g.FYName,
        //                         g.ClaimAmt,
        //                         g.ProbableAmt,
        //                         g.CNResult,
        //                         g.StateName,
        //                         g.Opposition
        //                     } into GCS
        //                     select new sp_LitigationNoticeReportWithCustomParameter_Result()
        //                     {
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         Title = GCS.Key.Title,
        //                         OpenDate = GCS.Key.OpenDate,
        //                         CloseDate = GCS.Key.CloseDate,
        //                         Status = GCS.Key.Status,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         BranchName = GCS.Key.BranchName,
        //                         TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                         NoticeCategoryID = GCS.Key.NoticeCategoryID,
        //                         Category = GCS.Key.Category,
        //                         Department = GCS.Key.Department,
        //                         StatusID = GCS.Key.StatusID,
        //                         CustBrachID = GCS.Key.CustBrachID,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         NoticeType = GCS.Key.NoticeType,
        //                         NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
        //                         RefNo = GCS.Key.RefNo,
        //                         TypeName = GCS.Key.TypeName,
        //                         Act = GCS.Key.Act,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CreatedBy = GCS.Key.CreatedBy,
        //                         WLResult = GCS.Key.WLResult,
        //                         Jurisdiction = GCS.Key.Jurisdiction,
        //                         Provisionalamt = GCS.Key.Provisionalamt,
        //                         BankGurantee = GCS.Key.BankGurantee,
        //                         ProtestMoney = GCS.Key.ProtestMoney,
        //                         FYName = GCS.Key.FYName,
        //                         ClaimAmt = GCS.Key.ClaimAmt,
        //                         ProbableAmt = GCS.Key.ProbableAmt,
        //                         CNResult = GCS.Key.CNResult,
        //                         StateName = GCS.Key.StateName,
        //                         Opposition = GCS.Key.Opposition
        //                     }).ToList();
        //        }
        //        if (StatusType != -1)
        //        {
        //            if (StatusType == 3) //3--Closed otherwise Open
        //                query = query.Where(entry => entry.StatusID == StatusType).ToList();
        //            else
        //                query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
        //        }
        //        //if (branchList.Count > 0)
        //        //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        //if (deptID != -1)
        //        //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        //if (caseStatus == 3) //3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.StatusID == caseStatus).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

        //        //if (caseType != "" && caseType != "B")//B--All --Inward(I) and Outward(O)
        //        //    query = query.Where(entry => entry.TypeName == caseType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TypeName)
        //                .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query;
        //    }
        //}

        public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 2);
                        //if (caseList.Count > 0)
                        //{
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        //}
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                }
                if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
                {

                    char[] MyChar = { '[', ']' };
                    AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

                    AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

                    List<int?> details = new List<int?>();
                    string[] values = AssingnedLawyer.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        details.Add(Convert.ToInt32(values[i]));
                    }
                    query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.CaseCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.TxnStatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.CaseType,
                                 g.NoticeCaseDesc,
                                 g.CaseRefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.CourtID,
                                 g.WLResult,
                                 g.Jurisdiction,
                                 g.ClaimAmt,
                                 g.ProbableAmt,
                                 g.CNResult,
                                 g.StateName,
                                 g.Provisionalamt,
                                 g.FYName,
                                 g.BankGurantee,
                                 g.ProtestMoney,
                                 g.Opposition,
                                 g.CaseStage,
                                 g.NextHearingDate,
                                 g.NoticeDate,
                                 g.CaseCreator,
                                 g.EmailId,
                                 g.ContactNumber,
                                 g.CBU,
                                 g.Zone,
                                 g.Region,
                                 g.Territory,
                                 g.Owner,
                                 g.OwnerName
                             } into GCS
                             select new SP_LitigationCaseReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 TxnStatusID = GCS.Key.TxnStatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 CaseType = GCS.Key.CaseType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 CourtID = GCS.Key.CourtID,
                                 WLResult = GCS.Key.WLResult,
                                 Jurisdiction = GCS.Key.Jurisdiction,
                                 ClaimAmt = GCS.Key.ClaimAmt,
                                 ProbableAmt = GCS.Key.ProbableAmt,
                                 CNResult = GCS.Key.CNResult,
                                 StateName = GCS.Key.StateName,
                                 Provisionalamt = GCS.Key.Provisionalamt,
                                 FYName = GCS.Key.FYName,
                                 BankGurantee = GCS.Key.BankGurantee,
                                 ProtestMoney = GCS.Key.ProtestMoney,
                                 Opposition = GCS.Key.Opposition,
                                 CaseStage = GCS.Key.CaseStage,
                                 NextHearingDate = GCS.Key.NextHearingDate,
                                 NoticeDate= GCS.Key.NoticeDate,
                                 CaseCreator=GCS.Key.CaseCreator,
                                 EmailId=GCS.Key.EmailId,
                                 ContactNumber=GCS.Key.ContactNumber,
                                 CBU=GCS.Key.CBU,
                                 Zone=GCS.Key.Zone,
                                 Region=GCS.Key.Region,
                                 Territory=GCS.Key.Territory,
                                 Owner = GCS.Key.Owner,
                                 OwnerName=GCS.Key.OwnerName
                             }).ToList();
                }

                if (StatusType != -1)
                {
                    if (StatusType == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == StatusType).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }


        //public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
        //                     select row).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //        }
        //        if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
        //        {

        //            char[] MyChar = { '[', ']' };
        //            AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

        //            AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

        //            List<int?> details = new List<int?>();
        //            string[] values = AssingnedLawyer.Split(',');
        //            for (int i = 0; i < values.Length; i++)
        //            {
        //                details.Add(Convert.ToInt32(values[i]));
        //            }
        //            query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
        //        }
        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.Title,
        //                         g.OpenDate,
        //                         g.CloseDate,
        //                         g.Status,
        //                         g.CustomerID,
        //                         g.BranchName,
        //                         g.TypeCaseNotice,
        //                         g.CaseCategoryID,
        //                         g.Category,
        //                         g.Department,
        //                         g.TxnStatusID,
        //                         g.CustBrachID,
        //                         g.DepartmentID,
        //                         g.CustomerBranchID,
        //                         g.CaseType,
        //                         g.NoticeCaseDesc,
        //                         g.CaseRefNo,
        //                         g.TypeName,
        //                         g.Act,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CreatedBy,
        //                         g.CourtID,
        //                         g.WLResult,
        //                         g.Jurisdiction,
        //                         g.ClaimAmt,
        //                         g.ProbableAmt,
        //                         g.CNResult,
        //                         g.StateName,
        //                         g.Provisionalamt,
        //                         g.FYName,
        //                         g.BankGurantee,
        //                         g.ProtestMoney,
        //                         g.Opposition,
        //                         g.CaseStage
        //                     } into GCS
        //                     select new SP_LitigationCaseReportWithCustomParameter_Result()
        //                     {
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         Title = GCS.Key.Title,
        //                         OpenDate = GCS.Key.OpenDate,
        //                         CloseDate = GCS.Key.CloseDate,
        //                         Status = GCS.Key.Status,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         BranchName = GCS.Key.BranchName,
        //                         TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                         CaseCategoryID = GCS.Key.CaseCategoryID,
        //                         Category = GCS.Key.Category,
        //                         Department = GCS.Key.Department,
        //                         TxnStatusID = GCS.Key.TxnStatusID,
        //                         CustBrachID = GCS.Key.CustBrachID,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         CaseType = GCS.Key.CaseType,
        //                         NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
        //                         CaseRefNo = GCS.Key.CaseRefNo,
        //                         TypeName = GCS.Key.TypeName,
        //                         Act = GCS.Key.Act,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CreatedBy = GCS.Key.CreatedBy,
        //                         CourtID = GCS.Key.CourtID,
        //                         WLResult = GCS.Key.WLResult,
        //                         Jurisdiction = GCS.Key.Jurisdiction,
        //                         ClaimAmt = GCS.Key.ClaimAmt,
        //                         ProbableAmt = GCS.Key.ProbableAmt,
        //                         CNResult = GCS.Key.CNResult,
        //                         StateName = GCS.Key.StateName,
        //                         Provisionalamt = GCS.Key.Provisionalamt,
        //                         FYName = GCS.Key.FYName,
        //                         BankGurantee = GCS.Key.BankGurantee,
        //                         ProtestMoney = GCS.Key.ProtestMoney,
        //                         Opposition = GCS.Key.Opposition,
        //                         CaseStage = GCS.Key.CaseStage
        //                     }).ToList();
        //        }
        //        if (StatusType != -1)
        //        {
        //            if (StatusType == 3) //3--Closed otherwise Open
        //                query = query.Where(entry => entry.TxnStatusID == StatusType).ToList();
        //            else
        //                query = query.Where(entry => entry.TxnStatusID < 3).ToList();

        //        }


        //        //if (branchList.Count > 0)
        //        //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        //if (deptID != -1)
        //        //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        //if (caseStatus == 3) //3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

        //        //if (caseType != "" && caseType != "B") //B--All --Inward(I) and Outward(O)
        //        //    query = query.Where(entry => entry.TypeName == caseType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TypeName)
        //                .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query;
        //    }
        //}


        //public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportData(long customerID, int loggedInUserID, string loggedInUserRole, int StatusType, string AssingnedLawyer)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
        //                     select row).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //        }
        //        if (AssingnedLawyer != "[]" && AssingnedLawyer != null && AssingnedLawyer != "-1")
        //        {

        //            char[] MyChar = { '[', ']' };
        //            AssingnedLawyer = AssingnedLawyer.TrimEnd(MyChar);

        //            AssingnedLawyer = AssingnedLawyer.TrimStart(MyChar);

        //            List<int?> details = new List<int?>();
        //            string[] values = AssingnedLawyer.Split(',');
        //            for (int i = 0; i < values.Length; i++)
        //            {
        //                details.Add(Convert.ToInt32(values[i]));
        //            }
        //            query = query.Where(x => details.Contains(x.AssingedUserID)).ToList();
        //        }
        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeCaseInstanceID,
        //                         g.Title,
        //                         g.OpenDate,
        //                         g.CloseDate,
        //                         g.Status,
        //                         g.CustomerID,
        //                         g.BranchName,
        //                         g.TypeCaseNotice,
        //                         g.CaseCategoryID,
        //                         g.Category,
        //                         g.Department,
        //                         g.TxnStatusID,
        //                         g.CustBrachID,
        //                         g.DepartmentID,
        //                         g.CustomerBranchID,
        //                         g.CaseType,
        //                         g.NoticeCaseDesc,
        //                         g.CaseRefNo,
        //                         g.TypeName,
        //                         g.Act,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CreatedBy,
        //                         g.CourtID,
        //                         g.WLResult,
        //                         g.Jurisdiction,
        //                         g.ClaimAmt,
        //                         g.ProbableAmt,
        //                         g.CNResult,
        //                         g.StateName,
        //                         g.Provisionalamt,
        //                         g.FYName,
        //                         g.BankGurantee,
        //                         g.ProtestMoney,
        //                         g.Opposition
        //                     } into GCS
        //                     select new SP_LitigationCaseReportWithCustomParameter_Result()
        //                     {
        //                         NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
        //                         Title = GCS.Key.Title,
        //                         OpenDate = GCS.Key.OpenDate,
        //                         CloseDate = GCS.Key.CloseDate,
        //                         Status = GCS.Key.Status,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         BranchName = GCS.Key.BranchName,
        //                         TypeCaseNotice = GCS.Key.TypeCaseNotice,
        //                         CaseCategoryID = GCS.Key.CaseCategoryID,
        //                         Category = GCS.Key.Category,
        //                         Department = GCS.Key.Department,
        //                         TxnStatusID = GCS.Key.TxnStatusID,
        //                         CustBrachID = GCS.Key.CustBrachID,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         CaseType = GCS.Key.CaseType,
        //                         NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
        //                         CaseRefNo = GCS.Key.CaseRefNo,
        //                         TypeName = GCS.Key.TypeName,
        //                         Act = GCS.Key.Act,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CreatedBy = GCS.Key.CreatedBy,
        //                         CourtID = GCS.Key.CourtID,
        //                         WLResult = GCS.Key.WLResult,
        //                         Jurisdiction = GCS.Key.Jurisdiction,
        //                         ClaimAmt = GCS.Key.ClaimAmt,
        //                         ProbableAmt = GCS.Key.ProbableAmt,
        //                         CNResult = GCS.Key.CNResult,
        //                         StateName = GCS.Key.StateName,
        //                         Provisionalamt = GCS.Key.Provisionalamt,
        //                         FYName = GCS.Key.FYName,
        //                         BankGurantee = GCS.Key.BankGurantee,
        //                         ProtestMoney = GCS.Key.ProtestMoney,
        //                         Opposition = GCS.Key.Opposition
        //                     }).ToList();
        //        }
        //        if (StatusType != -1)
        //        {
        //            if (StatusType == 3) //3--Closed otherwise Open
        //                query = query.Where(entry => entry.TxnStatusID == StatusType).ToList();
        //            else
        //                query = query.Where(entry => entry.TxnStatusID < 3).ToList();

        //        }


        //        //if (branchList.Count > 0)
        //        //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        //if (partyID != -1)
        //        //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        //if (deptID != -1)
        //        //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        //if (caseStatus == 3) //3--Closed otherwise Open
        //        //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
        //        //else
        //        //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

        //        //if (caseType != "" && caseType != "B") //B--All --Inward(I) and Outward(O)
        //        //    query = query.Where(entry => entry.TypeName == caseType).ToList();

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderBy(entry => entry.TypeName)
        //                .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query;
        //    }
        //}



    }
}