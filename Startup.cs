﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Net;
using AppWebApplication.Models;
using System.Reflection;

[assembly: OwinStartup(typeof(AppWebApplication.Startup))]

namespace AppWebApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            try
            {
                app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
                int timespan = Convert.ToInt32(ConfigurationManager.AppSettings["owinAuthtimespan"]);

                var myProvider = new MyAuthorizationServerProvider();
                OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
                {
                    AllowInsecureHttp = true,
                    TokenEndpointPath = new PathString("/token"),
                    //AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                    AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(timespan),
                    Provider = myProvider
                };
                string test = Convert.ToString(ConfigurationManager.AppSettings["chktest"]);
                if (test.Equals("true"))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                else
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                }
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                app.UseOAuthAuthorizationServer(options);
                app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
                HttpConfiguration config = new HttpConfiguration();
                WebApiConfig.Register(config);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}