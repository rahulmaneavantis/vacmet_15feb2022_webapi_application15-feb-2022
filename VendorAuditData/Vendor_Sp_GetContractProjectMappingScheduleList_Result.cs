//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VendorAuditData
{
    using System;
    
    public partial class Vendor_Sp_GetContractProjectMappingScheduleList_Result
    {
        public int ID { get; set; }
        public int CustID { get; set; }
        public int ContractorID { get; set; }
        public int ProjectID { get; set; }
        public int IsSubcontract { get; set; }
        public Nullable<int> UnderContractorID { get; set; }
        public string Frequency { get; set; }
        public System.DateTime DueDate { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<System.DateTime> Activationdate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> ClosureDate { get; set; }
        public Nullable<int> Isdisable { get; set; }
        public int DueDays { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public int IsShowContractor { get; set; }
        public Nullable<System.DateTime> ContractStartDate { get; set; }
        public Nullable<System.DateTime> ContractEndDate { get; set; }
        public Nullable<long> ProjectContSpocID { get; set; }
        public Nullable<long> ProjectSubContSpocID { get; set; }
        public string ProjectName { get; set; }
        public string ContractorName { get; set; }
        public string SubcontractorName { get; set; }
        public int LocationID { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Location { get; set; }
        public string StateName { get; set; }
        public string ContractorType { get; set; }
        public string FrequencyDetail { get; set; }
        public System.DateTime ScheduleOn { get; set; }
        public long ScheduleOnID { get; set; }
        public string ForMonth { get; set; }
        public long AuditorID { get; set; }
        public long ContractorProjectMappingID { get; set; }
        public Nullable<int> ProjectHead { get; set; }
        public Nullable<int> ProjectDirector { get; set; }
        public string Status { get; set; }
        public int AuditCheckListStatusID { get; set; }
        public string complitionstatus { get; set; }
        public Nullable<System.DateTime> auditupdateddate { get; set; }
    }
}
