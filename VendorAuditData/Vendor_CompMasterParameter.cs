//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VendorAuditData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vendor_CompMasterParameter
    {
        public int ID { get; set; }
        public int ComplianceId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterType { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<bool> UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
    }
}
