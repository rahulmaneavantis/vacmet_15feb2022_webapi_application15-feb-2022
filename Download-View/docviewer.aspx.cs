﻿using AppWebApplication.DataLitigation;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebApplication.Download_View
{
    public partial class docviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    string filepath = Request.QueryString["docurl"].ToString();
                    if (filepath != "undefined")
                    {
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {

                            doccontrol.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(Request.QueryString["docurl"]);
                        }
                        else
                        {
                            doccontrol.Document = Server.MapPath(Convert.ToString(Request.QueryString["docurl"]));
                        }
                    }
                    else
                    {
                        //lblMessage.Text = "Sorry File not find";
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["CCaseID"])
                && !string.IsNullOrEmpty(Request.QueryString["DocTypeN"])
                && !string.IsNullOrEmpty(Request.QueryString["ID"])
                && !string.IsNullOrEmpty(Request.QueryString["CDoc"]))
                {
                    long CCaseID = Convert.ToInt64(Request.QueryString["CCaseID"]);
                    string DocType = Convert.ToString(Request.QueryString["DocTypeN"]);
                    long ID = Convert.ToInt64(Request.QueryString["ID"]);

                    List<tax_tbl_CommercialCaseFileData> CMPDocuments = new List<tax_tbl_CommercialCaseFileData>();
                    CMPDocuments = TaxationMethods.GetCCDocuments(CCaseID, DocType, ID);

                    if (CMPDocuments.Count > 0)
                    {
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = "0";

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = 1 + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (true)
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(TaxationMethods.ReadDocFiles(filePath)));
                                }
                                bw.Close();

                                doccontrol.Document = doccontrol.Document = Server.MapPath(FileName);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }
        }
    }
}
