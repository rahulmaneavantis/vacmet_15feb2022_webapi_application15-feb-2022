﻿using AppWebApplication.DataLitigation;
using AppWebApplication.Models;
using AppWebApplication.Models.Litigation;
using AppWebApplication.VendorAuditModel;
using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VendorAuditData;

namespace AppWebApplication.Download_View
{
    public partial class DownloadMyDocuments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["ContractProjectMappingId"])
              && !string.IsNullOrEmpty(Request.QueryString["ScheduleOnId"])
              && !string.IsNullOrEmpty(Request.QueryString["FileID"])
              && !string.IsNullOrEmpty(Request.QueryString["Period"])
              && !string.IsNullOrEmpty(Request.QueryString["CustID"]))
            {
                try
                {
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        List<Vendor_tbl_FileTransactionData> CMPDocuments = new List<Vendor_tbl_FileTransactionData>();

                        CMPDocuments = (from row in entities.Vendor_tbl_FileTransactionData
                                        where row.IsDeleted == false
                                        select row).ToList();

                        int FileID = Convert.ToInt32(Request.QueryString["FileID"]);
                        long ContractProjectMappingId = Convert.ToInt64(Request.QueryString["ContractProjectMappingId"]);
                        long ScheduleOnId = Convert.ToInt64(Request.QueryString["ScheduleOnId"]);
                        int CustID = Convert.ToInt32(Request.QueryString["CustID"]);
                        string Period = Convert.ToString(Request.QueryString["Period"]);

                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var Result = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(CustID)
                                          where row.ContractorProjectMappingID == ContractProjectMappingId
                                          && row.ScheduleOnID == ScheduleOnId
                                          && row.ForMonth == Period
                                          select row).FirstOrDefault();

                            string directoryName = Result.ProjectName + "/" + Result.Location + "/";

                            directoryName = directoryName + Result.ForMonth + "/" + Result.ContractorName;
                            var CMPDocumentsRsult = CMPDocuments.Where(x => x.ContractProjectMappingId == Result.ContractorProjectMappingID
                            && x.ScheduleOnId == Result.ScheduleOnID).ToList();

                            if (CMPDocumentsRsult.Count > 0)
                            {
                                int i = 0;
                                if (CMPDocumentsRsult != null)
                                {
                                    foreach (var file in CMPDocumentsRsult)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName);
                                        string filePath = string.Empty;

                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;

                                            ComplianceZip.AddEntry(directoryName + "/" + getComplianceHeader(file.ComplianceId) + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            i++;
                                        }
                                    }
                                }

                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["ContractProjectMappingId"])
             && !string.IsNullOrEmpty(Request.QueryString["ComplianceId"])
             && !string.IsNullOrEmpty(Request.QueryString["ScheduleOnId"])
             && !string.IsNullOrEmpty(Request.QueryString["CustID"]))
            {
                BindMyDocuments();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"])
               && !string.IsNullOrEmpty(Request.QueryString["FileID"]))
            {
                BindInnerMyDocument();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["DownloadAuditChecklistDoc"]))
            {
                try
                {
                    List<ProjectComplianceWithDoc> objResult = new List<ProjectComplianceWithDoc>();
                    List<Vendor_Sp_GetContractProjectchecklistDocumentList_Result> objDoc = new List<Vendor_Sp_GetContractProjectchecklistDocumentList_Result>();

                    var keys = Request.QueryString["DownloadAuditChecklistDoc"];
                    dynamic key = JsonConvert.DeserializeObject(keys);

                    foreach (var item in key)
                    {
                        long ContractProjectMappingId = Convert.ToInt64(item["ContractProjectMappingId"].Value);
                        long ScheduleOnId = Convert.ToInt64(item["ScheduleOnId"].Value);
                        int CustID = Convert.ToInt32(item["CustomerID"].Value);
                        string Period = Convert.ToString(item["Period"].Value);
                        objResult.Add(new ProjectComplianceWithDoc
                        {
                            ContractProjectMappingId = ContractProjectMappingId,
                            ScheduleOnId = ScheduleOnId,
                            CustomerID = CustID,
                            Period = Period
                        });
                    }

                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {

                        List<Vendor_tbl_FileTransactionData> CMPDocuments = new List<Vendor_tbl_FileTransactionData>();

                        CMPDocuments = (from row in entities.Vendor_tbl_FileTransactionData
                                        where row.IsDeleted == false
                                        select row).ToList();

                        foreach (var items in objResult)
                        {
                            var Result = (from row in entities.Vendor_Sp_GetContractProjectchecklistDocumentList(items.CustomerID)
                                          where row.ContractorProjectMappingID == items.ContractProjectMappingId
                                          && row.ScheduleOnID == items.ScheduleOnId
                                           && row.ForMonth == items.Period
                                          select row).FirstOrDefault();
                            objDoc.Add(Result);
                        }

                        // objDoc = objDoc.GroupBy(x => new { x.ProjectID, x.Period, x.ContractorName }).Select(x => x.FirstOrDefault()).ToList();
                        int ProjectID = 0;
                        string Period = "1";

                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            foreach (var item in objDoc)
                            {
                                string directoryName = item.ProjectName + "/" + item.Location + "/";

                                //if (ProjectID != item.ProjectID && Period != item.Period)
                                if (Period != item.ForMonth)
                                {
                                    directoryName = directoryName + item.ForMonth + "/" + item.ContractorName;
                                    ProjectID = item.ProjectID;
                                    Period = item.ForMonth;

                                    var CMPDocumentsRsult = CMPDocuments.Where(x => x.ContractProjectMappingId == item.ContractorProjectMappingID
                                    && x.ScheduleOnId == item.ScheduleOnID).ToList();

                                    if (CMPDocumentsRsult.Count > 0)
                                    {
                                        int i = 0;
                                        if (CMPDocumentsRsult != null)
                                        {
                                            foreach (var file in CMPDocumentsRsult)
                                            {
                                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                                var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName)).FirstOrDefault();
                                                if (dictionary == null)
                                                    ComplianceZip.AddDirectoryByName(directoryName);
                                                string filePath = string.Empty;

                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string ext = Path.GetExtension(file.FileName);
                                                    string[] filename = file.FileName.Split('.');
                                                    //string str = filename[0] + i + "." + filename[1];
                                                    string str = filename[0] + i + "." + ext;

                                                    ComplianceZip.AddEntry(directoryName + "/" + getComplianceHeader(file.ComplianceId) + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    //ComplianceZip.AddEntry(file.ID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    i++;
                                                }
                                            }
                                        }

                                    }

                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["DocFileID"])
            && !string.IsNullOrEmpty(Request.QueryString["ComplianceId"])
            && !string.IsNullOrEmpty(Request.QueryString["CustID"]))
            {
                int DocFileID = Convert.ToInt32(Request.QueryString["DocFileID"]);
                int ComplianceId = Convert.ToInt32(Request.QueryString["ComplianceId"]);
                int CustID = Convert.ToInt32(Request.QueryString["CustID"]);

                using (ZipFile LitigationZip = new ZipFile())
                {
                    List<Vendor_tbl_FileTransactionData> CMPDocuments = new List<Vendor_tbl_FileTransactionData>();
                    using (VendorAuditEntities entities = new VendorAuditEntities())
                    {
                        CMPDocuments = (from row in entities.Vendor_tbl_FileTransactionData
                                        where row.IsDeleted == false
                                        && row.ComplianceId == ComplianceId
                                        && row.ID == DocFileID
                                        && row.IsDeleted == false
                                        select row).ToList();
                    }
                    if (CMPDocuments.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                if (true)
                                {
                                    LitigationZip.AddEntry(str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                i++;
                            }
                        }
                        var zipMs = new MemoryStream();
                        LitigationZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaxDocument.zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["CCaseID"])
            && !string.IsNullOrEmpty(Request.QueryString["DocType"])
            && !string.IsNullOrEmpty(Request.QueryString["ResponseID"]))
            {
                long CCaseID = Convert.ToInt64(Request.QueryString["CCaseID"]);
                string DocType = Convert.ToString(Request.QueryString["DocType"]);
                long ResponseID = Convert.ToInt64(Request.QueryString["ResponseID"]);

                using (ZipFile LitigationZip = new ZipFile())
                {
                    List<tax_tbl_CommercialCaseFileData> CMPDocuments = new List<tax_tbl_CommercialCaseFileData>();
                    CMPDocuments = TaxationMethods.GetCCDocumentsById(CCaseID, DocType, ResponseID);

                    if (CMPDocuments.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                LitigationZip.AddEntry(str, CryptographyHandler.AESDecrypt(TaxationMethods.ReadDocFiles(filePath)));
                                i++;
                            }
                        }
                        var zipMs = new MemoryStream();
                        LitigationZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaxDocument.zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["CCaseID"])
            && !string.IsNullOrEmpty(Request.QueryString["DocType1"])
            && !string.IsNullOrEmpty(Request.QueryString["DocType2"])
            && !string.IsNullOrEmpty(Request.QueryString["ResponseID"]))
            {
                long CCaseID = Convert.ToInt64(Request.QueryString["CCaseID"]);
                string DocType1 = Convert.ToString(Request.QueryString["DocType1"]);
                string DocType2 = Convert.ToString(Request.QueryString["DocType2"]);
                long ResponseID = Convert.ToInt64(Request.QueryString["ResponseID"]);

                string DocTypes = DocType1 + ',' + DocType2;

                using (ZipFile LitigationZip = new ZipFile())
                {
                    List<tax_tbl_CommercialCaseFileData> CMPDocuments = new List<tax_tbl_CommercialCaseFileData>();
                    CMPDocuments = TaxationMethods.GetCCDocumentsByIds(CCaseID, DocTypes, ResponseID);

                    if (CMPDocuments.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                LitigationZip.AddEntry(str, CryptographyHandler.AESDecrypt(TaxationMethods.ReadDocFiles(filePath)));
                                i++;
                            }
                        }
                        var zipMs = new MemoryStream();
                        LitigationZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaxDocument.zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["CCaseID"])
            && !string.IsNullOrEmpty(Request.QueryString["DocType"])
            && !string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                long CCaseID = Convert.ToInt64(Request.QueryString["CCaseID"]);
                string DocType = Convert.ToString(Request.QueryString["DocType"]);
                long ID = Convert.ToInt64(Request.QueryString["ID"]);

                using (ZipFile LitigationZip = new ZipFile())
                {
                    List<tax_tbl_CommercialCaseFileData> CMPDocuments = new List<tax_tbl_CommercialCaseFileData>();
                    CMPDocuments = TaxationMethods.GetCCDocuments(CCaseID, DocType, ID);

                    if (CMPDocuments.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                LitigationZip.AddEntry(str, CryptographyHandler.AESDecrypt(TaxationMethods.ReadDocFiles(filePath)));
                                i++;
                            }
                        }
                        var zipMs = new MemoryStream();
                        LitigationZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaxDocument.zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }

        }

        public static string getComplianceHeader(int ComplianceID)
        {
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                var query = (from row in entities.Vendor_ComplainceMaster
                             where row.ID == ComplianceID
                             && row.IsDeleted == false
                             select row.ComplainceHeader).FirstOrDefault();

                if (query != null)
                    return query;
                else
                    return "";
            }
        }

        public class ProjectComplianceWithDoc
        {
            public int CustomerID { get; set; }
            public long ContractProjectMappingId { get; set; }
            public long ScheduleOnId { get; set; }
            public string Period { get; set; }
        }


        public void BindMyDocuments()
        {
            string ICDocPath = string.Empty;
            int ContractProjectMappingId = Convert.ToInt32(Request.QueryString["ContractProjectMappingId"]);
            int ComplianceId = Convert.ToInt32(Request.QueryString["ComplianceId"]);
            int ScheduleOnId = Convert.ToInt32(Request.QueryString["ScheduleOnId"]);
            int CustID = Convert.ToInt32(Request.QueryString["CustID"]);
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<Vendor_tbl_FileTransactionData> CMPDocuments = (from row in entities.Vendor_tbl_FileTransactionData
                                                                     where row.ContractProjectMappingId == ContractProjectMappingId
                                                                     && row.CustomerID == CustID
                                                                     && row.ScheduleOnId == ScheduleOnId
                                                                     && row.IsDeleted==false
                                                                     select row).ToList();

                if (ComplianceId != 0)
                {
                    CMPDocuments = CMPDocuments.Where(x => x.ComplianceId == ComplianceId).ToList();
                }

                if (CMPDocuments.Count > 0)
                {

                    List<Vendor_tbl_FileTransactionData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        Vendor_tbl_FileTransactionData entityData = new Vendor_tbl_FileTransactionData();
                        entityData.Version = "1.0";
                        entitiesData.Add(entityData);
                    }

                    if (entitiesData.Count > 0)
                    {
                        rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        rptLitigationVersionView.DataBind();

                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles/Tax";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }


                                string customerID = "0";

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = 1 + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (true)
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(VendorAuditMaster.ReadDocFiles(filePath)));
                                }
                                bw.Close();

                                ICDocPath = FileName;
                                //ICDocPath = ICDocPath.Substring(2, ICDocPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + ICDocPath + "');", true);
                                //lblMessage.Text = "";
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation();", true);
                            }
                            break;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                }
            }
        }

        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView1");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);
            }
        }

        protected void rptLitigationVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string ICDocPath = string.Empty;
                int ContractProjectMappingId = Convert.ToInt32(Request.QueryString["ContractProjectMappingId"]);
                int ComplianceId = Convert.ToInt32(Request.QueryString["ComplianceId"]);
                int ScheduleOnId = Convert.ToInt32(Request.QueryString["ScheduleOnId"]);
                int CustID = Convert.ToInt32(Request.QueryString["CustID"]);
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<Vendor_tbl_FileTransactionData> LitigationFileData = new List<Vendor_tbl_FileTransactionData>();
                List<Vendor_tbl_FileTransactionData> LitigationDocument = new List<Vendor_tbl_FileTransactionData>();
                using (VendorAuditEntities entities = new VendorAuditEntities())
                {
                    LitigationDocument = (from row in entities.Vendor_tbl_FileTransactionData
                                          where row.ContractProjectMappingId == ContractProjectMappingId
                                          && row.CustomerID == CustID
                                          && row.ScheduleOnId == ScheduleOnId
                                          && row.IsDeleted==false
                                          select row).ToList();

                    if (ComplianceId != 0)
                    {
                        LitigationDocument = LitigationDocument.Where(x => x.ComplianceId == ComplianceId).ToList();
                    }

                    if (commandArgs[1].Equals("1.0"))
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (LitigationFileData.Count <= 0)
                        {
                            LitigationFileData = LitigationDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("View"))
                    {
                        if (LitigationFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in LitigationFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/Tax";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }


                                    string customerID = "0";

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = 1 + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (true)
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(VendorAuditMaster.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    ICDocPath = FileName;
                                    //ICDocPath = ICDocPath.Substring(2, ICDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalNew", "fopendocfileLitigation('" + ICDocPath + "');", true);
                                    //lblMessage.Text = "";
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindInnerMyDocument()
        {
            string ICDocPath = string.Empty;
            int CustID = Convert.ToInt32(Request.QueryString["CustomerID"]);
            int FileID = Convert.ToInt32(Request.QueryString["FileID"]);
            using (VendorAuditEntities entities = new VendorAuditEntities())
            {
                List<Vendor_tbl_FileTransactionData> CMPDocuments = (from row in entities.Vendor_tbl_FileTransactionData
                                                                     where row.CustomerID == CustID
                                                                     && row.IsDeleted == false
                                                                     select row).ToList();

                if (FileID != 0)
                {
                    CMPDocuments = CMPDocuments.Where(x => x.ID == FileID).ToList();
                }

                if (CMPDocuments.Count > 0)
                {

                    List<Vendor_tbl_FileTransactionData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        Vendor_tbl_FileTransactionData entityData = new Vendor_tbl_FileTransactionData();
                        entityData.Version = "1.0";
                        entitiesData.Add(entityData);
                    }

                    if (entitiesData.Count > 0)
                    {
                        foreach (var file in CMPDocuments)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles/Tax";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }


                                string customerID = "0";

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = 1 + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (true)
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(VendorAuditMaster.ReadDocFiles(filePath)));
                                }
                                bw.Close();

                                ICDocPath = FileName;
                                //ICDocPath = ICDocPath.Substring(2, ICDocPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + ICDocPath + "');", true);
                                //lblMessage.Text = "";
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation();", true);
                            }
                            break;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                }
            }
        }

    }
}