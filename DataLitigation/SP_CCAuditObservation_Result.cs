//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataLitigation
{
    using System;
    
    public partial class SP_CCAuditObservation_Result
    {
        public long ID { get; set; }
        public string CCaseID { get; set; }
        public string CCaseNo { get; set; }
        public Nullable<System.DateTime> ObservationRecordDate { get; set; }
        public string AuditType { get; set; }
        public string AuditObserveYN { get; set; }
        public string AuditTypeOthers { get; set; }
        public string StatusAuditObservartion { get; set; }
        public string AuditObserFileName { get; set; }
        public string AuditReplyFileName { get; set; }
        public Nullable<long> CustomerID { get; set; }
        public Nullable<long> UserID { get; set; }
    }
}
