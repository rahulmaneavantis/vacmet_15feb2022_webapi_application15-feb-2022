//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataLitigation
{
    using System;
    using System.Collections.Generic;
    
    public partial class tax_tbl_IndirectCaseDeptForum
    {
        public int ID { get; set; }
        public string SCNDINNo { get; set; }
        public Nullable<System.DateTime> SCNDINDate { get; set; }
        public string IssuingAuthority { get; set; }
        public Nullable<System.DateTime> DateRecieptSCN { get; set; }
        public string GroundsSCN { get; set; }
        public string DisputedTax_Amount { get; set; }
        public string DisputedPenalty { get; set; }
        public string DisputedInterest { get; set; }
        public string DisputedFine { get; set; }
        public string TotalDisputedAmount { get; set; }
        public Nullable<System.DateTime> dateSCNInterestCal { get; set; }
        public string Remarks { get; set; }
        public string OIONoBySCN { get; set; }
        public Nullable<System.DateTime> OIODateBySCN { get; set; }
        public string txtOIOIssuingSCN { get; set; }
        public Nullable<System.DateTime> OIOReceiptDate { get; set; }
        public string GroundforpassingOIOBydeptSCN { get; set; }
        public string drdSCNAppealFiledYN { get; set; }
        public Nullable<int> drpappealfilledwith { get; set; }
        public Nullable<System.DateTime> ApealFilledDate { get; set; }
        public Nullable<System.DateTime> RepliedfiledByDate { get; set; }
        public Nullable<System.DateTime> RepliedfiledOnDate { get; set; }
        public string OngcDefence { get; set; }
        public string RepliedfiledWith { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<long> ICaseID { get; set; }
        public string OIOTaxAmount { get; set; }
        public string OIOPenalty { get; set; }
        public string OIOInterest { get; set; }
        public string OIOFine { get; set; }
        public string OIOTotalAmount { get; set; }
    }
}
