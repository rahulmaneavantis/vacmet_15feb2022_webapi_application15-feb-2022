//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWebApplication.DataLitigation
{
    using System;
    
    public partial class SP_IndirectCaseWorkspaceReport_Result
    {
        public long ICaseInstanceID { get; set; }
        public string Location { get; set; }
        public string ICaseNo { get; set; }
        public Nullable<int> TaxCategoryID { get; set; }
        public string TaxCategory { get; set; }
        public string CaseType { get; set; }
        public string CustomerBranchID { get; set; }
        public Nullable<int> ForumID { get; set; }
        public string Forum { get; set; }
        public string Assessee { get; set; }
        public Nullable<long> CustomerID { get; set; }
        public string AssesseeName { get; set; }
        public string OCStatus { get; set; }
        public string MergeCaseNO { get; set; }
        public string MergeStatus { get; set; }
        public string CaseBrief { get; set; }
        public string PrincipalAmount { get; set; }
        public string Interest { get; set; }
        public string Penalty { get; set; }
        public string Fine { get; set; }
        public string TotalAmount { get; set; }
        public string SAPDocNo { get; set; }
        public string ContingentAmount { get; set; }
        public Nullable<int> PreDepositTotal { get; set; }
        public Nullable<int> UnderProtestTotal { get; set; }
        public string CreatedForContingentLiability { get; set; }
        public string CreatedBy { get; set; }
    }
}
